# This script will create all the needed objects to support the Provisioning and Order states
# All object will be in a Pending/Order Received state and you will have to manually bring each Segment
# up to Live.
# This SHOULD NOT BE USED TO UPGRADE A LIVE SYSTEM - use state_upgrade.rb for that
#

# The order is important - do the Order first as the Prov state changes the Order state
OffNetOvcOrder.all.each {|o| o.statefuls << SegmentOrderStateful.create if o.statefuls.size == 0}
OnNetOvcOrder.all.each {|o| o.statefuls << SegmentOrderStateful.create if o.statefuls.size == 0}
EnniOrderNew.all.each {|o| o.statefuls << DemarcOrderStateful.create if o.statefuls.size == 0}

Node.all.each {|n| n.statefuls << NodeProvStateful.create if n.statefuls.size == 0}
Demarc.all.each {|d|
 d.statefuls << EnniProvStateful.create if d.is_a?(EnniNew) && d.statefuls.size == 0 
 d.statefuls << UniProvStateful.create if d.is_a?(Uni) && d.statefuls.size == 0
 d.statefuls << DemarcProvStateful.create if d.is_a?(Demarc) && d.statefuls.size == 0
}

Path.all.each {|path| path.statefuls << PathProvStateful.create if path.statefuls.size == 0}
SegmentEndPoint.all.each {|ep| ep.statefuls << EpProvStateful.create if ep.statefuls.size == 0}


Segment.all.each {|o| o.statefuls << SegmentProvStateful.create if o.statefuls.size == 0}

# Check that all statefuls are created
puts "Error not all ServiceOrder created" if !ServiceOrder.all.all? {|p| p.statefuls.size == 1}
puts "Error not all Node created" if !Node.all.all? {|p| p.statefuls.size == 1}
puts "Error not all Path created" if !Path.all.all? {|p| p.statefuls.size == 1}
puts "Error not all Segment created" if !Segment.all.all? {|p| p.statefuls.size == 1}
puts "Error not all SegmentEndPoint created" if !SegmentEndPoint.all.all? {|p| p.statefuls.size == 1}
puts "Error not all Demarc created" if !Demarc.all.all? {|p| p.statefuls.size == 1}

puts "Placing all Nodes insv"
Node.all.each {|n|
  n.set_prov_state(ProvPending)
  n.set_prov_state(ProvTesting)
  n.set_prov_state(ProvReady)
  n.set_prov_state(ProvLive)
}
=begin
Node.find_by_name("RIVNJALU01").set_prov_state(ProvPending)
Node.find_by_name("RIVNJALU01").set_prov_state(ProvTesting)
Node.find_by_name("RIVNJALU01").set_prov_state(ProvReady)
Node.find_by_name("RIVNJALU01").set_prov_state(ProvLive)

Node.find_by_name("RIVNJALU02").set_prov_state(ProvPending)
Node.find_by_name("RIVNJALU02").set_prov_state(ProvTesting)
Node.find_by_name("RIVNJALU02").set_prov_state(ProvReady)
Node.find_by_name("RIVNJALU02").set_prov_state(ProvLive)
=end



