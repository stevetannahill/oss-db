# Oct 27  2011
# All eventable object now have an event_record which is the calculated alarm state for the object
# The migration will have created all the event record but now they must be set to the current alarm
# This is done by doing an alarm sync on all alarm histories followed by an update for each alarm history with
# a full re-evaluation
AlarmHistory.alarm_sync_all
BrixAlarmHistory.alarm_sync_all

AlarmHistory.find_all_by_type("AlarmHistory").each {|ah| ah.event_ownerships.each {|eo| eo.eventable.update_alarm([],true) if eo.eventable != nil}}
BrixAlarmHistory.all.each {|ah| ah.event_ownerships.each {|eo| eo.eventable.update_alarm([],true) if eo.eventable != nil}}