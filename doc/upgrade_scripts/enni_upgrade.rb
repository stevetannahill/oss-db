# Aug 11 2011
# Ennis now need to have the port alarms added to their alarm histories
# Find all Ennis that are > Pending and are not "Fake" Ennis
# Call go_in_service for all those ennis
# Note If this is not done then nothig will break but you won't see the correct SM state in SIN when a port on a MCard LAG does down
ennis = EnniNew.all.collect {|enni| enni if not (enni.get_prov_state.is_a?(ProvPending) || enni.alarm_histories.any? {|ah| ah.event_filter[/FAKE/]})}.compact
ennis.each do |enni|
  enni.go_in_service   
end