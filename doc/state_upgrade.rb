# Script to do a live upgrade to the new Provisioning and Order states
# This only needs to be once when upgarding a live system from te old s/w to the new one
#
# The upgrade assumes the following 
# No object is in mtc
# The resulting Prov state will be either ProvPending or ProvLive - Nothing else
# The resulting Order State will be either FOC or Delivered
# If an insv object has a SO then it's 1st service order must have all dates filled in
#
# Before the deploy of the new load do the following on the old load
# script/console -s
# load "doc/upgrade_script.rb"
# check_upgrade
#
# Check the output for any errors or warnings. These should be fixed BEFORE doing the deploy
#
# After fixing the errors do a deploy
# The web pages will not load so do a 
# script/console
# load "doc/upgrade_script.rb"
# check_upgrade
# do_upgrade
#
# The web pages should load and the objects should be in the correct state and there should be no
# disruption to the system

def inservice(object)
  if object.respond_to?("do_not_use_in_service?")
    return object.do_not_use_in_service?
  else
    return object.in_service?
  end
end

def determinate_new_state(object)
  insv = inservice(object)
  if insv
    if object.mtc_state.state == AlarmSeverity::MTC
      return "mtc"
    end
    return "insv"
  end
  return nil
end

def anything_in_mtc
  result = false
  MtcHistory.all.each {|m|
    if m.alarm_state.state == AlarmSeverity::MTC
      puts "Upgrade script assumes nothing is in mtc"
      m.mtc_ownerships.each {|mt| puts "Object #{mt.mtcable.class}:#{mt.mtcable.id}"}
      result = true
    end
  }
  return result
end

def update_ts(object, ts)
  object.timestamp = ts.to_time
  object.save
end


def get_prov_state_times_from_so(object)
  # If 1st order has acceptance date use that as the Pending time
  so = object.service_orders.first
  if so == nil
    ts = object.prov_state.states.first.timestamp
    # The live timestamp is either creation date of the object + 3 minutes
    # or the timestamp of when the mtc history was created (if it has one)
    live_ts = ts + 3.minutes
    if object.mtc_periods.size != 0
      live_ts = object.mtc_periods.first.created_at 
    end
    
    return ts, ts + 1.minute, ts + 2.minutes, live_ts
  end
  
  pending_ts = object.prov_state.states.first.timestamp
  if so.order_acceptance_date != nil
    pending_ts = so.order_acceptance_date.to_time
  end
  
  testing_ts = pending_ts + 1.minute
  
  if so.order_completion_date != nil
    ready_ts = so.order_completion_date.to_time
    if so.order_acceptance_date == so.order_completion_date
      ready_ts = ready_ts + 2.minutes
    end
  end

  if so.billing_start_date != nil
    live_ts = so.billing_start_date.to_time
    if so.customer_acceptance_date == so.billing_start_date
      live_ts = live_ts + 3.minutes
    end
  end
  
  return pending_ts, testing_ts, ready_ts, live_ts
end  

# The order dates are just the dates and not the times but the states are ordered by time so
# must make a time to ensure that the 2 dates on the same day are different
def set_so(so, state)
  if state.new.is_a?(OrderAccepted)
    ts = so.order_received_date.to_time
    update_ts(so.order_state.states.first, ts)
    if so.order_acceptance_date != nil
      ts = so.order_acceptance_date.to_time
    end
    ts = ts + 10.minutes
    
    so.order_state.states << OrderAcceptedSegment.create(:timestamp => ts) if so.is_a?(OnNetOvcOrder) || so.is_a?(OffNetOvcOrder)
    so.order_state.states << OrderAcceptedDemarc.create(:timestamp => ts) if so.is_a?(DemarcOrder)
  else
    if so.order_received_date != nil
      ts = so.order_received_date.to_time
      update_ts(so.order_state.states.first, ts) 
      if so.order_acceptance_date != nil
        ts = so.order_acceptance_date.to_time
      end
      ts = ts + 10.minutes
      
      so.order_state.states << OrderAcceptedSegment.create(:timestamp => ts) if so.is_a?(OnNetOvcOrder) || so.is_a?(OffNetOvcOrder)
      so.order_state.states << OrderAcceptedDemarc.create(:timestamp => ts) if so.is_a?(DemarcOrder)
      
      if so.order_completion_date != nil
        ts = so.order_completion_date.to_time
      end
      ts = ts + 20.minutes
                      
      so.order_state.states << OrderProvisionedSegment.create(:timestamp => ts-1.minute) if so.is_a?(OnNetOvcOrder) || so.is_a?(OffNetOvcOrder)
      so.order_state.states << OrderProvisionedDemarc.create(:timestamp => ts-1.minute) if so.is_a?(DemarcOrder)
      so.order_state.states << OrderTestedSegment.create(:timestamp => ts) if so.is_a?(OnNetOvcOrder) || so.is_a?(OffNetOvcOrder)
      so.order_state.states << OrderTestedDemarc.create(:timestamp => ts) if so.is_a?(DemarcOrder)

      if so.customer_acceptance_date != nil
        ts = so.customer_acceptance_date.to_time
      end  
      ts = ts + 30.minutes
                  
      so.order_state.states << OrderCustomerAcceptedSegment.create(:timestamp => ts) if so.is_a?(OnNetOvcOrder) || so.is_a?(OffNetOvcOrder)
      so.order_state.states << OrderCustomerAcceptedDemarc.create(:timestamp => ts) if so.is_a?(DemarcOrder)
    
      if so.billing_start_date != nil
        ts = so.billing_start_date.to_time
      end   
      ts = ts + 40.minutes
                 
      so.order_state.states << OrderDeliveredSegment.create(:timestamp => ts) if so.is_a?(OnNetOvcOrder) || so.is_a?(OffNetOvcOrder)
      so.order_state.states << OrderDeliveredDemarc.create(:timestamp => ts) if so.is_a?(DemarcOrder)
    else
      puts "Error #{so.class}:#{so.id} does not have a order_received_date"
    end
  end
end  

# The upgrade assumes the following 
# No object is in mtc
# The resulting Prov state will be either ProvPending or ProvLive - Nothing else
# The resulting Order State will be either FOC or Delivered
def do_upgrade
  # Check that nothing is in mtc - The script assumes this...for simplicity sake
  return if anything_in_mtc
  if Stateful.all.size != 0 || State.all.size != 0 || StatefulOwnership.all.size != 0
    puts "do_upgrade has already been run - exiting"
    return
  end
  
  # Hook to disable the running of pre/post condition when creating a OrderStateful or ProvStateful
  StatefulOwnership.set_state_on_create(false)
  
  # when a stateful object is created it sets the initial state to ProvPending/OrderReceived
  # then the states are set according to the old in_service? api
  
  # The order is important - do the Order first as the Prov state changes the Order state
  # Set the first timestamp to be the create_at date
  OffNetOvcOrder.all.each {|o|
    if o.statefuls.size == 0
      o.statefuls << SegmentOrderStateful.create
      update_ts(o.order_state.states.first, o.created_at)
    end
  }
  
  OnNetOvcOrder.all.each {|o| 
    if o.statefuls.size == 0
      o.statefuls << SegmentOrderStateful.create
      update_ts(o.order_state.states.first, o.created_at)      
    end
  }
  
  EnniOrderNew.all.each {|o| 
    if o.statefuls.size == 0
      o.statefuls << DemarcOrderStateful.create 
      update_ts(o.order_state.states.first, o.created_at)      
    end
  }

  # All nodes have provisioning state
  Node.all.each {|n|
    if n.statefuls.size == 0
      n.statefuls << NodeProvStateful.create 
      update_ts(n.prov_state.states.first, n.created_at)
    end
  }

  # All ENNI and Uni have provisioning state
  Demarc.all.each {|d|
   if d.is_a?(EnniNew) && d.statefuls.size == 0
     d.statefuls << EnniProvStateful.create  
     update_ts(d.prov_state.states.first, d.created_at)
   elsif d.is_a?(Uni) && d.statefuls.size == 0
     d.statefuls << UniProvStateful.create  
     update_ts(d.prov_state.states.first, d.created_at)
   else  
     d.statefuls << DemarcProvStateful.create  
     update_ts(d.prov_state.states.first, d.created_at)
   end
  }

  # All Paths have provisioning state
  Path.all.each {|path|
    if path.statefuls.size == 0
      path.statefuls << PathProvStateful.create
      update_ts(path.prov_state.states.first, path.created_at)
    end
  }

  # All Endpoints have provisioning state
  SegmentEndPoint.all.each {|ep|
    if ep.statefuls.size == 0
      ep.statefuls << EpProvStateful.create
      update_ts(ep.prov_state.states.first, ep.created_at)      
    end
  }

  # All Segments have provisioning state
  Segment.all.each {|o| 
    if o.statefuls.size == 0
      o.statefuls << SegmentProvStateful.create
      update_ts(o.prov_state.states.first, o.created_at)
    end
  }


  # Check that all statefuls are created
  puts "Error not all ServiceOrder created" if !ServiceOrder.all.all? {|p| p.statefuls.size == 1}
  puts "Error not all Node created" if !Node.all.all? {|p| p.statefuls.size == 1}
  puts "Error not all Path created" if !Path.all.all? {|p| p.statefuls.size == 1}
  puts "Error not all Segment created" if !Segment.all.all? {|p| p.statefuls.size == 1}
  puts "Error not all SegmentEndPoint created" if !SegmentEndPoint.all.all? {|p| p.statefuls.size == 1}
  puts "Error not all Demarc created" if !Demarc.all.all? {|p| p.statefuls.size == 1}


  Node.all.each {|n| 
    s = determinate_new_state(n)
    if s == 'insv'
      ts = n.prov_state.states.first.timestamp       
      n.prov_state.states << ProvTestingNode.create(:timestamp => ts + 1.minutes)
      n.prov_state.states << ProvReadyNode.create(:timestamp => ts + 2.minutes)      
      # by default the live time is creation time + 3
      # unless there is a mtc history in which case it is it's creation time
      live_ts = ts + 3.minutes 
      if n.mtc_periods.size != 0
        live_ts = n.mtc_periods.first.created_at 
      end
      n.prov_state.states << ProvLiveNode.create(:timestamp => live_ts)
    end     
  }

  Demarc.all.each {|d|
   s = determinate_new_state(d)
   ts = d.prov_state.states.first.timestamp
   so = d.service_orders.first
   
   if s != 'insv'
     # If 1st order has acceptance date use that as the Pending time
     if so != nil && so.order_acceptance_date != nil
       update_ts(d.prov_state.states.first, so.order_acceptance_date.to_time)
       ts = so.order_acceptance_date.to_time
     end
   end
  
   pending_ts, testing_ts, ready_ts, live_ts = get_prov_state_times_from_so(d)
         
   if d.is_a?(EnniNew) && s == "insv"
     # If 1st order has acceptance date use that as the Pending time
     update_ts(d.prov_state.states.first, pending_ts)
     d.prov_state.states << ProvTestingEnni.create(:timestamp => testing_ts)
     d.prov_state.states << ProvReadyEnni.create(:timestamp => ready_ts)
     d.prov_state.states << ProvLiveEnni.create(:timestamp => live_ts)
   elsif d.is_a?(Uni) && s == "insv"
     update_ts(d.prov_state.states.first, pending_ts)
     d.prov_state.states << ProvTestingUni.create(:timestamp => testing_ts)
     d.prov_state.states << ProvReadyDemarc.create(:timestamp => ready_ts)
     d.prov_state.states << ProvLiveDemarc.create(:timestamp => live_ts)
   elsif s == "insv"
     update_ts(d.prov_state.states.first, pending_ts)
     d.prov_state.states << ProvTestingDemarc.create(:timestamp => testing_ts)
     d.prov_state.states << ProvReadyDemarc.create(:timestamp => ready_ts)
     d.prov_state.states << ProvLiveDemarc.create(:timestamp => live_ts)     
   end
   
   # All but the latest service order must be delivered. The latest SO will be either Delivered or
   # OrderAccepted
   d.service_orders[0..-2].each {|so|
     set_so(so, OrderDeliveredDemarc)
   }
   if d.get_latest_order != nil
     so = d.get_latest_order
     if d.get_prov_state.is_a?(ProvLive)
       set_so(so, OrderDeliveredDemarc)
     elsif d.get_prov_state.is_a?(ProvPending)
       set_so(so, OrderAcceptedDemarc)
     else
       puts "Demarc #{d.id} is not Live or Pending #{d.get_prov_name} latest SO #{so.id}"
     end
   end
  }


  # Set the Segment state according to the old state and set the endpoint states to the same 
  # state.
  Segment.all.each {|o| 
    s = determinate_new_state(o)
    ts = o.prov_state.states.first.timestamp  
    if s == "insv"
      pending_ts, testing_ts, ready_ts, live_ts = get_prov_state_times_from_so(o)
      update_ts(o.prov_state.states.first, pending_ts)
      o.prov_state.states << ProvTestingSegment.create(:timestamp => testing_ts)
      o.prov_state.states << ProvReadySegment.create(:timestamp => ready_ts)   
      o.prov_state.states << ProvLiveSegment.create(:timestamp => live_ts) 
      o.segment_end_points.each {|ep|
        update_ts(ep.prov_state.states.first, pending_ts)      
        ep.prov_state.states << ProvTestingEp.create(:timestamp => testing_ts)
        ep.prov_state.states << ProvReadyEp.create(:timestamp => ready_ts)
        ep.prov_state.states << ProvLiveEp.create(:timestamp => live_ts)
      }
    end
    
    # All but the latest service order must be delivered. The latest SO will be either Delivered or
    # OrderAccepted
    o.service_orders[0..-2].each {|so|
      set_so(so, OrderDeliveredSegment)
    }
    o.reload
    if o.get_latest_order != nil
      so = o.get_latest_order
      if o.get_prov_state.is_a?(ProvLive)
        set_so(so, OrderDeliveredSegment)
      elsif o.get_prov_state.is_a?(ProvPending)
        set_so(so, OrderAcceptedSegment)
      else
        puts "Segment #{o.id} is not Live or Pending #{o.get_prov_name} latest SO #{so.id}"
      end
    end
    
  }

  # The Path state is determinted by the lowest of the Segment states
  Path.all.each {|path|   
    new_state = nil
    segment_states = path.segments.collect {|segment| segment.prov_state.reverse_mapping(segment.get_prov_state) if segment.prov_state != nil}.compact
    
    if segment_states.any?{|state| state == ProvMaintenance}
      new_state = ProvMaintenance
    elsif !segment_states.empty?
      # There are Segment's so find the lowest one
      new_state = path.prov_state.state_hierarchy[segment_states.collect {|segment_state| path.prov_state.state_hierarchy.index(segment_state)}.compact.min]
    else
      # There are no Segments just stay in the same state
      new_state = nil
    end
    
    if new_state != nil
      if new_state == ProvLive
        live_ts = path.segments.collect {|segment| segment.get_prov_state.timestamp if segment.get_prov_state.is_a?(ProvLive)}.compact.max
        if live_ts == nil
          puts "Error Path #{live_ts} #{path.id}"
        else
          path.prov_state.states << ProvLivePath.create(:timestamp => live_ts)
        end
      else
        puts "Path #{path.id} is not Live"
      end
    end
  }
   
  # Check that all statefuls are created
  puts "Error not all ServiceOrder have correct number of states" if !OrderStateful.all.all? {|p| p.states.size == 2 || p.states.size == 6}
  puts "Error not all ServiceOrder created" if !(ProvStateful.all - PathProvStateful.all).all? {|p| p.states.size == 1 || p.states.size == 4}
  
  # Check that all prov states are either Pending or Live
  puts "Error stateful not in Prov or Live" if !ProvStateful.all.all? {|p| p.get_state.is_a?(ProvPending) || p.get_state.is_a?(ProvLive) }
  puts "Error stateful not in Foc or Delivered" if !OrderStateful.all.all? {|p| p.get_state.is_a?(OrderAccepted) || p.get_state.is_a?(OrderDelivered) }
  
  
  StatefulOwnership.set_state_on_create(true)
  
end

# Check that all the Path/Segments and Endpoints will all align
# if an Segment is insv and some of it's ep are not insv this is a problem
# If an Segment is not insv and the endpoints are insv this is ok as the Segment will be in pending state as will the endpoints so a change to the Segment state
# will cause a re-init of the endpoints - either make the Segment insv or just leave
# If an endpoint is insv then the ENNI and Node must be insv
# If an Path is NOT insv and all of it's Segments are insv then this is problem as the Path state will straight to live without setting up SM history etc
def check_upgrade
  
  return if anything_in_mtc
  
  # Check for each endpoint that is insv that the Demarc and Node are insv
  # Each endpoint that is insv should have the Segment insv
  SegmentEndPoint.all.each {|ep|
    #puts "Checking Endpoint #{ep.class}:#{ep.id}"
    if determinate_new_state(ep) == "insv"
      if ep.demarc != nil
        if determinate_new_state(ep.demarc) == 'insv'
          if ep.demarc.ports.empty? && ep.demarc.is_a?(EnniNew)
            puts "Fail Endpoint #{ep.class}:#{ep.id} is insv but demarc #{ep.demarc.class}:#{ep.demarc.id} has no ports"
          else
            if ep.demarc.ports.all? {|port| determinate_new_state(port.node) == "insv"}
              puts "Endpoint #{ep.class}:#{ep.id} is Ok"
            else
              puts "Fail Endpoint #{ep.class}:#{ep.id} is Bad - The following nodes are not insv"
              ep.demarc.ports.each {|port| puts "#{port.node.class}:#{port.node.id}" if determinate_new_state(port.node) != "insv"}
            end
          end
        else
          puts "Fail Endpoint #{ep.class}:#{ep.id} is Insv but the demarc #{ep.demarc.class}:#{ep.demarc.id} is not insv it has #{ep.demarc.ports.size} Ports (should be non zero) mtc size is #{ep.demarc.mtc_periods.size} (should be non zero)"
        end
      else
        puts "Fail Endpoint #{ep.class}:#{ep.id} - The endpoint is insv but has no demarc"
      end
      if determinate_new_state(ep.segment) != "insv"
        puts "Warning Endpoint #{ep.class}:#{ep.id} is insv but the Segment #{ep.segment.class}:#{ep.segment.id} is not insv. Either the Segment is not meant to be insv or make sure the Segment is insv"
      end
    end      
  }
  
  # If an Path is not insv then if all of it's Segments are insv then the Path must be placed insv
  Path.all.each {|path|
    if determinate_new_state(path) != "insv"
      if path.segments.all? {|segment| determinate_new_state(segment) == "insv"}
        puts "Fail Path #{path.class}:#{path.id} is not insv but all it's Segments are - Make the Path insv"
      end
    end
  }
  
  # If an Segment is insv then all of it's EPs should be insv 
  Segment.all.each {|segment|
    if determinate_new_state(segment) == "insv"
      if !segment.segment_end_points.all? {|ep| determinate_new_state(ep) == "insv"}
        puts "Fail Segment #{segment.class}:#{segment.id} is insv but some of it's endpoints are not insv"
        segment.segment_end_points.each {|ep| puts "  endpoint #{ep.class}:#{ep.id}" if determinate_new_state(ep) != "insv"}
      else
        # Use the mtc_period creation date as insv date for ENNIs
        if segment.mtc_periods.empty?
          timestamp = segment.created_at
        else
          timestamp = segment.mtc_periods.first.created_at
        end 
        if timestamp == nil
          puts "Error Segment #{segment.class}:#{segment.id} is insv but can't determie the creation time"
        end
        puts "Segment #{segment.class}:#{segment.id} is Ok"
      end
    end
  }
  
  # The service order dates must be accending
  
  ServiceOrder.all.each do |so|
    #puts "Checking SO #{so.id}"
    ts = nil
    if so.order_received_date != nil
      ts = so.order_received_date
      if so.order_acceptance_date != nil && so.order_acceptance_date >= ts
        ts = so.order_acceptance_date
      else
        puts "Error #{so.class}:#{so.id} dates are not accending prevous #{ts} order_acceptance_date #{so.order_acceptance_date}" if so.order_acceptance_date != nil
      end

      if so.order_completion_date != nil && so.order_completion_date >= ts
        ts = so.order_completion_date
      else
        puts "Error #{so.class}:#{so.id} dates are not accending prevous #{ts} order_completion_date #{so.order_completion_date}" if so.order_completion_date != nil
      end

      if so.customer_acceptance_date != nil && so.customer_acceptance_date >= ts
        ts = so.customer_acceptance_date
      else
        puts "Error #{so.class}:#{so.id} dates are not accending prevous #{ts} customer_acceptance_date #{so.customer_acceptance_date}" if so.customer_acceptance_date != nil     
      end  

      if so.billing_start_date != nil && so.billing_start_date >= ts
        ts = so.billing_start_date
      else
        puts "Error #{so.class}:#{so.id} dates are not accending prevous #{ts} billing_start_date #{so.billing_start_date.inspect}" if so.billing_start_date != nil      
      end   
    else
      puts "Error #{so.class}:#{so.id} does not have a order_received_date"
    end
    # If the ordered_entity is insv it must have ALL the dates filled in
    if inservice(so.ordered_entity)
      if so.order_received_date.nil? || so.order_acceptance_date.nil? || so.order_completion_date.nil? || so.customer_acceptance_date.nil? || so.billing_start_date.nil?
        puts "Error #{so.class}:#{so.id} for entity #{so.ordered_entity.class}:#{so.ordered_entity.id} is insv but the ServiceOrder is missing dates"
      end
    end 
  end
  
  return nil
end
  
  
  
