OssDb::Application.routes.draw do
  root :to => 'admin#home'
  match '/logout' => 'application#logout', :as => :logout
  match '/version' => 'version#show', :as => :version
  match '/plan_of_execution/process/:id' => 'plan_of_execution#process_poe'

  resource :report, :only => [:index] do
    get :index    
    get :sla
    get :oem_sla
    get :circuit_utilization
    get :enni_utilization
  end
  
  resources :ennis
  resources :ports
  resources :nodes

  match '/plan_of_execution(/:action(/:id(/:version)))' => "plan_of_execution"

  match '/:controller(/:action(/:id))'
end

