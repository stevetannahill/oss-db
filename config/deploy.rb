#-----------------
# CUSTOM
#-----------------

require "bundler/capistrano"

set :application, "oss-db"
set :repo_name, application

after "deploy:update_code",       "deploy:copy_cas_configuration"
after "deploy:update_code",       "deploy:copy_02_cenx_apps_configuration"
after "deploy:update_code",       "deploy:copy_action_mailer_config"
after "deploy:update_code",       "deploy:copy_database_configuration"
after "deploy:update_code",       "deploy:copy_cmaudit_configuration"
after "deploy:update_code",       "deploy:copy_event_server_properties"
after "deploy:update_code",       "deploy:copy_sphinx_configuration"
# ONLY RUN HACKS FOR NEW SERVERS, OR WHEN ORDERING SCREENSHOTS CHANGE
# after "deploy:update_code",       "deploy:hack_copy_ordering_screenshots"
# after "deploy:update_code",       "deploy:hack_update_ordering_cmd_history"
after "deploy:update_code",       "deploy:fix_sessions_dir"
after "deploy:update_code",       "deploy:copy_lkp_config"
after "deploy:update_code",       "deploy:copy_osl_poe_config"
after "deploy:update_code",       "deploy:copy_grid_config"
after "deploy:update_code",       "deploy:copy_abc_config"
after "deploy:update_code",       "deploy:copy_summary_page_ssh"
after "deploy:create_symlink",    "deploy:monit:copy_configs" unless exists?(:headless)
after "deploy:create_symlink",    "deploy:monit:restart" unless exists?(:headless)

require "bundler/capistrano"

set :whenever_command, "bundle exec whenever"
require "whenever/capistrano"

set :monit_configs, [
          :sam_if_server_daemon, 
          :bx_if_server_daemon, 
          :bx_snmp_daemon, 
          :event_server_daemon, 
          :sam_jms_daemon,
          :osl_resque_daemon, # only in CDB (check if SIN needs updating)
          :chris_order_daemon, # only in CDB
          :availability_daemon, # only in CDB (check if SIN needs updating)
          :automated_build_center,
          :exception_daemon
        ]

namespace :bundle do
  desc "bundle install for the main application and the sam_stats_if dir"
  task :install do
    run("cd #{release_path};bundle install --gemfile #{File.join(release_path, "Gemfile")} --path /cenx/oss-db/shared/bundle --quiet --without development test")
    sam_if_path = File.join(release_path, "lib", "sam_stats_if")
    run("cd #{sam_if_path};LANG=en_CA.UTF-8 bundle install --gemfile #{File.join(sam_if_path, "Gemfile")} --path /cenx/oss-db/shared/sam_jms_bundle --quiet --without development test")
  end
end

namespace :deploy do
  task :copy_02_cenx_apps_configuration do 
    config_file = "cenx_apps.rb"
    cenx_apps_config = "/cenx/sensitive/#{config_file}"
    run "cp #{cenx_apps_config} #{release_path}/config/initializers/02_#{config_file}"
  end  
  
  task :copy_osl_poe_config do 
    config_file = "osl.poe.yml"
    cenx_apps_config = "/cenx/sensitive/#{config_file}"
    run "cp #{cenx_apps_config} #{release_path}/config/#{config_file}"
  end
  
  task :copy_action_mailer_config do 
    config_file = "action_mailer_config.rb"
    cenx_apps_config = "/cenx/oss-db/sensitive/#{config_file}"
    run "cp #{cenx_apps_config} #{release_path}/config/initializers/#{config_file}"
  end

  task :copy_summary_page_ssh do 
    config_file = "summary_page_ssh.json"
    cenx_apps_config = "/cenx/oss-db/sensitive/#{config_file}"
    run "cp #{cenx_apps_config} #{release_path}/config/#{config_file}"
  end

  [{:action => :copy_lkp_config, :file => "lkp.json"},
   {:action => :copy_abc_config, :file => "abc_config.cmd"},
   {:action => :copy_grid_config, :file => "grid.yml"}].each do |config_file|
     task config_file[:action] do
       config_file = config_file[:file]
       cenx_apps_config = "/cenx/oss-db/sensitive/#{config_file}"
       run "cp #{cenx_apps_config} #{release_path}/config/#{config_file}"
     end
   end

  #TODO reused in sin's deploy.rbenv.rb need to pull this out into tasks, or something
  namespace :monit do
    [:disable,:enable,:restart].each do |action|
      desc "#{action.to_s.capitalize} all monitoring processes during a deploy"
      task action do
        monit_configs.each do |monit_name|
          if action == :disable
            run("monit stop #{monit_name}; true") #unmonitor will fail if it isn't configured yet, and that's fine
          elsif action == :enable
            run("while ! monit monitor #{monit_name}; do ruby -e 'sleep 0.5'; done")
          else
            run("while ! monit #{action} #{monit_name}; do ruby -e 'sleep 0.5'; done")
          end
        end
      end
    end
    
    desc "copy the monit.d config files into position"
    task :copy_configs do
      # Clear old or now unnecessary monit config file
      run("rm -f ~/monit.d/#{application}/*.cnf")
      # Copy only needed monit config file, so different machines can run different daemons
      # adjust this in the config/deploy/*.rb scripts by setting monit_configs
      monit_configs.each do |monit_name|
        run("cp #{release_path}/config/monit.d/#{monit_name}.cnf ~/monit.d/#{application}")
      end
      run("monit reload")
      sleep 2 # without this a monit restart shortly after fails
    end
  end
end

#-----------------
# COMMON DEPLOY TASKS
#-----------------

set :scm, :git 
set :repository,  "git@smeagol.cenx.localnet:#{repo_name}.git" # Must be accessible from each deployment location
set :user, "deployer"
set :deploy_to, "/cenx/#{application}"
set :shared_sensitive_path, "/cenx/sensitive"
set :use_sudo, false
set :default_stage, "staging"
set :git_enable_submodules, 1
set :deploy_via, :remote_cache # Makes deploying much faster since it caches the git repo on the target server instead of a full clone each time
default_run_options[:pty] = true
