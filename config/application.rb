require File.expand_path('../boot', __FILE__)

require 'rails/all'

# If you have a Gemfile, require the gems listed there, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env) if defined?(Bundler)

module OssDb
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Custom directories with classes and modules you want to be autoloadable.
    extra_lib_dirs = %W(sam_stats_if data_archive bx_stats_if stats id_tools name_tools automated_build_center audits cenx_crypt)
    config.autoload_paths += extra_lib_dirs.map{|dir_name| "#{config.root}/lib/#{dir_name}"}
    
    require 'find'
    # Add the state models to the load path
		Find.find("#{config.root}/app/models/admin") do |path|
			if FileTest.directory?(path)
				config.autoload_paths << path
			end
		end
    
    # Only load the plugins named here, in the order given (default is alphabetical).
    # :all can be used as a placeholder for all plugins not explicitly named.
    # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

    # Activate observers that should always be running.
    # config.active_record.observers = :cacher, :garbage_collector, :forum_observer

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # JavaScript files you want as :defaults (application.js is always included).
    config.action_view.javascript_expansions[:defaults] = %w(prototype effects dragdrop controls widgets/tooltip rails)

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = "utf-8"

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password, :password_confirmation]

    # Include coresite extensions if coresite application, determined by existence of coresite_application.rb file
    if FileTest.file?("#{Rails.root}/config/coresite_application.rb")
      config.autoload_paths << "#{config.root}/lib/coresite"
      # to_prepare callbacks for Action Dispatch which will be ran per request in development, or before the first request in production.
      # Must be load (instead of require), so modifying code in development doesn't require a server restart
      config.to_prepare do
        load "coresite_extensions.rb"
      end
    end


  end
end
