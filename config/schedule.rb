# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

set :output, 'log/cron.log'
job_type :runner_file, "/home/deployer/local/bin/oss-db_env bundle exec rails runner :path/:task"
job_type :runner,  "/home/deployer/local/bin/oss-db_env bundle exec rails runner ':task'"

every 1.day, :at => "10:00 pm" do
  runner "Schedule.remove_old_reports; Schedule.resync_crontab"
end


every 30.minutes do
  runner "ReportMaker.gen_prev_report"
end

