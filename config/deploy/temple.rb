# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
# set :branch, "ericsson5_demo"
set :branch, "ordering2"
set :versionable, false

role :web, "temple.cenx.localnet"                   # Your HTTP server, Apache/etc
role :app, "temple.cenx.localnet"                   # This may be the same as your `Web` server
role :db,  "temple.cenx.localnet", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"

set :monit_configs, [ ]
