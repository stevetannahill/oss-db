# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
set :branch, "master"
set :versionable, false

role :web, "172.16.247.147"                          # Your HTTP server, Apache/etc
role :app, "172.16.247.147"                          # This may be the same as your `Web` server
role :db,  "172.16.247.147", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"
