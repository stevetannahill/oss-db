# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
set :branch, "master"
set :versionable, false

role :web, "66.1.220.131"                   # Your HTTP server, Apache/etc
role :app, "66.1.220.131"                   # This may be the same as your `Web` server
role :db,  "66.1.220.131", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"

# All daemons aren't required on sprint's deployments
set :monit_configs, [
                      :event_server_daemon, 
                      :availability_daemon,
                      :automated_build_center 
                    ]