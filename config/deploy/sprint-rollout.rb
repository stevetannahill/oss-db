# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
# set :branch, "ericsson5_demo"
set :branch, "sprint_rollout"
set :versionable, false

role :web, "sprint-rollout.cenx.localnet"                   # Your HTTP server, Apache/etc
role :app, "sprint-rollout.cenx.localnet"                   # This may be the same as your `Web` server
role :db,  "sprint-rollout.cenx.localnet", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"

set :monit_configs, [
          :event_server_daemon, 
          :osl_resque_daemon, # only in CDB (check if SIN needs updating)
          :chris_order_daemon, # only in CDB
          :availability_daemon, # only in CDB (check if SIN needs updating)
          :automated_build_center
        ]
