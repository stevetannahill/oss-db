# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
set :branch, "monitoring_demo_1"
set :versionable, false

role :web, "market-demo.cenx.localnet"                          # Your HTTP server, Apache/etc
role :app, "market-demo.cenx.localnet"                          # This may be the same as your `Web` server
role :db,  "market-demo.cenx.localnet", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"

