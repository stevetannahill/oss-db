# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
set :branch, "coresite1a"
set :versionable, false

role :web, "coresite-dev.cenx.localnet"                          # Your HTTP server, Apache/etc
role :app, "coresite-dev.cenx.localnet"                          # This may be the same as your `Web` server
role :db,  "coresite-dev.cenx.localnet", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"

after "deploy:update_code",       "deploy:enable_coresite_auth"
after "deploy:update_code",       "deploy:configure_coresite"

set :monit_configs, []