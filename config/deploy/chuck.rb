# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
set :branch, "chuck"
set :versionable, false

role :web, "chuck.cenx.localnet"                          # Your HTTP server, Apache/etc
role :app, "chuck.cenx.localnet"                          # This may be the same as your `Web` server
role :db,  "chuck.cenx.localnet", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"
