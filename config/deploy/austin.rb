# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
# set :branch, "ericsson5_demo"
set :branch, "sprint_rollout_ordering_merge"
set :versionable, false

role :web, "austin.cenx.localnet"                   # Your HTTP server, Apache/etc
role :app, "austin.cenx.localnet"                   # This may be the same as your `Web` server
role :db,  "austin.cenx.localnet", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"

set :monit_configs, [ ]
