# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
set :branch, "sprint_ordering"
set :versionable, true

role :web, "gollum.cenx.localnet"                          # Your HTTP server, Apache/etc
role :app, "gollum.cenx.localnet"                          # This may be the same as your `Web` server
role :db,  "gollum.cenx.localnet", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"
