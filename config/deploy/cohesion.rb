# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
set :branch, "vanilla_demo"
set :versionable, false

role :web, "cohesion.cenx.localnet"                   # Your HTTP server, Apache/etc
role :app, "cohesion.cenx.localnet"                   # This may be the same as your `Web` server
role :db,  "cohesion.cenx.localnet", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"
