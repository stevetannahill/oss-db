# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
set :branch, "master"
set :versionable, false

ssh_options[:port] = 8902

role :web, "staging-test1.cenx.test"                   # Your HTTP server, Apache/etc
role :app, "staging-test1.cenx.test"                   # This may be the same as your `Web` server
role :db,  "staging-test1.cenx.test", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"

# All daemons aren't required on sprint's deployments
set :monit_configs, [
                      :event_server_daemon, 
                      :availability_daemon,
                      :automated_build_center 
                    ]