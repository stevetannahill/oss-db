# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
set :branch, "master"
set :versionable, false

role :web, "inventory-live-prod.cenx.localnet"                          # Your HTTP server, Apache/etc
role :app, "inventory-live-prod.cenx.localnet"                          # This may be the same as your `Web` server
role :db,  "inventory-live-prod.cenx.localnet", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"
