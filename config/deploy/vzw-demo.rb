# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
set :branch, "vzw_demo"
set :versionable, false

role :web, "vzw-demo.cenx.localnet"                   # Your HTTP server, Apache/etc
role :app, "vzw-demo.cenx.localnet"                   # This may be the same as your `Web` server
role :db,  "vzw-demo.cenx.localnet", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"
