# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
set :branch, "master"
set :versionable, true

role :web, "hayden.cenx.localnet"                          # Your HTTP server, Apache/etc
role :app, "hayden.cenx.localnet"                          # This may be the same as your `Web` server
role :db,  "hayden.cenx.localnet", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"
