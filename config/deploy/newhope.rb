# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
# set :branch, "ericsson5_demo"
set :branch, "sprint_rollout"
set :versionable, false

role :web, "newhope.cenx.localnet"                   # Your HTTP server, Apache/etc
role :app, "newhope.cenx.localnet"                   # This may be the same as your `Web` server
role :db,  "newhope.cenx.localnet", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"
