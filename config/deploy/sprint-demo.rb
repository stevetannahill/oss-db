# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
set :branch, "master"
set :versionable, false

role :web, "sprint-demo.cenx.test"                   # Your HTTP server, Apache/etc
role :app, "sprint-demo.cenx.test"                   # This may be the same as your `Web` server
role :db,  "sprint-demo.cenx.test", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"
