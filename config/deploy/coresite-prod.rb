# STAGING-specific deployment configuration
# please put general deployment config in config/deploy.rb
set :branch, "coresite1a"
set :versionable, false

role :web, "64.62.162.30"                          # Your HTTP server, Apache/etc
role :app, "64.62.162.30"                          # This may be the same as your `Web` server
role :db,  "64.62.162.30", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"

after "deploy:update_code",       "deploy:enable_coresite_auth"
after "deploy:update_code",       "deploy:configure_coresite"

set :monit_configs, []