require 'tmpdir'

namespace :deploy do
  desc "create the database, can only be called after a cap deploy"
  task :create_db, :roles => :db do
    run "cd #{current_path} && rvm use #{rvm_ruby_gemset} && rake db:create RAILS_ENV=production && rake db:migrate RAILS_ENV=production"
  end
end

namespace :db do

  namespace :push do
    desc <<-DESC
      Copy the database(s) from local to deployment machine
    DESC
    task :default, :roles => :app do
      Dir.mktmpdir do |dump_dir|
        dump_file_path = File.join(dump_dir,"dump.sql")
        Tempfile.open('mysql_options') do |file|
          file.write("[client]\npassword=\"#{db_info['password']}\"")
        
          `mysqldump --defaults-file=#{file.path} -u #{db_info['username']} #{db_info['database']} > #{dump_file_path}`
          #Add any additional database dumping here to be included in the tar file
        end
        Dir.mktmpdir do |output_dir|
          tar_file_path = File.join(output_dir,"dump.tgz")
          `cd #{dump_dir} && tar czf #{tar_file_path} .`

          push_path = "#{deploy_to}/current/tmp/push"
          dump_path = "#{push_path}/dump_#{Time.now.to_i}.tgz"
          run "mkdir -p #{push_path}"
          upload tar_file_path, dump_path
          
          run "rake db:drop"
          run "rake db:create"
          run "tar xzOf #{dump_path} ./dump.sql | script/dbconsole" #Still needs to be fully tested
        end
      end
    end
  end
  namespace :pull do
    desc <<-DESC
      Copy the database(s) from deployment to local machine
    DESC
    task :default, :roles => :app do
      pull_path = "#{deploy_to}/current/tmp/pull"
      dump_path = "#{pull_path}/dump"
      run "mkdir -p #{dump_path}"
      dump_file_path = File.join(dump_path,"dump.sql")
      
      Dir.mktmpdir do |yml_dir|
        rails_env = "production"
        
        get "#{deploy_to}/current/config/database.yml", "#{yml_dir}/database.yml"
        
        info = YAML::load(IO.read("#{yml_dir}/database.yml"))
        remote_db_info = info[rails_env]
        
        mysql_options_file = "#{pull_path}/mysql_options"
        put "[client]\npassword=\"#{remote_db_info['password']}\"", mysql_options_file
        
        run "mysqldump --defaults-file=#{mysql_options_file} -u #{remote_db_info['username']} #{remote_db_info['database']} > #{dump_file_path}"
        
        tar_file_path = File.join(pull_path,"dump.tgz")
        run "cd #{dump_path} && tar czf #{tar_file_path} ."
        
        Dir.mktmpdir do |download_dir|
          download_tar_path = "#{download_dir}/dump.tgz"
          download tar_file_path, download_tar_path, :once => true
          `rake db:drop`
          `rake db:create`

          if File.exists?('script/dbconsole')
            `tar xzOf #{download_tar_path} ./dump.sql | script/dbconsole`
          else
            `tar xzOf #{download_tar_path} ./dump.sql | rails dbconsole`
          end

          `rake db:migrate`
          `rake ts:rebuild`

        end
      end
    end
  end
end
