before "deploy:setup", "deploy:configure_gemset"
namespace :deploy do
  task :copy_rvmrc do
    run "cp #{release_path}/config/deploy.rvmrc #{release_path}/.rvmrc"
  end
  task :trust_rvmrc do
    run "cd #{release_path} && rvm rvmrc trust .rvmrc"
    run "cd #{current_path} && rvm rvmrc trust .rvmrc"
  end
  
  task :configure_gemset do
    run "rvm use #{rvm_ruby} && rvm gemset create #{rvm_gemset}"
  end
  
end