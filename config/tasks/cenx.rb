def remote_file_exists?(full_path)
  capture("if [ -e #{full_path} ]; then echo 'true'; fi").strip == 'true'
end

before "deploy:update_code", "local:update_version"
namespace :deploy do

  task :copy_database_configuration do 
    template_file = "#{deploy_to}/sensitive/database.yml" 
    run "cp #{template_file} #{release_path}/config/database.yml"
  end

  task :copy_cmaudit_configuration do 
    template_file = "#{deploy_to}/shared/cmaudit.yml" 
    run "cp #{template_file} #{release_path}/config/cmaudit.yml"
  end

  task :fix_sessions_dir do
    run "mkdir #{release_path}/tmp/sessions"
  end

  task :copy_cas_configuration do 
    template_file = "#{shared_sensitive_path}/cas_name.rb"
    run "cp #{template_file} #{release_path}/config/initializers/cas_name.rb"
  end

  task :copy_sphinx_configuration do 
    template_file = "#{deploy_to}/sensitive/sphinx.yml"
    run "cp #{template_file} #{release_path}/config/sphinx.yml"
  end

  task :copy_cenx_apps_configuration do 
    config_file = "cenx_apps.rb"
    cenx_apps_config = "/cenx/sensitive/#{config_file}"
    run "cp #{cenx_apps_config} #{release_path}/config/initializers/#{config_file}"
  end

  task :copy_event_server_properties do 
    template_file = "#{deploy_to}/sensitive/event_server.properties"
    run "cp #{template_file} #{release_path}/lib/event/event_server.properties"
  end

  task :copy_shared_sphinx_folder do
    # Sphinx deploy setup
    # does something like this run "mkdir -p #{shared_path}/db/sphinx/production"
    thinking_sphinx.shared_sphinx_folder
  end
  
  task :enable_coresite_auth do 
    put "enabled = true", "#{release_path}/config/coresite_application.rb"
  end

  task :configure_coresite do 
    config_file = "coresite.yml"
    cenx_apps_config = "/cenx/sensitive/#{config_file}"
    run "cp #{cenx_apps_config} #{release_path}/config/#{config_file}"
  end

  desc "Somewhere in the build we are creating a 'public' directory in the /releases folder"
  task :hack_remove_public_dir do
    run "rm -rf #{deploy_to}/releases/public"
  end
  
  desc "HACK -- Copy Ordering Screenshots"
  task :hack_copy_ordering_screenshots do 
    run "tar zxfv /cenx/osl/co.tar.gz -C #{release_path}/public/"
  end

  desc "HACK -- Update Ordering CMD History"
  task :hack_update_ordering_cmd_history do
    run "cd /cenx/oss-db/current/ && bundle exec eve --redis local --id order.DA03SW271.64 --cmd eve reset --cmd eve load /cenx/osl/order.DA03SW271.64.eve"
    run "cd /cenx/oss-db/current/ && bundle exec eve --redis local --id order.DA03SW271.224 --cmd eve reset --cmd eve load /cenx/osl/order.DA03SW271.224.eve"
    run "cd /cenx/oss-db/current/ && bundle exec eve --redis local --id order.DA03SW307.68 --cmd eve reset --cmd eve load /cenx/osl/order.DA03SW307.68.eve"
    run "cd /cenx/oss-db/current/ && bundle exec eve --redis local --id order.DA03SW307.226 --cmd eve reset --cmd eve load /cenx/osl/order.DA03SW307.226.eve"
  end

end

namespace :local do
  task :update_version do
    if versionable
      system "rake version:deploy"
    else
      print "No updating version\n"
    end
  end
end
