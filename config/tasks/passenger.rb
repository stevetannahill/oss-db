namespace :passenger do
  
  desc "Start the Passenger standalone webserver"
  task :start do
    socket_file = "#{current_path}/log/passenger.socket"
    log_file = "#{current_path}/log/passenger.socket.log"
    pid_file = "#{current_path}/log/passenger.socket.pid"
    nginx_bin = "/home/deployer/monitor/src/nginx/sbin/nginx"
    run "cd #{current_path} && rvm use #{rvm_ruby_gemset} && passenger start -S #{socket_file} -e production -d --log-file #{log_file} --pid-file #{pid_file} --nginx-bin #{nginx_bin} --nginx-version 1.0.0 "
  end
  
  task :stop do
    pid_file = "#{current_path}/log/passenger.socket.pid"
    run "cd #{current_path} && passenger stop --pid-file #{pid_file}"
  end
  
end

namespace :deploy do
  
  desc <<-DESC
    Restarts the application on the next request.
  DESC
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
  
  desc <<-DESC
    Starts the application servers. \
    Please note that this task is not supported by Passenger server.
  DESC
  task :start, :roles => :app do
    logger.info ":start task not supported by Passenger server"
  end

  desc <<-DESC
    Stops the application servers. \
    Please note that this task is not supported by Passenger server.
  DESC
  task :stop, :roles => :app do
    logger.info ":stop task not supported by Passenger server"
  end
  
  desc <<-DESC
    Starts or Restarts the application servers. \
    Please note that this task is not supported by Passenger server.
  DESC
  task :start_or_restart, :roles => :app do
    logger.info ":start_or_restart task not supported by Passenger server"
  end

end