# MUST BE CALLED AFTER cas_name.rb
if $cas_server_enabled then
  require 'casclient'
  require 'casclient/frameworks/rails/filter'

  cas_logger = CASClient::Logger.new("#{File.dirname(__FILE__)}/../../log/casclient.log")
  #cas_logger.level = Logger::DEBUG
  CASClient::Frameworks::Rails::Filter.configure(
      # :cas_base_url => "https://localhost:8443/cas",
      :cas_base_url => $cas_server_enabled,
      :validate_url => $cas_validate_url,
      :enable_single_sign_out => true,
      :authenticate_on_every_request => false,
      :logger => cas_logger,
      :username_session_key => 'cas_user', #we want a string instead of a symbol so it's json compatible
      :cas_destination_logout_param_name => 'service'    
  )
elsif $coresite_authentication
  require 'core_site'
  CoreSite::Authentication::Filter.configure
end


