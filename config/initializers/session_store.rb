# Be sure to restart your server when you modify this file.

OssDb::Application.config.session_store :active_record_store, :key => '_sin_session'

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rails generate session_migration")
# OssDb::Application.config.session_store :active_record_store

# Some monkey patching so our session store can be read by our socket.io server
ActiveRecord::SessionStore::Session.table_name = 'sin_sessions'
class ActiveRecord::SessionStore::Session
  JSONABLE_CLASSES = [Array,Exception,FalseClass,Hash,NilClass,Numeric,Range,Regexp,String,Struct,TrueClass]
  def self.marshal(data)
    if data
      clean_data = {}
      data.each do |key,value|
        clean_key,clean_value = *[key,value].map do |subject|      
          if JSONABLE_CLASSES.any?{|c|subject.is_a?(c)}
            subject
          else
            ActiveSupport::Base64.encode64(Marshal.dump(subject))
          end
        end
        clean_data[clean_key] = clean_value
      end
      # JSON.generate(clean_data)
      clean_data.to_json
    end
  end
  
  def self.unmarshal(data)
    if data
      new_data = {}
      JSON.parse(data).each do |key,value|
        clean_key,clean_value = *[key,value].map do |subject|
          if subject.is_a?(String)
            begin
              Marshal.load(ActiveSupport::Base64.decode64(subject))
            rescue
              subject
            end
          else
            subject
          end
        end
        new_data[clean_key] = clean_value
      end
      new_data
    end
  end
end
