#path relative to lib
require 'constants/common_types'
require 'constants/common_helpers'
require 'validations'
require 'pop_seeds/common_pop'
require 'pop_seeds/common_coresite_pop'
require 'pop_seeds/standard_exchange_7750'
require 'pop_seeds/standard_exchange_7450'
require 'pop_seeds/light_squared_exchange_7450_TAP'
require 'pop_seeds/light_squared_exchange_7450_RSC'
require 'pop_seeds/light_squared_exchange_7210_SGW'
require 'pop_seeds/sprint_msc_samsung'
require 'pop_seeds/sprint_msc_samsung_mock'
require 'pop_seeds/sprint_msc_alu'
require 'pop_seeds/sprint_msc_ericsson'
require "pop_seeds/sprint_msc_ericsson_s6"
require 'pop_seeds/sprint_wdc'
require 'pop_seeds/sprint_cdc'
require 'pop_seeds/yoigo_rnc'
require 'pop_seeds/yoigo_ll_site'
require 'pop_seeds/yoigo_ml_site'
require 'pop_seeds/yoigo_poc_15'
require 'pop_seeds/yoigo_xpoc_005'
require 'pop_seeds/yoigo_hub'
require 'pop_seeds/cox_retail_agg_site'
require 'pop_seeds/cox_wholesale_agg_site'
require 'pop_seeds/vzw_cs_alu'
require 'pop_seeds/coresite_cenx_playground'
require 'pop_seeds/coresite_lalab'
require 'pop_seeds/coresite_427slasalle'
require 'pop_seeds/coresite_1275kstreet'
require 'pop_seeds/coresite_12100sunrisevalley'
require 'pop_seeds/coresite_900nalameda'
require 'pop_seeds/coresite_onewilshire'
require 'pop_seeds/coresite_32aa'
require 'pop_seeds/coresite_55southmarket'
require 'pop_seeds/coresite_1656mccarthy'
require 'pop_seeds/coresite_2972stender'
require 'tools/sw_err'
require 'tools/move_helper'
require 'sam_stats_if/sam_api'
require 'sla_exception_generator'
require 'bx_stats_if/bx_api'
require 'appstats_helpers.rb'
require 'version'
require 'app'
require 'name_tools/cenx_name_generator'
require 'name_tools/qos_policy_helper.rb'
require 'ruby-bsearch-1.5/bsearch.rb'
# Try instantiate Demarc to see if database has been migrated yet since Model Maker is attemptimg to include Models
# TODO: Not Pretty but it works
begin 
  Demarc.new 
rescue 
  puts 'No tables yet, so ModelMaker not loaded. !!!!!!!!!!!!!!!!!!!!!!!!!'
else 
  require 'model_maker/requires.rb' 
end
require 'ostruct'
require 'cenx_id_generator'
require 'modules/common_exception'
require 'modules/exception_reports'

# used by db migration code to signal that diagrams have to be regenerated at the end of the migrations
require 'modules/migrate_extension'

#Overwrite OpenStruct's default id handling to allow setting the id
OpenStruct.__send__(:define_method, :id) { @table[:id] }

