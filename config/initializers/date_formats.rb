Time::DATE_FORMATS[:friendly] = "%B %e, %Y"
Time::DATE_FORMATS[:ymd_time] = "%Y-%m-%d %H:%M:%S %Z"
Time::DATE_FORMATS[:ymd] = "%Y-%m-%d %Z"