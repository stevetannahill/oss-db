# IMPORTANT if you change this be sure to change the one in monitor_configs which is used by Gandalf
# For the Live system this file is overwritten so it sends an email
# Also this is invoked from both a rails app AND a non Rails app (sam_jms_server) so test both cases
#
# Setup the action mailer - by default don't send any mail
ActionMailer::Base.view_paths = ActionView::Base.process_view_paths("#{File.dirname(__FILE__)}" + "/../../app/views/")
ActionMailer::Base.delivery_method = :smtp

# Jruby fails to authenticate with :ntlm even though all gems etc are installed. It does tries the NTLM challenage/response
# but the server rejects the authenication for some reason. Running the same code with normal Ruby works fine.
# So for now the JMS server will use barracuda as external emails from that process are not needed.
if RUBY_PLATFORM == "java"
  ActionMailer::Base.smtp_settings = {
    :domain             => "cenx.com",
    :address            => "barracuda.cenx.com",
    :port               => 25,
    :enable_starttls_auto => false,
    :openssl_verify_mode => 'none' #Shouldn't be necessary, might make things a bit unsecure
  }
else
  ActionMailer::Base.smtp_settings = {
    :domain             => "cenx.com",
    :address            => "mail2.cenx.com",
    :port               => 25,
    :user_name          => "automailer",
    :password           => "tHR4NGvmwvnn6m",
    :authentication     => :ntlm,
    :enable_starttls_auto => true,
    :openssl_verify_mode => 'none' #Shouldn't be necessary, might make things a bit unsecure
  }
end

ActionMailer::Base.perform_deliveries = false
ActionMailer::Base.raise_delivery_errors = true

# The following can be enabled for debug
# override_event_email_address will override event email addresses with the one provided
# override_order_email_address will override order email addresses with the one provided
# debug_bcc_event_email_addresses will bcc the given addresses for each event ticket. This is useful to
# listen on email alerts on a live system
# The various cenx email address can also be overridden eg cenx_ops, cenx_billing etc
#
# A restart of the bx_snmp_server, sam_jms_server and the cdb app will be needed for the changes to take effect

# Check to see if CdbMailer class exists first as this file can be deployed (via monitor_configs) to a system which has not
# had oss-db upgraded yet to include CdbMailer
begin
  require 'cdb_mailer' if Rails.env.development? # prevent the class from autoloading in development mode so resetting the class variables on every controller request
  
  klass = Module.const_get("CdbMailer")
  if klass.is_a?(Class)  
    #CdbMailer.override_order_email_address = ["bruce.wessels@cenx.com"]
    #CdbMailer.override_event_email_address = ["bruce.wessels@cenx.com"]
    #CdbMailer.debug_bcc_event_email_addresses = ["bruce.wessels@cenx.com"]
    #CdbMailer.debug_bcc_order_email_addresses = ["bruce.wessels@cenx.com"]
  end
rescue NameError
end

