CENX_APPS = {
  cdb:  "http://cdb-cenx-ea2.nmcc.sprintspectrum.com/",
  sin: "http://cenx-ea2.nmcc.sprintspectrum.com/",
  
  # ASSUMING THIS IS OVERWRITTEN, SO DEFAULTED TO LOCAL SETTINGS
  osl: "http://localhost:3010/",
}

CENX_PUSH_SERVER = 'http://cenx-ea2.nmcc.sprintspectrum.com/'