if defined?(LKP)
  File.open(File.expand_path("../../../config/lkp.json",__FILE__)) do |f|
    config = JSON.load(f)
    LKP.redis_options = config["redis_options"].symbolize_keys
    LKP::Prism.amqp_options = config["amqp_options"]
  end
  
  # Test if we can connect to the LKP::Prism.amqp server first, then we can check later to see if it connected and skip the communication
  class LKPPrismConnection
    cattr_accessor :available
  end
  LKPPrismConnection.available = false
  
  require 'bunny'
  
  begin
    Bunny.run(LKP::Prism.amqp_options) do |client|
    end
    LKPPrismConnection.available = true
  rescue
    Rails.logger.error(">>>>   Could not connect to Prism.amqp server, avoiding communication now... <<<<")
  end
end
