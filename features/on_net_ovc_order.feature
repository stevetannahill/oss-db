Feature: CENX OVC orders
  In order to allow for something I (Dan) don't yet understand
  Customers should be able to
  order CENX OVC's
  
  Scenario: Setting CENX OVC order cenx_id's
    Given that 2 new CENX OVC orders have been created
    When I set the 1st CENX OVC orders cenx_id to '10071234567'
    Then the 1st CENX OVC order should save
    When I set the 1st CENX OVC orders cenx_id to '100'
    Then the 1st CENX OVC order should not save
    Given that 2 new CENX OVC orders have been created
    When I set the 1st CENX OVC orders cenx_id to itself
    Then the 1st CENX OVC order should save
    When I set the 1st CENX OVC orders cenx_id to duplicate
    Then the 1st CENX OVC order should not save
  
  Scenario: Generating many orders
    Given that 1000 new CENX OVC orders have been created
    Then each CENX OVC order should have a valid cenx id
    Then each CENX OVC order should have a unique cenx id
    
    