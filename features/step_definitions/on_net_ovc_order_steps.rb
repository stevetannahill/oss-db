Given /^that (\d*) new On Net OVC orders have been created$/ do |num|
  @onovcos = Array.new(num.to_i) { Factory.create(:on_net_ovc_order) }
end

When /^I set the (\d*).* On Net OVC orders cenx_id to (.*)$/ do |index, id|
  case id
    when /^'\d*'$/
      @onovcos[index.to_i - 1].cenx_id = id[1..-2]
    when "itself"
      @onovcos[index.to_i - 1].cenx_id = @onovcos[index.to_i - 1].cenx_id
    when "duplicate"
      @onovcos[index.to_i - 1].cenx_id = @onovcos[index.to_i].cenx_id
  end
end

Then /^the (\d).* On Net OVC order should (not )?save$/ do |index, notsave|
  @onovcos[index.to_i - 1].save
  if(notsave)
    @onovcos[index.to_i - 1].valid?.should == false
  else
    @onovcos[index.to_i - 1].valid?.should == true
  end
end

Then /^each On Net OVC order should have a (.*) cenx id$/ do |check|
  case check
    when "valid"
      @onovcos.each { |onovco| IdTools::CenxIdGenerator.decode_cenx_id(onovco.cenx_id)[0].should == true }
    when "unique"
      @onovcos.each { |onovco| OnNetOvcOrder.find(:all, :conditions => ["cenx_id = ?", onovco.cenx_id]).length.should == 1 }
  end
end