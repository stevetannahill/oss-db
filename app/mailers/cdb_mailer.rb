if RUBY_PLATFORM != "java"
  # Jruby does not support ntlm authenication
  require 'ntlm/smtp'
end

require 'uri'

# NOTE: each email method must call EmailConfig.instance even if it's not used. This method
# gets the current email config but also set the ActionMailer perform_deliveries. The reason this must be called
# is that emails can be sent from different applications/daemons so setting the perform_delivery in one app won't update the other apps
# so they must reload the EmailConfig to set the perform_deliveries.
# It would be nice if ActionMailer had a before_filter but it does not..
class CdbMailer < ActionMailer::Base
  helper :application
  helper :filter_templates
  helper :dashboard
  helper :paths
  
  #
  # These are obsolete and are kept here for upgrade from this object to the EmailConfig object
  # Once all servers are running the new code this can be removed. Also CHEF (monitor_configs) 
  # can be updated to reove this specific config
  #
  cattr_accessor :debug_bcc_event_email_addresses
  cattr_accessor :debug_bcc_order_email_addresses
  cattr_accessor :override_event_email_address # Override event To and Cc 
  cattr_accessor :override_order_email_address # Override order To and Cc, Bcc 
  cattr_accessor :cenx_ops
  cattr_accessor :cenx_ops_request
  cattr_accessor :cenx_eng
  cattr_accessor :cenx_cust_ops
  cattr_accessor :cenx_prov
  cattr_accessor :cenx_billing 
  
  @@override_event_email_address = []
  @@override_order_email_address = []
  @@debug_bcc_event_email_addresses = []
  @@debug_bcc_order_email_addresses = []
  @@cenx_ops_request = ["ops-request@support.cenx.com"]
  @@cenx_ops = ["support@cenx.com"]
  @@cenx_eng = ["eng@support.cenx.com"]
  @@cenx_cust_ops = ["customerops@support.cenx.com"]
  @@cenx_prov = ["Provisioning@CENX.com"]
  @@cenx_billing = [""] #["cenx_billing@cenx.com"]
  #
  # End of obsolete code
  #
  
  @@server = Socket.gethostname.split(".").first
  @@server = (@@server == "gandalf") ? "Live" : @@server

  FROM_ADDRESS = "automailer@cenx.com"
  
  default :from => FROM_ADDRESS,
          :reply_to => "do-not-reply@cenx.com"
  
  def self.base_url(app=:cdb)
    url = URI.parse(CENX_APPS[app])
    if url.port != 80
      return "#{url.scheme}://#{url.host}:#{url.port}/"
    else
      return "#{url.scheme}://#{url.host}/"
    end
  end

  def self.sin_url
    self.base_url(:sin)
  end

  def self.cdb_url
    self.base_url(:cdb)
  end

  def self.sin_host
    if URI.parse(self.sin_url).port != 80
      return "#{URI.parse(self.sin_url).host}:#{URI.parse(self.sin_url).port}"
    else
      return URI.parse(self.sin_url).host
    end
  end

  def self.cdb_host
    if URI.parse(self.cdb_url).port != 80
      return "#{URI.parse(self.cdb_url).host}:#{URI.parse(self.cdb_url).port}"
    else
      return URI.parse(self.cdb_url).host
    end
  end

  # Fault emails
  def path_fault_ticket(obj)
    ec = EmailConfig.instance
    @obj = obj
    to, copy, blind_copy = ec.get_event_recepients(obj)
    mail( :to => to,
          :cc => copy,
          :bcc => blind_copy,
          :subject => "#{@@server}: #{obj.cenx_name} has indicated a fault")
  end
   
  def enni_new_fault_ticket(obj)
    ec = EmailConfig.instance
    @obj = obj
    @affected_paths = obj.paths
    
    to, copy, blind_copy = ec.get_event_recepients(obj)
    mail( :to => to,
          :cc => copy,
          :bcc => blind_copy,
          :subject => "#{@@server}: #{obj.cenx_name} has indicated a fault" )
  end
  # Ordering emails - currently only for Segments
  def order_ticket(obj)
    ec = EmailConfig.instance
    @so = obj
    @ordered_entity_type = obj.ordered_entity_subtype.to_s.underscore.humanize.upcase
    to, copy, blind_copy = ec.get_order_recipients(obj.get_order_state)
    mail( :to => to,
          :cc => copy,
          :bcc => blind_copy,
          :subject => "#{@@server}: #{obj.title} is now #{obj.get_order_name}")
  end
  
  def order_design_complete_for_customer(obj)
    ec = EmailConfig.instance
    @so = obj
    @ordered_entity_type = obj.ordered_entity_subtype.to_s.underscore.humanize.upcase
    customer_address = obj.primary_contact.e_mail 
    
    # For now don't send the email directly to the customer but to cenx_prov instead
    mail( :to => ec.order_recipient(ec.cenx_prov), #order_recipient("do_not_send" + customer_address)
          #:cc => order_recipient(cenx_prov),
          # If the customer replies then it goes to the cenx customer ops queue
          :reply_to => ec.cenx_cust_ops,
          :subject => "#{@@server}: #{obj.title} is now Design Complete")
  end
  
  
  def test_email(email_address)
    ec = EmailConfig.instance
    mail( :to => email_address,
          :subject => "#{@@server}: Test Email from platform #{RUBY_PLATFORM}")
  end

  def scheduled_report(report, exceptions, exception_finder, exception_stats=nil)
    ec = EmailConfig.instance
    @host = CdbMailer.sin_host
    @report = report
    @sched_report = report.schedule
    @body_text = @sched_report.email_body

    @current_user = @sched_report.scheduled_task.user
    @exceptions = exceptions
    @exception_stats = exception_stats
    @params = @sched_report.scheduled_task.filter_options

    @date_range_start = @params['date_range_start'] 
    @date_range_end= @params['date_range_end'] 

    @sched_report.formats.each do |k,v|
      if k == 'csv' && v == 'true'
        attachments["report.csv"] = {:content => render_to_string(:template => "/reports/#{exception_finder}.csv", :locals => {:exceptions => @exceptions, :user => @current_user})}
      elsif k == 'pdf' && v == 'true'
        if @exception_stats
          @total = @exception_stats.count
          @minor = @exception_stats.s_minor.count
          @major = @exception_stats.s_major.count
          @clear = @exception_stats.s_clear.count
        end

        attachments["report.pdf"] = {:content => WickedPdf.new.pdf_from_string(
          render_to_string(:pdf => 'report', :template => "/reports/#{exception_finder}.pdf", :layout => 'pdf.html', :disposition => 'attachment', :header => {:right => '[page] of [topage]', :params => @params})
        )}
      end
      self.instance_variable_set(:@_lookup_context, nil)
    end  

    mail(:to => @sched_report.email_to, :cc => @sched_report.email_cc, :bcc => @sched_report.email_bcc,
         :subject => @sched_report.email_subject + " #{report_subject_postpend(@report)}")         
  end
  
  def scheduled_audit(report, report_body, attach)
    ec = EmailConfig.instance
    @host = CdbMailer.sin_host
    @report = report
    @sched_report = report.schedule
    @body_text = @sched_report.email_body + report_body
    attach.each {|name, content| attachments[name] = content}

    mail(:to => @sched_report.email_to, :cc => @sched_report.email_cc, :bcc => @sched_report.email_bcc,
         :subject => "#{@@server}: #{@sched_report.email_subject} #{report_subject_postpend(@report)}")         
  end
   
  def password_reset(user)
    ec = EmailConfig.instance
    @user = user
    @host = CdbMailer.sin_host
    mail :to => user.email, :subject => "Password Reset"
  end


  private
  # Needed to render the views to get class names
  def self.parent_type_from_table(table)
    CDBInfo::CLASS_SHORT_NAMES[table.is_a?(ActiveRecord::Base) ? table.class_name : table.to_s]
  end
  
  def report_subject_postpend(rep)
    "Generated: #{rep.run_date.strftime("%Y%m%d")}"
  end
   
end
