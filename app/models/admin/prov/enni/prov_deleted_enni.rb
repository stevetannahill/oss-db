# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class ProvDeletedEnni < ProvDeletedDemarc
  def pre_conditions(stateful, previous_state, failed_objects = [])
    result = super
    if result
      # Check that all endpoints are in a deleted state
      demarc = stateful.stateful_ownership.stateable
      if !demarc.segment_end_points.all? {|ep| ep.get_prov_state.is_a?(ProvDeleted) if ep.prov_state != nil}
        # Add all endpoints that are not deleted to the list of failed_objects if any are not deleted
        demarc.segment_end_points.each {|ep| failed_objects << {:obj => ep, :reason => "Endpoint is not Deleted state"} if ep.prov_state != nil && !ep.get_prov_state.is_a?(ProvDeleted)}
        result = false
      end
    end
    return result
  end
end
