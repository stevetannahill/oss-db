# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class ProvMaintenancePath < ProvMaintenance
  include PathStateMixin
  
  def post_conditions(stateful, previous_state, failed_objects = [])    
    result = super
    target_obj = stateful.stateful_ownership.stateable
    latest_order = target_obj.get_latest_order
    
    # Set the Order to either OrderAccepted or Tested depending on the current order
    if latest_order != nil && target_obj.active_order?
      if latest_order.get_order_state.is_a?(OrderCreated)
        case latest_order.action
          when "Disconnect"
            new_order_state = OrderTested.new
          else
            new_order_state = OrderAccepted.new
        end
        pre_result, post_result, pre_failed_objects, post_failed_objects = latest_order.order_state.drive_state(new_order_state)
        if !pre_result
          result = false
          failed_objects << pre_failed_objects
        end
        if !post_result
          result = false
          failed_objects << post_failed_objects
        end
      end
    end
    target_obj.set_mtc(true)

    return result
  end
end
