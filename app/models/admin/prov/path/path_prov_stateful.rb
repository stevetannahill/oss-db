# == Schema Information
#
# Table name: statefuls
#
#  id             :integer(4)      not null, primary key
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  name           :string(255)
#

class PathProvStateful < ProvStateful  
  private 
  def mappings
    return {ProvPending => ProvPendingPath, 
            ProvTesting => ProvTestingPath, 
            ProvReady => ProvReadyPath, 
            ProvLive => ProvLivePath, 
            ProvMaintenance => ProvMaintenancePath, 
            ProvDeleted => ProvDeletedPath}
  end
  
  public
  def set_state(new_state, top_level_call = true)    
    # The Path state is determinted by the "lowest" state of it's Segments (for Pending, Testing, Ready, Live or Deleted) 
    # If any Segment is in Maintenance then the Path is in Maintenance - ignore the passed state and find what state it should be
    target_obj = stateful_ownership.stateable    
    segment_states = target_obj.segments.collect {|segment| segment.prov_name}.uniq.compact
    segment_states = segment_states.collect {|state| target_obj.prov_state.to_generic(target_obj.prov_state.to_specific(state))}.uniq.compact      
        
    if segment_states.any?{|state| state == ProvMaintenance}
      new_state = ProvMaintenance
    elsif !segment_states.empty?
      # There are Segment's so find the lowest one
      target_obj.set_mtc(false) # Hack - if Path is in READY it can go to Mtc and then back to Ready so Mtc alarm history is not updated....
      new_state = state_hierarchy[segment_states.collect {|segment_state| state_hierarchy.index(segment_state)}.compact.min]
    else
      # There are no Segments just stay in the same state
      return true, true, [], []
    end
    return super(new_state, top_level_call)    
  end 
end
