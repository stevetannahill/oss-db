# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class ProvLivePath < ProvLive
  include PathStateMixin
    
  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    target_obj = stateful.stateful_ownership.stateable
    target_obj.set_mtc(false)
    
    # if we are comming from Maintenance then don't update any orders
    if !previous_state.is_a?(ProvMaintenance)
      # set order to Delivered if an order is active
      latest_order = target_obj.get_latest_order
      if latest_order != nil && target_obj.active_order?
        # If the Order is created don't do anything - it means a new order has been added.
        if !latest_order.get_order_state.is_a?(OrderCreated)
          if !update_state_and_failed_objs(latest_order.order_state, OrderDelivered, failed_objects, true)
            result = false
          end
        end
      end
    end
    
    return result
  end
end
