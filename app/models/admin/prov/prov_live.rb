# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class ProvLive < ProvState
  @@state_name = ProvStateful::LIVE
  def self.state_name
    return @@state_name
  end
   
  def valid_transitions
    return [ProvMaintenance, ProvLive]
  end

end
