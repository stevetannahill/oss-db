# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class ProvReadyDemarc < ProvReady
  
  def valid_transitions
    transitions = super       
    # If an order exists and it's Not CustomerAccepted or Delivered or Created then cannot move to Live
    target_obj = stateful.stateful_ownership.stateable
    latest_order = target_obj.get_latest_order
    if latest_order != nil && !(latest_order.get_order_state.is_a?(OrderCustomerAccepted) || latest_order.get_order_state.is_a?(OrderDelivered) || latest_order.get_order_state.is_a?(OrderCreated))
      transitions.delete(ProvLive)
    end
    
    return transitions
  end
  
  def pre_conditions(stateful, previous_state, failed_objects = [])
    result = super
    target_obj = stateful.stateful_ownership.stateable
    latest_order = target_obj.get_latest_order

    if latest_order != nil
      # If the previous state was Maintenance then drive the state to the Provisioned state
      if previous_state.is_a?(ProvMaintenance)
        pre_result, post_result, pre_failed_objects, post_failed_objects = latest_order.order_state.drive_state(OrderProvisioned.new)
        if !(pre_result && post_result)
           failed_objects << pre_failed_objects << post_failed_objects
           result = false
        end
      end
    end
    return result
  end

  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    target_obj = stateful.stateful_ownership.stateable
    # set order to Tested if an order exists
    latest_order = target_obj.get_latest_order
    if latest_order != nil
      if !update_state_and_failed_objs(latest_order.order_state, OrderTested, failed_objects, true)
        result = false
      end
    end
    
    return result
  end
  
end
