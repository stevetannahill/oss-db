# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class ProvTestingSegment < ProvTesting  
  include SegmentStateMixin
  
  def pre_conditions(stateful, previous_state, failed_objects = [])
    # Check order pre-conditions of order if an order exists
    result = super
    # If an order exists and it's Not DesignComplete or Provisioned then fail
    segment = stateful.stateful_ownership.stateable
    latest_order = segment.get_latest_order
    if latest_order != nil && ! (latest_order.get_order_state.is_a?(OrderDesignComplete) || latest_order.get_order_state.is_a?(OrderProvisioned))
      failed_objects << {:obj => latest_order, :reason => "Order is in #{latest_order.get_order_name} state not in #{OrderDesignComplete.state_name} or #{OrderProvisioned.state_name} state"}
      result = false
    end
    
    if result
      segment = stateful.stateful_ownership.stateable
      go_insv_result = stateful.wrap_go_in_service
      result = go_insv_result[:result]
      failed_objects << {:obj => segment, :reason => go_insv_result[:info]}
    end

    return result
  end
  
  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    segment = stateful.stateful_ownership.stateable
    
    # set order to Provisioned if an order exists
    latest_order = segment.get_latest_order
    if latest_order != nil
      if !update_state_and_failed_objs(latest_order.order_state, OrderProvisioned, failed_objects, true)
        result = false
      end
    end
    
    return result
  end
  
  def self.pre_text(stateful)
    text = "Ensure all components are configured in CDB and Network"
    return text
  end
  
  def self.post_text(stateful)
    s =  "Test the service per CENX Turnup Test Procedure\n"
    s += "Store Test Report and Birth Cirtificate in Alfresco and add links to Order Notes\n"
    entity = stateful.stateful_ownership.stateable.class.to_s.underscore.humanize.upcase
    entity.gsub!(/ NEW/, "")
    s += "Move #{entity} to 'Ready' State in CDB"
    return s
  end
  
end
