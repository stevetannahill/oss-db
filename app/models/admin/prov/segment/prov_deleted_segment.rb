# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class ProvDeletedSegment < ProvDeleted
  include SegmentStateMixin
  
  def pre_conditions(stateful, previous_state, failed_objects = [])
    result = true
    segment = stateful.stateful_ownership.stateable
    latest_order = segment.get_latest_order
    if latest_order != nil
      if !(latest_order.get_order_state.is_a?(OrderTested) && latest_order.action == "Disconnect")
        failed_objects << {:obj => segment, :reason => "No active Disconnect Service order"}
        result = false
      end
    end
    
    if result
      result = super
    end
    
    return result
  end
  
  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    segment = stateful.stateful_ownership.stateable
    
    # set order to Delivered if an order exists
    latest_order = segment.get_latest_order
    if latest_order != nil
      pre_result, post_result, pre_failed_objects, post_failed_objects = latest_order.order_state.drive_state(OrderDelivered.new)
      if !pre_result
        result = false
        failed_objects << pre_failed_objects
      end
      if !post_result
        result = false
        failed_objects << post_failed_objects
      end
    end
    
    return result
  end
  
  def self.post_text(stateful)
    return "Delete service from the network"
  end

end
