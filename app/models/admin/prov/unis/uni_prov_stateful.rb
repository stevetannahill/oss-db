# == Schema Information
#
# Table name: statefuls
#
#  id             :integer(4)      not null, primary key
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  name           :string(255)
#

class UniProvStateful < DemarcProvStateful      
  private
  def mappings
    return {ProvPending => ProvPendingDemarc, 
            ProvTesting => ProvTestingUni, 
            ProvReady => ProvReadyDemarc, 
            ProvLive => ProvLiveDemarc, 
            ProvMaintenance => ProvMaintenanceDemarc, 
            ProvDeleted => ProvDeletedDemarc}
  end
end
