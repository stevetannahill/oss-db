# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class ProvMaintenanceNode < ProvMaintenance
  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    
    node = stateful.stateful_ownership.stateable
    node.set_mtc(true)
    
    # Now set all Demarcs to Mtc
    demarcs = node.ports.collect {|port| port.demarc if port.demarc != nil && port.demarc.prov_state != nil && port.demarc.get_prov_state.is_a?(ProvLive)}.compact
    
    demarcs.each {|demarc| 
      pre_result, post_result, pre_failed_objs = demarc.set_prov_state(self, false)      
      failed_objects << pre_failed_objs
      result = false if !pre_result    
    }
    return result
  end
  
  def self.pre_text(stateful)
    text = ""
    node = stateful.stateful_ownership.stateable
    # Warn about the number of Ports this will affect
    text = "Warning: This will affect #{node.ports.size} ports" if node.ports.size != 0
    return text
  end
  
end
