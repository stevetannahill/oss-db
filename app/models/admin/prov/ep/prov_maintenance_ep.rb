# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class ProvMaintenanceEp < ProvMaintenance
  def valid_transitions    
    transitions = super
    # If there is an active order then the following transitions are valid. Note no need to check
    # for the order here as the user does not directly set state but is driven from the Segment which checks the order
    # state
    transitions += [ProvPending, ProvTesting, ProvReady]
    return transitions
  end
end
