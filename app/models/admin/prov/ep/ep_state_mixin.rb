module EpStateMixin
  
  def pre_conditions(stateful, previous_state, failed_objects = [])
    result = super
    ep = stateful.stateful_ownership.stateable
    demarc = stateful.stateful_ownership.stateable.demarc
    # If there is no demarc and the state is pending then it's ok as the endpoint has been
    # created but no demarc is linked. If the state is not pending then the endpoint must have a
    # demarc linked 
    if demarc == nil && !self.is_a?(ProvPending)
      # There is no Demarc and the endpoint is not Pending then fail      
      result = false
      failed_objects << {:obj => ep, :reason => "Endpoint has no Demarc linked"}
    else      
      if demarc != nil
        if demarc.prov_state != nil
          # Check the Demarcs's are in a higher prov state (for all states except Maintainance & Deleted)
          demarc_state = stateful.state_hierarchy.index(demarc.prov_state.to_generic(demarc.get_prov_state))
          ep_state = stateful.state_hierarchy.index(stateful.to_generic(self))
          result = result && (demarc_state >= ep_state)    
    
          # Add the demarc to the list of failed_objects if demarc was in the wrong state
          failed_objects << {:obj => demarc, :reason => "Demarc is in the wrong state"} if !(demarc_state >= ep_state)
        else
          SW_ERR "Demarc #{demarc.class}:#{demarc.id} has no prov state"
        end
      end
    end
        
    return result
  end
  
  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    return result
  end

end