# == Schema Information
#
# Table name: statefuls
#
#  id             :integer(4)      not null, primary key
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  name           :string(255)
#

class EpProvStateful < ProvStateful  
  private 
  def mappings
    return {ProvPending => ProvPendingEp,
            ProvTesting => ProvTestingEp,
            ProvReady => ProvReadyEp, 
            ProvLive => ProvLiveEp, 
            ProvMaintenance => ProvMaintenanceEp, 
            ProvDeleted => ProvDeletedEp} 
  end
  
  public
  # helper method to move a state from one state through to another state
  def drive_state(new_state, top_level = true)
    pre_failed_objects = []
    post_failed_objects = []
    pre_result = true
    post_result = true
  
    # find set of states to go through 
    start_state = state_hierarchy.find_index(to_generic(self.get_state))
    end_state = state_hierarchy.find_index(to_generic(to_specific(new_state)))
  
    # If we are driving down eg Maintenance to Testing we must go Mtc -> Pending -> Testing (skipping the "Deleted" state in the hierarchy)
    # If we go from Mtc to Live just go to Live!!!don't go Pending -> Testing -> Ready -> Live
    states_to_traverse = []
    if self.get_state.is_a?(ProvMaintenance) && new_state.is_a?(ProvLive)
      states_to_traverse = [ProvLive]
    elsif start_state > end_state
      # Don't include the last ProvDeleted
      states_to_traverse = state_hierarchy[start_state+1..-2]
      states_to_traverse += state_hierarchy[0..end_state]
    else  
      start_state += 1 if start_state < end_state # if moving to a different state skip the current state
      states_to_traverse = state_hierarchy[start_state..end_state]
    end
  
    states_to_traverse.each do |state|
      pre_result, post_result, pre_failed_objects, post_failed_objects = set_state(state, top_level)
      # If anything failed then stop
      if !pre_result || !post_result
        SW_ERR "#{Time.now} Failed Trying to move #{self.stateful_ownership.stateable.class} #{self.stateful_ownership.stateable.id} to #{state}"
        break
      end
    end
  
    return pre_result, post_result, pre_failed_objects.flatten, post_failed_objects.flatten
  end
  
end
