# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class ProvMaintenance < ProvState
  @@state_name = ProvStateful::MAINTENANCE
  def self.state_name
    return @@state_name
  end
  
  def valid_transitions    
    return [ProvLive, ProvDeleted, ProvMaintenance]
  end
end
