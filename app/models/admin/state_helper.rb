# Methods to automatcally move circuits/nodes etc to a desired state(currently just Live)
module StateHelper

  def self.change_prov_state(object, target_state, log_details = "")  
    object.reload
    success = false
    current_state = object.prov_state.to_generic(object.get_prov_state)
    states = object.prov_state.state_hierarchy - [ProvMaintenance, ProvDeleted]  
    start_idx = states.index(current_state)
    target_idx = states.index(target_state)

    if start_idx != nil && target_idx != nil
      success = true
      if start_idx < target_idx    
        # The object needs to be moved
        log_details << "Changing state of #{object.class}:#{object.id} to "
        states[start_idx+1..target_idx].each do |state|
          pre, post, pre_reason, post_reason = object.set_prov_state(state)
          success = process_state_results(object, state, pre, post, pre_reason, post_reason , log_details)
          break unless success
        end
        log_details << "\n"
      end
    else
      log_details << "Failed to find state #{current_state} or #{target_state} in #{states}\n"
    end
    return success
  end

  def self.change_order_state(object, target_state, log_details = "")
    object.reload
    success = false
    order = object.get_latest_order
    current_state = order.order_state.to_generic(order.get_order_state)
    states = order.order_state.state_hierarchy
    start_idx = states.index(current_state)
    target_idx = states.index(target_state)

    if start_idx != nil && target_idx != nil
      success = true
      if start_idx < target_idx    
        # The object needs to be moved
        log_details << "Changing state of #{object.class}:#{object.id} to "
        states[start_idx+1..target_idx].each do |state|
          pre, post, pre_reason, post_reason = order.set_order_state(state)
          success = process_state_results(object, state, pre, post, pre_reason, post_reason , log_details)
          break unless success
        end
        log_details << "\n"
      end
    else
      log_details <<  "Failed to find state #{current_state} or #{target_state} in #{states}\n"
    end
    return success
  end

  def self.bring_object_with_order_live(object, log_details = "")  
    success = false
    if object.get_latest_order != nil    
      if change_order_state(object, OrderDesignComplete, log_details)
        if change_prov_state(object, ProvReady, log_details) 
          if change_order_state(object, OrderCustomerAccepted, log_details)       
            success = change_prov_state(object, ProvLive, log_details) 
          end
        end
      end   
    else
      success = change_prov_state(object, ProvLive, log_details)
    end
    return success
  end
  
  
  def self.bring_object_with_order_to_state(object, state, log_details = "")  
    success = false
    if object.get_latest_order != nil    
      order = {
       ProvPending => {},
       ProvTesting => {:order => OrderDesignComplete, :prov => ProvTesting},
       ProvReady => {:prov => ProvReady},
       ProvLive => {:order => OrderCustomerAccepted, :prov => ProvLive}
      }
      success = true
      catch(:finished) do
        order.each do |the_state, actions|
          actions.each do |type, target_state|
            success = case type
             when :order then change_order_state(object, target_state, log_details)
             when :prov then change_prov_state(object, target_state, log_details)
            end
            throw(:finished) if !success
          end
          throw(:finished) if the_state == state
        end 
      end
    else
      success = change_prov_state(object, state, log_details)
    end
    return success
  end


  def self.evc_live(evc, log_details = "")
    success = false
    nodes = evc.ennis.collect {|e| e.site.nodes}.flatten.uniq
    log_details <<  "Bringing Node Insv\n"
    if nodes.all? {|obj| change_prov_state(obj, ProvLive, log_details)}    
      log_details <<  "Bringing ENNIs in service\n"
      if evc.ennis.all? {|obj| bring_object_with_order_live(obj, log_details)}
        log_details <<  "Bringing UNI in service\n"
        if evc.demarcs.all? {|obj| bring_object_with_order_live(obj, log_details)}
          log_details <<  "Bringing Segments in service\n"
          success = evc.segments.all? {|obj| bring_object_with_order_live(obj, log_details)}
        end
      end
    end
    return success
  end
  
  def self.enni_live(enni, log_details = "")
    success = false
    nodes = enni.site.nodes.uniq
    log_details <<  "Bringing Node Insv\n"
    if nodes.all? {|obj| change_prov_state(obj, ProvLive, log_details)}    
      log_details <<  "Bringing ENNI in service\n"
      success =  bring_object_with_order_live(enni, log_details)
    end
    return success
  end
  
  def self.path_to_state(path, state, log_details = "")
    success = false
    nodes = path.ennis.collect {|e| e.site.nodes}.flatten.uniq
    log_details <<  "Bringing Node to #{state.name}\n"
    if nodes.all? {|obj| change_prov_state(obj, state, log_details)}    
      log_details <<  "Bringing ENNIs to #{state.name}\n"
      if path.ennis.all? {|obj| bring_object_with_order_to_state(obj, state, log_details)}
        log_details <<  "Bringing UNI to #{state.name}\n"
        if path.demarcs.all? {|obj| bring_object_with_order_to_state(obj, state, log_details)}
          log_details <<  "Bringing Segments to #{state.name}\n"
          success = path.segments.all? {|obj| bring_object_with_order_to_state(obj, state, log_details)}
        end
      end
    end
    return success
  end
    
  def self.format_state_reason(reasons)
    reasons.collect {|k| "#{k[:obj].class}:#{k[:obj].id}\n#{k[:reason]}" unless k[:reason].blank?}.compact
  end
  
  def self.process_state_results(object, state, pre, post, pre_reason, post_reason, log_details = "")
    success = true
    if !pre
      log_details << "\nFailed to change state of #{object.class}:#{object.id} to #{state} pre reason #{format_state_reason(pre_reason).join("\n")}\n"
      success = false
    elsif !post
      log_details << "\nFailed to change state of #{object.class}:#{object.id} to #{state} post reason #{format_state_reason(post_reason).join("\n")}"
      success = false
    else
      info = format_state_reason(pre_reason + post_reason).join("\n")
      log_details << "#{state}" 
      log_details << "\nInformation:\n" + info + "\n" if !info.blank?
      log_details << ".."
    end
    return success
  end
  
end  