# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class OrderTested < OrderState 
  @@state_name = OrderStateful::TESTED
  def self.state_name
    return @@state_name
  end
  
  def valid_transitions
    return [OrderTested, OrderCustomerAccepted, OrderRejected, OrderCancelled]
  end
  
  def self.post_text(stateful)
    s =  "Notify customer with acceptance test report and brith certificate\n"
    s += "Wait 5 days for Customer Acceptance\n"
    s += "Once accepted by the customer,\n"
    s += " Fill in the billing Start date in CDB\n"
    s += " Move the Order to '#{OrderCustomerAccepted.state_name}' State in CDB"
    return s
  end
  
  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    result = result && set_order_date(stateful, "order_completion_date", Time.now, failed_objects)
    return result
  end
  
end
