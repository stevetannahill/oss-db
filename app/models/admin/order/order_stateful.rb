# == Schema Information
#
# Table name: statefuls
#
#  id             :integer(4)      not null, primary key
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  name           :string(255)
#

class OrderStateful < Stateful  
  before_create :default_values
  
  CREATED = "Created"
  READY_TO_ORDER = "Ready to Order"
  RECEIVED = "Received"
  ACCEPTED = "Order Accepted"
  DESIGN_COMPLETE = "Design Complete"
  PROVISIONED = "Provisioned"
  TESTED = "Tested"
  CUSTOMER_ACCEPTED = "Customer Accepted"
  DELIVERED = "Delivered" 
  REJECTED = "Rejected"
  CANCELLED = "Cancelled"
  
  private
  
  def default_values
    self.name ||= "Ordering"
  end  
  
  def mappings
    return {OrderCreated => OrderCreated,
            OrderReadyToOrder => OrderReadyToOrder,
            OrderReceived => OrderReceived,
            OrderAccepted => OrderAccepted,
            OrderDesignComplete => OrderDesignComplete,
            OrderProvisioned => OrderProvisioned,
            OrderTested => OrderTested,
            OrderCustomerAccepted => OrderCustomerAccepted,
            OrderDelivered => OrderDelivered,
            OrderRejected => OrderRejected,
            OrderCancelled => OrderCancelled}
  end
  
  def name_mappings
    return {CREATED => OrderCreated,
            READY_TO_ORDER => OrderReadyToOrder,
            RECEIVED => OrderReceived,
            ACCEPTED => OrderAccepted,
            DESIGN_COMPLETE => OrderDesignComplete,
            PROVISIONED => OrderProvisioned,
            TESTED => OrderTested,
            CUSTOMER_ACCEPTED => OrderCustomerAccepted,
            DELIVERED => OrderDelivered,
            REJECTED => OrderRejected,
            CANCELLED => OrderCancelled}
  end
  
  public

  def initial_state
    return OrderCreated
  end
  
  def state_hierarchy
    return [OrderCreated, OrderReadyToOrder, OrderReceived, OrderAccepted, OrderDesignComplete, OrderProvisioned, OrderTested, OrderCustomerAccepted, OrderDelivered, OrderRejected, OrderCancelled]
  end

  def to_specific(state)
    the_state = state   
    # If a Class was passed create an object so is_a? can be used
    if state.class == Class
      the_state = state.new
    elsif state.class == String
      the_state = name_mappings[state].new
    end
    return mappings.find {|k,v| the_state.is_a?(k)}[1] 
  end

  def to_generic(state)
    the_state = state   
    # If a Class was passed create an object so is_a? can be used
    if state.class == Class
      the_state = state.new
    elsif state.class == String
      the_state = name_mappings[state].new
    end
    return mappings.find {|k,v| the_state.is_a?(v)}[0]     
  end
  
  def name_to_class(name)
    to_specific(name_mappings[name])
  end
    
  def update_target_entity(new_state)
    if stateful_ownership != nil
      target_obj = stateful_ownership.stateable 
      # Update the order 
      target_obj.class.where("id = ?", target_obj.id).update_all(:order_name => new_state.name, :order_notes => new_state.notes, :order_timestamp => (new_state.timestamp.to_f*1000).to_i)
      target_obj.reload
    
      # Update the object assocaiated with the order
      target = target_obj.ordered_entity
      if target != nil
        target.class.where("id = ?", target.id).update_all(:order_name => new_state.name, :order_notes => new_state.notes, :order_timestamp => (new_state.timestamp.to_f*1000).to_i)
        # The update_all just does an sql query and does not invoke the after_save (which is what we want) BUT it does not update the object copy in memory so must reload
        target.reload
      end
    end
  end
  
  private
    
  def do_notify(new_state, changed)
    # Update any Bulk orders this order belongs 
    target_obj = stateful_ownership.stateable
    target_obj.containing_bulk_orders.each do |order|
      if order.bulk_order_type == "Circuit Order" && order.ordered_entity != nil && !(self.get_state.is_a?(OrderCustomerAccepted) || self.get_state.is_a?(OrderDesignComplete))
        # A circuit order that has an Path does not have it's state determined by the state of
        # the componenet orders only by the state of the Path itself. This is because the Path state 
        # may depend on other orders that are NOT in the Circuit order.
        # Note if the the current state is trying to go to CustomerAccepted or DesignComplete try to update the state. This will enable
        # them to fill in the customer acceptance/design complete date 
      else
        pre_result, post_result, pre_failed_objs, post_failed_objs = order.set_order_state(self.get_state)
        if !pre_result || !post_result
          SW_ERR "Failed pre or post #{pre_result}:#{post_result} pre_objs = #{pre_failed_objs.inspect} post_objs = #{post_failed_objs.inspect}", :TEMPORARILY_IGNORE
        end
      end
    end
    
    if changed
      # Need to do event server notify here
    end
  end

end
