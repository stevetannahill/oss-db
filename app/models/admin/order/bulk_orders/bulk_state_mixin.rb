module BulkStateMixin
  # Bulk orders take on the lowest state of all it's orders so it's state can change from any state to any state
  def valid_transitions
    return [OrderCreated, OrderReadyToOrder, OrderReceived, OrderAccepted, OrderDesignComplete, OrderProvisioned, OrderTested, OrderCustomerAccepted, OrderDelivered, OrderRejected, OrderCancelled]
  end
  
  def self.included(base)
    # Bulk order currently don't have any pre or post text
    def base.pre_text(stateful)
      return ""
    end
  
    def base.post_text(stateful)
      return ""
    end
  end
end