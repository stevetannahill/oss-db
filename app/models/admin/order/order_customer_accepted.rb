# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class OrderCustomerAccepted < OrderState 
  @@state_name = OrderStateful::CUSTOMER_ACCEPTED
  def self.state_name
    return @@state_name
  end
  
  def valid_transitions
    transitions = [OrderCustomerAccepted, OrderDelivered, OrderRejected, OrderCancelled]
    # If there is an ordered object and it's provisioned state is not Live or Deleted then disallow OrderDelivered state
    so = stateful.stateful_ownership.stateable
    ordered_object = so.ordered_entity

    # TODO: Removed by Andrew as we want to be able to transition to 'Customer Accepted' even if not in the Live/Deleted state
    # if ordered_object != nil
    #   if !(ordered_object.get_prov_state.is_a?(ProvLive) || ordered_object.get_prov_state.is_a?(ProvDeleted))
    #     transitions.delete(OrderDelivered)
    #   end
    # end
    return transitions
  end
  
  def self.pre_text(stateful)
    return "Ensure the customer has accepted the service"
  end
  
  def self.post_text(stateful)
    ordered_entity_type = stateful.stateful_ownership.stateable.ordered_entity_subtype.to_s.underscore.humanize.upcase
    return "Move applicable components to Live state in CDB\nBegin Billing for this #{ordered_entity_type} per Billing Start Date of the order."
  end
  
  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    result = result && set_order_date(stateful, "customer_acceptance_date", Time.now, failed_objects)
    return result
  end
  
end
