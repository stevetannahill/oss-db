# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class OrderCreated < OrderState 
  @@state_name = OrderStateful::CREATED
  def self.state_name
    return @@state_name
  end
  
  def valid_transitions
    transitions = [OrderCreated, OrderReadyToOrder, OrderRejected, OrderCancelled]
    return transitions 
  end
  
  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    result = result && set_order_date(stateful, "order_created_date", Time.now, failed_objects)
    return result
  end
end
