# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class OrderState < State
  
  def initialize(*params)
    super(*params)
    self.name = self.class.state_name if name.nil?    
    self.timestamp = (Time.now.to_f*1000).to_i if timestamp.nil? 
  end
  
  def pre_conditions(stateful, previous_state, failed_objects = [])
    result = super    
    # Check to see if transition is possible
    result = result && stateful.get_state.valid_transitions.any? {|klass| self.is_a?(klass) } 
    failed_objects << {:obj => stateful.stateful_ownership.stateable, :reason => "Not a valid transition from #{previous_state.class} to #{self.class}"} if !result
    return result
  end
  
  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    failed_objects << {:obj => stateful.stateful_ownership.stateable, :reason => ""} if !result
    return result
  end
  
  private
  
  def set_order_date(stateful, which_date, date, failed_objects)   
    so = stateful.stateful_ownership.stateable    
    so.send("#{which_date}=".intern, date.to_date)
    result = so.save
    if !result
      failed_objects <<  {:obj => so, :reason => so.errors.full_messages.join(", ")}
    end
    return result
  end
  
end  
