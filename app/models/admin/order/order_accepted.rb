# == Schema Information
#
# Table name: states
#
#  id          :integer(4)      not null, primary key
#  timestamp   :integer(8)
#  name        :string(255)
#  notes       :text
#  stateful_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#  type        :string(255)
#

class OrderAccepted < OrderState 
  @@state_name = OrderStateful::ACCEPTED
  def self.state_name
    return @@state_name
  end
  
  def valid_transitions
    return [OrderAccepted, OrderDesignComplete, OrderRejected, OrderCancelled]
  end
  
  def self.pre_text(stateful)
    return "Ensure the order has been reviewed with OPS"
  end
  
  def self.post_text(stateful)
    s =  "Review the Order\n"
    s += "Add  per CoS QoS information to the Order form and save it in Alfresco\n"
    s += "Add FOC Date to the Order\n"
    s += "Move order to 'Design Complete' State in CDB"
  end
  
  def post_conditions(stateful, previous_state, failed_objects = [])
    result = super
    result = result && set_order_date(stateful, "order_acceptance_date", Time.now, failed_objects)
    return result
  end
end
