# == Schema Information
#
# Table name: service_orders
#
#  id                          :integer(4)      not null, primary key
#  type                        :string(255)
#  title                       :string(255)
#  cenx_id                     :string(255)
#  action                      :string(255)
#  primary_contact_id          :integer(4)
#  testing_contact_id          :integer(4)
#  requested_service_date      :date
#  order_received_date         :date
#  order_acceptance_date       :date
#  order_completion_date       :date
#  customer_acceptance_date    :date
#  billing_start_date          :date
#  operator_network_id         :integer(4)
#  expedite                    :boolean(1)
#  status                      :string(255)
#  notes                       :text
#  ordered_entity_id           :integer(4)
#  ordered_entity_type         :string(255)
#  created_at                  :datetime
#  updated_at                  :datetime
#  ordered_entity_subtype      :string(255)
#  path_id                     :integer(4)
#  ordered_entity_group_id     :integer(4)
#  ordered_operator_network_id :integer(4)
#  order_state                 :string(255)
#  ordered_object_type_id      :integer(4)
#  ordered_object_type_type    :string(255)
#  ordered_entity_snapshot     :text
#  technical_contact_id        :integer(4)
#  local_contact_id            :integer(4)
#  foc_date                    :date
#  bulk_order_type             :string(255)
#  design_complete_date        :date
#  order_created_date          :date
#  order_name                  :string(255)
#  order_notes                 :text
#  order_timestamp             :integer(8)
#

class OffNetOvcOrder < OvcOrder
  belongs_to :ordered_entity, :class_name => "OffNetOvc",  :foreign_key => "ordered_entity_id"
  
  validates_presence_of :ordered_entity_group_id
  validates_class_of :ordered_entity, :is_a => "OffNetOvc", :allow_nil => true
  
  def select_valid_oegs oegs
    return oegs.select{|oeg| oeg.ordered_entity_types.map{|oet| oet.class}.include? SegmentType}
  end
  
  def get_candidate_ordered_object_types
    return ordered_entity_group ? ordered_entity_group.segment_types : []
  end
  
end
