# == Schema Information
# Schema version: 20110802190849
#
# Table name: member_attr_instances
#
#  id                   :integer(4)      not null, primary key
#  member_attr_id       :integer(4)
#  affected_entity_id   :integer(4)
#  affected_entity_type :string(255)
#  value                :string(255)     default("TBD")
#  created_at           :datetime
#  updated_at           :datetime
#  type                 :string(255)
#  owner_id             :integer(4)
#  owner_type           :string(255)
#

class MemberAttrInstance < ActiveRecord::Base
  belongs_to :affected_entity, :polymorphic => true
  belongs_to :member_attr
  delegate :member_attr_controls, :to => :member_attr
  
  scope :owned_by, lambda { |provider|
    { :conditions => [ "owner_id = ?", provider.id ] }
  }

  validates_presence_of :affected_entity_id, :affected_entity_type, :member_attr_id
  validates_uniqueness_of :affected_entity_id, :scope => [:member_attr_id, :affected_entity_type, :owner_id, :owner_type]
  validate :valid_value?
  
  # TODO: Put back in once rake spec is fixed
  # validates_length_of :value, :maximum => 255

  def self.search str
    attrs = all
    results = attrs.select { |attr| attr[:value] != nil && attr[:value].to_s.downcase.include?(str.downcase) }
    return results
  end
  
  def member_attr_id=(ma_id)
    self[:member_attr_id] = ma_id
    self.value = member_attr.default_value.to_s if member_attr && self.new_record? && !member_attr.default_value.to_s.empty?
  end

  def name
    member_attr.name
  end
  
  def update_value
    member_attr_controls.each{|mac|
      case mac.action
      when "Set Value To" then self[:value] = eval(mac.eval_code)
      when "Execute Code" then eval(mac.eval_code)
      end
    }
  end
  
  before_validation :general_before_validation

  def general_before_validation
    update_value
  end
  
  before_save :general_before_save

  def general_before_save
    update_value
  end
  
  def observes
    mais = []
    member_attr_controls.each{|mac|
      mac.eval_code.scan(/(^|\s|[;+-=*(){}\[\]&|<>,?%:])(affected_entity\.member_attr_instance\s*("([^\\"]*\\.)*[^\\"]*"|\("([^\\"]*\\.)*[^\\"]*"\)|'([^\\']*\\.)*[^\\']*'|\('([^\\']*\\.)*[^\\']*'\)))/){|match|
        mais.push(eval($2))
      }
    }
    mais.uniq
  end
  
  def form_id
    "mai_" + member_attr_id.to_s
  end
  
  def full_form_id
    form_id + "_" + affected_entity_type.to_s + "_" + affected_entity_id.to_s
  end
  
  def self.form_id_pattern
    return /^mai_\d+(_\d+_[^_]+)?(_[^_]+_\d+)?$/
  end
  
  def self.full_form_id_pattern
    /^mai_\d+(_\d+_[^_]+)?_[^_]+_\d+?$/
  end

  def const
    member_attr.const
  end
  
  def value
    return case syntax
    when "integer" then self[:value].to_i
    when "boolean" then (self[:value] == "1")
    else self[:value]
    end
  end

  def syntax
    member_attr.syntax
  end

  def select_options
    member_attr.select_options
  end
  
  def min
    member_attr.min
  end
  
  def max
    member_attr.max
  end
  
  def pattern
    member_attr.pattern
  end
  
  def evaluate(mac)
    eval(mac.eval_code)
  end
  
  def visible
    macs = member_attr_controls.select{|mac| mac.action == "Show if True"}
    macs.empty? || !macs.select{|mac| eval(mac.eval_code)}.empty?
  end
  
  def allow_blank
    !visible || member_attr.allow_blank && member_attr_controls.select{|mac| mac.action == "Require if True" && eval(mac.eval_code)}.empty?
  end
  
  def disabled
    macs = member_attr_controls.select{|mac| mac.action == "Disable if True" }
    member_attr.disabled || !macs.empty? && macs.reject{|mac| eval(mac.eval_code)}.empty?
  end
  
  private
  def valid_value?
    error_macs = member_attr_controls.reject{|mac| mac.action != "Assure is True" || eval(mac.eval_code)}
    if self[:value] == nil
      return true if allow_blank
      errors[:base] << "#{name} is nil!"
      return false
    elsif self[:value].to_s.empty?
      return true if allow_blank
      errors[:base] << "#{name} is blank!"
      return false
    elsif !error_macs.empty?
      error_macs.each{|mac| errors[:base] << "#{name} #{mac.info}"}
      return false
    elsif syntax == "integer"
      if !self[:value].to_s.match(/\A\d+\z/)
        errors[:base] << "#{name} is not a valid integer!"
        return false
      elsif self[:value].to_i > max
        errors[:base] << "#{name} may not be larger than #{max}!"
        return false
      elsif self[:value].to_i < min
        errors[:base] << "#{name} may not be smaller than #{min}!"
        return false
      else
        return true
      end
    elsif syntax == "menu"
      return true if select_options.include? self[:value]
      errors[:base] << "#{name} invalid, Use: #{select_options.join(", ")}"
      return false
    elsif syntax == "string"
      return true if self[:value].match(pattern) || self[:value] == pattern
      errors[:base] << "#{name} does not match #{pattern.inspect}!"
      return false
    end
  end

end
