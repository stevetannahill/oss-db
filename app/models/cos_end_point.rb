# == Schema Information
#
# Table name: cos_end_points
#
#  id                         :integer(4)      not null, primary key
#  segment_end_point_id       :integer(4)
#  cos_instance_id            :integer(4)
#  ingress_mapping            :string(255)
#  ingress_cir_kbps           :string(255)
#  ingress_eir_kbps           :string(255)
#  ingress_cbs_kB             :string(255)
#  ingress_ebs_kB             :string(255)
#  created_at                 :datetime
#  updated_at                 :datetime
#  connected_cos_end_point_id :integer(4)
#  type                       :string(255)
#  egress_marking             :string(255)
#  egress_cir_kbps            :string(255)
#  egress_eir_kbps            :string(255)
#  egress_cbs_kB              :string(255)
#  egress_ebs_kB              :string(255)
#  ingress_rate_limiting      :string(255)     default("Policing")
#  egress_rate_limiting       :string(255)     default("None")
#  event_record_id            :integer(4)
#  sm_state                   :string(255)
#  sm_details                 :string(255)
#  sm_timestamp               :integer(8)
#

class CosEndPoint < InstanceElement
  include Eventable
  
  belongs_to :segment_end_point #structural
  belongs_to :cos_instance
  has_and_belongs_to_many :cos_end_points,
                          :foreign_key => 'self_id',
                          :after_add => :add_reverse_link,
                          :after_remove => :remove_reverse_link

  has_many :cos_test_vectors, :dependent => :destroy

  validates_presence_of :segment_end_point_id, :cos_instance_id, :ingress_mapping, :egress_marking #Not always known, :cir_kbps, :eir_kbps, :cbs_kB, :ebs_kB
  validates_uniqueness_of :cos_instance_id, :scope => :segment_end_point_id
  validates_inclusion_of :ingress_rate_limiting, :in => ServiceCategories::RATE_LIMITING_MECHANISM_ACTUAL.map{|value| value },
                         :message => "invalid, Use: #{ServiceCategories::RATE_LIMITING_MECHANISM_ACTUAL.map{|value| value }.join(", ")}"
  validates_inclusion_of :ingress_mapping, :in => ("0".."7").to_a  << '*',
                         :message => "invalid, Use: #{(("0".."7").to_a  << '*').join(", ")}"
  validates_inclusion_of :egress_marking, :in => ("0".."7").to_a  << '-',
                         :message => "invalid, Use: #{(("0".."7").to_a  << '-').join(", ")}"
  validate :valid_cose_links?
  validate :valid_rlm_data?
  validate :valid_ingress_mapping?
  validate :valid_settings_for_test_endpoint?
  before_validation :strip_data
  after_create :general_after_create
  
  def general_after_create
    update_attributes(:sm_state => AlarmSeverity::NOT_LIVE.to_s, :sm_details => "", :sm_timestamp => (Time.now.to_f*1000).to_i)
    event_record_create
  end

  def is_system_owner?
    return false
  end

  #Helper mbps functions
  def byte_convert str, dest
    float = str.to_f
    if dest == :mb
      value = float / 1000
    elsif dest == :kb
      value = float * 1000
    end
    #Remove unneeded decimals
    value = value.to_i if value % 1 == 0
    return value.to_s
  end

  def ingress_cir_Mbps
    return byte_convert ingress_cir_kbps, :mb
  end
  def ingress_eir_Mbps
    return byte_convert ingress_eir_kbps, :mb
  end
  def egress_cir_Mbps
    return byte_convert egress_cir_kbps, :mb
  end
  def egress_eir_Mbps
    return byte_convert egress_eir_kbps, :mb
  end

  def ingress_cir_Mbps= value
    self.ingress_cir_kbps = byte_convert value, :kb
  end
  def ingress_eir_Mbps= value
    self.ingress_eir_kbps = byte_convert value, :kb
  end
  def egress_cir_Mbps= value
    self.egress_cir_kbps = byte_convert value, :kb
  end
  def egress_eir_Mbps= value
    self.egress_eir_kbps = byte_convert value, :kb
  end
  
  def add_reverse_link(cose)
    cose.cos_end_points << self unless cose.cos_end_points.include?(self) || cose == self
  end
                          
  def remove_reverse_link(cose)
    cose.cos_end_points.delete(self)  if cose.cos_end_points.include?(self) && cose != self
  end
  
  def ingress_egress_compatible? cose
    if (((cose.ingress_mapping.include?(egress_marking)) && (cose.egress_marking.include?(ingress_mapping)))||
        ((ingress_mapping.include?(cose.egress_marking)) && (egress_marking.include?(cose.ingress_mapping)))||
        (cose.ingress_mapping == '*') ||
        (cose.egress_marking == '-') || #Sprint Demo
        (ingress_mapping == '*')) ||
        (egress_marking == '-') #Sprint Demo

      return true
    else
      return false
    end
    
  end

  def get_candidate_cos_end_points
    coses = (segment_end_point && !segment_end_point.segment_end_points.empty?) ? segment_end_point.segment_end_points.map{|segmentep| segmentep.cos_end_points}.flatten : []
    coses.reject!{|cose| !ingress_egress_compatible? cose}
    return coses - cos_end_points
  end

  def get_candidate_cos_instances
    segment_end_point ? segment_end_point.get_candidate_cos_instances : []
  end

  def get_candidate_service_level_guarantee_types
    if cos_instance
      return cos_instance.get_candidate_service_level_guarantee_types
    else
      SW_ERR "No bw_profile_type defined on #{pulldown_name}"
      return []
    end
  end

  def demarc_connection_info_segmentep
    segment_end_point ? "#{segment_end_point.demarc_connection_info_segmentep}" : "Nothing"
  end

  def end_point_info
    segment_end_point ? segment_end_point.cenx_name : "Nothing"
  end

  def segment_end_point_connection_info
    segment_end_point ? "#{segment_end_point.end_point_connection_info}" : "Nothing"
  end

  def cos_end_point_connection_info
    cos_end_points.empty? ? "Nothing" : cos_end_points.collect{|cose| cose.pulldown_name}.join(", ")
  end

  def cos_name
    return cos_instance ? cos_instance.cos_name : 'Unknown'
  end

  def class_of_service_type
    return cos_instance ? cos_instance.class_of_service_type : "Unknown"
  end

  def name
    return "#{cos_name} - (E. Mark:#{egress_marking}; I. Map:#{ingress_mapping}) - #{cos_instance ? cos_instance.ethernet_service_type_info : 'Unknown'}"
  end

  def pulldown_name
    return "#{end_point_info} - #{name}"
  end

  def is_test?
    (segment_end_point) ? segment_end_point.is_connected_to_test_port : false
  end
  
  def get_candidate_ingress_mapping
    (segment_end_point.kind_of? OvcEndPointEnni) ? cos_instance.get_candidate_cos_mapping_enni_ingress : cos_instance.get_candidate_cos_mapping_uni_ingress
  end

  def get_candidate_egress_marking
    (segment_end_point.kind_of? OvcEndPointEnni) ? cos_instance.get_candidate_cos_marking_uni_egress : cos_instance.get_candidate_cos_marking_uni_egress
  end

  def locale
    (segment_end_point.kind_of? OvcEndPointEnni) ? "ENNI" : "UNI"
  end

  def get_attached_endpoint_in_path path_ids
    ep = nil

    if cos_end_points.size == 0
      return ep
    end

    cos_end_points.each do |e|
      if ((e.get_path_ids & path_ids).any?)
        return e
      end
    end

    return ep

  end

  def is_connected_to_other_endpoint
    return (cos_end_points.size != 0)
  end


  def get_cenx_monitoring_cos_end_point
    candidates = get_cos_end_points_in_monitoring_chain get_path_ids
    candidates.reject!{|ep| !(ep.segment_end_point.kind_of? OnNetOvcEndPointEnni)}
    candidates.reject!{|ep| !(ep.segment_end_point.is_connected_to_monitoring_port)}

    if candidates.size == 1
      #SW_ERR "ERROR - Test: (#{chain.size})"
      return candidates.first
     else
      # Never expect != 1
      SW_ERR "ERROR - wrong number of Cenx Monitoring Endpoints found: (#{candidates.size})"
      return nil
    end

  end

  def get_remote_sibling_cos_end_points
    candidates = get_cos_end_points_in_monitoring_chain get_path_ids
    
    # remove eps in this Segment
    candidates.reject!{|ep| (ep.segment_end_point.segment == segment_end_point.segment)}

    #remove eps that are not connected to anyting
    candidates.reject!{|ep| !ep.is_connected_to_other_endpoint}

    # HACK remove the near end ep at Cenx ENNI
    # TODO this wont work for Tethering case since there may be 2 unis in the way
    # probably need another 'get_chain' type of function to do this properly
    candidates.reject!{|ep| !(ep.segment_end_point.is_a? OvcEndPointEnni)}

    return candidates

  end
  
  def get_path_ids
    ids = []
    paths = segment_end_point.segment.paths
    paths.each do |path|
      ids << path.id
    end
    return ids
  end

  # This function is called recursively
  def get_cos_end_points_in_monitoring_chain path_ids
    ep_chain = []

    # Always push this endpoint
    ep_chain << self

    # if we are cenx endpoint, push mon endpoint and return
    if segment_end_point.kind_of? OnNetOvcEndPointEnni
      cenx_monitoring_cos_end_points = get_cenx_monitoring_cos_endpoints

      if cenx_monitoring_cos_end_points.size == 1
        ep_chain << cenx_monitoring_cos_end_points.first
      else
        # Never expect != 1
        SW_ERR "ERROR - wrong number of Cenx Monitoring Endpoints found: (#{cenx_monitoring_cos_end_points.size})", :TEMPORARILY_IGNORE
      end

      return ep_chain.flatten
    end

    # not a cenx endpoint - find next endpoint in this Segment in the chain
    sibling_cos_end_points = get_sibling_cos_end_points
    connected_sibblings = sibling_cos_end_points.reject{|ep| !ep.is_connected_to_other_endpoint}

    if connected_sibblings.size == 1
      # push the first endpoint that continues the chain
      next_ep =  connected_sibblings.first
      ep_chain << next_ep

      # Find the ep on the other side of the demark
      attached_ep = next_ep.get_attached_endpoint_in_path path_ids
      if attached_ep
        # Recursive call
        ep_chain << (attached_ep.get_cos_end_points_in_monitoring_chain path_ids)
      end
    else
      # Never expect != 1
      SW_ERR "ERROR - wrong number of Connected Siblings found: (#{connected_sibblings.size})", :IGNORE_IF_TEST
    end

    return ep_chain.flatten
  end

  def get_sibling_cos_end_points
    return cos_instance.get_sibling_cos_end_points id
  end

  def get_monitoring_manager
    cos_test_vectors.first.get_monitoring_manager if !cos_test_vectors.empty?
  end
  
  def mtc_history
    return segment_end_point.mtc_history
  end
  
  def mtc_state
    return segment_end_point.mtc_state
  end
  
  def alarm_history
    final_history = super
    cos_test_vectors.each do |ctv|      
      # For all primary if there is a backup take the lowest or make it a warning
      # If there is no backup then just take the stte of the primary
      # and then take the highest of them all if there happen to be multiple primary/backup pairs
      if ctv.primary
        ctv_history = AlarmHistory.merge([ctv.alarm_history, ctv.paired_ctv.alarm_history]) {|events| AlarmRecord.select_lowest_alarm_or_warn(events)}   
        final_history = final_history + ctv_history
      end
    end
    return final_history
  end
  
  def alarm_state
    state = super
    #cos_test_vectors.each {|ctv| state = state + ctv.event_record} 
    cos_test_vectors.each do |ctv|      
      # If there is a paired CTV AND they are both on the same endpoint (not CosEndPoint) take the lowest or make it a warning
      # If there is no paired CTV then just take the state of the CTV
      # and then take the highest of them all if there happen to be multiple primary/backup pairs
      paired_ctv = ctv.paired_ctv
      if paired_ctv && paired_ctv.cos_end_point.segment_end_point == segment_end_point
        paired_ctv_state = paired_ctv.event_record if paired_ctv
      else
        paired_ctv_state = nil
      end
      state_ctv = AlarmRecord.select_lowest_alarm_or_warn([ctv.event_record, paired_ctv_state])
      state = state + state_ctv
    end
    return state
  end
  
  def alarm_history
    state = super
    cos_test_vectors.each {|ctv| state = state + ctv.alarm_history}  
    return state
  end
      
  def update_alarm(list_of_evaled_objects = [], full_eval = false)
    changed = super
    if changed
      segment_end_point.eval_alarms(list_of_evaled_objects, full_eval)
      # If there is a paired CTV we must re-eval those COS endpoints as well
      ceps = cos_test_vectors.collect {|ctv| ctv.paired_ctv.cos_end_point if ctv.paired_ctv && ctv.paired_ctv.cos_end_point.segment_end_point == segment_end_point}.compact.uniq      
      ceps = ceps - [self]
      ceps.each {|cep| cep.eval_alarms(list_of_evaled_objects, full_eval)} 
    end
    
    return changed
  end
    
  def go_in_service
    info = ""   
    return_value = cos_test_vectors.all? do |ctv| 
      ctv_result = ctv.go_in_service
      info << ctv_result[:info]
      ctv_result[:result]  
    end
    info = "Failed to initialise" if !return_value && info.blank? 
    return {:result => return_value, :info => info}
  end 

  private
  
  def valid_cose_links?
    cos_end_points.each{|cose|
      if !ingress_egress_compatible? cose
        errors.add(:cos_end_points, "must have compatible Ingress Mapping and Egress Marking as the attached COS end point")
        return false
      end
    }
  end

  def valid_rlm_data?
    return if is_test?

    if egress_rate_limiting == "None"
      if  (egress_cir_kbps != '' and egress_cir_kbps != nil) or
          (egress_cbs_kB != '' and egress_cbs_kB != nil) or
          (egress_eir_kbps != '' and egress_eir_kbps != nil) or
          (egress_ebs_kB != '' and egress_ebs_kB != nil)
        values = "(egress_cir_kbps [#{egress_cir_kbps}], egress_cbs_kB [#{egress_cbs_kB}], egress_eir_kbps [#{egress_eir_kbps}], egress_ebs_kB [#{egress_ebs_kB}],)"
        errors.add(:base,"Don't specify rates (#{values}) if there is no rate limiting mechanism on egress, rate")
      end
    else
      if (egress_cir_kbps.empty? or egress_cir_kbps.nil? or egress_cir_kbps == "0") and
         (egress_eir_kbps.empty? or egress_eir_kbps.nil? or egress_eir_kbps == "0")
       errors.add(:base,"Egress CIR or EIR must be non zero (was #{egress_cir_kbps} / #{egress_eir_kbps}) for rate limiting mechanism #{egress_rate_limiting} on egress, rate")
      end
    end

    if ingress_rate_limiting == "None"
      if  (ingress_cir_kbps != '' and ingress_cir_kbps != nil) or
          (ingress_cbs_kB != '' and ingress_cbs_kB != nil) or
          (ingress_eir_kbps != '' and ingress_eir_kbps != nil) or
          (ingress_ebs_kB != '' and ingress_ebs_kB != nil)
        values = "(ingress_cir_kbps [#{ingress_cir_kbps}], ingress_cbs_kB [#{ingress_cbs_kB}], ingress_eir_kbps [#{ingress_eir_kbps}], ingress_ebs_kB [#{ingress_ebs_kB}],)"
        errors.add(:base,"Don't specify rates if there is no rate limiting mechanism on ingress, rate")
      end
    else
      if (ingress_cir_kbps.empty? or ingress_cir_kbps.nil? or ingress_cir_kbps == "0") and
         (ingress_eir_kbps.empty? or ingress_eir_kbps.nil? or ingress_eir_kbps == "0")
       errors.add(:base,"Ingress CIR or EIR must be non zero (was #{ingress_cir_kbps} / #{ingress_eir_kbps}) for rate limiting mechanism #{ingress_rate_limiting} on ingress, rate")
      end
    end
  end

  def valid_ingress_mapping?

    #Attempting to make a new cos_endpoint with * but there already exists other endpoints
    if ingress_mapping == "*" and segment_end_point.cos_end_point_count > 1
      errors.add(:base,"Ingress mapping can not be \"*\" because this Segment is multi-CoS, mapping")
      return
    end

    #Attempting to add a new cos_endpoint on top of an already existing * cos endpoint
    segment_end_point.cos_end_points.each do |ep|
      if ep.ingress_mapping == '*' and ep != self
        errors.add(:base,"Cos Endpoint #{ep.cos_name} has ingress_mapping set to \"*\", can not create endpoint. It")
        break
      end
    end
  end

  def valid_settings_for_test_endpoint?
    return unless is_test?
    
    rates = ServiceCategories::CENX_TEST_COS_VALUES[:rates]
    rates.keys.each do |method|
      if send(method).to_i != rates[method].to_i
        errors.add(:base,"#{method.gsub("_"," ")} must be #{rates[method].to_s.empty? ? "empty" : rates[method]} for COSE connected to Test")
      end
    end
    
    if ingress_rate_limiting != ServiceCategories::CENX_TEST_COS_VALUES[:rlm]["Ingress"]
      errors.add(:base,"Ingress rate limiting method must be #{ServiceCategories::CENX_TEST_COS_VALUES[:rlm]["Ingress"]} for test cose")
    end
    if egress_rate_limiting != ServiceCategories::CENX_TEST_COS_VALUES[:rlm]["Egress"]
      errors.add(:base,"Egress rate limiting method must be #{ServiceCategories::CENX_TEST_COS_VALUES[:rlm]["Egress"]} for test cose")
    end
    
    fwding_class = ServiceCategories::ON_NET_OVC_COS_TYPES[class_of_service_type.name][:fwding_class]
    if ingress_mapping != ServiceCategories::CENX_TEST_COS_VALUES[:mapping][fwding_class]
       errors.add(:base,"Ingress mapping must be #{ServiceCategories::CENX_TEST_COS_VALUES[:mapping][fwding_class]} for #{cos_name} test cose")
    end
    if egress_marking != ServiceCategories::CENX_TEST_COS_VALUES[:mapping][fwding_class]
      errors.add(:base,"Egress marking must be #{ServiceCategories::CENX_TEST_COS_VALUES[:mapping][fwding_class]} for #{cos_name} test cose")
    end

  end

  def strip_data
    self.ingress_cir_kbps = ingress_cir_kbps.to_s.strip
    self.ingress_cbs_kB = ingress_cbs_kB.to_s.strip
    self.ingress_eir_kbps = ingress_eir_kbps.to_s.strip
    self.ingress_ebs_kB = ingress_ebs_kB.to_s.strip

    self.egress_cir_kbps = egress_cir_kbps.to_s.strip
    self.egress_cbs_kB = egress_cbs_kB.to_s.strip
    self.egress_eir_kbps = egress_eir_kbps.to_s.strip
    self.egress_ebs_kB = egress_ebs_kB.to_s.strip
  end
  
  public

  # return the capacity of a COS endpoint for the given direction
  def rate_mbps(direction)
    capacity_mbps = 0
    if direction == :ingress
      if 'Policing' == ingress_rate_limiting
        capacity_mbps = (ingress_cir_kbps.to_i + ingress_eir_kbps.to_i) / 1000
      else
        capacity_mbps = segment_end_point.enni.rate
      end
    else
      if 'Policing' == egress_rate_limiting
        capacity_mbps = (egress_cir_kbps.to_i + egress_eir_kbps.to_i) / 1000
      else
        capacity_mbps = segment_end_point.enni.rate
      end
    end
    #puts "CosEndPoint capacity: capacity_mbps(#{capacity_mbps}) direction(#{direction}), ingress_rate_limiting(#{ingress_rate_limiting}), ingress_cir_kbps(#{ingress_cir_kbps}), ingress_eir_kbps(#{ingress_eir_kbps}) | egress_rate_limiting(#{egress_rate_limiting}), egress_rate_limiting(#{egress_rate_limiting}), egress_cir_kbps(#{egress_cir_kbps}), egress_eir_kbps(#{egress_eir_kbps})"
    return capacity_mbps
  end
  
  # where can stats be retrieved
  def stats_entities
    ctvs = cos_test_vectors
    # Display Primary ctv (sorted by name) and then backups (sorted by name)
    primary = ctvs.select {|v| v.primary}.sort_by!{|v| v.short_name}
    backup = ctvs.reject {|v| v.primary}.sort_by!{|v| v.short_name}
    primary + backup
  end

end

