class MetricsReport < Report
  extend ExceptionReports
  
  def self.generate_report(schedule_id)
    generate_exception_report(schedule_id, :metrics_exceptions)
  end
  
  def update_report
    false
  end
  
end