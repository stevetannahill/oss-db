# encoding: UTF-8
# == Schema Information
# Schema version: 20110802190849
#
# Table name: service_providers
#
#  id                       :integer(4)      not null, primary key
#  name                     :string(255)
#  website                  :string(255)
#  is_system_owner                  :boolean(1)
#  password_expiry_interval :integer(4)
#  created_at               :datetime
#  updated_at               :datetime
#  logo_url                 :string(255)
#  address                  :string(255)
#  cenx_cma                 :string(255)
#  cenx_id                  :string(255)
#  notes                    :string(255)
#

class ServiceProvider < ActiveRecord::Base
  include Kpiable
          
  has_many :users, :dependent => :destroy
  has_many :operator_networks, :order => "name", :dependent => :destroy
  has_many :operator_network_types, :order => "name", :dependent => :destroy
  has_many :enni_news, :through => :operator_networks
  has_many :ennis_on_behalf_of, :class_name => 'EnniNew', :foreign_key => :on_behalf_of_service_provider_id # Some Ennis were provisioned on behalf of this service provider
  has_many :demarcs, :through => :operator_networks
  has_many :paths, :through => :operator_networks
  has_many :contacts, :order => "name", :dependent => :destroy
  has_many :uploaded_files, :as => :uploader, :dependent => :destroy
  has_many :segments, :through => :operator_networks
  has_many :sin_path_service_provider_sphinxes
  has_many :sin_enni_service_provider_sphinxes
  store :terms, accessors: [ :network_type, :aggregation_site, :site, :wdc, :gateway_site, :demarc, :site_group, :service_provider, :path, :enni, :segment, :aav, :microwatt, :cascade ]

  validates_presence_of :name
  validates_uniqueness_of :name
  validates_as_cenx_id :cenx_id
  validate :valid_cenx_cma?

  after_save :set_path_sphinx_delta_flag
  after_create :general_after_create
  before_validation :generate_id_and_cma, :on => :create
  after_initialize :initialize_terms
  before_save :update_kpi_table, :if => :is_system_owner_changed?

  scope :real, :conditions => ["IFNULL(is_system_owner,0) = ?",false], :order => 'name ASC'

  attr_protected :is_system_owner, :is_system_logo

  def self.system_owner
    return find_by_is_system_owner(true)
  end
  
  def initialize_terms
    # Set the default terms
    self.path             ||= 'Path'
    self.enni             ||= 'ENNI'
    self.network_type     ||= 'Network Type'
    self.aggregation_site ||= 'Aggregation Site'
    self.site             ||= 'Site'
    self.wdc              ||= 'WDC'
    self.gateway_site     ||= 'CDC'
    self.demarc           ||= 'Demarc'
    self.site_group       ||= 'Site Group'
    self.service_provider ||= 'Service Provider'
    self.segment          ||= 'Segment'
    self.microwatt        ||= 'µW'
    self.aav              ||= 'AAV'
    self.cascade          ||= 'Cascade ID'
  end

  def general_after_create
    create_default_kpis
  end

  def self.find_rows_for_index
    order("name")
  end

  def self.system_admins
    where(:is_system_owner => true)
  end

  def self.set_system_logo(sp)
    self.transaction do
      self.update_all(:is_system_logo => false)
      sp.is_system_logo = true
      sp.save!
    end
  end

  def name_with_admin_label
    is_system_owner? ? "#{name} (System Owner)" : name
  end
  
  def really_cenx?
    # COX HACK
    name.upcase == 'CENX' || name.upcase == 'COX'
  end

  def is_sprint?
    name.downcase.include? 'sprint'
  end

  def service_orders
    onovcos = []
    operator_networks.each do |on|
      onovcos += on.service_orders
    end

    return onovcos
  end

  def enni_count
    return enni_news.size
  end

  def path_count
    return paths.size
  end

  def demarc_count
    return demarcs.size
  end

  def ont_count
    return operator_network_types.size
  end

  def contact_count
    return contacts.size
  end

  def short_name
    CenxNameTools::CenxNameHelper.shorten_name(name)
  end

  def generate_id_and_cma
    self.cenx_id = IdTools::CenxIdGenerator.generate_cenx_id(self.class)
    self.cenx_cma = IdTools::CenxCmaGenerator.generate_cenx_cma(self.class)
  end
      
  # KPI methods
  def update_kpi(parent, parent_kpi, kpi_name, kpi_object)
    if parent_kpi == nil
      parent_kpi = get_kpi_mask(kpi_name, kpi_object)
    end
    case kpi_object
    when EnniNew.name, OnNetOvcEndPointEnni.name, BxCosTestVector.name, SpirentCosTestVector.name
      kpi = calculate_kpi(kpi_name, kpi_object, parent_kpi, self)
      # Find all operator networks which have Enni KPIs that don't point to the service provider and only update those
      #ons = operator_networks.joins([:enni_news, :kpis]).where("`kpis`.`id` != ? and `kpis`.`name` = ? and `kpis`.`for_klass` = ? and `kpis`.`actual_kpi` = true", kpi.id, kpi_name, kpi_object)
      ons = operator_networks
      ons.each do |on|
        on.update_kpi(parent, kpi, kpi_name, kpi_object)
      end
    else
      SW_ERR "Invalid kpi_object #{kpi_object} for #{self.class}:#{self.id}"
    end         
  end
  
  def create_default_kpis
    default_kpis = [    
      {:name => "egress_util", :for_klass => EnniNew.name, :warning_time => 80.hours, :error_time => 80.hours, :delta_t => 5.minutes, :warning_threshold => 50, :error_threshold => 75, :units => "%", :actual_kpi => false},
      {:name => "ingress_util", :for_klass => EnniNew.name, :warning_time => 80.hours, :error_time => 80.hours, :delta_t => 5.minutes, :warning_threshold => 50, :error_threshold => 75, :units => "%", :actual_kpi => false},
      
      {:name => "delay", :for_klass => BxCosTestVector.name, :warning_time => 30.minutes, :error_time => 10.minute, :delta_t => 59.seconds, :warning_threshold => nil, :error_threshold => nil, :units => "ms", :actual_kpi => false},
      {:name => "delay_variation", :for_klass => BxCosTestVector.name, :warning_time => 30.minutes, :error_time => 10.minute, :delta_t => 59.seconds, :warning_threshold => nil, :error_threshold => nil, :units => "us", :actual_kpi => false},
      {:name => "flr", :for_klass => BxCosTestVector.name, :warning_time => 30.minutes, :error_time => 10.minute, :delta_t => 59.seconds, :warning_threshold => nil, :error_threshold => nil, :units => "%", :actual_kpi => false},
      
      {:name => "delay", :for_klass => SpirentCosTestVector.name, :warning_time => 30.minutes, :error_time => 10.minute, :delta_t => 5.minutes, :warning_threshold => nil, :error_threshold => nil, :units => "ms", :actual_kpi => false},
      {:name => "delay_variation", :for_klass => SpirentCosTestVector.name, :warning_time => 30.minutes, :error_time => 10.minute, :delta_t => 5.minutes, :warning_threshold => nil, :error_threshold => nil, :units => "us", :actual_kpi => false},
      {:name => "flr", :for_klass => SpirentCosTestVector.name, :warning_time => 30.minutes, :error_time => 10.minute, :delta_t => 5.minutes, :warning_threshold => nil, :error_threshold => nil, :units => "%", :actual_kpi => false}
    ]
    
    cenx_kpis = [    
      {:name => "egress_util", :for_klass => OnNetOvcEndPointEnni.name, :warning_time => 80.hours, :error_time => 80.hours, :delta_t => 5.minutes, :warning_threshold => 50, :error_threshold => 75, :units => "%", :actual_kpi => false},
      {:name => "ingress_util", :for_klass => OnNetOvcEndPointEnni.name, :warning_time => 80.hours, :error_time => 80.hours, :delta_t => 5.minutes, :warning_threshold => 50, :error_threshold => 75, :units => "%", :actual_kpi => false}
    ]

    if is_system_owner?
      default_kpis += cenx_kpis
    end
    
    default_kpis.each {|kpi| kpis << Kpi.create(kpi)}
  end
  
  # If the is_system_owner changes add/remove the KPIs
  def update_kpi_table
    cenx_kpis = [    
      {:name => "egress_util", :for_klass => OnNetOvcEndPointEnni.name, :warning_time => 80.hours, :error_time => 80.hours, :delta_t => 5.minutes, :warning_threshold => 50, :error_threshold => 75, :units => "%", :actual_kpi => false},
      {:name => "ingress_util", :for_klass => OnNetOvcEndPointEnni.name, :warning_time => 80.hours, :error_time => 80.hours, :delta_t => 5.minutes, :warning_threshold => 50, :error_threshold => 75, :units => "%", :actual_kpi => false}
    ]
    if is_system_owner?
      cenx_kpis.each {|kpi| kpis << Kpi.create(kpi)}
    else
      kpis.find_all_by_actual_kpi_and_for_klass(false, OnNetOvcEndPointEnni.name).each {|kpi| kpi.destroy}
    end
  end
  
  private
  
  def valid_cenx_cma?
      valid_cma, error_string = IdTools::CenxCmaGenerator.decode_cenx_cma(self.cenx_cma)
      unless ( valid_cma )
        self.cenx_cma = "Error Generating CMA!"
        errors.add(:cenx_cma, error_string)
        return false
      end
      return true
  end

  def set_path_sphinx_delta_flag
    ReindexSphinx.schedule
  end

end
