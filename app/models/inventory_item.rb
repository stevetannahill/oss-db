# == Schema Information
# Schema version: 20110802190849
#
# Table name: inventory_items
#
#  id                   :integer(4)      not null, primary key
#  description          :string(255)
#  cenx_naming_std      :string(255)
#  vendor               :string(255)
#  cabinet_position     :string(255)
#  make_model           :string(255)
#  serial_number        :string(255)
#  site_id :integer(4)
#  created_at           :datetime
#  updated_at           :datetime
#  cenx_po_number       :string(255)
#  service_start_date   :date
#  service_end_date     :date
#  notes                :string(255)
#

class InventoryItem < ActiveRecord::Base
  belongs_to :site
  validates_presence_of :vendor, :description, :site_id

  def self.find_rows_for_index
    order("description")
  end
  
  def set_from_node node
    nodes = CommonSite::NODE
    HwTypes::SITE_LAYOUTS.each do |site_layout|
      mod = Kernel.const_get(site_layout)
      nodes.merge!(mod::NODE)
    end
    node_info = nodes["N_#{node.node_type}"]
    

    self.site = node.site
    self.vendor = node_info[:vendor]
    self.make_model = node_info[:make_model]
    self.description = node_info[:description]
    self.cabinet_position = node.cabinet.name if node.cabinet
    self.cenx_naming_std = node.name
  end

  def site_info
    return "#{site.name}"    
  end
  
end
