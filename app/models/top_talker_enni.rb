# == Schema Information
#
# Table name: top_talkers
#
#  id                :integer(4)      not null, primary key
#  type              :string(255)
#  enni_id           :integer(4)
#  cos_end_point_id  :integer(4)
#  octets_count      :float
#  direction         :string(255)
#  time_period_start :integer(4)
#  time_period_end   :integer(4)
#

# To change this template, choose Tools | Templates
# and open the template in the editor.

class TopTalkerEnni  < TopTalker
 belongs_to :enni_new, :foreign_key => 'enni_id'
 validates_presence_of :enni_id

 def capactiy_percentage
   (self.average_mbps/self.enni_new.rate) * 100
 end
 
end
