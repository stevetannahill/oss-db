# == Schema Information
#
# Table name: reports
#
#  id                  :integer(4)      not null, primary key
#  type                :string(255)
#  schedule_id         :integer(4)
#  run_date            :datetime
#  report_period_start :datetime
#  report_period_end   :datetime
#  minor_exceptions    :integer(4)
#  major_exceptions    :integer(4)
#  critical_exceptions :integer(4)
#  created_at          :datetime        not null
#  updated_at          :datetime        not null
#
class EnniUtilizationReport < Report
  extend ExceptionReports
  
  def self.generate_report(schedule_id)
    scheduled_report = Schedule.find(schedule_id)
    user = scheduled_report.scheduled_task.user
    path_owner = user.system_admin? ? 'admin' : 'all'
    params = scheduled_report.scheduled_task.filter_options
    date_range_start, date_range_end = get_date_range(params['date_range_selector'], params['date_range_start'], params['date_range_end'])
    
    ennis = EnniNew.find_ennis(path_owner, user).s_unique
    exceptions = enni_utilization_exceptions(ennis, params, date_range_start, date_range_end, user)
    
    report = self.create(:major_exceptions => exceptions.s_major.count, :minor_exceptions => exceptions.s_minor.count, :critical_exceptions => exceptions.s_critical.count)
    scheduled_report.reports << report
    
    if scheduled_report.email_distribution
      CdbMailer.scheduled_report(report, exceptions, exception_finder).deliver
    end

    report
  end
  
  
  def update_report
    false
  end
  
end


