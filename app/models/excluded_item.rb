# == Schema Information
#
# Table name: excluded_items
#
#  id                :integer(4)      not null, primary key
#  exclusion_list_id :integer(4)
#  item              :string(255)
#
class ExcludedItem < ActiveRecord::Base  
  belongs_to :exclusion_list
  validates :exclusion_list, :presence => true

  after_save :update_reports, :if => :item_changed?
  before_destroy :update_reports
  
  
  def update_reports
    reports = exclusion_list.schedules.collect {|sr| sr.reports}.flatten
    reports.each {|rep| rep.update_report}
    true
  end

end

