# == Schema Information
#
# Table name: metrics_cos_test_vectors
#
#  id                  :integer(4)      not null, primary key
#  pop_id              :integer(4)
#  demarc_id           :integer(4)
#  protection_path_id  :integer(4)      default(1)
#  cos_test_vector_id  :integer(4)
#  period_start_epoch  :integer(8)
#  period_length_secs  :integer(4)
#  time_declared_epoch :integer(8)
#  severity            :string(0)
#  metric_type         :string(0)
#  metric              :float
#  minor_time_over     :integer(4)
#  major_time_over     :integer(4)
#
class MetricsWeeklyCosTestVector < ActiveRecord::Base
  include CommonException
  belongs_to :cos_test_vector, :primary_key => :grid_circuit_id, :foreign_key => :grid_circuit_id

  def entity_type
    'Off Net OVC'
  end

  def self.to_exception_metric_type stats_type
    case stats_type
    when :availability
      return :availability
    when :frame_loss_ratio
      return :flr
    when :delay
      return :fd
    when :delay_variation
      return :fdv
    end
  end

  def to_internal_metric_type
    case self.metric_type
    when 'availability' || 'avail'
      return :availability
    when 'flr'
      return :frame_loss_ratio
    when 'fd'
      return :delay
    when 'fdv'
      return :delay_variation
    end
  end

  def get_errored_period_threshold 
    severity = self.severity.to_sym
    cos_test_vector.get_errored_period_threshold(severity, self.to_internal_metric_type)
  end

  #   returns delta_t which can be longer than the native period (59 secs or 5 mins)
  # if 0 is returned then the native time period shall be used.
  def get_errored_period_delta_time_secs
    cos_test_vector.get_errored_period_delta_time_secs
  end

  # Returns the maximum number of time periods allowed over the threshold without
  # declaring an exception.
  # has to be greater than 1
  def get_errored_period_max_errored_count 
    severity = self.severity.to_sym
    cos_test_vector.get_errored_period_max_errored_count(severity, self.to_internal_metric_type)
  end

  def get_time_over_threshold_seconds
    severity = self.severity.to_sym
    (get_errored_period_max_errored_count || 0) * get_errored_period_delta_time_secs
  end


  def get_value_units_s
    # all severities now return the converted metric units, instead of major and minor always being seconds
    Stats::METRIC_UNITS[self.to_internal_metric_type]
  end

  def get_threshold_units_s
    metric_type = self.to_internal_metric_type
    metric_type = :frame_loss_ratio if :frame_loss == metric_type
    return Stats::METRIC_UNITS[metric_type]
  end

  def get_errored_period_max_secs
    get_errored_period_max_errored_count * get_errored_period_delta_time_secs
  end

  def availability_flr_threshold
    return 3.33 # 3.33 %
  end

  def availability_consecutive_count_threshold
    return 2
  end

  def weekly_metric_value
    time_d = Time.at(period_start_epoch)
    period_start = Time.utc(time_d.utc.year, time_d.utc.month, 1)
    matches = MetricsWeeklyCosTestVector.where(
      :grid_circuit_id => grid_circuit_id,
      :metric_type => metric_type,
      :period_start_epoch => period_start.to_i )
    if matches.size > 1
      SW_ERR "ERROR: found more than one match for exception(#{id}) in MetricsWeeklyCosTestVector\n"
    end
    matches.first.try(:metric)
  end
end
