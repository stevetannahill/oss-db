# == Schema Information
#
# Table name: sites
#
#  id                     :integer(4)      not null, primary key
#  name                   :string(255)
#  address                :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  contact_info           :string(255)
#  site_host              :string(255)
#  floor                  :string(255)
#  clli                   :string(255)
#  icsc                   :string(255)
#  network_address        :string(255)
#  broadcast_address      :string(255)
#  host_range             :string(255)
#  hosts_per_subnet       :string(255)
#  notes                  :text
#  node_prefix            :string(255)
#  postal_zip_code        :string(255)
#  emergency_contact_info :string(255)
#  site_access_notes      :text
#  city                   :string(255)
#  state_province         :string(255)
#  site_type              :string(255)
#  latitude               :decimal(9, 6)   default(0.0)
#  longitude              :decimal(9, 6)   default(0.0)
#  type                   :string(255)
#  operator_network_id    :integer(4)
#  country                :string(255)
#  service_id_low         :integer(4)
#  service_id_high        :integer(4)
#  site_layout            :string(255)     default("StandardExchange7750")
#

class Site < ActiveRecord::Base
  include Kpiable
  
  has_many :site_groups_sites
  has_many :site_groups, :through => :site_groups_sites

  has_many :nodes, :order => "name", :dependent => :destroy
  has_many :inventory_items, :dependent => :destroy
  has_many :patch_panels, :order => "name", :dependent => :destroy
  has_many :cabinets, :order => "name", :dependent => :destroy
  has_many :ports, :through => :nodes, :order => "name"
  has_many :uploaded_files, :as => :uploader, :dependent => :destroy
  belongs_to :operator_network
  has_one :service_provider, :through => :operator_network
    
  has_many :demarcs, :dependent => :destroy
  has_many :ennis, :class_name => 'EnniNew'
  has_many :on_net_ovcs, :class_name => 'OnNetOvc', :dependent => :nullify
  has_many :qos_policies, :dependent => :destroy
  has_many :enni_news, :class_name => 'EnniNew'
  has_many :segments

  validates_presence_of :name, :country, :city, :node_prefix, :site_host, :address, :contact_info, :floor, :network_address, :broadcast_address, :host_range, :hosts_per_subnet, :site_type, :operator_network_id
  validates_uniqueness_of :name
  validates_uniqueness_of :network_address, :unless => Proc.new { |site| site.network_address == "TBD" }
  validates_format_of :network_address, :with => /(^$)|(^(\d{1,3}\.){3}(\d{1,3})(\/\d{1,2})$)/, :message => "Should be format a.b.c.d/e", :unless => Proc.new { |site| site.network_address == "TBD" }
  validates_uniqueness_of :broadcast_address, :unless => Proc.new { |site| site.broadcast_address == "TBD" }
  validates_format_of :broadcast_address, :with => /(^$)|(^(\d{1,3}\.){3}(\d{1,3})$)/, :message => "Should be dotted decimal IP", :unless => Proc.new { |site| site.broadcast_address == "TBD" }
  validates_format_of :loopback_address, :with => /(^$)|(^(\d{1,3}\.){3}(\d{1,3})$)/, :message => "Should be dotted decimal IP"
  validates_uniqueness_of :host_range, :unless => Proc.new { |site| site.host_range == "TBD" }
  validates_inclusion_of :site_layout, :in => HwTypes::SITE_LAYOUTS, :message => "invalid, Use: #{HwTypes::SITE_LAYOUTS.join(", ")}"
  
  validate :site_layout_valid?
  validate :valid_site_type?
  validate :valid_operator_network?  
  
  MINI_SITE_LAYOUTS = ["LightSquaredExchange7210_SGW"]
  
  # Determine type of Site based on the site layout
  # TODO: Probably should be attribute instead of regex
  def general_type
    case site_layout
    when /wdc/i
      'wdc'
    when /cdc/i
      'gateway_site'
    when /msc/i
      'agg_site'
    else
      'agg_site'
    end
  end
  
  def enni_count
    return demarcs.size #TODO only count demarks that are ENNIs
  end

  # TODO: I think site.path_count is obsolete now?
  def path_count
    # LESS EFFICIENT: on_net_ovcs.map{|onovc| onovc.paths.map(&:id)}.flatten.uniq.count
    on_net_ovcs.joins(:paths).count(:path_id, :distinct => true)
  end

  # Path counts need to restricted by operator network type
  def path_count_by_operator_network_type(operator_network_types)
    # use diagrams to determine Paths by operator network
    path_count = if operator_network_types.empty?
      ennis.joins(:path_displays)
    else
      ennis.joins(:path_displays => {:path => :operator_network}).where(:operator_networks => {:operator_network_type_id => operator_network_types})
    end
    path_count = path_count.count(:path_id, :distinct => true)
  end

  def cenx_mon_ennis_count
    return demarcs.select{ |d| d.is_connected_to_monitoring_port}.size
  end

  def member_ennis_count
    return demarcs.reject{ |d| d.is_connected_to_monitoring_port}.size
  end

  def on_ovc_count
    return on_net_ovcs.size
  end
  
  def self.find_rows_for_index
    order("name")
  end
  

  def site_configuration

    switch_nodes = []
    nodes.each do |node|
      if (node.is_service_router)
        switch_nodes << node
      end
    end
    
    mon_nodes = []
    nodes.each do |node|
      if (node.is_monitoring_node)
        mon_nodes << node
      end
    end

    one_or_ten = "One"
    
    switch_nodes.each do |node|
      node.ports.each do |port|
        if port.phy_type.include? "10GigE"
          one_or_ten = "Ten"
          break
        end
      end
    end

    #TODO fix DUALN vs DUALP
    return "#{switch_nodes.size > 1 ? "DUALP" : "SINGLE"} #{one_or_ten} #{mon_nodes.size > 0 ? "MON" : "NOT-MON"}"
  end
  
  def is_mini_site?
    return MINI_SITE_LAYOUTS.include? site_layout
  end


  def get_seed_module
    #This function gets the site layout, removes spaces
    # and gets the module of the same type
    type = site_layout.gsub(/ +/, "")
    mod = Kernel.const_get(type)
    return mod
  end

  def site_types
    #returns the possible sites of the seed module
    mod = get_seed_module
    return mod::SITES.keys
  end

  def possible_site_types
    possible = site_types.dup
    possible.delete(site_type)
    return possible
  end


  def site_group_info
    site_groups.collect {|sg| sg.name}.uniq.join(" ;")
  end

  def onovc_count
    return on_net_ovcs.size
  end
  
  def on_info_site
    return operator_network ? operator_network.name : "TBD"
  end
  
  def rename_nodes new_prefix
    return false unless Site.find_by_node_prefix(new_prefix).nil?

    nodes.each do |node|
      node.name = node.name.sub(node_prefix, new_prefix)
      node.save false
    end

    self.node_prefix = new_prefix
    self.save false
    return true
  end
  
  def get_setup
    seeds = get_seed_module
    setup = seeds::SITES[site_type]
    return setup
  end
  
  def can_seed
    setup = get_setup
    #Must have no nodes and have a seed defined for it
    return nodes.empty? && defined?(setup)
  end
  
  def seed
    setup = get_setup
    #Make sure all requirements are meant before seeding.
    unless nodes.empty?
      SW_ERR "EXL: Error #{name} can not seed, already has defined nodes"
      return nil
    end
    unless defined?(setup)
      SW_ERR "EXL: Error #{name} can not seed, #{site_type} seed not found/valid"
      return nil
    end
    unless defined?(setup::PORT_CONNECTIONS) && defined?(setup::NODE_INSTANCE)
      SW_ERR "EXL: Error #{name} can not seed, #{site_type} seed missing headers"
      return nil
    end
    
    seed_nodes
    connect_ports
    seed_ennis
  end
  
  def seed_nodes
    @node_seeds = Hash.new
    setup = get_setup
    #for each item in the seeds
    setup::NODE_INSTANCE.each{|name, node_instance|
      #create the associated node
      node = Node.new
      node.node_type = node_instance[:node][:type]
      node.site = self
      node.set_defaults node_instance
      info = node_instance[:node][:nm_connection]
      node.network_manager = NetworkManager.first(:conditions => ["name = ? and nm_type = ?", info[:name], info[:nm_type]]) unless info.nil?
      SW_ERR("Could not create Node: #{node.name} #{node.primary_ip} " + node.errors.full_messages.join(", ")) unless node.save
      node.seed_ports node_instance
      node.save
      node.reload
      #Store the node according to it's instance for easy retrieval later
      @node_seeds[node_instance] = node
    }
    nodes(true)
  end
  
  def connect_ports
    setup = get_setup
    setup::PORT_CONNECTIONS.each do |connection|
      port1 = port_from_array connection[0], connection[1]
      port2 = port_from_array connection[1], connection[0]

      SW_ERR("Could not find Port1: #{connection[0][1]} on #{(@node_seeds[connection[0][0]]).name}") unless port1
      SW_ERR("Could not find Port2: #{connection[1][1]} on #{(@node_seeds[connection[1][0]]).name}") unless port2

      #puts "******** Connect #{self.name}: #{port1.name} to #{port2.name}"

      port1.connected_port = port2
      port2.connected_port = port1
      SW_ERR("Could not save Port: #{port1.name} " + port1.errors.full_messages.join(", ")) unless port1.save
      SW_ERR("Could not save Port: #{port2.name} " + port2.errors.full_messages.join(", ")) unless port2.save
    end
  end

  def port_from_array array, connection
    #Get the actual node by it's instance from the array
    node = @node_seeds[array[0]]

    unless node
      SW_ERR ("Could not find Node for #{array} to connect to #{connection}")
      return nil
    end

    #Find the first port that matches the name in the array
    return node.ports.first(:conditions => ['name = ?', array[1]])
  end
  
  
  def seed_ennis
    setup = get_setup
    #for every node we've seeded
    @node_seeds.each do |instance, node|
      #If this node doesn't connect to an ENNI skip it
      next if instance[:enni_connections].nil?
      instance[:enni_connections].each do |connection|       
        #seperate the enni attrs from the connected ports
        attrs = connection.dup
        port_info = attrs.delete(:port_connections)
        member_handle = attrs.delete(:member_handle)
        #create a new enni on the site
        enni = EnniNew.new(attrs)
        enni.site = self
        enni.operator_network = operator_network
        enni.cenx_demarc_type = "ENNI"
        enni.demarc_icon = "enni" unless attrs[:demarc_icon]
        SW_ERR("Could not create EnniNew: " + enni.errors.full_messages.join(", ")) unless enni.save
        
        #for each port connected to the enni
        port_info.each do |port_name|
          port = node.ports.find_by_name(port_name)
          unless port
            SW_ERR("Could not find Port: #{port_name}")
            break
          end
          port.demarc = enni
          SW_ERR("Could not save Port: " + port.errors.full_messages.join(", ")) unless port.save
          port.reload
        end

        SW_ERR("Could not save ENNI: " + enni.errors.full_messages.join(", ")) unless enni.save
        enni.reload

        #Supply a member_handle to the ENNI
        owner = operator_network.service_provider
        handle = enni.member_handle_instance_by_owner(owner)
        SW_ERR("Could not find member handle for #{owner.name} on #{enni.name}") if handle.nil?
        handle.value = member_handle unless member_handle.nil?
        SW_ERR("Could not save member handle #{member_handle} for #{owner.name} on #{enni.name}") unless handle.save
      end
    end
    return nil
  end

  def seed_additional information
    information.each do |name, info |
      instance = get_setup::NODE_INSTANCE[name]
      node = get_node_from_instance instance
      node.seed_additional info unless node.nil?
    end
  end

  def get_node_from_instance instance
    results = nodes.select do |node|
      node.of_instance instance
    end
    SW_ERR("EXL: #{self.name} - ERROR can not find node of instance #{instance.inspect}") if results.empty?
    return results.first
  end
  
  def generate_connection_info
    priority = (["7750-sr12", "Terminal-Server-2511", "cisco-3750"] + HwTypes::NODE_TYPES.map{|key, value| value}).uniq
    sorted_nodes = nodes.sort{|nodea, nodeb| priority.index(nodea.node_type) <=> priority.index(nodeb.node_type)}
    links = []
    sorted_nodes.each{|node|
      node.ports.each{|port|
        if port.is_connected_to_port
          if sorted_nodes.index(port.connected_port.node).nil? || sorted_nodes.index(node) < sorted_nodes.index(port.connected_port.node) || node == port.connected_port.node && node.ports.index(port) <= node.ports.index(port.connected_port)
            links.push([port, port.connected_port])
          end
        end
      }
    }
    text = ""
    text += "#--------------------------------" + "-"*name.length + "-#\n"
    text += "# Port connections for Site: #{name} #\n"
    text += "#--------------------------------" + "-"*name.length + "-#\n\n"
    links.each{|porta, portb|
      text += ((porta.id == portb.id)? "Port: #{porta.name} on #{porta.node.name} (#{porta.node.node_type})" : "#{portb.port_connection_info_port}") + " => #{porta.port_connection_info_port}\n"
    }
    return text
  end
  

  # KPI methods
  def update_kpi(parent, parent_kpi, kpi_name, kpi_object)    
    if parent_kpi == nil
      case kpi_object
      when EnniNew.name
        ennis.collect {|enni| enni.operator_network}.compact.uniq.each do |on|
          on_kpi = on.get_kpi(kpi_name, kpi_object, on.service_provider)
          site_update_kpi(on, on_kpi, kpi_name, kpi_object)
        end
      when OnNetOvcEndPointEnni.name
        on = operator_network
        on_kpi = on.get_kpi(kpi_name, kpi_object, on.service_provider)
        site_update_kpi(on, on_kpi, kpi_name, kpi_object)
      else
        SW_ERR "Invalid kpi_object (#{kpi_object}) for #{self.class}:#{self.id}"
      end
    else
      site_update_kpi(parent, parent_kpi, kpi_name, kpi_object)
    end
  end
   
  private
  
  def site_update_kpi(parent, parent_kpi, kpi_name, kpi_object)
    kpi = calculate_kpi(kpi_name, kpi_object, parent_kpi, parent)
    case kpi_object
    when EnniNew.name
      ennis.where(:operator_network_id => parent.id).each do |enni|         
        enni.update_kpi(self, kpi, kpi_name, kpi_object)
      end
      
    when OnNetOvcEndPointEnni.name
      segments.each do |segment|
        segment.update_kpi(self, kpi, kpi_name, kpi_object)
      end
    else
      SW_ERR "Invalid kpi_object (#{kpi_object}) for #{self.class}:#{self.id}"
    end
  end

  def valid_site_type?
    return possible_site_types.include? site_type
  end

  def valid_operator_network?
    puts "ON: #{operator_network}, versus ON_id: #{operator_network_id}"
    network_type = operator_network.operator_network_type.name
    unless HwTypes::VALID_SITE_LAYOUT[site_layout].include? network_type
      errors.add(:base,"#{operator_network.name} network of type #{network_type} is incompatible with site layout of type #{site_layout} network, Operator Network selection")
    end
  end
  
  def site_layout_valid?
    if operator_network
      network_type = operator_network.operator_network_type.name
      if HwTypes::VALID_SITE_LAYOUT[site_layout].nil?
        errors.add(:base, "#{site_layout} is not in the list of valid site layouts")
        return false
      end

      return true if HwTypes::VALID_SITE_LAYOUT[site_layout].include? network_type

      errors.add(:base,"#{operator_network.name} network of type #{network_type} is incompatible with site layout of type #{site_layout} network, Site Type selection")
    end
  end
  

end
