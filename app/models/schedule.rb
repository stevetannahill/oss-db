
class Schedule< ActiveRecord::Base
  after_save :update_crontab
  before_create :general_before_create  
  before_destroy :general_before_destroy
  
  belongs_to :scheduled_task
  has_many :reports, :order => "created_at DESC", :dependent => :destroy
  store :formats
  
  validates_uniqueness_of :name
  validates :name, :retention_time, :scheduled_task, :trigger, :timezone, :presence => true
  validates_associated :scheduled_task
  validates :trigger, :inclusion => { :in => %w(daily weekly monthly yearly), :message => "value is not supported"}
  validate :validate_email_options

  CRONTAB_PREFIX = "ScheduledReport_"
  CRON_LOG_DIR = "#{Rails.root}/log/cron/"
  
  def self.for_user(user)
    self.includes({:scheduled_task => :user}, :reports).where('scheduled_tasks.user_id = ?', user.id)
  end
  
  def general_before_create
    if formats.blank?
      self.formats = scheduled_task.supported_attachemts
    end
    true
  end
  
  def general_before_destroy
    CronEdit::Crontab.Remove(CRONTAB_PREFIX + self.name)
    true
  end
  
  def update_reports
    reports.each {|rep| rep.update_report}
    true
  end
  
  def formats_sentence
    self.formats.map { |k,v| 
      if v 
        if k.length > 3 
          k.titleize 
        else
          k.upcase
        end
      end
    }.compact.to_sentence
  end

  def self.remove_old_reports
    Schedule.all.each do |sr|
      # remove any old records
      if sr.retention_time != 0
        # Don't delete the last one as we want to guarantee there will always be history for the retention time as it's used to calculate
        # availability so a record is needed that is at least retention_time old
        reports = sr.reports.where("run_date < ?", (Time.now-sr.retention_time.days))
        reports.each {|report| report.destroy}
      end
    end
  end
  
  # If databases are deleted/added then the crontab won't match so need to resync crontab with the database
  def self.resync_crontab
    current_crontab = CronEdit::Crontab.List.keys
    # Remove any that don't have a key starting with CRONTAB_PREFIX - This is because "whenever" gem uses ##__nnnn__## as a name so Crontab.List returns
    # those entries as well
    current_crontab.delete_if {|name| name[/^#{CRONTAB_PREFIX}/] == nil}
    database_crontab = Schedule.pluck(:name)
    # Add the prefix to the names in the database
    database_crontab.collect! {|name| CRONTAB_PREFIX + name}
    to_delete = current_crontab-database_crontab
    
    cm = CronEdit::Crontab.new 
    to_delete.each {|name| cm.remove(name)}  
    Schedule.find_each {|sr| sr.make_crontab(cm)}      
    cm.commit
  end

  
  def run_now
    system(make_cmd)
  end

  def update_crontab    
    if enabled_changed? || execution_time_changed? || trigger_changed? || timezone_changed? || execution_time.utc < Time.now.utc
      cm = CronEdit::Crontab.new 
      make_crontab(cm)
      cm.commit
    end
    return true
  end
  
  def make_crontab(cron_list)
    # '07 14 15 3 3 /bin/bash -l -c \'cd /Users/bwessels/oss-db_kpi/ && bundle exec rails runner "File.open(\"/Users/bwessels/bjw_test1.txt\", \"w+\")"\''
    cron_entry = format_crontab_time + " " + make_cmd 
    if enabled
      cron_list.add(CRONTAB_PREFIX + name, cron_entry)
    else
      cron_list.remove(CRONTAB_PREFIX + name)
    end
    return cron_list
  end
  
  private
  
  def enabled_or_execution_time_changed
    enabled || execution_time
  end
    
  def validate_email_options
    if email_distribution
      if [email_to.blank?, email_cc.blank?, email_bcc.blank?].all?
        errors.add(:base, "There must be at least one email address provided")
      end
    end
  end

  def format_crontab_time
    # crontab format: Minute Hour Day_of_Month Month Day_of_Week
    utc_execution_time = execution_time.utc 
    if utc_execution_time > Time.now.utc
      # If the execution time is in the future set the first cron run time to be at that absolute time
      # After the first run the crontab entry will be updated to be periodic based on the trigger
      return "#{utc_execution_time.min} #{utc_execution_time.hour} #{utc_execution_time.day} #{utc_execution_time.month} *"
    end
    
    # Calculate the dst offset between now and the configured execution time then adjust the time
    dst_offset = execution_time.in_time_zone(timezone).utc_offset - Time.now.in_time_zone(timezone).utc_offset
    utc_execution_time = execution_time.utc + dst_offset
    min = utc_execution_time.min
    hour = utc_execution_time.hour
    
    case trigger
    when "monthly"
      "#{min} #{hour} #{utc_execution_time.day} * *"
    when "weekly"
      "#{min} #{hour} * * #{utc_execution_time.wday}"
    when "daily"
      "#{min} #{hour} * * *"
    when "yearly"
      "#{min} #{hour} #{utc_execution_time.day} #{utc_execution_time.month} *"
    else
      SW_ERR "Invalid trigger #{trigger}"
      nil
    end
  end
  
  def self.preamble_cron_script(sched_id)
    sched = Schedule.find(sched_id)
    Time.zone = sched.timezone
    result = "Schedule Time: #{Time.now.in_time_zone(sched.timezone)} #{sched.timezone} #{Time.zone}"
    puts result
    sched.update_crontab
  end
  
  def make_cmd
    rails_cmd = "\"Schedule.preamble_cron_script(#{self.id});" + scheduled_task.executable_script.gsub(/%SR_ID%/,self.id.to_s) + "\""
    if Rails.env == 'production'
      cmd = 'nice -n 15 /bin/bash -l -c ' + "\'export TZ=\"#{timezone}\" && #{scheduled_task.get_env_script} bundle exec rails runner #{rails_cmd}\'"
    else
      Dir::mkdir(CRON_LOG_DIR) unless File.exists?(CRON_LOG_DIR)
      cron_log_file = CRON_LOG_DIR + "#{name}.log".gsub(/ /, "_")
      cmd = 'nice -n 15 /bin/bash -l -c ' + "\'export TZ=\"#{timezone}\" && cd #{Rails.root} && bundle exec rails runner #{rails_cmd} >> #{cron_log_file} 2>&1\'"
    end
    return cmd
  end
    
end
# == Schema Information
#
# Table name: schedules
#
#  id                 :integer(4)      not null, primary key
#  name               :string(255)     not null
#  scheduled_task_id  :integer(4)
#  exclusion_list_id  :integer(4)
#  enabled            :boolean(1)
#  trigger            :string(255)
#  formats            :text
#  last_run           :datetime
#  retention_time     :integer(4)
#  execution_time     :datetime
#  email_distribution :boolean(1)
#  email_to           :string(255)
#  email_cc           :string(255)
#  email_bcc          :string(255)
#  email_subject      :string(255)
#  email_body         :string(255)
#  created_at         :datetime        not null
#  updated_at         :datetime        not null
#  timezone           :string(255)
#

