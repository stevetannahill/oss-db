# == Schema Information
# Schema version: 20110802190849
#
# Table name: sin_enni_service_provider_sphinxes
#
#  id                  :integer(4)      not null, primary key
#  enni_id             :integer(4)
#  service_provider_id :integer(4)
#  created_at          :datetime
#  updated_at          :datetime
#  delta               :boolean(1)      default(TRUE), not null
#

class SinEnniServiceProviderSphinx < ActiveRecord::Base
  
  # include multiple models to return a more complete Enni from sphinx
  belongs_to :enni, :class_name => 'EnniNew', :include => [{:operator_network => :service_provider}]
  belongs_to :service_provider
  
  scope :with_provider, lambda { |service_provider|
    { :conditions => { :service_provider_id => service_provider } }
  }

  # Return a scoped sphinx class based on user
  def self.by_user(user)
    # CENX Administrator can access all ENNIs 
    enni_sphinx = user.system_admin? ? self : self.by_service_provider(user.service_provider.id)
    # Use operator_network_type to determine if user can see enni or not
    # User has a network_type then they can only see circuits with that network type (oem)
    # User does not have a network type then they see all circuits
    user_operator_network_types = user.operator_network_types
    unless user_operator_network_types.empty?
      enni_sphinx = enni_sphinx.by_operator_network_type(user_operator_network_types.map(&:id))
    end
    enni_sphinx
  end
    
  ## Experimenting with Arel
  def self.arel_engine
    @arel_engine ||= Arel::Sql::Engine.new(self)
  end
  
  # Class methods and their respective table name
  AREL_TABLES = {
                  :mai => :member_attr_instances,
                  :enni => :demarcs,
                  :site => :sites,
                  :demarc => :demarcs,
                  :sesps => :sin_enni_service_provider_sphinxes,
                  :network => :operator_networks,
                  :network_type => :operator_network_types,
                  :port => :ports,
                  :node => :nodes,
                  :provider => :service_providers,
                  :segment => :segments,
                  :stateful_ownership => :stateful_ownerships,
                  :stateful => :statefuls,
                  :state => :states,
                  :paths_segment => :paths_segments,
                  :service_order => :service_orders,
                  :sm_ownership => :sm_ownerships,
                  :event_history => :event_histories,
                  :event_record => :event_records
                }
  
  def self.method_missing(method_sym,*args)
    if AREL_TABLES[method_sym]
      @arel_tables ||= {}
      @arel_tables[method_sym] ||= Arel::Table.new(AREL_TABLES[method_sym],arel_engine)
    elsif method_sym.to_s =~ /_sql$/ and respond_to?(sqlless_name = method_sym.to_s.gsub(/_sql$/,""))
      send(sqlless_name,*args).to_sql
    else
      super
    end
  end
  
  # HELPERS
  def self.enni_base
    demarc.
      where(demarc[:id].eq(sesps[:enni_id]))
  end

  def self.network_base
    enni_base.
      join(site).on(site[:id].eq(demarc[:site_id])).
      join(network).on(network[:id].eq(site[:operator_network_id]))
  end

  # HACK: remove ennis with demarc_icon of router from list results
  def self.enni_list_base
    enni_base.
      where(demarc[:type].eq('EnniNew'))
  end

  # SQL Methods

  def self.list_ennis
    enni_list_base.
      project(true)
  end

  def self.monitoring_nis
    connected_ports_port = port.alias('connected_ports_port')
    enni_list_base.
      join(port).on(port[:demarc_id].eq(demarc[:id])).
      join(connected_ports_port).on(connected_ports_port[:id].eq(port[:connected_port_id])).
      join(node).on(node[:id].eq(connected_ports_port[:node_id])).
      where(node[:node_type].in(Node::MONITORING_TYPES)).  
      project(true)
  end
  
  def self.operator_network_type_ids
    enni_network = network.alias('enni_network')
    network_base.
      join(enni_network).on(enni_network[:id].eq(demarc[:operator_network_id])).
      project("CONCAT(operator_networks.operator_network_type_id, ' ', enni_network.operator_network_type_id)")
  end

  # Get site operator network name and operator network type name at same time
  def self.site_operator_network
    network_base.
      join(network_type).on(network_type[:id].eq(network[:operator_network_type_id])).
      project("CONCAT(operator_network_types.name, ' ', operator_networks.name)")
  end

  def self.enni_member_handle
    mai.
      project(mai[:value]).
      where(mai[:type].eq('MemberHandleInstance')).
      where(mai[:owner_type].eq('ServiceProvider')).
      where(mai[:owner_id].eq(sesps[:service_provider_id])).
      where(mai[:affected_entity_type].eq('Demarc')).
      where(mai[:affected_entity_id].eq(sesps[:enni_id])).
      take(1)
  end
  
  define_index do

    set_property :group_concat_max_len => 8192 # Will work until we GROUP_CONCAT more then ~31 member handles or varchar(255) 

    # Sortable indexes
    indexes enni.cenx_id, :as => :cenx_id, :sortable => true
    indexes enni.operator_network.service_provider.name, :as => :enni_service_provider_name,  :sortable => true
    indexes enni.prov_name, :type => :integer, :as => :provisioning_state_text, :sortable => true
    indexes enni.order_name, :type => :integer, :as => :ordering_state_text, :sortable => true
    indexes enni.sm_state, :type => :integer, :as => :monitoring_state_text, :sortable => true
    indexes "(#{SinEnniServiceProviderSphinx::enni_member_handle_sql})", :as => :enni_member_handle, :sortable => true

    # Non Sortable indexes
    indexes enni.operator_network.name, :as => :enni_operator_network_name
    indexes enni.operator_network.operator_network_type.name, :as => :enni_operator_network_type_name
    indexes enni.site.name, :as => :site_name
    indexes "(#{SinEnniServiceProviderSphinx::site_operator_network_sql})", :as => :site_operator_network
    
    # Attributes
    has :enni_id, :type => :integer, :as => :enni_id
    has :service_provider_id, :type => :integer, :as => :current_service_provider_id
    has enni.site_id, :type => :integer, :as => :enni_site_id
    has enni.operator_network.service_provider_id, :type => :integer, :as => :enni_service_provider_id
    has enni.operator_network.operator_network_type_id, :type => :integer, :as => :oem_id
    has "(#{SinEnniServiceProviderSphinx::operator_network_type_ids_sql})", :type=>:multi, :as => :operator_network_type_ids
    has "(#{SinEnniServiceProviderSphinx::list_ennis_sql})", :type => :boolean, :as => :list_ennis
    has "(#{SinEnniServiceProviderSphinx::monitoring_nis_sql})", :type => :boolean, :as => :monitoring_nis
    has "CRC32(demarcs.prov_name)", :type => :integer, :as => :provisioning_state
    has "CRC32(demarcs.order_name)", :type => :integer, :as => :ordering_state
    has "CRC32(demarcs.sm_state)", :type => :integer, :as => :monitoring_state
    has "CRC32(demarcs.demarc_icon)", :type => :integer, :as => :demarc_icon

  end

  sphinx_scope(:by_service_provider) { |service_provider_id|
    {:with => {:current_service_provider_id => service_provider_id}}
  }

  sphinx_scope(:by_operator_network_type) { |operator_network_type_ids|
    {:with => {:operator_network_type_ids => operator_network_type_ids}}
  }
  
end
