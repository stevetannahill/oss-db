# == Schema Information
#
# Table name: segment_end_points
#
#  id                    :integer(4)      not null, primary key
#  type                  :string(255)
#  cenx_id               :string(255)
#  segment_id            :integer(4)
#  demarc_id             :integer(4)
#  segment_end_point_type_id :integer(4)
#  md_format             :string(255)
#  md_level              :string(255)
#  md_name_ieee          :string(255)
#  ma_format             :string(255)
#  ma_name               :string(255)
#  is_monitored          :boolean(1)
#  notes                 :string(255)
#  stags                 :string(255)
#  ctags                 :string(255)
#  oper_state            :string(255)
#  created_at            :datetime
#  updated_at            :datetime
#  eth_cfm_configured    :boolean(1)      default(TRUE)
#  reflection_enabled    :boolean(1)
#  mep_id                :string(255)     default("TBD")
#  event_record_id       :integer(4)
#  sm_state              :string(255)
#  sm_details            :string(255)
#  sm_timestamp          :integer(8)
#  prov_name             :string(255)
#  prov_notes            :text
#  prov_timestamp        :integer(8)
#  order_name            :string(255)
#  order_notes           :text
#  order_timestamp       :integer(8)
#


class OvcEndPointUni < OvcEndPoint
  validates_class_of :segment, :is_a => "OffNetOvc"
  validates_class_of :demarc, :not_a => "EnniNew", :allow_nil => true
  
  validate :valid_tag_presence?

  validates_format_of :segment_endpoint_type_name, :with => /(^UNI.*)/, :message => "Segment UNI Endpoint Type must be a UNI Endpoint"
  
  def is_connected_to_uni
    return  demarc_id != nil
  end
  
  def get_candidate_segment_end_point_types
    return ethernet_service_type ? ethernet_service_type.segment_end_point_types.select{|t| t.name =~ /^UNI/} : []
  end

  def class_get_candidate_endpoints_to_link_with
    base_get_candidate_endpoints_to_link_with.select{|e| e.is_a? OvcEndPointUni }
  end

  def get_candidate_demarcs
    candidate_demarcs = segment.reachable_operator_networks.map{|on| on.demarcs}.flatten.uniq
    candidate_demarcs.reject{|d| ((segment.is_connected_to_demarc d) || (d.kind_of? EnniNew))}
  end

#  def get_candidate_demarcs
#     candidate_demarcs = segment.get_operator_network.demarcs.reject{
#       |d| ((segment.is_connected_to_demarc d) || (d.kind_of? EnniNew))}
#  end

  # Removed by Corey for DUAL EVC purposes
 # def valid_uni_loopback? mla = nil
 #   mla = mla ? mla : (demarc ? demarc.mon_loopback_address : nil)
 #   if demarc && demarc.cenx_demarc_type == "UNI" && mla && mla.match(/^(([A-Fa-f0-9]{2}):){5}([A-Fa-f0-9]{2})$/)
 #     segmenteps = segment.segment_end_points
 #     segmenteps.each{|segmentep|
 #       if (segmentep != self && (segmentep.is_a? OvcEndPointUni) && segmentep.demarc && segmentep.demarc.mon_loopback_address == mla)
 #         errors.add(:demarc, "- UNI's monitored loopback address is already taken within Segment: #{segment.cenx_name}.")
 #         return false
 #       end
 #     }
 #   end
 #   return true
 # end
  
  def outer_tag_value
    ctags
  end

  def get_alarm_keys
    # Get the MEP alarm from the OnNetOvc connected to this endpoint
    meg_level = [md_level.to_i]
    new_mep_alarms = []
    return_code = true
  
    if cos_end_points.size != 0
      cos_end_points_to_cenx = cos_end_points.first.get_remote_sibling_cos_end_points
      if !cos_end_points_to_cenx.empty?
        cenx_ep = cos_end_points_to_cenx.first.segment_end_point
        if cenx_ep != nil
          ip_ping_monitoring_is_required, ccm_monitoring_is_required, lbm_monitoring_is_required, dmm_monitoring_is_required, monitored_remote_endpoint = cenx_ep.segment.get_service_monitoring_params
          if ccm_monitoring_is_required && monitored_remote_endpoint == self
            return_code, new_mep_alarms = cenx_ep.segment.get_mep_alarm_keys(meg_level)          
            if !return_code
              SW_ERR "#{self.class}:#{self.id} Failed to get MEP alarms from On Net OVC(#{cenx_ep.segment.class}:#{cenx_ep.segment.id})"
            elsif new_mep_alarms.empty?
              SW_ERR "#{self.class}:#{self.id} Expected to get MEP alarms from On Net OVC(#{cenx_ep.segment.class}:#{cenx_ep.segment.id}) but got none"
              return_code = false
            end
          end
        else
          return_code = false
          SW_ERR "Cannot find connected Cenx Endpoint for #{self.class}:#{self.id}"
        end
      else
        SW_ERR "#{self.class}:#{self.id} no remote sibling cos endpoints found (#{cos_end_points.inspect})"
        return_code = false
      end
    end
    return return_code, new_mep_alarms
  end
  
  def go_in_service
    return_code = super
    
    if return_code[:result]
      return_code[:result], new_mep_alarms = get_alarm_keys
      if return_code[:result]          
        mep_mapping = AlarmSeverity.default_alu_alarm_mapping
        mep_mapping["major"] = AlarmSeverity::FAILED
        new_alarms = {} 
        new_mep_alarms.each {|filter| new_alarms[filter] = {:type => AlarmHistory, :mapping => mep_mapping}}
        return_code[:result] = update_alarm_history_set(new_alarms)
      end
    end
    return_code[:info] = "Failed to initialise" unless return_code
    return return_code
  end
  
  private

  def valid_tag_presence?
   unless (not ctags.to_s.empty?) and (stags.nil? or stags.to_s.empty?)
     errors.add(:base,"ctags must be valid and stags must be empty for Segment Endpoint UNIs")
   end
  end

end
