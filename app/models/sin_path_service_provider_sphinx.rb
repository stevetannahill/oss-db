# == Schema Information
#
# Table name: sin_path_service_provider_sphinxes
#
#  id                  :integer(4)      not null, primary key
#  path_id             :integer(4)
#  service_provider_id :integer(4)
#  created_at          :datetime
#  updated_at          :datetime
#  delta               :boolean(1)      default(TRUE), not null
#  member_handle       :string(255)
#

class SinPathServiceProviderSphinx < ActiveRecord::Base
  
  # include multiple models to return a more complete Path from sphinx
  belongs_to :path
  belongs_to :service_provider
  
  has_many :segment_displays, :class_name => 'PathDisplay', :foreign_key => :path_id, :primary_key => :path_id, :conditions => {:entity_type => ["Segment","OnNetOvc","OffNetOvc"]}
  has_many :bulk_service_orders, :class_name => 'BulkOrder', :foreign_key => :ordered_entity_id, :primary_key => :path_id, 
                         :conditions => {:ordered_entity_type => 'Path', :type => 'BulkOrder', :bulk_order_type => 'Circuit Order'}
  
  scope :with_provider, lambda { |service_provider|
        { :conditions => { :service_provider_id => service_provider } }
  }

  before_save :denormalize_member_handle

  # Return a scoped sphinx class based on user
  def self.by_user(user)
    # CENX Administrator can access all Paths 
    path_sphinx = user.system_admin? ? self : self.by_service_provider(user.service_provider.id)
    # Use operator_network_type to determine if user can see path or not
    # User has a network_type then they can only see circuits with that network type (oem)
    # User does not have a network type then they see all circuits
    unless App.is?(:coresite)
      user_operator_network_types = user.operator_network_types
      unless user_operator_network_types.empty?
        path_sphinx = path_sphinx.by_operator_network_type(user_operator_network_types.map(&:id))
      end
    end
    path_sphinx
  end

  def denormalize_member_handle
    # Update the member handle (denormalized) 
    self.member_handle = path.member_handles.where(:owner_id => service_provider_id).first.try(:value)
  end  

  ## Experimenting with Arel
  def self.arel_engine
    @arel_engine ||= Arel::Sql::Engine.new(self)
  end
  
  
  # Class methods and their respective table name
  AREL_TABLES = {
                  :mai => :member_attr_instances,
                  :path => :paths,
                  :pop => :pops,
                  :demarc => :demarcs,
                  :sesps => :sin_path_service_provider_sphinxes,
                  :network => :operator_networks,
                  :provider => :service_providers,
                  :segment => :segments,
                  :path_display => :path_displays,
                  :segment_end_point => :segment_end_points,
                  :cos_end_point => :cos_end_points,
                  :cos_test_vector => :cos_test_vectors,
                  :stateful_ownership => :stateful_ownerships,
                  :stateful => :statefuls,
                  :state => :states,
                  :paths_segment => :paths_segments,
                  :service_order => :service_orders,
                  :bulk_orders_service_order => :bulk_orders_service_orders,
                  :sm_ownership => :sm_ownerships,
                  :event_history => :event_histories,
                  :event_record => :event_records
                }
  
  def self.method_missing(method_sym,*args)
    if AREL_TABLES[method_sym]
      @arel_tables ||= {}
      @arel_tables[method_sym] ||= Arel::Table.new(AREL_TABLES[method_sym],arel_engine)
    elsif method_sym.to_s =~ /_sql$/ and respond_to?(sqlless_name = method_sym.to_s.gsub(/_sql$/,""))
      send(sqlless_name,*args).to_sql
    else
      super
    end
  end

  #HELPERS
  def self.path_display_base
    path_display.
      where(path_display[:service_provider_id].eq(sesps[:service_provider_id])).
      where(path_display[:path_id].eq(sesps[:path_id]))
  end
  
  def self.path_display_base_redacted
    path_display_base.
      where(path_display[:redacted].eq(0))
  end
  
  #SQL Methods
  def self.path_member_handle
    mai.
      project(mai[:value]).
      where(mai[:type].eq('MemberHandleInstance')).
      where(mai[:owner_type].eq('ServiceProvider')).
      where(mai[:owner_id].eq(sesps[:service_provider_id])).
      where(mai[:affected_entity_type].eq('Path')).
      where(mai[:affected_entity_id].eq(sesps[:path_id])).
      take(1)
  end
  
  def self.display_member_handle_group(entity_type)
    path_display_base_redacted.
      where(path_display[:entity_type].eq(entity_type)).
      join(mai).on(
        mai[:type].eq('MemberHandleInstance'),
        mai[:owner_type].eq('ServiceProvider'),
        mai[:affected_entity_type].eq(path_display[:entity_type]),
        mai[:affected_entity_id].eq(path_display[:entity_id])).
      where(mai[:owner_id].eq(sesps[:service_provider_id])).
      project("GROUP_CONCAT(member_attr_instances.value SEPARATOR ' ')")
  end

  def self.demarc_cenx_id_group
    path_display_base_redacted.
      where(path_display[:entity_type].eq('Demarc')).
      join(demarc).on(demarc[:id].eq(path_display[:entity_id])).
      project("GROUP_CONCAT(demarcs.cenx_id SEPARATOR ' ')")
  end
  
  def self.ovc_cenx_id_group
    path_display_base_redacted.
      where(path_display[:entity_type].eq('Segment')).
      join(segment).on(segment[:id].eq(path_display[:entity_id])).
      project("GROUP_CONCAT(segments.cenx_id SEPARATOR ' ')")
  end
  
  def self.path_order_cenx_id_group
    service_order.
      where(service_order[:ordered_entity_type].eq('Path')).
      where(service_order[:ordered_entity_id].eq(sesps[:path_id])).
      project("GROUP_CONCAT(service_orders.cenx_id SEPARATOR ' ')")
  end
  
  def self.path_compontents_order_cenx_id_group
    path_display_base_redacted.
      join(service_order).on(service_order[:ordered_entity_id].eq(path_display[:entity_id])).
      where(service_order[:ordered_entity_type].eq(path_display[:entity_type])).
      project("GROUP_CONCAT(service_orders.cenx_id SEPARATOR ' ')")
  end

  def self.path_cos_test_vector_test_id_group
    path_display_base_redacted.
      where(path_display[:entity_type].eq('Segment')).
      join(segment_end_point).on(segment_end_point[:segment_id].eq(path_display[:entity_id])).
      join(cos_end_point).on(cos_end_point[:segment_end_point_id].eq(segment_end_point[:id])).
      join(cos_test_vector).on(cos_test_vector[:cos_end_point_id].eq(cos_end_point[:id])).
      project("GROUP_CONCAT(cos_test_vectors.circuit_id SEPARATOR ' ')")
  end

  def self.enni_id_group
    path_display_base_redacted.
      where(path_display[:entity_type].eq('Demarc')).
      join(demarc).on(demarc[:id].eq(path_display[:entity_id]).and(demarc[:type].eq('EnniNew'))).
      project("GROUP_CONCAT(demarcs.id SEPARATOR ' ')")
  end

  def self.has_path_id_group
    path_display_base_redacted.
      where(path_display[:entity_type].eq('Segment')).
      join(paths_segment).on(paths_segment[:segment_id].eq(path_display[:entity_id])).
      where(paths_segment[:path_id].not_eq(path_display[:path_id])).
      join(path).on(path[:id].eq(paths_segment[:path_id]).and(path[:cenx_path_type].in(HwTypes::CENX_Path_TUNNEL_TYPES))).
      project("GROUP_CONCAT(paths_segments.path_id SEPARATOR ' ')")
  end
  
  define_index do
    set_property :group_concat_max_len => 8192 # Will work until we GROUP_CONCAT more then ~31 member handles or varchar(255) 
    
    # Member Handles
    indexes "(#{SinPathServiceProviderSphinx::path_member_handle_sql})", :as=>:sp_name, :sortable => true
    indexes "(#{SinPathServiceProviderSphinx::display_member_handle_group_sql('Demarc')})", :as=> "demarc_member_handle"
    indexes "(#{SinPathServiceProviderSphinx::display_member_handle_group_sql('Segment')})", :as=> "ovc_member_handle"

    # CENX IDs
    indexes path.cenx_id, :as => :cenx_id
    indexes "(#{SinPathServiceProviderSphinx::demarc_cenx_id_group_sql})", :as => "demarc_cenx_id"
    indexes "(#{SinPathServiceProviderSphinx::ovc_cenx_id_group_sql})", :as => "ovc_cenx_id"
    indexes "(#{SinPathServiceProviderSphinx::path_order_cenx_id_group_sql})", :as => "path_order_cenx_id"
    indexes "(#{SinPathServiceProviderSphinx::path_compontents_order_cenx_id_group_sql})", :as => "path_compontents_order_cenx_id"

    # CTV test IDs
    indexes "(#{SinPathServiceProviderSphinx::path_cos_test_vector_test_id_group_sql})", :as => "path_cos_test_vector_test_id"


    # Attributes
    has :path_id, :type=>:integer, :as => :path_id
    has :service_provider_id, :type => :integer, :as => :current_service_provider_id
    has path.cenx_id, :type => :integer, :as => :cenx_id_sort
    has path.operator_network.operator_network_type_id, :type => :integer, :as => :path_operator_network_type_id
    has "(#{SinPathServiceProviderSphinx::enni_id_group_sql})", :type=>:multi, :as => 'enni_ids'
    has "(#{SinPathServiceProviderSphinx::has_path_id_group_sql})", :type=>:multi, :as => 'has_path_ids'

  end

  sphinx_scope(:by_service_provider) { |service_provider_id|
    {:with => {:current_service_provider_id => service_provider_id}}
  }

  sphinx_scope(:by_operator_network_type) { |operator_network_type_ids|
    {:with => {:path_operator_network_type_id => operator_network_type_ids}}
  }
  
end
