# == Schema Information
# Schema version: 20110802190849
#
# Table name: event_histories
#
#  id             :integer(4)      not null, primary key
#  event_filter   :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  alarm_mapping  :text
#  retention_time :integer(4)
#


class MtcHistory < AlarmHistory
  has_many :mtc_ownerships, :dependent => :destroy
   
  # Class method to lookup the event and then add the record
  def self.process_event(event)
    return 
  end

  def alarm_sync(timestamp = nil)
    SW_ERR "Not supported for #{self.class}"
    return true
  end
      
        
end
