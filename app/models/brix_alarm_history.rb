# == Schema Information
# Schema version: 20110802190849
#
# Table name: event_histories
#
#  id             :integer(4)      not null, primary key
#  event_filter   :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  alarm_mapping  :text
#  retention_time :integer(4)
#

class BrixAlarmHistory < AlarmHistory
  
  def self.default_brix_alarm_mapping
    return {"cleared"     => AlarmSeverity::OK,
            "warning"     => AlarmSeverity::WARNING,
            "failed"      => AlarmSeverity::FAILED,
            "unavailable" => AlarmSeverity::UNAVAILABLE}
  end
         
  def default_mapping
    self.alarm_mapping = BrixAlarmHistory::default_brix_alarm_mapping if alarm_mapping == nil 
  end
  
  def self.alarm_sync_all(nms = nil)
    # Group the alarm histories by network manager
    alarm_history_by_nms = Hash.new {|h,k| h[k]=[]}
    BrixAlarmHistory.all.each {|ah| alarm_history_by_nms[ah.event_ownerships.first.eventable.get_monitoring_manager] << ah}
    
    # If only a single nms needs to be resynced select the alarm histories
    alarm_history_by_nms = {nms => alarm_history_by_nms[nms]} if nms != nil
          
    return_code = true
    alarm_history_by_nms.each do |nms, alarm_histories|
      alarm_histories.each {|ah|
        if !ah.alarm_sync
          return_code = false
        end  
      }
    end
    return return_code
  end      

  def alarm_sync(timestamp = nil)
    return_code = false
    ctv = event_ownerships.first.eventable_type.constantize.find_by_id(event_ownerships.first.eventable_id)
    if ctv == nil
      SW_ERR "Failed to find object associated with #{self.inspect}"
    else
      sla_id = ctv.sla_id
      test_class_id = ctv.test_instance_class_id
      module_name = ctv.get_bx_module_name
      test_type = ctv.test_type
      
      if sla_id.empty? || test_class_id.empty?
        SW_ERR "#{self.id} #{self.event_filter} - sla_id(#{sla_id}) and/or test_instance_class_id(#{test_class_id}) are empty"
      else
        # Get the last 3 minutes of data - From testing it must be 3 to guarantee a reply - If it's one or two
        # minutes then sometimes the SOAP request comes back with an error "No data for the given filter"
        attributes = {}
        if (test_type == 'Ethernet Frame Delay Test')
          attributes = {"Percent Frames Lost" => "flr", "Average Network Round-Trip Latency" => "delay", "Average Network Round-Trip Latency Variation" => "dv"}
        elsif (test_type == 'Ping Active Test')
          attributes = {"Percent Lost Packets" => "flr", "Average Round-Trip Latency" => "delay", "Average Jitter" => "dv"}
        elsif (test_type == 'Ethernet Loopback Test')
          attributes = {"Percent Frames Lost" => "flr", "Average Round-Trip Latency" => "delay", "Average Round-Trip Latency Variation" => "dv"}
        else
          SW_ERR "ERROR: Unknown Test Type: #{test_type}\n"
        end

        api = BxIf::StatsApi.new(ctv.get_monitoring_manager)       
        result, data = api.get_test_data("#{module_name}", sla_id, (Time.now-3.minutes).to_s , Time.now.to_s, test_class_id, ["Result"] + attributes.keys)

        if !result
          SW_ERR "#{self.id} #{self.event_filter} BxIf::StatsApi.get_test_data(#{ctv.cos_end_point.segment_end_point.cenx_name}) FAILED"
          api.close
        else
          api.close
          return_code = true
          state = data.last[0]["Result"] == "SUCCESSFUL" ? "cleared" : "failed"
          details = ""
          if state == 'cleared'
            target_obj = event_ownerships.first.eventable_type.constantize.find_by_id(event_ownerships.first.eventable_id)
            attributes.each do |attribute, sla_method|              
              # The delay in CDB is in ms but Brix reports in us so need to convert
              # The delay and delay variation is in round-trip need to report one-way
              actual_value = data.last[0][attribute].to_f
              case sla_method
                when 'flr'
                  actual_value = data.last[0][attribute].to_f
                  error_threshold = target_obj.send("#{sla_method}_error_threshold").to_f
                  warning_threshold = target_obj.send("#{sla_method}_warning_threshold").to_f
                when 'dv'
                  actual_value = actual_value.to_f/2.0
                  error_threshold = target_obj.send("#{sla_method}_error_threshold").to_f
                  warning_threshold = target_obj.send("#{sla_method}_warning_threshold").to_f                  
                when 'delay'
                  actual_value = (actual_value.to_f/1000)/2.0
                  error_threshold = target_obj.send("#{sla_method}_error_threshold").to_f
                  warning_threshold = target_obj.send("#{sla_method}_warning_threshold").to_f               
              end
                                
              # Check to see if any thresholds have been crossed
              attribute_state = 'cleared'
              if target_obj.send("#{sla_method}_warning_threshold") != "N/A"
                attribute_state = "warning" if actual_value >= warning_threshold
              end
              if target_obj.send("#{sla_method}_error_threshold") != "N/A"
                attribute_state = "failed" if actual_value >= error_threshold
              end
              
              if attribute_state != 'cleared'
                details += BrixAlarmHistory.details_formatter(sla_method, attribute_state, actual_value, warning_threshold, error_threshold) + "\n"
              end
              state = attribute_state if (state == 'cleared') || (state == 'warning' && attribute_state == 'failed') 
            end
          else
            details += BrixAlarmHistory.details_formatter("result", "failed", data.last[0]["Result"], nil, nil)
          end
          timestamp = (data.last[0]["Time Stamp"].to_i - BrixAlarmHistory.alarm_time_correction) 
          add_event(timestamp, state, details)
        end
      end
    end
    return return_code
  end
        
  def self.alarm_time_correction
    return 1.minutes * 1000
  end
  
  def self.details_formatter (test, result, actual_value, warning_threshold, error_threshold)
    test_convertion = {"flr" => "FLR", "dv" => "Delay Variation", "delay" => "Delay", "result" => "Overall result"}
    units = {"flr" => "%", "dv" => "us", "delay" => "ms", "result" => ""}
    test_name = test_convertion[test] || "Unknown Test (#{test})"
    details = "#{test_name}: #{result}\nMeasured Value: #{actual_value}#{units[test]}\n"
    if error_threshold != nil && warning_threshold != nil
      details += "SLA Guarantees: Error: #{error_threshold}#{units[test]}; Warning: #{warning_threshold}#{units[test]}"
    end
    return details
  end
  
end
