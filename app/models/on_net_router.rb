# == Schema Information
#
# Table name: segments
#
#  id                       :integer(4)      not null, primary key
#  type                     :string(255)
#  status                   :string(255)
#  notes                    :text
#  segment_owner_role       :string(255)
#  segment_type_id          :integer(4)
#  cenx_id                  :string(255)
#  operator_network_id      :integer(4)
#  service_id               :integer(4)
#  created_at               :datetime
#  updated_at               :datetime
#  ethernet_service_type_id :integer(4)
#  use_member_attrs         :boolean(1)
#  path_network_id          :integer(4)
#  emergency_contact_id     :integer(4)
#  event_record_id          :integer(4)
#  sm_state                 :string(255)
#  sm_details               :string(255)
#  sm_timestamp             :integer(8)
#  prov_name                :string(255)
#  prov_notes               :text
#  prov_timestamp           :integer(8)
#  order_name               :string(255)
#  order_notes              :text
#  order_timestamp          :integer(8)
#  site_id                  :integer(4)
#
class OnNetRouter < OnNetSegment

  # Layer 3
  def layer
    3
  end

  def cenx_name
    short_sp_name = CenxNameTools::CenxNameHelper.shorten_name path_network.service_provider.name
    short_site_name = CenxNameTools::CenxNameHelper.shorten_name site_info_segment

    type = CenxNameTools::CenxNameHelper.generate_type_string self
    return "#{type}:#{short_sp_name}:#{short_site_name}:#{on_switch_name}"
  end

  def on_switch_name
    return "vrf #{service_id}"
  end
end

