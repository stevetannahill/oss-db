# == Schema Information
#
# Table name: segment_end_point_types
#
#  id                       :integer(4)      not null, primary key
#  name                     :string(255)
#  created_at               :datetime
#  updated_at               :datetime
#  operator_network_type_id :integer(4)
#  class_of_service_type_id :integer(4)
#  notes                    :string(255)
#
class OvcEndPointType < SegmentEndPointType
end

