# == Schema Information
#
# Table name: nodes
#
#  id                   :integer(4)      not null, primary key
#  name                 :string(255)
#  node_type            :string(255)
#  primary_ip           :string(255)
#  created_at           :datetime
#  updated_at           :datetime
#  site_id :integer(4)
#  secondary_ip         :string(255)
#  net_mask             :string(255)
#  gateway_ip           :string(255)
#  notes                :string(255)
#  cabinet_id           :integer(4)
#  loopback_ip          :string(255)
#  network_manager_id   :integer(4)
#  event_record_id      :integer(4)
#  sm_state             :string(255)
#  sm_details           :string(255)
#  sm_timestamp         :integer(8)
#  prov_name            :string(255)
#  prov_notes           :text
#  prov_timestamp       :integer(8)
#  order_name           :string(255)
#  order_notes          :text
#  order_timestamp      :integer(8)
#


class Node < ActiveRecord::Base
  include Eventable
  include Stateable
 
  BRIXWORX_TYPES = ["BV-3000","RTU-310"]
  SAM5620_TYPES = ["7750-sr12", "7450-ESS7", "7450-ESS7-2", "7210 SAS-M", "ASR9100","7705 SAR-8","ALU MMBTS"]
  ESCOUT_TYPES = ["QoS-Scope-1G"]
  SPRINT_NEM_TYPES = ["ASR9010", "SE1200", "7750-sr12", "7450-ESS7", "7609-S"]
  TELLABS_EMS_TYPES = ["Tellabs 8660", "Tellabs 8630"]
  OSS_RC_EMS_TYPES = ["RNC", "BSC", "RBS 2000", "RBS 3000", "RBS 6000", "SIU", "RBS"]
  IPT_NMS_TYPES = ["MINI-LINK TN"]
  SOLARWIND_TYPES = ["Brocade MLXe8", "Brocade MLXe16", "Brocade MLXe32", "Brocade CES 2024", "Brocade CES 2048"]
  MONITORING_TYPES = %w(cNode2020 BV-3000 RTU-310 QoS-Scope-1G QoS-Scope-10G WIPM-LLC)
  TROUBLE_SHOOTING_TYPES = %w(smb-600 smb-6000)
  
  belongs_to :site
  belongs_to :network_manager
  belongs_to :cabinet
  has_many :ports, :dependent => :destroy, :order => Port::STANDARD_ORDER
  
  validates_presence_of :name, :node_type, :site_id
  validates_presence_of :primary_ip
  #validates_presence_of :cabinet_id, :on => :update
  validates_uniqueness_of :name
  validates_uniqueness_of :primary_ip, :scope => :site_id, :unless => Proc.new { |node| node.site && node.site.network_address == "TBD" && node.primary_ip == "TBD" }
  validates_uniqueness_of :loopback_ip, :scope => :site_id, :allow_blank => true, :allow_nil => true
  validates_inclusion_of :node_type, :in => HwTypes::NODE_TYPES.map{|disp, value| value }, 
                         :message => "invalid, Use: #{HwTypes::NODE_TYPES.map{|disp, value| value }.join(", ")}"
  validates_format_of :primary_ip, :with => /(^(\d{1,3}\.){3}(\d{1,3})$)/, :message => "Should be dotted decimal IP", :unless => Proc.new { |node| node.site && node.site.network_address == "TBD" && node.primary_ip == "TBD" }
  validates_format_of :secondary_ip, :with => /(^$)|(^(\d{1,3}\.){3}(\d{1,3})$)/, :message => "Should be dotted decimal IP"
  validates_format_of :loopback_ip, :with => /(^$)|(^(\d{1,3}\.){3}(\d{1,3})$)/, :message => "Should be dotted decimal IP"
  validates_format_of :net_mask, :with => /(^$)|(^\/\d{1,2}$)/, :message => 'Should be CIDR format (ex: /24, /8 )'
  validates_presence_of :network_manager_id, :if => Proc.new(){|node| (BRIXWORX_TYPES + SAM5620_TYPES + SPRINT_NEM_TYPES + ESCOUT_TYPES + SOLARWIND_TYPES).uniq.include? node.node_type}
  validates_inclusion_of :network_manager_id, :in => [], :allow_nil => true, :unless => Proc.new(){|node| (BRIXWORX_TYPES + SAM5620_TYPES + SPRINT_NEM_TYPES + ESCOUT_TYPES + SOLARWIND_TYPES + TELLABS_EMS_TYPES + OSS_RC_EMS_TYPES + IPT_NMS_TYPES).uniq.include? node.node_type},
          :message => "may not be attached to a node of this type"
  validate :valid_nm_type?
  
  
  after_create do
    # Set the SM state to OK - hardcoded 
    update_attributes(:sm_state => AlarmSeverity::OK.to_s, :sm_details => "", :sm_timestamp => (Time.now.to_f*1000).to_i)
    # create the Provisioning state
    statefuls << NodeProvStateful.create
    state = prov_state.get_state
    update_attributes(:prov_name => state.name , :prov_timestamp => state.timestamp, :notes => state.notes)    
    event_record_create
  end
  
  def self.find_rows_for_index
    order("name")
  end
  
  def default_name suffix, id, id_length=nil
    id_length = 2 if id_length.nil?
    base = "#{site.node_prefix}#{suffix}"
    return sprintf("#{base}%1$0*2$d", id, id_length) #Basically returns Base01 (for length 2) and Base001 for length 3 etc
  end
  
  def default_ip tail
    return nil unless tail && site && site.network_address && !site.network_address.empty? && site.network_address != "TBD"
    return tail ? site.network_address.match(/^(\d{1,3}\.){3}/)[0] + tail : nil
  end

  def default_loopback tail
    return nil unless tail && site && site.loopback_address && !site.loopback_address.empty? && site.loopback_address != "TBD"
    return tail ? site.loopback_address.match(/^(\d{1,3}\.){3}/)[0] + tail : nil
  end
  
  def set_defaults instance
    self.name = default_name instance[:identifier], instance[:id], instance[:id_length]
    ip_suffix = instance[:ip_suffix]
    self.primary_ip = default_ip ip_suffix[:primary]
    self.primary_ip = "TBD" if self.primary_ip.nil?
    self.secondary_ip = default_ip ip_suffix[:secondary]
    self.gateway_ip = default_ip ip_suffix[:gateway]
    self.loopback_ip = default_loopback ip_suffix[:loopback]
  end
  
  def get_candidate_network_managers
    nms = []

    if BRIXWORX_TYPES.include? node_type
      nms << NetworkManager.all.select{|nm| nm.nm_type == "BrixWorx"}
    end

    if SAM5620_TYPES.include? node_type
      nms << NetworkManager.all.select{|nm| nm.nm_type == "5620Sam"}
    end

    if SPRINT_NEM_TYPES.include? node_type
      nms << NetworkManager.all.select{|nm| nm.nm_type == "Sprint NEM"}
    end

    if ESCOUT_TYPES.include? node_type
      nms << NetworkManager.all.select{|nm| nm.nm_type == "eScout"}
    end

    if TELLABS_EMS_TYPES.include? node_type
      nms << NetworkManager.all.select{|nm| nm.nm_type == "Tellabs EMS"}
    end

    if OSS_RC_EMS_TYPES.include? node_type
      nms << NetworkManager.all.select{|nm| nm.nm_type == "OSS-RC"}
    end

    if IPT_NMS_TYPES.include? node_type
      nms << NetworkManager.all.select{|nm| nm.nm_type == "IPT-NMS"}
    end
    
    
    return nms.flatten
  end

  def site_info
    return "#{site.site_groups.collect{|sg| sg.name}.join(" ;")}/#{site.name}"
  end
  
  def cabinet_info
    return cabinet_id ? cabinet.name : "TBD"
  end
  
  def site_layout
    site.site_layout
  end

  def unlinked_ports
    u_ports = []
    ports.each do |port|
      #logger.info("**** Check Port: #{port.name}")
      if !port.is_port_connected_to_demarc
        #logger.info("**** add Port #{port.name}")
        u_ports << port
      end
    end
    return u_ports
  end

  def is_test_node
    return (is_trouble_shooting_node || is_monitoring_node)
  end

  def is_trouble_shooting_node
    TROUBLE_SHOOTING_TYPES.include?(node_type)
  end

  def is_monitoring_node
    MONITORING_TYPES.include?(node_type)
  end
  
  def can_seed_ports
    ports.empty? 
  end
 
  def seed_ports instance
    if can_seed_ports and not instance[:node][:ports].nil?
      #for each port for a node with node type
      do_seed_ports instance[:node][:ports]
    end
  end

  def do_seed_ports ports
    ports.each{|params|
        #add a new port to node
        port = Port.new(params)
        port.node = self
        SW_ERR("Could not create Port: #{port.name} " + port.errors.full_messages.join(", ")) unless port.save
      }
  end

  def seed_additional information
    do_seed_ports information[:ports]
  end

  def of_instance instance
    #Type, and IP Address Must Match
    return node_type == instance[:node][:type] &&
      primary_ip == default_ip(instance[:ip_suffix][:primary]) &&
      secondary_ip == default_ip(instance[:ip_suffix][:secondary]) &&
      gateway_ip == default_ip(instance[:ip_suffix][:gateway])
  end

 def is_service_router
   return (SAM5620_TYPES + SPRINT_NEM_TYPES + SOLARWIND_TYPES + TELLABS_EMS_TYPES + OSS_RC_EMS_TYPES + IPT_NMS_TYPES).uniq.include? node_type
 end

 def is_mini_site_node?
   return site.is_mini_site?
 end

 def go_in_service
   return {:result => true, :info => ""} if !is_service_router
   info = ""
   # create the Maintenance history    
   if mtc_periods.empty?
     self.mtc_periods = [MtcHistory.create(:event_filter => "Mtc node:#{name}")]
     self.set_mtc(false)
   end
   return {:result => true, :info => info}
 end
 
 def get_SM_state
   SW_ERR "Should NOT be called"
   super
 end

 private
 
 def valid_nm_type?
   if network_manager
     if (BRIXWORX_TYPES.include?(node_type) && network_manager.nm_type == "BrixWorx") or
        (SAM5620_TYPES.include?(node_type) && network_manager.nm_type == "5620Sam") or
        (SPRINT_NEM_TYPES.include?(node_type) && network_manager.nm_type == "Sprint NEM") or
        (ESCOUT_TYPES.include?(node_type) && network_manager.nm_type == "eScout") or
        (TELLABS_EMS_TYPES.include?(node_type) && network_manager.nm_type == "Tellabs EMS") or
        (OSS_RC_EMS_TYPES.include?(node_type) && network_manager.nm_type == "OSS-RC") or
        (IPT_NMS_TYPES.include?(node_type) && network_manager.nm_type == "IPT-NMS") or
        (SOLARWIND_TYPES.include?(node_type) && network_manager.nm_type == "SolarWinds")
        return
     else
       errors.add(:network_manager_id, "type #{network_manager.nm_type} not allowed for a node of type #{node_type}")
     end
   end
 end
 

end
