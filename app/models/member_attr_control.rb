# == Schema Information
# Schema version: 20110802190849
#
# Table name: member_attr_controls
#
#  id              :integer(4)      not null, primary key
#  member_attr_id  :integer(4)
#  description     :string(255)
#  execution_order :integer(4)
#  action          :string(255)
#  code            :text
#  info            :string(255)
#  notes           :text
#  created_at      :datetime
#  updated_at      :datetime
#

class MemberAttrControl < ActiveRecord::Base
  belongs_to :member_attr
  validates_inclusion_of :action, :in => MemberAttrTypes::ATTR_CONTROL_ACTIONS
  delegate :member_attr_instances, :to => :member_attr
  
  def eval_code
    #remember code is executed from within a member attr instance
    return code.gsub(/(^|\s|[;+-=*(){}\[\]&|<>,?%:])instance(\.(\d?[a-zA-Z0-9_]*(\s|\.|[;+-=*(){}\[\]&|<>,?%:]|$)))?/){|match|
      "#{$1}affected_entity#{$2}"
    }
  end
  
end
