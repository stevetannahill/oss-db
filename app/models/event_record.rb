# == Schema Information
# Schema version: 20110802190849
#
# Table name: event_records
#
#  id               :integer(4)      not null, primary key
#  state            :string(255)
#  details          :string(255)
#  event_history_id :integer(4)
#  created_at       :datetime
#  updated_at       :datetime
#  type             :string(255)
#  original_state   :string(255)
#  time_of_event    :integer(8)      not null
#

class EventRecord < ActiveRecord::Base
  include Enumerable
  belongs_to :event_history

  after_save :set_path_sphinx_delta_flag

  def human_time
    Time.at(time_of_event/1000).in_time_zone
  end

  def <=>(other)
    time_of_event <=> other.time_of_event
  end
  
  def set_path_sphinx_delta_flag
    ReindexSphinx.schedule
  end
end
