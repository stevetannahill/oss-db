# == Schema Information
# Schema version: 20110802190849
#
# Table name: class_of_service_types
#
#  id                             :integer(4)      not null, primary key
#  name                           :string(255)
#  operator_network_type_id       :integer(4)
#  cos_id_type_enni               :string(255)
#  cos_mapping_enni_ingress       :string(255)
#  cos_mapping_uni_ingress        :string(255)
#  availability                   :string(255)
#  frame_loss_ratio               :string(255)
#  mttr_hrs                       :string(255)
#  notes                          :text
#  created_at                     :datetime
#  updated_at                     :datetime
#  cos_id_type_uni                :string(255)     default("pcp")
#  cos_marking_enni_egress        :string(255)
#  cos_marking_uni_egress         :string(255)
#  cos_marking_enni_egress_yellow :string(255)     default("N/A")
#  cos_marking_uni_egress_yellow  :string(255)     default("N/A")
#  type                           :string(255)
#

class CenxClassOfServiceType < ClassOfServiceType
  validates_uniqueness_of :fwding_class, :scope => :operator_network_type_id
  validates_uniqueness_of :short_name , :scope => :operator_network_type_id
  validates_numericality_of :cir_cac_limit, :only_integer => true, :message => "can only be whole number."
  validates_numericality_of :eir_cac_limit, :only_integer => true, :message => "can only be whole number."
  validates_presence_of :cir_cac_limit, :eir_cac_limit, :short_name, :fwding_class
  validates_inclusion_of :name, :in => ServiceCategories::ON_NET_OVC_COS_TYPES.keys, :message => "invalid, cenx costs must use: #{ServiceCategories::ON_NET_OVC_COS_TYPES.keys.join(", ")}"
  validates_uniqueness_of :name, :scope => [:type], :message => "invalid, cenx cost #{name} already exists."
end
