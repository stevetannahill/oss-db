# == Schema Information
#
# Table name: demarcs
#
#  id                               :integer(4)      not null, primary key
#  type                             :string(255)
#  cenx_id                          :string(255)
#  cenx_demarc_type                 :string(255)
#  notes                            :text
#  operator_network_id              :integer(4)
#  address                          :string(255)
#  port_name                        :string(255)
#  physical_medium                  :string(255)
#  connected_device_type            :string(255)
#  connected_device_sw_version      :string(255)
#  reflection_mechanism             :string(255)
#  mon_loopback_address             :string(255)
#  end_user_name                    :string(255)
#  is_new_construction              :boolean(1)
#  contact_info                     :string(255)
#  created_at                       :datetime
#  updated_at                       :datetime
#  demarc_type_id                   :integer(4)
#  max_num_segments                 :integer(4)
#  ether_type                       :string(255)
#  protection_type                  :string(255)
#  lag_id                           :integer(4)
#  fiber_handoff_type               :string(255)
#  auto_negotiate                   :boolean(1)
#  ah_supported                     :boolean(1)
#  lag_mac_primary                  :string(255)
#  emergency_contact_id             :integer(4)
#  lag_mac_secondary                :string(255)
#  port_enap_type                   :string(255)     default("QINQ")
#  lag_mode                         :string(255)
#  cir_limit                        :integer(4)      default(100)
#  eir_limit                        :integer(4)      default(300)
#  cenx_name_prefix                 :string(255)
#  demarc_icon                      :string(255)     default("building")
#  on_behalf_of_service_provider_id :integer(4)
#  event_record_id                  :integer(4)
#  sm_state                         :string(255)
#  sm_details                       :string(255)
#  sm_timestamp                     :integer(8)
#  prov_name                        :string(255)
#  prov_notes                       :text
#  prov_timestamp                   :integer(8)
#  order_name                       :string(255)
#  order_notes                      :text
#  order_timestamp                  :integer(8)
#  near_side_clli                   :string(255)     default("TBD")
#  far_side_clli                    :string(255)     default("TBD")
#  latitude                         :decimal(9, 6)   default(0.0)
#  longitude                        :decimal(9, 6)   default(0.0)
#  site_id                          :integer(4)
#


class Uni < Demarc
  validates_format_of :mon_loopback_address,
                      :with => /(^$)|(^(\d{1,3}\.){3}(\d{1,3})\/(\d{1,2})$)|(^(([A-Fa-f0-9]{2}):){5}([A-Fa-f0-9]{2})$)/,
                      :message => "should be strict IP/CIDR or MAC address format ex: IP: 111.222.333.444/55 or MAC: 11:22:33:44:55:66"
  validates_inclusion_of :reflection_mechanism, :in => FrameTypes::MONITORING_FRAME_REFLECTION.map{|disp, value| value },
                         :message => "invalid, Use: #{FrameTypes::MONITORING_FRAME_REFLECTION.map{|disp, value| value }.join(", ")}",
                         :allow_nil => true, :allow_blank => true
  validates_inclusion_of :physical_medium, :in => HwTypes::UNI_PHY_TYPES.map{|disp, value| value },
                         :message => "invalid, Use: #{HwTypes::UNI_PHY_TYPES.map{|disp, value| value }.join(", ")}",
                         :allow_nil => true, :allow_blank => true
  validates_inclusion_of :connected_device_type, :in => HwTypes::DEMARC_DEVICE_TYPES.map{|disp, value| value },
                         :message => "invalid, Use: #{HwTypes::DEMARC_DEVICE_TYPES.map{|disp, value| value }.join(", ")}",
                         :allow_nil => true, :allow_blank => true

  #validate :valid_loopback_for_segmenteps?
  validates_class_of :demarc_type, :is_a => "UniType", :allow_nil => true
  
  def general_after_create
    # Create Service Monitoring history
    update_attributes(:sm_state => AlarmSeverity::NOT_LIVE.to_s, :sm_details => "", :sm_timestamp => (Time.now.to_f*1000).to_i)
    sm_histories << SmHistory.create(:event_filter => "SM #{self.class.name}:#{cenx_id}")
    # create the Provisioning state
    statefuls << UniProvStateful.create
    state = prov_state.get_state
    update_attributes(:prov_name => state.name , :prov_timestamp => state.timestamp, :notes => state.notes)
    event_record_create
  end
    
  def get_possible_candidate_demarc_types
    if operator_network_id != nil
      return operator_network.operator_network_type.uni_types
    else
      return []
    end
  end
  
  # Commented out by Corey for Dual EVC
  # def valid_loopback_for_segmenteps?
  #   if cenx_demarc_type == "UNI" && mon_loopback_address && mon_loopback_address.match(/^(([A-Fa-f0-9]{2}):){5}([A-Fa-f0-9]{2})$/)
  #     segment_end_points.each{|segmentep|
  #       if !segmentep.valid_uni_loopback? mon_loopback_address
  #         errors.add(:mon_loopback_address, "Attached Segment End Point:#{segmentep.cenx_name} cannot accept mac address. Mac address is already used in Segment:#{segmentep.segment.cenx_name}.")
  #         return false
  #       end
  #     }
  #   end
  #   return true
  # end
  
  def go_in_service
    info = ""
    sm_histories << SmHistory.create(:event_filter => "SM #{self.class.name}:#{cenx_id}") if sm_histories.empty?
    if mtc_periods.empty?
      self.mtc_periods = [MtcHistory.create(:event_filter => "Mtc UNI:#{cenx_id}")]
      self.set_mtc(false)
    end
    return {:result => true, :info => info}
  end
  
  # TODO: Not completely correct but good enough for demo
  # If any end points are monitored then UNI is monitored
  def is_monitored?
    segment_end_points.any?{|e| e.is_monitored?}
  end
  
end
