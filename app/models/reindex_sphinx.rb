# == Schema Information
# Schema version: 20110802190849
#
# Table name: reindex_sphinxes
#
#  id          :integer(4)      not null, primary key
#  started_at  :datetime
#  finished_at :datetime
#  scheduled   :boolean(1)
#  created_at  :datetime
#  updated_at  :datetime
#

class ReindexSphinx < ActiveRecord::Base

  @@status = :live # :live, :ignore

  def self.status
    @@status
  end
  
  def self.status=(val)
    @@status = val  
  end
  
  def self.reset
    @@status = :live
  end

  def self.schedule
    return false if ReindexSphinx.status == :ignore
    master_record.scheduled = true
    master_record.save
  end
  
  def self.master_record
    @master_record ||= ReindexSphinx.find(1)
    if @master_record.nil?
      @master_record = ReindexSphinx.new
      @master_record.id = 1;
      @master_record.save
    end
    @master_record
  end
end
