# == Schema Information
#
# Table name: exception_sla_cos_test_vectors
#
#  id                 :integer(4)      not null, primary key
#  time_declared      :integer(4)
#  value              :float
#  severity           :string(0)
#  cos_test_vector_id :integer(4)
#  sla_type           :string(0)
#  time_over          :integer(4)
#
class ExceptionSlaCosTestVector < ActiveRecord::Base
  include CommonException

  SEVERITY = [:critical, :major, :minor]
  SLA_TYPE = [:availability, :flr, :fd, :fdv]

  belongs_to :cos_test_vector, :primary_key => :grid_circuit_id, :foreign_key => :grid_circuit_id
  validates_presence_of  :grid_circuit_id
  validates_inclusion_of :sla_type, :in => SLA_TYPE
  validates :time_declared, :presence => true
  validates_inclusion_of :severity, :in => SEVERITY


  def severity
    read_attribute(:severity).to_sym
  end
  def severity= (value)
    write_attribute(:severity, value.to_s)
  end
  
  def sla_type
    read_attribute(:sla_type).to_sym
  end
  def sla_type= (value)
    write_attribute(:sla_type, value.to_s)
  end

  def entity_type
    'Off Net OVC'
  end

  def self.to_exception_sla_type stats_type
    case stats_type
    when :availability
      return :availability
    when :frame_loss_ratio
      return :flr
    when :delay
      return :fd
    when :delay_variation
      return :fdv
    end
  end

  def to_internal_metric_type
    case self.sla_type
    when :availability
      return :availability
    when :flr
      return :frame_loss_ratio
    when :fd
      return :delay
    when :fdv
      return :delay_variation
    end
  end

  def get_errored_period_threshold 
    the_severity = read_attribute(:severity).to_sym
    cos_test_vector.get_errored_period_threshold the_severity, self.to_internal_metric_type
  end

  #   returns delta_t which can be longer than the native period (59 secs or 5 mins)
  # if 0 is returned then the native time period shall be used.
  def get_errored_period_delta_time_secs
    cos_test_vector.get_errored_period_delta_time_secs
  end

  # Returns the maximum number of time periods allowed over the threshold without
  # declaring an exception.
  # has to be greater than 1
  def get_errored_period_max_errored_count 
    cos_test_vector.get_errored_period_max_errored_count severity, self.to_internal_metric_type
  end


  def get_value_units_s
    # all severities now return the converted metric units, instead of major and minor always being seconds
    Stats::METRIC_UNITS[self.to_internal_metric_type]
  end

  def get_threshold_units_s
    metric_type = self.to_internal_metric_type
    metric_type = :frame_loss_ratio if :frame_loss == metric_type
    return Stats::METRIC_UNITS[metric_type]
  end

  def get_errored_period_max_secs
    get_errored_period_max_errored_count.to_i * get_errored_period_delta_time_secs.to_i
  end

  def availability_flr_threshold
    return 3.33 # 3.33 %
  end

  def availability_consecutive_count_threshold
    return 2
  end

  def monthly_metric_value
#    MetricsCosTestVector.find_by_grid_circuit_id_and_period_type_and_metric_type(grid_circuit_id, 'monthly', sla_type).metric
    time_d = Time.at(time_declared)
    period_start = Time.utc(time_d.utc.year, time_d.utc.month, 1)
    matches = MetricsCosTestVector.where(
      :grid_circuit_id => grid_circuit_id,
      :period_type => :monthly,
      :metric_type => sla_type,
      :period_start_epoch => period_start.to_i )
    if matches.size > 1
      SW_ERR "ERROR: found more than one match for exception(#{id}) in MetricsCosTestVector\n"
    end
    matches.first.try(:metric)
  end
end

