# == Schema Information
#
# Table name: segments
#
#  id                       :integer(4)      not null, primary key
#  type                     :string(255)
#  status                   :string(255)
#  notes                    :text
#  segment_owner_role           :string(255)
#  segment_type_id              :integer(4)
#  cenx_id                  :string(255)
#  operator_network_id      :integer(4)
#  site_id              :integer(4)
#  service_id               :integer(4)
#  created_at               :datetime
#  updated_at               :datetime
#  ethernet_service_type_id :integer(4)
#  use_member_attrs         :boolean(1)
#  path_network_id           :integer(4)
#  emergency_contact_id     :integer(4)
#  event_record_id          :integer(4)
#  sm_state                 :string(255)
#  sm_details               :string(255)
#  sm_timestamp             :integer(8)
#  prov_name                :string(255)
#  prov_notes               :text
#  prov_timestamp           :integer(8)
#  order_name               :string(255)
#  order_notes              :text
#  order_timestamp          :integer(8)
#

class Segment < InstanceElement
  include Eventable
  include Stateable
  include Kpiable
  
  has_and_belongs_to_many :paths, #structural
                          :after_add => :assoc_service_providers_for_indexing_path,
                          :after_remove => :assoc_service_providers_for_indexing_path

  belongs_to :site
  belongs_to :segment_type
  belongs_to :operator_network
  has_one :service_provider, :through => :operator_network
  belongs_to :path_network, :class_name => "OperatorNetwork"
  belongs_to :ethernet_service_type
  belongs_to :emergency_contact, :class_name => "Contact"
  has_many :service_orders, :as => :ordered_entity, :dependent => :destroy
  has_many :segment_end_points, :class_name => 'SegmentEndPoint', :dependent => :destroy
  has_many :cos_instances, :dependent => :destroy
  has_many :uploaded_files, :as => :uploader
  
  has_many :path_displays, :as => :entity, :dependent => :destroy
  
  
  validates_length_of :paths, :minimum => 1, :too_short => "is empty, need at least 1 Path"
  validates_presence_of :ethernet_service_type_id, :segment_type_id, :segment_owner_role, :path_network_id
  validates_inclusion_of :segment_owner_role, :in => ServiceProviderTypes::OWNER_ROLE.map{|disp, value| value },
                         :message => "invalid, Use: #{ServiceProviderTypes::OWNER_ROLE.map{|disp, value| value }.join(", ")}"

  validates_as_cenx_id :cenx_id
  validate :valid_paths?
  validate :valid_cosis?
  validate :valid_emergency_contact?

  after_save :assoc_service_providers_for_indexing_entities
    
  scope :service_providers, :include => :service_provider, 
                                  :conditions => ['segments.type = ?', 'OffNetOvc'], 
                                  :group => 'segments.operator_network_id'

  scope :sites, :include => :site, 
                          :conditions => ['segments.type = ?', 'OnNetOvc']
  
  #Active Record Callbacks
  before_validation(:on => :create) do
    self.cenx_id = IdTools::CenxIdGenerator.generate_cenx_id(self.class)
  end
  
  # Default to layer 2
  def layer
    2
  end
  
  def effected_members
    paths.uniq.map{|e|e.effected_members}.flatten.uniq
  end
    
  after_save :general_after_save

  def general_after_save    
    lo = get_latest_order
    if lo
      lo.ordered_entity_snapshot = attr_snapshot
      lo.save      
    end
  end
  
  #---------------------------------
  # Start Instance Element Code
  #---------------------------------

  def assoc_service_providers_for_indexing_path(path)
    # populate Sphinx Access Point so we have uniqure rows for each member handle (which sphinx requires)
    path.assoc_service_providers_for_indexing_paths
  end
  

  def assoc_service_providers_for_indexing_entities
    # populate Sphinx Access Point so we have uniqure rows for each member handle (which sphinx requires)
    paths.each do |path|
      path.assoc_service_providers_for_indexing_paths
    end
    
    # Ennis have their own Sphinx index
    ovc_end_point_ennis.each do |segmentep|
      segmentep.enni.assoc_service_providers_for_indexing_ennis if segmentep.enni
    end
  end
  
  after_destroy :general_after_destroy

  def general_after_destroy
    paths.map{|path| path.segments}.flatten.each{|segment|
      segment.update_member_attr_instances
      segment.save(:validate => false)
    }
  end
    
  after_create do
    segments = paths.map{|path| path.segments}.flatten.each{|segment|
      if segment != self
        segment.update_member_attr_instances
        segment.save(:validate => false)
      end
    }
    
    # Create Service Monitoring history
    update_attributes(:sm_state => AlarmSeverity::NOT_LIVE.to_s, :sm_details => "", :sm_timestamp => (Time.now.to_f*1000).to_i)
    sm_histories << SmHistory.create(:event_filter => "SM #{self.class.name}:#{cenx_id}")
    # create the Provisioning state
    statefuls << SegmentProvStateful.create
    state = prov_state.get_state
    update_attributes(:prov_name => state.name , :prov_timestamp => state.timestamp, :notes => state.notes)    
    event_record_create
  end
  
  def type_elements
    [segment_type, ethernet_service_type]
  end
  
  def member_handle_owners
    maos = paths.map{|path| path.member_handle_owners}.flatten.uniq
    # Sprint demo hack
    #maos.reject!{|mao| mao.is_system_owner}
    return maos
  end
  
  def member_handle_prefix owner
    owner.name + "'s "
  end
  
  def tma_enabled_logic
    return use_member_attrs
  end

  # used for segment total view
  def get_related_instance_elements
    demarcs = []
    segmenteps = []
    segment_end_points.each{|segmentep|
      if segmentep.is_connected_to_demarc
        demarcs << segmentep.demarc
      end
      segmenteps << segmentep
    }

    demarcs.uniq!
    return demarcs + segmenteps
  end
  #---------------------------------
  # End Instance Element Code
  #---------------------------------
  
  def buyer
    path_network ? path_network.service_provider : nil
  end

  def buyer_name
    buyer ? buyer.name : "TBD"
  end
  
  def seller
    get_operator_network ? get_operator_network.service_provider : nil
  end

  def seller_name
    seller ? seller.name : "TBD"
  end

  def get_candidate_contacts
    (service_provider_id) ? service_provider.contacts : []
  end

  def get_candidate_segment_types
    ethernet_service_type.nil? ? [] : ethernet_service_type.segment_types
  end
  
  def get_candidate_paths
    path_network ? path_network.paths - paths : []
  end

  def name
    return cenx_name
  end
  
  def sp_info_segment
    service_provider.name
  end
  
  def on_info_segment
    get_operator_network.name
  end
  
  def est_info_segment
    ethernet_service_type.name
  end
  
  def segmentt_info_segment
    segment_type.name
  end

  def service_provider
    (get_operator_network) ? get_operator_network.service_provider : nil
  end

  def service_provider_id
    (get_operator_network && get_operator_network.service_provider) ? get_operator_network.service_provider.id : nil
  end

  def get_candidate_operator_networks
    return service_provider ? service_provider.operator_networks : nil
  end

  def get_candidate_ethernet_service_types
    get_operator_network ? get_operator_network.get_candidate_ethernet_service_types : nil
  end
  
  def get_candidate_costs
    return ethernet_service_type.class_of_service_types.reject{|cost| cos_instances.map{|cosi| cosi.class_of_service_type}.include? cost}
  end

  def emergency_contact_number
    emergency_contact ? emergency_contact.work_phone : "TBD"
  end

  def path_info_segment
    paths.map{|path| path.cenx_name}.join("; ")
  end

  def end_point_count
    return segment_end_points.size
  end

  def nodes_involved
     nodes = []
     end_points_to_check = segment_end_points.reject{|e| e[:type] == "OvcEndPointUni"}
     nodes = end_points_to_check.map{|e| e.nodes_involved}
     return nodes.flatten.uniq
  end

  def node_names
    nodes = nodes_involved
    names = nodes.map{|n| n.name}

    if names.empty?
      names << "None"
    end

    return names
  end

  def node_info
    return node_names.join("; ")
  end

  def ethernet_service_type_info
    return ethernet_service_type ? ethernet_service_type.name : "TBD"
  end

  def operator_info_segment
    return service_provider ? service_provider.name : "TBD"
  end

  def owner_info_segment
    return path_network ? path_network.service_provider.name : "TBD"
  end

  def owner_member_handle_value
    value = ''
    if member_handle_for_owner_object(path_network.service_provider)
      value = member_handle_for_owner_object(path_network.service_provider).value
    else
      value = cenx_name
    end

    return value
  end

  def operator_member_handle_value
    value = ''
    if member_handle_for_owner_object(service_provider)
      value = member_handle_for_owner_object(service_provider).value
    else
      value = cenx_name
    end

    return value
  end

  def get_operator_network
    operator_network_id ? operator_network : nil
  end

  def set_operator_network on
    self.operator_network = on
    #on.segments.push(self) if on
  end


  def on_info_segment
    get_operator_network ? get_operator_network.name : "TBD"
  end

  def get_latest_order
    service_orders.last
  end
  
  def active_order?
    order = get_latest_order
    if order
      current_state = order.get_order_state
      ![OrderDelivered, OrderRejected, OrderCancelled].any? {|order_state| current_state.is_a?(order_state)}
    end
  end

  def is_connected_to_demarc target_demarc
     segment_end_points.each do |ep|
      if ep.is_connected_to_demarc
        if ep.demarc.id == target_demarc.id
          return true
        end
      end
    end
    return false
  end

  def is_multi_cos_segment
    return cos_instances.size > 1
  end

  def unlinked_segment_end_points
    u_segmenteps = []
    segment_end_points.each do |segmentep|
      if !segmentep.demarc
        u_segmenteps << segmentep
      end
    end
    return u_segmenteps
  end

  def on_net_ovc_end_point_ennis
    segmenteps = SegmentEndPoint.where("segment_id = ? AND type = ?", id, "OnNetOvcEndPointEnni")
  end

  def ovc_end_point_ennis
    segmenteps = SegmentEndPoint.where("segment_id = ? AND type = ?", id, "OvcEndPointEnni")
  end

  def ovc_end_point_unis
    segmenteps = SegmentEndPoint.where("segment_id = ? AND type = ?", id, "OvcEndPointUni")
  end

  def demarcs
    ovc_end_point_unis.collect{ |o| o.demarc }.flatten.uniq
  end

  def ennis
    (ovc_end_point_ennis + on_net_ovc_end_point_ennis).collect{|o| o.enni }.flatten.uniq
  end

  def has_monitored_endpoints
    monitored_end_points = segment_end_points.reject{|e| !e.is_monitored}
    return !(monitored_end_points.empty?)
  end
  
  def alarm_sync
    success = super
    success = success && segment_end_points.all? {|ep| ep.alarm_sync}
    return success
  end
  
  def alarm_state            
    state = super
    ers = segment_end_points.collect {|ep| ep.event_record_id}    
    er_states = EventRecord.where("id In (?)", ers)
    er_states << state
    state = AlarmRecord.highest(er_states)
    #segment_end_points.each {|ep| state = ep.event_record + state}
    state = AlarmRecord.new(:time_of_event => 0, :state => AlarmSeverity::OK, :details => "", :original_state => "cleared") if state == nil    
    return state
  end
  
  def alarm_history      
    history = super
    segment_end_points.each {|ep| history = ep.alarm_history + history}
    return history
  end
  
  def go_in_service
    info = ""
    sm_histories << SmHistory.create(:event_filter => "SM #{self.class.name}:#{cenx_id}") if sm_histories.empty?
    if mtc_periods.empty?
      self.mtc_periods = [MtcHistory.create(:event_filter => "Mtc Segment:#{cenx_id}")]
      self.set_mtc(false)
    end
    return {:result => true, :info => info}
  end
  
  def monitored?
    # if any endpoint is monitored the Off Net OVC is monitored
    not segment_end_points.find_by_is_monitored(true).blank?
  end
  
  def get_network_manager
    return Node.find_by_name(node_names.first).network_manager
  end
  

  def reachable_operator_networks
    rons = paths.map{|path| path.reachable_operator_networks}.flatten.uniq
    return rons
  end

  def get_vlan_translation

    tags = []
    segmenteps = segment_end_points.reject{|e| e.is_connected_to_test_port}
    segmenteps.each{|segmentep|
      tags << segmentep.outer_tag_value
    }
    tags.uniq!

    #return tags.size == 1 ? "Preserve #{tags.join(";")}" : "Encap/Decap #{tags.join("-")}"
    if tags.size == 0
      return 'TBD'
    elsif tags.size == 1
      return 'Encap/Decap'
    else
      # More than one tag found

      tags.each do |t|
        # Handle Routing case
        return 'Routing' if t.match(/(^(\d{1,3}\.){3}(\d{1,3})\/(\d{1,2})$)/)
      end

      tags.each do |t|
        # Handle cases where tags are not numbers
        return 'TBD' if t == 'TBD'
        return 'Encap/Decap' if t.to_i == 0
      end
      
      # More than one tag and both are numbers
      if self.kind_of? OnNetOvc
        
        encaps = []
        segmenteps = segment_end_points.reject{|e| e.is_connected_to_test_port}
        segmenteps.each{|segmentep|
          encaps << segmentep.port_encap_type_onovce
        }
        encaps.uniq!
        
        if encaps.size == 1 or is_in_standard_path
          return 'Remap'
        else
          return 'Encap/Decap'
        end

      else
        return 'Encap/Decap'
      end

    end
  end

  # Used to correct Egress stats for FLR calculation
  # Overridden in subclasses
  def ccm_frames_generated_internally_per_minute
    return 0
  end

  def is_in_tunnel_path
    e = paths.reject{|path| path.cenx_path_type != "Tunnel"}
    return e.empty? ? false : true
  end

  def is_in_ls_core_transport_path
    e = paths.reject{|path| path.cenx_path_type != "Light Squared Core Transport"}
    return e.empty? ? false : true
  end

  def is_in_standard_path
    e = paths.reject{|path| path.cenx_path_type != "Standard"}
    return e.empty? ? false : true
  end

  def valid_paths?
    #Tunnel Path Check
    paths.each do |path|
      if path.cenx_path_type == 'Tunnel' and path.segments.size > 1
        errors.add :base, "Tunnel Paths can not contain more than one Segment, paths"
      end
    end

    #Duplication Checks
    paths.each do |path|
      if path.segments.select { |segment| segment == self  }.size > 1
        errors.add :base, "Can not add Segment to the same Path more than once"
      end
    end
  end

  def valid_cosis?
    cos_instances.each do |cosi|
      duplicates = cos_instances.reject{|c| c.equal?(cosi) }.select {|c| c.cos_name == cosi.cos_name}
      errors.add :base, "Can not add another Cos Intance of type #{cosi.cos_name} because it already exists" if duplicates.any?
    end
  end

  def valid_emergency_contact?
    if emergency_contact and operator_network and not service_provider.contacts.include? emergency_contact
      errors.add :base, "Emergency contact with ID #{emergency_contact_id} is not associated with Segment's Service Provider #{service_provider.name}"
    end
  end

  private :operator_network
  
  def update_alarm(list_of_evaled_objects = [], full_eval = false)
    changed = super
    # need to re-eval the EP's - If it's a LightSquared core transport a Brix Alarm *may* mean the other endpoint in the Segment must also
    # change it's state so we have to evaluate that end point as well. The other endpoint depends on the Segment state so this must be done
    # after updating the Segment state (hence after super above)
    if changed
      segment_end_points.each {|ep| ep.eval_alarms(list_of_evaled_objects, full_eval)}
      paths.each{|path| path.eval_alarms(list_of_evaled_objects, full_eval)}    
    end
    return changed
  end

  # KPI methods
  def update_kpi(parent, parent_kpi, kpi_name, kpi_object)
    on = get_operator_network
    if parent_kpi == nil
      case kpi_object
      when BxCosTestVector.name, SpirentCosTestVector.name
        parent = service_provider
        parent_kpi = on.get_kpi(kpi_name, kpi_object, parent)
      when OnNetOvcEndPointEnni.name
        parent = on
        parent_kpi = site.get_kpi(kpi_name, kpi_object, parent)
      else
        SW_ERR "Invalid kpi_object #{kpi_object} for #{self.class}:#{self.id}"
      end
    end
    kpi = calculate_kpi(kpi_name, kpi_object, parent_kpi, on)
    case kpi_object
    when BxCosTestVector.name, SpirentCosTestVector.name
      #ctvs = segment_end_points.collect {|ep| ep.cos_end_points.collect {|cep| cep.cos_test_vectors.first}}.flatten.compact
      ctvs = kpi_object.constantize.joins(:cos_end_point => {:segment_end_point => :segment}).where("`segments`.`id` = ?", self).uniq
      ctvs.each do |ctv|
        ctv.update_kpi(self, kpi, kpi_name, kpi_object)
      end
    when OnNetOvcEndPointEnni.name
      on_net_ovc_end_point_ennis.each do |ep|
        ep.update_kpi(self, kpi, kpi_name, kpi_object)
      end
    else
      SW_ERR "Invalid kpi_object #{kpi_object}"
    end
  end
  
end
