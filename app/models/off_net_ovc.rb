# == Schema Information
#
# Table name: segments
#
#  id                       :integer(4)      not null, primary key
#  type                     :string(255)
#  status                   :string(255)
#  notes                    :text
#  segment_owner_role           :string(255)
#  segment_type_id              :integer(4)
#  cenx_id                  :string(255)
#  operator_network_id      :integer(4)
#  site_id              :integer(4)
#  service_id               :integer(4)
#  created_at               :datetime
#  updated_at               :datetime
#  ethernet_service_type_id :integer(4)
#  use_member_attrs         :boolean(1)
#  path_network_id           :integer(4)
#  emergency_contact_id     :integer(4)
#  event_record_id          :integer(4)
#  sm_state                 :string(255)
#  sm_details               :string(255)
#  sm_timestamp             :integer(8)
#  prov_name                :string(255)
#  prov_notes               :text
#  prov_timestamp           :integer(8)
#  order_name               :string(255)
#  order_notes              :text
#  order_timestamp          :integer(8)
#

class OffNetOvc < Ovc
  
  # SIN convenience association
  has_many :ovc_end_point_unis, 
           :class_name => 'OvcEndPointUni', 
           :foreign_key => 'segment_id',
           :dependent => :destroy

  # SIN convenience association
  has_many :ovc_end_point_ennis, 
           :class_name => 'OvcEndPointEnni', 
           :foreign_key => 'segment_id',
           :dependent => :destroy
  
   validates_presence_of :operator_network_id

   def cenx_name
    onovce_info = "#{segment_end_points.size}-end-points"
    return "#{short_cenx_name}:#{has_monitored_endpoints ? "Mon" : "Not-Mon"}:#{onovce_info}"
   end

   def short_cenx_name
     type = CenxNameTools::CenxNameHelper.generate_type_string self
     buyer_sp_name = CenxNameTools::CenxNameHelper.shorten_name owner_info_segment
     oper_sp_name = CenxNameTools::CenxNameHelper.shorten_name operator_info_segment
     return "#{type}:#{cenx_id}:#{buyer_sp_name}(Own):#{oper_sp_name}(Oper)"
   end

   def site_info_segment
     sites = []
     if get_operator_network
       get_operator_network.enni_news.each do |e|
         sites << e.site.name
       end
     end
     return sites.flatten.uniq.join("; ")
  end
  
end
