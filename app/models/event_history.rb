# == Schema Information
# Schema version: 20110802190849
#
# Table name: event_histories
#
#  id             :integer(4)      not null, primary key
#  event_filter   :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  alarm_mapping  :text
#  retention_time :integer(4)
#

class EventHistory < ActiveRecord::Base  
  has_many :event_records, :dependent => :destroy
  has_many :event_ownerships, :dependent => :destroy
  before_save :default_retention_time

  # Moved from alarm history due to issues with polymorphic relationships on single table inheritance objects
  serialize :alarm_mapping  
  
  # Need to validate uniqness of node_id and event_filter
  validates_uniqueness_of :event_filter
  
  def default_retention_time
    self.retention_time = 3*30 if retention_time == nil # 3 months in days
  end
  
  def eventable_type=(sType)
    super(sType.to_s.classify.constantize.base_class.to_s)
  end
  
  
  # Class method to take an event see if this class handles it and if it does
  # find all the instances that match the event info and add the event to them
  def self.process_event(event)
    return 
  end
  
  def add_event(time_of_event)
    EventRecord.create(:time_of_event => time_of_event, :event_history => self)
  end
  

end
