# == Schema Information
#
# Table name: demarcs
#
#  id                               :integer(4)      not null, primary key
#  type                             :string(255)
#  cenx_id                          :string(255)
#  cenx_demarc_type                 :string(255)
#  notes                            :text
#  operator_network_id              :integer(4)
#  address                          :string(255)
#  port_name                        :string(255)
#  physical_medium                  :string(255)
#  connected_device_type            :string(255)
#  connected_device_sw_version      :string(255)
#  reflection_mechanism             :string(255)
#  mon_loopback_address             :string(255)
#  end_user_name                    :string(255)
#  is_new_construction              :boolean(1)
#  contact_info                     :string(255)
#  created_at                       :datetime
#  updated_at                       :datetime
#  demarc_type_id                   :integer(4)
#  max_num_segments                 :integer(4)
#  ether_type                       :string(255)
#  protection_type                  :string(255)
#  lag_id                           :integer(4)
#  fiber_handoff_type               :string(255)
#  auto_negotiate                   :boolean(1)
#  ah_supported                     :boolean(1)
#  lag_mac_primary                  :string(255)
#  emergency_contact_id             :integer(4)
#  lag_mac_secondary                :string(255)
#  port_enap_type                   :string(255)     default("QINQ")
#  lag_mode                         :string(255)
#  cir_limit                        :integer(4)      default(100)
#  eir_limit                        :integer(4)      default(300)
#  cenx_name_prefix                 :string(255)
#  demarc_icon                      :string(255)     default("building")
#  on_behalf_of_service_provider_id :integer(4)
#  event_record_id                  :integer(4)
#  sm_state                         :string(255)
#  sm_details                       :string(255)
#  sm_timestamp                     :integer(8)
#  prov_name                        :string(255)
#  prov_notes                       :text
#  prov_timestamp                   :integer(8)
#  order_name                       :string(255)
#  order_notes                      :text
#  order_timestamp                  :integer(8)
#  near_side_clli                   :string(255)     default("TBD")
#  far_side_clli                    :string(255)     default("TBD")
#  latitude                         :decimal(9, 6)   default(0.0)
#  longitude                        :decimal(9, 6)   default(0.0)
#  site_id                          :integer(4)
#


class Demarc < InstanceElement
  include Eventable
  include Stateable
  
  belongs_to :operator_network #structural
  belongs_to :demarc_type
  belongs_to :emergency_contact, :class_name => "Contact"
  belongs_to :on_behalf_of_service_provider, :class_name => "ServiceProvider", :foreign_key => "on_behalf_of_service_provider_id"
  
  has_many :uploaded_files, :as => :uploader, :dependent => :destroy
  has_many :service_orders, :as => :ordered_entity, :dependent => :destroy
  has_many :segment_end_points, :class_name => 'SegmentEndPoint', :dependent => :nullify
  has_many :ports, :dependent => :nullify, :order => "name"
  has_many :path_displays, :as => :entity, :dependent => :destroy

  validates_presence_of :demarc_icon
  validates_inclusion_of :demarc_icon, :in => HwTypes::DEMARC_ICONS.map{|disp, value| value},
                         :message => "invalid, Use: #{HwTypes::DEMARC_ICONS.map{|disp,value| value}.join(", ")}"
  validates_inclusion_of :cenx_demarc_type, :in => HwTypes::DEMARC_TYPES.map{|disp, value| value },
                         :message => "invalid, Use: #{HwTypes::DEMARC_TYPES.map{|disp, value| value }.join(", ")}"
  validates_format_of :lag_mac_primary, :lag_mac_secondary,
                      :with => /(^$)|(^(([A-Fa-f0-9]{2}):){5}([A-Fa-f0-9]{2})$)/,
                      :message => "Should be left empty or strict MAC address format xx:xx:xx:xx:xx:xx "
  validates_as_cenx_id :cenx_id
  
  scope :filter_cenx_demarcs, lambda { |cenx_demarcs| where(["EXISTS (SELECT * FROM `operator_networks`
                                                 LEFT JOIN `service_providers` ON `service_providers`.`id` = `operator_networks`.`service_provider_id`
                                                 WHERE `operator_networks`.id = `demarcs`.`operator_network_id`
                                                   AND IFNULL(`service_providers`.is_system_owner,0) = ?)",cenx_demarcs]) }
  
  scope :turbo_path_count, select("demarcs.*").
                          select("COUNT(DISTINCT paths.id) as turbo_path_count").
                          joins('LEFT OUTER JOIN `segment_end_points` ON `segment_end_points`.`demarc_id` = `demarcs`.`id`
                                 LEFT OUTER JOIN `segments` ON `segments`.`id` = `segment_end_points`.`segment_id`
                                 LEFT OUTER JOIN `paths_segments` ON `paths_segments`.`segment_id` = `segments`.`id`
                                 LEFT OUTER JOIN `paths` ON `paths`.`id` = `paths_segments`.`path_id`').
                          group("demarcs.id")
  
  scope :turbo_prov_name, select("(SELECT `states`.`name` FROM `stateful_ownerships` 
                                          INNER JOIN `statefuls` ON `statefuls`.`id` = `stateful_ownerships`.`stateful_id` 
                                          INNER JOIN `states` ON `states`.`stateful_id` = `statefuls`.`id` 
                                          WHERE `stateful_ownerships`.`stateable_id` = `demarcs`.`id` 
                                            AND `stateful_ownerships`.`stateable_type` = 'Demarc'
                                            AND `statefuls`.`type` LIKE '%ProvStateful'
                                          ORDER BY `states`.`timestamp` DESC
                                          LIMIT 1) as turbo_prov_name")
  
  def self.is_system_owner_demarc
    filter_cenx_demarcs(true)
  end
  
  def self.only_member_owned
    filter_cenx_demarcs(false)
  end

  # Default layer is 2
  def layer
    2
  end
  
	before_validation(:on => :create) do
    self.cenx_id = IdTools::CenxIdGenerator.generate_cenx_id(self.class)
  end
  
  # Assoc service provider to the proper paths and ennis for indexing
  after_save :assoc_service_providers_for_indexing_entities

  def tma_enabled_logic
    !segment_end_points.any?{|segmentep| segmentep.segment.get_operator_network == operator_network && !segmentep.segment.use_member_attrs}
  end
  
  after_save :general_after_save
  
  def general_after_save
    lo = get_latest_order
    if lo
      lo.ordered_entity_snapshot = attr_snapshot
      lo.save
    end
  end
  
  def effected_members
    segment_end_points.uniq.map{|e|e.effected_members}.flatten.uniq
  end

  #---------------------------------
  # Start Instance Element Code
  #---------------------------------

  belongs_to :type_element, :class_name => "DemarcType", :foreign_key => "demarc_type_id"
  
  def member_handle_owners
    owners = service_provider ? [service_provider] : []
    segment_end_points.each{|segmentep|
      owners += segmentep.segment.member_handle_owners
    }
    
    # We want cenx included for demarc case (not ENNI tho)
    return owners.uniq
    
  end

  def member_handle_prefix owner
    owner.name + "'s "
  end

  #---------------------------------
  # End Instance Element Code
  #---------------------------------

  #Demarcs don't have any dynamic content in the Cenx Name
  def cenx_name
    return cenx_name_prefix
  end

  before_save :general_before_save
  
  def general_before_save
    self.cenx_name_prefix = build_cenx_name_prefix
  end

  def build_cenx_name_prefix

    if ! operator_network
      return "To Be determined"
    end

    short_sp_name = CenxNameTools::CenxNameHelper.shorten_name sp_info_demarc
    short_phy_type = CenxNameTools::CenxNameHelper.shorten_phy_type physical_medium

    type = CenxNameTools::CenxNameHelper.generate_type_string self
    name = "#{type}:#{short_sp_name}:#{short_phy_type}:#{member_handle}:#{port_name}"

    unless demarc_type_id == 0 || demarc_type_id == nil
      name += ":[#{demarc_type.name}]"
    end
    return name
    
  end

  def bx_act_name
    # WARNING THIS CODE SHOULDNT BE CHANGED
    # Please Talk to Dave Schubet first if you need to change this code
     if ! operator_network
      return "To Be determined"
    end

    type = CenxNameTools::CenxNameHelper.generate_type_string self
    if self.kind_of? EnniNew
      name = "#{cenx_name}"
    else
      short_sp_name = CenxNameTools::CenxNameHelper.shorten_name sp_info_demarc
      name = "#{type}:#{short_sp_name}"
    end

    unless demarc_type_id == 0 || demarc_type_id == nil
      name += ":[#{demarc_type.name}]"
    end
    return name

  end
  
  def member_handle
    handle = MemberAttr.where("affected_entity_id is NULL and affected_entity_type = ?", self.class.table_name.classify).first
    if handle.nil?
      SW_ERR("No member handle for class #{self.class.table_name.classify}")
      return "ERROR!"
    end
    member_handle_instance(handle.name, service_provider).try(:value)
  end
  
  def member_handle=(mh)
    handle = MemberAttr.where("affected_entity_id is NULL and affected_entity_type = ?", self.class.table_name.classify).first
    if handle.nil?
      SW_ERR("No member handle for class #{self.class.table_name.classify}!")
      return "ERROR!"
    end
    if member_handle_instance_exists(handle.name, service_provider)
      return member_handle_instance(handle.name, service_provider).value = mh
    else
      SW_ERR("Tried to assign a member handle to a Demarc with CENX as owner.") unless mh.nil? || mh.empty?
      return nil
    end
  end

  def pull_down_name
    return "#{cenx_name} -- #{cenx_id}"
  end

  def rate(direction = :egress)
    return HwTypes::PHY_TYPES_RATE[physical_medium]
  end

  def self.find_all_demarcs
    where("cenx_demarc_type != ?", "ENNI").order("id")
  end

  def self.find_member_demarcs
    return find_all_demarcs.reject {|e| e.is_system_owner_internal_demarc}
  end

  def self.find_cenx_demarcs
    demarcs = find_all_demarcs.reject {|e| !e.is_system_owner_internal_demarc}
  end

  def self.find_all_ennis
    where("cenx_demarc_type = ?", "ENNI").order("demarcs.id")
  end

  def self.find_member_ennis
    find_all_ennis.only_member_owned
  end

  def self.find_cenx_ennis
    ennis = find_all_ennis.is_system_owner_demarc
  end

  def self.find_cenx_nm_ennis
    ennis = find_cenx_ennis.reject {|e| (e.is_connected_to_monitoring_port)}
  end
  
  def get_latest_order
    service_orders.sort{|a, b| b.created_at <=> a.created_at}.first
  end
  
  def active_order?
    order = get_latest_order
    if order
      current_state = order.get_order_state
      ![OrderDelivered, OrderRejected, OrderCancelled].any? {|order_state| current_state.is_a?(order_state)}
    end
  end
  
  def get_candidate_contacts
    contacts = []
    contacts += (operator_network && operator_network.service_provider) ? operator_network.service_provider.contacts : []
    contacts += (site_id && site.operator_network.service_provider) ? site.operator_network.service_provider.contacts : []
    return contacts.flatten
  end
  
  def ordered_entity_group
    service_orders.empty? ? nil : get_latest_order.ordered_entity_group
  end
  
  def get_possible_candidate_demarc_types
    if operator_network_id != nil
      demarc_types = DemarcType.where("operator_network_type_id = ? AND type IS NULL", operator_network.operator_network_type_id)
    else
      return []
    end
  end
  protected :get_possible_candidate_demarc_types
  
  def get_candidate_demarc_types
    return ordered_entity_group ? get_possible_candidate_demarc_types & ordered_entity_group.ordered_entity_types : get_possible_candidate_demarc_types
  end
  
  def sp_info_demarc
    operator_network ? operator_network.service_provider.name : "TBD"
  end

  def on_behalf_of_info_demarc
    on_behalf_of_service_provider.try(:name)
  end


  def service_provider
    operator_network ? operator_network.service_provider : nil
  end
  
  def emergency_contact_number
    emergency_contact ? emergency_contact.work_phone : "TBD"
  end
  
  def get_valid_segment_list
    return operator_network.segments
  end

  def get_mon_loopback_address
    return mon_loopback_address
  end
  
  def demarc_type_name
    return demarc_type ? demarc_type.name : "Not Available"
  end

  def is_system_owner_internal_demarc
    return operator_network ? operator_network.service_provider.is_system_owner : false
  end
  
  def supports_eth_oam_reflection
    return cenx_demarc_type == "ENNI" || cenx_demarc_type == "UNI" && ["DMM/DMR", "LBM/LBR"].include?(reflection_mechanism)
  end

  def end_point_count
    return segment_end_points.size
  end

  def path_count
    paths.count
  end
  
  def paths
    Path.
      select('distinct paths.*').
      joins(:segments => {:segment_end_points => :demarc}).
      where('demarcs.id = ?', self.id).all
  end
  
  def site_type
    # Must be a better way..
    segment_end_points.collect {|ep| ep.cos_end_points.collect {|cep| cep.cos_test_vectors.collect {|ctv| ctv.site_type}}}.flatten.first
  end
      
  #Active Record Callbacks
  
  def go_in_service
    sm_histories << SmHistory.create(:event_filter => "SM #{self.class.name}:#{cenx_id}") if sm_histories.empty?
    if mtc_periods.empty?
      self.mtc_periods = [MtcHistory.create(:event_filter => "Mtc Demarc:#{cenx_id}")]
      self.set_mtc(false)
    end
    return {:result => true, :info => ""}
  end
  
  after_create :general_after_create
  
  def general_after_create
    # Create Service Monitoring history
    update_attributes(:sm_state => AlarmSeverity::NOT_LIVE.to_s, :sm_details => "", :sm_timestamp => (Time.now.to_f*1000).to_i)
    sm_histories << SmHistory.create(:event_filter => "SM #{self.class.name}:#{cenx_id}")
    # create the Provisioning state
    statefuls << DemarcProvStateful.create
    state = prov_state.get_state
    update_attributes(:prov_name => state.name , :prov_timestamp => state.timestamp, :notes => state.notes)
    event_record_create
  end
  
  def assoc_service_providers_for_indexing_entities
    # populate Sphinx Access Point so we have uniqure rows for each member handle (which sphinx requires)
    segment_end_points.each do |endpoint|
      endpoint.segment.paths.each do |path|
        path.assoc_service_providers_for_indexing_paths
      end
    end
  end
  
  def update_alarm(list_of_evaled_objects = [], full_eval = false)
    changed = super
    segment_end_points.each {|ep| ep.eval_alarms(list_of_evaled_objects, full_eval)} if changed
    return changed
  end
  
end
