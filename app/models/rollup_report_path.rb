class RollupReportPath < ActiveRecord::Base
  include CommonException
  belongs_to :demarc
  belongs_to :site
  has_many :member_attr_instances, :primary_key => :demarc_id, :foreign_key => :affected_entity_id, :conditions => ['affected_entity_type = ?', 'Demarc']

  def metric_percentage(count_total)
    "%0.02f" % ((count_total.to_f / self.metric_count.to_f) * 100.0)
  end

  def metric_ok_count
    self.metric_count - self.metric_minor_count - self.metric_major_count
  end

  def role
    case self.protection_path_id
    when 1
      return 'P'
    when 2
      return 'B'
    else
      return ''
    end
  end
end
# == Schema Information
#
# Table name: rollup_report_paths
#
#  id                 :integer(4)      not null, primary key
#  period_start_epoch :integer(4)
#  pop_id             :integer(4)
#  demarc_id          :integer(4)
#  protection_path_id :integer(4)
#  severity           :string(0)
#  metric_count       :integer(4)
#  metric_minor_count :integer(4)
#  metric_major_count :integer(4)
#

