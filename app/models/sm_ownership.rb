# == Schema Information
# Schema version: 20110802190849
#
# Table name: sm_ownerships
#
#  id            :integer(4)      not null, primary key
#  smable_id     :integer(4)
#  smable_type   :string(255)
#  sm_history_id :integer(4)
#

class SmOwnership < ActiveRecord::Base
belongs_to :smable, :polymorphic => true
belongs_to :sm_history, :dependent => :destroy

after_create :general_after_create

def general_after_create
  # Add the initial state based on the values associated with this history
  sm_history.add_event(smable.sm_timestamp, sm_history.alarm_mapping.key(smable.sm_state), smable.sm_details)  
end

end
