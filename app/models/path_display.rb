# == Schema Information
#
# Table name: path_displays
#
#  id                        :integer(4)      not null, primary key
#  path_id                   :integer(4)      not null
#  entity_id                 :integer(4)      not null
#  entity_type               :string(255)     not null
#  row                       :integer(4)      not null
#  column                    :string(255)     not null
#  created_at                :datetime
#  updated_at                :datetime
#  relative_position         :string(255)
#  service_provider_id       :integer(4)
#  redacted                  :boolean(1)
#  latitude                  :decimal(9, 6)
#  longitude                 :decimal(9, 6)
#  site_type                 :string(255)
#  demarc_icon               :string(255)
#  owner_service_provider_id :integer(4)
#  component_type            :string(255)
#  site_id                   :integer(4)
#

class PathDisplay < ActiveRecord::Base
  COLUMNS_MAX = 11 
  ROWS_MAX = 2
  
  belongs_to :path
  belongs_to :entity, :polymorphic => true
  belongs_to :service_provider
  has_many :site_groups_sites, :foreign_key => :site_id
  has_many :site_groups, :through => :site_groups_sites, :foreign_key => :site_id

  scope :by_row, lambda { |row|
    { :conditions => { :row => row } }
  }
  
  scope :by_service_provider, lambda { |service_provider|
    { :conditions => {:service_provider_id => service_provider.id} }
  }
  
  scope :not_redacted, :conditions => {:redacted => false}
  
  # Get all components to draw map (demarcs for markers and segments for lines)
  scope :network_map, not_redacted.where("(latitude IS NOT NULL AND longitude IS NOT NULL) OR entity_type = 'Segment'").order("path_displays.path_id, path_displays.row, path_displays.column")
      
  # track if component is part of a multi-endpoint
  attr_accessor :multi_end_point
  attr_accessor :injections
  attr_accessor :reflection_direction
  attr_accessor :monitoring
  attr_accessor :slice_pointer
  attr_accessor :component_group_direction
  
  def reference 
    column.to_s + row.to_s
  end
  
  def segment?
    entity_type == 'Segment'
  end

  def on_net_ovc?
    segment? && entity.is_a?(OnNetSegment)
  end

  def on_net_router?
    segment? && entity.is_a?(OnNetRouter)
  end
  
  def on_net?
    entity.service_provider.is_system_owner?
  end

  def off_net?
    !on_net?
  end
  
  def demarc?
    entity_type == 'Demarc'
  end
  
  def end_point?
    entity_type == 'SegmentEndPoint'
  end

  def enni?
    entity.class.name == 'EnniNew'
  end

  def end_point_uni?
    entity.class.name == 'OvcEndPointUni'
  end
  
  def uni?
    # TODO: not sure why I have to convert entity.type to a string now 
    ['Uni', 'Demarc'].include?(entity.class.name)
  end
  
  def evc
    path
  end

  # COX HACK: Pass in prev and next to determine which of the new building icons to use
  def entity_class(mode, prev_column=nil, next_column=nil)

    cell_classes = []
    
    cell_classes << case entity_type
      when "SegmentEndPoint" then relative_position == 'Left' ? "segment_end_point_left" : "segment_end_point_right"
      else entity_type.underscore
    end

    cell_classes << component_type.underscore if component_type

    # Segments are special so the color against the component, not the td
    # TODO: Redo this special case for segments and state color
    cell_classes << state_class(mode) if !segment? || on_net_router?

    # Segments are special so the color against the component, not the td
    cell_classes << "clickable_during_edit" unless segment? || end_point?
    
    cell_classes << "on_net" if on_net_ovc?
    
    # add demarc icon class
    cell_classes << "demarc_icon #{entity.demarc_icon ? entity.demarc_icon.downcase.gsub(' ','_') : ''}_icon" if demarc?

    # Add on classes for styling and javascript
    cell_classes << "row_#{row.to_s} #{entity_data_reference} entity"
    
    cell_classes.join(' ')

  end

  def vertical_line_class(mode, previous_column)
    cell_classes = [state_class(mode)]
    cell_classes << (previous_column.try(:entity) ? 'left' : 'right')
    cell_classes << "on_net" if on_net_ovc?    
    cell_classes.join(' ')
  end

  def data_set_cell_class(previous_column, next_column)
    cell_classes = [multi_end_point ? 'vertical_line_info' : 'static_info']
    cell_classes << component_type.underscore if component_type
    # on net segments are visually small so they have expand overtop of endpoints
    if component_type == 'OnNetRouter'
      if previous_column.try(:entity).nil? && next_column.try(:entity).present?
        cell_classes <<  'overlap_right'
      elsif previous_column.try(:entity).present? && next_column.try(:entity).nil?
        cell_classes <<  'overlap_left'
      end
    end
    cell_classes.join(' ')
  end
  
  def entity_state(mode)
    t = case mode
    when 'ordering'
      service_order.try(:order_state_for_diagram)
    when 'inventory'
      entity.prov_state_for_diagram 
    when 'monitoring'
      entity.get_SM_state.try(:state_css_classify)
    end
  end
  
  # def get_SM_state(monitoring_mode=true)
  #   if monitoring_mode
  #     entity.get_SM_state
  #   else
  #     entity.prov_state_for_diagram 
  #   end
  # end
  
  def state_class(mode)
    state = case mode
    when 'ordering'
      state = service_order.try(:order_state_class)
    when 'inventory'
      state = entity.prov_state_class
    when 'monitoring'
      state = "monitoring #{entity_state(mode)}"
    when 'editing'
      state = 'editing'
    end
    
    " #{state.blank? ? 'transparent' : state} #{unique_entity_id} "
    
  end
  
  def service_order
    entity.respond_to?(:get_latest_order) ? entity.get_latest_order : nil
  end
  
  def entity_demarc_type
    if entity.is_a?(EnniNew)
      'Enni'
    else
      ''
    end
  end
  
  def diagram_id
    "#{id}_#{entity.class.name}_#{entity.id}"
  end

  def unique_entity_id
    "#{entity.class.name}_#{entity.id}"
  end

  def entity_data_reference
    "#{column.to_s}#{row.to_s}#{relative_position.to_s}"
  end
  
  # is entity reference (ie A1) in Diagram?
  def is_referenced?(entity) 
    # if relative position set (ie Left or Right for End points) then do not reference in diagram
    entity.relative_position.blank?
  end
  
end
