# == Schema Information
#
# Table name: sites
#
#  id                     :integer(4)      not null, primary key
#  name                   :string(255)
#  address                :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  contact_info           :string(255)
#  site_host              :string(255)
#  floor                  :string(255)
#  clli                   :string(255)
#  icsc                   :string(255)
#  network_address        :string(255)
#  broadcast_address      :string(255)
#  host_range             :string(255)
#  hosts_per_subnet       :string(255)
#  notes                  :text
#  node_prefix            :string(255)
#  postal_zip_code        :string(255)
#  emergency_contact_info :string(255)
#  site_access_notes      :text
#  city                   :string(255)
#  state_province         :string(255)
#  site_type              :string(255)
#  latitude               :decimal(9, 6)   default(0.0)
#  longitude              :decimal(9, 6)   default(0.0)
#  type                   :string(255)
#  operator_network_id    :integer(4)
#  country                :string(255)
#  service_id_low         :integer(4)
#  service_id_high        :integer(4)
#  site_layout            :string(255)     default("StandardExchange7750")
#
class AggregationSite < Site
  validates_presence_of :site_layout
  validate :valid_service_ids?

  SERVICE_ID_REQUIRED_SITES = [
     "StandardExchange7750",
     "StandardExchange7450",
  ]

  private
  def valid_service_ids?
    if SERVICE_ID_REQUIRED_SITES.include? site_layout
      validates_presence_of :service_id_low, :service_id_high
      validates_range_of :service_id_low, :service_id_high, :overlapping => false, :greater_than_or_equal_to => 0, :only_integer => true, :unique_ends => true
    else
      self.service_id_low = 0 if service_id_low.nil?
      self.service_id_high = 49999 if service_id_high.nil?
    end
  end
  
end
