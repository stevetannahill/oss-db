# == Schema Information
# Schema version: 20110802190849
#
# Table name: qos_policies
#
#  id          :integer(4)      not null, primary key
#  type        :string(255)
#  policy_name :string(255)
#  policy_id   :integer(4)
#  site_id :integer(4)
#  created_at  :datetime
#  updated_at  :datetime
#

class QosPolicy < ActiveRecord::Base
  belongs_to :site
  has_and_belongs_to_many :on_net_ovc_end_point_ennis
  validate :segmenteps_on_site

  validates_presence_of :policy_name, :policy_id, :site_id

  validates_uniqueness_of :policy_name, :scope => [:type, :site_id]
  validates_uniqueness_of :policy_id, :scope => [:type, :site_id]


  before_validation :generate_policy_id

  def generate_policy_id
    return unless policy_id.nil?

    if policy_name =~ /Default BV3000 \w+ policy/
      if not self.class.where("site_id = ? and policy_id = ?", site.id, 65535).empty?
        error = "Testing QoS Policy ID already taken"
        SW_ERR error
        errors.add(error)
      else
        self.policy_id = 65535
        return
      end
    end
    
    (1000..655534).each do |id|
      if self.class.where("site_id = ? and policy_id = ?", site.id, id).empty?
        self.policy_id = id
        return
      end
    end

    #No policy found (really bad!)
    error = "No QoS policy IDs remaining for site #{site.name}"
    SW_ERR error
    errors.add(error)
  end

  def self.get_subclass direction
    return EgressQosPolicy if direction == "E"
    return IngressQosPolicy if direction == "I"
  end

  def direction
    policy = get_own_policy
    return policy.direction
  end

  def classes_of_service
    policy = get_own_policy
    return policy.classes_of_service
  end

  def version
    policy = get_own_policy
    return policy.version
  end

  def associated_endpoints_count
    return on_net_ovc_end_point_ennis.size
  end

  def site_name
    return site.name
  end

  def is_on_mini_site?
    return site.is_mini_site?
  end

  def node_names
    names = on_net_ovc_end_point_ennis.collect do |ep|
      ep.segment.node_names
    end

    names = names.flatten.uniq

    return names
  end

  def node_info
    return node_names.join(";")
  end

  def get_index_of name
    if policy_id == 65535
      name = "Rate limit to 1Mbps to prevent accidentally flooding Segment"
    end
    config = Config.new(get_own_policy)
    config.get_index_of name
  end

  private
  
  def get_own_policy
    p =  QosPolicyHelper.decode_policy_name(policy_name)
    p.policy_primary_key = id
    return p
  end

  def segmenteps_on_site
    on_net_ovc_end_point_ennis.each do |onovcee|
      if onovcee.segment.site != site
        errors.add(:base,"On Net OVCE @ ENNI #{onovcee.cenx_name} is not on the QoS Policie's Site #{site.name}. Something has gone horribly wrong.")
      end
    end
  end

  #The following Classes are for generating the QoS Policy config
  
  #This class keeps track of all the parts of the config and is also
  # a parent to the important classes, IngressQosPolicy::Config and EgressQosPolicy::Config
  class Config
    def initialize policy
      rlm_to_class = {
      "Shaping" => Shaper,
      "Policing" => Policer,
      "None" => RateLimiter
      }
      
      @policy = policy
      #A hash of arrays
      @sections = Hash.new {|hash, key| hash[key] = Array.new}

      #Build our shapers and policers
      single_cos = policy.classes_of_service.one?
      policy.classes_of_service.each do |cos|
        mechanism = rlm_to_class[cos.rlm]
        index = @sections[cos.rlm].size + 1
        @sections[cos.rlm] << mechanism.new(cos, index, single_cos, self)
      end
    end

    #The base config for all config types.
    def generate_config
      return "description \"#{@policy.name}\"\n"
    end

    def get_qos_policy
      p =  QosPolicy.find @policy.policy_primary_key
      if !p
        SW_ERR "Cant find QosPolicy (#{@policy.policy_primary_key})"
      end
      return p
    end

    def ingress_qos_policy_config_name
      return 'sap-ingress'
    end

    def egress_qos_policy_config_name
      return get_qos_policy.is_on_mini_site? ? 'access-egress' : 'sap-egress'
    end

    def ingress_qos_entity_config_name
      return get_qos_policy.is_on_mini_site? ? 'meter' : 'queue'
    end

    def egress_qos_entity_config_name
      return 'queue'
    end

    def egress_dot1p_marking_config_name
      return get_qos_policy.is_on_mini_site? ? 'dot1p-in-profile' : 'dot1p'
    end

    def get_index_of name
      @sections.each do |mechanism, objects|
        match = objects.select { |o| o.cos.name == name }.first
        return match.index unless match.nil?
      end
    end
  end


  #This is a parent class for Policer or Shaper which handles common code
  # it also handles the case where no rate limiting is used.
  class RateLimiter
    attr_reader :cos, :index
    def initialize class_of_service, index, single_cos, parent
      @cos = class_of_service
      @index = index
      @single_cos = single_cos
      @parent = parent
    end

    #This function is used for the ingress final dot1p section
    # here we fill in our fwding_class
    def register_mapping roster
      @cos.mapping.each do |fc, map|

        break if map == "*"
        
        map.split(',').each do |s|
          m = s.to_i
          roster[m] = fc
        end
      end

      return roster
    end

    #Return a properly formatted string with our attributes
    def get_description
      cir = (@cos.cir_kbps == 0 ? "0" : "#{@cos.cir_mbps}M")
      eir = (@cos.eir_kbps == 0 ? "0" : "#{@cos.eir_mbps}M")

      cbs = "#{@cos.cbs_kB}kB"
      cbs = "-" if @cos.cbs_kB.nil?
      cbs = "0" if @cos.cbs_kB == 0

      ebs = "#{@cos.ebs_kB}kB"
      ebs = "-" if @cos.ebs_kB.nil?
      ebs = "0" if @cos.ebs_kB == 0

      return "#{@cos.name}:#{cir}:#{cbs}:#{eir}:#{ebs}"
    end

    #Create a valid setup section config for this policer (None)
    def generate_config
      return "" if @single_cos
      config = ""
      config += "policer #{@index} create\n"
      config += "description \"#{get_description}\"\n"
      config += "exit\n"
      return config
    end

    #Return a proper fc section for this rate limiter on an ingress config
    def generate_ingress_fc_config
      config = ""
      @cos.mapping.keys.each do |fwding_class|
        config += "fc \"#{fwding_class}\" create\n"
        config += "policer #{@index}\n" unless @single_cos
        config += "exit\n"
      end
      return config
    end

    #Return a proper fc section for this rate limiter on an egress config
    def generate_egress_fc_config
      config = ""
      @cos.mapping.each do |fwding_class, map|
        config += "fc \"#{fwding_class}\" create\n"
        config += "policer #{@index}\n" unless @single_cos
        config += "#{@parent.egress_dot1p_marking_config_name} #{map}\n" unless map == "-"
        config += "exit\n"
      end
      return config
    end

    #This function is called when we encounter the '*' character
    def default_fc
      config = "default-fc \"#{@cos.mapping.first[0]}\"\n"
      return config
    end
  end

  #This class will create valid config for a policed section
  class Policer < RateLimiter

     #Create a valid setup section config for this policer (Policer)
    def generate_config
      config = ""
      config += "policer #{@index} create\n"
      config += "description \"#{get_description}\"\n"
      config += "rate #{@cos.cir_kbps + @cos.eir_kbps} cir #{@cos.cir_kbps}\n"
      config += "mbs #{@cos.cbs_kB + @cos.ebs_kB} kilobytes\n" unless @cos.cbs_kB.nil? or @cos.ebs_kB.nil?
      config += "cbs #{@cos.cbs_kB} kilobytes\n" unless @cos.cbs_kB.nil?
      config += "high-prio-only 0\n"
      config += "packet-byte-offset subtract 4\n"
      config += "exit\n"
      return config
    end

    #Return a proper fc section for this policer on an ingress config
    def generate_ingress_fc_config
      config = ""
      @cos.mapping.each do |fwding_class, map|
        config += "fc \"#{fwding_class}\" create\n"
        config += "policer #{@index}\n"
        config += "multicast-policer #{@index}\n"
        config += "broadcast-policer #{@index}\n"
        config += "unknown-policer #{@index}\n"
        config += "exit\n"
      end
      return config
    end

    #Return a proper fc section for this policer on an egress config
    def generate_egress_fc_config
      config = ""
      @cos.mapping.each do |fwding_class, map|
        config += "fc \"#{fwding_class}\" create\n"
        config += "policer #{@index}\n"
        config += "#{@parent.egress_dot1p_marking_config_name} #{map}\n" unless map == "-"
        config += "exit\n"
      end
      return config
    end
  end

  #This class created valid config for a shaped section
  class Shaper < RateLimiter

    #Create a valid setup section config for this queue (Shaper)
    def generate_config
      config = ""
      config += "queue #{@index} create\n"
      config += "description \"#{get_description}\"\n"
      config += "rate #{@cos.cir_kbps + @cos.eir_kbps} cir #{@cos.cir_kbps}\n"
      config += "mbs #{@cos.cbs_kB + @cos.ebs_kB} kilobytes\n" unless @cos.cbs_kB.nil? or @cos.ebs_kB.nil?
      config += "cbs #{@cos.cbs_kB} \n" unless @cos.cbs_kB.nil?
      config += "high-prio-only 0\n"
      config += "exit\n"
      return config
    end

    #Return a proper fc section for this shaper on an ingress config
    def generate_ingress_fc_config
      config = ""
      @cos.mapping.each do |fwding_class, map|
        config += "fc \"#{fwding_class}\" create\n"
        config += "queue #{@index}\n"
        config += "multicast-queue 11\n"
        config += "broadcast-queue 11\n"
        config += "unknown-queue 11\n"
        config += "exit\n"
      end
      return config
    end

    #Return a proper fc section for this shaper on an egress config
    def generate_egress_fc_config
      config = ""
      @cos.mapping.each do |fwding_class, map|
        config += "fc \"#{fwding_class}\" create\n"
        config += "queue #{@index}\n"
        config += "#{@parent.egress_dot1p_marking_config_name} #{map}\n" unless map == "-"
        config += "exit\n"
      end
      return config
    end
  end
end
