# == Schema Information
#
# Table name: service_orders
#
#  id                          :integer(4)      not null, primary key
#  type                        :string(255)
#  title                       :string(255)
#  cenx_id                     :string(255)
#  action                      :string(255)
#  primary_contact_id          :integer(4)
#  testing_contact_id          :integer(4)
#  requested_service_date      :date
#  order_received_date         :date
#  order_acceptance_date       :date
#  order_completion_date       :date
#  customer_acceptance_date    :date
#  billing_start_date          :date
#  operator_network_id         :integer(4)
#  expedite                    :boolean(1)
#  status                      :string(255)
#  notes                       :text
#  ordered_entity_id           :integer(4)
#  ordered_entity_type         :string(255)
#  created_at                  :datetime
#  updated_at                  :datetime
#  ordered_entity_subtype      :string(255)
#  path_id                     :integer(4)
#  ordered_entity_group_id     :integer(4)
#  ordered_operator_network_id :integer(4)
#  order_state                 :string(255)
#  ordered_object_type_id      :integer(4)
#  ordered_object_type_type    :string(255)
#  ordered_entity_snapshot     :text
#  technical_contact_id        :integer(4)
#  local_contact_id            :integer(4)
#  foc_date                    :date
#  bulk_order_type             :string(255)
#  design_complete_date        :date
#  order_created_date          :date
#  order_name                  :string(255)
#  order_notes                 :text
#  order_timestamp             :integer(8)
#


class ComponentOrder < ServiceOrder
  belongs_to :ordered_entity_group
  has_many :bulk_orders_service_orders_containing, :foreign_key => :service_order_id, :class_name => "BulkOrdersServiceOrder"
  has_many :containing_bulk_orders, :through => :bulk_orders_service_orders_containing, :source => :bulk_order
  
  belongs_to :ordered_object_type, :polymorphic => true
  
  validates_presence_of :ordered_entity_id, :unless => Proc.new{|so| so.action == "New"}, :message => "must be specified for a Change or Delete Order"
  validates_presence_of :ordered_object_type_id, :ordered_object_type_type, :unless => Proc.new{|so| so.ordered_entity_group.nil?}, :message => "must be specified when an Ordered Entity Group is specified"
  validates_inclusion_of :action, :in => ServiceProviderTypes::ORDER_TYPES.map{|disp, value| value },
                         :message => "invalid, Use: #{ServiceProviderTypes::ORDER_TYPES.map{|disp, value| value }.join(", ")}"
  validates_inclusion_of :ordered_entity_type , :in => ServiceProviderTypes::ORDERED_ENTITY_TYPES.map{|disp, value| value },
                         :message => "invalid, Use: #{ServiceProviderTypes::ORDERED_ENTITY_TYPES.map{|disp, value| value }.join(", ")}"
  validates_inclusion_of :ordered_entity_subtype , :in => ServiceProviderTypes::ORDERED_ENTITY_SUBTYPES.map{|disp, value| value },
                         :message => "invalid, Use: #{ServiceProviderTypes::ORDERED_ENTITY_SUBTYPES.map{|disp, value| value }.join(", ")}"
  validates_inclusion_of :ordered_entity_type , :in => ServiceProviderTypes::ORDERED_ENTITY_TYPES.map{|disp, value| value },
                         :message => "invalid, Use: #{ServiceProviderTypes::ORDERED_ENTITY_TYPES.map{|disp, value| value }.join(", ")}"
  validates_inclusion_of :ordered_entity_subtype , :in => ServiceProviderTypes::ORDERED_ENTITY_SUBTYPES.map{|disp, value| value },
                         :message => "invalid, Use: #{ServiceProviderTypes::ORDERED_ENTITY_SUBTYPES.map{|disp, value| value }.join(", ")}"

  def type_element= te
    self.ordered_entity_group = te
  end
  
  def type_element_id= teid
    self.ordered_entity_group_id = teid
  end
 
  def type_elements
    ordered_entity_group ? [ordered_entity_group.order_type, ordered_entity_group] : []
  end
 
  def order_type
    ordered_entity_group ? ordered_entity_group.order_type : nil
  end
  
  def get_candidate_ordered_object_types
    return []
  end
  
  def oeg_info_so
    ordered_entity_group.name
  end

  def oet_info_so
    ordered_object_type.name
  end
  
  def get_candidate_ordered_entity_groups ot
    []
  end

  #Active Record Callbacks

  # See http://stackoverflow.com/questions/4138957/activerecordsubclassnotfound-error-when-using-sti-in-rails/4139245
  # and https://rails.lighthouseapp.com/projects/8994/tickets/2389
  if Rails.env == "development"
    def self.subclasses
       [SegmentOrder, DemarcOrder, OnNetOvcOrder, OffNetOvcOrder, EnniOrderNew]
    end
  end
end
