# == Schema Information
# Schema version: 20110802190849
#
# Table name: bulk_orders_service_orders
#
#  id               :integer(4)      not null, primary key
#  service_order_id :integer(4)
#  bulk_order_id    :integer(4)
#

class BulkOrdersServiceOrder < ActiveRecord::Base
  belongs_to :bulk_order
  belongs_to :service_order
  validate :bulk_order, :check_bo
  validate :service_order
  validates_uniqueness_of :service_order_id, :scope => :bulk_order_id, :message => "cannot contain duplicate orders"
  
  
  def check_bo
    bo = ServiceOrder.find(self.bulk_order_id)
    so = ServiceOrder.find(self.service_order_id)
    
    if bo.id == so.id
      errors[:base] << "A bulk order cannot contain itself"
    end
    
    if bo.bulk_order_type == "Circuit Order" && !so.is_a?(ComponentOrder)
      errors[:base] << "A Circuit Order cannot contain a Bulk Order"
    elsif bo.bulk_order_type != "Circuit Order" && so.is_a?(ComponentOrder)
      errors[:base] << "A non Circuit Order cannot contain a Component Order"
    end
  end
end
