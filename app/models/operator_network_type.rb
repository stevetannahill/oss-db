# == Schema Information
# Schema version: 20110802190849
#
# Table name: operator_network_types
#
#  id                  :integer(4)      not null, primary key
#  name                :string(255)
#  service_provider_id :integer(4)
#  created_at          :datetime
#  updated_at          :datetime
#  notes               :string(255)
#

class OperatorNetworkType < TypeElement

  belongs_to :service_provider
  
  has_many :operator_networks, :dependent => :nullify
  has_many :instance_elements, :class_name => "OperatorNetwork"
  has_many :oss_infos,:dependent => :destroy
  has_many :ethernet_service_types, :order => "name",:dependent => :destroy
  has_many :class_of_service_types,:dependent => :destroy
  has_many :path_types,:dependent => :destroy
  has_many :demarc_types, :order => "name",:dependent => :destroy
  has_many :uni_types, :order => "name",:dependent => :destroy
  has_many :enni_types, :order => "name",:dependent => :destroy
  has_many :segment_types, :order => "name",:dependent => :destroy
  has_many :segment_end_point_types, :order => "name",:dependent => :destroy
  has_many :order_types, :dependent => :destroy
  has_many :ordered_entity_groups, :through => :order_types
  
  validates_presence_of :name
  validates_uniqueness_of :name, :scope => :service_provider_id
  
  def sp_info_ont
    return service_provider.name
  end

  def oss_count
    return oss_infos.size
  end

  def est_count
    return ethernet_service_types.size
  end
end
