# == Schema Information
# Schema version: 20110802190849
#
# Table name: ethernet_service_types
#
#  id                       :integer(4)      not null, primary key
#  operator_network_type_id :integer(4)
#  name                     :string(255)
#  created_at               :datetime
#  updated_at               :datetime
#  notes                    :string(255)
#

class EthernetServiceType < TypeElement
  belongs_to :operator_network_type
  has_many :segments, :dependent => :nullify
  has_many :instance_elements, :class_name => "Segment"
  has_and_belongs_to_many :segment_types
  has_and_belongs_to_many :segment_end_point_types
  has_and_belongs_to_many :demarc_types
  has_and_belongs_to_many :class_of_service_types
  
  validates_presence_of :operator_network_type_id, :name
  validates_uniqueness_of :name, :scope => :operator_network_type_id
  
  def get_candidate_segment_types
    operator_network_type.segment_types.reject{|segmentt| segment_types.include? segmentt }
  end
  
  def get_candidate_segment_end_point_types
    operator_network_type.segment_end_point_types.reject{|segmentept| segment_end_point_types.include? segmentept }
  end
  
  def get_candidate_class_of_service_types
    operator_network_type.class_of_service_types.reject{|cost| class_of_service_types.include? cost }
  end
  
  def get_candidate_demarc_types
    operator_network_type.demarc_types.reject{|dt| demarc_types.include? dt }
  end
  
  def get_candidate_class_of_service_types
    operator_network_type.class_of_service_types.reject{|cost| class_of_service_types.include? cost }
  end

  def cost_count
    return class_of_service_types.size
  end

 def dt_count
   return demarc_types.size
 end

  def segment_count
    return segments.size
  end
  
end
