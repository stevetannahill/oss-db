# == Schema Information
# Schema version: 20110802190849
#
# Table name: cos_instances
#
#  id                       :integer(4)      not null, primary key
#  class_of_service_type_id :integer(4)
#  segment_id               :integer(4)
#  notes                    :string(255)
#  created_at               :datetime
#  updated_at               :datetime
#  type                     :string(255)
#

class CosInstance < InstanceElement
  belongs_to :segment #structural
  belongs_to :class_of_service_type
  belongs_to :type_element, :class_name => "ClassOfServiceType", :foreign_key => "class_of_service_type_id"
  has_many :cos_end_points, :dependent => :nullify

  validates_presence_of :segment_id
  validates_presence_of :class_of_service_type_id
  validates_uniqueness_of :class_of_service_type_id , :scope => :segment_id

  def get_candidate_costs
    return segment.get_candidate_costs
  end

  def cos_end_point_count
    return cos_end_points.size
  end

  def get_cenx_monitoring_cos_endpoints
     return []
  end

  def cos_id_type_enni
    return class_of_service_type ? class_of_service_type.cos_id_type_enni : '-'
  end

  def cos_id_type_uni
    return class_of_service_type ? class_of_service_type.cos_id_type_uni : '-'
  end

  def cos_mapping_enni_ingress
    return class_of_service_type ? class_of_service_type.cos_mapping_enni_ingress : '-'
  end

  def cos_mapping_uni_ingress
    return class_of_service_type ? class_of_service_type.cos_mapping_uni_ingress : '-'
  end

  def cos_marking_enni_egress
    return class_of_service_type ? class_of_service_type.cos_marking_enni_egress : '-'
  end

  def cos_marking_uni_egress
    return class_of_service_type ? class_of_service_type.cos_marking_uni_egress : '-'
  end

  def cos_name
    return class_of_service_type ? class_of_service_type.name : 'Unknown'
  end

  def stats_entities
    [segment]
  end

  def segment_name
    return segment.cenx_name
  end

  def ethernet_service_type_info
    segment.ethernet_service_type_info
  end

  def get_candidate_cos_mapping_enni_ingress
    class_of_service_type ? class_of_service_type.get_candidate_cos_mapping_enni_ingress : []
  end

  def get_candidate_cos_mapping_uni_ingress
    class_of_service_type ? class_of_service_type.get_candidate_cos_mapping_uni_ingress : []
  end

  def get_candidate_service_level_guarantee_types
    if class_of_service_type
      return class_of_service_type.service_level_guarantee_types
    else
      SW_ERR "No class_of_service_type defined on #{cos_name}"
      return []
    end
  end

  def get_bwp_at_locale locale
    all_bwps = class_of_service_type ? class_of_service_type.bw_profile_types : []
    match = all_bwps.reject{|b| b.bwp_type != locale }

    if match.size == 0
      # Match UNI/ENNI first
      match = all_bwps.reject{|b| !(b.bwp_type.include?(locale[0,3])) }
    end

    if match.size == 0
      # Match INGRESS/EGRESS second
      match = all_bwps.reject{|b| !(b.bwp_type.include?(locale[4,6])) }
    end
    
    if match.size == 0
      SW_ERR "get_bwp_at_locale #{locale} - no matches found"
    end

    return match.first
  end

  def sla_max_queue_delay
    # Subclass should implement
    SW_ERR "No function sla_queue_delay defined in #{self.class.to_s}. Returning 0."
    return 0
  end

  def get_sibling_cos_end_points id
    return cos_end_points.select{ |ce| (ce.id != id) }
  end

  # TODO Delete
#  def get_enni_cos_end_points
#    return cos_end_points.select{ |ce| (ce.segment_end_point.kind_of? OvcEndPointEnni) }
#  end

end
