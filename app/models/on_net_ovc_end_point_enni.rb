# == Schema Information
#
# Table name: segment_end_points
#
#  id                    :integer(4)      not null, primary key
#  type                  :string(255)
#  cenx_id               :string(255)
#  segment_id            :integer(4)
#  demarc_id             :integer(4)
#  segment_end_point_type_id :integer(4)
#  md_format             :string(255)
#  md_level              :string(255)
#  md_name_ieee          :string(255)
#  ma_format             :string(255)
#  ma_name               :string(255)
#  is_monitored          :boolean(1)
#  notes                 :string(255)
#  stags                 :string(255)
#  ctags                 :string(255)
#  oper_state            :string(255)
#  created_at            :datetime
#  updated_at            :datetime
#  eth_cfm_configured    :boolean(1)      default(TRUE)
#  reflection_enabled    :boolean(1)
#  mep_id                :string(255)     default("TBD")
#  event_record_id       :integer(4)
#  sm_state              :string(255)
#  sm_details            :string(255)
#  sm_timestamp          :integer(8)
#  prov_name             :string(255)
#  prov_notes            :text
#  prov_timestamp        :integer(8)
#  order_name            :string(255)
#  order_notes           :text
#  order_timestamp       :integer(8)
#

require "sam_api.rb"

class OnNetOvcEndPointEnni < OvcEndPointEnni
  include Kpiable

  has_and_belongs_to_many :qos_policies
  #validates_class_of :segment, :is_a => "OnNetOvc"

  validate :valid_tag_range?
  validate :valid_tag_values?
  validate :valid_tag_presence?
  validate :valid_qos_policies?
  validate :valid_coseps?
  
  after_save :update_kpis, :if => :segment_id_changed?

  def interface_name
    if is_connected_to_enni
	    return "#{demarc.interface_name}"
	  else
      return "TBD - interface not yet selected"
    end
  end

  def stats_displayed_name
    if is_connected_to_enni
      text = "#{demarc.stats_displayed_name}#{sap_encap_string}"
      if port_encap_type_onovce == 'DOT1Q' || port_encap_type_onovce == 'NULL'
        text += ".0"
      end
      return text
    else
      SW_ERR "Not Connetced to ENNI"
    end
  end

  def on_switch_name
    return "sap #{interface_name}#{sap_encap_string}"
  end

  def cenx_name
    return "#{super}:#{on_switch_name}"
  end

  def enni_mac_address
    enni ? enni.get_mac_address : "TBD"
  end

  def ingress_cir_kbps
    cos_end_points.collect { |c| c.ingress_cir_kbps }.sum
  end

  def ingress_eir_kbps
    cos_end_points.collect { |c| c.ingress_eir_kbps }.sum
  end

  def egress_cir_kbps
    cos_end_points.collect { |c| c.egress_cir_kbps }.sum
  end

  def egress_eir_kbps
    cos_end_points.collect { |c| c.egress_eir_kbps }.sum
  end

  def ingress_cir_Mbps
    ingress_cir_kbps.to_f / 1000
  end

  def ingress_eir_Mbps
    ingress_eir_kbps.to_f / 1000
  end

  def egress_cir_Mbps
    egress_cir_kbps.to_f / 1000
  end

  def egress_eir_Mbps
    egress_eir_kbps.to_f / 1000
  end

  def rate(direction = :egress)
    cos_end_points.collect { |c| c.rate_mbps(direction) }.sum
  end

  def suggest_valid_stag
    result = true
    valid_stag = 0

    unless is_connected_to_enni
      result = false
    else
      valid_stag = 1
      while demarc.has_endpoint_with_stag valid_stag.to_s
         valid_stag += 1
         if valid_stag > 4095
           result = false
           break
         end
      end
    end

    return result, valid_stag
  end

  def generate_mep_id
    
    if is_connected_to_test_port
      self.mep_id = "N/A"
      return
    end

    unless demarc_id
      self.mep_id = "TBD"
      return
    end

    segment = Segment.find(segment_id)

    if !segment.is_in_ls_core_transport_path
      segmenteps = segment.segment_end_points
      valid = (1..segmenteps.count+1).to_a

      segmenteps.each do |ep|
        meps = ep.get_mep_ids
        meps.each do |m|
          valid.reject! { |i| i == m.to_i }
        end
      end

      mep = valid.first.to_s

      mep = "#{mep};#{mep}1" if enni.is_multi_chassis
    else
      if segment.is_on_mini_site? && is_connected_to_another_endpoint?
        mep = "2;100"
      elsif is_connected_to_another_endpoint?
        mep = "1"
      else
        mep = "N/A"
      end
    end

    self.mep_id = mep
    
  end
  
  def class_get_candidate_endpoints_to_link_with
    base_get_candidate_endpoints_to_link_with.reject{|e| e.class != OvcEndPointEnni }
  end

  def is_connected_to_test_port
    return (is_connected_to_enni ? demarc.is_connected_to_test_port : false)
  end

  def is_connected_to_trouble_shooting_port
    return (is_connected_to_enni ? demarc.is_connected_to_trouble_shooting_port : false)
  end

  def is_connected_to_monitoring_port
    return (is_connected_to_enni ? demarc.is_connected_to_monitoring_port : false)
  end

  def get_monitoring_node
    if is_connected_to_monitoring_port
      return demarc.get_monitoring_node
    end
    SW_ERR "get_monitoring_node called on unmonitored endpoint"
    return nil
  end

  def exists_on_node node_name
    return (node_info.include? node_name)
  end

  def is_in_multi_cos_segment
    return segment.is_multi_cos_segment
  end

  def get_cenx_monitoring_segment_endpoints
    return segment.get_cenx_monitoring_segment_endpoints
  end

  def get_sibling_enni_endpoints
    return segment.on_net_ovc_end_point_ennis.reject {|ep| ep == self or ep.is_connected_to_test_port}
  end

  def get_ccm_monitoring_mep_ids
    ccm_meps = []

    if is_connected_to_test_port or segment.is_in_ls_core_transport_path
      return ccm_meps
    end
    
    ip_ping_monitoring_is_required, ccm_monitoring_is_required, lbm_monitoring_is_required, dmm_monitoring_is_required, monitored_remote_endpoint = segment.get_service_monitoring_params

    if ccm_monitoring_is_required
      enni_siblings = get_sibling_enni_endpoints
      enni_siblings.each do |ep|
        if ep.get_attached_monitored_offovc_endpoints.size != 0
          meps = get_mep_ids
          meps.each do |m|
            ccm_meps << "#{m.to_i*10}"
          end
        end
      end
    end
    
    return ccm_meps
  end

  def ccm_mep_id
    get_ccm_monitoring_mep_ids.join(';')
  end

  def all_mep_ids
    return "#{mep_id}#{ccm_mep_id.empty? ? '' : ' / '}#{ccm_mep_id}"
  end

  def generate_remote_meps_config
    meps = get_mep_ids
    text = ""
    if is_connected_to_enni
      text += "remote-mepid #{meps[0]}\n"
      if enni.is_multi_chassis
        text += "remote-mepid #{meps[1]}\n"
      end
    elsif is_connected_to_test_port
      text += "ERROR: #{on_switch_name} is TEST Port \n"
    else
      text += "remote-mepid #{meps[0]}\n"
    end
    return text
  end

  def generate_ccm_monitoring_remote_meps_config
    local_meps = get_ccm_monitoring_mep_ids
    text = ""
    local_meps.each do |lm|
      text += "remote-mepid #{lm}\n"
    end

    remote_meps = []
    enni_siblings = get_sibling_enni_endpoints
    enni_siblings.each do |ep|
      remote_meps += ep.get_attached_monitored_offovc_endpoints.collect {|c| c.mep_id}
    end

    remote_meps.flatten!
    remote_meps.each do |rm|
      text += "remote-mepid #{rm}\n"
    end

    return text
    
  end

  def get_attached_monitored_offovc_endpoints
    endpoints = []

    segment_end_points.each do |e|
      endpoints += e.segment.segment_end_points.reject{|x| x == e or !x.is_monitored}
    end

    return endpoints.flatten
  end

  def generate_sm_mac_fwd_filter
    return enni.generate_sm_mac_fwd_filter
  end

  def generate_sm_mac_drop_filter
    return enni.generate_sm_mac_drop_filter
  end

  def config_description
    return "#{is_connected_to_trouble_shooting_port ? "Trouble_Shooting " : "" }Endpoint on #{demarc_connection_info_segmentep} #{is_connected_to_enni ? "; #{enni.cenx_id}" : ""}"
  end

  def generate_end_point_config is_primary_node
    meps = get_mep_ids

    if enni.is_multi_chassis
      mep_id = (is_primary_node) ? meps[0] : meps[1]
    else
      mep_id = meps[0]
    end

    text = ""
    text += "\n# Service Endpoint Configuration\n"
    text += "#-------------------------------\n"
    text += "#{on_switch_name} create\n"

    if !is_connected_to_monitoring_port
      text += "description \"#{config_description}\"\n"
      ip_ping_monitoring_is_required, ccm_monitoring_is_required, lbm_monitoring_is_required, dmm_monitoring_is_required, monitored_remote_endpoint = segment.get_service_monitoring_params
      
      if eth_cfm_configured
        if !segment.is_on_mini_site?
          if !segment.is_in_ls_core_transport_path
            # Regular Segment on 7750 or 7450 (Core business or LS TAP)
            text += "eth-cfm\n"
            text += "mip\n"
            text += "mep #{mep_id} domain 2 association #{segment.service_id} direction up\n"
            text += "ais-enable\n"
            text += "exit\n"
            text += "one-way-delay-threshold 0\n" # This command enables/disables eth-test functionality on MEP.
            text += "eth-test-enable\n"
            text += "exit\n"
            text += "ccm-enable\n"
            text += "no shutdown\n"
            text += "exit\n"
            get_ccm_monitoring_mep_ids.each do |m|
              text += "mep #{m} domain #{monitored_remote_endpoint.get_ccm_monitoring_domain_id} association #{segment.service_id} direction up\n"
#              text += "ais-enable\n"
#              text += "exit\n"
              text += "ccm-enable\n"
              text += "no shutdown\n"
              text += "exit\n"
            end
            
            text += "exit\n"
          else
            # LS Core transport case on 7450
            if is_connected_to_another_endpoint?
              if meps.size != 1
                SW_ERR "#{cenx_name} wrong number of mep IDs: #{meps.size}"
                text += "!!!! ERROR wrong number of MEPS: #{meps.size}\n"
                return text
              end

              text += "eth-cfm\n"
              text += "mep #{meps[0]} domain 8 association #{segment.service_id} direction down\n"
              text += "ais-enable\n"
              text += "exit\n"
              text += "eth-test-enable\n"
              text += "exit\n"
              text += "ccm-enable\n"
              text += "no shutdown\n"
              text += "exit\n"
              text += "exit\n"
            end
          end

        else
          # Mini Site Case
          if is_connected_to_another_endpoint?
            if meps.size != 2
              SW_ERR "#{cenx_name} wrong number of mep IDs: #{meps.size}"
              text += "!!!! ERROR wrong number of MEPS: #{meps.size}\n"
              return text
            end
            
            text += "eth-cfm\n"
            text += "mep #{meps[0]} domain 8 association #{segment.service_id} direction down\n"
            text += "ais-enable\n"
            text += "exit\n"
            text += "eth-test-enable\n"
            text += "exit\n"
            text += "ccm-enable\n"
            text += "no shutdown\n"
            text += "exit\n"
            if is_connected_to_monitored_endpoint?

              unless segment_end_points.size == 1
                domain = 'ERROR wrong number of connected End Points'
                SW_ERR "#{cenx_name} wrong number of connected End Points: #{meps.size}"
              else
                domain = segment_end_points.first.md_level
              end

              text += "mep #{meps[1]} domain #{domain} association #{segment.service_id} direction down\n"
              text += "eth-test-enable\n"
              text += "exit\n"
              text += "no shutdown\n"
              text += "exit\n"
            end
            text += "exit\n"
          end
        end
      end
      
      ingress = qos_policies.select {|qosp| qosp.is_a? IngressQosPolicy}.first  
      if ingress
        text += "ingress qos #{ingress.policy_id}\n"
      else
        SW_ERR "ERROR: #{cenx_name} no ingress QoS policy found"
        text += "ERROR:  #{cenx_name} no ingress QoS policy found"
      end


      if !segment.is_on_mini_site?

        egress = qos_policies.select {|qosp| qosp.is_a? EgressQosPolicy}.first
        if egress
          text += "egress qos #{egress.policy_id}\n"
        else
          SW_ERR "ERROR: #{cenx_name} no egress QoS policy found"
          text += "ERROR:  #{cenx_name} no egress QoS policy found"
        end

        if ip_ping_monitoring_is_required
          remote_monitored_offovc_endpoints = []
          enni_siblings = get_sibling_enni_endpoints
          enni_siblings.each do |ep|
              remote_monitored_offovc_endpoints += ep.get_attached_monitored_offovc_endpoints
          end
          remote_monitored_offovc_endpoints.flatten!
          if !remote_monitored_offovc_endpoints.empty?
            text += "egress filter mac 65535\n"
          end
        end

       end

      if is_in_multi_cos_segment
        text += "collect-stats\n"
        text += "accounting-policy 2\n" #Per Queue Packet stats
      end

      text += "exit\n"
    else
      text += "description \"Service Monitoring Endpoint\"\n"

      # Not required because LTMs are multicast - will confuse matters
#      if eth_cfm_configured
#        text += "eth-cfm\n"
#        text += "mip\n"
#        text += "exit\n"
#      end

      text += "ingress qos 65535\n"
      text += "egress qos 65535\n"
      
      text += "egress filter mac 199\n"

      if is_in_multi_cos_segment
        text += "collect-stats\n"
        text += "accounting-policy 2\n" #SLA Packet stats
      end

      text += "exit\n"

    end
  end

  def qos_policies_correct?
    return false if qos_policies.empty?

    #Get existing policy_names by direction
    @existing_names = {}
    @existing_policies = {}
    qos_policies.each do |qosp|
      @existing_names[qosp.direction[0,1]] = qosp.policy_name
      @existing_policies[qosp.policy_name] = qosp
    end

    #Get new policy_names by direction
    @new_names = {}
    ["E", "I"].each do |direction|
      name = QosPolicyHelper.generate_policy_name self, direction
      @new_names[direction] = name
    end

    #If the old and new policies are identical we are done
    return @new_names == @existing_names

  end

  def configure_qos_policies
    
    update = Array.new

    #If we have no cos endpoints then that is very bad
    if cos_end_points.empty? and (not is_connected_to_monitoring_port)
      update << "Error: No Cos Endpoints"
      SW_ERR "On Net OVCE: Error, attempting to create QoS Policy for endpoint that contains no classes of service", :IGNORE_IF_TEST
      return update
    end

    if qos_policies_correct?
      update << "No Changes"
      update << qos_policies.collect do |qosp|
        "#{qosp.direction} policy #{qosp_html_link qosp} in use"
      end
      return update
    end

    #If we have no policies (first time config) then just create them
    if qos_policies.empty?
      update << "Attached 2 QoS policies"
      egress, estatus = get_qos_policy("E")
      ingress, istatus = get_qos_policy("I")
      qos_policies << egress
      qos_policies << ingress
      update << [estatus, istatus]
      return update
    end

    if qos_policies.size == 1
      direction = ["Ingress", "Egress"] - qos_policies.collect {|q| q.direction}
      direction = direction.first
      update << "Attached missing #{direction} policy"
      policy, status = get_qos_policy(direction[0,1])
      qos_policies << policy
      update << [status]
      return update
    end

    #Test each direction separately
    ["E", "I"].each do |direction|
      new_name = @new_names[direction]
      existing_name = @existing_names[direction]
      #Get the existing policy
      p = @existing_policies[existing_name]

      #Is this a clashing policy?
      if new_name != existing_name
        update << "#{p.direction} policy #{qosp_html_link p} changed"

        #Check if there already exists a new policy we need and if it's the only user of the policy or all other users changed too
        not_exist = QosPolicy.find_all_by_policy_name(new_name).empty?
        alone = p.on_net_ovc_end_point_ennis.size == 1
        all_change = p.on_net_ovc_end_point_ennis.collect {|e| QosPolicyHelper.generate_policy_name(e, p.direction[0,1]) == new_name}.uniq == [true]
        
        if not_exist and (alone or all_change)
          #If so we can change the policy
          
          p.policy_name = new_name
          p.save
          update << ["Changed current #{p.direction} policy from #{existing_name} [#{p.policy_id}] to #{qosp_html_link p}"]
        else
          #If not we must get a new policy
          qos_policies.delete(p)
          new_policy, status = get_qos_policy(direction)
          qos_policies << new_policy

          update << ["Removed association with #{p.direction} policy - #{qosp_html_link p}", status]
        end

        update << "Warning!: #{p.policy_name} is #{p.policy_name.size} characters. Only #{p.policy_name[0,80]} will be stored on switch" if p.policy_name.size > 80
      else
        update << "No changes to #{p.direction} policy #{qosp_html_link p}"
      end
    end
    return update
  end

  #This method will search for an existing QoS Policy or
  # create a new one if it is not found
  def get_qos_policy direction
    status = ""
    site_id = segment.site_id
    name = QosPolicyHelper.generate_policy_name self, direction
    qp = QosPolicy.where("policy_name = ? and site_id = ?", name, site_id)
    if qp.size > 1
      SW_ERR "Error more than one policy with name #{name} found"
    end
    qp = qp.first

    if qp.nil?
      qp = QosPolicy.get_subclass(direction).new(:policy_name => name, :site_id => site_id)
      error = "Qos Policy failed to save with name #{name}" unless qp.save
      if error.nil?
        status = "Created new #{qp.direction} policy - #{qosp_html_link qp}"
      else
        SW_ERR error
        status = error
      end
    else
      status = "Associated with existing #{qp.direction} policy - #{qosp_html_link qp}"
    end

    return qp, status

  end

  def get_provisioned_bandwidth exclude = []
    info = {}
    (cos_end_points - exclude).each do |cosep|
      next unless cosep.is_a? CenxCosEndPoint
      info[cosep.cos_name] = cosep.get_provisioned_bandwidth
    end
    return info
  end

  def get_alarm_keys
    return_code = true
    new_sap_alarms = []
    new_mep_alarms = []
    meg_levels = [2]
    
    # Get the SAM object name for this sap
    api = SamIf::StatsApi.new(get_network_manager)
    nodes_involved.each do |n|
      result, data = api.get_inventory(
        "vpls.L2AccessInterface",
        {"serviceId" => segment.service_id, "displayedName" => stats_displayed_name, "nodeId" => n.loopback_ip},
        ["objectFullName"])
      if !result || data.empty?
        SW_ERR "Failed to create alarm histories for Service: #{segment.service_id} SAP: #{stats_displayed_name} Node: #{node_info.to_s}"
        sap_object_name = ""
        return_code = false
      else
        sap_object_name = data[0]["objectFullName"] # There should only be one item
      end

      if !sap_object_name.empty?
        new_sap_alarms << sap_object_name
      end
    end
    api.close
    if !segment.is_in_ls_core_transport_path && return_code 
      # MEP alarms not associated w a SAP LightSquared Core case
      # Get the SAM MEP object name associated with this SAP
      return_code, new_mep_alarms = get_mep_alarm_keys(meg_levels)
    end    

    return return_code, new_sap_alarms, new_mep_alarms
  end
  
  def get_mep_alarm_keys(acceptable_meg_levels = (0..15).to_a)
    return_code = true
    new_mep_alarms = []
    # Get the SAM object name for this sap
    api = SamIf::StatsApi.new(get_network_manager)
    nodes_involved.each do |n|     
      result, data = api.get_inventory(
        "vpls.L2AccessInterface",
        {"serviceId" => segment.service_id, "displayedName" => stats_displayed_name, "nodeId" => n.loopback_ip},
        ["objectFullName"])
      if !result || data.empty?
        SW_ERR "Failed to get Sap Name for: #{segment.service_id} SAP: #{stats_displayed_name} Node: #{node_info.to_s}"
        sap_object_name = ""
        return_code = false
      else
        sap_object_name = data[0]["objectFullName"] # There should only be one item
      end
      
      result, data = api.get_inventory(
          "ethernetoam.Mep",
          {"maintenanceAssociationId" => segment.service_id, "sapPointer" => sap_object_name},
          ["objectFullName", "maintDomainLevel"])
      if !result || data.empty?
        SW_ERR "Failed to get MEP Name for: #{segment.service_id} SAP: #{stats_displayed_name} Node: #{node_info.to_s}"
        mep_object_names = []
        return_code = false
      else
        # Find all meps that are in the acceptable MEG level
        mep_object_names = data.collect{|mep_data| mep_data["objectFullName"] if acceptable_meg_levels.include?(mep_data["maintDomainLevel"].to_i)}.compact
      end

      if !mep_object_names.empty?
        new_mep_alarms += mep_object_names
      end
    end
    api.close

    return return_code, new_mep_alarms
  end    
  

  def go_in_service
    return_code = true
    info = ""
    if is_connected_to_test_port 
      # Delete any existing histories
      alarm_histories.destroy_all
      sm_histories.destroy_all
      info = "Failed to initialise" unless return_code
      return {:result => return_code, :info => info}
    end
    
    # For MEP alarms map the major alarm to Warning rather than UNAVAILABLE
    # critical alarms still map to Unavailable
    mep_mapping = AlarmSeverity.default_alu_alarm_mapping 
    mep_mapping["major"] = AlarmSeverity::WARNING

    sm_histories << SmHistory.create(:event_filter => "SM #{self.class.name}:#{cenx_id}") if sm_histories.empty?

    return_code, new_sap_alarms, new_mep_alarms = get_alarm_keys
    
    if return_code
      new_alarms = {} 
      new_mep_alarms.each {|filter| new_alarms[filter] = {:type => AlarmHistory, :mapping => mep_mapping}}
      new_sap_alarms.each {|filter| new_alarms[filter] = {:type => AlarmHistory, :mapping => AlarmSeverity.default_alu_alarm_mapping}}
      return_code = update_alarm_history_set(new_alarms)
    end
    info = "Failed to initialise" unless return_code
    return {:result => return_code, :info => info}
  end  
  
  def alarm_state        
    final_state = nil
    enni_state = nil
    if !enni.nil?
      enni_state = enni.event_record
    end
    
    if !enni.nil? && enni.is_multi_chassis && !is_connected_to_test_port
      # for MultiChassis LAG add the alarm histroies on each node and then OR the two together
      alarms_by_ip = {}      
      alarm_histories.each do |ah|
        ip = /\b(?:[0-9]{1,3}\.){3}[0-9]{1,3}\b/.match(ah.event_filter).to_s
        alarms_by_ip[ip] = ah.alarm_state + alarms_by_ip[ip]
      end
      
      final_state = AlarmRecord.select_lowest_alarm_or_warn(alarms_by_ip.values)
      if final_state == nil
        final_state = AlarmRecord.new(:time_of_event => (Time.now.to_f*1000).to_i, :state => AlarmSeverity::NO_STATE, :original_state => "No State", :details => "Error cannot calculate alarm state")
        SW_ERR "Unable to calculate #{self.class.name} alarm state probably due to missing alarm histories #{alarm_histories.collect {|ah| ah.event_filter}.inspect}"
      end 
      
    else
      # Just add all alarm histories 
      final_state = AlarmRecord.highest(alarm_histories.collect {|ah| ah.alarm_state})
      # If there are no histories then return OK
      final_state = AlarmRecord.new(:time_of_event => 0, :state => AlarmSeverity::OK, :details => "", :original_state => "cleared") if final_state == nil
     # final_state = super
    end
    # If the ENNI state is Failed or Unavailable then the endpoint is Dependency
    # If the ENNI state is Warning(ie one port of a lag has gone down) then the endpoint is OK
    final_state = calculate_alarm_state([final_state, enni_state])    
    return final_state
  end  
  
  def alarm_history    
    final_history = nil
    enni_history = nil
    if !enni.nil?
      enni_history = enni.alarm_history
    end
    if !enni.nil? && enni.is_multi_chassis && !is_connected_to_test_port
      # for MultiChassis LAG  - add the alarm histroies for each SAP and then "and" two SAP histories   
      ah_by_ip = {}      
      alarm_histories.each do |ah|
        ip = /\b(?:[0-9]{1,3}\.){3}[0-9]{1,3}\b/.match(ah.event_filter).to_s
        ah_by_ip[ip] = ah + ah_by_ip[ip]
      end 
      # Now merge based on the lowest alarm or if the other node is Unavailable then make it a warning
      final_history = AlarmHistory.merge(ah_by_ip.values) {|events| AlarmRecord.select_lowest_alarm_or_warn(events)}   
    else
      # Just add all alarm histories for non multi-chassis
      final_history = super      
    end
    # Now take into account the ENNI state
    final_history = AlarmHistory.merge([final_history, enni_history]) {|events| calculate_alarm_state(events)}   
    
    return final_history
  end
  
  def monitored?
    # Cenx endpoints are always monitored
    return true 
  end

  def sla_availability_value

    #TODO Demo Hack
    if is_connected_to_enni
      if enni.sla_availability_value < 0.999 && segment.segment_end_points.size > 3
        return 1
      end
    end
    
    if !is_connected_to_test_port
      if is_connected_to_enni
        return enni.sla_availability_value
      end
    end
    return 1
  end

  def outer_tag_value
    if stags == '0'
      return 'null'
    end
    return stags 
  end

  def supports_eth_oam_reflection
    return false
  end
  
  # KPI methods
  def kpi_hierarchy(kpi_object)
    on = segment.get_operator_network
    site = segment.site
    sp = on.service_provider    
    return [[self, segment],[segment, site],[site, on],[on, sp],[sp, sp]]
  end
  
  def update_kpi(parent, parent_kpi, kpi_name, kpi_object)
    if parent_kpi == nil
      parent_kpi = segment.get_kpi(kpi_name, kpi_object, segment.get_operator_network)
    end
    calculate_kpi(kpi_name, kpi_object, parent_kpi, segment)
  end  
  
  def update_kpis
    # The segment_id has been changed so update the KPIs
    if segment != nil
      Kpi::KPI_BY_KLASS[OnNetOvcEndPointEnni.name].each do |kpi_name|
        kpi_setup(kpi_name, OnNetOvcEndPointEnni.name)
      end
    end
  end
  

  protected

  def get_candidate_ennis
     #candidate_ennis = segment.site.demarcs.reject{|e| segment.is_connected_to_demarc e }
     # Can have >1 SAP on same ENNI (ex for support of 2 stags) 
     candidate_ennis = segment.site.demarcs
  end

  private

  def valid_tag_range?
    if is_connected_to_enni
      if is_null_encap
        if stags == nil || (stags != "TBD" && stags != '*' && stags.to_i != 0 && !stags.match(/(^(\d{1,3}\.){3}(\d{1,3})\/(\d{1,2})$)/))
          errors.add(:stags, "Should be 0 or \'*\' or TBD or strict IP/CIDR  (was #{stags})")
        end
      elsif stags == nil || (stags != "TBD" && stags != '*' && (stags.to_i < 1 || stags.to_i > 4095))
        errors.add(:stags, "Should be 1-4095 or \'*\' or TBD (was #{stags})")
      end

      if is_qinq_encap
        if ctags == nil || ( ctags != "TBD" && ctags != '*' && (ctags.to_i < 1 || ctags.to_i > 4095))
          errors.add(:ctags, "Should be 1-4095 or \'*\' or TBD (was #{ctags})")
        end
      end
    else
      return true
    end
  end

  def valid_coseps?
    if cos_end_points.reject{ |c| c.is_a? CenxCosEndPoint }.any?
      errors.add(:base,"All CoS Endpoints on a On Net OVC Endpoint must be of type Cenx Cos Endpoint")
    end
  end

  def valid_tag_presence?
    if is_qinq_encap
      if (ctags.nil? or ctags.to_s.empty?) or (stags.nil? or stags.to_s.empty?)
        errors.add(:base,"Must supply both Inner and Outer tags for QINQ encap")
      end
    elsif is_connected_to_demarc
      if stags.nil? or stags.to_s.empty?
        errors.add(:base,"Must supply Outer tag for #{port_encap_type_onovce} encap")
      end
    end
  end

  def valid_tag_values?
    if stags == nil
      return
    end
    
    if is_qinq_encap
      other_qinq_endpoints = enni.segment_end_points.reject{|e| e == self }
      other_qinq_endpoints.each do |e|
        if (e.stags != "TBD" && e.stags != "N/A") && e.sap_encap_string == sap_encap_string
          errors.add(:base,"Encap #{sap_encap_string} is already assigned on ENNI #{demarc.cenx_name}")
          break
        end
      end
    else
      if (stags != "TBD" && stags != "N/A") && ((is_connected_to_enni) && (demarc.has_endpoint_with_stag stags))
        if( (demarc.get_endpoint_with_stag stags).id != self.id )
          errors.add(:base,"Encap #{stags} is already assigned on ENNI #{demarc.cenx_name}")
        end
      end
    end

  end
  
  def valid_qos_policies?
    ingress = qos_policies.select { |policy| policy.is_a? IngressQosPolicy}
    egress = qos_policies.select { |policy| policy.is_a? EgressQosPolicy}

    errors.add(:base,"Can not have more than 2 QoS Policies") if qos_policies.size > 2
    errors.add(:base,"Can not have more than 1 Ingress QoS Policy") if ingress.size > 1
    errors.add(:base,"Can not have more than 1 Egress QoS Policy") if egress.size > 1
  end
  
  #Helper function for displaying the info of a policy as a link
  def qosp_html_link policy
    return "<a href='/admin/edit_qosp/#{policy.id}'>#{policy.policy_name} [#{policy.policy_id}]</a>".html_safe
  end
  
  # Calculate if the endpoint is Dependency or not
  # If the ENNI state is Failed or Unavailable then the endpoint is Dependency
  # If the ENNI state is Warning(ie one port of a lag has gone down) then the endpoint is OK
  def calculate_alarm_state(events)
    events.compact!
    if events.size < 2
      return events.first.dup
    end
    enni_state = events[1]
    endpoint_state = events[0].dup
    # If the ENNI state is Failed or Unavailable then the endpoint is Dependency
    # If the ENNI state is Warning(ie one port of a lag has gone down) then the endpoint is OK
    if !enni_state.nil?
      if enni_state.state >= AlarmSeverity::FAILED && enni_state.state <= AlarmSeverity::UNAVAILABLE
        endpoint_state.state = AlarmSeverity::DEPENDENCY
        endpoint_state.details = enni_state.details
      elsif enni_state.state == AlarmSeverity::WARNING
        endpoint_state.state = AlarmSeverity::OK
        endpoint_state.details = ""
      else
        endpoint_state = (endpoint_state + enni_state).dup
      end
    end
    
    return endpoint_state
  end
  
end

