# == Schema Information
#
# Table name: cos_end_points
#
#  id                         :integer(4)      not null, primary key
#  segment_end_point_id       :integer(4)
#  cos_instance_id            :integer(4)
#  ingress_mapping            :string(255)
#  ingress_cir_kbps           :string(255)
#  ingress_eir_kbps           :string(255)
#  ingress_cbs_kB             :string(255)
#  ingress_ebs_kB             :string(255)
#  created_at                 :datetime
#  updated_at                 :datetime
#  connected_cos_end_point_id :integer(4)
#  type                       :string(255)
#  egress_marking             :string(255)
#  egress_cir_kbps            :string(255)
#  egress_eir_kbps            :string(255)
#  egress_cbs_kB              :string(255)
#  egress_ebs_kB              :string(255)
#  ingress_rate_limiting      :string(255)     default("Policing")
#  egress_rate_limiting       :string(255)     default("None")
#  event_record_id            :integer(4)
#  sm_state                   :string(255)
#  sm_details                 :string(255)
#  sm_timestamp               :integer(8)
#


class CenxCosEndPoint < CosEndPoint
  validates_class_of :segment_end_point, :is_a => "OnNetOvcEndPointEnni"
  validates_class_of :cos_instance, :is_a => "CenxCosInstance"
  
  validate :valid_data_for_type?
  
  # HACK: Should be removed once our randomize can properly assign ennis
  # validate :valid_enni_cac_limits?

  def self.get_default_burst_rate(cosi, info_rate, method)
    return "" if (info_rate.nil? or info_rate.empty?)
    info_rate = info_rate.to_i if info_rate.is_a? String
    
    values = ServiceCategories::ON_NET_OVC_COS_TYPES[cosi.cos_name][method].keys
    values = values.reject {|v| v < info_rate }.sort
    value = ServiceCategories::ON_NET_OVC_COS_TYPES[cosi.cos_name][method][values.first]
    return value
  end

  def is_system_owner?
    return true
  end

  def sla_max_queue_delay

    # TODO handle A-Z Vs Z-A directions
    value_usec = 0

    # Only shapers add delay
    if egress_rate_limiting != "Shaping"
      return value_usec
    end

    # Doesnt count if test port SAP
    if not is_test?
      cir_bits_per_usec = (egress_cir_kbps.to_f)*1000/1000000
      eir_bits_per_usec = (egress_eir_kbps.to_f)*1000/1000000
      cir_queue_size_bits = (egress_cbs_kB.to_f)*1000*8
      eir_queue_size_bits = (egress_ebs_kB.to_f)*1000*8

      value_usec += cir_queue_size_bits/cir_bits_per_usec if cir_bits_per_usec > 0
      value_usec += eir_queue_size_bits/eir_bits_per_usec if eir_bits_per_usec > 0
    end

    return value_usec
  end

  def get_cenx_monitoring_cos_endpoints
    return cos_instance.get_cenx_monitoring_cos_endpoints
  end

  def get_provisioned_bandwidth
    infinity = (+1.0/0.0)
    demarc = segment_end_point.demarc
    ingress_cir = (ingress_rate_limiting == "None" ? infinity : ingress_cir_Mbps.to_f)
    ingress_eir = (ingress_rate_limiting == "None" ? infinity : ingress_eir_Mbps.to_f)
    return {
      :cir => ingress_cir,
      :eir => ingress_eir,
      :cir_rate => (ingress_cir / demarc.rate) * 100,
      :eir_rate => (ingress_eir / demarc.rate) * 100
    }
  end

  def get_RL_info
    policies = segment_end_point.qos_policies
    ingress = policies.select { |p| p.is_a? IngressQosPolicy  }.first
    egress = policies.select { |p| p.is_a? EgressQosPolicy }.first

    if ingress.nil? or egress.nil?
      SW_ERR "ERROR: Must generate policies before checking the RL info"
      return [nil, nil]
    end
    
    ingress_type = (ingress_rate_limiting == "Shaping" ? "queue" : "policer")
    egress_type = (egress_rate_limiting == "Shaping" ? "queue" : "policer")
    ingress_id = ingress.get_index_of cos_name
    egress_id = egress.get_index_of cos_name

    return [[ingress_type, ingress_id], [egress_type, egress_id]]
    
  end

  # Return monitoring manager connected directly to me (ie: not thru my chain)
  def get_connected_monitoring_manager
    node = segment_end_point.get_monitoring_node
    if node
      return node.network_manager
    end

    SW_ERR "Monitoring Manager Doesnt Exist! on CenxCosEndPoint: #{pulldown_name}"
    return nil
  end
  
  private

  def valid_enni_cac_limits?
    return if is_test?
    
    demarc = segment_end_point.demarc
    return unless demarc.is_a? EnniNew

    #Build the additional information
    if ingress_rate_limiting == "None"
      infinity = (+1.0/0.0)
      cir_cac_limit = ServiceCategories::ON_NET_OVC_COS_TYPES[cos_name][:cir_cac_limit]
      eir_cac_limit = ServiceCategories::ON_NET_OVC_COS_TYPES[cos_name][:eir_cac_limit]
      additional = {cos_name => {
        :cir => infinity,
        :eir => infinity,
        :cir_rate => cir_cac_limit.to_f,
        :eir_rate => eir_cac_limit.to_f
      }}
    else
      additional = {cos_name => {
        :cir => ingress_cir_Mbps.to_f,
        :eir => ingress_eir_Mbps.to_f,
        :cir_rate => (ingress_cir_Mbps.to_f / demarc.rate) * 100,
        :eir_rate => (ingress_eir_Mbps.to_f / demarc.rate) * 100
      }}
    end

    results = demarc.get_provisioned_bandwidth(additional, [self])
    errs = demarc.check_provisioned_bandwidth(results)

    errs.each do |error|
      errors.add(:base,error)
    end
  end

  def valid_data_for_type?
    #HACK: FAILING WHEN TRYING TO BUILD A STANDARD COS WHERE THE CIR/EIR MUST BE ZERO BUT THE MONIRTOING SETS IT TO 1000
    return true
    
    return if is_test?
    
    #Make sure one of CBS or EBS is non zero
    if ingress_cbs_kB == "0" and ingress_ebs_kB == "0"
      errors.add(:base,"Either CBS or EBS must be non zero on ingress, selection")
    end
    if egress_cbs_kB == "0" and egress_ebs_kB == "0"
      errors.add(:base,"Either CBS or EBS must be non zero on egress, selection")
    end

    if ingress_cbs_kB.nil? and ingress_ebs_kB.to_f > 0
      errors.add(:base,"Can not have ebs > 0 with no CBS, ingress")
    end
    if egress_cbs_kB.nil? and egress_ebs_kB.to_f > 0
      errors.add(:base,"Can not have EBS > 0 with no CBS, egress")
    end

    info = ServiceCategories::ON_NET_OVC_COS_TYPES[class_of_service_type.name]
    #Make sure that data follows the CAC Limits
    if ingress_rate_limiting != "None"
      if ingress_cir_kbps != nil and !ingress_cir_kbps.empty? and ingress_cir_kbps != "0" and info[:cir_cac_limit].zero?
        errors.add(:base,"Ingress CIR must be 0 (was #{ingress_cir_kbps}) for cos type #{cos_name} (#{self.inspect}), CIR")
      end

      if ingress_eir_kbps != nil and !ingress_eir_kbps.empty? and ingress_eir_kbps != "0" and info[:eir_cac_limit].zero?
        errors.add(:base,"Ingress EIR must be 0 (was #{ingress_eir_kbps}) for cos type #{cos_name}, EIR")
      end
    end

    if egress_rate_limiting != "None"
      if egress_cir_kbps != nil and !egress_cir_kbps.empty? and egress_cir_kbps != "0" and info[:cir_cac_limit].zero?
        errors.add(:base,"Egress CIR must be 0 (was #{egress_cir_kbps}) for cos type #{cos_name}, CIR")
      end

      if egress_eir_kbps != nil and !egress_eir_kbps.empty? and egress_eir_kbps != "0" and info[:eir_cac_limit].zero?
        errors.add(:base,"Egress EIR must be 0 (was #{egress_eir_kbps}) for cos type #{cos_name}, EIR")
      end
    end
  end

  public
  # KPI methods These will be moved to kpiable

  def get_utilization_threshold_mbps(direction, severity_level)
    rate = rate_mbps(direction)
    percent = get_utilization_threshold_percent(direction, severity_level) 
    return (rate *  percent.to_f)/100
  end
  
  def get_utilization_threshold_percent(direction, severity_level)   
    case severity_level
    when :minor
      0.5
    when :major
      0.75
    else
      0.0
    end
  end

  def get_utilization_errored_period_threshold(severity_level, metric)
    return 5
  end

  def get_utilization_errored_period_delta_time_secs
    return 300
  end

  def get_utilization_errored_period_max_errored_count(severity_level, metric)
    return 5
  end


end
