# == Schema Information
#
# Table name: segment_types
#
#  id                                  :integer(4)      not null, primary key
#  name                                :string(255)
#  segment_type                            :string(255)
#  mef9_cert                           :string(255)
#  mef14_cert                          :string(255)
#  max_mtu                             :integer(4)
#  c_vlan_cos_preservation             :boolean(1)
#  c_vlan_id_preservation              :string(255)
#  s_vlan_cos_preservation             :boolean(1)
#  s_vlan_id_preservation              :string(255)
#  color_marking                       :string(255)
#  color_marking_notes                 :string(255)
#  unicast_frame_delivery_conditions   :string(255)
#  multicast_frame_delivery_conditions :string(255)
#  broadcast_frame_delivery_conditions :string(255)
#  unicast_frame_delivery_details      :string(255)
#  multicast_frame_delivery_details    :string(255)
#  broadcast_frame_delivery_details    :string(255)
#  notes                               :text
#  created_at                          :datetime
#  updated_at                          :datetime
#  operator_network_type_id            :integer(4)
#  underlying_technology               :string(255)
#  equipment_vendor                    :string(255)
#  service_attribute_notes             :string(255)
#  max_num_cos                         :string(255)
#  redundancy_mechanism                :string(255)
#  is_frame_aware                      :boolean(1)      default(TRUE)
#


class SegmentType < TypeElement
  belongs_to :operator_network_type
  has_and_belongs_to_many :ethernet_service_types
  has_many :segments
  has_many :instance_elements, :class_name => "Segment"
  
  validates_presence_of :name, :operator_network_type_id
  validates_inclusion_of :segment_type, :in => HwTypes::Path_TYPES, :message => "invalid, Use: #{HwTypes::Path_TYPES.join(", ")}"
#  validates_inclusion_of :max_num_cos, :in => HwTypes::OVC_MAX_NUM_COS, :message => "invalid, Use: #{HwTypes::OVC_MAX_NUM_COS.join(", ")}"
  validates_numericality_of :max_num_cos, :only_integer => true, :less_than =>8, :message => 'is not an integer [1-8]'
  validates_uniqueness_of :name, :scope => :operator_network_type_id
  validates_numericality_of :max_mtu, :on => :update, :only_integer => true, :less_than =>9601, :message => 'is not an integer [1-9600]'

  validates_inclusion_of :color_marking, :on => :update, :in => FrameTypes::FRAME_COLOR_MARKING_TYPES.map{|disp, value| value },
                         :message => "invalid, Use: #{FrameTypes::FRAME_COLOR_MARKING_TYPES.map{|disp, value| value }.join(", ")}"

  validates_inclusion_of :unicast_frame_delivery_conditions, :on => :update, :in => ServiceCategories::SERVICE_FRAME_DELIVERY.map{|disp, value| value },
                         :message => "invalid, Use: #{ServiceCategories::SERVICE_FRAME_DELIVERY.map{|disp, value| value }.join(", ")}"

  validates_inclusion_of :multicast_frame_delivery_conditions, :on => :update, :in => ServiceCategories::SERVICE_FRAME_DELIVERY.map{|disp, value| value },
                         :message => "invalid, Use: #{ServiceCategories::SERVICE_FRAME_DELIVERY.map{|disp, value| value }.join(", ")}"

  validates_inclusion_of :broadcast_frame_delivery_conditions, :on => :update, :in => ServiceCategories::SERVICE_FRAME_DELIVERY.map{|disp, value| value },
                         :message => "invalid, Use: #{ServiceCategories::SERVICE_FRAME_DELIVERY.map{|disp, value| value }.join(", ")}"


  validates_inclusion_of :mef9_cert, :on => :update, :in => ServiceCategories::SERVICE_MEF9_CERTIFICATION.map{|disp, value| value },
                         :message => "invalid, Use: #{ServiceCategories::SERVICE_MEF9_CERTIFICATION.map{|disp, value| value }.join(", ")}"

  validates_inclusion_of :mef14_cert, :on => :update, :in => ServiceCategories::SERVICE_MEF14_CERTIFICATION.map{|disp, value| value },
                         :message => "invalid, Use: #{ServiceCategories::SERVICE_MEF14_CERTIFICATION.map{|disp, value| value }.join(", ")}"

  validates_inclusion_of :c_vlan_id_preservation, :on => :update, :in => ServiceCategories::CE_VLAN_PRESERVATION.map{|disp, value| value },
                         :message => "invalid, Use: #{ServiceCategories::CE_VLAN_PRESERVATION.map{|disp, value| value }.join(", ")}"
                           

  def cenx_name
    short_sp_name = CenxNameTools::CenxNameHelper.shorten_name sp_info_st

    type = CenxNameTools::CenxNameHelper.generate_type_string self
    return "#{type}:#{short_sp_name}:#{name}"
  end

  def display_name
    return "#{name}"
  end

   def sp_info_st
    return operator_network_type.service_provider.name
   end
  
end
