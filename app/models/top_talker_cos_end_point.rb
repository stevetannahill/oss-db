# == Schema Information
#
# Table name: top_talkers
#
#  id                :integer(4)      not null, primary key
#  type              :string(255)
#  enni_id           :integer(4)
#  cos_end_point_id  :integer(4)
#  octets_count      :float
#  direction         :string(255)
#  time_period_start :integer(4)
#  time_period_end   :integer(4)
#

# To change this template, choose Tools | Templates
# and open the template in the editor.

class TopTalkerCosEndPoint  < TopTalker
    belongs_to :cos_end_point
    validates_presence_of :cos_end_point_id

    def capactiy_percentage
      if self.cos_end_point.rate_mbps(self.direction.to_sym) == 0
        0
      else
        (self.average_mbps/self.cos_end_point.rate_mbps(self.direction.to_sym)) * 100
      end
    end
end
