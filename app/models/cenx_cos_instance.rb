# == Schema Information
# Schema version: 20110802190849
#
# Table name: cos_instances
#
#  id                       :integer(4)      not null, primary key
#  class_of_service_type_id :integer(4)
#  segment_id               :integer(4)
#  notes                    :string(255)
#  created_at               :datetime
#  updated_at               :datetime
#  type                     :string(255)
#


class CenxCosInstance < CosInstance
  validates_class_of :segment, :is_a => "OnNetOvc"
  validates_class_of :class_of_service_type, :is_a => "CenxClassOfServiceType"

  def sla_max_queue_delay
    value = 0
    
    cos_end_points.each do |cep|
      value += cep.sla_max_queue_delay
    end
    return value
  end

  def get_cenx_monitoring_cos_endpoints
     return cos_end_points.select{ |ce| (ce.segment_end_point.is_connected_to_monitoring_port) }
  end

end
