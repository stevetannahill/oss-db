class CellSiteReport < Report
  extend ExceptionReports
  
  def self.generate_report(schedule_id)
    generate_exception_report(schedule_id, :cell_site_exceptions, :cell_site_exceptions)
  end
  
  def update_report
    false
  end
  
end