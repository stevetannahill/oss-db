# == Schema Information
# Schema version: 20110802190849
#
# Table name: user_service_providers
#
#  id                  :integer(4)      not null, primary key
#  user_id             :integer(4)      not null
#  service_provider_id :integer(4)      not null
#  created_at          :datetime
#  updated_at          :datetime
#

class UserServiceProvider < ActiveRecord::Base
  belongs_to :user
  belongs_to :service_provider
  
  delegate :email, :first_name, :last_name, :to => :user
end
