# == Schema Information
# Schema version: 20110802190849
#
# Table name: event_histories
#
#  id             :integer(4)      not null, primary key
#  event_filter   :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  alarm_mapping  :text
#  retention_time :integer(4)
#

class SmHistory < AlarmHistory
  has_many :sm_ownerships, :dependent => :destroy

  def self.default_sm_alarm_mapping
    return {"ok" => AlarmSeverity::OK,
            "warning" => AlarmSeverity::WARNING,
            "failed" => AlarmSeverity::FAILED,
            "unavailable" => AlarmSeverity::UNAVAILABLE,
            "dependency" => AlarmSeverity::DEPENDENCY,
            "maintenance" => AlarmSeverity::MTC,
            "not_monitored" => AlarmSeverity::NOT_MONITORED,
            "not_live" => AlarmSeverity::NOT_LIVE,
            "No State" => AlarmSeverity::NO_STATE
           }
  end
  
  def default_mapping
    self.alarm_mapping = SmHistory::default_sm_alarm_mapping if alarm_mapping == nil 
  end

  def self.alarm_sync_all(nms = nil)
    SW_ERR "Not supported for #{self.class}"
    return true
  end

  def alarm_sync(timestamp = nil)
    SW_ERR "Not supported for #{self.class}"
    return true
  end

  def +
    SW_ERR "Not supported for #{self.class}"
  end

  def self.process_event(event)
    SW_ERR "Not supported for #{self.class}"
  end

  # Take 2 histories and "or" them together ie the one with the highest
  # state will be placed into the history
  def +(other)
    SW_ERR "Not supported for #{self.class}"
  end

  # Take 2 histories and "and" them together ie the one with the lowest
  # state will be placed into the history
  def &(other)
    SW_ERR "Not supported for #{self.class}"
  end

  def add_event(event_time, state, details)
    
    cenx_state = alarm_mapping[state]
    self.alarm_records << AlarmRecord.create(:time_of_event => event_time, :state => cenx_state, :details => details, :original_state => state, :event_history => self)
    save
    sm_ownerships.each do |eo|
      obj = eo.smable      
      obj.class.where("id = ?", obj.id).update_all(:sm_state => cenx_state.to_s, :sm_details => details, :sm_timestamp => event_time)    
    end
    
    # remove any old alarm records
    # This is based on the creation time as Service Monitoring states are not based on the time_of_event
    if retention_time != 0
      # Don't delete the last one as we want to guarantee there will always be one record
      ars = alarm_records.where("time_of_event < ?", (Time.now-retention_time.days).to_i * 1000)
      ars[0..-2].each {|ar| ar.destroy}
    end

  end

  def alarm_state
    ar = alarm_records.last
    # If there are no alarm records return NO_STATE
    ar = AlarmRecord.new(:time_of_event => 0, :state => AlarmSeverity::NO_STATE, :details => "No state", :original_state => "No State", :original_state => "No State", :details => "Error no SM records") if ar == nil
    return ar
  end

  def availability_percent(start_time, end_time, threshold, exclude_threshold = AlarmSeverity::MTC)
    SW_ERR "Not supported for #{self.class}"
    return "Not supported for #{self.class}"
  end

  # Take a number of histories and merge them together. The block is used to indicate if the
  # the alarm with the highest state should be placed in the history or if the
  # alarm with the lowest state.
  # This takes a block which should return an alarmRecord based on some logic
  # A typical example would be to return the alarmRecord with the highest state
  def self.merge(alarm_histories)
    SW_ERR "Not supported for #{self.class}"
  end

end
