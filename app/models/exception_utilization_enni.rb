# == Schema Information
#
# Table name: exception_utilization_ennis
#
#  id                                     :integer(4)      not null, primary key
#  time_declared                          :integer(4)
#  value                                  :float
#  severity                               :string(0)
#  enni_new_id                            :integer(4)
#  direction                              :string(0)
#  time_over                              :integer(4)
#  average_rate_while_over_threshold_mbps :float
#
class ExceptionUtilizationEnni < ActiveRecord::Base
  include CommonException
  
  belongs_to :enni_new
  validates :enni_new_id, :presence => true
  validates :time_declared, :presence => true
  validates :value, :presence => true
  validates_inclusion_of :severity, :in => [ :major, :minor]
  validates_inclusion_of :direction, :in => [:ingress, :egress]
  validates :direction, :presence => true
  validates :time_over, :presence => true
  validates :average_rate_while_over_threshold_mbps, :presence => true


  def direction
    read_attribute(:direction).to_sym
  end
  def direction= (value)
    write_attribute(:direction, value.to_s)
  end


  def get_value_units_s
    case read_attribute(:severity).to_sym
    when :critical
      return 'n/a'
    when :major
      return 'seconds'
    when :minor
      return 'seconds'
    end
  end

  def severity
    read_attribute(:severity).to_sym
  end
  def severity= (value)
    write_attribute(:severity, value.to_s)
  end

  def entity_type
    'ENNI'
  end


  def get_errored_period_threshold_mbps
    EnniNew.find(enni_new_id).get_utilization_threshold_mbps direction, severity
  end
  
  def get_errored_period_threshold_percent
    EnniNew.find(enni_new_id).get_utilization_threshold_percent direction, severity
  end

  def get_errored_period_threshold
    EnniNew.find(enni_new_id).get_utilization_errored_period_threshold direction, severity
  end

  #   returns delta_t which can be longer than the native period (59 secs or 5 mins)
  # if 0 is returned then the native time period shall be used.
  def get_errored_period_delta_time_secs
    EnniNew.find(enni_new_id).get_utilization_errored_period_delta_time_secs
  end

  # Returns the maximum number of time periods allowed over the threshold without
  # declaring an exception.
  # has to be greater than 1
  def get_errored_period_max_errored_count
    EnniNew.find(enni_new_id).get_utilization_errored_period_max_errored_count direction, severity
  end


  def get_errored_period_max_secs
    get_errored_period_max_errored_count * get_errored_period_delta_time_secs
  end

end

