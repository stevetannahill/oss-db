class AccessPermission < ActiveRecord::Base
  belongs_to :operator_network
  belongs_to :target, :class_name => 'OperatorNetwork'

  scope :for_buyer, lambda { |target_operator_network| where(:target_id => target_operator_network)}

  PERMISSION_TYPES = ["Full Access", "Approval Required", "No Access"]

  validates_presence_of :operator_network_id, :target_id
  validates_uniqueness_of :target_id, :scope => :operator_network_id
  validates_inclusion_of :permission, :in => PERMISSION_TYPES, :message => "invalid, Use: #{PERMISSION_TYPES}"

  def operator_network_name
    return operator_network ? operator_network.name : ""
  end

  def target_name
    return target ? target.name : ""
  end

  def name
    return "#{operator_network_name}:#{target_name}:#{permission}"
  end
end
