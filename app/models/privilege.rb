class Privilege < ActiveRecord::Base
  has_and_belongs_to_many :roles
  validates_uniqueness_of :name
  validates_presence_of :name
  default_scope :order => :name

  # This is a list of the attributes a user is allowed to change through a mass update.
  attr_accessible :name
end
