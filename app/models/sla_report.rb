# == Schema Information
#
# Table name: reports
#
#  id                  :integer(4)      not null, primary key
#  type                :string(255)
#  schedule_id         :integer(4)
#  run_date            :datetime
#  report_period_start :datetime
#  report_period_end   :datetime
#  minor_exceptions    :integer(4)
#  major_exceptions    :integer(4)
#  critical_exceptions :integer(4)
#  created_at          :datetime        not null
#  updated_at          :datetime        not null
#
class SlaReport < Report
  extend ExceptionReports
  
  def self.generate_report(schedule_id)
    generate_exception_report(schedule_id, :sla_exceptions)
  end
  
  
  def update_report
    false
  end
  
end