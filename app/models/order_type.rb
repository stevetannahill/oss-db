# == Schema Information
# Schema version: 20110802190849
#
# Table name: order_types
#
#  id                       :integer(4)      not null, primary key
#  name                     :string(255)
#  operator_network_type_id :integer(4)
#  created_at               :datetime
#  updated_at               :datetime
#

class OrderType < TypeElement
  belongs_to :operator_network_type
  has_many :ordered_entity_groups, :dependent => :destroy
  has_many :service_orders, :through => :ordered_entity_groups
  has_many :instance_elements, :through => :ordered_entity_groups, :class_name => "ServiceOrder"
  
  validates_presence_of :name, :operator_network_type_id
  validates_uniqueness_of :name, :scope => :operator_network_type_id
  
  def ordered_entity_group_count
    return ordered_entity_groups.length
  end
  
end
