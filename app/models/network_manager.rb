# == Schema Information
#
# Table name: network_managers
#
#  id              :integer(4)      not null, primary key
#  name            :string(255)
#  nm_type         :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#  url             :string(255)
#  data_archive_id :integer(4)      default(1)
#  event_record_id :integer(4)
#

class NetworkManager < ActiveRecord::Base
  include Eventable
  attr_accessor :new_password, :new_password_confirmation, :old_password
  attr_accessible :new_password, :new_password_confirmation, :old_password, :name, :nm_type, :url, :username
  validates_confirmation_of :new_password
  validates_presence_of :new_password_confirmation, :if => "!new_password.blank?"
  validate :validate_old_password, :if => "!old_password.blank?"
  
  has_many :network_management_servers, :dependent => :destroy
  has_many :nodes, :dependent => :nullify
  belongs_to :data_archive

  validates_presence_of :name, :nm_type
  validates_inclusion_of :nm_type, :in => HwTypes::NM_TYPES, :message => "invalid, Use: #{HwTypes::NM_TYPES.join(", ")}"

  after_create :general_after_create
  after_save :update_alarm_histories_after_change, :if => "name_changed? || nm_type_changed?"
  before_save :encrypt_password
  
  def encrypt_password
    if !new_password.blank?
      crypt_pwd = Crypt.cenx_encode(new_password)
      self.password = crypt_pwd
    end
  end
  
  def get_password
    Crypt.cenx_decode(password)
  end
  
  def validate_old_password
    # If there is a current password and it does not match the old password then fail
    if (!password.blank?) && get_password != old_password
      errors.add(:old_password, "does not match current password")
    end
  end
  
  def general_after_create
    create_alarm_histories
    true
  end
  
  def update_alarm_histories_after_change
    create_alarm_histories
    true
  end
  
  def create_alarm_histories
    event_record_create
    alarm_keys = ["#{name}-#{nm_type}"]
    new_alarms = {} 
    alarm_keys.each {|filter| new_alarms[filter] = {:type => SystemAlarmHistory, :mapping => SystemAlarmHistory.default_system_alarm_mapping}}
    return_value = update_alarm_history_set(new_alarms)
  end
  
  
  def update_alarm(list_of_evaled_objects = [], full_eval = false)
    changed = super
    # Always update file for Monit even if the state has not changed. If the file gets deleted then it will show an error in monit and won't
    # clear until a state change. 
    state = AlarmRecord.highest(NetworkManager.all.collect {|nm| nm.event_record})      
    details = NetworkManager.all.collect {|nm| "#{nm.name}-#{nm.event_record.state.to_s}-#{nm.event_record.details}" if nm.event_record.state != AlarmSeverity::OK}.compact.join("\n") 
    SystemAlarmHistory.update_monit("#{Rails.root}/log/" + "network_manager.monit_status", state, details)
    return changed
  end
  
  def update_sm(list_of_evaled_objects = [])
    # No Service monitoring state
  end

  def node_count
    return nodes.size
  end
  
  def nm_server_count
    return network_management_servers.size
  end
end
