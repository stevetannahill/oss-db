# == Schema Information
# Schema version: 20110802190849
#
# Table name: event_histories
#
#  id             :integer(4)      not null, primary key
#  event_filter   :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  alarm_mapping  :text
#  retention_time :integer(4)
#

require "sam_api.rb"
require "jms_alarm_parse.rb"

class AlarmHistory < EventHistory
  has_many :alarm_records, :foreign_key => :event_history_id, :order => "time_of_event" 
  
  before_save :default_mapping
  before_save :alarm_mapping_on_save, :if => :alarm_mapping_changed? 

  def default_mapping
    self.alarm_mapping = AlarmSeverity::default_alu_alarm_mapping if alarm_mapping == nil 
  end
  
  # Class method to lookup the event and then add the record
  def self.process_event(event)
    ah = AlarmHistory.find_by_event_filter(event[:event_filter])
    if ah != nil
      ah.add_event(event[:timestamp], event[:state], event[:details])
      Rails.logger.info "Added Event for Alarm name #{event[:event_filter]}  #{Time.at(event[:timestamp].to_i/1000)} #{event[:state]} #{event[:details]}"
    else
      Rails.logger.info "No Filter found for Alarm name *#{event[:event_filter]}*  #{Time.at(event[:timestamp].to_i/1000)} #{event[:state]} #{event[:details]}"
    end 
  end
  
  def self.alarm_sync_all(nms = nil)
    return_code = true
    
    # Group the alarm histories by network manager
    alarm_history_by_nms = Hash.new {|h,k| h[k]=[]}
    AlarmHistory.find_all_by_type(AlarmHistory.to_s).each {|ah| 
      if ah.event_ownerships.first.nil?
        SW_ERR "Alarm History #{ah.id}:#{ah.event_filter} does not belong to any object!"
      else
        alarm_history_by_nms[ah.event_ownerships.first.eventable.get_network_manager] << ah
      end
    }
    
    # If only a single nms needs to be resynced select the alarm histories
    alarm_history_by_nms = {nms => alarm_history_by_nms[nms]} if nms != nil
    
    # For each network manager get all the alarms assocaited with it
    alarm_history_by_nms.each do |nms, alarm_histories|
      if alarm_histories.size > 0
        alarms = alarm_histories.map(&:event_filter)
        # Do a soap request to get the alarm and then call add event
        sam_api = SamIf::StatsApi.new(nms)
        result, data = sam_api.get_alarm(alarms,  ["firstTimeDetected", "affectedObjectFullName", "severity", "nodeName", "probableCause"])
        sam_api.close
        if !result
          SW_ERR "Failed to connect to sam_if_server"
          return_code = false
        else
          alarm_info = JmsAlarmParse.extract_JMS_alarm_info(data)
          alarmed = alarm_info.map{|a| a[:event_filter]}
          # Add all the alarms found
          alarm_info.each {|a| self.process_event(a)}
          # Add "cleared" for all alarms that have no alarm
          (alarms-alarmed).each {|a| self.process_event({:event_filter => a, :timestamp => (Time.now.to_f*1000).to_i, :state => "cleared", :details => ""})}
        end
      end
    end
    return return_code
  end      

  def alarm_records_count
    return alarm_records.size
  end

  def alarm_mapping_on_save
    # update all alarm records to recalculate the CENX alarm state
    alarm_records.each {|ar|
      ar.state = alarm_mapping[ar.original_state]
      ar.save 
    }
  end
  
  # Take 2 histories and "or" them together ie the one with the highest
  # state will be placed into the history
  def +(other)
    return AlarmHistory.merge([self, other]) {|events| AlarmRecord.highest(events) }    
  end

  # Take 2 histories and "and" them together ie the one with the lowest
  # state will be placed into the history 
  def &(other)
    return AlarmHistory.merge([self, other]) {|events| AlarmRecord.lowest(events) }    
  end

  def alarm_sync(timestamp = nil)
    return_code = false
    # Do a soap request to get the alarm and then call add event
    # Find the network manager server assocaited with the alarm history. Only need to take the first
    # object to find the nms for the alarm
    nms = event_ownerships.first.eventable.get_network_manager
    sam_api = SamIf::StatsApi.new(nms)
    result, data = sam_api.get_alarm([event_filter], ["firstTimeDetected", "affectedObjectFullName", "severity", "nodeName", "probableCause"])
    sam_api.close
    if !result
      SW_ERR "Failed to connect to sam_if_server"
    else
      alarm_info = JmsAlarmParse.extract_JMS_alarm_info(data)
      # If there are no alarms then SOAP will reply with empty data so we must assume it means
      # there are no alarms. However if the port/service etc does not exist the same empty SOAP reply is
      # sent back - This is not what we really want
      timestamp = (Time.now.to_f*1000).to_i if timestamp == nil
      alarm_info = [{:timestamp => timestamp.to_s, :state => "cleared", :details => ""}] if alarm_info.size == 0
      add_event(alarm_info.first[:timestamp], alarm_info.first[:state], alarm_info.first[:details])
      return_code = true
    end
    return return_code
  end
  
  def alarm_sync_past(timestamp = nil)
    return false
  end
  
  
  def add_event(event_time, state, details)
    cenx_state = alarm_mapping[state]
    ar = AlarmRecord.create(:time_of_event => event_time, :state => cenx_state, :details => details, :original_state => state, :event_history => self)
    if ar.valid?
      self.alarm_records << ar
      save
      event_ownerships.each {|eo| eo.eventable.eval_alarms}
    end

    # remove any old alarm records
    if retention_time != 0
      # Don't delete the last one as we want to guarantee there will always be history for the retention time as it's used to calculate
      # availability so a record is needed that is at least retention_time old
      ars = alarm_records.where("time_of_event < ?", (Time.now-retention_time.days).to_i * 1000)
      ars[0..-2].each {|ar| ar.destroy}
    end
    
  end
      
  def alarm_state
    ar = alarm_records.last
    # If there are no alarm records return NO_STATE
    ar = AlarmRecord.new(:time_of_event => 0, :state => AlarmSeverity::NO_STATE, :details => "No state", :original_state => "No State", :details => "Error No alarm records") if ar == nil
    return ar
  end

  # return non-zero only if time is within the range [start_time, end_time]
  def compute_time_delta event_start, event_end, start_time, end_time
    delta = 0
    event_start = start_time if (event_start < start_time)
    event_end = end_time if (event_end > end_time)
    if (event_end > event_start) and (event_start >= start_time and event_end <= end_time)
      delta = event_end - event_start
    end
    return delta
  end

  def unavailable_time_msecs(start_time, end_time, threshold, exclude_threshold = AlarmSeverity::MTC)
    records = alarm_records
    return -1 if records.size == 0  # No data available
    unavailable_time =0
    prev_time  = records[0].time_of_event
    prev_state = records[0].state
    if (prev_time> start_time )
      # Oldest record is after start time, we don't have enough info to
      # determine unavailable_time over that period.
      return -1
    end
    for i in 0..records.size-1 do
      event_time  = records[i].time_of_event
      if (prev_state >= threshold) && (prev_state < exclude_threshold)
        delta = compute_time_delta prev_time, event_time, start_time, end_time
        unavailable_time += delta
      end
      event_state = records[i].state
      prev_state = event_state
      prev_time  = event_time
    end
    if prev_time <= end_time
      if (prev_state >= threshold) && (prev_state < exclude_threshold)
        delta = compute_time_delta prev_time, end_time, start_time, end_time
        unavailable_time += delta
      end
    end
    return unavailable_time
  end

  def availability_percent(start_time, end_time, threshold, exclude_threshold = AlarmSeverity::MTC)
    time_above_threshold = unavailable_time_msecs(start_time, end_time, threshold, exclude_threshold)
    if time_above_threshold >= 0
      total_time = end_time - start_time
      percent = ((total_time - time_above_threshold.to_f)/total_time.to_f) * 100
      raise(RuntimeError, "Invalid percent calculation #{percent} total time #{total_time} above thesshold #{time_above_threshold }", caller) if percent < 0 || percent > 100 
      return "#{'%.4f' % percent}"
    else
      return "No Data Available"
    end
  end
  
  # return a subset of alarm records from start time to end time. 
  # It will include the alarm immediately before the start time (if there is one)
  def subset(start_time_ms = nil, end_time_ms = nil)
    return self.class.new(:event_filter => self.to_s, :alarm_records => []) if alarm_records.size == 0

    # Use sql to sort if this alarm history is in the database otherwise no need to sort as derived history should be ordered
    records = alarm_records
    
    start_time_ms = records.first.time_of_event if start_time_ms == nil
    end_time_ms = records.last.time_of_event if end_time_ms == nil 
    
    start_index = records.bsearch_upper_boundary {|ar| ar.time_of_event <=> start_time_ms}-1
    end_index = records.bsearch_upper_boundary {|ar| ar.time_of_event <=> end_time_ms}-1    
    start_index = 0 if start_index < 0 # The first alarm is after the start time so reset the start_index  
    
    # If there are no alarms before the end time return no history
    return self.class.new(:event_filter => self.to_s, :alarm_records => []) if end_index < 0 
     
    return self.class.new(:event_filter => self.to_s, :alarm_records => records[start_index..end_index]) # need a unqiue id so use self.to_s    
  end
      
  def alarm_debug_dump
    puts "#{self.class.to_s} #{self.event_filter}"
    alarm_records.each do |r|
      puts("#{Time.at(r.time_of_event/1000).strftime("%a %b %d %H:%M:%S")}:#{'.%04d' % (r.time_of_event % 1000)} #{r.state.to_s[0..10]} \t #{r.details}\n")
    end
    nil
  end

  # Take a number of histories and merge them together. The block is used to indicate if the
  # the alarm with the highest state should be placed in the history or if the
  # alarm with the lowest state. 
  # This takes a block which should return an alarmRecord based on some logic
  # A typical example would be to return the alarmRecord with the highest state
  def self.merge(alarm_histories)
    alarm_histories.compact!

    history = []
    events = []

    # Need to keep track of which event belongs to which parameter so
    # tag each record with 0,1,2..n
    all_records = []
    alarm_histories.each_index do |ah_idx| 
      alarm_histories[ah_idx].alarm_records.each {|r| all_records << [ah_idx,r]}
    end
    all_records.sort!{|a, b| a[1] <=> b[1]}

    all_records.each do |r|
      events[r[0]] = r[1]      
      # Got records for each alarm - set selected to the other alarm from the current alarm
      #selected = r[0] == 0 ? events.values[1] : selected = events.values[0]
      selected = yield(events)
      # If the current event happened at the same time as the previous one then overwrite
      # the last event with the event with the highest or lowest state
      # If the time is different then only add a new event if the state changed      
      if history.size == 0
        history << selected.dup
      elsif selected == history[-1]
        # Events happened at the same time so take the highest or lowest and overwrite the previous
        history[-1] = yield([selected, history[-1]])        
      elsif selected.state != history[-1].state
        # adjust time to handle case of eg history 1 = Failed, History 2 = mtc, ok 
        # when get ok from history 2 the highest is Failed but the time needs to be adjusted to the current time
        # downside is we lose the orginal time stamp.....
        h = selected.dup
        h.time_of_event = r[1].time_of_event
        history << h 
      end
    end
   return name.constantize.new(:event_filter => self.to_s, :alarm_records => history) # need a unqiue id so use self.to_s
  end


end
