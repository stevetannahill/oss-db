class SiteList < ActiveRecord::Base 
	belongs_to :user
	has_many :paths_site_lists, :dependent => :destroy
	has_many :paths, :through => :paths_site_lists
	has_many :sin_path_service_provider_sphinxes, :through => :paths_site_lists, :conditions => lambda { |service_provider_id| "service_provider_id = #{self.user.service_provider_id}" }
	default_scope :order => :name

	validates_presence_of :name
	validates_uniqueness_of :name, :scope => :user_id
end# == Schema Information
#
# Table name: site_lists
#
#  id         :integer(4)      not null, primary key
#  name       :string(255)
#  user_id    :integer(4)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

