# == Schema Information
#
# Table name: demarcs
#
#  id                               :integer(4)      not null, primary key
#  type                             :string(255)
#  cenx_id                          :string(255)
#  cenx_demarc_type                 :string(255)
#  notes                            :text
#  operator_network_id              :integer(4)
#  address                          :string(255)
#  port_name                        :string(255)
#  physical_medium                  :string(255)
#  connected_device_type            :string(255)
#  connected_device_sw_version      :string(255)
#  reflection_mechanism             :string(255)
#  mon_loopback_address             :string(255)
#  end_user_name                    :string(255)
#  is_new_construction              :boolean(1)
#  contact_info                     :string(255)
#  created_at                       :datetime
#  updated_at                       :datetime
#  demarc_type_id                   :integer(4)
#  max_num_segments                 :integer(4)
#  ether_type                       :string(255)
#  protection_type                  :string(255)
#  lag_id                           :integer(4)
#  fiber_handoff_type               :string(255)
#  auto_negotiate                   :boolean(1)
#  ah_supported                     :boolean(1)
#  lag_mac_primary                  :string(255)
#  emergency_contact_id             :integer(4)
#  lag_mac_secondary                :string(255)
#  port_enap_type                   :string(255)     default("QINQ")
#  lag_mode                         :string(255)
#  cir_limit                        :integer(4)      default(100)
#  eir_limit                        :integer(4)      default(300)
#  cenx_name_prefix                 :string(255)
#  demarc_icon                      :string(255)     default("building")
#  on_behalf_of_service_provider_id :integer(4)
#  event_record_id                  :integer(4)
#  sm_state                         :string(255)
#  sm_details                       :string(255)
#  sm_timestamp                     :integer(8)
#  prov_name                        :string(255)
#  prov_notes                       :text
#  prov_timestamp                   :integer(8)
#  order_name                       :string(255)
#  order_notes                      :text
#  order_timestamp                  :integer(8)
#  near_side_clli                   :string(255)     default("TBD")
#  far_side_clli                    :string(255)     default("TBD")
#  latitude                         :decimal(9, 6)   default(0.0)
#  longitude                        :decimal(9, 6)   default(0.0)
#  site_id                          :integer(4)
#


class EnniNew < Demarc  
  include Kpiable
  
  belongs_to :site
  has_many :uploaded_files, :as => :uploader, :dependent => :destroy
  has_many :sin_enni_service_provider_sphinxes, :foreign_key => :enni_id, :dependent => :destroy

  validates_presence_of :operator_network_id, :site_id, :cir_limit, :eir_limit
  validates_uniqueness_of :lag_id, :scope => :site_id, :allow_nil => true
  validates_inclusion_of :protection_type, :in => HwTypes::PROTECTION_TYPES.map{|disp, value| value },
                         :message => "invalid, Use: #{HwTypes::PROTECTION_TYPES.map{|disp, value| value }.join(", ")}"
  validates_inclusion_of :physical_medium, :in => HwTypes::PHY_TYPES.map{|disp, value| value },
                         :message => "invalid, Use: #{HwTypes::PHY_TYPES.map{|disp, value| value }.join(", ")}"
  validates_inclusion_of :ether_type, :in => FrameTypes::ETHER_TYPES.map{|disp, value| value },
                         :message => "invalid, Use: #{FrameTypes::ETHER_TYPES.map{|disp, value| value }.join(", ")}"
  validates_inclusion_of :port_enap_type, :in => FrameTypes::PORT_ENCAP_TYPES.map{|value| value },
                         :message => "invalid, Use: #{FrameTypes::PORT_ENCAP_TYPES.map{|value| value }.join(", ")}"
  validates_inclusion_of :demarc_icon, :in => ["enni","router", "switch", "cell site", "MW site"],
                         :message => "invalid, ENNI's demarc icon must be ENNI, Router, switch, cell site or MW site"

  validate :valid_lag_id?, :valid_ports?, :valid_lag_mode?
  validates_class_of :demarc_type, :is_a => EnniType, :allow_nil => true
  validate :demarc_type_is_correct
  validate :valid_emergency_contact?

  after_save :assoc_service_providers_for_indexing_entities
  after_save :update_kpis, :if => :site_id_changed? || :operator_network_id_changed?

  scope :turbo_site_info, select("demarcs.*").
                        select("IFNULL(sites.name,'') as turbo_site_info").
                        joins("LEFT OUTER JOIN sites ON sites.id = demarcs.site_id")

  scope :turbo_sp_info_enni, select("demarcs.*").
                             select("IFNULL(service_providers.name,'TBD') as turbo_sp_info_enni").
                             joins('LEFT OUTER JOIN `operator_networks` ON `operator_networks`.`id` = `demarcs`.`operator_network_id` 
                                   LEFT OUTER JOIN `service_providers` ON `service_providers`.`id` = `operator_networks`.`service_provider_id`')

  def self.connected_node_exists_sql(valid_types)
    "EXISTS (SELECT * FROM `ports` INNER JOIN `ports` AS c_ports ON c_ports.id = `ports`.connected_port_id INNER JOIN `nodes` ON `nodes`.id = c_ports.node_id  WHERE `ports`.demarc_id = `demarcs`.`id` AND `ports`.connected_port_id IS NOT NULL AND `nodes`.node_type IN (#{valid_types.map{|t|"'#{t}'"}.join(',')}))"
  end

  scope :turbo_cenx_name, select("demarcs.*"). #"*#{p.name}"
                          select("IF(`demarcs`.operator_network_id IS NULL,'To Be determined',
                                    CONCAT(`demarcs`.cenx_name_prefix,
                                         IF(demarcs.protection_type != 'unprotected',CONCAT(':lag-',demarcs.lag_id),''),
                                         (SELECT GROUP_CONCAT(CONCAT('*',name) ORDER BY name SEPARATOR '') FROM ports WHERE demarc_id = `demarcs`.`id`),
                                         IF(#{connected_node_exists_sql(Node::MONITORING_TYPES)},'**Serv.Mon IF',
                                           IF(#{connected_node_exists_sql(Node::TROUBLE_SHOOTING_TYPES)},'**TestSet IF','')))) as turbo_cenx_name")

  scope :s_agg_site_counts, select("count(distinct demarcs.id) as enni_count, sites.name as site_name, sites.id as site_id").
                            where('sites.type = ?', 'AggregationSite').
                            group('sites.name')

  scope :s_oem_counts, select("count(distinct demarcs.id) as enni_count, operator_network_types.name as operator_network_type_name, operator_network_types.id as operator_network_type_id").
                       joins("INNER JOIN operator_network_types ON operator_networks.operator_network_type_id = operator_network_types.id").
                       group('operator_network_types.id')

  scope :s_service_provider_counts, select("count(distinct demarcs.id) as enni_count, service_providers.name as service_provider_name, service_providers.id as service_provider_id").
                                    group('service_providers.name')                             

  scope :s_provisioning_counts, select("count(distinct demarcs.id) as prov_count, demarcs.prov_name as prov_name").
                                where('demarcs.prov_name IS NOT NULL').
                                group('demarcs.prov_name')     

  scope :s_ordering_counts, select("count(distinct demarcs.id) as order_count, demarcs.order_name as order_name").
                            where('demarcs.order_name IS NOT NULL').
                            group('demarcs.order_name')     

  scope :s_monitoring_counts, select("count(distinct demarcs.id) as sm_count, demarcs.sm_state as sm_state").
                              where('demarcs.sm_state IS NOT NULL').
                              group('demarcs.sm_state')                      

  scope :s_events_by_state, lambda { |states|  where('demarcs.sm_state IN (?)', states).group('demarcs.id') }

  scope :s_unique, group('demarcs.id')

  def demarc_type_is_correct
    # The Demrac type must be nil or a non-CENX demarc type if there an Order
    if demarc_type != nil      
      so = get_latest_order
      demarc_type_is_system_owner = demarc_type.operator_network_type.service_provider.is_system_owner
      if demarc_type_is_system_owner
        if so != nil
          errors.add(:demarc_type, "is a CENX Demac type")       
        end
      end
    end
  end
  
  #---------------------------------
  # Start Instance Element Code
  #---------------------------------

  def member_handle_owners

    owners = []
    owners = [site.operator_network.service_provider] if site
    
    # None for test ports
    if is_connected_to_test_port
      return owners.uniq
    end

    owners += service_provider ? [service_provider] : []

    segment_end_points.each{|segmentep|
      owners += segmentep.segment.member_handle_owners
    }
    
    # We want is_system_owner provider included
    return owners.uniq#.reject{|mho| mho.is_system_owner}
  end

  #---------------------------------
  # End Instance Element Code
  #---------------------------------

  #---------------------------------
  # Start Override some Demarc attr values For SIN Monitoring view
  #---------------------------------
  def connected_device_type
    ports.map{|p| "#{p.node.node_type}"}.uniq.join('; ')
  end

  def port_name
    port_names
  end

  def reflection_mechanism
    #Hard code for now, in future make this configurable
    if demarc_icon == 'router' or demarc_icon == 'enni'
      return 'TWAMP'
    end
    return 'DMM/DMR'
  end
  #---------------------------------
  # End Override some Demarc attr values For SIN Monitoring view
  #---------------------------------


  after_create :general_after_create
  
  def general_after_create
    # Create Service Monitoring history
    update_attributes(:sm_state => AlarmSeverity::NOT_LIVE.to_s, :sm_details => "", :sm_timestamp => (Time.now.to_f*1000).to_i)
    sm_histories << SmHistory.create(:event_filter => "SM #{self.class.name}:#{cenx_id}")
    # create the Provisioning state
    statefuls << EnniProvStateful.create
    state = prov_state.get_state
    update_attributes(:prov_name => state.name , :prov_timestamp => state.timestamp, :notes => state.notes)
    event_record_create
  end

  # Assoc service provider to the proper paths and ennis for indexing
  def assoc_service_providers_for_indexing_entities
    assoc_service_providers_for_indexing_ennis
  end

  def assoc_service_providers_for_indexing_ennis

    # Do not want this run while building circuits, so run at end (turn off quick and save each demarc)
    unless ModelMaker.quick?
  
      # Add new service provider associations
      current_list_of_service_provider_ids = sin_enni_service_provider_sphinxes.map(&:service_provider_id)
      updated_list_of_service_provider_ids = []
      updated_list_of_service_provider_ids << service_provider.id if service_provider
      updated_list_of_service_provider_ids << on_behalf_of_service_provider.id if on_behalf_of_service_provider

      segment_end_points.each do |ep|
        segment = ep.segment
        updated_list_of_service_provider_ids << segment.service_provider.id if segment.service_provider
        segment.paths.each do |path|
          updated_list_of_service_provider_ids << path.service_provider.id if path.service_provider
    
          # CORESITE HACK:
          if App.is?(:coresite)
            order = segment.get_latest_order
            updated_list_of_service_provider_ids += [order.buying_operator_network.try(:service_provider_id), order.target_operator_network.try(:service_provider_id)].compact if order
          end

        end
      end

      # only unique service providers are needed
      updated_list_of_service_provider_ids.uniq!
    
      # If service provider is not a new service provider then delete
      delete_list_of_service_provider_ids = current_list_of_service_provider_ids - updated_list_of_service_provider_ids

      # NOTE: Have todo this manually as we don't want to fire save on this object again (infinite loop) and collection methods (besides <<) do not save the association records
      updated_list_of_service_provider_ids.each do |sp_id|
        SinEnniServiceProviderSphinx.find_or_create_by_enni_id_and_service_provider_id(id, sp_id)
      end

      delete_list_of_service_provider_ids.each do |sp_id|
        SinEnniServiceProviderSphinx.delete_all({:enni_id => id, :service_provider_id => sp_id})
      end
    end
    
    # Reindex
    ReindexSphinx.schedule
    
  end
  
  
  def self.get_active_cenx_ennis
    all.reject{|enni| enni.get_prov_name == "Pending"}
  end 
  
  def name
    return cenx_name
  end

  def cenx_name
    if ! operator_network
      return "To Be determined"
    end
    
    port_names = ""
    if is_lag
      port_names += ":lag-#{lag_id}"
    end
    ports.each do |p|
      port_names += "*#{p.name}"
    end

    port_type = ""
    if is_connected_to_monitoring_port
      port_type = "**Serv.Mon IF"
    elsif is_connected_to_trouble_shooting_port
      port_type = "**TestSet IF"
    end

    "#{cenx_name_prefix}#{port_names}#{port_type}"
  end

  def owner_member_handle_value
    value = ''
    if member_handle_for_owner_object(operator_network.service_provider)
      value = member_handle_for_owner_object(operator_network.service_provider).value
    else
      value = cenx_name
    end

    return value
  end
  

  before_save :general_before_save
  
  def general_before_save
    self.cenx_name_prefix = build_cenx_name_prefix
  end

  def build_cenx_name_prefix
    short_sp_name = CenxNameTools::CenxNameHelper.shorten_name sp_info_demarc
    short_site_name = CenxNameTools::CenxNameHelper.shorten_name site_info
    short_phy_type = CenxNameTools::CenxNameHelper.shorten_phy_type physical_medium
    type = CenxNameTools::CenxNameHelper.generate_type_string self

    return "#{type}:#{short_sp_name}:#{short_site_name}:#{short_prot_type}:#{short_phy_type}"
  end

  def port_names
    ports.map{|p| "#{p.node.name} #{p.name}"}.join('; ')
  end

  def node_types
    ports.map{|p| "#{p.node.node_type}"}.uniq.join('; ')
  end

  def short_prot_type
    CenxNameTools::CenxNameHelper.shorten_protection_type protection_type
  end

  def is_connected_to_test_port
    EnniNew.joins("INNER JOIN `ports` ON ports.demarc_id = demarcs.id 
                   INNER JOIN `ports` connected_ports_ports ON `connected_ports_ports`.id = `ports`.connected_port_id
                   INNER JOIN `nodes` ON `nodes`.id = `connected_ports_ports`.node_id").
                   where("`demarcs`.id IN (?) and node_type IN (?)", self.id, Node::MONITORING_TYPES).size != 0
  end
  
  def is_connected_to_trouble_shooting_port
    result = false
    ports.each do |p|
      if p.is_trouble_shooting_port
        result = true
      end
    end

    return result
  end
  
  def is_connected_to_monitoring_port
    result = false

    ports.each do |p|
      if p.is_monitoring_port
        result = true
      end
    end

    return result
  end

  def get_monitoring_node
    if is_connected_to_monitoring_port
      ports.each do |p|
        if p.is_monitoring_port
          return p.connected_port.node
        end
      end
    end

    SW_ERR "get_monitoring_node called on wrong ENNI "
    return nil
  end

  def sp_info_enni
    operator_network ? operator_network.service_provider.name : "TBD"
  end

  def site_info
    site ? site.name : ""
  end
  
  def get_possible_candidate_demarc_types
    if operator_network_id != nil
      if get_latest_order == nil
        return operator_network.operator_network_type.enni_types
      else        
        return get_latest_order.ordered_operator_network.operator_network_type.enni_types
      end
    else
      return []
    end
  end

  def node_ids
    ids = []
    ports.each do |port|
      if !ids.find_index(port.node.id)
        ids << port.node.id
      end
    end
    return ids
  end

  def node_info
    nodes = []
    if ports.empty?
      nodes << "TBD"
    else
      ids = node_ids
      ids.each do |id|
        node_name = Node.find(id).name
        if !nodes.find_index(node_name)
          nodes << "#{node_name}"
        end
      end
    end
    return nodes.join("; ")
  end

  def get_operator_network_type
    on = site_id ? site.operator_network : nil
    return on ? on.operator_network_type : nil
  end

  def get_on_net_ovc_end_point_ennis
    return segment_end_points.select {|segmentepp| segmentepp.is_a? OnNetOvcEndPointEnni}
  end

  def get_cenx_class_of_service_types
    get_operator_network_type ? get_operator_network_type.class_of_service_types : []
  end

  #This function will return a hash with a lot of information about all
  #Cenx Class of Service Types and their values and totals and grand total
  # Returned hash is of form:
  # { "Cost Name" => {:cir => <cir>, :cir_rate => <ratio of cir / enni.rate>, <... same for eir>, :total => <cir + eir>, :rate => <cir_rate + eir_rate>},
  #   :grand => { <same as above but for ALL costs combined> }
  # }
  #
  def get_provisioned_bandwidth additional = {}, exclude = []

    #Create basic empty structure
    info = Hash.new
    get_cenx_class_of_service_types.each do |cost|
      info[cost.name] = Hash.new(0.0)
    end

    #Get all On Net Segment End Point ENNIs individual provisioned bandwidth and merge them
    endpoints = get_on_net_ovc_end_point_ennis
    endpoints.each do |ep|
      info.merge!(ep.get_provisioned_bandwidth(exclude)) { |k,a,b| a.merge(b) {|k,x,y| x+y}}
    end

    #Turn any Ingress Rate Limited "None" endpoints to 100% capacity
    info.each do |name, values|
      cir_cac_limit = ServiceCategories::ON_NET_OVC_COS_TYPES[name][:cir_cac_limit]
      eir_cac_limit = ServiceCategories::ON_NET_OVC_COS_TYPES[name][:eir_cac_limit]
      if values[:cir].infinite?
        values[:cir] = rate * (cir_cac_limit / 100.0)
        values[:cir_rate] = (values[:cir] / rate) * 100
      end
      
      if values[:eir].infinite?
        values[:eir] = rate * (eir_cac_limit / 100.0)
        values[:eir_rate] = (values[:eir] / rate) * 100
      end
    end

    #Add the additional values to the info gathered from the ENNIs
    additional.each do |name, data|
      cir_cac_limit = ServiceCategories::ON_NET_OVC_COS_TYPES[name][:cir_cac_limit]
      eir_cac_limit = ServiceCategories::ON_NET_OVC_COS_TYPES[name][:eir_cac_limit]
      max_cir = rate * (cir_cac_limit / 100.0)
      max_eir = rate * (eir_cac_limit / 100.0)

      #Pre-calculate proper (non infinite) cir and eir if one is infinite
      cir = info[name][:cir] + (data[:cir].infinite? ? max_cir : data[:cir])
      eir = info[name][:eir] + (data[:eir].infinite? ? max_eir : data[:eir])

      #Store actual cir and eir (can be infinite)
      info[name][:cir] += data[:cir]
      info[name][:eir] += data[:eir]

      #Recalculate ratios
      info[name][:cir_rate] = (cir / rate) * 100
      info[name][:eir_rate] = (eir / rate) * 100
    end

    #Add new :grand total information
    results = {:grand => Hash.new(0)}

    info.each do |name, values|

      results[name] = values
      #Add new :total information (for specific cos type)
      results[name][:total] = values[:cir] + values[:eir]
      results[name][:rate] = (results[name][:total] / rate) * 100

      #Add to grand total
      results[:grand][:cir] += results[name][:cir]
      results[:grand][:eir] += results[name][:eir]
      results[:grand][:cir_rate] += results[name][:cir_rate]
      results[:grand][:eir_rate] += results[name][:eir_rate]
      results[:grand][:total] += results[name][:total]
      results[:grand][:rate] += results[name][:rate]
    end
    return results
  end

  #This will check the provisioned bandwidth for errors within the CAC Limits
  # this function can take a pre-calculated provisioned_bandwidth
  def check_provisioned_bandwidth provisioned_bandwidth = nil

    provisioned_bandwidth = get_provisioned_bandwidth if provisioned_bandwidth.nil?

    #Build list of invalid values
    errors = Array.new
    get_cenx_class_of_service_types.each do |cost|
      cir_cac_limit = ServiceCategories::ON_NET_OVC_COS_TYPES[cost.name][:cir_cac_limit]
      eir_cac_limit = ServiceCategories::ON_NET_OVC_COS_TYPES[cost.name][:eir_cac_limit]

      if provisioned_bandwidth[cost.name][:cir_rate] > cir_cac_limit
        if provisioned_bandwidth[cost.name][:cir].infinite?
          errors << "Ingress Rate Limiting of None not allowed on populated ENNI"
        else
          errors << "CIR Rate on #{cost.name} exceeds CAC Limit of #{cir_cac_limit}% for ENNI"
        end
        
      end

      if provisioned_bandwidth[cost.name][:eir_rate] > eir_cac_limit
        if provisioned_bandwidth[cost.name][:eir].infinite?
          errors << "Ingress Rate Limiting of None not allowed on populated ENNI"
        else
          errors << "EIR Rate on #{cost.name} exceeds CAC Limit of #{eir_cac_limit}% for ENNI"
        end
      end
    end

    if provisioned_bandwidth[:grand][:cir_rate] > cir_limit
      if provisioned_bandwidth[:grand][:cir].infinite?
        errors << "Ingress Rate Limiting of None not allowed on populated ENNI"
      else
        errors << "CIR Rate #{provisioned_bandwidth[:grand][:cir_rate]}% exceeds CIR Rate Limit of #{cir_limit}% for ENNI "
      end
      
    end

    if provisioned_bandwidth[:grand][:eir_rate] > eir_limit
      if provisioned_bandwidth[:grand][:eir].infinite?
        errors << "Ingress Rate Limiting of None not allowed on populated ENNI"
      else
        errors << "EIR Rate #{provisioned_bandwidth[:grand][:eir_rate]}% exceeds EIR Rate Limit of #{eir_limit}% for ENNI "
      end
    end

    return errors
  end
  
  # additional is { "Cos Name" => {:dir => <value>}}
  def cac_capacity_check(additional)
    results = get_provisioned_bandwidth(additional)
    errors = check_provisioned_bandwidth(results)
    return errors
  end
    

  def new_port_can_be_linked
    if ports.empty?
      return true
    end

    if ports.size == 2
      return false
    end

    # ports not empty
    if is_unprotected
      return false
    end

    return true
  end

  def has_endpoint_with_stag stag_to_check
    segment_end_points.each do |segmentep|
      if segmentep.stags != nil && segmentep.stags == stag_to_check
        return true
      end
    end
    return false
  end

  def get_endpoint_with_stag stag_to_check
    segment_end_points.each do |segmentep|
      if segmentep.stags != nil && segmentep.stags == stag_to_check
        return segmentep
      end
    end
    return nil
  end

  def get_valid_site_list
    return [site]
  end

  def get_valid_segment_list
    return site.on_net_ovcs
  end

  # This overrides the default implementation in the Demarc superclass
  def get_mon_loopback_address
    return get_mac_address
  end

  def get_mac_address primary=true
    if is_lag
      if primary
        return lag_mac_primary
      else
        return lag_mac_secondary
      end
    else
      if ports.first
        return ports.first.mac
      else
        return 'TBD'
      end
    end
  end

  def interface_name
    if not is_lag
      if ports.empty?
        return "TBD"
      else
        return ports.first.name
      end
    else
      return "lag-#{lag_id}"
    end
  end

  def lag_name
    if not is_lag
      return "ERROR - #{cenx_name} is not a lag"
    else
      return "lag #{lag_id}"
    end
  end

  def stats_displayed_name
    if not is_lag
      if ports.empty?
        return "TBD"
      else
        return "Node:#{ports.first.node.name}/Port:#{ports.first.name}"
      end
    else
      return "Lag #{lag_id}"
    end
  end

  def is_active_active
    return lag_mode == 'Active/Active'
  end

  def is_unprotected
    return protection_type == "unprotected" || protection_type == "unprotected-LAG"
  end

  def is_multi_chassis
    return protection_type == "MChassis-LAG"
  end

  def is_single_chassis_lag
    return protection_type == "MCard-LAG" || protection_type == "SCard-LAG" || protection_type == "unprotected-LAG"
  end

  def is_lag
    return protection_type != "unprotected"
  end

  def is_system_owner_internal_enni
    return is_system_owner_internal_demarc
  end

  def config_description
    if is_connected_to_monitoring_port
      node = get_monitoring_node
      return "To #{node.node_type} (#{node.name})"
    elsif is_connected_to_trouble_shooting_port
      return "To SMB-6000"
    else
      return "#{cenx_name} -> Cenx ID: #{cenx_id}"
    end
  end

  def generate_config
    text = ""

    if (is_connected_to_trouble_shooting_port)
    text += "#------------------------------------------------\n"
    text += "# No Configuration is generated for ENNIs connected to SMB test sets\n"
    text += "#------------------------------------------------\n"
    return text
    end

    text += "#------------------------------------------------\n"
    text += "# Configuration is required on nodes: #{node_info}\n"
    text += "#------------------------------------------------\n"
    priority = 0
    ports.each do |port|
      text += "\n# Port Config on node #{port.node.name}\n"
      text += "#--------------------------------\n"
      text += "configure port #{port.name}\n"
      text += "shutdown\n"
      if is_connected_to_test_port
        text += "description \"#{config_description} - #{port.connected_port.name}\"\n"
      else
        text += "description \"#{config_description}\"\n"
      end
      text += "ethernet\n"
      text += "mode access\n"

      if port_enap_type == 'DOT1Q'
        text += "encap-type dot1q\n"
        text += "dot1q-etype #{ether_type}\n"
      elsif port_enap_type == 'QINQ'
        text += "encap-type qinq\n"
        text += "qinq-etype #{ether_type}\n"
      elsif port_enap_type == 'NULL'
        text += "encap-type null\n"
      else
        SW_ERR "ENNI PORT genconfig - bad encap type: #{port_enap_type}"
        text += "encap-type ERROR\n"
      end
      
      text += "mtu 9212\n"
      text += (auto_negotiate ? "autonegotiate limited": "no autonegotiate" ) + "\n" # 19/07/2010 added autoneg limited
      text += ah_supported ? "efm-oam\nno shutdown\nexit\n" : "efm-oam\nshutdown\nexit\n"
      text += "exit\n"
      if ! port.is_secondary_monitoring_port
        text += "no shutdown\n"
      end
      text += "exit\n"

      if(is_multi_chassis)
        #Figure out loopback address of peer node.
        peer_node = nil
        node_ids.each do |ni|
          if ni != port.node.id
            peer_node = Node.find(ni)
            #logger.info "********** Peer Node #{peer_node.name} -- #{ni} --- #{port.node.id}"
          end
        end

        if !(peer_node && peer_node.loopback_ip)
          #TODO readd - its causing tests to fail
          #SW_ERR "MCh LAG ENNI defined where peer node is unknown ID: #{cenx_id} Name: #{cenx_name}"
        end

        priority += 10
        text += "\n# MCh-Lag Config on node #{port.node.name}\n"
        text += "#-----------------------------------------\n"
        text += "configure #{lag_name}\n"
        text += "description \"MChassis Lag - #{config_description}\"\n"
        text += "mode access\n"

        if port_enap_type == 'DOT1Q'
          text += "encap-type dot1q\n"
        elsif port_enap_type == 'QINQ'
          text += "encap-type qinq\n"
        elsif port_enap_type == 'NULL'
          text += "encap-type null\n"
        else
          SW_ERR "ENNI PORT genconfig - bad encap type: #{port_enap_type}"
          text += "encap-type ERROR\n"
        end

        text += "access\n"
        text += "adapt-qos link\n"
        text += "exit\n"

        text += "port #{port.name} priority #{priority}\n"
        text += "lacp active\n"
        text += "no shutdown\n"
        text += "exit\n"
        text += "\n# MCh Redundancy Config on node #{port.node.name}\n"
        text += "#-----------------------------------------\n"
        text += "configure redundancy multi-chassis\n"
        text += "peer #{(peer_node && peer_node.loopback_ip) ? peer_node.loopback_ip : "ERROR: UNKNOWN_PEER_IP" } create\n"
        text += "mc-lag\n"
        text += "hold-on-neighbor-failure 2\n"
        text += "keep-alive-interval 5\n"
        text += "#{lag_name} lacp-key #{lag_id} system-id #{generate_mch_system_id} remote-#{lag_name} system-priority 100\n"
        text += "no shutdown\n"
        text += "exit\n"
        text += "no shutdown\n"
        text += "exit\n"
        text += "exit\n"
        text += "exit\n"
      end
    end

    if(is_single_chassis_lag)
      text += "\n# Lag Config on node #{node_info}\n"
      text += "#-------------------------------\n"
      text += "configure #{lag_name}\n"
      text += "description \"Lag - #{config_description}\"\n"
      text += "mode access\n"
      
      if port_enap_type == 'DOT1Q'
        text += "encap-type dot1q\n"
      elsif port_enap_type == 'QINQ'
        text += "encap-type qinq\n"
      elsif port_enap_type == 'NULL'
        text += "encap-type null\n"
      else
        SW_ERR "ENNI PORT genconfig - bad encap type: #{port_enap_type}"
        text += "encap-type ERROR\n"
      end

      # Only applies for business ENNIs
      if ! is_connected_to_monitoring_port
        text += "access\n"
        text += "adapt-qos link\n"
        text += "exit\n"
      end

      sub_group = 1
      ports.each do |port|
        priority += 10
        sub_group += 1
        text += "port #{port.name}"
        
        if protection_type == "unprotected-LAG"  || is_active_active
          # No priority applied to ports
          text += "\n"
        else
          text += " priority #{priority} sub-group #{sub_group}\n"
        end

      end 
      if ( protection_type != "unprotected-LAG")
        if(!is_connected_to_monitoring_port)
          text += "lacp active\n"
        end
      end
      text += "no shutdown\n"
      text += "exit\n"
    end

    if (is_connected_to_monitoring_port)
      text += "\n# This ENNI is connected to the EXFO Monitoring HW \n"
      text += "# Configure SM MAC Fwd. Filter on node #{node_info} \n"
      text += "#--------------------------------------------------\n"
      text += generate_sm_mac_fwd_filter

      text += "\n# Configure SM MAC Drop. Filter on node #{node_info} \n"
      text += "#--------------------------------------------------\n"
      text += generate_sm_mac_drop_filter
    end


    text += "\n#Save Configuration\n"
    text += "#------------------\n"
    text += "exit\n"
    text += "admin save\n"

    text += "\n#-------------------------------\n"
    text += "# Useful Troubleshoting Commands"
    text += "\n#-------------------------------\n"

    ports.each do |port|
      text += "\n# For Port #{port.node.name}\n"
      text += "show port #{port.name}\n"
      text += "show port #{port.name} detail\n"
      text += "show port #{port.name} statistics\n"
      text += "monitor port #{port.name}\n"
      text += "show port #{port.name} ethernet efm-oam\n"
      text += "clear port #{port.name} statistics\n"
    end

    if(is_lag)
      text += "\n# For #{interface_name}\n"
      text += "show #{interface_name}\n"
      text += "show #{interface_name} detail\n"
      text += "show #{interface_name} statistics\n"
      text += "monitor #{interface_name}\n"
      text += "clear #{interface_name} statistics\n"
    end

    if(is_multi_chassis)
      text += "\n# For Multi Chassis\n"
      text += "show redundancy multi-chassis all\n"
      text += "show redundancy multi-chassis mc-lag peer <Peer_node_ip>\n"
      text += "show redundancy multi-chassis mc-lag peer <Peer_node_ip> statistics\n"
      text += "tools perform lag force lag-id #{lag_id} standby\n"
      text += "tools perform lag clear-force lag-id #{lag_id}\n"
    end

    return text
  end

  def generate_mch_system_id
    id = lag_id.to_s
    SW_ERR "ERROR: lag_id #{id} on ENNI #{cenx_name} is larger than 12 characters and can not fit in a system-id" if id.length > 12
    #Calculate number of 0s to prefix
    prefix = [12 - id.length, 0].max
    #Build string without ":" deliminator
    mch_id = "0"*prefix + id
    #Split string into chunks of 2 then join with ":" between
    mch_id = mch_id.scan(/.{2}/).join(":")
    return mch_id[0, "00:00:00:00:00:00".length]
  end

  def generate_sm_mac_fwd_filter
    text = ""

    if !is_connected_to_monitoring_port
      SW_ERR "generate_sm_mac_fwd_filter called unnecessarily"
      return text
    end

    text += "configure filter mac-filter #{lag_id} create\n"
    node = get_monitoring_node
    text += "description \"#{node.node_type} Test Interface MAC Filter - allow frames to #{node.node_type} (#{node.name})\"\n"

    #for each Port add ACL config...
    entry_num = 10
      ports.each do |port|
        if ! port.is_test_port
          text += "ERROR #{port.name} not connected to a test port\n"
        else
          text += "entry #{entry_num} create\n"
          text += "description \"Dest MAC filter #{port.port_connection_info_port}\"\n"
          text += "match dst-mac #{port.connected_port.mac} ff:ff:ff:ff:ff:ff\n"

          # The Mac address is made equal to the Mac address of Provider A NID device.
          text += "action forward\n"
          text += "exit\n"
          entry_num += 10
        end
     end

     # Exit to top level
     text += "exit\nexit\n"
    return text 
  end

  def generate_sm_mac_drop_filter
    text = ""

    if !is_connected_to_monitoring_port
      SW_ERR "generate_sm_mac_drop_filter called unnecessarily"
      return text
    end

    offset = (lag_id.to_i - 199).abs
    filter_id = 65535 - offset
    text += "configure filter mac-filter #{filter_id} create\n"
    text += "default-action forward\n"
    node = get_monitoring_node
    text += "description \"Blocks #{node.node_type} ARP over non-monitored member ENNI - for IP Ping monitoring\"\n"

    #for each Port add ACL config...
    entry_num = 10
      ports.each do |port|
        if ! port.is_test_port
          text += "ERROR #{port.name} not connected to a test port\n"
        else
          text += "entry #{entry_num} create\n"
          text += "description \"Dest MAC filter #{port.port_connection_info_port}\"\n"
          text += "match dst-mac #{port.connected_port.mac} ff:ff:ff:ff:ff:ff\n"

          # The Mac address is made equal to the Mac address of Provider A NID device.
          text += "action drop\n"
          text += "exit\n"
          entry_num += 10
        end
     end

     # Exit to top level
     text += "exit\nexit\n"
    return text
  end

  def sla_availability_value
    
    case protection_type
      when "MChassis-LAG" then return 0.999995
      when "MCard-LAG" then return 0.99999
      when "unprotected", "unprotected-LAG" then return 0.998
    end

    SW_ERR "sla_availability_value: unhandled protection type #{protection_type}. Returning 1."
    return 1

  end
  
  def valid_ports?
    if (is_unprotected) && ports.length > 1
      errors.add(:ports, "- May not have more then one port for an unprotected ENNI")
    elsif ports.length > 2
      errors.add(:ports, "- May not have more then two ports for an ENNI")
    end
    nodes = ports.collect{|port| port.node}
    cards = ports.collect{|port| port.card_info}
    case protection_type
      when "MChassis-LAG" then
        errors.add(:ports, "must each be on a seperate node for MChassis-LAG") if nodes != nodes.uniq
      when "MCard-LAG" then
        if nodes.uniq.length > 1
          errors.add(:ports, "must each be on the same node for MCard-LAG")
        elsif cards != cards.uniq
          errors.add(:ports, "must each be on a seperate card for MCard-LAG")
        end
      when "SCard-LAG" then
        if nodes.uniq.length > 1
          errors.add(:ports, "must each be on the same node for SCard-LAG")
        elsif cards.uniq.length > 1
          errors.add(:ports, "must each be on the same card for SCard-LAG")
        end
    end
    return !errors[:ports].any?
  end
  
  def alarm_state        
    final_state = nil
    if is_lag
      # It's a lag of some sort - Each type of lag has a different number of lag.Interface (LI) and lag.PortTermination (LP)
      # Also check the state of each port in the lag 
      # An unprotected Lag - 1 LI and 1 LP
      # MultiCard Lag - 1 LI and 2 LP
      # MultiChassis Lag - 2 LI and 2 LP
      # The lag.Interface is alarmed when ALL lag ports are down
      # The lag.PortTermination is alarmed when the corresponding lag memeber is down.
      # So the state is calculated by 
      
      # 1 - For all the (lag.PortTermination + port alarm) for a node find the lowest alarm and if one of them is Unavilable then the state is Warning
      # 2 - For all lag.Interface find the lowest alarm
      # 3 - Take the highest of 1 & 2 as the final state
      if !is_connected_to_test_port
        # Find the lag.PortTermination and it's corresponding port alarm and take the highest alarm
        lag_port_termination_states = alarm_histories.collect do |ah| 
          if ah.event_filter[/lag:.*port-/] != nil
            slot, daughterslot, port = ah.event_filter[/port-.*/].split("-")[1].split("/")
            port_filter = ah.event_filter.split("lag")[0] + "shelf-1:cardSlot-#{slot}:card:daughterCardSlot-#{daughterslot}:daughterCard:port-#{port}"
            port_ah = AlarmHistory.find_by_event_filter(port_filter) 
            if port_ah
              ah.alarm_state + port_ah.alarm_state
            else   
              ah.alarm_state
            end
          end
        end
        
        lag_port_termination_alarm = AlarmRecord.select_lowest_alarm_or_warn(lag_port_termination_states)     

        # Find the lowest alarm of all the lag.Interface alarms
        lag_interface_alarm = nil          
        alarm_histories.each {|ah| lag_interface_alarm = ah.alarm_state & lag_interface_alarm if ah.event_filter[':port-'] == nil}
        if lag_interface_alarm == nil
          lag_interface_alarm = AlarmRecord.new(:time_of_event => (Time.now.to_f*1000).to_i, :state => AlarmSeverity::NO_STATE, :original_state => "No State", :details => "Error cannot calculate alarm state")
          SW_ERR "Unable to calculate #{self.class.name} alarm state probably due to missing alarm histories #{alarm_histories.collect {|ah| ah.event_filter}.inspect}"
        end
        final_state = lag_interface_alarm + lag_port_termination_alarm
      end
    else
      # Just add all alarm histories 
      final_state = super
    end    
    return final_state
  end
  
  def alarm_history    
    final_history = nil
    if !is_connected_to_test_port && is_lag
      lag_port_termination_histories = alarm_histories.collect do |ah| 
        if ah.event_filter[/lag:.*port-/] != nil
          slot, daughterslot, port = ah.event_filter[/port-.*/].split("-")[1].split("/")
          port_filter = ah.event_filter.split("lag")[0] + "shelf-1:cardSlot-#{slot}:card:daughterCardSlot-#{daughterslot}:daughterCard:port-#{port}"
          port_ah = AlarmHistory.find_by_event_filter(port_filter) 
          if port_ah
            ah + port_ah
          else   
            ah
          end
        end
      end
            
      lag_port_termination_history = AlarmHistory.merge(lag_port_termination_histories) {|events| AlarmRecord.select_lowest_alarm_or_warn(events)}     
      # merge the lag.Interface taking the lowest alarm
      lag_interface_history = nil          
      alarm_histories.each {|ah| lag_interface_history = ah & lag_interface_history if ah.event_filter[':port-'] == nil}
      if lag_interface_history == nil
        SW_ERR "Unable to calculate #{self.class.name} alarm history probably due to missing alarm histories #{alarm_histories.collect {|ah| ah.event_filter}.inspect}"
        lag_interface_history = AlarmHistory.new
      end
      # now merge the two taking the highest alarm
      final_history = lag_port_termination_history + lag_interface_history      
    else
       # Just add all alarm histories for non lag
       final_history = super      
    end
    return final_history
  end
  
  def go_in_service    
    if is_connected_to_test_port
      # delete alarm and mtc histories
      mtc_periods.destroy_all
      alarm_histories.destroy_all
      sm_histories.destroy_all
      return {:result => true, :info => ""}
    end
        
    # create alarm and service monitoring histories
    sm_histories << SmHistory.create(:event_filter => "SM #{self.class.name}:#{cenx_id}") if sm_histories.empty?
    return_code = create_alarm_histories
    info = ""
    
    # create the Maintenance history    
    if return_code && mtc_periods.empty?
      mtc_periods << MtcHistory.create(:event_filter => "Mtc ENNI:#{cenx_id}")
      self.set_mtc(false)
    end
    
    info = "Failed to initialise" unless return_code
    return {:result => return_code, :info => info}
  end
  
  def mtc_state
    nodes = node_info.gsub(/ /,'').split(';')
    state = nil
    nodes.each {|node| state = Node.find_by_name(node).mtc_state & state if Node.find_by_name(node) != nil}
    state = super + state
    return state
  end
  
  def mtc_history    
    nodes = node_info.gsub(/ /,'').split(';')
    state = nil
    nodes.each {|node| state = Node.find_by_name(node).mtc_history & state if Node.find_by_name(node) != nil}
    state = super + state
    return state
  end

  def get_enni_mac_addresses
    result = false
    mac_primary = mac_secondary = ""

    if ports.size == 0
      mac_primary = mac_secondary = "ERROR no ports attached to ENNI"
      return result, mac_primary, mac_secondary
    end

    if is_lag
      sam_class_name = "lag.Interface"
      macs = []    
      api = SamIf::StatsApi.new(get_network_manager)
      node_ids.each do |node_id|
        result, data = api.get_inventory(sam_class_name, {"displayedName" => stats_displayed_name, "siteId" => Node.find(node_id).loopback_ip}, ["macAddress"])
        if data.size != 1
          SW_ERR "None or more than one object returned for inventory query class:#{sam_class_name} Port: #{stats_displayed_name} Node: #{Node.find(node_id).loopback_ip} returned data:#{data.inspect}"
        else
          macs << data[0]["macAddress"]
        end
      end
      api.close
      mac_primary = macs[0].gsub(/-/, ":") if macs[0] != nil
      mac_secondary = macs[1].gsub(/-/, ":") if macs[1] != nil     
    end
    return result, mac_primary, mac_secondary
  end

  def get_network_manager
    if ports.first == nil
      return nil
    else
      return ports.first.node.network_manager
    end
  end

  def get_alarm_keys
    # To get LAG event must look for alarms raised on the lag.Interface and lag.PortTermination classes
    # lag.PortTermination is for per Lag member alarms (eg a shutdown of the Lag will cause an alarm agist each Lag memeber)
    # lag.Interface is for the whole Lag for a node so for a MultiCard or unprotected LAG when the all the ports is shutdown then this alarm is triggered (but no lag.PortTermination alarms)
    # For MultiChassis there are 2 lag.Interface's one on each node but the appear to sync ie when both ports are shutdown the lag.Interface on each node has an alarm.
    return_code = true
    queries = []
    # Always get the physical port filters
    sam_class_name = "equipment.PhysicalPort"
    ports.each do |port|
      queries << {:class_name => sam_class_name, :query_attrs => {"displayedName" => port.stats_displayed_name, "siteId" => Node.find(port.node_id).loopback_ip}}
    end
    
    if is_lag
      # Get the lag.PortTermination objectName
      ports.each do |port|
        clean_name = port.name.strip #strip any leading/trailing spaces
        queries << {:class_name => "lag.PortTermination", :query_attrs => {"displayedName" => "Port #{clean_name}", "nodeId" => port.node.loopback_ip, "lagId" => lag_id}}
      end
      # Get the lag.Interface objectName
      sam_class_name = "lag.Interface"
      node_ids.each do |node_id|
        queries << {:class_name => sam_class_name, :query_attrs => {"displayedName" => stats_displayed_name, "siteId" => Node.find(node_id).loopback_ip}}
      end
    end

    # Get the SAM object name for each object
    new_alarms = []
    api = SamIf::StatsApi.new(get_network_manager)
    queries.each do |query|
      enni_object_name = ""
      result, data = api.get_inventory(query[:class_name], query[:query_attrs], ["objectFullName"])
      if !result || data.blank?
        SW_ERR "Failed to get inventory for: #{cenx_name} class: #{query[:class_name]} query_attrs: #{query[:query_attrs]}"
        enni_object_name = ""
      else
        enni_object_name = data[0]["objectFullName"] # There should only be one item
        if data.size != 1
          SW_ERR "None or more than one object returned for inventory query class:#{query[:class_name]} query_attr: #{query[:query_attrs]} returned data:#{data.inspect}"
        end
      end

      if !enni_object_name.blank?
        new_alarms << enni_object_name
      else
        SW_ERR "ENNI Name is blank"
        return_code = false
      end
    end
    api.close

    return return_code, new_alarms

  end

  # Return a Stats object which is used to hold/get Stats information
  # for the given time period
  def stats_data(start_time, end_time, current_time = Time.now, adjust_time = true)
    stats = EhealthEnniStatsArchive.new(self, start_time, end_time, current_time, adjust_time)
    return stats
  end

  def update_alarm(list_of_evaled_objects = [], full_eval = false)
    return false if is_connected_to_test_port # This is connected to a test port - ignore
    changed = super
    return changed
  end
  
  
  def self.find_ennis(path_owner, user, service_provider_ids=nil, site_ids=nil, ordering_state=nil, provisioning_state=nil, monitoring_state=nil, operator_network_type_ids=nil, connected_to_test_port=nil)
    
    owner_service_provider_id = user.service_provider.id
    operator_network_types_ids = user.operator_network_types.map(&:id)

    conditions_args = {:operator_network_types_ids => operator_network_types_ids,
                       :current_service_provider_id => owner_service_provider_id, 
                       :filtered_service_provider_id => service_provider_ids, 
                       :site_ids => site_ids,
                       :ordering_state => ordering_state,
                       :provisioning_state => provisioning_state,
                       :monitoring_state => monitoring_state,
                       :operator_network_type_ids => operator_network_type_ids}
    finder = EnniNew.joins("INNER JOIN sin_enni_service_provider_sphinxes ON sin_enni_service_provider_sphinxes.enni_id = demarcs.id
                            INNER JOIN sites ON sites.id = demarcs.site_id
                            INNER JOIN operator_networks AS exchange_operator_networks ON exchange_operator_networks.id = sites.operator_network_id
                            INNER JOIN operator_networks ON operator_networks.id = demarcs.operator_network_id
                            INNER JOIN service_providers ON service_providers.id = operator_networks.service_provider_id").
                     where('demarcs.demarc_icon <> ?', 'router')
    
    
    unless connected_to_test_port.nil?
      if connected_to_test_port
        finder = finder.joins('INNER JOIN `ports` ON ports.demarc_id = demarcs.id 
                               INNER JOIN `ports` connected_ports_ports ON `connected_ports_ports`.id = `ports`.connected_port_id
                               INNER JOIN `nodes` ON `nodes`.id = `connected_ports_ports`.node_id').
                        where('node_type IN (?)', Node::MONITORING_TYPES)
      else
        finder = finder.joins('LEFT OUTER JOIN `ports` ON ports.demarc_id = demarcs.id 
                               LEFT OUTER JOIN `ports` connected_ports_ports ON `connected_ports_ports`.id = `ports`.connected_port_id
                               LEFT OUTER JOIN `nodes` ON `nodes`.id = `connected_ports_ports`.node_id').
                        where('node_type NOT IN (?) OR node_type IS NULL', Node::MONITORING_TYPES)
      end
    end

    # Scope by users operator_network_types
    unless operator_network_types_ids.empty?
      finder = finder.where('operator_networks.operator_network_type_id IN (:operator_network_types_ids) OR exchange_operator_networks.operator_network_type_id IN (:operator_network_types_ids)', conditions_args)
    end
    
    unless ordering_state.blank?
      finder = finder.where('demarcs.order_name IN (:ordering_state)', conditions_args)
    end
    
    unless provisioning_state.blank?
      finder = finder.where('demarcs.prov_name IN (:provisioning_state)', conditions_args)    
    end
    
    unless monitoring_state.blank?
      finder = finder.where('demarcs.sm_state IN (:monitoring_state)', conditions_args)          
    end
    
    unless path_owner == 'admin'
      finder = finder.where('sin_enni_service_provider_sphinxes.service_provider_id = :current_service_provider_id', conditions_args)
    end

    unless service_provider_ids.blank?
      finder = finder.where('service_providers.id IN (:filtered_service_provider_id)', conditions_args)
    end

    unless site_ids.blank?
      finder = finder.where('demarcs.site_id IN (:site_ids)', conditions_args)
    end

    unless operator_network_type_ids.blank?
      finder = finder.where('operator_networks.operator_network_type_id IN (:operator_network_type_ids) OR exchange_operator_networks.operator_network_type_id IN (:operator_network_type_ids)', conditions_args)
    end

    finder
  end
  
  def latitude
    site ? site.latitude : 0.0
  end
  
  def longitude
    site ? site.longitude : 0.0
  end
  
  # Get address from Site if there is one
  def address
    site ? site.address : super
  end

  # KPI methods
  def kpi_hierarchy(kpi_object)
    on = operator_network
    sp = on.service_provider
    return [[self, site],[site, on],[on, sp],[sp, sp]]
  end  
  
  def update_kpi(parent, parent_kpi, kpi_name, kpi_object)
    if parent_kpi == nil      
      parent_kpi = site.get_kpi(kpi_name, kpi_object, operator_network)
      parent = site
    end
    calculate_kpi(kpi_name, kpi_object, parent_kpi, parent)
  end  
  
  def update_kpis
    if site != nil && operator_network != nil
      Kpi::KPI_BY_KLASS[EnniNew.name].each do |kpi_name|
        kpi_setup(kpi_name, EnniNew.name)
      end
    end
  end
  
  
  private

  def create_alarm_histories
    return_code, new_alarms = get_alarm_keys
    if return_code
      new_enni_alarms = {} 
      # All port alarms will map minor, warning => Unavailable
      port_alarm_mapping = AlarmSeverity.default_alu_alarm_mapping
      port_alarm_mapping["minor"] = AlarmSeverity::UNAVAILABLE
      port_alarm_mapping["warning"] = AlarmSeverity::UNAVAILABLE
      
      new_alarms.each do |filter| 
        if filter[/daughterCard:port-/]
          # All port alarms will map minor, warning => Unavailable
          new_enni_alarms[filter] = {:type => AlarmHistory, :mapping => port_alarm_mapping}
        else
          new_enni_alarms[filter] = {:type => AlarmHistory, :mapping => AlarmSeverity.default_alu_alarm_mapping}
        end
      end
      return_code = update_alarm_history_set(new_enni_alarms)
    end

    return return_code
  end

  def valid_lag_id?
    if not is_lag
      unless ( lag_id == nil )
        errors.add(:lag_id, "must be blank for unprotected enni")
      end
    else
      unless ( lag_id && lag_id >= 1 && lag_id <= 199 )
        errors.add(:lag_id, " ID not valid. Range is: 1 to 199")
      end
    end
  end

  def valid_lag_mode?
    # TODO should call function that returns allowable values
    # This function should also be reused in the pulldown
    if is_unprotected
      if is_lag # ie: unprotected_lag
        unless ( lag_mode == "N/A" )
          errors.add(:lag_mode, "must be N/A for #{protection_type} ENNI")
        end
      else #ie: unprotected
        unless ( lag_mode == nil or lag_mode == '')
          errors.add(:lag_mode, "must be blank for #{protection_type} ENNI")
        end
      end
    elsif is_multi_chassis
      unless ( lag_mode == "Active/Standby" )
        errors.add(:lag_mode, " Must be Active/Standby for #{protection_type} ENNI")
      end
    else # ie: single chassis
      unless ( lag_mode == "Active/Standby" || lag_mode == "Active/Active")
        errors.add(:lag_mode, " Must be Active/Standby or Active/Active for #{protection_type} ENNI")
      end
    end
  end

  def valid_emergency_contact?
    contacts = service_provider.contacts + site.operator_network.service_provider.contacts
    contacts.flatten
    if (emergency_contact and (not contacts.include? emergency_contact))
      errors.add :emergency_contact, "Emergency contact with ID #{emergency_contact_id} is not associated with ENNI's Service Provider #{service_provider.name}"
    end
  end

end
