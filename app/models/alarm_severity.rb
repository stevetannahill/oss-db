class AlarmSeverity
include Comparable  
  OK = "Ok"
  WARNING = "Warning"
  FAILED = "Failed"
  NO_STATE = "No State"
  MTC = "Maintenance"
  UNAVAILABLE = "Unavailable"
  DEPENDENCY = "Dependency"
  NOT_MONITORED = "Not Monitored"
  NOT_LIVE = "Not Live"
  attr_accessor :state
  
  def self.serverities
    # return severities in accending order
    return [OK, WARNING, FAILED, DEPENDENCY, UNAVAILABLE, NOT_MONITORED, MTC, NOT_LIVE, NO_STATE]
  end
  
  def self.default_alu_alarm_mapping
    # These strings are obtained from the ALU summary.xml file - type = equipment.ExternalAlarmSeverity
    return {"cleared" => OK,
            "suppressed" => OK,
            "indeterminate" => OK,
            "info" => OK,
            "condition" => OK,
            "warning" => WARNING,
            "minor" => WARNING,
            "major" => UNAVAILABLE,
            "critical" => UNAVAILABLE,
            "maintenance" => MTC, # This is not an ALU alarm level - Used for CENX maintenance periods
            "No State" => NO_STATE
           }
  end
  
  def initialize(severity)    
    AlarmSeverity.serverities.include?(severity) ? @severity = severity : raise(ArgumentError, "Invalid alarm severity #{severity}", caller)
  end

  def severity=(new_severity)
    AlarmSeverity.serverities.include?(new_severity) ? @severity = new_severity : raise(ArgumentError, "Invalid alarm severity #{new_severity}", caller)
  end
      
  def to_s
    @severity.to_s
  end
                
  def <=>(other)
    return TRUTH_TABLE[@severity][other.to_s]               
  end
  
  protected
  
  TRUTH_TABLE = {OK             => {OK => 0, WARNING => -1, FAILED => -1, DEPENDENCY => -1, UNAVAILABLE => -1, NOT_MONITORED => -1, MTC => -1, NOT_LIVE => -1, NO_STATE => -1},
                 WARNING        => {OK => 1, WARNING =>  0, FAILED => -1, DEPENDENCY => -1, UNAVAILABLE => -1, NOT_MONITORED => -1, MTC => -1, NOT_LIVE => -1, NO_STATE => -1},
                 FAILED         => {OK => 1, WARNING =>  1, FAILED =>  0, DEPENDENCY => -1, UNAVAILABLE => -1, NOT_MONITORED => -1, MTC => -1, NOT_LIVE => -1, NO_STATE => -1},
                 DEPENDENCY  => {OK => 1, WARNING =>  1, FAILED =>  1, DEPENDENCY =>  0, UNAVAILABLE => -1, NOT_MONITORED => -1, MTC => -1, NOT_LIVE => -1, NO_STATE => -1},
                 UNAVAILABLE    => {OK => 1, WARNING =>  1, FAILED =>  1, DEPENDENCY =>  1, UNAVAILABLE =>  0, NOT_MONITORED => -1, MTC => -1, NOT_LIVE => -1, NO_STATE => -1},
                 NOT_MONITORED  => {OK => 1, WARNING =>  1, FAILED =>  1, DEPENDENCY =>  1, UNAVAILABLE =>  1, NOT_MONITORED =>  0, MTC => -1, NOT_LIVE => -1, NO_STATE => -1},
                 MTC            => {OK => 1, WARNING =>  1, FAILED =>  1, DEPENDENCY =>  1, UNAVAILABLE =>  1, NOT_MONITORED =>  1, MTC => 0, NOT_LIVE => -1, NO_STATE => -1},
                 NOT_LIVE => {OK => 1, WARNING =>  1, FAILED =>  1, DEPENDENCY =>  1, UNAVAILABLE =>  1, NOT_MONITORED =>  1, MTC => 1, NOT_LIVE =>  0, NO_STATE => -1},
                 NO_STATE       => {OK => 1, WARNING =>  1, FAILED =>  1, DEPENDENCY =>  1, UNAVAILABLE =>  1, NOT_MONITORED =>  1, MTC => 1, NOT_LIVE =>  1, NO_STATE =>  0},
                }
  
end
