
class ScheduledTask < ActiveRecord::Base
  ENVIRONMENTS = %w(sin oss-db)
  
  has_many :schedules, :dependent => :destroy
  belongs_to :user
    
  validates :name, :presence => true
  validates_inclusion_of :environment, :in => ENVIRONMENTS  
  
  attr_protected :executable_script
  
  def supported_attachemts
    {"pdf" => false, "csv" => false, "web_link" => false}
  end 
  
  def get_env_script
    case environment
    when "sin" then "/home/deployer/local/bin/sin_env"
    when "oss-db" then "/home/deployer/local/bin/oss-db_env"
    else
      SW_ERR "Invalid environment (#{environment})"
      ""
    end
  end

end
# == Schema Information
#
# Table name: scheduled_tasks
#
#  id                :integer(4)      not null, primary key
#  name              :string(255)     not null
#  user_id           :integer(4)
#  filter_options    :text
#  executable_script :string(255)
#  created_at        :datetime        not null
#  updated_at        :datetime        not null
#  report_type       :string(255)
#  type              :string(255)
#

