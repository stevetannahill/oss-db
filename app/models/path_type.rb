# == Schema Information
# Schema version: 20110802190849
#
# Table name: path_types
#
#  id                       :integer(4)      not null, primary key
#  name                     :string(255)
#  operator_network_type_id :integer(4)
#  path_type                 :string(255)
#  max_num_unis             :integer(4)
#  mtu                      :integer(4)
#  created_at               :datetime
#  updated_at               :datetime
#


class PathType < TypeElement
  belongs_to :operator_network_type
  has_many :paths, :dependent => :nullify
  has_many :instance_elements, :class_name => "Path"
  has_many :member_attrs, :as => :affected_entity, :dependent => :destroy
  
  validates_presence_of :name, :operator_network_type_id, :path_type
  validates_uniqueness_of :name, :scope => :operator_network_type_id
  validates_inclusion_of :path_type, :in => HwTypes::Path_TYPES, :message => "invalid, Use: #{HwTypes::Path_TYPES.join(", ")}"
  validates_numericality_of :mtu, :max_num_unis, :greater_or_equal_to => 0, :only_integer => true, :allow_nil => true
  
end
