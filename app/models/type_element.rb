class TypeElement < ActiveRecord::Base
  self.abstract_class = true
  
  has_many :member_attrs, :as => :affected_entity, :dependent => :destroy
  
  def member_attr_count
    return member_attrs.length
  end
  
  def id_class
    return id.to_s + ";" + self.class.table_name.classify
  end
  
  def self.get_subclasses
    self.subclasses.dup
  end
  
end
