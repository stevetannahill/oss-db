# == Schema Information
#
# Table name: top_talkers
#
#  id                :integer(4)      not null, primary key
#  type              :string(255)
#  enni_id           :integer(4)
#  cos_end_point_id  :integer(4)
#  octets_count      :float
#  direction         :string(255)
#  time_period_start :integer(4)
#  time_period_end   :integer(4)
#

# To change this template, choose Tools | Templates
# and open the template in the editor.

class TopTalker  < ActiveRecord::Base

  def average_mbps
     ((self.octets_count * 8) / (self.time_period_end - self.time_period_start)) / 1000000
  end
  
  def capactiy_percentage
  end
  
end
