# == Schema Information
#
# Table name: site_groups
#
#  id         :integer(4)      not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#  type       :string(255)
#

#

class SiteGroup < ActiveRecord::Base  
  has_many :site_groups_sites
  has_many :sites, :through => :site_groups_sites, :order => :name

  validates_presence_of :name
  validates_uniqueness_of :name

  def self.find_rows_for_index
    order("name")
  end
end
