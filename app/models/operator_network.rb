# == Schema Information
#
# Table name: operator_networks
#
#  id                       :integer(4)      not null, primary key
#  name                     :string(255)
#  service_provider_id      :integer(4)
#  is_processing            :boolean(1)
#  job_type                 :string(255)
#  job_id                   :integer(4)
#  created_at               :datetime
#  updated_at               :datetime
#  operator_network_type_id :integer(4)
#

class OperatorNetwork < InstanceElement
  include Kpiable
  
  belongs_to :service_provider
  belongs_to :operator_network_type
  belongs_to :type_element, :class_name => "OperatorNetworkType", :foreign_key => "operator_network_type_id"
  
  has_many :sites, :dependent => :nullify
  has_many :paths, :dependent => :destroy
  has_many :demarcs, :dependent => :destroy
  has_many :enni_news, :class_name => 'EnniNew', :dependent => :destroy
  has_many :unis, :class_name => 'Uni', :dependent => :destroy
  has_many :segments, :class_name => 'Segment', :dependent => :destroy
  has_many :reachable_segments, :class_name => "Segment", :foreign_key => :path_network_id, :dependent => :destroy
  has_many :service_orders, :dependent => :destroy

  has_many :access_permissions
  
  validates_presence_of :name, :operator_network_type_id
  validates_uniqueness_of :name, :scope => :service_provider_id
  
  after_save :clear_invalid_types

  def self.find_rows_for_index
    order("name")
  end

  def self.find_rows_for_path_index
    order( "name" )
  end

  def self.find_cenx_operator_networks
    ons = find_rows_for_index.select {|on| on.is_system_owner_internal_operator_network}
  end

  def self.find_member_operator_networks
    ons = find_rows_for_index.reject {|on| on.is_system_owner_internal_operator_network}
  end

  def self.find_cenx_networks_w_no_sites_attached
    @ons = find_cenx_operator_networks.select{|on| on.sites.empty? }
  end

  def is_system_owner_internal_operator_network
    return service_provider.is_system_owner
  end

  def pull_down_name_on
    return "#{service_provider.name} - #{name}"
  end
  
  def sp_info_on
    return service_provider ? "#{service_provider.name}" : "None!"
  end
  
  def type_info_on
    return operator_network_type ? "#{operator_network_type.name}" : "Not Set!"
  end

  def get_candidate_ethernet_service_types
    return operator_network_type ? operator_network_type.ethernet_service_types : []
  end

  def get_candidate_segment_types
    return operator_network_type ? operator_network_type.segment_types : []
  end
  
  def get_candidate_contacts
    return service_provider ? service_provider.contacts : []
  end

  def ennis_on_behalf_of
    ennis = service_provider.ennis_on_behalf_of.reject {|e| (e.site.operator_network ? e.site.operator_network.id : nil) != id}
  end

  # KPI methods
  def update_kpi(parent, parent_kpi, kpi_name, kpi_object)
    if parent_kpi == nil
      case kpi_object 
      when EnniNew.name, BxCosTestVector.name, OnNetOvcEndPointEnni.name, SpirentCosTestVector.name
        parent_kpi = service_provider.get_kpi(kpi_name, kpi_object, service_provider)
        parent = service_provider
      else
        SW_ERR "Invalid kpi_object #{kpi_object} for #{self.class}:#{self.id}"
      end
    end
    on_update_kpi(parent, parent_kpi, kpi_name, kpi_object)
  end

  def valid_access_permission_targets
    return OperatorNetwork.all - [self]
  end

  def full_access_operator_networks
    valid_access_permission_targets - approval_required_operator_networks - no_access_operator_networks
  end

  def approval_required_operator_networks
    AccessPermission.find_all_by_target_id_and_permission(id, "Approval Required", :include => :operator_network).map{|ap| ap.operator_network}
  end

  def no_access_operator_networks
    AccessPermission.find_all_by_target_id_and_permission(id, "No Access", :include => :operator_network).map{|ap| ap.operator_network}
  end
  
  private
  
  def on_update_kpi(parent, parent_kpi, kpi_name, kpi_object)
    kpi = calculate_kpi(kpi_name, kpi_object, parent_kpi, parent)
    case kpi_object
    when EnniNew.name
      sites = Site.joins(:ennis => :operator_network).where(:demarcs => {:operator_network_id => self}).uniq
      sites.each do |site|
        site.update_kpi(self, kpi, kpi_name, kpi_object)
      end    
    when BxCosTestVector.name, SpirentCosTestVector.name
      #The following checks to see if all CTVs points to my KPI in which case nothing to do...
      #num_segments_with_ctv = segments.joins(:segment_end_points => {:cos_end_points => :cos_test_vectors}).size
      #return if segments.joins(:segment_end_points => {:cos_end_points => {:cos_test_vectors => :kpis}}).where("`kpis`.`id` = ?", kpi.id).size == num_segments_with_ctv    
      segments.each do |segment|
        segment.update_kpi(self, kpi, kpi_name, kpi_object)
      end
    when OnNetOvcEndPointEnni.name
      self.sites.each do |site|
        site.update_kpi(self, kpi, kpi_name, kpi_object)
      end
    else
      SW_ERR "Invalid kpi_object #{kpi_object}"
    end
  end
  
  def clear_invalid_types
    demarcs.each{|demarc|
      if demarc.demarc_type && !(demarc.get_candidate_demarc_types.include? demarc.demarc_type)
        demarc.demarc_type_id = nil
        demarc.save(false)
      end
    }
  end
  
end
