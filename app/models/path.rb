# == Schema Information
#
# Table name: paths
#
#  id                  :integer(4)      not null, primary key
#  operator_network_id :integer(4)
#  created_at          :datetime
#  updated_at          :datetime
#  cenx_id             :string(255)
#  description         :string(255)
#  path_type_id        :integer(4)
#  cenx_path_type      :string(255)     default("Standard")
#  cenx_name_prefix    :string(255)
#  event_record_id     :integer(4)
#  sm_state            :string(255)
#  sm_details          :string(255)
#  sm_timestamp        :integer(8)
#  prov_name           :string(255)
#  prov_notes          :text
#  prov_timestamp      :integer(8)
#  order_name          :string(255)
#  order_notes         :text
#  order_timestamp     :integer(8)
#  type                :string(255)
#


class Path < InstanceElement
  include Eventable
  include Stateable
  
  # SIN association, model used for sphinx indexing
  has_many :sin_path_service_provider_sphinxes, :dependent => :destroy
  has_many :path_displays, :order => "path_displays.column, path_displays.row, ifnull(path_displays.relative_position, 'Middle')", :dependent => :destroy
  has_many :service_orders, :as => :ordered_entity, :dependent => :destroy
  # has_many :network_components, :class_name => 'PathDisplay', :order => "path_displays.row, path_displays.column"

  belongs_to :operator_network
  has_one :service_provider, :through => :operator_network
  belongs_to :path_type
  belongs_to :type_element, :class_name => "PathType", :foreign_key => "path_type_id"
  has_and_belongs_to_many :segments, :class_name => 'Segment'
    
  has_many :fallout_exceptions, :as => :fallout_item, :order => "timestamp DESC", :dependent => :destroy
  has_one :fallout_exception, :as => :fallout_item

  validates_presence_of :operator_network_id, :cenx_path_type
  validates_inclusion_of :cenx_path_type, :in => HwTypes::CENX_Path_TYPES,
                      :message => "invalid. Use one of #{HwTypes::CENX_Path_TYPES.join(", ")}."
  
  validates_as_cenx_id :cenx_id
  
  # Associate service providers to Paths and Ennis
  after_save :assoc_service_providers_for_indexing_entities

  scope :on_net_ovcs, includes(:segments => :site).where("segments.type = ?", "OnNetOvc")
  scope :on_net_routers, includes(:segments => :site).where("segments.type = ?", "OnNetRouter")

  # Force an index since MySql doesn't use it sometimes :(
  scope :path_display_by_provider, lambda { |index_name, skip_index=false|
    # Must create unique alias for table incase multiple filters are used
    table_alias = "ed_#{index_name}"
    table_index = skip_index ? "" : "USE INDEX (#{index_name})"
    joins("INNER JOIN path_displays as #{table_alias} #{table_index} ON #{table_alias}.path_id = paths.id").where("#{table_alias}.service_provider_id = sin_path_service_provider_sphinxes.service_provider_id")
  }

  scope :s_sow_order_counts, lambda { |service_order_id|
                                        select('service_orders.id, service_orders.title, service_orders.cenx_id, service_orders.notes, count(distinct circuit_order_join.id) as circuit_order_count').
                                        joins('INNER JOIN service_orders AS circuit_orders ON circuit_orders.ordered_entity_id = paths.id 
                                               INNER JOIN bulk_orders_service_orders AS circuit_order_join ON circuit_orders.id = circuit_order_join.service_order_id
                                               INNER JOIN service_orders ON circuit_order_join.bulk_order_id = service_orders.id').
                                        where('circuit_orders.bulk_order_type = ?', "Circuit Order").
                                        where('service_orders.bulk_order_type = ?', "Statement of Work").
                                        where('service_orders.id = ?', service_order_id)
                                    }

  scope :s_agg_site_counts, lambda { |skip_index|
                              select("count(distinct paths.id) as path_count, sites.name as site_name, ont.name as network_type_name, sites.id as site_id").
                              path_display_by_provider('site_search', skip_index).
                              joins("INNER JOIN sites ON sites.id = ed_site_search.site_id").
                              joins("INNER JOIN operator_network_types as ont ON operator_networks.operator_network_type_id = ont.id").
                              where('sites.type = ?', 'AggregationSite').
                              group('sites.name')
                            }
             
  scope :s_demarc_sites, joins(:path_displays).
                         where("path_displays.service_provider_id = sin_path_service_provider_sphinxes.service_provider_id").
                         where("path_displays.demarc_icon = ?", 'cell site').
                         where("path_displays.entity_type = ?", 'Demarc').
                         where("path_displays.redacted = ?", false)
                            
  scope :s_aav, where("ed_owner_service_provider_search.component_type = 'OffNetOvc'")

  scope :s_provisioning_counts, select("count(distinct paths.id) as prov_count, paths.prov_name as prov_name_display").
                                where('paths.prov_name IS NOT NULL').
                                group('prov_name_display')     

  scope :s_provisioning_counts_with_fallout, select("count(distinct paths.id) as prov_count, paths.prov_name as prov_name, IF(paths.fallout_severity = 'ok', paths.prov_name, 'fallout') as prov_name_display").
                                             where('paths.prov_name IS NOT NULL').
                                             group('prov_name_display')  


  scope :s_ordering_counts, select("count(distinct paths.id) as order_count, paths.order_name as order_name").
                            where('paths.order_name IS NOT NULL').
                            group('paths.order_name')     
                            
  scope :s_monitoring_counts, select("count(distinct paths.id) as sm_count, paths.sm_state as sm_state").
                              where('paths.sm_state IS NOT NULL').
                              group('paths.sm_state')              
                              
  scope :s_events_by_state, lambda { |states|  where('paths.sm_state IN (?)', states).group('paths.id') }

  scope :s_unique, group('paths.id')

  # Fallout exception scopes
  scope :s_fallout_category_counts, select("count(distinct paths.id) as fallout_category_count, fallout_category").
                                    where("paths.fallout_category IS NOT NULL AND paths.fallout_category != ''").
                                    group("fallout_category")     

  scope :s_date_range, lambda { |from_timestamp, to_timestamp|
    where('paths.fallout_timestamp >= ? AND paths.fallout_timestamp <= ?', from_timestamp.to_i, to_timestamp.to_i)
  }

  before_validation(:on => :create) do
    self.cenx_id = IdTools::CenxIdGenerator.generate_cenx_id(self.class)
  end
  
  before_save :general_before_save
  before_destroy :destroy_orphaned_segments
  
  def self.s_service_provider_counts(finder, skip_index, params={})
    unless params[:oem].present? || params[:operator_network].present?
      finder = finder.joins("INNER JOIN paths_segments ON paths_segments.path_id = paths.id").
                      joins("INNER JOIN segments ON paths_segments.segment_id = segments.id")
    end

    finder = finder.select("count(distinct paths.id) as path_count, service_providers.name as service_provider_name, service_providers.id as service_provider_id, count(distinct segments.site_id) as site_count").
                    path_display_by_provider('owner_service_provider_search', skip_index).
                    joins("INNER JOIN service_providers ON service_providers.id = ed_owner_service_provider_search.owner_service_provider_id").
                    group('service_providers.name')                            

    finder
  end

  def self.s_oem_counts(finder, params={})
    unless params[:oem].present? || params[:operator_network].present?
      finder = finder.joins("INNER JOIN paths_segments ON paths_segments.path_id = paths.id").
                      joins("INNER JOIN segments ON paths_segments.segment_id = segments.id")
    end

    unless params[:site].present?
      finder = finder.path_display_by_provider('site_search', true)
    end

    finder = finder.select("count(distinct paths.id) as path_count, operator_network_types.name as operator_network_type_name, operator_network_types.id as operator_network_type_id, count(distinct ed_site_search.site_id) as site_count").
                    joins('INNER JOIN operator_networks AS segments_operator_networks ON segments.operator_network_id = segments_operator_networks.id').
                    joins('INNER JOIN operator_network_types ON segments_operator_networks.operator_network_type_id = operator_network_types.id').
                    group('operator_network_types.id')
  end

  # Convenience methods for accessing fallout exception severity
  def fallout_severity
    read_attribute(:fallout_severity).to_sym
  end
  
  def fallout_severity= (value)
    write_attribute(:fallout_severity, value.to_s)
  end

  def fallout?
    fallout_severity != :ok
  end

  def latest_fallout_exception
    FalloutException.exception_types.collect do |type|
      fallout_exceptions.current.where(:exception_type => type).first
    end.compact.max
  end

  # Default to layer 2
  def layer
    2
  end

  # List of service providers that might need to know about this object's events
  def effected_members
    path_owner_member = operator_network.service_provider
    segment_member_owners = segments.map{|o|o.get_operator_network && o.get_operator_network.service_provider}.compact #TODO why can't I use the operator_network instead, checking for nil sucks
    
    ([path_owner_member] + segment_member_owners).uniq
  end

  #---------------------------------
  # Start Instance Element Code
  #---------------------------------

  def member_handle_owners

    # if cenx owns the Path let CENX gve it a name
    # Sprint demo hack
#    if service_provider.is_system_owner
#      return [service_provider]
#    end

    #Else Path gets a name from all Buyers/Sellers involved
    maos = segments.select{|segment| segment.is_a? OffNetOvc}.map{|offovc| offovc.service_provider}.uniq #member handle owners
    maos += (demarcs + ennis).map {|demarc| demarc.service_provider}.uniq
    maos << ServiceProvider.system_owner
    maos.push(self.service_provider) if self.service_provider && !maos.include?(self.service_provider)
    # Sprint demo hack
    #maos.reject!{|mao| mao.is_system_owner}
    return maos.flatten.uniq
    #operator_network ? [operator_network.service_provider] : nil
  end

  def member_handle_prefix owner
      owner.name + "'s " 
  end

  #---------------------------------
  # End Instance Element Code
  #---------------------------------

  def owner_info_path
    return operator_network ? operator_network.service_provider.name : nil
  end
  
  def cascade_id
    handle = member_handle
    case handle
    when nil, "ERROR!", "ERROR"
      nil
    else
      secondary_data = ["-P","-p","-B","-b"]
      ext = handle[-2..-1]
      secondary_data.include?(ext) ? handle[0...-2] : handle
    end
  end
  
  def member_handle
    handle = MemberAttr.first(:conditions => ["affected_entity_id is NULL and affected_entity_type = ?", self.class.table_name.classify])
    if handle.nil?
      SW_ERR("No member handle for class #{self.class.table_name.classify}!")
      return "ERROR!"
    end
    if operator_network && member_handle_instance_exists(handle.name, operator_network.service_provider)
      mh = member_handle_instance handle.name, operator_network.service_provider
      return mh.value
    end
    SW_ERR("No member handle value for class #{self.class.table_name.classify}!")
    return "ERROR"
  end
  
  def member_handle=(name)
    handle = MemberAttr.first(:conditions => ["affected_entity_id is NULL and affected_entity_type = ?", self.class.table_name.classify])
    if handle.nil?
      SW_ERR("No member handle for class #{self.class.table_name.classify}!")
      return "ERROR!"
    end
    if operator_network && (member_handle_instance_exists handle.name, operator_network.service_provider)
      return member_handle_instance(handle.name, operator_network.service_provider).value = name
    else
      nil
    end
  end

  #Paths don't have any dynamic content in the Cenx Name
  def cenx_name
    return cenx_name_prefix
  end

  def general_before_save
    self.cenx_name_prefix = build_cenx_name_prefix
  end

  def build_cenx_name_prefix
    short_sp_name = CenxNameTools::CenxNameHelper.shorten_name owner_info_path
    onovc_names = ""

#    if on_net_ovcs.empty?
#      onovc_names += "*No-Cx-OVCs"
#    end
#
#    on_net_ovcs.each do |segment|
#      onovc_names += "*#{segment.cenx_name}"
#    end
#
#    if off_net_ovcs.empty?
#      onovc_names += "*No-Memb-OVCs"
#    end
#
#    off_net_ovcs.each do |segment|
#      onovc_names += " *#{segment.short_cenx_name}"
#    end

    type = CenxNameTools::CenxNameHelper.generate_type_string self
    #return "#{type}:#{short_sp_name}:#{member_handle}:#{member_id}#{onovc_names}" TODO use
    return "#{type}:#{short_sp_name}:#{member_handle}#{onovc_names}"
  end
  
  def destroy_orphaned_segments
    demarcs_to_destroy = self.demarcs
    segments.each do |segment|
      if segment.paths.size == 1 # after destroy there will be 0 paths
        segment.destroy
      end
    end

    demarcs_to_destroy.each do |demarc|
      if demarc.paths.size == 0 
        demarc.destroy
      end
    end
  end

  def service_provider
    operator_network.service_provider
  end
  
  def get_candidate_path_types
    operator_network ? operator_network.operator_network_type.path_types : []
  end
  
  def get_candidate_reachable_segments
    operator_network ? operator_network.reachable_segments - segments : []
  end

  def segment_count
    return segments.size
  end

  def on_net_ovcs
    segments.reject{|o| !o.is_a?(OnNetOvc)}
  end

  def on_net_routers
    segments.reject{|o| !o.is_a?(OnNetRouter)}
  end

  def off_net_ovcs
    segments.reject{|o| !o.is_a?(OffNetOvc)}
  end
  
  # AAV for Path
  # TODO: How to handle more than one AAV per path?
  def aav
    path_owner = service_provider
    aav = off_net_ovcs.reject{ |s| s.service_provider_id == path_owner.id}.collect{|segment| segment.service_provider}.first
    aav.blank? ? path_owner.name : aav.name
  end

  # List of site names
  def site_names
    on_net_ovcs.collect{|segment| segment.site.name}.list
  end

  def get_related_instance_elements
    return segments + demarcs + ennis
  end

  def demarcs
    segments.collect{|o| o.demarcs }.flatten.uniq.reject {|d| d == nil}
  end

  def ennis
    segments.collect{|o| o.ennis }.flatten.uniq.reject {|e| e == nil}
  end

  #Active Record Callbacks
  after_create :general_after_create
  def general_after_create
    # Create Service Monitoring history
    update_attributes(:sm_state => AlarmSeverity::NOT_LIVE.to_s, :sm_details => "", :sm_timestamp => (Time.now.to_f*1000).to_i)
    sm_histories << SmHistory.create(:event_filter => "SM #{self.class.name}:#{cenx_id}")
    # create the Provisioning state
    # NOTE: :: tells ruby/rails to look in the global scope for the constant instead of local, this solves a weird bug with how we did the load paths for the state classes
    statefuls << ::PathProvStateful.create
    state = prov_state.get_state
    update_attributes(:prov_name => state.name , :prov_timestamp => state.timestamp, :prov_notes => state.notes)
    event_record_create
  end
  
  def assoc_service_providers_for_indexing_entities
    assoc_service_providers_for_indexing_paths
  end
  
  def assoc_service_providers_for_indexing_paths
        
    # Add new service provider associations
    current_list_of_service_provider_ids = sin_path_service_provider_sphinxes.map(&:service_provider_id)
    updated_list_of_service_provider_ids = []
    updated_list_of_service_provider_ids << service_provider.id if service_provider

    segments.each do |segment|
      updated_list_of_service_provider_ids << segment.service_provider.id if segment.service_provider
    end

    # CORESITE HACK:
    if App.is?(:coresite)
      updated_list_of_service_provider_ids += segments.map { |segment| 
        order = segment.get_latest_order
        [order.buying_operator_network.try(:service_provider_id), order.target_operator_network.try(:service_provider_id)].compact
      }.flatten
    end

    # only unique service providers are needed
    updated_list_of_service_provider_ids.uniq!
    
    # If service provider is not a new service provider then delete
    delete_list_of_service_provider_ids = current_list_of_service_provider_ids - updated_list_of_service_provider_ids

    # NOTE: Have todo this manually as we don't want to fire save on this object again (infinite loop) and collection methods (besides <<) do not save the association records
    updated_list_of_service_provider_ids.each do |sp_id|
      SinPathServiceProviderSphinx.find_or_create_by_path_id_and_service_provider_id(id, sp_id)
    end

    delete_list_of_service_provider_ids.each do |sp_id|
      SinPathServiceProviderSphinx.delete_all({:path_id => id, :service_provider_id => sp_id})
    end

    # Reindex
    ReindexSphinx.schedule
    
  end
  
  def go_in_service    
    # delete & create the Maintenance history    
    sm_histories << SmHistory.create(:event_filter => "SM #{self.class.name}:#{cenx_id}") if sm_histories.empty?
    info = ""
    if mtc_periods.empty?
      self.mtc_periods = [MtcHistory.create(:event_filter => "Mtc Path:#{cenx_id}")]
      self.set_mtc(false)
    end
    return {:result => true, :info => info}
  end
  
  def mtc_history  
    state = super
    segments.each {|segment| state = state + segment.mtc_history}
    return state
  end
   
  def alarm_history    
    mon_segments = segments.collect {|segment| segment if segment.monitored?}.compact
    if cenx_path_type == "Tunnel"
      # This path is a tunnel
      state = super
      segments.each {|segment| state = segment.alarm_history + state}
    else
      # Must be a quicker way..Find if any Segment is in a tunnel but an Segment can be in many Path's
      tunnel_segments = mon_segments.collect {|segment| segment if segment.paths.any? {|path| path.cenx_path_type == "Tunnel"}}.compact
      if tunnel_segments.empty?
        # This Path has none of it's Segments through a tunnel
        state = super
        mon_segments.each {|segment| state = segment.alarm_history + state}
      else
        # if any of the tunnel Segments are in an alarmed state then the state should be DEPENDENCY
        # but not those which are Not Live, Not Monitored or in Maintenance
        tunnel_segment_history = nil
        tunnel_segments.each {|segment| tunnel_segment_history = segment.alarm_history + tunnel_segment_history}
        segment_histories = (mon_segments - tunnel_segments).collect {|segment| segment.alarm_history}
        state = AlarmHistory.merge([tunnel_segment_history, segment_histories].flatten) {|events| calculate_alarm_state(events)}          
      end
    end
    
    return state
  end
  
  def alarm_state        
    # If the Path is a tunnel and it fails then mark all Paths that use the tunnel as DEPENDENCY
    mon_segments = segments.collect {|segment| segment if segment.monitored?}.compact
    if cenx_path_type == "Tunnel"
      # This path is a tunnel
      state = super
      ers = mon_segments.collect {|segment| segment.event_record_id}        
      segment_states = EventRecord.find(:all, :conditions => ["id In (?)", ers])
      state = AlarmRecord.highest(segment_states << state)
    else      
      # Find if any Segment is in a tunnel but an Segment can be in many Path's
      # tunnel_segments = mon_segments.collect {|segment| segment if segment.paths.any? {|path| path.cenx_path_type == "Tunnel"}}.compact
      tunnel_segments = Segment.joins("INNER JOIN `paths_segments` ON `segments`.id = `paths_segments`.segment_id INNER JOIN `paths` ON `paths`.id = `paths_segments`.path_id").
                           where("`segments`.id IN (?) AND cenx_path_type='Tunnel'", segments)      
      if tunnel_segments.empty?
        # This Path has none of it's Segments through a tunnel
        state = super
        ers = mon_segments.collect {|segment| segment.event_record_id}        
        segment_states = EventRecord.find(:all, :conditions => ["id In (?)", ers])
        state = AlarmRecord.highest(segment_states << state)             
      else
        # if any of the tunnel Segments are in an alarmed state then the state should be DEPENDENCY
        # but not those which are Not Live, Not Monitored or in Maintenance
        tunnel_segment_state = nil
        ers = (tunnel_segments).collect {|segment| segment.event_record_id}        
        segment_states = EventRecord.find(:all, :conditions => ["id In (?)", ers])           
        tunnel_segment_state = AlarmRecord.highest(segment_states)             
        ers = (mon_segments-tunnel_segments).collect {|segment| segment.event_record_id}        
        segment_states = EventRecord.find(:all, :conditions => ["id In (?)", ers])        
        state = calculate_alarm_state([tunnel_segment_state, segment_states].flatten)        
      end
    end    
    return state
  end
  
  def monitored?
    # if any of the Segments are monitored the Path is monitored
    segments.any? {|segment| segment.monitored?}
  end 
    
  def derived_admin_status
    return sm_state, sm_details, sm_timestamp/1000
  end

  def reachable_operator_networks
    rons = []
    rons << operator_network
    segments.each do |segment|
      rons << segment.get_operator_network
    end

    return rons.flatten.uniq
  end
  
  def get_latest_order
    service_orders.last
  end
  
  def active_order?
    order = get_latest_order
    if order
      current_state = order.get_order_state
      ![OrderDelivered, OrderRejected, OrderCancelled].any? {|order_state| current_state.is_a?(order_state)}
    end
  end

  def paths
    Path.joins(:segments => :paths).
        where('paths.id = ?', self.id).
        group('paths.id')
  end

  def self.find_paths(path_owner, user, service_provider_ids=nil, site_ids=nil, circuit_type=nil, ordering_state=nil, provisioning_state=nil, monitoring_state=nil, statement_of_work_id=nil, operator_network_type_ids=nil, fallout_exception_type=nil, fallout_categories=nil, path_ids=nil, site_group_ids=nil, operator_network_id=nil)
      
    user_service_provider_id = user.service_provider.id
    operator_network_types_ids = user.operator_network_types.map(&:id)
    
    conditions_args = {:operator_network_types_ids => operator_network_types_ids,
                       :site_ids => site_ids, 
                       :site_group_ids => site_group_ids,
                       :service_provider_ids => service_provider_ids, 
                       :user_service_provider_id => user_service_provider_id, 
                       :circuit_type => circuit_type,
                       :ordering_state => ordering_state,
                       :provisioning_state => provisioning_state,
                       :monitoring_state => monitoring_state,
                       :statement_of_work_id => statement_of_work_id,
                       :operator_network_type_ids => operator_network_type_ids, 
                       :fallout_exception_type => fallout_exception_type,
                       :fallout_categories => fallout_categories,
                       :operator_network_id => operator_network_id, 
                       :path_ids => path_ids}

    # Search for Paths
    finder = Path.joins(:sin_path_service_provider_sphinxes, :operator_network).where(:sin_path_service_provider_sphinxes => {:service_provider_id => user_service_provider_id})

    # CORESITE HACK: I think the scoping by operator_network_type_ids is for another client, not coresite
    unless App.is?(:coresite)
      # Scope by users operator_network_types
      unless operator_network_types_ids.empty?
        finder = finder.where('operator_networks.operator_network_type_id IN (:operator_network_types_ids)', conditions_args)
      end
    end
    case path_owner
      # Bought By
      when 'user'
        finder = finder.where('operator_networks.service_provider_id = :user_service_provider_id', conditions_args)
      # Sold by
      when 'other'
        finder = finder.where('operator_networks.service_provider_id <> :user_service_provider_id', conditions_args)
    end

    unless fallout_exception_type.blank?
      finder = finder.where('paths.fallout_exception_type = :fallout_exception_type', conditions_args) 
    end

    unless fallout_categories.blank?
      finder = finder.joins(:fallout_exceptions).where('fallout_exceptions.category IN (:fallout_categories)', conditions_args).merge(FalloutException.current)
    end

    unless conditions_args[:service_provider_ids].blank?
      # TODO: Test joins vs subselects on large dataset, subselects seem to be much faster because mysql wants to table scan paths first then join, need to limi
      if App.is?(:coresite)
        # For coresite service_provider doesn't have to own path
        finder = finder.
                 joins("INNER JOIN path_displays as coresite_path_displays ON coresite_path_displays.path_id = paths.id").
                 where("coresite_path_displays.service_provider_id IN (:service_provider_ids)", conditions_args)
                 
      else
	  	finder = finder.path_display_by_provider('owner_service_provider_search', conditions_args[:site_ids].present?).where("ed_owner_service_provider_search.owner_service_provider_id IN (:service_provider_ids)", conditions_args)
      end
      # finder = finder.where("EXISTS (SELECT * FROM paths_segments AS member_paths_ovc_join
      #                       JOIN segments AS off_net_ovc ON (member_paths_ovc_join.segment_id = off_net_ovc.id AND off_net_ovc.type = 'OffNetOvc')
      #                       JOIN operator_networks AS member_network ON (off_net_ovc.operator_network_id = member_network.id AND member_network.service_provider_id = :service_provider_id)
      #                       WHERE member_paths_ovc_join.path_id = paths.id)", conditions_args)
      
    end

    unless conditions_args[:site_ids].blank?
      finder = finder.path_display_by_provider('site_search', conditions_args[:service_provider_ids].present?).where("ed_site_search.site_id IN (:site_ids)", conditions_args)
      # finder = finder.where("EXISTS (SELECT * FROM paths_segments AS member_paths_ovc_join
      #                       JOIN segments AS off_net_ovc ON (member_paths_ovc_join.segment_id = off_net_ovc.id AND off_net_ovc.type = 'OnNetOvc' AND off_net_ovc.site_id = :site_id)
      #                       WHERE member_paths_ovc_join.path_id = paths.id)", conditions_args)
    end

    unless conditions_args[:site_group_ids].blank?
      finder = finder.path_display_by_provider('site_search', conditions_args[:service_provider_ids].present?).joins('INNER JOIN site_groups_sites ON ed_site_search.site_id = site_groups_sites.site_id').where("site_groups_sites.site_group_id IN (:site_group_ids)", conditions_args)
    end

    unless conditions_args[:statement_of_work_id].blank?
      finder = finder.joins('INNER JOIN service_orders AS circuit_orders ON circuit_orders.ordered_entity_id = paths.id 
                             INNER JOIN bulk_orders_service_orders AS circuit_order_join ON circuit_orders.id = circuit_order_join.service_order_id
                             INNER JOIN service_orders ON circuit_order_join.bulk_order_id = service_orders.id').
                      where('circuit_orders.bulk_order_type = ?', "Circuit Order").
                      where('service_orders.bulk_order_type = ?', "Statement of Work").
                      where('service_orders.id = :statement_of_work_id', conditions_args)                                          
    end
    
    unless conditions_args[:operator_network_type_ids].blank?
      finder = finder = finder.joins(:segments).
                               joins('INNER JOIN operator_networks AS segments_operator_networks ON segments.operator_network_id = segments_operator_networks.id').
                               joins('INNER JOIN operator_network_types ON segments_operator_networks.operator_network_type_id = operator_network_types.id').
                               where('segments_operator_networks.operator_network_type_id IN (:operator_network_type_ids)', conditions_args)
    end

    unless conditions_args[:operator_network_id].blank?
      if App.is?(:coresite)
        # CORESITE HACK:  For CoreSite we need to use the order to determine operator networks, Path operator networks belong to CoreSite
        finder = finder.joins({:segments => :service_orders}).
                        joins("LEFT OUTER JOIN operator_networks as target_operator_networks ON service_orders.notes LIKE CONCAT('%', target_operator_networks.name, '%')").
                        where('service_orders.operator_network_id = :operator_network_id OR target_operator_networks.id = :operator_network_id', conditions_args)
      else
        finder = finder.joins(:segments).where('segments.operator_network_id = :operator_network_id', conditions_args)
      end
    end
    
    unless conditions_args[:circuit_type].blank?
      finder = finder.where('paths.cenx_path_type = :circuit_type', conditions_args)
    end

    unless conditions_args[:ordering_state].blank?
      finder = finder.where('paths.order_name IN (:ordering_state)', conditions_args)
    end
    
    unless conditions_args[:provisioning_state].blank?
      # Special case for fallout state on inventory
      where_conditions = []
      if [*conditions_args[:provisioning_state]].any?{ |s| s.casecmp('Fallout').zero? }
        where_conditions << 'paths.fallout_severity != "ok"'
      end
      where_conditions << 'paths.prov_name IN (:provisioning_state)'

      finder = finder.where(where_conditions.join(' OR '), conditions_args)
    end
    
    unless conditions_args[:monitoring_state].blank?
      finder = finder.where('paths.sm_state IN (:monitoring_state)', conditions_args)          
    end

    unless conditions_args[:path_ids].blank?
      finder = finder.where('paths.id IN (:path_ids)', conditions_args)          
    end

    finder
    
  end

  protected
  
  # Calculate if the Path is Dependency or not
  # If the Tunnel Segment state is Warning, Failed or Unavailable then the Paths that include the tunnel are Dependency
  # If the Tunnel state is OK then the Path state is the highest of all the Segment states
  def calculate_alarm_state(events)  
    events.compact!    
    if events.size < 2
      return events.first.dup
    end
    tunnel_state = events[0]
    segments = events[1..-1]
    
    case tunnel_state.state.to_s
    when AlarmSeverity::WARNING, AlarmSeverity::FAILED, AlarmSeverity::UNAVAILABLE
      state = tunnel_state.dup
      state.state = AlarmSeverity::DEPENDENCY
    else
      state = nil
      events.each {|segment| state = segment + state}
    end
    return state 
  end

  public  
  has_and_belongs_to_many :buyer_segments, 
                          :class_name => 'OffNetOvc', 
                          :conditions => {:segment_owner_role => 'Buyer'},
                          :join_table => :paths_segments,
                          :foreign_key => 'path_id',
                          :association_foreign_key => 'segment_id'

  has_and_belongs_to_many :on_net_ovcs, 
                          :class_name => 'OnNetOvc',
                          :join_table => :paths_segments,
                          :foreign_key => 'path_id',
                          :association_foreign_key => 'segment_id'

  has_and_belongs_to_many :seller_segments, 
                          :class_name => 'OffNetOvc', 
                          :conditions => {:segment_owner_role => 'Seller'}, 
                          :join_table => :paths_segments,
                          :foreign_key => 'path_id',
                          :association_foreign_key => 'segment_id'


  # Get unique components (ie On Net OVC can be referenced more than once for multi-endpoint)
  # Used when editing member handles
  def unique_diagram_components(service_provider)
    components = {}
    unique_components = path_displays.by_service_provider(service_provider).not_redacted.reject do |component| 
      found = false
      if components.has_key?(component.unique_entity_id)
        found = true
      end
      components[component.unique_entity_id] = true
      found
    end
  end

 
  # Get rows and columns in diagram
  # row = 0 to get all rows
  def diagram_parts(service_provider, reverse_diagram, row=0)

    # Get one or all rows
    rows_and_columns = if row > 0 then
      path_displays.by_service_provider(service_provider).by_row(row) 
    else
      path_displays.by_service_provider(service_provider).includes(:entity)
    end

    # Exit early if no diagram to display
    return [] if rows_and_columns.empty?

    # Find last column to be display
    column_letters = rows_and_columns.map{|r| r[:column]}
    # Column range (ie A..F or B..E)
    col_range = (column_letters.min..column_letters.max).to_a.join('')

    rows_array = rows_and_columns.group_by{|col| col[:row]}.values
    
    # If reverse then reverse diagram
    if reverse_diagram
      col_range_reverse = col_range.reverse
      rows_array.each do |row|
        row.reverse!.each do |col|
          col[:column].tr!(col_range, col_range_reverse) 
          col[:relative_position] = col[:relative_position] == 'Left' ? 'Right' : 'Left' if col[:relative_position]
        end
      end
    end

    # Mark proper segments as multi endpoint, injection points, reflection points, monitoring indicator and slice pointer positions
    multi_end_points = {}
    rows_array.each do |row|
      injection_found = false
      reflection_found = false
      row.each_with_prev_next do |previous_column, column, next_column|
        
        if column.segment?
          # if same segment is referenced more than once than it is a multi endpoint
          if multi_end_points.has_key?(column.unique_entity_id)
            column.multi_end_point = true
          end
          multi_end_points[column.unique_entity_id] = true
          
          # look for injection point
          entity = column.entity
          if entity.respond_to?(:has_monitoring_endpoint) && entity.has_monitoring_endpoint
            column.injections = injection_direction(entity.get_cenx_monitoring_segment_endpoints, reflection_found)
            injection_found = true
          elsif layer == 3 
            if entity.monitored?
              column.reflection_direction = injection_found ? :right : :left
              reflection_found = true
            end
          end
        
        # COX HACK
        elsif column.path.cenx_path_type =~ /^Cox/ && column.demarc? && next_column.nil?
          # look for injection point
          column.injections = injection_direction([true], reflection_found)
          injection_found = true

        # COX HACK
        elsif column.path.cenx_path_type == 'Cox_Buy' && column.enni? && !next_column.try(:entity).try(:is_monitored?)
          # look for injection point
          column.injections = injection_direction([true], reflection_found)
          injection_found = true
        
        # look for reflection point  
        elsif column.demarc?
          # We are looking ahead so make sure column is actually an nonredacted entity instead of set of redacted elements (array)
          if layer == 2
            column.reflection_direction = if previous_column.try(:entity).try(:is_monitored?) && injection_found
              :right
            elsif next_column.try(:entity).try(:is_monitored?)
              :left
            end
            reflection_found = true if column.reflection_direction
          end
        # look for monitoring point
        elsif column.end_point?
          entity = column.entity
          cos_end_point = entity.cos_end_points.first
          if cos_end_point
            primary_backup = cos_end_point.cos_test_vectors.map{|ctv| ctv.primary ? 'P' : (ctv.backup ? 'B' : '') }.join('/')
            column.monitoring = primary_backup if primary_backup.length > 0
          end
        end
        
        if column.segment?
          # OVC pointers are always centered
          column.slice_pointer = 'pointer_in_center'
          
          # Box around components for grouping sites and nodes
          column.component_group_direction = if column.column == 'B'
            'component_group_left' 
          else
            'component_group_right' 
          end

        elsif column.demarc?
          column.slice_pointer = if previous_column.nil? 
            'pointer_on_left' 
          elsif next_column.nil?
            'pointer_on_right' 
          end
          
        elsif column.end_point?
          column.slice_pointer = case column.relative_position.downcase
          when 'left' then
            'pointer_on_left'
          when 'right' then
            'pointer_on_right'
          end
        end   
        
      end
    end

    # Now fill in the gaps
    filled_results = rows_array.map do |row|
      rows = []
    
      # Column map for fast look to determine if column exists
      col_map = row.each_with_index.inject({}) do |map, (c, i)| 
        map.merge Hash["#{c.column}#{c.relative_position}", i] 
      end
      
      # loop through each possible column and fill in missing objects
      col_range.each_char do |col|
        # col.ord works for ruby 1.9.2, col[0] works for ruby 1.8.7
        if col.ord % 2 == 0
          rows << (col_map[col + 'Left'].nil? ? PathDisplay.new({:column => col, :relative_position => 'Left'}) : row[col_map[col + 'Left']]) 
          rows << (col_map[col].nil? ? PathDisplay.new({:column => col}) : row[col_map[col]]) 
          rows << (col_map[col + 'Right'].nil? ? PathDisplay.new({:column => col, :relative_position => 'Right'}) : row[col_map[col + 'Right']]) 
        else
          rows << (col_map[col].nil? ? PathDisplay.new({:column => col}) : row[col_map[col]]) 
        end
      end
      
      rows
    end
    
    # Now group redacted components into single column
    final_results = filled_results.map do |row|
      col_index = -1
      in_redacted_components = false
      row.inject([]) do |new_row, col|
        unless col.redacted?
          col_index += 1
          new_row[col_index] = col
          in_redacted_components = false
        else
          unless in_redacted_components
            col_index += 1
            new_row[col_index] = RedactedPathDisplay.new
            in_redacted_components = true
          end
          new_row[col_index] << col
        end
        new_row
      end
    end

  end
 
  # Layout an Path for the diagram display
  def layout_diagram()

    @columns = ('A'..'Z').to_a
    @layout = []
    @owner = service_provider
    @cenx = ServiceProvider.find(:first, :conditions => {:is_system_owner => 1})

    # get service providers who own components (segments)
    # make sure owner of path and cenx get a view as well
    # service_providers = (segments.map{|o| o.service_provider} << @owner << @cenx)
    service_providers = (sin_path_service_provider_sphinxes.map(&:service_provider) << @cenx).compact.uniq

    # Each service provider has their own view of the diagram
    service_providers.each do |sp| 
      get_layout_for_service_provider(sp)
    end

    # Clear existing Path data
    PathDisplay.delete_all(["path_id = ?", id])

    # once we have layout create the path display entries
    # @layout.each do |entry|
    #   # Recreate or create path_display entry
    #   PathDisplay.create(entry)
    # end
    
    # Bulk insert new diagrams
    PathDisplay.import @layout

    # remove from memory so when running in a loop we don't take up too much memory
    @layout = nil

    true
    
  end
  
  private

  # Layout an Path for the diagram display
  def get_layout_for_service_provider(service_provider)

    @service_provider = service_provider
    @processed = {}

    # Start laying out diagram, starting with column A
    row, col = 0, 0
    starting_components.each do |component|
      row = row.next
      add row, col.next, component # start at B (endpoint)
    end

  end
  
  # Get starting components for Diagram (start of Left with Column A)
  def starting_components
    
    # Optimal starting point
    starting_component = case cenx_path_type
    when 'Standard'
      # Start with Buyer Segment
      sc = buyer_segments.size ? buyer_segments.map{|o| o.ovc_end_point_unis.first}.take(1) : nil
      # If no Buyer segments then just pick one of the sellers (i.e. seller<->cenx<->seller)
      sc.compact.empty? ? seller_segments.map{|o| o.ovc_end_point_unis.first}.take(1) : sc
    when 'LightSquared Single Cell', 'LightSquared Multi Cell'
      # Start with Tunnels (left).
      off_net_ovcs.select{|o| o.is_in_tunnel_path}.map{|o| o.ovc_end_point_unis.first}
    when 'Light Squared Core Transport', 'Sprint Core Transport'
      # Start with On Net OVC (there are two), Pick ENNI with only 1 endpoint
      on_net_ovcs.map{|o| o.on_net_ovc_end_point_ennis.reject{|ep| ep.is_connected_to_another_endpoint? || ep.is_connected_to_test_port}.first}.take(1)
    when 'Sprint Samsung AT&T', 'Sprint ALU VzT Tagged', 'Sprint ALU LAG Protected', 'Sprint ALU LAG Protected with MW', 'Sprint Samsung AT&T Pt-Pt', "Sprint Fiber BackHaul Site", "Sprint Donor Site", "Sprint Intermediate Site", "Sprint Remote Site"
      on_net_ovcs.map{|o| o.on_net_ovc_end_point_ennis.reject{|ep| ep.is_connected_to_another_endpoint? || ep.is_connected_to_test_port}.first}
    # COX HACK
    when 'Cox_Internal', 'Cox_Buy'
      # Start with On Net OVCs
      sc = on_net_ovcs.map{|o| o.on_net_ovc_end_point_ennis.reject{|ep| ep.is_connected_to_another_endpoint? || ep.is_connected_to_test_port}.first}
      # if none then start at buyer
      sc.compact.empty? ? seller_segments.map{|o| o.ovc_end_point_unis.first}.take(1) : sc
    when 'Cox_Sell'
      off_net_ovcs.first.ovc_end_point_ennis.take(1)
    when /CoreSite/ != cenx_path_type
      # For coresite start on buyer side
      if order = get_latest_order
        if buying_enni = order.buying_enni
          buying_enni.segment_end_points.reject{|ep| ep.is_connected_to_test_port}.take(1)
        end
      end
    when 'VzW Cell Site'
      # Verizon cell site start with the router
      enni = ennis.select{|e| e.demarc_icon == 'router'}.first
      if enni
        on_net_ovcs.map{|o| o.on_net_ovc_end_point_ennis.select{|ep| ep.demarc == enni }.first}.take(1)
        #enni.segment_end_points.take(1) 
      else
        []
      end
    # Layer 3 IP Flow starts at segment
    when 'Generic IP Flow'
      on_net_routers.take(1)
    else
      []
    end

    # if path is not complete or not a special type then try to find any Uni
    if starting_component.compact.empty?
      starting_component = off_net_ovcs.map{|o| o.ovc_end_point_unis.reject{|ep| ep.is_connected_to_test_port}.first}.take(1)
    end
    
    # If no Unis then try starting at a ENNI
    if starting_component.compact.empty?
      starting_component = on_net_ovcs.map{|o| o.on_net_ovc_end_point_ennis.reject{|ep| ep.is_connected_to_test_port}.first}.take(1)
    end

    # If no ENNIs attached to a On Net OVC then try looking for a Off Net OVC
    if starting_component.compact.empty?
      starting_component = off_net_ovcs.map{|o| o.ovc_end_point_ennis.reject{|ep| ep.is_connected_to_test_port}.first}.take(1)
    end

    starting_component
        
  end

  # Add a component to the layout and get next component to process (add to layout)
  def add(row, col, component, previous_component=nil)

    # No more components to process
    return if component.nil?
   
    case component.class.name
   
    when 'Uni', 'EnniNew', 'Demarc'
      layout_entry(row, col, component)

    when 'OvcEndPointUni'
      # Starting point at (Endpoint so look back and add demarc as well)
      layout_entry(row, col.pred, component.demarc) unless previous_component

      if previous_component.class.name == 'OffNetOvc'
        layout_entry(row, col, component, 'Right')
        add row, col.next, component.demarc, component
        # Find connected endpoints for this path
        add row, col.next.next, component.segment_end_points.by_path(self).reject{|ep| ep == component || ep.is_connected_to_test_port}.first, component.demarc
      else
        layout_entry(row, col, component, 'Left')
        add row, col, component.segment, component
      end

    when 'OvcEndPointEnni',  'OnNetOvcEndPointEnni', 'OnNetRouterSubIf'
      
      # Starting point at (Endpoint so look back and add demarc as well)
      layout_entry(row, col.pred, component.demarc) unless previous_component

      if ['OffNetOvc', 'OnNetOvc', 'OnNetRouter'].include?(previous_component.class.name)
        layout_entry(row, col, component, 'Right')
        # Add Enni now so it is easier to track connected points
        add row, col.next, component.enni, component
        # Find connected endpoints for this path
        add row, col.next.next, component.segment_end_points.by_path(self).reject{|ep| ep == component || ep.is_connected_to_test_port}.first, component.enni
      else
        layout_entry(row, col, component, 'Left')
        add row, col, component.segment, component
      end


    when 'OffNetOvc'
      # TODO: Sprint Hack
      sibling_end_points = case cenx_path_type
      when 'Sprint Samsung AT&T', 'Sprint ALU VzT Tagged', 'Sprint ALU LAG Protected', 'Sprint ALU LAG Protected with MW', 'Sprint Samsung AT&T Pt-Pt'
        # PROGRAMMERS NOTE: don't assume endpoints have demarcs
        # Off Net OVCs are not bookended by an enni and uni, so use this to find sibling endpoint
        component.segment_end_points.reject{|ep| already_processed?(ep) || (previous_component.try(:demarc).is_a?(EnniNew) && ep.try(:demarc).is_a?(EnniNew)) || ep.is_connected_to_test_port}
      when "Sprint Fiber BackHaul Site", "Sprint Donor Site", "Sprint Intermediate Site", "Sprint Remote Site"
        # HACK
        # ENNIs do not flow to other ENNIs
        sep = component.segment_end_points.reject{|ep| already_processed?(ep) || (previous_component.try(:demarc).is_a?(EnniNew) && ep.try(:demarc).is_a?(EnniNew)) || ep.is_connected_to_test_port}
        # if previous reject did not narrow down enough then make sure they are not on the same network
        sep = sep.reject{|ep| ep.try(:demarc).try(:operator_network_id) == previous_component.try(:demarc).try(:operator_network_id)} if sep.size > 1
        sep
      else
        component.segment_end_points.reject{|ep| ep == previous_component || ep.is_connected_to_test_port}
      end

      # if no sibling endpoint (on other side of Segment) then just add Segment to diagram
      if sibling_end_points.empty?
        layout_entry(row, col, component)
      # if there are sibling endpoints then add segment for each and continue on adding more components
      else
        sibling_end_points.each do |ep|
          layout_entry(row, col, component)
          add row, col, ep, component
          row = row.next
        end
      end

    when 'OnNetOvc'

      sibling_end_points = component.segment_end_points.reject{|ep| already_processed?(ep) || ep.is_connected_to_test_port}
      unless sibling_end_points.one?
        sibling_end_points = component.segment_end_points.reject{|ep| already_processed?(ep) || ep.try(:demarc).try(:operator_network_id) == previous_component.try(:demarc).try(:operator_network_id) || ep.is_connected_to_test_port}
      end

      # if no sibling endpoint (on other side of Segment) then just add Segment to diagram
      if sibling_end_points.empty?
        layout_entry(row, col, component)
      # if there are sibling endpoints then add segment for each and continue on adding more components
      else
        sibling_end_points.each do |ep|
          layout_entry(row, col, component)
          add row, col, ep, component
          row = row.next
        end
      end

    when 'OnNetRouter'
      
      # Sprint links to Sprint so don't use the operator network to find siblings
      sibling_end_points = component.segment_end_points.reject{|ep| already_processed?(ep) || ep.is_connected_to_test_port}

      # if no sibling endpoint (on other side of Segment) then just add Segment to diagram
      if sibling_end_points.empty?
        layout_entry(row, col, component)
      # if there are sibling endpoints then add segment for each and continue on adding more components
      else
        sibling_end_points.each do |ep|
          layout_entry(row, col, component)
          add row, col, ep, component
          row = row.next
        end
      end
      
    end
    
    
    
    
  end

  # Need to track processed entities so we don't process them twice
  def already_processed?(entity)
    @processed[entity.cenx_id]
  end

  # Entry for Path Display table
  def layout_entry(row, col, component, position=nil)
    unless component.nil?
      
      # Determine if component should be mapped on Network View
      site = if component.class.name == 'EnniNew'
        component.site
      elsif ['Uni', 'Demarc'].include?(component.class.name)
        component
      else
        nil
      end

      site_id = component.class.name == 'EnniNew' ? component.site_id : nil
      owner_service_provider_id = component.is_a?(Segment) ? component.service_provider.try(:id) : nil
      latitude, longitude = site && site.latitude != 0.000000 ? [site.latitude, site.longitude] : [nil, nil]
      site_type = site && site.is_a?(Demarc) ? site.site_type : nil
      demarc_icon = component.respond_to?(:demarc_icon) ? component.demarc_icon : nil
      
      @processed[component.cenx_id] = 1 
      @layout << PathDisplay.new(
        :path_id => id,
        :entity => component,
        :row => row,
        :column => @columns[col],
        :relative_position => position,
        :service_provider_id => @service_provider.id,
        :latitude => latitude,
        :longitude => longitude,
        :site_type => site_type,
        :demarc_icon => demarc_icon,
        :site_id => site_id, # TODO BJW Should be :site_id - not sure Need to talks to Shane
        :owner_service_provider_id => owner_service_provider_id,
        :component_type => component.class.name,
        :redacted => redacted?(component)
      )
    end
  end
  
  def redacted?(component)
    # if service provider owns entire path than they can see all the components in the diagram
    # all coresite tenants involved can see entire path
    return false if @service_provider.id == @owner.id || @service_provider.is_system_owner? || App.is?(:coresite)
          
    # if service provider owns this component than they can see this component in the diagram
    view = case component.class.name
      # Full view of Demarc if you own any Segment attached to it
      when 'Uni', 'Demarc', 'EnniNew'
        component.segment_end_points.joins({:segment => :operator_network}).where({:operator_networks => {:service_provider_id => @service_provider.id}}).count > 0
        # WRONG WAY: component.segment_end_points.any?{|ep| ep.segment.service_provider.id == @service_provider.id}
      # Full view of End Point if you own the Segment it is attached to
      when 'OvcEndPointUni', 'OvcEndPointEnni'
        component.segment.service_provider.id == @service_provider.id
      # Full view of Segment if you own it
      when 'OffNetOvc'
        component.service_provider.id == @service_provider.id
      # Only Path owners see the On Net OVC
      when 'OnNetOvc', 'OnNetOvcEndPointEnni'
        false
    end
    
    # return the opposite of view
    return view ? false : true
        
  end
  
  # Does injection arrow point left or right?
  def injection_direction(injections, reflection_found)
    injections.map do |injection|
      {:direction => (reflection_found ? :left : :right), :data => injection}
    end 
  end
  
end
