# == Schema Information
# Schema version: 20110802190849
#
# Table name: uploaded_files
#
#  id            :integer(4)      not null, primary key
#  comment       :string(255)
#  name          :string(255)
#  content_type  :string(255)
#  data          :binary(16777215
#  uploader_id   :integer(4)
#  uploader_type :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#  url           :string(255)
#

class UploadedFile < ActiveRecord::Base

belongs_to :uploader, :polymorphic => true
validates_presence_of :comment
validates_uniqueness_of :comment, :scope => [:uploader_id, :uploader_type]
validates_format_of :content_type, :with => /[a-zA-Z]([a-zA-Z.+-]*[a-zA-Z])?\/[a-zA-Z]([a-zA-Z.+-]*[a-zA-Z])?/, :if => Proc.new{|uf| uf.data}
validate :valid_size?, :valid_url?, :url_or_file?

def uploaded_file=(file_field)
  self.name	= base_part_of(file_field.original_filename)
  self.content_type = file_field.content_type.chomp
  if(file_field.size < @@maxfilesize)
    self.data	= file_field.read
  else
    self.data = nil
    @data_too_large = true
  end
  @file = true
end

def url=(url_string)
  #Hack to handle the '|' character in Alfredo URLs
  url_string.gsub!("|", "%7C")
  self[:url] = url_string
  return if url_string.nil? || url_string.empty?
  begin
    uri = URI.parse(url_string)
    if uri.scheme == "https"
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      store = OpenSSL::X509::Store.new
      store.set_default_paths
      http.cert_store = store
      http.verify_mode = OpenSSL::SSL::VERIFY_PEER
      request = Net::HTTP::Get.new(uri.request_uri)
      response = http.request(request)
    elsif uri.scheme == "http"
      response = Net::HTTP.new(uri.host).head(url_string)
    else
      raise URI::InvalidURIError
    end
    raise URI::BadURIError if !response.is_a? Net::HTTPSuccess
    self.name = base_part_of(url_string)
    self.content_type = response.content_type
  rescue URI::InvalidURIError
    @url_invalid = true
  rescue URI::BadURIError
    @url_invalid = true
  end
  @url = true
end

def base_part_of(file_name)
  File.basename(file_name).gsub(/[^\w._-]/,'' )
end

private

@@maxfilesize = 1 * 1024 * 1024

def valid_size?
  if(@data_too_large)
    helper = Object.new.extend(ActionView::Helpers::NumberHelper)
    errors.add(:uploaded_file, "- File is too large. Maximum size is " + helper.number_to_human_size(@@maxfilesize) + ".")
    return false
  end
end

def valid_url?
  if @url_invalid
    self.errors.add(:url)
  else
    return true
  end
end

def url_or_file?
  if @url && @file
    self.errors[:base] << "Both a file and a url were specified, please choose only one."
  elsif !@url && !@file && self.new_record?
    self.errors[:base] << "Must specify either a url or file."
  end
end

end
