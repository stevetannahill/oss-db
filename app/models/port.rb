# == Schema Information
# Schema version: 20110802190849
#
# Table name: ports
#
#  id                    :integer(4)      not null, primary key
#  name                  :string(255)
#  created_at            :datetime
#  updated_at            :datetime
#  node_id               :integer(4)
#  patch_panel_slot_port :string(255)
#  strands               :string(255)
#  color_code            :string(255)
#  notes                 :text
#  patch_panel_id        :integer(4)
#  connected_port_id     :integer(4)
#  mac                   :string(255)
#  phy_type              :string(255)
#  other_phy_type        :string(255)
#  demarc_id             :integer(4)
#


class Port < ActiveRecord::Base
    
  belongs_to :node #structural
  belongs_to :demarc #if linked to an ENNI
  belongs_to :connected_port, :class_name => "Port",  :foreign_key => "connected_port_id"
  has_many :uploaded_files, :as => :uploader

  validates_presence_of :name, :phy_type, :node_id
  validates_uniqueness_of :name, :scope => :node_id, :message => "of port on this network element has already been allocated"
  validates_inclusion_of :phy_type, :in => HwTypes::PHY_TYPES.map{|disp, value| value },
                         :message => "invalid, Use: #{HwTypes::PHY_TYPES.map{|disp, value| value }.join(", ")}"
  
  #validates_format_of :name, :with => /(^(\d{1,2}\/){2}(\d{1,2})$)/, :message => "Should be format a/b/c"
  #validates_format_of :patch_panel_slot_port, :with => /(^$)|(^(\d{1,2}\/)(\d{1,2})$)/, :message => "Should be format a/b"
  validates_format_of :mac,
                      :with => /(^$)|(^(([A-Fa-f0-9]{2}):){5}([A-Fa-f0-9]{2})$)/,
                      :message => "Should be left empty or strict MAC address format xx:xx:xx:xx:xx:xx "
                      
  STANDARD_ORDER = "CASE name RLIKE '^[0-9]*/' WHEN 0 THEN SUBSTRING_INDEX(name, '/', '1') ELSE LPAD(SUBSTRING_INDEX(name, '/', '1'), 255, '0') END, CASE name RLIKE '^[^/]*/[0-9]*(/|$)' WHEN 0 THEN SUBSTRING_INDEX(name, '/', '2') ELSE LPAD(SUBSTRING_INDEX(name, '/', '2'), 255, '0') END, CASE name RLIKE '^[^/]*/[^/]*/[0-9]*(/|$)' WHEN 0 THEN SUBSTRING_INDEX(name, '/', '3') ELSE LPAD(SUBSTRING_INDEX(name, '/', '3'), 255, '0') END, name"

  before_save :general_before_save

  def general_before_save
    self.name = self.name.strip
  end

  def before_destory
    connected_port.connected_port = nil if connected_port
  end
  
  def self.per_page
    50
  end
  
  def card_info
    name.match(/^\d+\/\d+\/\d+/) ? name.split("/")[0] : nil
  end
  
  def subcard_info
    name.match(/^\d+\/\d+\/\d+/) ? name.split("/")[1] : nil
  end

  def site_info
    return node.site.name
  end

  def node_info
    return node.name
  end

  def cabinet_info
    if node.cabinet != nil
     return node.cabinet.name
    end
    return "Unknown"
  end

  def get_candidate_demarcs
    candidate_demarcs = []
    demarcs_to_check = node.site.demarcs
    demarcs_to_check.each do |d|
      if d.new_port_can_be_linked
        candidate_demarcs << d
      end
    end
    return candidate_demarcs

  end

  def get_valid_site_list
    return [node.site]
  end

  def can_be_connected_to_endpoint
    return (!is_connected_to_enni_new)
  end

  def is_port_connected_to_demarc
    return (is_connected_to_enni_new)? true : false
  end

  def is_connected_to_port
    return  connected_port_id != nil
  end

  def is_connected_to_enni #TODO_sr remove
    is_connected_to_enni_new
  end

  def is_connected_to_enni_new
    return  demarc_id != nil
  end

  def is_test_port
    return (is_connected_to_port ? connected_port.node.is_test_node : false )
  end

  def is_trouble_shooting_port
    return (is_connected_to_port ? connected_port.node.is_trouble_shooting_node : false )
  end

  def is_monitoring_port
    return (is_connected_to_port ? connected_port.node.is_monitoring_node : false )
  end

  def is_secondary_monitoring_port
    return is_monitoring_port && name=="3/2/23"
  end

  def remote_node_type
    return (is_connected_to_port ? connected_port.node.node_type : "Unkown" )
  end

  def all_connection_info_port
    if !is_connected_to_enni_new && !is_connected_to_port
      return "Nothing"
    else
      return "#{demarc_connection_info_port} | #{port_connection_info_port}"
    end
  end

  def demarc_connection_info_port
    if is_connected_to_enni_new
      return "#{demarc.cenx_name}"
	  else
      return "No-ENNI"
    end
  end

  def port_connection_info_port
    #TODO_sr fix
    # puts "************* ID = #{id}"
    if is_connected_to_port
      if id == connected_port.id
        return "Loopback"
      end
      return "Port: #{connected_port.name} on #{connected_port.node.name} (#{connected_port.node.node_type})"
	  else
      return "No-Port"
    end
  end

  def get_mac_address
    mac = ""
    clean_name = name.strip #strip any leading/trailing spaces

    api = SamIf::StatsApi.new(get_network_manager)
    result, data = api.get_inventory("equipment.PhysicalPort", {"displayedName" => "Port #{clean_name}", "siteId" => node.loopback_ip}, ["macAddress"])
    if data.size != 1
      SW_ERR "None or more than one object returned for inventory query class equipment.PhysicalPort Port: \'#{clean_name}\' Node: #{node.name}/#{node.loopback_ip} returned data:#{data.inspect}"
    else
      mac = data[0]["macAddress"].gsub(/-/, ":")
    end
    api.close
    return result, mac
  end
  
  def get_network_manager
    return node.network_manager
  end
  
  def stats_displayed_name
    return "Port #{name}"
  end

end
