# == Schema Information
#
# Table name: exclusion_lists
#
#  id         :integer(4)      not null, primary key
#  name       :string(255)
#  user_id    :integer(4)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#
class ExclusionList < ActiveRecord::Base
  belongs_to :user
  has_many :schedules, :dependent => :nullify
  has_many :excluded_items, :dependent => :delete_all
  
  validates_uniqueness_of :name
  validates :name, :user, :presence => true

end

