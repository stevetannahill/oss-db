# == Schema Information
# Schema version: 20110802190849
#
# Table name: member_attrs
#
#  id                   :integer(4)      not null, primary key
#  name                 :string(255)
#  affected_entity_id   :integer(4)
#  affected_entity_type :string(255)
#  type                 :string(255)
#  syntax               :string(255)
#  value_range          :string(255)
#  created_at           :datetime
#  updated_at           :datetime
#  allow_blank          :boolean(1)
#  disabled             :boolean(1)
#  default_value        :string(255)
#

class MemberHandle < MemberAttr
  validates_uniqueness_of :affected_entity_type, :if => Proc.new{|mh| mh.affected_entity_id.nil?}, :message => "already has a member handle."
end
