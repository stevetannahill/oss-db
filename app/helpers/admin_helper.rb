require "cenx_id_generator.rb"

module AdminHelper

 
  def table_from_type(type)
    table = CDBInfo::CLASS_SHORT_NAMES.invert[type.to_s]
    return table ? table.constantize : nil
  end

  def parent_type_from_table(table)
    CDBInfo::CLASS_SHORT_NAMES[table.is_a?(ActiveRecord::Base) ? table.class_name : table.to_s]
  end

  def get_class_lookup_table
    return IdTools::CenxIdGenerator::CLASS_LOOKUP_TABLE
  end

=begin
  def options_name_from_parents parents = []
  end

  def hash_to_uri_options hash = {}, parents = []
    options = ""
    hash.each{|key, value| options += "&" + value.is_a?(Hash) ? hash_to_uri value, parents + [key] :
    (value.is_a?(Array) ? : )}
    return (options.length > 1) options[1..-1] : ""
  end
=end

end
