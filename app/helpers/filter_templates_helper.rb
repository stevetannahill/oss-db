module FilterTemplatesHelper

  def filter_template_url(filter_template, options={})
    case filter_template.report_type
      when 'sla'
        sla_report_url(filter_template.filter_options.merge(options))
      when 'network_type'
        oem_sla_report_url(filter_template.filter_options.merge(options))
      when 'circuit_utilization'
        circuit_utilization_report_url(filter_template.filter_options.merge(options))
      when 'enni_utilization'
        enni_utilization_report_url(filter_template.filter_options.merge(options))
    end
  end
    
end
