module PathsHelper
    
  def display_value(value)
    return value if value.blank?
    value.gsub(" ", "&nbsp;")
  end

  def entity_data_urls(path_display)

    # return entity type, override some types to make things a little cleaner
    entity = path_display.entity

    # convert class name into entity path for routing
    entity_path = convert_to_path(entity)
    
    # return url used to retrieve data via ajax
    urls = {}

    urls['data-ordering_url'] = if path_display.path
      send("ordering_#{entity_path}_path", entity.id, {:path_id => path_display.path.id, :mode => 'ordering'}) 
    # ENNI Display is not in context of path
    else
      send("ordering_#{entity_path}_path", entity.id, {:mode => 'ordering'}) 
    end

    urls['data-inventory_url'] = if path_display.path
      send("#{entity_path}_path", entity.id, {:path_id => path_display.path.id, :mode => 'inventory'}) 
    # ENNI Display is not in context of path
    else
      send("#{entity_path}_path", entity.id, {:mode => 'inventory'}) 
    end

    urls['data-monitoring_url'] = if show_monitoring?(entity)
      send("monitoring_#{entity_path}_path", entity.id, {:mode => 'monitoring'})
    else
      send("sm_state_#{entity_path}_path", entity.id, {:mode => 'monitoring'})
    end
    
    urls

  end

  def show_monitoring?(entity)
    entity_path = convert_to_path(entity)
      
    # Special cases when not to show monitoring bubble
    show_monitoring = case entity_path
      when 'off_net_ovc'
        entity.is_in_ls_core_transport_path
      when 'off_net_ovc_end_point_uni'
        entity.is_monitored
      when 'uni', 'demarc'
        false
      else 
        true
    end
    # TODO: we removed the additional check that you are not in ProvPending, as we still want to show graphs for ProvPending
    # show_monitoring && !entity.get_prov_state.is_a?(ProvPending)
  end


  # determines which direction the entity pop goes (north, south, east ,west)
  def entity_popup_direction(path_display, mode)
    # return entity type, override some types to make things a little cleaner
    # default_direction = 'nswe'
    # if monitoring
    #   'nwe'
    # else
    #   default_direction
    # end
    
    # Since we can switch modes in bubbles now the direction is always down
    'nwe'

  end

  # determine if pointer should be in the centered, left side or right side for views
  def pointer_class(row, column)

    number_of_columns = row.size
    position = row.index{ |c| c.is_a?(PathDisplay) && c.try(:id) == column.id } + 1
    placement = 'pointer_in_center'

    # Endpoints are always left or right
    if column.end_point?
      placement = case column.relative_position.downcase
      when 'left' then
        'pointer_on_left'
      when 'right' then
        'pointer_on_right'
      end
    else
      # OVCs are always centered
      unless column.ovc?
        placement = case position 
        when 1 then
          'pointer_on_left' 
        when number_of_columns then
          'pointer_on_right' 
        end
      end
    end
    placement
  end

  def path_top_level_entity_attrs(path, mode, details)
    ['ordering', 'inventory', 'monitoring'].inject({'data-mode' => mode}) do |hash, mode|
      params = {:mode => mode}
      params.merge!({:path_details => path.id}) if details
      hash.merge!("data-#{mode}_url" => list_row_path_path(path.id, params))
    end
  end

  def enni_top_level_entity_attrs(enni, mode, details)
    ['ordering', 'inventory', 'monitoring'].inject({'data-mode' => mode}) do |hash, mode|
      params = {:mode => mode}
      params.merge!({:enni_details => enni.id}) if details
      hash.merge!("data-#{mode}_url" => list_row_enni_path(enni.id, params))
    end
  end

  def path_ordering_state_data_url(path) 
    ordering_state_path_path(path.id)
  end

  def enni_ordering_state_data_url(enni) 
    ordering_state_enni_path(enni.id)
  end
  
  def path_monitoring_state_data_url(path) 
    sm_state_path_path(path.id)
  end
  
  def enni_monitoring_state_data_url(enni) 
    sm_state_enni_path(enni.id)
  end

  def entity_ordering_data_url(path_display) 
    # return entity type, override some types to make things a little cleaner
    entity = path_display.entity
    type = case entity.class.name
      when 'OnNetOvc' then ordering_cenx_ovc_path(entity.id)
      when 'OffNetOvc' then ordering_off_net_ovc_path(entity.id)
      when 'EnniNew' then ordering_enni_path(entity.id)
      when 'Uni', 'Demarc' then ordering_demarc_path(entity.id)
      when 'CenxOvcEndPointEnni' then ordering_cenx_ovc_end_point_enni_path(entity.id)
      when 'OvcEndPointEnni' then ordering_off_net_ovc_end_point_enni_path(entity.id)
      when 'OvcEndPointUni' then ordering_off_net_ovc_end_point_uni_path(entity.id)
      # TODO: Handle invalid type with appropriate error message, maybe even a error_path() url
      else 'Invalid URL'
    end
  end

  def entity_export_graphs_data_url(entity)
    type = case entity.class.name
      when 'OnNetOvc' then 'export_graphs_cenx_ovc_path'
      when 'OffNetOvc' then 'export_graphs_off_net_ovc_path'
      when 'EnniNew' then 'export_graphs_enni_path'
      when 'Uni', 'Demarc' then 'export_graphs_demarc_path'
      when 'CenxOvcEndPointEnni' then 'export_graphs_cenx_ovc_end_point_enni_path'
      when 'OvcEndPointEnni' then 'export_graphs_off_net_ovc_end_point_enni_path'
      when 'OvcEndPointUni' then 'export_graphs_off_net_ovc_end_point_uni_path'
      else 'Invalid URL'
    end
  end

  # return a label and value or label and default if no value
  def list_label_and_value(label, object, method, formatter=nil, default=nil, class_name=nil)

    # Assign value, either value return from method or just a value passed in
    value = if method.is_a?(Symbol) 
      object.respond_to?(method) ? object.send(method) : nil
    else
      # method is actually a value 
      method
    end
    
    # if no value found then use default
    if value.blank? || value.to_s.strip.upcase == 'N/A'
      value = default 
    # else format the value (if formatter supplied)
    else
      # format value, pass in '%s ms' as shortcut for format('%s ms')
      value = if formatter.is_a? String
        format(formatter).call(value)
      elsif formatter.is_a? Proc
        formatter.call(value)
      else
        value
      end
    end
    
    class_attr = class_name ? " class='#{class_name}'" : ''
    
    # nil default means display nothing (not even label)
    value.nil? ? '' : raw("<li#{class_attr}><label>#{label}: </label><span>#{value}</span></li>")
  end
 
  def monitoring_state_class(state)
    "monitoring #{state_to_class(state)}"
  end

  def alarm_state_class(state)
    "alarm #{state_to_class(state)}"
  end
  
  def state_to_class(state)
    state.downcase.gsub(' ', '_')
  end

  def unique_entity_id(entity)
    "#{type}_#{entity.id}"
  end
  
  def truncate_by_line(text, length)
    text.split("\n").map { |line|
      truncate(line, :length => length)
    }.join("\n") unless text.nil?
  end

  # Member handle for current user
  def member_handle_for_current_user(entity, user)
    PathsHelper.member_handle_for_current_user(entity, user)
  end
  
  # Member handle for current user
  def self.member_handle_for_current_user(entity, user)
    handle = entity.member_handle_for_owner_object(user.service_provider)
    handle ? handle.value : entity.cenx_name
  end

  # Member handle labels based on member attr type
  def member_handle_label(member_handle, is_system_owner=false)
    member_attr = member_handle.member_attr
    label = case member_attr.affected_entity_type
      when 'Path'
        "Circuit ID"
      when 'Demarc'
        address = member_handle.affected_entity.try(:address)
        "#{member_attr.name} <span>( Owned by </span> #{member_handle.affected_entity.service_provider.name}" + (address.blank? ? '' : " <span> / Located at </span>#{address}") + "<span> )</span>"
      when 'Segment'
        "#{member_attr.name} <span>( Owned by </span> #{member_handle.affected_entity.service_provider.name}<span> )</span>"
    end
    raw label && is_system_owner ? "<span>#{member_handle.service_provider.name}'s</span> " + label : label
  end
  
  # Entire row is readacted if not of the columns have an entity
  def entire_row_redacted?(row)
    row.any?{|col| col.entity} ? false : true
  end

  # Get service providers hidden in redacted columns
  def get_redacted_service_providers(redacted_columns)
    redacted_columns.map{|col| col.entity.try(:service_provider) if col.ovc?}.compact.uniq
  end

  # Show injection and reflection indicator arrows (images) over top of entities (components)
  def injection_reflection_indicators(column)
    if column.cenx_ovc?
      # Frames are injected at cenx ovcs
      injection_indicator(column)
      
    # COX HACK
    elsif column.demarc? && column.injection
      injection_indicator(column)
      
    elsif column.demarc? 
      # Frames are reflected at demarcs
      reflection_indicator(column)
    end
  end

  # Show injection and reflection indicator arrows (images) over top of redacted entities (components)
  def redacted_injection_indicators(redacted_columns)
    injection_indicators = redacted_columns.inject('') do |str, column|
      # Frames are injected at cenx ovcs
      injection_ind = injection_indicator(column)
      injection_ind ? str + injection_ind : str
    end
    # Must call raw as Rails 3 escape everything
    raw(injection_indicators)
  end
  
  # Show reflection indicator arrow (images) over top of entity (component)
  def reflection_indicator(column)
    image_src = if column.reflection_direction == :right
      "reflection_on_right.png"
    elsif column.reflection_direction == :left
      "reflection_on_left.png"
    end
    if image_src
      render :partial => '/paths/reflection_indicator', :locals => {:image_src => image_src} 
    end
  end

  # Show injection indicator arrow (images) over top of entity (component)
  def injection_indicator(column)
    if column.injection
      render :partial => '/paths/injection_indicator', :locals => {:image_src => 'injection.png'} 
    end
  end

  def monitoring_indicator(previous_column, column, next_column)
    render :partial => '/paths/monitoring_indicator', :locals => {:primary_backup => column.monitoring} if show_monitoring_based_on_reflection(previous_column, column, next_column)
  end
  
  def show_monitoring_based_on_reflection(previous_column, column, next_column)
    if column.end_point? && show_monitoring?(column.entity) && column.monitoring
      show = if column.relative_position == 'Right'
        next_column.reflection_direction == :right
      elsif column.relative_position == 'Left'
        previous_column.reflection_direction == :left
      end
    end
  end

  # TODO: This will not hold up when there is more than 1000 paths for a provider
  def path_count_for_enni(enni)
    SinPathServiceProviderSphinx.by_user(current_user).search_count("", :with => {:enni_ids => enni.id}, :group => 'path_id')
  end

  # TODO: This will not hold up when there is more than 1000 paths for a provider
  def path_count_for_path(path)
    SinPathServiceProviderSphinx.by_user(current_user).search_count("", :with => {:has_path_ids => path.id}, :group => 'path_id')
  end

  # Determine Side A / B for inventory bubble of Demarc/UNI
  def determine_side_a_b_for_demarc(path, demarc_in_diagram)
    right_ovc = PathDisplay.find_by_id_and_entity_type_and_path_id(demarc_in_diagram.id + 1, 'SegmentEndPointNew', path.id).try(:entity).try(:segment)
    # [A]<--->[B]<---->[C]
    # A: Demarc, OVC
    # B: Demarc, OVC (not OVC, OVC)
    # C: OVC, Demarc
    if right_ovc
      [demarc_in_diagram.entity, right_ovc]
    else
      left_ovc = PathDisplay.find_by_id_and_entity_type_and_path_id(demarc_in_diagram.id - 1, 'SegmentEndPointNew', path.id).try(:entity).try(:segment)
      [left_ovc, demarc_in_diagram.entity]
    end
  end

  
end