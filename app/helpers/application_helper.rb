# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper

  def hinted_text_field_tag(name, value = nil, hint = "Click and enter text", options={})
    value = value.nil? ? hint : value
    text_field_tag name, value, {:onclick => "if($(this).value == '#{hint}'){$(this).value = ''}", :onblur => "if($(this).value == ''){$(this).value = '#{hint}'}" }.update(options.stringify_keys)
  end
  
  def formatted_address
    sp = ServiceProvider.find_by_is_system_owner(true)
    return "N\\A" if sp.nil?
    sp.address.gsub("\n", "<br/>")
  end
  
  def formatted_website
    sp = ServiceProvider.find_by_is_system_owner(true)
    return "N\\A" if sp.nil?
    sp.website
  end

  def time_zone_options
    time_zone_options_for_select(Time.zone.name, [ActiveSupport::TimeZone['UTC'], ActiveSupport::TimeZone.us_zones].flatten)
  end

  def timestamp_in_time_zone(timestamp)
    timestamp = timestamp.to_i
    Time.at(timestamp > 9999999999 ? timestamp / 1000 : timestamp).in_time_zone
  end

  def timestamp_to_date_time(timestamp)
    timestamp_in_time_zone(timestamp).to_formatted_s(:ymd_time)
  end

  def timestamp_to_date(timestamp)
    timestamp_in_time_zone(timestamp).to_formatted_s(:ymd)
  end

end
