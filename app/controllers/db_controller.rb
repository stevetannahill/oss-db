require "pathname"
require "#{File.dirname(__FILE__)}/../../spec/db_helper.rb"

class DbController < ApplicationController 
  
  layout "admin"

  def index
    load_sin_docs
  end
  
  def success
    load_sin_docs
  end
  
  def reset_orders
    load_sin_docs
    eve = Eve::Application.new(:config => "./config/chris.yml")
    eve.cmd("sub purge chris.oss_db_orders.task_queue",true)
    @ids = eve.redis.smembers("Eve.ApplicationIds").collect { |id| id.start_with?("path") ? id : nil }.compact
    @ids.each { |id| eve.cmd("eve destroy #{id}",true,true) }

    run_cmd("rm -rf #{File.dirname(__FILE__)}/../../public/circuit_orders")

    asred_path = "#{File.dirname(__FILE__)}/../../lib/asred"
    run_rails_cmd("ruby #{asred_path}/asred_cmd.rb #{asred_path}/e3_nni.csv NNI","oss-db")
    run_rails_cmd("ruby #{asred_path}/asred_cmd.rb #{asred_path}/e3_att_template.csv TMPLT","oss-db")
    run_rails_cmd("ruby #{asred_path}/asred_cmd.rb #{@sin_doc_path}/poe/ericsson/poe_e3.csv POE","oss-db")
  end

  def reset_grid
    data = grid_app.cmd("grid reset!")[:data]
    @sources = data[:sources]
    @targets = data[:targets]
    @archives = data[:archives]
  end

  def reset_lkp
    @data = LKP.reset!
  end

  def reset
    @cmds_run = []
    load_sin_docs

    if @available
      run_cmd("cd #{@sin_doc_path} && git pull origin master")
      run_rails_cmd("rake db:drop","oss-db")
      run_rails_cmd("rake db:create","oss-db")
      execute_sql_file(@db_configs,"#{@sin_doc_path}/#{@db}.sql",true)
      run_cmd("rm -rf /cenx/osl/data/cm")
      run_rails_cmd("rake db:migrate","oss-db")
      run_cmd("touch #{File.dirname(__FILE__)}/../../tmp/restart.txt")
      run_rails_cmd("rake ts:reindex --silent","sin")

      osl_app.cmd("destroy")
      @osl_app = nil
      order_app.cmd("destroy")
      @order_app = nil

      reset_orders
    else
      flash[:notice] = "Unable to locate [#{@db}]"
    end

    @reset_name = "RESET OF #{@db}"
    calculate_summary
  end
  
  private 

    def calculate_summary
      @summary = { :succeeded => 0, :failed => 0 }
      @cmds_run.each { |data| id = data[:did_work] ? :succeeded : :failed; @summary[id] += 1 }
      @overall_success = @summary[:failed] == 0
    end

    def run_cmd(script, fresh_env = true)
      @cmds_run ||= []
      begin
        puts "RUNNING (#{script})"
        result = `#{script}`
        did_work = $?.to_i == 0
        answer = system(script,:unsetenv_others => fresh_env)
        puts "COMPLETED (#{script})"
      rescue Exception => e
        puts "ERROR: #{e.message}\n\n  #{e.backtrace.join("\n  ")}"
        result = "ERROR: #{e.message}\n\n  #{e.backtrace.join("\n  ")}"
        did_work = false
      end
      @cmds_run << { :script => script, :result => result, :did_work => did_work }
    end
  
    def run_rails_cmd(script,env)
      server = "/home/deployer/local/bin/#{env}_env"
      local = "#{Pathname.new(".").realpath}/../#{env}"
      File.exists?(server) ? 
        run_cmd("cd /cenx/#{env}/current && #{server} bundle exec #{script}") : 
        run_cmd("cd #{local} && #{script}",false)
    end
    
    def load_sin_docs
      @available_paths = [ "/home/deployer/sin_doc", "#{File.dirname(__FILE__)}/../../../sin_doc" ]
      @sin_doc_path = locate_sin_doc
      @avaiable_dbs = locate_dbs(@sin_doc_path)
      @db_configs = YAML::load(File.open("#{File.dirname(__FILE__)}/../../config/database.yml"))[Rails.env]
      
      unless params[:id].nil?
        @db = params[:id]
        @available = @avaiable_dbs.include?(@db)
      end
    end
  
    def locate_dbs(path)
      Dir["#{path}/*.sql"].collect do |f|
        Pathname.new(f).basename.to_s[0..-5]
      end
    end
  
    def locate_sin_doc
      @available_paths.each do |path|
        return path if File.exists?(path)
      end
      nil
    end

end