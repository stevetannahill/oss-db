class VersionController < ApplicationController 
  def show
    @commit = `cd #{File.dirname(__FILE__)}/../../ && git show | grep ^commit`.split[1]
    @version = "#{Cdb::VERSION}.#{@commit}"
    render :layout => nil
  end
end
