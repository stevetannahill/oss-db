class InventoryController < ApplicationController

  def get_entity_types authority
    response = EntityTypesResponse.new
    response.return_code = 0
    response.entity_type_list = []
    response.entity_type_list << "Path"
    response.entity_type_list << "Segment"
    response.entity_type_list << "SegmentEndPoint"
    response.entity_type_list << "Demarc"
    return response
  end

  def get_entity_information authority, entity_type
    response = EntityInformationResponse.new
    response.return_code = 0
    entity = nil
    case entity_type
    when 'Path'
      attributes = {'cenx_id'=>'string', 'member_id'=>'string' }
      assocs = {'Segment'=>'child' }
    when 'Segment'
      attributes = {'cenx_id'=>'string', 'member_id'=>'string' }
      assocs = {'Path'=>'parent', 'SegmentEndPoint'=>'child' }
    when 'SegmentEndPoint'
      attributes = {'cenx_id'=>'string', 'member_id'=>'string' }
      assocs = {'Segment'=>'parent', 'Demarc'=>'link', 'SegmentEndPoint'=>'link'}
    when 'Demarc'
      attributes = {'cenx_id'=>'string', 'member_id'=>'string' }
      assocs = {'SegmentEndPoint'=>'link' }
    else
      error_string = "get_inventory_attributes - invalid entity type: #{entity_type}"
      SW_ERR(error_string)
      response.error_string = error_string
      response.return_code = 1
      return response
    end

    response.entity_type = entity_type

    response.attribute_information_list = []
    attributes.each do |name,type|
      ai = AttributeInformation.new
      ai.attribute_name = name
      ai.attribute_type = type
      response.attribute_information_list << ai
    end

    response.assoc_information_list = []
    assocs.each do |entity_type,assoc_type|
      ai = AssocInformation.new
      ai.entity_type = entity_type
      ai.assoc_type = assoc_type
      response.assoc_information_list << ai
    end
    
    return response
  end

  def get_entity_ids authority, entity_type
    response = EntityIdsResponse.new
    response.return_code = 0

    case entity_type
    when 'Path'
      table = Path
    when 'Segment'
      table = Segment
    when 'SegmentEndPoint'
      table = SegmentEndPoint
    when 'Demarc'
      table = Demarc
    else
      error_string = "get_entity_ids - invalid entity type: #{entity_type}"
      SW_ERR(error_string)
      response.error_string = error_string
      response.return_code = 1
      return response
    end

    response.entity_type = entity_type
    response.entity_ids_list = []

    rows = table.all
    rows.each do |r|
      response.entity_ids_list << r.id
    end
    
    return response

  end

  def get_entity_data authority, entity_type, entity_id, attribute_filter, assoc_filter
    response = EntityDataResponse.new
    response.return_code = 0
    
    case entity_type
    when 'Path'
      table = Path
      attributes = {'cenx_id'=>'string', 'member_id'=>'string' }
    when 'Segment'
      table = Segment
      attributes = {'cenx_id'=>'string', 'member_id'=>'string' }
    when 'SegmentEndPoint'
      table = SegmentEndPoint
      attributes = {'cenx_id'=>'string', 'member_id'=>'string' }
    when 'Demarc'
      table = Demarc
      attributes = {'cenx_id'=>'string', 'member_id'=>'string' }
    else
      error_string = "get_entity_ids - invalid entity type: #{entity_type}"
      SW_ERR(error_string)
      response.error_string = error_string
      response.return_code = 1
      return response
    end

    rows = []
    if entity_id != ''
      rows << table.where("id = #{entity_id}").first
    else
      rows = table.all
    end

    response.entity_list = []

    rows.each do |row|
      ei = EntityInformation.new
      ei.entity_type = entity_type
      ei.entity_id = row.id
      ei.attribute_list = []
      attributes.each do |name,type|
        avi = AttributeValueInformation.new
        avi.attribute_name = name
        case name
        when 'cenx_id'
          avi.attribute_value = row.cenx_id
        when 'member_id'
          avi.attribute_value = row.member_handle_for_any_object.value
        end
        ei.attribute_list << avi
      end

      ei.assoc_list = []
      case entity_type
      when 'Path'
        assocs = {
          'Segment' => {'fcn' => 'segments', 'rel' => 'child', 'num' => '*'}}
      when 'Segment'
        assocs = {
          'Path' => {'fcn' => 'paths', 'rel' => 'parent', 'num' => '*'},
          'SegmentEndPoint' => {'fcn' => 'segment_end_points', 'rel' =>'child', 'num' => '*'}}
      when 'SegmentEndPoint'
        assocs = {
          'Segment' => {'fcn' => 'segment', 'rel' => 'parent', 'num' => '1'},
          'Demarc' => {'fcn' => 'demarc', 'rel' => 'link', 'num' => '1'},
          'SegmentEndPoint' => {'fcn' => 'segment_end_points', 'rel' => 'link', 'num' => '*'}}
      when 'Demarc'
        assocs = {'SegmentEndPoint' => {'fcn' => 'segment_end_points', 'rel' => 'link', 'num' => '*'} }
      end
      assocs.keys.each do |k|
        #raise "#{entity_type} #{k}"
        if assocs[k]['num'] == '*'
          row.send(assocs[k]['fcn']).each do |a|
            avi = AssocValueInformation.new
            avi.entity_type = "#{k}"
            avi.entity_id = a.id
            avi.assoc_type = "#{assocs[k]['rel']}"
            ei.assoc_list << avi
          end
        else
          a = row.send(assocs[k]['fcn'])
          avi = AssocValueInformation.new
          avi.entity_type = "#{k}"
          avi.entity_id = a.id
          avi.assoc_type = "#{assocs[k]['rel']}"
          ei.assoc_list << avi
        end
      end

      response.entity_list << ei
    end

    return response
  end
  
end
