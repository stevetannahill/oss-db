class OrderController < ApplicationController 
  
  layout "admin"

  def index
    begin

      @order_q = cmd("order q")
      @sitehandler_q = cmd("sitehandler q")

      @polling = cmd("poll running?")
      @poll_cmds = cmd("poll")
      @poll_duration = cmd("poll how-often")

      @raw_files = cmd("cm ls-raw-files 100").reverse!
      @order_exec = cmd("eve exec?")
      @order_enqueued = cmd("sub ls #{@order_q}")
      @order_processed = cmd("sub ls-processed #{@order_q}")
      @order_failed = cmd("sub ls-failed #{@order_q}")

      @sitehandler_enqueued = cmd("sub ls #{@sitehandler_q}")

      @uni_orders = cmd("att ls-uni")
      @evc_orders = cmd("att ls-evc")

      @uni_statuses = cmd("att unconfirmed-uni-status")
      @evc_statuses = cmd("att unconfirmed-evc-status")

      @registries = cmd("register")
      @versions = cmd("version").collect { |d| d[:data] }
      @redis = cmd("redis")

      @jira_cmds = cmd("jira ls 10")

      @webit_globals = cmd("webit global-get-all")
      @jira_globals = cmd("jira get-all")


    rescue Errno::ECONNREFUSED => e
      render action: 'connection_refused', locals: {error: "Redis is not running!"}
    end
  end

  def history
    num_lines = params[:num_lines].to_i
    num_lines = 100 if num_lines == 0
    @history = get_history(num_lines)
    respond_to do |format|
      format.html
    end
  end

  def _order_status
    @history = get_history(10)
    render partial: "order_status"
  end  

  def cmd(msg)
    osl_app.cmd(msg,true,false)[:data]
  end

  def get_history(size)
    history = cmd("tail -v #{size}").reverse
    regex_cases = [/^appid/, /^exit/, /^version/, /^archive name/, /^target/, /^source/, /^shell/, /^help/]
    history.select!{ |hist| regex_cases.none?{|reg| reg =~ hist[:cmd]} }
    history = history[0..size]
    history
  end

end