class AutomatedBuildCenterController < ApplicationController 
  
  layout "admin"
  
  def index
    @grid_info = GridTools.get_grid_info
    @cid_filter = ""
    # Show Failed by default
    @summary_filter = "Failed"
    @row_limit = "50"
    @builder_logs = get_logs(@summary_filter, @cid_filter, @row_limit) 
  end
  
  def edit_builder_log
    @builder_log = BuilderLog.find(params[:id])
  end
  
  def auto_update
    @grid_info = GridTools.get_grid_info
    abc_status
    render :partial => 'update'
  end

  def _abc_status
    abc_status
    render :partial => 'abc_status'
  end

  def abc_status
    @statuses = get_abc_monit

    @in_progress = osl_app.cmd("cid current")[:data]

    @pending = AutomatedBuildCenter.pending(osl_app)
    @total_pending = @pending.size
    @pending = @pending[0..15] # Only show first few

    total_summary = BuilderLog.summary
    @summary = Hash.new{|h,k| h[k]=Hash.new{|h1,k1| h1[k1]=Hash.new(0)}}    
    total_summary.each {|result, count| @summary[result]["Total"] = count}
    ["hour", "day", "week", "month"].each do |period|
      sub_summary = BuilderLog.summary(false, Time.now-1.send(period))
      @summary.keys.each {|result| @summary[result][period.capitalize] = sub_summary[result]}
    end
  end
  
  def destroy_builder_log
    @builder_log = BuilderLog.find(params[:id]).destroy

    respond_to do |format|
      format.html { redirect_to(:action => :index) }
    end
  end

  def requeue_all_of_type
    circuits = get_logs(params[:summary], "*")
    build_list = []
    circuits.each do |circuit|
      cid1, gid1 = GridTools.circuit_cid_gid(circuit["name"])
      cid2, gid2 = GridTools.circuit_cid_gid(circuit["id"])
      if cid1 == cid2 && gid1 == gid2
        build_list << "#{circuit["name"]}:#{circuit["id"]}"
      else
        if cid1 && gid1
          build_list << "#{cid1}:#{gid1}"
        end
        if cid2 && gid2
          build_list <<  "#{cid2}:#{gid2}"
        end
      end
    end
    if result = bulk_build(build_list.join("\n"))
      flash[:notice] = "#{build_list.size} Circuits have been queued"  
    elsif result.nil?
      flash[:notice] = "Circuit parsing failed: Circuits not queued"
    elsif !result
      flash[:notice] = "No circuits found"
    end
    render :partial => 'abc_bulk_build'
  end
  
  def abc_to_csv
    result_filter = params[:result]
    result_filter = "%" if result_filter.blank?
    outdata = BuilderLog.csv(result_filter)

    zippedfile = Tempfile.new("ABC_tempfile",  Rails.root.to_s + "/tmp")

    zip_out = Zip::ZipOutputStream.new(zippedfile.path)
    zip_out.put_next_entry("automated_build_center_results.csv")
    zip_out << outdata
    zip_out.close
    
    send_file(zippedfile.path,
       :filename => "automated_build_center_results.zip",
       :type => "application/zip",
       :dispostion => 'attachment')
    zippedfile.close!
  end  
  
  def abc_build_cid
    cid = params[:cid]
    gid = params[:gid]
    
    if gid.blank?
      flash[:notice] = "Grid Id must be valid #{gid}"
    else
      result = osl_app.cmd("pub send #{AutomatedBuildCenter::QUEUE} cid new #{cid} #{gid}")[:data]
      flash[:notice] = "Circuit Id #{cid} with Grid Id #{gid} has been queued"
    end
    render :partial => 'abc_build_cid', :locals => {:cid => cid, :gid => gid}
  end
  
  def abc_bulk_build
    if bulk_build(params[:cids])
      flash[:notice] = "Circuits have been queued"
    else
      flash[:notice] = "Circuits must be formatted as circuit:grid_id"
    end
    
    render :partial => 'abc_bulk_build'
  end
  
  def abc_filter
    @builder_logs = get_logs(params[:summary_filter], params[:cid_filter], params[:row_limit])

    render :partial => 'abc_results'      
  end
  
  def abc_pause_grid
    GridTools.stop_grid
    @grid_info = GridTools.get_grid_info
    abc_status
    render :partial => 'automated_build_center/update'
  end
  
  def abc_resume_grid
    GridTools.start_grid
    @grid_info = GridTools.get_grid_info
    abc_status
    render :partial => 'automated_build_center/update'
  end

  def remove_circuit
    circuit = params[:circuit]
    circuit.strip!
    temp = circuit.split(":")
    circuit = temp[0] #this handles the cases: ([cid], [gid], [cid/gid]:[any])

    # handle the case where one id is given in the form ([blank]:[cid/gid])
    circuit = temp[1] if temp.size == 2 and circuit.empty?

    #if it is a grid id it will be just an integer
    circuit = circuit.to_i if circuit.to_i.to_s == circuit

    if (result = GridTools.remove_circuit(circuit)).nil?
      remove_status = "Circuit #{circuit} Successfully removed"
    else
      remove_status = result
    end

    render :update do |page|
      page.replace :remove_status, :partial => 'remove_status', :locals => { :remove_status => remove_status }
    end
  end
  
  
  private

  def bulk_build(id_pairs = "")
    if !id_pairs.blank?
      split_ids = id_pairs.split("\n").collect {|cid_gid| cid_gid.split(":")}
      if split_ids.all? {|cid_gid| cid_gid.size == 2}  
        split_ids.each do |cid_gid|
          cid, gid = cid_gid
          osl_app.cmd("pub send #{AutomatedBuildCenter::QUEUE} cid new #{cid} #{gid}")
        end
        true # success
      else
        nil # bad format
      end
    else
      false # no data
    end
  end
  
  def get_logs(summary_filter = "*", cid_filter = "*", row_limit = "")
    summary_filter = "*" if summary_filter.blank?
    cid_filter = "*" if cid_filter.blank?
    summary_filter.gsub!(/\*/, "%")
    cid_filter.gsub!(/\*/, "%")

    if row_limit.to_i.to_s == row_limit #is it an int string
      row_limit = row_limit.to_i
      if row_limit < 1
        row_limit = nil
      end
    elsif row_limit.empty? || row_limit == "*"
      row_limit = nil
    else
      row_limit = 50
    end

    if row_limit
      BuilderLog.where("summary like ? AND name like ?", summary_filter, cid_filter).order('updated_at DESC').limit(row_limit)
    else
      BuilderLog.where("summary like ? AND name like ?", summary_filter, cid_filter).order('updated_at DESC')
    end
  end

  def get_config_hash
    config_file = File.expand_path("../../../config/summary_page_ssh.json",__FILE__)
    JSON.load(File.open(config_file)).with_indifferent_access[:abc]
  end

  def get_abc_monit
    @config_hash = get_config_hash unless @config_hash

    output = nil
    if @config_hash[:host] and !@config_hash[:host].empty?
      Net::SSH.start(@config_hash[:host], @config_hash[:user], :password => @config_hash[:pswd]) do |ssh|
        output = ssh.exec!("monit status")
      end
    else
      output = `monit status`
    end

    found_status = false
    status = []
    if output
      output.split("\n").each do |line|
        if line =~ /^[A-Z].*'.*'/
          if found_status
            break # stop recording
          elsif line == "Process 'automated_build_center'"
            found_status = true # start recording
          end
        end
        status << line if found_status
      end
    else
      if !@config_hash[:host] or @config_hash[:host].empty?
        host = "local machine"
      else
        host = @config_hash[:host]
      end
      status = ["Monit Not Running on: #{host}"]
    end
    return [status]
  end
end