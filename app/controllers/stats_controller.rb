require "cenx_id_generator.rb"
require "cenx_name_generator.rb"
require 'csv'

# Need to change the LineStyle class so that dash lines work correctly
module OpenFlashChart
  class LineStyle < LineBase
    def initialize on, off, args={}
      #super args
      @on    = on
      @off   = off
      @style = "dash"
    end
  end
end


# The stats_controller uses REDIS to store the results of queries to SAM and Brix beccause
# collecting data is a time consuming process and to display the seperate graphs is a seperate action
# so there are multiple fetches of the same data when in fact only one fetch is required. 
class StatsController < ApplicationController

  REDIS_SESSION_ID = "cdb_stats_graph:id"
  REDIS_DATA_KEY = "cdb_stats_graph:data:"
  REDIS_EXPIRE = 10.minutes

  def home
  end

  def index
    respond_to do |format|
      format.html { redirect_to(:action => :home) }
    end
  end
  
  def stats_input_params_onovc
    @onovcs = OnNetOvc.get_active_onovcs
    @onovcs = @onovcs.sort_by {|o| o.pull_down_name_onovc}
    @graphs = []
    @id = params[:id].to_i
    @error_text = "Please fill out params above"    
  end

  def stats_input_params_segmentep
    @segmenteps = SegmentEndPoint.get_bx_monitored_endpoints
    @segmenteps = @segmenteps.sort_by {|o| o.cenx_name}
    @graphs = []
    @id = params[:id].to_i
    @error_text = "Please fill out params above"
  end
  
  def stats_input_params_enni
    @ennis = EnniNew.get_active_cenx_ennis
    @ennis = @ennis.sort_by {|e| e.config_description}
    @graphs = []
    @id = params[:id].to_i
    @error_text = "Please fill out params above"
  end

  # added to recieve the post data for the OFC png image of the OFC graph
  def upload_image_onovc
    @onovc = OnNetOvc.find(params[:obj_id])
    @service_id = @onovc.service_id
    @start_time = Time.parse(params[:start_time])
    @end_time = Time.parse(params[:end_time]) + 24.hours
    @report_type = params[:report_type]
    data = params[:data]
    
    @png_images = []
    data.each_line('#') {|png| @png_images << png[0..-2]}    

    princely = Princely.new()
    html_string = render_to_string(:template => "stats/sla_report.pdf.erb", :layout => false)

    send_data(princely.pdf_from_string(html_string),
       :filename => "sla_report_#{@service_id}.pdf",
       :type => "application/pdf",
       :dispostion => 'attachment'
    )
  end
  
  # added to recieve the post data for the OFC png image of the OFC graph
  def upload_image_enni
    @enni = EnniNew.find(params[:obj_id])
    @start_time = Time.parse(params[:start_time])
    @end_time = Time.parse(params[:end_time]) + 24.hours
    @report_type = params[:report_type]
    data = params[:data]
    
    @png_images = []
    data.each_line('#') {|png| @png_images << png[0..-2]}    

    princely = Princely.new()
    html_string = render_to_string(:template => "stats/sla_report_enni.pdf.erb", :layout => false)

    send_data(princely.pdf_from_string(html_string),
       :filename => "sla_report_#{@enni.cenx_name}.pdf",
       :type => "application/pdf",
       :dispostion => 'attachment'
    )
  end
  

  # added to recieve the post data for the OFC png image of the OFC graph
  def upload_image_segmentep
    @segmentep = SegmentEndPoint.find(params[:obj_id])
    @start_time = Time.parse(params[:start_time])
    @end_time = Time.parse(params[:end_time]) + 24.hours
    data = params[:data]
    
    @png_images = []
    data.each_line('#') {|png| @png_images << png[0..-2]}    

    princely = Princely.new()
    html_string = render_to_string(:template => "stats/sla_report_segmentep.pdf.erb", :layout => false)

    send_data(princely.pdf_from_string(html_string),
       :filename => "sla_report_#{@segmentep.cenx_name}.pdf",
       :type => "application/pdf",
       :dispostion => 'attachment'
    )
  end

  def save_to_csv_onovc
    time_format = '%d/%m/%y %H:%M:%S'
    onovc = OnNetOvc.find(params[:obj_id])
    service_id = onovc.service_id
    start_time = Time.parse(params[:start_time]).to_i
    end_time = (Time.parse(params[:end_time]) + 24.hours).to_i
    report_type = params[:report_type]
    
    start_time_ms = start_time.to_i * 1000
    end_time_ms = end_time.to_i * 1000
    stats = onovc.stats_data(start_time, end_time)
    supported_stats = stats.get_supported_stats 
    
    delay_attributes = stats.get_delay_attributes(:sla)
    if report_type == "Troubleshooting Report"  
      ignore_sla = true
      delay_attributes += stats.get_delay_attributes(:troubleshooting)
    else
      ignore_sla = false
    end
    
    outfile = ""
    csv_out = CSV.new(outfile)

    csv_out << ["SLA report for: #{onovc.owner_info_segment}"]
    csv_out << ["Circuit ID: #{onovc.owner_member_handle_value}"]
    csv_out << ["Period: #{Time.at(start_time).strftime(time_format)} to #{Time.at(end_time).strftime(time_format)}"]
    csv_out << ["","COS", "SLA", "Actual", "Details"]
    
    # Get a list of unavailable times so the results in those times can be removed from the data
    alarms = onovc.alarm_history.subset(start_time_ms, end_time_ms)
    mtc_times = onovc.mtc_history.subset(start_time_ms, end_time_ms)
    chop_times = mtc_times + alarms   
    if supported_stats.include?(:availability) 
      data = stats.stats_availability_results 
      csv_out << ["Availability", "", data[:sla_monthly], data[:aggregate_avail]]
      csv_out << ["","","",""] + data[:data].flatten.collect{|v| Time.at(v[:x]).strftime(time_format)} 
      csv_out << ["","","",""] + data[:data].flatten.collect{|v| v[:y] == 1 ? "Available" : "Unavailable"}
    
      data = stats.stats_availability_results(AlarmSeverity::MTC, mtc_times) 
      csv_out << ["Maintenance"]
      csv_out << ["","","",""] + data[:data].flatten.collect{|v| Time.at(v[:x]).strftime(time_format)} 
      csv_out << ["","","",""] + data[:data].flatten.collect{|v| v[:y] == 1 ? "Not in Maintenance" : "In Maintenance"}
    end
    
    if supported_stats.include?(:flr)
      normalized_data = stats.stats_get_normalized_flr_data(ignore_sla)
        
      # Calculate the FLR for the data
      flr_data = stats.stats_flr_results(normalized_data)
      csv_out << ["Frame Loss Ratio for #{service_id}"]
    
      flr_data.each do |cos_name, results|
        overall_flr = results[:aggregate_flr]
        csv_out << ["", cos_name, results[:sla][:sla_threshold], overall_flr]
        results[:data].each do |name, data|  
          if ["Turnup Test Report", "Troubleshooting Report"].include?(report_type)
            # Show all data for these reports
            seg_lines = [data]
          else
            seg_lines = stats.stats_chop_results(data, chop_times.alarm_records, Stats::UNAVAILABLE_THRESHOLD)
          end   
          csv_out << [name, "", "", ""] + seg_lines.flatten.collect{|v| Time.at(v[:x]).strftime(time_format)}
          csv_out << ["", "", "", ""] + seg_lines.flatten.collect{|v| v[:y]}
        end
      end
    end

    if supported_stats.include?(:delay)
      raw_data = stats.stats_get_delay_data(delay_attributes)
      [:delay, :delay_variation].each do |delay_type|
        stats.stats_delay_attribute_subset(delay_type, delay_attributes).each do |delay_attribute|       
          data = stats.stats_delay_results(raw_data, delay_attribute, ignore_sla)
          if data[:sla].size != 0
            csv_out << [delay_attribute.to_s.camelcase]
            data[:sla].each_pair do |cos_name, sla_info|
              data[:data].each_pair do |test_name, results|
                if results.has_key?(cos_name) 
                  if ["Turnup Test Report", "Troubleshooting Report"].include?(report_type)
                    # Show all data for these reports
                    seg_lines = [results[cos_name]]
                  else
                    seg_lines = stats.stats_chop_results(results[cos_name], chop_times.alarm_records, Stats::UNAVAILABLE_THRESHOLD)
                  end
                  csv_out << [test_name, cos_name] + sla_info.collect {|sla, value| [sla.to_s.gsub(/sla_/, "").humanize, value]}.flatten
                  csv_out << ["", "",       ""       , ""] + seg_lines.flatten.collect{|v| Time.at(v[:x]).strftime(time_format)}
                  csv_out << ["", "",       ""       , ""] + seg_lines.flatten.collect{|v| v[:y]}
                end
              end
            end
          end
        end
      end
    end
  
    if (report_type == "Troubleshooting Report") && (supported_stats.include?(:traffic))
      counters = [:dropped_frames, :frame_counts, :dropped_octets, :octet_counts]
      normalized_data = stats.stats_get_frame_count_data(counters)
      counters.each do |counter_type|  
        counter_data = stats.stats_frame_results(normalized_data, counter_type)
        csv_out << [counter_type.to_s]
        counter_data.each do |sap_name, sap_data| 
          sap_data[:data].each do |cos_name, results|
            csv_out << [sap_name, cos_name, "Ingress", ""]        
            csv_out << ["",       "", "", ""] + results[:ingress].collect {|v| Time.at(v[:x]).strftime(time_format) }
            csv_out << ["",       "", "", ""] + results[:ingress].collect {|v| v[:y] }
            csv_out << [sap_name, cos_name, "Egress", ""]        
            csv_out << ["",     "", "", ""] + results[:egress].collect {|v| Time.at(v[:x]).strftime(time_format) }
            csv_out << ["",     "", "", ""] + results[:egress].collect {|v| v[:y] }
          end
        end
      end
      
      # Add the traffic rate in Mbits/s
      counter_data = stats.stats_frame_results(normalized_data, :octet_counts, stats.octets_to_mbps)
      csv_out << ["Traffic Rate(Mbits/s)"]
      counter_data.each do |sap_name, sap_data| 
        sap_data[:data].each do |cos_name, results|
          sla_header = []
          if (!results[:sla].blank?) && (!results[:sla][:ingress].blank?)  
            sla_header = ["Utilization High Threshold (Mbits/s)", results[:sla][:ingress][:utilization_high_threshold],
                          "Utilization Low Threshold (Mbits/s)", results[:sla][:ingress][:utilization_low_threshold]]
          end          
          csv_out << [sap_name, cos_name, "Ingress"] + sla_header        
          csv_out << ["",       "", "", ""] + results[:ingress].collect {|v| Time.at(v[:x]).strftime(time_format) }
          csv_out << ["",       "", "", ""] + results[:ingress].collect {|v| v[:y] }
          
          sla_header = []
          if (!results[:sla].blank?) && (!results[:sla][:egress].blank?)  
            sla_header = ["Utilization High Threshold (Mbits/s)", results[:sla][:egress][:utilization_high_threshold],
                          "Utilization Low Threshold (Mbits/s)", results[:sla][:egress][:utilization_low_threshold]]
          end
          csv_out << [sap_name, cos_name, "Egress"] + sla_header        
          csv_out << ["",     "", "", ""] + results[:egress].collect {|v| Time.at(v[:x]).strftime(time_format) }
          csv_out << ["",     "", "", ""] + results[:egress].collect {|v| v[:y] }
        end
      end
    end
    
    send_data(outfile,
       :filename => "sla_report_#{service_id}.csv",
       :type => "application/text",
       :dispostion => 'attachment'
    )
  end    

  def save_to_csv_segmentep
    time_format = '%d/%m/%y %H:%M:%S'
    segmentep = SegmentEndPoint.find(params[:obj_id])
    start_time = Time.parse(params[:start_time]).to_i
    end_time = (Time.parse(params[:end_time]) + 24.hours).to_i
    report_type = params[:report_type]

    start_time_ms = start_time.to_i * 1000
    end_time_ms = end_time.to_i * 1000
        
    outfile = ""
    csv_out = CSV.new(outfile)


    segmentep.cos_end_points.collect {|cep| cep.cos_test_vectors}.flatten.each do |ctv|
      csv_out << ["SLA report for #{segmentep.cenx_name}-#{ctv.name} for the period #{Time.at(start_time).strftime(time_format)} to #{Time.at(end_time).strftime(time_format)}",
                  "COS", "SLA", "Actual", "Details"]
      cosep = ctv.cos_end_point
      stats = ctv.stats_data(start_time, end_time)
      supported_stats = stats.get_supported_stats
      
      stats.bx_availability_kludge
      delay_attributes = stats.get_delay_attributes(:sla)
      if report_type == "Troubleshooting Report"  
        ignore_sla = true
        delay_attributes += stats.get_delay_attributes(:troubleshooting)
      else
        ignore_sla = false
      end
      
      # Get a list of unavailable times so the results in those times can be removed from the data
      alarms = ctv.alarm_history.subset(start_time_ms, end_time_ms)
      mtc_times = ctv.mtc_history.subset(start_time_ms, end_time_ms)
      chop_times = mtc_times + alarms   
      if supported_stats.include?(:availability) 
        data = stats.stats_availability_results
        csv_out << ["Availability", "", data[:sla_monthly], data[:aggregate_avail]]
        csv_out << ["","","",""] + data[:data].flatten.collect{|v| Time.at(v[:x]).strftime(time_format)} 
        csv_out << ["","","",""] + data[:data].flatten.collect{|v| v[:y] == 1 ? "Available" : "Unavailable"}
      end
      
      if supported_stats.include?(:flr)  
        normalized_data = stats.stats_get_normalized_flr_data
        
        # Calculate the FLR for the data
        flr_data = stats.stats_flr_results(normalized_data)
        flr_data.each do |cos_name, results|
          overall_flr = results[:aggregate_flr]
          csv_out << ["Frame Loss Ratio", cos_name, results[:sla][:sla_threshold], overall_flr]
          results[:data].each do |name, data|
            seg_lines = stats.stats_chop_results(data, chop_times.alarm_records, Stats::UNAVAILABLE_THRESHOLD)     
            csv_out << [name, "", "", ""] + seg_lines.flatten.collect{|v| Time.at(v[:x]).strftime(time_format)}
            csv_out << ["", "", "", ""] + seg_lines.flatten.collect{|v| v[:y]}
          end
        end
      end
      
      if supported_stats.include?(:delay)
        raw_data = stats.stats_get_delay_data(delay_attributes)
        [:delay, :delay_variation].each do |delay_type|
          stats.stats_delay_attribute_subset(delay_type, delay_attributes).each do |delay_attribute|       
            data = stats.stats_delay_results(raw_data, delay_attribute, ignore_sla)          
            if data[:sla].size != 0
              csv_out << [delay_attribute.to_s.camelcase]
              data[:sla].each_pair do |cos_name, sla_info|
                data[:data].each_pair do |test_name, results|
                  if results.has_key?(cos_name) 
                    seg_lines = stats.stats_chop_results(results[cos_name], chop_times.alarm_records, Stats::UNAVAILABLE_THRESHOLD)  
                    thresholds = sla_info.collect {|sla, value| [sla.to_s.gsub(/sla_/, "").humanize, value]}.flatten        
                    thresholds = ["", "", ""]*2 if thresholds.empty? # Pad if there are no threshold
                    csv_out << [test_name, cos_name] + thresholds + seg_lines.flatten.collect{|v| Time.at(v[:x]).strftime(time_format)}                    
                    csv_out << ["",              ""] + [""]*thresholds.size + seg_lines.flatten.collect{|v| v[:y]}
                  end
                end
              end
            end
          end
        end
      end
    end
    send_data(outfile,
       :filename => "sla_report_#{segmentep.cenx_name}.csv",
       :type => "application/text",
       :dispostion => 'attachment'
    )
  end  
  
  def save_to_csv_enni
    time_format = '%d/%m/%y %H:%M:%S'
    enni = EnniNew.find(params[:obj_id])
    start_time = Time.parse(params[:start_time]).to_i
    end_time = (Time.parse(params[:end_time]) + 24.hours).to_i
    report_type = params[:report_type]
    
    start_time_ms = start_time.to_i * 1000
    end_time_ms = end_time.to_i * 1000
    
    outfile = ""
    csv_out = CSV.new(outfile)

    csv_out << ["Service Monitoring Report for: #{enni.operator_network.service_provider.name}"]
    csv_out << ["ENNI ID: #{enni.owner_member_handle_value}"]
    csv_out << ["CENX ID: #{enni.cenx_id}"]
    csv_out << ["Period: #{Time.at(start_time).strftime(time_format)} to #{Time.at(end_time).strftime(time_format)}"]
    
    stats = enni.stats_data(Time.parse(params[:start_time]).to_i, (Time.parse(params[:end_time])+24.hours).to_i)  
    supported_stats = stats.get_supported_stats 
    if supported_stats.include?(:traffic)
      # Collect data
      counters = [:octet_counts, :frame_counts]
      normalized_data = stats.stats_get_enni_count_data(counters)
    
      octet_mbps = stats.stats_frame_results(normalized_data, :octet_counts, stats.octets_to_mbps)
      data = {:octet_counts => stats.stats_frame_results(normalized_data, :octet_counts), :frame_counts => stats.stats_frame_results(normalized_data, :frame_counts)}  
    
      # Get list of LAGs/ports to display
      display_items = [enni.stats_displayed_name]
      if enni.is_lag
        display_items += enni.ports.collect {|port| "#{port.node.name} " + port.stats_displayed_name}
      end
    
      # output the Line rate, CIR and EIR
      csv_out << ["Line capacity (Mbits/s)","CIR (Mbits/s)", "EIR (Mbits/s)", "Utilization High Threshold (Mbits/s)", "Utilization Low Threshold (Mbits/s)"] 
      csv_out << [octet_mbps[enni.stats_displayed_name][:sla][:line_rate], octet_mbps[enni.stats_displayed_name][:sla][:cir], octet_mbps[enni.stats_displayed_name][:sla][:eir],
                  octet_mbps[enni.stats_displayed_name][:sla][:utilization_high_threshold], octet_mbps[enni.stats_displayed_name][:sla][:utilization_low_threshold]] 
      csv_out << ["","COS", "Direction", "", "Details"]
      display_items.each do |displayed_name|  
        # Add the traffic rate in Mbits/s        
        csv_out << ["Traffic Rate(Mbits/s)"]
        octet_mbps[displayed_name][:data].each do |cos_name, results| 
            csv_out << [displayed_name, cos_name, "Ingress", ""]        
            csv_out << ["",       "", "", ""] + results[:ingress].collect {|v| Time.at(v[:x]).strftime(time_format) }
            csv_out << ["",       "", "", ""] + results[:ingress].collect {|v| v[:y] }
            csv_out << [displayed_name, cos_name, "Egress", ""]        
            csv_out << ["",     "", "", ""] + results[:egress].collect {|v| Time.at(v[:x]).strftime(time_format) }
            csv_out << ["",     "", "", ""] + results[:egress].collect {|v| v[:y] }
          end
          
        # Add the octets and frame counts
        counters.each do |counter_type|               
          csv_out << [counter_type.to_s]
          data[counter_type][displayed_name][:data].each do |cos_name, results| 
            csv_out << [displayed_name, cos_name, "Ingress", ""]        
            csv_out << ["",       "", "", ""] + results[:ingress].collect {|v| Time.at(v[:x]).strftime(time_format) }
            csv_out << ["",       "", "", ""] + results[:ingress].collect {|v| v[:y] }
            csv_out << [displayed_name, cos_name, "Egress", ""]        
            csv_out << ["",     "", "", ""] + results[:egress].collect {|v| Time.at(v[:x]).strftime(time_format) }
            csv_out << ["",     "", "", ""] + results[:egress].collect {|v| v[:y] }
          end
        end
      end
    end
    
    send_data(outfile,
       :filename => "sla_report_#{enni.cenx_id}.csv",
       :type => "application/text",
       :dispostion => 'attachment'
    )
  end    
  
    
  def stats_graph_onovc
    @graphs = []   
    
    @error_text = validate_params(params, "Segment")
    if !@error_text.empty?
      render :partial => 'stats_graph', :locals => {:action_postfix => "onovc"}
      return
    end

    @onovc_id = params[:obj_id]
    @onovc = OnNetOvc.find(@onovc_id)   
    @start_time = params[:start_time]
    @end_time = params[:end_time]
    @report_type = params[:report_type]
    
    redis_id = set_session_data(@onovc, params[:start_time], params[:end_time], @report_type)
    stats = @onovc.stats_data(Time.parse(@start_time), Time.parse(@end_time) + 24.hours)
    supported_stats = stats.get_supported_stats
    
    graph_root_name = "cenxsla"
    @graphs = []
    if supported_stats.include?(:availability)
      name = graph_root_name + "#{@graphs.size}"
      @graphs << open_flash_chart_object_from_hash(
          "#{url_for :controller => 'stats',
                     :action => 'gen_graph_availability',
                     :object_id => @onovc_id,
                     :class => @onovc.class.to_s,
                     :start_time => params[:start_time],
                     :end_time => params[:end_time],
                     :report_type => @report_type,
                     :title => "Circuit: #{@onovc.owner_member_handle_value}"
           }", :div_name => name, :width => "100%",:height => 300)
    end
    
    if supported_stats.include?(:flr)
      name = graph_root_name + "#{@graphs.size}"
      @graphs << open_flash_chart_object_from_hash(
           "#{url_for :controller => 'stats',
                      :action => 'gen_graph_mib_frame_loss_ratio',
                      :object_id => @onovc_id, 
                      :class => @onovc.class.to_s,
                      :start_time => params[:start_time],
                      :end_time => params[:end_time],
                      :report_type => @report_type,
                      :title => "Circuit: #{@onovc.owner_member_handle_value}",
                      :redis_id => redis_id
            }", :div_name => name, :width =>'100%',:height => 500)
    end

    if supported_stats.include?(:delay)
      # For each delay jitter test create a seperate graph
      #stats = @onovc.stats_data(Time.parse(params[:start_time]).to_i, (Time.parse(params[:end_time])+24.hours).to_i)  
      delay_attributes = stats.get_delay_attributes(:sla)
      if @report_type == "Troubleshooting Report"
        ignore_sla = true
        delay_attributes += stats.get_delay_attributes(:troubleshooting)
      else
        ignore_sla = false
      end
    
      delay_tests = stats.stats_get_delay_tests(ignore_sla)
      delay_tests["delay_variation"].each do |test_name|
        stats.stats_delay_attribute_subset(:delay_variation, delay_attributes).each do |delay_attribute|           
          name = graph_root_name + "#{@graphs.size}"
          @graphs << open_flash_chart_object_from_hash(
              "#{url_for :controller => 'stats',
                         :action => 'gen_graph_delay_for_one_test',
                         :object_id => @onovc_id,
                         :class => @onovc.class.to_s,
                         :start_time => params[:start_time],
                         :end_time => params[:end_time],
                         :report_type => @report_type,
                         :test_name => test_name,
                         :delay_type => delay_attribute,
                         :test_title => "#{stats.stats_delay_attriute_type(delay_attribute)} One way Frame Delay Variation",
                         :title => "Circuit: #{@onovc.owner_member_handle_value}",
                         :redis_id => redis_id
                }", :div_name => name, :width =>'100%',:height => 500)
        end
      end

      # For each delay test create a seperate graph
      delay_tests["delay"].each do |test_name|    
        stats.stats_delay_attribute_subset(:delay, delay_attributes).each do |delay_attribute|                
          name = graph_root_name + "#{@graphs.size}"
          @graphs << open_flash_chart_object_from_hash(
              "#{url_for :controller => 'stats',
                         :action => 'gen_graph_delay_for_one_test',
                         :object_id => @onovc_id,
                         :class => @onovc.class.to_s,
                         :start_time => params[:start_time],
                         :end_time => params[:end_time],
                         :test_name => test_name,
                         :report_type => @report_type,
                         :delay_type => delay_attribute,
                         :test_title => "#{stats.stats_delay_attriute_type(delay_attribute)} One way Frame Delay",
                         :title => "Circuit: #{@onovc.owner_member_handle_value}",
                         :redis_id => redis_id
                }", :div_name => name, :width =>'100%',:height => 500)
        end
      end
    end
  
    if (@report_type == "Troubleshooting Report") && (supported_stats.include?(:traffic))
      # Get the Dropped and frame data via SOAP  
      #stats = @onovc.stats_data(Time.parse(params[:start_time]).to_i, (Time.parse(params[:end_time])+24.hours).to_i)  
      
      redis_data = LKP.redis.get(REDIS_DATA_KEY + "#{redis_id}")
      redis_data = Marshal.load(redis_data) if redis_data
      
      normalized_data = stats.stats_get_frame_count_data([:dropped_frames, :frame_counts, :dropped_octets, :octet_counts])
      frame_data = stats.stats_frame_results(normalized_data, :dropped_frames)       
      redis_data[:dropped_frame_data] = frame_data      
      frame_data = stats.stats_frame_results(normalized_data, :frame_counts) 
      redis_data[:frame_count_data] = frame_data
      frame_data = stats.stats_frame_results(normalized_data, :octet_counts) 
      redis_data[:octet_count_data] = frame_data
      frame_data = stats.stats_frame_results(normalized_data, :dropped_octets) 
      redis_data[:dropped_octet_data] = frame_data
      frame_data = stats.stats_frame_results(normalized_data, :octet_counts, stats.octets_to_mbps)
      redis_data[:octet_mbps_data] = frame_data
      
      redis_marshal = Marshal.dump(redis_data)
      LKP.redis.setex(REDIS_DATA_KEY + "#{redis_id}", REDIS_EXPIRE, redis_marshal)
      
      # To make for better display if the Segment is a simple 2 endpoint COS (with an optional test endpoint) then plot
      # The Endpoint_1 Ingress and Endpoint_2 Egress on one graph and Endpoint_2 Ingress and Endpoint_1 Egress
      # on another and then the optional test endpoint.
      # If there are more than 2 endpoints then just plot a graph per endpoint ie Ingress and Egress for that endpoint
      mon_endpoints = @onovc.get_cenx_monitoring_segment_endpoints
      endpoints = @onovc.segment_end_points.reject {|ep| mon_endpoints.include?(ep)} 
      
      display_endpoints = []
      if endpoints.size == 2
        display_endpoints << [[endpoints.first.id, :ingress], [endpoints.last.id, :egress]]
        display_endpoints << [[endpoints.last.id, :ingress], [endpoints.first.id, :egress]]
      else
        display_endpoints = endpoints.collect {|ep| [[ep.id, :ingress], [ep.id, :egress]] }
      end
      display_endpoints += mon_endpoints.collect {|ep| [[ep.id, :ingress], [ep.id, :egress]] }

      display_endpoints.each do |ep|
        ep_ids = ep.collect {|p| p[0]}
        dirs = ep.collect {|p| p[1]}
        ep_names = ep_ids.collect {|ep| SegmentEndPoint.find(ep).stats_displayed_name}.uniq.join("->")

        # packet count
        name = graph_root_name + "#{@graphs.size}"
        @graphs << open_flash_chart_object_from_hash(
            "#{url_for :controller => 'stats',
                       :action => 'gen_graph_ingress_egress_counts',
                       :object_id => @onovc_id,
                       :class => @onovc.class.to_s,
                       :ep_ids => ep_ids,
                       :dirs => dirs,
                       :start_time => params[:start_time],
                       :end_time => params[:end_time],
                       :report_type => @report_type,
                       :title => "#{ep_names}",
                       :frame_title => "Forwarded Frames",
                       :frame_type => :frame_count_data,
                       :units => "Frames",
                       :redis_id => redis_id
              }", :div_name => name, :width =>'100%',:height => 500)

        # Octet count
        name = graph_root_name + "#{@graphs.size}"
        @graphs << open_flash_chart_object_from_hash(
            "#{url_for :controller => 'stats',
                       :action => 'gen_graph_ingress_egress_counts',
                       :object_id => @onovc_id,
                       :class => @onovc.class.to_s,
                       :ep_ids => ep_ids,
                       :dirs => dirs,
                       :start_time => params[:start_time],
                       :end_time => params[:end_time],
                       :report_type => @report_type,
                       :title => "#{ep_names}",
                       :frame_title => "Forwarded Octets",
                       :frame_type => :octet_count_data,
                       :units => "Octets",
                       :redis_id => redis_id
             }", :div_name => name, :width =>'100%',:height => 500)

        # Octet count in Mbits/s
        name = graph_root_name + "#{@graphs.size}"
        @graphs << open_flash_chart_object_from_hash(
            "#{url_for :controller => 'stats',
                       :action => 'gen_graph_ingress_egress_counts',
                       :object_id => @onovc_id,
                       :class => @onovc.class.to_s,
                       :ep_ids => ep_ids,
                       :dirs => dirs,
                       :start_time => params[:start_time],
                       :end_time => params[:end_time],
                       :report_type => @report_type,
                       :title => "#{ep_names}",
                       :frame_title => "Forwarded Traffic Rate",
                       :frame_type => :octet_mbps_data,
                       :units => "Mbits/s",
                       :redis_id => redis_id
            }", :div_name => name, :width =>'100%',:height => 500)
             
        # Dropped frames
        name = graph_root_name + "#{@graphs.size}"
        @graphs << open_flash_chart_object_from_hash(
            "#{url_for :controller => 'stats',
                       :action => 'gen_graph_ingress_egress_counts',
                       :object_id => @onovc_id,
                       :class => @onovc.class.to_s,
                       :ep_ids => ep_ids,
                       :dirs => dirs,
                       :start_time => params[:start_time],
                       :end_time => params[:end_time],
                       :report_type => @report_type,
                       :title => "#{ep_names}",
                       :frame_title => "Dropped frames",
                       :frame_type => :dropped_frame_data,
                       :units => "Frames",
                       :redis_id => redis_id
            }", :div_name => name, :width =>'100%',:height => 400)
             

        # Dropped octets
        name = graph_root_name + "#{@graphs.size}"
        @graphs << open_flash_chart_object_from_hash(
            "#{url_for :controller => 'stats',
                       :action => 'gen_graph_ingress_egress_counts',
                       :object_id => @onovc_id,
                       :class => @onovc.class.to_s,
                       :ep_ids => ep_ids,
                       :dirs => dirs,
                       :start_time => params[:start_time],
                       :end_time => params[:end_time],
                       :report_type => @report_type,
                       :title => "#{ep_names}",
                       :frame_title => "Dropped Octets",
                       :frame_type => :dropped_octet_data,
                       :units => "Octets",
                       :redis_id => redis_id
            }", :div_name => name, :width =>'100%',:height => 400)
      end
    end
    @obj_id = @onovc_id
    render :partial => 'stats_graph', :locals => {:action_postfix => "onovc"}
    return
  end

  def stats_graph_segmentep
    @error_text = validate_params(params, "Segment Endpoint")
    @graphs = []   
    
    if !@error_text.empty?
      render :partial => 'stats_graph', :locals => {:action_postfix => "segmentep"}
      return
    end

    @segmentep_id = params[:obj_id]
    @segmentep = SegmentEndPoint.find(@segmentep_id)
    @start_time = params[:start_time]
    @end_time = params[:end_time]
    @report_type = params[:report_type]    
        
    graph_root_name = "cenxsla"
    @graphs = []
    @segmentep.cos_end_points.collect {|cep| cep.cos_test_vectors}.flatten.each do |ctv|
      cosep = ctv.cos_end_point
      redis_id = set_session_data(ctv, params[:start_time], params[:end_time], @report_type)
      stats = ctv.stats_data(Time.parse(@start_time), Time.parse(@end_time) + 24.hours)
      supported_stats = stats.get_supported_stats
      
      if supported_stats.include?(:availability)    
        name = graph_root_name + "#{@graphs.size}"
        @graphs << open_flash_chart_object_from_hash(
            "#{url_for :controller => 'stats',
                       :action => 'gen_graph_availability',
                       :object_id => ctv.id,
                       :class => ctv.class.to_s,
                       :start_time => params[:start_time],
                       :end_time => params[:end_time],
                       :report_type => @report_type,
                       :title => "#{@segmentep.segment.owner_member_handle_value}-#{ctv.name}\nCoS: #{cosep.cos_name}"
             }", :div_name => name, :width => "100%",:height => 350)
      end
      if supported_stats.include?(:flr)    
        name = graph_root_name + "#{@graphs.size}"
        @graphs << open_flash_chart_object_from_hash(
            "#{url_for :controller => 'stats',
                       :action => 'gen_graph_mib_frame_loss_ratio',
                       :object_id => ctv.id,
                       :class => ctv.class.to_s,                   
                       :start_time => params[:start_time],
                       :end_time => params[:end_time],
                       :report_type => @report_type,
                       :title => "#{@segmentep.segment.owner_member_handle_value}-#{ctv.name}\nCoS: #{cosep.cos_name}",
                       :redis_id => redis_id
             }", :div_name => name, :width =>'100%',:height => 400)
      end
      
      if supported_stats.include?(:delay)
        delay_attributes = stats.get_delay_attributes(:sla)
        if @report_type == "Troubleshooting Report"
          ignore_sla = true
          delay_attributes += stats.get_delay_attributes(:troubleshooting)
        else
          ignore_sla = false
        end
        delay_tests = stats.stats_get_delay_tests(ignore_sla)      
        # For each delay jitter test create a seperate graph
        delay_tests["delay_variation"].each do |test_name|
          stats.stats_delay_attribute_subset(:delay_variation, delay_attributes).each do |delay_attribute|              
            name = graph_root_name + "#{@graphs.size}"
            @graphs << open_flash_chart_object_from_hash(
                "#{url_for :controller => 'stats',
                           :action => 'gen_graph_delay_for_one_test',
                           :object_id => ctv.id,
                           :class => ctv.class.to_s,
                           :start_time => params[:start_time],
                           :end_time => params[:end_time],
                           :report_type => @report_type,
                           :test_name => test_name,
                           :delay_type => delay_attribute,
                           :test_title => "#{stats.stats_delay_attriute_type(delay_attribute)} One way Frame Delay Variation",
                           :title => "#{@segmentep.segment.owner_member_handle_value}-#{ctv.name}\nCoS: #{cosep.cos_name}",
                           :redis_id => redis_id
                  }", :div_name => name, :width =>'100%',:height => 400)
          end
        end

        # For each delay test create a seperate graph
        delay_tests["delay"].each do |test_name|         
          stats.stats_delay_attribute_subset(:delay, delay_attributes).each do |delay_attribute|             
            name = graph_root_name + "#{@graphs.size}"
            @graphs << open_flash_chart_object_from_hash(
                "#{url_for :controller => 'stats',
                           :action => 'gen_graph_delay_for_one_test',
                           :object_id => ctv.id,
                           :class => ctv.class.to_s,
                           :start_time => params[:start_time],
                           :end_time => params[:end_time],
                           :report_type => @report_type,
                           :test_name => test_name,
                           :delay_type => delay_attribute,
                           :test_title => "#{stats.stats_delay_attriute_type(delay_attribute)} One way Frame Delay",
                           :title => "#{@segmentep.segment.owner_member_handle_value}-#{ctv.name}\nCoS: #{cosep.cos_name}",
                           :redis_id => redis_id
                  }", :div_name => name, :width =>'100%',:height => 400)        
          end
        end
      end
    end
    
    @obj_id = @segmentep_id
    render :partial => 'stats_graph', :locals => {:action_postfix => "segmentep"}
    return
  end


  def stats_graph_enni
    @graphs = []   

    @error_text = validate_params(params, "ENNI")
    if !@error_text.empty?
      render :partial => 'stats_graph', :locals => {:action_postfix => "enni"}
      return
    end

    @enni_id = params[:obj_id]
    @enni = EnniNew.find(@enni_id)
    @start_time = params[:start_time]
    @end_time = params[:end_time]
    @report_type = params[:report_type]
        
    redis_id = set_enni_session_data(@enni, params[:start_time], params[:end_time], @report_type)
        
    graph_root_name = "cenxsla"
    @graphs = []
    # Get the ENNI octet data 
    stats = @enni.stats_data(Time.parse(params[:start_time]).to_i, (Time.parse(params[:end_time])+24.hours).to_i)
    supported_stats = stats.get_supported_stats 
    
    if supported_stats.include?(:traffic) 
      redis_data = LKP.redis.get(REDIS_DATA_KEY + "#{redis_id}")
      redis_data = Marshal.load(redis_data) if redis_data
      
      normalized_data = redis_data[:octet_counts]
      frame_data = stats.stats_frame_results(normalized_data, :octet_counts, stats.octets_to_mbps)
      
      redis_data[:octet_mbps_data] = frame_data      
      frame_data = stats.stats_frame_results(normalized_data, :octet_counts) 
      redis_data[:octet_count_data] = frame_data
      frame_data = stats.stats_frame_results(normalized_data, :frame_counts) 
      redis_data[:frame_count_data] = frame_data
      
      redis_marshal = Marshal.dump(redis_data)
      LKP.redis.setex(REDIS_DATA_KEY + "#{redis_id}", REDIS_EXPIRE, redis_marshal)
    
      # Get list of LAgs/ports to display
      display_items = [@enni.stats_displayed_name]
      if @enni.is_lag
        display_items += @enni.ports.collect {|port| "#{port.node.name} " + port.stats_displayed_name}
      end
    
      display_items.each do |display_name|      
        # Octet count in Mbits/s
        name = graph_root_name + "#{@graphs.size}"
        @graphs << open_flash_chart_object_from_hash(
            "#{url_for :controller => 'stats',
                       :action => 'gen_graph_enni_utilization',
                       :object_id => @enni_id,
                       :class => @enni.class.to_s,
                       :dirs => [:ingress, :egress],
                       :start_time => params[:start_time],
                       :end_time => params[:end_time],
                       :report_type => @report_type,
                       :title => "#{@enni.cenx_name}",
                       :displayed_name => display_name,
                       :frame_title => "Forwarded Traffic Rate",
                       :frame_type => :octet_mbps_data,
                       :units => "Mbits/s",
                       :redis_id => redis_id
            }", :div_name => name, :width =>'100%',:height => 500)
        
        # Octet count
        name = graph_root_name + "#{@graphs.size}"
        @graphs << open_flash_chart_object_from_hash(
            "#{url_for :controller => 'stats',
                       :action => 'gen_graph_enni_utilization',
                       :object_id => @enni_id,
                       :class => @enni.class.to_s,
                       :dirs =>  [:ingress, :egress],
                       :start_time => params[:start_time],
                       :end_time => params[:end_time],
                       :report_type => @report_type,
                       :title => "#{@enni.cenx_name}",
                       :displayed_name => display_name,
                       :frame_title => "Forwarded Octets",
                       :frame_type => :octet_count_data,
                       :units => "Octets",
                       :redis_id => redis_id
             }", :div_name => name, :width =>'100%',:height => 500)
         
        # Frame count
        name = graph_root_name + "#{@graphs.size}"
        @graphs << open_flash_chart_object_from_hash(
            "#{url_for :controller => 'stats',
                       :action => 'gen_graph_enni_utilization',
                       :object_id => @enni_id,
                       :class => @enni.class.to_s,
                       :dirs =>  [:ingress, :egress],
                       :start_time => params[:start_time],
                       :end_time => params[:end_time],
                       :report_type => @report_type,
                       :displayed_name => display_name,
                       :title => "#{@enni.cenx_name}",
                       :frame_title => "Forwarded Frames",
                       :frame_type => :frame_count_data,
                       :units => "Frames",
                       :redis_id => redis_id
             }", :div_name => name, :width =>'100%',:height => 500)
      end
    end
    @obj_id = @enni_id
    render :partial => 'stats_graph', :locals => {:action_postfix => "enni"}
    return
  end

MAX_X_AXIS_POINTS = 40


def gen_graph_enni_utilization
    object_id = params[:object_id]
    obj = params[:class].constantize.find(params[:object_id])
    start_time = Time.parse(params[:start_time])
    end_time = Time.parse(params[:end_time]) + 24.hours
    report_type = params[:report_type]
    displayed_name = params[:displayed_name]
    redis_id = params[:redis_id].to_i
    start_time_ms = start_time.to_i * 1000
    end_time_ms = end_time.to_i * 1000

    redis_data = LKP.redis.get(REDIS_DATA_KEY + "#{redis_id}")
    redis_data = Marshal.load(redis_data) if redis_data
    
    if !redis_data
      SW_ERR "Redis Data is nil"
    end
    
    unavail_mtc = redis_data[:unavail_times] #obj.mtc_history.subset(start_time_ms, end_time_ms) + alarms
    stats = obj.stats_data(start_time, end_time)
    frame_data = redis_data[params[:frame_type].intern]

    y_steps = 10
    max_val = 0.0
    min_val = 1e20

    lines = []
    sla_data = frame_data[displayed_name][:sla]     
    enni_data = frame_data[displayed_name][:data]
    # For each direction create a line
    params[:dirs].each do |dir|
      direction = dir.intern
      enni_data.each do |cos_name, results|
        if ["Turnup Test Report", "Troubleshooting Report"].include?(report_type)
           # Show all data for these reports 
           seg_lines = [results[direction]]
         else
           seg_lines = stats.stats_chop_results(results[direction], unavail_mtc.alarm_records, Stats::UNAVAILABLE_THRESHOLD)
         end
         
        seg_lines.collect! {|sl| sl.collect {|point| ScatterValue.new(point[:x], point[:y]) }}                                                                          
        lines << {:line_data => {"#{displayed_name} #{direction.to_s.camelcase}" => seg_lines}}
        
        sap_max = results[direction].each.max{|a,b| a[:y] <=> b[:y]}
        sap_min = results[direction].each.min{|a,b| a[:y] <=> b[:y]}
        max_val = sap_max[:y] if sap_max != nil && sap_max[:y] > max_val
        min_val = sap_min[:y] if sap_min != nil && sap_min[:y] < min_val
      end
    end   
                                                                   
    if max_val == min_val
      max_val = max_val+1
    elsif max_val < min_val
      # in the case where is no data make max, min some default values
      max_val = 10
      min_val = 0
    end
    min_val, max_val = adjust_min_max(min_val, max_val, 10)    
    
    lines += mtc_lines(unavail_mtc.alarm_records, start_time_ms, end_time_ms, min_val, max_val)

    # Create the title and threshold lines
    title = "#{params[:frame_title]} for #{params[:title]}"
    if sla_data != nil
      title << "\nCapacity (#{sla_data[:line_rate]} Mbits/s)\nCIR limit (#{sla_data[:cir]} Mbits/s) EIR limit (#{sla_data[:eir]} Mbits/s)"
      [:utilization_high_threshold, :utilization_low_threshold].each do |util_threshold|
        if sla_data.has_key?(util_threshold)          
          threshold = sla_data[util_threshold]
          lines << {:sla_data => {"#{util_threshold.to_s.humanize} (#{threshold}Mbits/s)" => 
            [ScatterValue.new(start_time.to_i, threshold), ScatterValue.new(end_time.to_i, threshold)]}}
        end
      end
    end
    
    chart = create_graph(title, params[:units], lines, start_time, end_time, min_val, max_val, y_steps)
    # The render will create floats in the mantissa/exponent format but Javascript does not appear to recognize this format so need to convert
    # floats to expanded format eg 1.0000000023
    b = chart.to_s    
    c = b.gsub(/:[+,-]*[0-9]\.[0-9]*e[-|+]../){|m| ":%0.10f" % m[1..-1]}
    render :text => c
end

# takes a list of endpoints (:ep_ids) and a direction for each ep_id the direction (:dirs) and plot them
def gen_graph_ingress_egress_counts
    object_id = params[:object_id]
    obj = params[:class].constantize.find(params[:object_id])
    start_time = Time.parse(params[:start_time])
    end_time = Time.parse(params[:end_time]) + 24.hours
    start_time_ms = start_time.to_i * 1000
    end_time_ms = end_time.to_i * 1000
    redis_id = params[:redis_id].to_i
    
    redis_data = LKP.redis.get(REDIS_DATA_KEY + "#{redis_id}")
    redis_data = Marshal.load(redis_data) if redis_data
    
    if !redis_data
      SW_ERR "Redis Data is nil"
    end
    
    unavail_mtc = redis_data[:unavail_times] #obj.mtc_history.subset(start_time_ms, end_time_ms) + alarms
    frame_data = redis_data[params[:frame_type].intern]

    y_steps = 10
    max_val = 0.0
    min_val = 1e20

    lines = []

    # For each endpoint_id get the ep_id and the direction to display
    # and create a line for each CoS
    params[:ep_ids].each_index do |idx|
      ep_id = params[:ep_ids][idx]
      direction = params[:dirs][idx].intern
      sap_name = SegmentEndPoint.find(ep_id).stats_displayed_name
      if !frame_data[sap_name].blank?
        sap_data = frame_data[sap_name][:data]
        sap_data.each do |cos_name, results|
          direction_line = results[direction].collect {|point| ScatterValue.new(point[:x], point[:y]) }
          lines << {:line_data => {"#{sap_name} #{cos_name} #{direction.to_s.camelcase}" => [direction_line]}}      
          if !results[:sla].blank? && !results[:sla][direction].blank?          
            [:utilization_high_threshold, :utilization_low_threshold].each do |util_threshold|
              if results[:sla][direction].has_key?(util_threshold)
                threshold = results[:sla][direction][util_threshold]
                lines << {:sla_data => {"#{sap_name} #{cos_name} #{direction.to_s.camelcase} #{util_threshold.to_s.humanize} (#{threshold}Mbits/s)" => 
                  [ScatterValue.new(start_time.to_i, threshold), ScatterValue.new(end_time.to_i, threshold)]}}
              end
            end
          end
          sap_max = results[direction].each.max{|a,b| a[:y] <=> b[:y]}
          sap_min = results[direction].each.min{|a,b| a[:y] <=> b[:y]}
          max_val = sap_max[:y] if sap_max != nil && sap_max[:y] > max_val
          min_val = sap_min[:y] if sap_min != nil && sap_min[:y] < min_val
        end
      end
    end
    if max_val == min_val
      max_val = max_val+1
    elsif max_val < min_val
      # In case there is no data make default values
      max_val = 10
      min_val = 0
    end
    min_val, max_val = adjust_min_max(min_val, max_val, 5)
    
    lines += mtc_lines(unavail_mtc.alarm_records, start_time_ms, end_time_ms, min_val, max_val)

    # Create the title
    title = "#{params[:frame_title]} for #{params[:title]}"

    chart = create_graph(title, params[:units], lines, start_time, end_time, min_val, max_val, y_steps)
    # The render will create floats in the mantissa/exponent format but Javascript does not appear to recognize this format so need to convert
    # floats to expanded format eg 1.0000000023
    b = chart.to_s
    c = b.gsub(/:[+,-]*[0-9]\.[0-9]*e[-|+]../){|m| ":%0.10f" % m[1..-1]}
    render :text => c
end

def gen_graph_mib_frame_loss_ratio
    object_id = params[:object_id]
    obj = params[:class].constantize.find(params[:object_id])
    report_type = params[:report_type]
    start_time = Time.parse(params[:start_time])
    end_time = Time.parse(params[:end_time]) + 24.hours
    start_time_ms = start_time.to_i * 1000
    end_time_ms = end_time.to_i * 1000
    redis_id = params[:redis_id].to_i
    
    redis_data = LKP.redis.get(REDIS_DATA_KEY + "#{redis_id}")
    redis_data = Marshal.load(redis_data) if redis_data
    
    if !redis_data
      SW_ERR "Redis Data is nil"
    end

    chop_times = redis_data[:unavail_times]
    stats = obj.stats_data(start_time, end_time)
    normalized_data = redis_data[:flr_normalized_data]

    # Calculate the FLR for the data
    flr_data = stats.stats_flr_results(normalized_data)

    lines = []
    title = "Frame Loss Ratio for #{params[:title]}\n"
    max_val = 0.0
    min_val = 100.0
    
    flr_data.each do |cos_name, results|
      results[:data].each do |name, data|
        if ["Turnup Test Report", "Troubleshooting Report"].include?(report_type)
          # Show all data for these reports 
          seg_lines = [data]
        else
          seg_lines = stats.stats_chop_results(data, chop_times.alarm_records, Stats::UNAVAILABLE_THRESHOLD)
        end

        seg_lines.collect! {|sl| sl.collect {|point| ScatterValue.new(point[:x], point[:y]) }}

        if data.size > 0 #!seg_lines.empty?
          max_val = [max_val, data.each.max{|a,b| a[:y] <=> b[:y]}[:y]].max
          min_val = [min_val, data.each.min{|a,b| a[:y] <=> b[:y]}[:y]].min
        end
      
        lines += [{:line_data => {"Frame Loss Ratio (#{cos_name}-#{name})" => seg_lines}}]
      end
      
      # Create the title
      overall_flr = results[:aggregate_flr]
      title += "#{cos_name} Aggregate Value: #{overall_flr}% Guaranteed Monthly Values: (#{results[:sla][:sla_threshold]}%)\n"
    end
    
    # Adjust the max val if they are very small so the graph looks straight
    # if it's less than 0.01 % frame loss then set the max to 0.01
    y_steps = 10
    if max_val <= 0.05
      max_val = 0.05
      y_steps = 5
    end    
    
    lines += mtc_lines(chop_times.alarm_records, start_time_ms, end_time_ms, min_val, max_val)
    
    chart = create_graph(title, "Percent", lines, start_time, end_time, min_val, max_val, y_steps)
    # The render will create floats in the mantissa/exponent format but Javascript does not appear to recognize this format so need to convert
    # floats to expanded format eg 1.0000000023
    b = chart.to_s
    c = b.gsub(/:[+,-]*[0-9]\.[0-9]*e[-|+]../){|m| ":%0.10f" % m[1..-1]}
    render :text => c
end

def gen_graph_delay_for_one_test
    object_id = params[:object_id]
    obj = params[:class].constantize.find(params[:object_id])
    report_type = params[:report_type]
    start_time = Time.parse(params[:start_time])
    end_time = Time.parse(params[:end_time]) + 24.hours
    start_time_ms = start_time.to_i * 1000
    end_time_ms = end_time.to_i * 1000
    test_name = params[:test_name]
    redis_id = params[:redis_id].to_i

    stats = obj.stats_data(start_time, end_time)
    
    redis_data = LKP.redis.get(REDIS_DATA_KEY + "#{redis_id}")
    redis_data = Marshal.load(redis_data) if redis_data
    
    if !redis_data
      SW_ERR "Redis Data is nil"
    end

    data = redis_data[params[:delay_type].intern]

    if data[:sla].size == 0
      render :nothing => true
      return
    end

    chop_times = redis_data[:unavail_times]
    min_val = 65535
    max_val = 0

    lines = []

    # create the data lines and segment based on alarms
    data[:sla].each_pair do |cos_name, sla_info|
      points = []
      if data[:data].has_key?(test_name) && data[:data][test_name].has_key?(cos_name)
        # Strip out any data with NIL values as the graph package does not handle nils nil means no data available
        data[:data][test_name][cos_name].delete_if {|result| !result[:y]}
        max_this_cos = data[:data][test_name][cos_name].each.max{|a,b| a[:y] <=> b[:y]}[:y]
        min_this_cos = data[:data][test_name][cos_name].each.min{|a,b| a[:y] <=> b[:y]}[:y]
        max_val = max_this_cos if max_val < max_this_cos
        min_val = min_this_cos if min_val > min_this_cos
        if ["Turnup Test Report", "Troubleshooting Report"].include?(report_type)
          # Show all data for these reports
          seg_lines = [data[:data][test_name][cos_name]]
        else
          seg_lines = stats.stats_chop_results(data[:data][test_name][cos_name], chop_times.alarm_records, Stats::UNAVAILABLE_THRESHOLD)
        end
        seg_lines.collect! {|sl| sl.collect! {|point| ScatterValue.new(point[:x], point[:y]) }}
        points = seg_lines
      end

      x = {}
      x[:line_data] = {cos_name => points}
      x[:sla_data] = {}
      # Add the sla thresholds if any
      sla_info.each do |sla, value|
        x[:sla_data]["#{cos_name} #{sla.to_s.gsub(/sla_/, "").humanize} (#{value}us)"] = [ScatterValue.new(start_time.to_i, value.to_f),      
                                                                                          ScatterValue.new(end_time.to_i, value.to_f)]
      end                                                          
      lines << x
    end
    if max_val == 0 && min_val == 65535
      min_val = 0
      max_val = 10
    end

    # Adjust max and min so there is space at the top & bottom
    # Make sure there is a difference between min & max otherwise the graph is messed up...
    if min_val == max_val
      max_val += 10
    end
    min_val, max_val = adjust_min_max(min_val, max_val, 10)    
    

    # If there is no data set the title to indicate this
    title = "#{params[:test_title]} for #{params[:title]}\n"
    title += "Test ID: #{test_name}\n" if !test_name.empty?
    title += "Aggregate Value: #{data[:aggregate]}" if data[:aggregate]
    if !data[:data].has_key?(test_name)
      title = title + " No data is available"
      min_val = 0
      max_val = 1
    end

    lines += mtc_lines(chop_times.alarm_records, start_time_ms, end_time_ms, min_val, max_val)
    units = "microseconds"
    chart = create_graph(title, units, lines, start_time, end_time, min_val, max_val)
    render :text => chart.to_s
end

def gen_graph_availability
  object_id = params[:object_id]
  obj = params[:class].constantize.find(params[:object_id])
  report_type = params[:report_type]
  start_time = Time.parse(params[:start_time])
  end_time = Time.parse(params[:end_time]) + 24.hours
  start_time_ms = start_time.to_i * 1000
  end_time_ms = end_time.to_i * 1000

  stats = obj.stats_data(start_time, end_time)

  lines= []
  data = stats.stats_availability_results
  chop_times = obj.mtc_history.subset(start_time_ms, end_time_ms)
  seg_lines = stats.stats_chop_results(data[:data], chop_times.alarm_records, AlarmSeverity::MTC, true)

  title = "Availability for #{params[:title]}\n"

  if seg_lines.size == 0
    title = title + "No Availability available"
  else
    lines = []
    seg_lines.each do |sl|
      points = []
      previous_p = nil
      sl.each do |p|
        if p == sl.last
          points << ScatterValue.new(p[:x], previous_p[:y]) if previous_p != nil && previous_p != p
        else
          if  previous_p != nil && p[:y] != previous_p[:y]
            points << ScatterValue.new(p[:x], previous_p[:y])
            points << ScatterValue.new(p[:x], p[:y])
          else
          end
          points << ScatterValue.new(p[:x], p[:y])

        end
        previous_p = p
      end
      lines << points
    end

    title += "Aggregate Value: #{data[:aggregate_avail]}%\n"
    title += "Period: #{data[:from_time].strftime("%d/%m/%y %H:%M")} - #{data[:to_time].strftime("%d/%m/%y %H:%M")}"
  end

  # Add the SLA requirements to the title
  title << "\n"
  title = title + "Guaranteed Monthly Value: #{data[:sla_monthly]}%\n"

  min_val = 0
  max_val = 1

  lines = [{:line_data => {"Availability" => lines}}]
  lines += mtc_lines(chop_times.alarm_records, start_time_ms, end_time_ms, min_val, max_val)

  chart = create_graph(title, "", lines, start_time, end_time, min_val, max_val, 10, MAX_X_AXIS_POINTS, ["Not Available", "Available"])
  render :text => chart.to_s
end


def mtc_lines(mtc_times, start_time_ms, end_time_ms, min_val, max_val, fail_threshold = Stats::UNAVAILABLE_THRESHOLD, ok_threshold = AlarmSeverity::FAILED)
  mtc_points = []
  alarm_points = []
  ok_points = []
  previous_state = mtc_times.first.state if !mtc_times.empty? 
  mtc_start_record = nil
  draw_mtc_box = false
  mtc_times.each do |mt|
    if mt.time_of_event >= start_time_ms.to_i
      if mt.state >= fail_threshold #&& previous_state <= ok_threshold
        if mt.state == AlarmSeverity::MTC && previous_state != mt.state
          mtc_start_record = mt
          previous_state = mt.state
        else
          alarm_points << [ScatterValue.new(mt.time_of_event/1000, min_val), ScatterValue.new(mt.time_of_event/1000, max_val)] if previous_state != mt.state
          draw_mtc_box = true if mtc_start_record != nil
          previous_state = fail_threshold
        end
        #previous_state = fail_threshold
      elsif mt.state <= ok_threshold
        if mtc_start_record != nil
          draw_mtc_box = true
        else
          ok_points << [ScatterValue.new(mt.time_of_event/1000, min_val), ScatterValue.new(mt.time_of_event/1000, max_val)] if previous_state >= fail_threshold
        end
        previous_state = ok_threshold
      end
      if draw_mtc_box
        if mtc_start_record != nil
          mtc_points << [ScatterValue.new(mtc_start_record.time_of_event/1000, min_val), ScatterValue.new(mtc_start_record.time_of_event/1000, max_val),
                         ScatterValue.new(mtc_start_record.time_of_event/1000, max_val), ScatterValue.new(mt.time_of_event/1000, max_val),
                         ScatterValue.new(mt.time_of_event/1000, max_val), ScatterValue.new(mt.time_of_event/1000, min_val)]
          mtc_start_record = nil
        end
        draw_mtc_box = false
      end
    else
      previous_state = mt.state
    end
  end

  if mtc_start_record != nil
    mtc_points << [ScatterValue.new(mtc_start_record.time_of_event/1000, min_val), ScatterValue.new(mtc_start_record.time_of_event/1000, max_val),
                   ScatterValue.new(mtc_start_record.time_of_event/1000, max_val), ScatterValue.new(Time.now.to_i, max_val),
                   ScatterValue.new(Time.now.to_i, max_val), ScatterValue.new(Time.now.to_i, min_val)]
    mtc_start_record = nil
  end

  return [{:mtc_data => {:maintenance => mtc_points}},
          {:mtc_data => {:unavailable => alarm_points}},
          {:mtc_data => {:ok => ok_points}}]
end


# The y_data [
#              {:line_data => {"line_name" => [[scatter_points],[scatter_points]]}, {:sla_data => {"sla_name" => [scatter_points]}}
#              ......
#           ]
def create_graph title, y_legend, y_data, start_time, end_time, min_val, max_val, y_steps = 10, x_steps = MAX_X_AXIS_POINTS, y_labels = nil

  colours = ["#000000", "#FF0000", "#00C000", "#0000FF", "#996633", "#483D8B", "#D2691E", "#FF8C00"]
  mtc_colours = {:maintenance => "#666666", :unavailable => "#FF0000", :ok => "#006600"}

  title = Title.new("#{title}")
  title.set_style('{font-size: 20px; color: #181818}')

  lines = Array.new()
  colour_index = 0

  # add data lines
  y_data.each do |a_line|
    if a_line.has_key?(:line_data)
      line_text_set_once = false
      a_line[:line_data].values.first.each do |line_segment|
        line = ScatterLine.new("#{colours[colour_index]}", 3)
        default_dot = SolidDot.new()
        default_dot.size = 3
        default_dot.tooltip = "#{a_line[:line_data].keys.first.to_s}<br>#date:Y-m-d H:i#<br>#val#"
        line.default_dot_style = default_dot
        if !line_text_set_once
          line.text = "#{a_line[:line_data].keys.first.to_s}"
          line_text_set_once = true
        end
        line.colour = "#{colours[colour_index]}"
        line.values = line_segment #a_line[:line_data].values.first
        lines = lines << line
      end
    end
    if a_line.has_key?(:sla_data)
      a_line[:sla_data].each do |sla_name, points|
      line = ScatterLine.new("#{colours[colour_index]}", 1)
        sla_dot = SolidDot.new()
        sla_dot.size = 2
        sla_dot.tooltip = "#{sla_name} #date:Y-m-d H:i# <br>#val#"
        line.default_dot_style = sla_dot
        line.text = "#{sla_name}"
        line.colour = "#{colours[colour_index]}"
        line.values = points
        lines = lines << line
      end
    end
    if a_line.has_key?(:mtc_data)
      line_text_set_once = false
      key = a_line[:mtc_data].keys.first
      a_line[:mtc_data].values.first.each do |line_segment|
        if key == :maintenance # Draw a shaded area for maintenance periods
          line = AreaBase.new
          line.width = 1
        else
          line = Line.new
          line.line_style = LineStyle.new(5,3)
          line.width = 2
        end
        default_dot = SolidDot.new()
        default_dot.size = 1
        default_dot.tooltip = "#{key.to_s} #date:Y-m-d H:i#"
        line.default_dot_style = default_dot
        if !line_text_set_once
          line.text = "#{key.to_s}"
          line_text_set_once = true
        else
          line.text = ""
        end
        line.colour = "#{mtc_colours[key]}"
        line.values = line_segment 
        lines = lines << line
      end
    end
    colour_index += 1 if a_line.has_key?(:sla_data) || a_line.has_key?(:line_data)
  end

  x_labels = XAxisLabels.new()
  x_step = (end_time.to_i - start_time.to_i) / x_steps
  x_labels.text = "#date:Y-m-d H:i#"
  x_labels.steps = x_step.to_i
  x_labels.visible_steps = 1
  x_labels.rotate = 90

  x = XAxis.new
  x.steps = x_step.to_i
  x.set_range(start_time.to_i, end_time.to_i)
  x.labels = x_labels

  y = YAxis.new
  y.set_range(min_val,max_val,(max_val-min_val)/y_steps)
  #y.set_range(min_val,max_val)
  y.labels = y.set_labels_from_array(y_labels) if y_labels != nil
  #y.log_scale = true if loger

  y_legend = YLegend.new(y_legend)
  y_legend.set_style('{font-size: 20px; color: #770077}')

  chart = OpenFlashChart.new
  chart.set_title(title)
  chart.set_y_legend(y_legend)
  chart.x_axis = x
  chart.y_axis = y
  chart.set_bg_colour("#FFFFFF")
  lines.each {|line| chart.add_element(line)}

  return chart
end

# Alarm, Mtc and in service state

def index_node_state
  @nodes = Node.all
  respond_to do |format|
    format.html 
  end
end

def index_path_state
  @paths = Path.all
  respond_to do |format|
    format.html 
  end
end

def index_enni_new_state  
  @ennis = EnniNew.all.select{|enni| !enni.is_connected_to_test_port}
  respond_to do |format|
    format.html 
  end
end

def index_segment_end_point_state
  @eps = SegmentEndPoint.all
  respond_to do |format|
    format.html 
  end
end

def index_segment_state
  @segments = Segment.all
  respond_to do |format|
    format.html 
  end
end

def redirect_to_admin
  unless params[:id].empty?
    render :update do |page|
      page.redirect_to :controller => :admin, :action => params[:page], :id => params[:id]
    end
  end
end

protected

def set_session_data(obj, start_time, end_time, report_type)
  start_time = Time.parse(start_time).to_i
  end_time = (Time.parse(end_time) + 24.hours).to_i    
  start_time_ms = start_time.to_i * 1000
  end_time_ms = end_time.to_i * 1000
  
  redis = LKP.redis
  
  stats = obj.stats_data(start_time, end_time)
  if obj.is_a?(BxCosTestVector)
    stats.bx_availability_kludge
  end

  id = redis.incr(REDIS_SESSION_ID)
  redis_data = {}
  
  ignore_sla = false
  if report_type == "Troubleshooting Report" 
    # For troubleshooting always show the frame loss, delay, delay variation even if there is no SLA for the CoS
    ignore_sla = true
  end
  
  # Calculate the alarms and mtc history once and pass using redis
  # as it can take time
  redis_data[:alarms] = obj.alarm_history.subset(start_time_ms, end_time_ms)
  redis_data[:mtc_times] = obj.mtc_history.subset(start_time_ms, end_time_ms)
  redis_data[:unavail_times] = redis_data[:mtc_times] + redis_data[:alarms]
  
  # Get all the data and store it in redis as it takes time to collect 
  # to collect in each contoller.
  supported_stats = stats.get_supported_stats
  
  if supported_stats.include?(:flr)
    # Frame loss
    normalized_data = stats.stats_get_normalized_flr_data(ignore_sla)     
    redis_data[:flr_normalized_data] = normalized_data
  end

  if supported_stats.include?(:delay)
    delay_attributes = stats.get_delay_attributes(:sla)
    if report_type == "Troubleshooting Report"
      delay_attributes += stats.get_delay_attributes(:troubleshooting)
    end

    raw_data = stats.stats_get_delay_data(delay_attributes)
  
    # For each delay statistic compute the results 
    delay_attributes.each do |attribute|
      dv_data = stats.stats_delay_results(raw_data, attribute, ignore_sla)
      redis_data[attribute] = dv_data
    end
  end
  
  redis_marshal = Marshal.dump(redis_data)  
  redis.setex(REDIS_DATA_KEY + "#{id}", REDIS_EXPIRE, redis_marshal)
  return id
end


def set_enni_session_data(obj, start_time, end_time, report_type)
  start_time = Time.parse(start_time).to_i
  end_time = (Time.parse(end_time) + 24.hours).to_i
  start_time_ms = start_time.to_i * 1000
  end_time_ms = end_time.to_i * 1000
  redis = LKP.redis
  
  id = redis.incr(REDIS_SESSION_ID)
  redis_data = {}
  
  stats = obj.stats_data(start_time, end_time)
  
  # Calculate the alarms and mtc history once and pass using redis
  # as it can take time
  redis_data[:alarms] = obj.alarm_history.subset(start_time_ms, end_time_ms)
  redis_data[:mtc_times] = obj.mtc_history.subset(start_time_ms, end_time_ms)
  redis_data[:unavail_times] = redis_data[:mtc_times] + redis_data[:alarms]
  
  supported_stats = stats.get_supported_stats 
  if supported_stats.include?(:traffic)
    # Get all the data and store it in redis as it takes time to collect 
    # to collect in each contoller.
    normalized_data = stats.stats_get_enni_count_data([:octet_counts, :frame_counts])
    redis_data[:octet_counts] = normalized_data  
  end
  
  redis_marshal = Marshal.dump(redis_data)
  redis.setex(REDIS_DATA_KEY + "#{id}", REDIS_EXPIRE, redis_marshal)
  
  return id
end

private
def validate_params(params, entity)
  error_text = ""
  if (params[:obj_id] == "")
     error_text = "No #{entity} specified"
  elsif (params[:start_time] == "") || (params[:end_time] == "" )
     error_text = "Please provide start and end times"
  elsif (Time.parse(params[:start_time]) > Time.parse(params[:end_time]))
     error_text = "Start time must be before the End time"  
  elsif Time.parse(params[:start_time]) > Time.now
     error_text = "Start time cannot be in the future"
  elsif (params[:report_type] == "")
     error_text = "No Report Type specified"
  end
  return error_text
end

# Take the min, max value and adjust by percent
def adjust_min_max(min_val, max_val, percent)
  min_val = min_val - (min_val.abs * percent)/100
  max_val = max_val + (max_val.abs * percent)/100
  return min_val, max_val
end


end
