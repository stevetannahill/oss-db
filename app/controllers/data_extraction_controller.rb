class DataExtractionController < ApplicationController

  def get_metric_information authority, entity_type, entity_id

    response = MetricInformationResponse.new
    response.return_code = 0
    response.entity_type = entity_type
    response.entity_id = entity_id
    response.metric_information_list = []
    entity = nil

    case entity_type
    when 'Segment'
      metrics = {
          'Availability' => {'type' => 'SLA', 'unit' => 'percent'},
          'FrameLossRatio' => {'type' => 'SLA', 'unit' => 'percent'},
          'Delay' => {'type' => 'SLA', 'unit' => 'micro-seconds'},
          'DelayVariation' => {'type' => 'SLA', 'unit' => 'micro-seconds'},
          'AtoZRate' => {'type' => 'Utilization', 'unit' => 'kbps'},
          'ZtoARate' => {'type' => 'Utilization', 'unit' => 'kbps'}
          }
    when 'SegmentEndPoint'
       metrics = {
          'Availability' => {'type' => 'SLA', 'unit' => 'percent'},
          'FrameLossRatio' => {'type' => 'SLA', 'unit' => 'percent'},
          'Delay' => {'type' => 'SLA', 'unit' => 'micro-seconds'},
          'DelayVariation' => {'type' => 'SLA', 'unit' => 'micro-seconds'},
          'AtoZRate' => {'type' => 'Utilization', 'unit' => 'kbps'},
          'ZtoARate' => {'type' => 'Utilization', 'unit' => 'kbps'}
          }
    when 'Demarc'
       metrics = {
          'AtoZRate' => {'type' => 'Utilization', 'unit' => 'kbps'},
          'ZtoARate' => {'type' => 'Utilization', 'unit' => 'kbps'}
          }
    else
      error_string = "No Metrics Available for #{entity_type}"
      response.error_string = error_string
      response.return_code = 1
      return response
    end

    metrics.keys.each do |k|
      mi = MetricInformation.new
      mi.metric_name = "#{k}"
      mi.metric_type = "#{metrics[k]['type']}"
      mi.metric_units = "#{metrics[k]['unit']}"
      response.metric_information_list << mi
    end

    return response

  end

  def get_metric_data authority, entity_type, entity_id, start_time, end_time, time_interval, metric_filter

    metrics = ['Availability','DelayVariation', 'Delay']
    response = MetricDataResponse.new
    response.return_code = 0
    response.entity_type = entity_type
    response.entity_id = entity_id
    response.start_time = "#{start_time}"
    response.end_time = end_time
    response.time_interval = time_interval
    response.metric_result_list = []

    st = start_time.to_i
    et = end_time.to_i

    while st < et do
      mr = MetricResultSet.new
      mr.metric_time_stamp = st
      mr.metric_data_list = []
      metrics.each do |m|
        md = MetricData.new
        md.metric_name = "#{m}"
        case m
          when 'Availability'
            md.metric_value = rand(1)
          when 'DelayVariation'
            md.metric_value = 10 + rand(5)
          when 'Delay'
            md.metric_value = 1000 + rand(500)
        end
        mr.metric_data_list << md
      end
      response.metric_result_list << mr
      st += time_interval.to_i
    end

    return response

  end

  def get_fault_state authority, entity_type, entity_id
    response = FaultStateResponse.new
    response.return_code = 0
    response.entity_type = entity_type
    response.entity_id = entity_id
    response.fault_detected_time = '1292010960'
    response.fault_severity = 'critical'
    response.fault_probable_cause = 'interfaceDown'
    return response
  end

end
