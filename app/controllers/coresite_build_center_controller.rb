class CoresiteBuildCenterController < ApplicationController 
  
  layout "admin"
  
  def index
    # Show Failed by default
    @builder_logs = CoresiteBuilderLog.where("summary like ? AND name like ?", "Failed", "%").order('updated_at DESC')        
    @summary = get_summary
    @cid_filter = ""
    @summary_filter = "Failed"
    @paths = Path.all.sort_by {|p| p.cenx_name}
    @nodes = Node.where(:network_manager_id => NetworkManager.find_by_name("CoreSite SolarWinds").id)
    @config_options = ["Stop on Errors", "Ignore Errors"]
  end
  
  def edit_builder_log
    @builder_log = CoresiteBuilderLog.find(params[:id])
  end
  
  def auto_update
    @summary = get_summary
    render :partial => 'update'
  end
  
  def destroy_builder_log
    @builder_log = CoresiteBuilderLog.find(params[:id]).destroy

    respond_to do |format|
      format.html { redirect_to(:action => :index) }
    end
  end
  
  def abc_to_csv
    result_filter = params[:result]
    result_filter = "%" if result_filter.blank?
    outfile = CoresiteBuilderLog.csv(result_filter)
    
    send_data(outfile,
       :filename => "automated_build_center_results.csv",
       :type => "application/text",
       :dispostion => 'attachment')
  end  
  
  def abc_filter
    @builder_logs = get_logs(params[:summary_filter], params[:cid_filter])

    render :partial => 'abc_results'      
  end

  def upload_tenants
    file = params[:tenants_file]
    if file.nil?
      flash[:notice] = "Please specify a Tenants file for upload"
      respond_to do |format|
        format.html { redirect_to(:action => :index) }
      end
    else
      flash[:notice] = "Tenants file uploaded"
      data = file.read
      @output = StringIO.new
      @results = CoresiteBuildCenter.import_tenants(data, @output)
      @output = @output.string
    end
  end
  
  
  def del_config
    @path = Path.find_by_id(params[:path])
    @result = "Select a Path"
    if @path
      bc = CoresiteBuildCenter.new
      bl = bc.delete_router_configuration(@path, params[:ignore_errors] == "Ignore Errors")
      @result = bl.details
    end
    render :text => @result, :layout => false    
  end
  
  def router_config
    @path = Path.find_by_id(params[:path])
    @result = "Select a Path"
    if @path
      bc = CoresiteBuildCenter.new
      bl = bc.configure_router(@path, params[:ignore_errors] == "Ignore Errors")
      @result = bl.details
    end
    render :text => @result, :layout => false    
  end
  
  def execute_commands
    @node = Node.find_by_id(params[:node])
    @result = "Select a Node"
    if @node
      bc = CoresiteBuildCenter.new
      bl = bc.run_router_commands(@node.name, params[:cmds], params[:ignore_errors] == "Ignore Errors")
      @result = bl.details
    end
    render :text => @result, :layout => false
  end
  
  def clear_fallout
    @path = Path.find_by_id(params[:path])
    @result = "Select a Path"
    if @path
      bc = CoresiteBuildCenter.new
      bl = bc.clear_fallout(@path)
      @result = bl.details
    end
    render :text => @result, :layout => false    
  end
  
  def coresite_tools
    respond_to do |format|
      format.html 
    end
  end
  
  def test_coresite_oss
    @result = CoresiteBuildCenter.test_oss
    render :text => @result, :layout => false
  end
  
  def check_site
    @site = Site.find_by_id(params[:site])
    if @site
      @node_counts, @error_info = CoresiteBuildCenter.site_check(@site)
    end
    render :partial => 'site_info'    
  end     
  
  private
  
  def get_summary
    total_summary = CoresiteBuilderLog.summary
    summary = Hash.new{|h,k| h[k]=Hash.new{|h1,k1| h1[k1]=Hash.new(0)}}    
    total_summary.each {|result, count| summary[result]["Total"] = count}
    ["hour", "day", "week", "month"].each do |period|
      sub_summary = CoresiteBuilderLog.summary(false, Time.now-1.send(period))
      summary.keys.each {|result| summary[result][period.capitalize] = sub_summary[result]}
    end
    return summary
  end

  def get_logs summary_filter = "*", cid_filter = "*"
    summary_filter = "*" if summary_filter.blank?
    cid_filter = "*" if cid_filter.blank?
    summary_filter.gsub!(/\*/, "%")
    cid_filter.gsub!(/\*/, "%")

    CoresiteBuilderLog.where("summary like ? AND name like ?", summary_filter, cid_filter).order('updated_at DESC')
  end
end