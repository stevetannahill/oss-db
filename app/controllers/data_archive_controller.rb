#
# Data Archive Status and Summary
#
class DataArchiveController < ApplicationController

  layout "admin"

  def index

    results = DataArchiveIf.analyze_shards("spirent_archive", "ethframedelaytest")
    if !results
      @shard_count = 'error'
      @empty_shard = 'error'
      @empty_shard_count = 'error'
      @empty_shard_space = 'error'
      @partial_shards = {'error' => nil}
      @partial_shards_count = 'error'
      @total_space = 'error'
      @da_grid_ids = ['error']
      @da_grid_id_count = 'error'
    else
      @shard_count = results[:shard_count]
      @empty_shard = results[:first_empty]
      @empty_shard_count = results[:count_empty]
      @empty_shard_space = add_commas(results[:space_empty])
      @partial_shards = results[:partial_shards]
      @da_grid_ids = results[:gid_list]

      @partial_shards_count = @partial_shards.size
      if @partial_shards == {}
        @partial_shards = {'All Shards Are Full' => nil}
      end

      @total_space = 0
      @partial_shards.each_value {|value| @total_space += value}
      @total_space = add_commas(@total_space)

      @da_grid_id_count = @da_grid_ids.size
      if @da_grid_ids == []
        @da_grid_ids = ['No Circuits']
      end
      @da_grid_ids.sort!
    end

    @connections = DataArchiveIf.count_connections
    if !@connections
      @connections = 'error'
    end

    @process_list = DataArchiveIf.list_processes
    if !@process_list
      @process_list = [{'Id' =>'error'}]
    end

    results = DataArchiveIf.slow_query_status
    if !results
        @slow_queries = 'erorr'
        @slow_logging = 'erorr'
        @slow_log = 'erorr'
        @slow_time = 'erorr'
    else
      if !@slow_queries = results[:count]
        @slow_queries = 'erorr'
      end

      if !@slow_logging = results[:status]
        @slow_logging = 'erorr'
      end
      
      if !@slow_log = results[:file]
        @slow_log = 'erorr'
      end
      
      if !@slow_time = results[:time]
        @slow_time = 'erorr'
      end
    end

    @cleaner_timestamp = 'Under Construction'
  end

  def _da_status
    da_status
    render partial: 'da_status'
  end

  def da_status
    @config_hash = get_config_hash unless @config_hash
    da_deamon_hash = @config_hash[:data_archive]

    @statuses = get_da_status(da_deamon_hash)
    @da_partition = nil
    @da_size = nil

    da_hash = @config_hash[:databases][:da]
    if da_hash[:host] and !da_hash[:host].empty?
      Net::SSH.start(da_hash[:host], da_hash[:user], :password => da_hash[:pswd]) do |ssh|
        ssh.exec!("cd /")
        @da_partition = ssh.exec!("df -h #{da_hash[:path]}") 
        @da_size = ssh.exec!("du -sh #{da_hash[:path]}") 
      end
    else
      @da_partition = `df -h #{da_hash[:path]}`
      @da_size = `du -sh #{da_hash[:path]}`
    end

    temp_lines = @da_partition.split("\n")
    temp_lines.each_with_index do |line, index|
      if index == 0
        @da_partition = [line, ""]
      else
        @da_partition[1] = @da_partition[1] + " " + line
      end
    end

    if !@da_size || @da_size.empty?
      @da_size = "Database Not Found: #{da_hash[:host]} #{da_hash[:path]}"
    elsif @da_size =~ /^120K/ ||  @cdb_size =~ /^12K/ # size of folder without permissions
      @da_size = "#{da_hash[:user]} does not have permission to read from this directory #{da_hash[:path]}"
    end

    cdb_hash = @config_hash[:databases][:cdb]
    @cdb_partition = nil
    @cdb_size = nil
    if cdb_hash[:host] and !cdb_hash[:host].empty?
      Net::SSH.start(cdb_hash[:host], cdb_hash[:user], :password => cdb_hash[:pswd]) do |ssh|
        ssh.exec!("cd /")
        @cdb_partition = ssh.exec!("df -h #{cdb_hash[:path]}") 
        @cdb_size = ssh.exec!("du -sh #{cdb_hash[:path]}") 
      end
    else
      @cdb_partition = `df -h #{cdb_hash[:path]}`
      @cdb_size = `du -sh #{cdb_hash[:path]}`
    end
    
    temp_lines = @cdb_partition.split("\n")
    temp_lines.each_with_index do |line, index|
      if index == 0
        @cdb_partition = [line, ""]
      else
        @cdb_partition[1] = @cdb_partition[1] + " " + line
      end
    end

    if !@cdb_size || @cdb_size.empty?
      @cdb_size = "Database Not Found: #{cdb_hash[:host]} #{cdb_hash[:path]}"
    elsif @cdb_size =~ /^120K/ ||  @cdb_size =~ /^12K/ # size of folder without permissions
      @cdb_size = "#{cdb_hash[:user]} does not have permission to read from this directory #{cdb_hash[:path]}"
    end
        

    @newest_entry = DataArchiveIf.newest("spirent_archive", "ethframedelaytest")
    if @newest_entry
      if @newest_entry == 0
          @newest_entry = 'No Circuits Found'
      else 
        @newest_entry = Time.at(@newest_entry).to_s(:long)
      end
    else
      @newest_entry = 'error'
    end

    @log = da_deamon_hash[:log]
    @server = da_deamon_hash[:host]
    @newest_data = nil
    if da_deamon_hash[:host] and !da_deamon_hash[:host].empty?
      Net::SSH.start(da_deamon_hash[:host], da_deamon_hash[:user], :password => da_deamon_hash[:pswd]) do |ssh|
        @newest_data = ssh.exec!("tail -n #{da_deamon_hash[:num_lines]} #{da_deamon_hash[:log]}")
      end
    else
      @newest_data = `tail -n #{da_deamon_hash[:num_lines]} #{da_deamon_hash[:log]}`
      @newest_data = "Log File Not Found or Empty Log: #{da_deamon_hash[:log]}" if @newest_data.empty?
    end
  end

  def search_grid_id
    gid = params[:gid]
    gid.strip!

    gid_info = nil
    if gid.to_i.to_s == gid # insure the search is on an integer
      gid_info = {gid: gid.to_i}
      curcuit = DataArchiveIf.get_curcuit("spirent_archive", "ethframedelaytest", gid)

      if curcuit
        gid_info.merge!({ record_start: curcuit[:record_start_epoch], record_end: curcuit[:record_end_epoch] })
      end
    end

    render :update do |page|
      page.replace :gid_info, :partial => 'gid_info', :locals => { :gid_info => gid_info }
    end
  end

  private

  def get_config_hash
    config_file = File.expand_path("../../../config/summary_page_ssh.json",__FILE__)
    JSON.load(File.open(config_file)).with_indifferent_access
  end

  def get_da_status(config_hash)
    output = nil
    if config_hash[:host] and !config_hash[:host].empty?
      Net::SSH.start(config_hash[:host], config_hash[:user], :password => config_hash[:pswd]) do |ssh|
        output = ssh.exec!("monit status")
      end
    else
      output = `monit status`
    end

    found_status = false
    status = []
    if output
      output.split("\n").each do |line|
        if line =~ /^[A-Z].*'.*'/
          if found_status
            break # stop recording
          elsif line == "Process 'data_archive'"
            found_status = true # start recording
          end
        end
        status << line if found_status
      end
    else
      if !config_hash[:host] or config_hash[:host].empty?
        host = "local machine"
      else
        host = config_hash[:host]
      end
      status = ["Monit Not Running on: #{host}"]
    end
    return [status]
  end

  def add_commas(int)
    result = []
    begin
      segment = int % 1000
      int = (int / 1000).floor
      result << segment 
      result << ','
    end while int != 0
    result.pop
    result.reverse!
    result = result.join('')
  end
end
