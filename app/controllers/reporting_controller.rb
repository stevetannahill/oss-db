
class ReportingController < ApplicationController
  def home
  end

  def index
    respond_to do |format|
      format.html { redirect_to(:action => :home) }
    end
  end
  
  #
  # Schedule Management
  #
  def index_schedule
    @srs = Schedule.all
    respond_to do |format|
      format.html
    end
  end

  def edit_schedule
    @sr = Schedule.find(params[:id])
    Time.zone = @sr.timezone
    @reports = @sr.reports
    respond_to do |format|
      format.html
    end
  end

  def update_schedule
    @sr = Schedule.find(params[:id])
    @reports = @sr.reports
    Time.zone = params[:sr][:timezone]
    
    respond_to do |format|
      formats = params[:sr][:formats] ? params[:sr][:formats] : {}
      fm = @sr.formats
      unchecked = fm.keys - formats.keys
      unchecked.each {|uc| fm[uc] = false}
      formats.each {|k,v| fm[k] = v}
      params[:sr][:formats] = fm
      if @sr.update_attributes(params[:sr])   
        format.html { redirect_to(:action => :edit_schedule, :id => @sr.id )  }
      else
        format.html { render :action => 'edit_schedule' }
      end
    end
  end

  def new_schedule
    @sr = Schedule.new
    respond_to do |format|
      format.html
    end
  end

  def save_schedule
    @sr = Schedule.new(params[:sr])
    @reports = @sr.reports
    respond_to do |format|
      if @sr.save
        format.html { redirect_to(:action => :edit_schedule, :id => @sr.id) }
      else
        flash[:error] = "Failed to save #{@sr.errors.inspect}\n#{@sr.inspect}"        
        format.html { render :action => 'new_schedule' }
      end
    end
  end

  def destroy_schedule
    @sr = Schedule.find(params[:id])
    @reports = @sr.reports
    @sr.destroy
    respond_to do |format|
      format.html { redirect_to(:action => "index_schedule") }
    end
  end
  
  def run_now_schedule
    @sr = Schedule.find(params[:id])
    @sr.run_now
    respond_to do |format|
      format.html { redirect_to(:action => :edit_schedule, :id => @sr.id )  }
    end
  end
    

  #
  # Report Management
  #

  def edit_report
    @report = Report.find(params[:id])
    Time.zone = @report.schedule.timezone
    respond_to do |format|
      format.html
    end
  end

  def update_report
    @report = Report.find(params[:id])
    Time.zone = @report.schedule.timezone
    respond_to do |format|
      if @report.update_attributes(params[:report])
        format.html { redirect_to(:action => :edit_report, :id => @report.id )  }
      else
        format.html { render :action => 'edit_report' }
      end
    end
  end

  def destroy_report
    @report = Report.find(params[:id])
    @sr = @report.schedule
    @report.destroy
    respond_to do |format|
      format.html { redirect_to(:action => "edit_schedule", :id => @sr.id) }
    end
  end

  #
  # Scheduled Task Management
  #
  def index_st
    ScheduledAudit if Rails.env == "development" # For some reason finding the class hierarchy does not work well in development mode
    @sts = ScheduledTask.all
    respond_to do |format|
      format.html
    end
  end

  def edit_st
    @st = ScheduledTask.find(params[:id])
    @sched_reports = @st.schedules
    respond_to do |format|
      format.html
    end
  end

  def update_st
    filter_opts = params[:st][:filter_options]
    params[:st].delete(:filter_options)
    params[:st].delete(:type)
    @st = ScheduledTask.find(params[:id])
    @st.executable_script = params[:executable_script]
    
    @st.filter_options = JSON.parse(filter_opts) if !filter_opts.blank?
    
    respond_to do |format|
      if @st.update_attributes(params[:st])
        format.html { redirect_to(:action => :edit_st, :id => @st.id )  }
      else
        format.html { render :action => 'edit_st' }
      end
    end
  end

  def new_st
    @st = params[:type].constantize.new
    respond_to do |format|
      format.html
    end
  end

  def save_st
    filter_opts = params[:st][:filter_options]
    params[:st].delete(:filter_options)
    # If a ScheduledTask is created and the type then changed to FilterTemplate the validations for FilterTemplate are not run
    if params[:st][:type].blank?
      object_to_create = ScheduledTask
    else
      object_to_create = params[:st][:type].constantize
    end
    params[:st].delete(:type)
    @st = object_to_create.new(params[:st])
    @st.executable_script = params[:executable_script]
    @st.filter_options = JSON.parse(filter_opts) if !filter_opts.blank?

    respond_to do |format|
      if @st.save
        format.html { redirect_to(:action => :edit_st, :id => @st.id) }
      else
        format.html { render :action => 'new_st' }
      end
    end
  end

  def destroy_st
    @st = ScheduledTask.find(params[:id])
    @st.destroy
    respond_to do |format|
      format.html { redirect_to(:action => "index_st") }
    end
  end

end
