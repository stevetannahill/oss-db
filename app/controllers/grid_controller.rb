class GridController < ApplicationController 
  
  layout "admin"

  def index
    begin

      @out_limit = 40
      @start_date = (Time.now - 1.month).strftime("%b %d, %Y")

      @polling = cmd("poll running?")
      @poll_cmds = cmd("poll")
      @poll_duration = cmd("poll how-often")

      @sources = cmd("source")
      @targets = get_targets
      @archives = cmd("archive")
      @registries = cmd("register")
      @grid_api =  cmd("help Grid::GridRegistry")
      @gci_api =  cmd("help Grid::GCIRegistry")

      @versions = cmd("version").collect { |d| d[:data] }
      @big_t = cmd("T")
      @redis = cmd("redis")
    rescue Errno::ECONNREFUSED => e
      render action: 'connection_refused', locals: {error: "Redis is not running!"}
    end
  end

  def _grid_status
    @grid_info = GridTools.get_grid_info
    @statuses = get_grid_monit

    @history = cmd("tail -v 100").reverse
    regex_cases = [/^appid/, /^exit/, /^version/, /^archive name/, /^target/, /^source/]
    @history.select!{ |hist| regex_cases.none?{|reg| reg =~ hist[:cmd]} }
    @history = @history[0..9]

    get_logs
    render partial: "grid_status"
  end

  def control_grid
    render :partial => 'control_grid', locals: { grid_info: GridTools.get_grid_info }
  end

  def pause_grid
    GridTools.stop_grid
    control_grid
  end
  
  def resume_grid
    GridTools.start_grid
    control_grid
  end

  def history
    num_lines = params[:num_lines]
    #is it a positive int string
    if num_lines.to_i.to_s == num_lines && num_lines.to_i >= 1
      num_lines = num_lines.to_i
    else
      num_lines = 100
    end
    @history = cmd("tail -v #{num_lines}").reverse
    respond_to do |format|
      format.html
    end
  end

  def circuit_info
    circuit = params[:circuit]
    circuit.strip!

    circuit_info = nil
    circuit_info = GridTools.circuit_info(circuit) unless circuit.empty?

    render :update do |page|
      page.replace :circuit_info, :partial => 'circuit_info', :locals => { :circuit_info => circuit_info }
    end
  end

  def grid_audit_report
    @titles = GridAuditReport::SECTION_TITLES
    @report_data = {}
    report = GridAuditReport.new
    report.output_limit = params[:out_limit].to_i
    @report_data[:grid_audit_valid] = get_grid_audit_valid(report)
    @report_data[:grid_audit_abc] = get_grid_audit_abc(report)
    @report_data[:grid_audit_da] = get_grid_audit_da(report)
    @report_data[:grid_audit_lkp] = get_grid_audit_lkp(report)
    @report_data[:grid_audit_samples] = get_grid_audit_samples(report)
    @report_data[:grid_audit_avail] = get_grid_audit_avail(report, params[:start_date]) if params[:availability]
  end

  def grid_audit_valid
    report = GridAuditReport.new
    report.output_limit = params[:out_limit].to_i
    render action: '_grid_audit_partial', locals: { title: GridAuditReport::SECTION_TITLES[:grid_audit_valid], results: get_grid_audit_valid(report)}
  end

  def grid_audit_abc
    report = GridAuditReport.new
    report.output_limit = params[:out_limit].to_i
    render action: '_grid_audit_partial', locals: { title: GridAuditReport::SECTION_TITLES[:grid_audit_abc], results: get_grid_audit_abc(report)}
  end

  def grid_audit_da
    report = GridAuditReport.new
    report.output_limit = params[:out_limit].to_i
    render action: '_grid_audit_partial', locals: { title: GridAuditReport::SECTION_TITLES[:grid_audit_da], results: get_grid_audit_da(report)}
  end

  def grid_audit_lkp
    report = GridAuditReport.new
    report.output_limit = params[:out_limit].to_i
    render action: '_grid_audit_partial', locals: { title: GridAuditReport::SECTION_TITLES[:grid_audit_lkp], results: get_grid_audit_lkp(report)}
  end

  def grid_audit_samples
    report = GridAuditReport.new
    report.output_limit = params[:out_limit].to_i
    render action: '_grid_audit_partial', locals: { title: GridAuditReport::SECTION_TITLES[:grid_audit_samples], results: get_grid_audit_samples(report)}
  end

  def grid_audit_avail
    report = GridAuditReport.new
    report.output_limit = params[:out_limit].to_i
    render action: '_grid_audit_partial', locals: { title: GridAuditReport::SECTION_TITLES[:grid_audit_avail], results: get_grid_audit_avail(report, params[:start_date])}
  end

  private

  def cmd(msg)
    grid_app.cmd(msg,true,false)[:data]
  end

  def get_config_hash
    config_file = File.expand_path("../../../config/summary_page_ssh.json",__FILE__)
    JSON.load(File.open(config_file)).with_indifferent_access[:grid]
  end

  def get_targets
    @config_hash = get_config_hash unless @config_hash

    results = []
    targets = cmd("target")

    targets.each_with_index do |data,index|
      result = {}
      result[:path], result[:storage] = data
      result[:filename] = GridApp.cmd("target format #{index}")
      result[:storage_name] = GridApp.cmd("target name #{index}") || "#{index}"
      result[:target_format] = []
      file_contents = ""

      if @config_hash[:host] and !@config_hash[:host].empty?
        result[:host] = @config_hash[:host]
        if result[:filename]
          Net::SSH.start(@config_hash[:host], @config_hash[:user], :password => @config_hash[:pswd]) do |ssh|
            file_contents = ssh.exec!("cat #{result[:filename]}")
          end
        end
      else
        result[:host] = "local machine"
        if result[:filename]
          file_contents = `cat #{result[:filename]}`
        end
      end

      if file_contents && file_contents != "cat: #{result[:filename]}: No such file or directory" && !file_contents.empty?
        CSV.parse(file_contents) do |data_row|
          result[:target_format] << data_row
        end
      elsif result[:filename]
        result[:FNF] = true
      end
      results << result
    end
    results
  end

  def get_grid_monit
    @config_hash = get_config_hash unless @config_hash

    output = nil
    if @config_hash[:host] and !@config_hash[:host].empty?
      Net::SSH.start(@config_hash[:host], @config_hash[:user], :password => @config_hash[:pswd]) do |ssh|
        output = ssh.exec!("monit status")
      end
    else
      output = `monit status`
    end

    found_status = false
    status = []
    if output
      output.split("\n").each do |line|
        if line =~ /^[A-Z].*'.*'/
          if found_status
            break # stop recording
          elsif line == "Process 'grid_polling'"
            found_status = true # start recording
          end
        end
        status << line if found_status
      end
    else
      if !@config_hash[:host] or @config_hash[:host].empty?
        host = "local machine"
      else
        host = @config_hash[:host]
      end
      status = ["Monit Not Running on: #{host}"]
    end
    return [status]
  end

  def get_logs
    @config_hash = get_config_hash unless @config_hash
    @log = @config_hash[:log]
    @server = @config_hash[:host]
    @polling_logs = nil
    if @config_hash[:host] and !@config_hash[:host].empty?
      Net::SSH.start(@config_hash[:host], @config_hash[:user], :password => @config_hash[:pswd]) do |ssh|
        @polling_logs = ssh.exec!("tail -n #{@config_hash[:num_lines]} #{@config_hash[:log]}")
      end
    else
      @polling_logs = `tail -n #{@config_hash[:num_lines]} #{@config_hash[:log]}`
      @polling_logs = "Log File Not Found or Empty Log: #{@config_hash[:log]}" if @polling_logs.empty?
    end

    log_lines = @polling_logs.split("\n")
    entry_starts = [/^Looking/, /^CALLING/, /^\s*--\s*downloaded/, /^Target/, /^Sending the following message/, /^Sleeping/,
      /^bye/, /^eve/, /^WARNING/, /^Log File Not Found/]
    entry = ""
    @polling_logs = []
    log_lines.each do |line|
      if entry_starts.any?{ |start| line =~ start }
        @polling_logs << entry + "\n" unless entry.empty? #push complete entry
        entry = line #start new entry
      else
        entry = entry + "\n" + line unless entry.empty?
      end
    end
    @polling_logs << entry + "\n" unless entry.empty?
    @polling_logs.reverse!
  end

  def get_grid_audit_valid(report)
    report.results = []
    report.check_id_validity
    return report.results unless report.results.empty?
    "All circuit/grid IDs were found to be valid."
  end

  def get_grid_audit_abc(report)
    report.results = []
    report.check_grid_vs_build_logs
    return report.results unless report.results.empty?
    "There was no discrepancy found between Grid and ABC build logs."
  end
  
  def get_grid_audit_da(report)
    report.results = []
    report.check_grid_vs_da
    return report.results unless report.results.empty?
    "There was no discrepancy found between Grid and DA."
  end
  
  def get_grid_audit_lkp(report)
    report.results = []
    report.check_grid_vs_lkp
    return report.results unless report.results.empty?
    "There was no discrepancy found between Grid and LKP."
  end
  
  def get_grid_audit_samples(report)
    report.results = []
    report.check_samples_lost
    return report.results unless report.results.empty?
    "there were no circuits found with missing samples."
  end
  
  def get_grid_audit_avail(report, start_date)
    report.results = []
    begin
      start = Time.parse(start_date)
    rescue
      start = Time.now - 1.month
    end
    report.check_for_mismatches(start)
    return report.results unless report.results.empty?
    "No availability mismatches detected."
  end
end
