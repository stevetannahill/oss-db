class AuditController < ApplicationController 
  
  layout "admin"

  def index
    begin
      @raw_files = cmd(cmaudit_app,"cm ls-raw-files 50").reverse!
      @audit_exec = cmd(cmaudit_app,"eve exec?")

      @audit_queues = {
        "Audit Upcoming Tasks" => cmd(cmaudit_app,"sub ls audit.q"),
      }
      # # @extra_queue_names = cmd(cmaudit_app,"cm-meta fallout-queues audit.q")
      @extra_queue_names = [ "audit.q.newformats", "audit.q.dupfile", "audit.q.unknowndate", "audit.q.noresults"]
      @extra_queue_names.each do |qname|
        @audit_queues["Audit Fallout (#{qname})"] = cmd(cmaudit_app,"sub ls #{qname}")
      end
      @audit_processed = cmd(cmaudit_app,"sub ls-processed audit.q 20")
      @audit_failed = cmd(cmaudit_app,"sub ls-failed audit.q")

      @sources = cmd(grid_audit_app, "source")
      @archives = cmd(grid_audit_app, "archive")

      @polling = cmd(grid_audit_app, "poll running?")
      @poll_cmds = cmd(grid_audit_app, "poll")
      @poll_duration = cmd(grid_audit_app, "poll how-often")

      cmaudit_app.cmd("cm-meta audit")
      @cm_dir = cmaudit_app.cmd("cm dir")[:data]
      @audit_target = Cmaudit::Validator.audit_target 
      @format_paths = cmaudit_app.cmd("cm-meta format-paths")[:data]
      @registries = cmaudit_app.cmd("register")[:data]
      @redis = cmd(cmaudit_app, "redis")
    rescue Errno::ECONNREFUSED => e
      render action: 'connection_refused', locals: {error: "Redis is not running!"}
    end
  end

  def history
    num_lines = params[:num_lines].to_i
    num_lines = 100 if num_lines == 0
    @history = get_history(num_lines)
    respond_to do |format|
      format.html
    end
  end

  def _audit_status
    @history = get_history(10)
    render partial: "audit_status"
  end  

  def cmd(app,msg)
    app.cmd(msg,true,false)[:data]
  end

  def get_history(size)
    history = [ {  :cmd => "----- FILE TRANSFER HISTORY -----", :duration => "", :started => "" } ] + get_history_part(grid_audit_app,size) 
    history += [ {  :cmd => "----- AUDIT HISTORY -----", :duration => "", :started => "" } ] + get_history_part(cmaudit_app,size) 
    history
  end

  def get_history_part(app,size)
    history = cmd(app,"tail -v #{size}").reverse
    regex_cases = [/^appid/, /^exit/, /^version/, /^archive name/, /^target/, /^source/, /^shell/, /^help/]
    history.select!{ |hist| regex_cases.none?{|reg| reg =~ hist[:cmd]} }
    history = history[0..size]
  end

end