class CircuitHistoryController < ApplicationController 
  
  layout "admin"

  def index
    @all_cascades = osl_app.cmd("cm get")[:data]
  end
  
  def show
    @cascade = params[:id]
    @cascade_data = osl_app.cmd("cm get #{@cascade}")[:data]
    @cascade_data = @cascade_data[@cascade] unless @cascade_data.nil?
  end
    

end