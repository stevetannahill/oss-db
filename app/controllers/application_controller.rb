# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.
class ApplicationController < ActionController::Base

  if $cas_server_enabled then
    before_filter CASClient::Frameworks::Rails::Filter
  elsif $coresite_authentication
    before_filter CoreSite::Authentication::Filter
  end

  helper :all # include all helpers, all the time
  protect_from_forgery # See ActionController::RequestForgeryProtection for details
  before_filter :set_current_user
  before_filter :authorize_admin_user, :except => :logout
  before_filter :appstats_page_counter
  
  rescue_from CanCan::AccessDenied do |exception|
    render file: "#{Rails.root}/public/403", formats: [:html], status: 403, layout: false
  end

  def current_user
    @current_user
  end
  
  def current_user= user
    @current_user = user
  end

  # Required for declarative authorization
  def set_current_user
    if $cas_server_enabled || $coresite_authentication
      @current_user = User.find_by_email(session['cas_user'])
    else
      ["admin@sprint.com","admin@verizon.com","admin@ericcson.com","admin@cenx.com"].each do |default|
        session['cas_user'] = default
        @current_user = User.find_by_email(session['cas_user'])
        break unless @current_user.nil?
      end
    end

    if @current_user
      if @current_user.service_provider.nil?
        # Make sure there is a service provider for this user in cdb db
        return redirect_to('/500.html')
      else
        session['service_provider_id'] = @current_user.service_provider.id
      end
    elsif $cas_server_enabled
      # Removed invalid reference to @current_user.homepage_url(request), as @current_user is nil
      CASClient::Frameworks::Rails::Filter.logout(self)
    end
  end

  def authorize_admin_user
    authorize! :manage, :admin_user
  end

  # Common Code for searching CDB tables
  def table_from_type(type)
    table = CDBInfo::CLASS_SHORT_NAMES.invert[type.to_s]
    return table ? table.constantize : nil
  end

  def search_table
    table = table_from_type(params[:fcn_tail])
    attrs = params[:attrs] ? params[:attrs] : []
    details = params[:details] ? params[:details] : []
    if table
      @items = params[:items].collect{|id| table.find(id)}
    else
      @items = []
    end
    if(params[:search] && params[:search] != "")
      @matchnodes = []
      @items.each{|item|
        if(params[:local_link] && item.read_attribute(params[:local_link]).to_s.downcase.include?(params[:search].downcase))
          @matchnodes.push(item)
        elsif(params[:detail_link] && item.send(params[:detail_link]).to_s.downcase.include?(params[:search].downcase))
          @matchnodes.push(item)
        else
          push = false
          for a in attrs
            if (item.read_attribute(a) && item.read_attribute(a).to_s.downcase.include?(params[:search].downcase))
              push = true
              break
            end
          end
          if !push
            for d in details
              if (item.send(d) && item.send(d).to_s.downcase.include?(params[:search].downcase))
                push = true
                break
              end
            end
          end
          @matchnodes.push(item) if push
        end
      }
    else
      @matchnodes = @items
    end
    methods = (params[:methods] ? params[:methods] : {})
    confirm_methods = (params[:confirm_methods] ? params[:confirm_methods] : {})
    params[:legend].gsub!(/\+/, " ")
    params[:cols].each{|col| col.gsub!(/\+/, " ")}
    render( :partial => 'admin/list_any_type', :object => @matchnodes,
      :locals => {
  			:legend => params[:legend],
  			:cols => params[:cols],
  			:fcn_tail => params[:fcn_tail],
  			:local_link => params[:local_link],
  			:detail_link => params[:detail_link],
  			:attrs => attrs,
  			:details => details,
  			:methods => methods,
  			:confirm_methods => confirm_methods,
  			:search => params[:search],
  			:enable_search => params[:enable_search],
  			:enable_delete => params[:enable_delete],
  			:delete_on => params[:delete_on]
  			} )
  end
  
  def order_app
    @order_app = Eve::Application.new(:config => "#{File.dirname(__FILE__)}/../../config/osl.order.yml") if @order_app.nil?
    @order_app
  end
  
  def osl_app
    @osl_app = Eve::Application.new(:config => "#{File.dirname(__FILE__)}/../../config/osl.poe.yml") if @osl_app.nil?
    @osl_app
  end

  def cmaudit_app
    @cmaudit_app = Eve::Application.new(:config => "#{File.dirname(__FILE__)}/../../config/cmaudit.yml") if @cmaudit_app.nil?
    @cmaudit_app
  end

  def grid_audit_app
    if @grid_audit_app.nil?
      @grid_audit_app = Eve::Application.new(:config => "#{File.dirname(__FILE__)}/../../config/grid.audit.yml", :observer => true)
      GridApp.eve = @grid_audit_app
    end
    @grid_audit_app
  end

  def grid_app
    if @grid_app.nil?
      @grid_app = Eve::Application.new(:config => "#{File.dirname(__FILE__)}/../../config/grid.yml", :observer => true)
      GridApp.eve = @grid_app
    end
    @grid_app
  end
  
  def logout
    if $cas_server_enabled
      # Clean up CAS client and redirect to CAS logout page
      CASClient::Frameworks::Rails::Filter.logout(self, @current_user.homepage_url(request))
    elsif $coresite_authentication
      CoreSite::Authentication::Filter.remove_session_data(self)
      redirect_to root_path
    else
      redirect_to root_path
    end
  end
end
