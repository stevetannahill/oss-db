#
#LKP Summary Dashboard
#
class LkpController < ApplicationController

  layout "admin"

  def index
    logFile = ""
    
    @config_hash = get_config_hash unless @config_hash

    @lkp_redis = LKP.redis
    @database_size = @lkp_redis.info["used_memory_human"]

    @log = @config_hash[:log]
    @server = @config_hash[:host]

    if @config_hash[:host] and !@config_hash[:host].empty?
      Net::SSH.start(@config_hash[:host], @config_hash[:user], :password => @config_hash[:pswd]) do |ssh|
        logFile = ssh.exec!("cat #{@config_hash[:log]}")
      end
    else
      logFile = `cat #{@config_hash[:log]}`
    end

    # local(`cmd`) comands return "" on error net/ssh commands return the error itself
    if logFile != "cat: #{@config_hash[:log]}: No such file or directory" and !logFile.empty?
      @filelist = []
      lines = logFile.split("\n").map do |line|
        if result = /I, \[(.*)\ #.+INFO -- : Processing csv (.*)/.match(line)
          result
        else
          /I, \[(.*)\ #.+INFO -- : Done processing (.*)/.match(line)
        end
      end
      lines.compact!
      if lines.size > (2 * @config_hash[:num_lines])
        lines = lines[-(2 * @config_hash[:num_lines]) .. -1]
      end
      started = false
      i = 0
      @directory = ''
      @directory = File.dirname(lines[0][2]) if !lines.empty?
      lines.each do |line|
        if !started
          # start new file
          @filelist[i] = [File.basename(line[2]), Time.parse(line[1])]
        else
          if @filelist[i][0] == File.basename(line[2])
            @filelist[i][2] =  Time.parse(line[1])
            i += 1
          else # files are mismatched (should only occur if the oldest file does not have it's start time)
            @filelist[i][2] =  @filelist[i][1] # no end time available
            i += 1
            # start new file
            started = !started
            @filelist[i] = [File.basename(line[2]), Time.parse(line[1])]
          end
        end
        started = !started
      end
      if started # most recent file is not done processing
        @filelist[i][2] = Time.now
      end

      @filelist.reverse!
      @filelist << ['No Files Read', Time.now, Time.now] if @filelist.empty?
    else
      @filelist = [["Log file is either empty or does not exist: #{@config_hash[:log]}", Time.now, Time.now]]
    end
    
    begin
      @error_data = { "Monthly" => {}, "Weekly" => {} }
      @error_data["Monthly"]["Hour"] = MetricsMonthlyCosTestVector.where("severity != 'clear'").where(:time_declared_epoch => ((Time.now - 1.hours).to_i .. Time.now.to_i)).count
      @error_data["Monthly"]["Day"] = MetricsMonthlyCosTestVector.where("severity != 'clear'").where(:time_declared_epoch => ((Time.now - 1.days).to_i .. Time.now.to_i)).count
      @error_data["Monthly"]["Week"] = MetricsMonthlyCosTestVector.where("severity != 'clear'").where(:time_declared_epoch => ((Time.now - 1.weeks).to_i .. Time.now.to_i)).count
      @error_data["Monthly"]["Month"] = MetricsMonthlyCosTestVector.where("severity != 'clear'").where(:time_declared_epoch => ((Time.now - 1.months).to_i .. Time.now.to_i)).count
      @error_data["Monthly"]["Total"] = MetricsMonthlyCosTestVector.where("severity != 'clear'").count

      @error_data["Weekly"]["Hour"] = MetricsWeeklyCosTestVector.where("severity != 'clear'").where(:time_declared_epoch => ((Time.now - 1.hours).to_i .. Time.now.to_i)).count
      @error_data["Weekly"]["Day"] = MetricsWeeklyCosTestVector.where("severity != 'clear'").where(:time_declared_epoch => ((Time.now - 1.days).to_i .. Time.now.to_i)).count
      @error_data["Weekly"]["Week"] = MetricsWeeklyCosTestVector.where("severity != 'clear'").where(:time_declared_epoch => ((Time.now - 1.weeks).to_i .. Time.now.to_i)).count
      @error_data["Weekly"]["Month"] = MetricsWeeklyCosTestVector.where("severity != 'clear'").where(:time_declared_epoch => ((Time.now - 1.months).to_i .. Time.now.to_i)).count
      @error_data["Weekly"]["Total"] = MetricsWeeklyCosTestVector.where("severity != 'clear'").count

      @times_W = @lkp_redis.keys("lkp:*:W:*:tracking").collect{|key| key.split(":")[3].to_i}
      @count_W = @times_W.select{|time_W| time_W >= (Time.now - 1.weeks).to_i}.count

      @times_M = @lkp_redis.keys("lkp:*:M:*:tracking").collect{|key| key.split(":")[3].to_i}
      @count_M = @times_M.select{|time_M| time_M >= (Time.now - 1.months).to_i}.count

      @total_circuits = CosTestVector.count

      @flr_threshold = (LKP::Circuit::AVAILABILITY_FLR_THRESHOLD * 100).round(2)
      @availability_threshold = LKP::Circuit::AVAILABILITY_SAMPLES
    rescue
      @error_count_H = "Error" if !defined?(@error_count_H) || !@error_count_H
      @error_count_D = "Error" if !defined?(@error_count_D) || !@error_count_D
      @error_count_W = "Error" if !defined?(@error_count_W) || !@error_count_W
      @error_count_M = "Error" if !defined?(@error_count_M) || !@error_count_M
      @error_count_T = "Error" if !defined?(@error_count_T) || !@error_count_T

      @count_M = "Error" if !defined?(@count_M) || !@count_M

      @count_W = "Error" if !defined?(@count_W) || !@count_W

      @total_circuits = "Error" if !defined?(@total_circuits) || !@total_circuits

      @flr_threshold = "Error" if !defined?(@flr_threshold) || !@flr_threshold
      @availability_threshold = "Error" if !defined?(@availability_threshold) || !@availability_threshold
    end

    @log_summary = []
    # local(`cmd`) comands return "" on error net/ssh commands return the error itself
    if logFile != "cat: #{@config_hash[:log]}: No such file or directory" and !logFile.empty?
      lines = logFile.split("\n").select{|line| line =~ /^[WE]/}
      if lines.size > @config_hash[:num_lines]
        lines = lines[-@config_hash[:num_lines] .. -1]
      end
      lines.each do |line|
        @log_summary << line
      end
      @log_summary.reverse!
    else
      @log_summary.push('Log file is either empty or does not exist:')
      @log_summary.push(@config_hash[:log])
    end
  end

  def _lkp_status
    lkp_status
    render partial: 'lkp_status'
  end

  def lkp_status
    @lkp_redis = LKP.redis unless @lkp_redis

    @config_hash = get_config_hash unless @config_hash
    @statuses = get_lkp_statuses(@config_hash).reverse

    @back_logs = [0]
    if @statuses.count != 1
      # status countains 1 array for each worker + 1 array for prism
      (1 ... @statuses.count).each do |i| 
        @back_logs[i] = @lkp_redis.llen("lkp:#{i}:to_be_processed")
        @back_logs[i] = @back_logs[i]
        @back_logs[0] += @back_logs[i]
      end
    else 
      # monit is not running just give total for all workers
      @lkp_redis.keys("lkp:*:to_be_processed").each do |key|
        @back_logs[0] += @lkp_redis.llen(key)
      end
    end
  end

  private

  def get_config_hash
    config_file = File.expand_path("../../../config/summary_page_ssh.json",__FILE__)
    JSON.load(File.open(config_file)).with_indifferent_access[:lkp]
  end

  def get_lkp_statuses config_hash
    output = nil
    if config_hash[:host] and !config_hash[:host].empty?
      Net::SSH.start(config_hash[:host], config_hash[:user], :password => config_hash[:pswd]) do |ssh|
        output = ssh.exec!("monit status")
      end
    else
      output = `monit status`
    end

    statuses = []
    if output
      status = []
      output.split("\n").each do |line|
        if line =~ /^[A-Z].*'.*'/
          if status[0] == "Process 'prism'" or status[0] =~ /Process 'processor_\d+'/ 
            statuses << status # save recorded status
          end
          status = [] # start new recording
        end
        status << line
      end
    else
      if !config_hash[:host] or config_hash[:host].empty?
        host = "local machine"
      else
        host = config_hash[:host]
      end
      statuses << ["Monit Not Running on: #{host}"]
    end
    return statuses
  end
end
