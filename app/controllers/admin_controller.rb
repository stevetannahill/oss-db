require "cenx_id_generator.rb"
require "cenx_name_generator.rb"

class AdminController < ApplicationController

def home
end

def index
  respond_to do |format|
    format.html { redirect_to(:action => :home) }
  end
end

def backup_db
  command = "rake db:extract_fixtures"
  system command
  
  flash[:notice] = "Database backed up OK"  
  respond_to do |format|
    format.html { redirect_to(:action => :home) }
    format.xml  { head :ok }
  end 
end

#
# AccessPermission Management
#

# def index_ap
#   @access_permissions = AccessPermission.all

#   respond_to do |format|
#     format.html # index_ap.html.erb
#     format.xml  { render :xml => @access_permissions }
#   end
# end

def new_ap
  @operator_network = OperatorNetwork.find(params[:id])
  @access_permission = AccessPermission.new(:operator_network => @operator_network)
  @targets = @operator_network.valid_access_permission_targets
  @target_id = nil
  respond_to do |format|
    format.html # new_ap.html.erb
    format.xml  { render :xml => @access_permission }
  end
end

def save_ap
  @access_permission = AccessPermission.new(params[:access_permission])
  @operator_network = OperatorNetwork.find(params[:access_permission][:operator_network_id])

  respond_to do |format|
    if @access_permission.save
      flash[:notice] = "Cenx AccessPermission: #{@access_permission.name} created OK" 
      format.html { redirect_to(:action => :edit_on, :id => @operator_network.id) }
    else
      @targets = @operator_network.valid_access_permission_targets
      @target_id = params[:access_permission][:target_id]
      format.html { render :action => 'new_ap' }
    end
  end
end

def edit_ap
  @access_permission = AccessPermission.find(params[:id])
  @operator_network = @access_permission.operator_network
  @targets = @operator_network.valid_access_permission_targets
  @target_id = @access_permission.target.id
end    

def update_ap
  @access_permission = AccessPermission.find(params[:id])
  @operator_network = OperatorNetwork.find(params[:access_permission][:operator_network_id])
  
  respond_to do |format|
    if @access_permission.update_attributes(params[:access_permission])
      flash[:notice] = "AccessPermission: #{@access_permission.name} updated OK"
      format.html { redirect_to(:action => :edit_ap, :id => @access_permission.id)  }
    else
      @targets = @operator_network.valid_access_permission_targets
      @target_id = @access_permission.target.id
      format.html { render :action => 'edit_ap' }
    end
  end
end

def destroy_ap
  @access_permission = AccessPermission.find(params[:id])
  @access_permission.destroy

  respond_to do |format|
    format.html { redirect_to(:action => :index_ap) }
  end
end

#
# Network Managemer Management
#

def index_nm
  @nms = NetworkManager.all

  respond_to do |format|
    format.html # index_nm.html.erb
    format.xml  { render :xml => @nms }
  end
end

def new_nm
  @nm = NetworkManager.new
  
  respond_to do |format|
    format.html # new_nm.html.erb
    format.xml  { render :xml => @nm }
  end
end

def save_nm
  @nm = NetworkManager.new(params[:nm])

  respond_to do |format|
    if @nm.save
      flash[:notice] = "Network Managemer: #{@nm.name} created OK" 
      format.html { redirect_to(:action => :edit_nm, :id => @nm.id) }
    else
      format.html { render :action => 'new_nm' }
    end
  end
end

def edit_nm
  @nm = NetworkManager.find(params[:id])
  @nm_servers = @nm.network_management_servers
  @nodes = @nm.nodes
end

def update_nm
  @nm = NetworkManager.find(params[:id])
  respond_to do |format|
    if @nm.update_attributes(params[:nm])
      flash[:notice] = "Network Manager: #{@nm.name} updated OK"
      format.html { redirect_to(:action => :edit_nm, :id => @nm.id)  }
    else
      format.html { render :action => 'edit_nm' }
      @nm_servers = @nm.network_management_servers
      @nodes = @nm.nodes
    end
  end
end

def destroy_nm
  @nm = NetworkManager.find(params[:id])
  @nm.destroy

  respond_to do |format|
    format.html { redirect_to(:action => :index_nm) }
  end
end

#
# Network Management Server Management
#

def new_nm_server
  @nm_server = NetworkManagementServer.new
  @nm = NetworkManager.find(params[:id])
  @nm_server.network_manager = @nm
  
  respond_to do |format|
    format.html # new_nm_server.html.erb
    format.xml  { render :xml => @nm_server }
  end
end

def save_nm_server
  @nm_server = NetworkManagementServer.new(params[:nm_server])
  @nm = NetworkManager.find(params[:id])
  @nm_server.network_manager = @nm

  respond_to do |format|
    if @nm_server.save
      flash[:notice] = "Network Management Server: #{@nm_server.name} created OK" 
      format.html { redirect_to(:action => :edit_nm_server, :id => @nm_server.id) }
    else
      format.html { render :action => 'new_nm_server' }
    end
  end
end

def edit_nm_server
  @nm_server = NetworkManagementServer.find(params[:id])
end

def update_nm_server
  @nm_server = NetworkManagementServer.find(params[:id])
  
  respond_to do |format|
    if @nm_server.update_attributes(params[:nm_server])
      flash[:notice] = "Network Management Server: #{@nm_server.name} updated OK"
      format.html { redirect_to(:action => :edit_nm_server, :id => @nm_server.id)  }
    else
      format.html { render :action => 'edit_nm_server' }
    end
  end
end

def destroy_nm_server
  @nm_server = NetworkManagementServer.find(params[:id])
  @nm_id = @nm_server.network_manager_id
  @nm_server.destroy

  respond_to do |format|
    format.html { redirect_to(:action => :edit_nm, :id => @nm_id) }
  end
end
    
#
# SiteGroup Management
#

def index_site_group
  @site_groups = SiteGroup.find_rows_for_index

  respond_to do |format|
    format.html # index_site_group.html.erb
    format.xml  { render :xml => @site_groups }
  end
end

def new_site_group
  @site_group = SiteGroup.new
  @sites = []
  respond_to do |format|
    format.html # new_site_group.html.erb
    format.xml  { render :xml => @site_group }
  end
end

def save_site_group
  @site_group = Kernel.const_get(params[:class]).new(params[:site_group])

  respond_to do |format|
    if @site_group.save
      flash[:notice] = "Cenx SiteGroup: #{@site_group.name} created OK" 
      format.html { redirect_to(:action => :index_site_group) }
    else
      format.html { render :action => 'new_site_group' }
    end
  end
end

def edit_site_group
  @site_group = SiteGroup.find(params[:id])
  @sites = @site_group.sites
#  @nodes = @site_group.nodes
#  @ennins = @site_group.demarcs
#  @onovcs = @site_group.on_net_ovcs
#  @qosps = @site_group.qos_policies
end    

def update_site_group
  @site_group = SiteGroup.find(params[:id])
  #@qosps = @site_group.qos_policies
  
  respond_to do |format|
    if @site_group.update_attributes(params[:site_group])
      flash[:notice] = "SiteGroup: #{@site_group.name} updated OK"
      format.html { redirect_to(:action => :edit_site_group, :id => @site_group.id)  }
    else
      @sites = @site_group.sites
      #@nodes = @site_group.nodes
      #@ennins = @site_group.demarcs
      #@onovcs = @site_group.on_net_ovcs
      format.html { render :action => 'edit_site_group' }
    end
  end
end

def destroy_site_group
  @site_group = SiteGroup.find(params[:id])
  @site_group.destroy

  respond_to do |format|
    format.html { redirect_to(:action => :index_site_group) }
  end
end

#
# Site Management
#

def index_site
  @sites = Site.find_rows_for_index
  @descendants = Site.descendants.map{ |d| d.to_s }

  respond_to do |format|
    format.html 
    format.xml  { render :xml => @sites }
  end
end

def new_site
  @site = params[:site][:type].constantize.new
  
  @ons = @site.site_groups.collect {|sg| sg.sites.collect {|site| site.operator_network}}.flatten.uniq 
  if @ons.empty?
    @ons = OperatorNetwork.find_cenx_operator_networks
  end
  
  respond_to do |format|
    format.html # new_site.html.erb
  end
end

def save_site
  @site = Kernel.const_get(params[:class]).new(params[:site])
  @ons = @site.site_groups.collect {|sg| sg.sites.collect {|site| site.operator_network}}.flatten.uniq 
  if @ons.empty?
    @ons = OperatorNetwork.find_cenx_operator_networks
  end
  
  respond_to do |format|
    if @site.save
      flash[:notice] = "Site: #{@site.name} created OK" 
      format.html { redirect_to(:action => :edit_site, :id => @site.id) }
    else
      format.html { render :action => 'new_site', :site => {:type => @site.type} }
    end
  end
end

def edit_site
  @site = Site.find(params[:id])
  @ons = @site.site_groups.collect {|sg| sg.sites.collect {|site| site.operator_network}}.flatten.uniq 
  if @ons.empty?
    @ons = OperatorNetwork.find_cenx_operator_networks
  end
  
  @cabs = @site.cabinets
  @nodes = @site.nodes
  @iis = @site.inventory_items
  @pps = @site.patch_panels
  @ports = @site.ports
  @ufs = @site.uploaded_files
end
  
def rename_nodes_site
  @site = Site.find(params[:id])
  new_prefix = params[:new_prefix]
  if @site.rename_nodes new_prefix
    flash[:notice] = "Nodes on #{@site.name} renamed with prefix #{new_prefix}"
  else
    flash[:error] = "Prefix #{new_prefix} can not be used as it is in use."
  end
  redirect_to(:action => :edit_site, :id => @site.id)
end

def seed_all_site
  @site = Site.find(params[:id])
  @site.seed
  flash[:notice] = "All Site #{@site.name} connected OK"
  redirect_to(:action => :edit_site, :id => @site.id)
end

def connect_gen_site
  @site = Site.find(params[:id])
    @connect_text = @site.generate_connection_info
    @lines = @connect_text.count '\n'
    respond_to do |format|
      format.html # connect_gen_site.html.erb
    end
  end

def update_site
  @site = Site.find(params[:id])
  
  respond_to do |format|
    if @site.update_attributes(params[:site])
      flash[:notice] = "SiteGroup Locaton: #{@site.name} updated OK"
      format.html { redirect_to(:action => :edit_site, :id => @site.id )  }
    else
      if(@site_group.sites.any? {|site| site.operator_network_id})
        @ons = @site_group.sites.collect {|site| site.operator_network}.compact
      else
        @ons = OperatorNetwork.find_cenx_networks_w_no_sites_attached
      end
      @cabs = @site.cabinets
      @nodes = @site.nodes
      @iis = @site.inventory_items
      @pps = @site.patch_panels
      @ports = @site.ports
      @ufs = @site.uploaded_files
      format.html { render :action => 'edit_site' }
    end
  end
end

def destroy_site
  @site = Site.find(params[:id])
  #@site_groupid = @site.site_group_id
  @site.destroy

  respond_to do |format|
      format.html { redirect_to(:action => :index_site) }
  end
end

#
# Inventory Management
#

def index_ii
  @iis = InventoryItem.find_rows_for_index
  @sites = Site.all

  respond_to do |format|
    format.html # index_site_group.html.erb
  end
end
  
def filter_site_ii
  if(params[:id] && params[:id] != "")
    @site = Site.find(params[:id])
    @iis = @site.inventory_items
  else
    @site = nil
    @iis = InventoryItem.find_rows_for_index
  end
  render :partial => 'index_filter_ii'
end

def new_ii
  @ii = InventoryItem.new
  @site = Site.find(params[:id])
  @ii.site_id = @site.id
  @sites = [];
  
  respond_to do |format|
    format.html # new_ii.html.erb
  end
end

def save_ii
  @ii = InventoryItem.new(params[:ii])
  @site = Site.find(params[:id])
  @ii.site_id = @site.id
  @sites = [];

  respond_to do |format|
    if @ii.save
      flash[:notice] = "Patch Panel: #{@ii.description} created OK"
      format.html { redirect_to(:action => :edit_site, :id => @site.id) }
    else
      format.html { render :action => 'new_ii' }
    end
  end
end

  def new_ii_from_node
    @node = Node.find(params[:id])
    message = ""
    if @node.cabinet
      @ii = InventoryItem.new
      @ii.set_from_node @node
    else
      message = ", node does not have a cabinet."
    end
    respond_to do |format|
      if @ii && @ii.save
        flash[:notice] = "Inventory Item: #{@ii.description} created OK from #{@node.name}"
        format.html { redirect_to(:action => :edit_ii, :id => @ii.id) }
      else
        flash[:error] = "Not able to create Inventory Item from #{@node.name}#{message}"
        format.html { redirect_to(:action => :edit_site, :id => @node.site_id) }
      end
    end
  end

  def edit_ii
    @ii = InventoryItem.find(params[:id])
    @sites = Site.all
  end

  def update_ii
    @ii = InventoryItem.find(params[:id])

    respond_to do |format|
      if @ii.update_attributes(params[:ii])
        flash[:notice] = "Inventory Item #{@ii.description} updated OK"
        format.html { redirect_to(:action => :edit_site, :id => @ii.site_id )  }
      else
        @sites = Site.all
        format.html { render :action => 'edit_ii' }
      end
    end
  end

  def destroy_ii
    @ii = InventoryItem.find(params[:id])
    @siteid = @ii.site_id
    @ii.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :edit_site, :id => @siteid) }
    end
  end
#
# Cabinet Management
#
def new_cab
  @cab = Cabinet.new
  @site = Site.find(params[:id])

  respond_to do |format|
    format.html # new_cab.html.erb
  end
end

def save_cab
  @cab = Cabinet.new(params[:cab])
  @site = Site.find(params[:id])
  @cab.site_id = @site.id

  respond_to do |format|
    if @cab.save
      flash[:notice] = "Cabinet: #{@cab.name} created OK"
      format.html { redirect_to(:action => :edit_site, :id => @site.id) }
    else
      format.html { render :action => 'new_cab' }
    end
  end
end

  def edit_cab
    @cab = Cabinet.find(params[:id])
    @nodes = @cab.nodes
  end

  def update_cab
    @cab = Cabinet.find(params[:id])
    @siteid = @cab.site_id

    respond_to do |format|
      if @cab.update_attributes(params[:cab])
        flash[:notice] = "Cabinet #{@cab.name} updated OK"
        format.html { redirect_to(:action => :edit_site, :id => @siteid )  }
      else
        @nodes = @cab.nodes
        format.html { render :action => 'edit_cab' }
      end
    end
  end

def destroy_cab
    @cab = Cabinet.find(params[:id])
    @siteid = @cab.site_id
    @cab.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :edit_site, :id => @siteid) }
    end
end

#
# Patch Panel Management
#
def new_pp
  @pp = PatchPanel.new
  @site = Site.find(params[:id])
  
  respond_to do |format|
    format.html # new_pp.html.erb
  end
end

def save_pp
  @pp = PatchPanel.new(params[:pp])
  @site = Site.find(params[:id])
  @pp.site_id = @site.id

  respond_to do |format|
    if @pp.save
      flash[:notice] = "Patch Panel: #{@pp.name} created OK" 
      format.html { redirect_to(:action => :edit_site, :id => @site.id) }
    else
      format.html { render :action => 'new_pp' }
    end
  end
end

  def edit_pp
    @pp = PatchPanel.find(params[:id])
  end

  def update_pp
    @pp = PatchPanel.find(params[:id])
    @siteid = @pp.site_id

    respond_to do |format|
      if @pp.update_attributes(params[:pp])
        flash[:notice] = "Patch Panel #{@pp.name} updated OK"
        format.html { redirect_to(:action => :edit_site, :id => @siteid )  }
      else
        format.html { render :action => 'edit_pp' }
      end
    end
  end

def destroy_pp
    @pp = PatchPanel.find(params[:id])
    @siteid = @pp.site_id
    @pp.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :edit_site, :id => @siteid) }
    end
end

#
# Node Management
#
  def index_node
    @nodes = Node.find_rows_for_index
    @sites = Site.all
    respond_to do |format|
      format.html # index_node.html.erb
    end
  end
  
  def filter_site_node
    if(params[:id] && params[:id] != "")
      @site = Site.find(params[:id])
      @nodes = @site.nodes
    else
      @site = nil
      @nodes = Node.find_rows_for_index
    end
    render :partial => 'index_filter_node'
  end

  def new_node
    @node = Node.new
    @site = Site.find(params[:id])
    @node.site_id = @site.id
    @nms = @node.get_candidate_network_managers
    @cabs = @site.cabinets
    #@node.set_defaults
    
    respond_to do |format|
      format.html # new_node.html.erb
      format.xml  { render :xml => @node }
    end
  end
  
  def options_nm_node
    @node = Node.where("id = ?", params[:id]).first
    @node = Node.new if @node.nil?
    @node.node_type = params[:node_type]
    @nms = @node.get_candidate_network_managers
    render :partial => 'options_nm_node'
  end
  
  def options_ip_name_node
    @node = Node.where("id = ?", params[:id]).first
    @node = Node.new if @node.nil?
    @node.node_type = params[:node_type]
    @node.site_id = params[:site_id]
    #@node.set_defaults
    render :partial => 'options_ip_name_node'
  end
  
  def save_node
    @node = Node.new(form_to_params(params, "node", ["id"]))
    @site = Site.find(params[:id])
    @node.site_id = @site.id

    respond_to do |format|
      if @node.save
        flash[:notice] = "Node: #{@node.name} created OK" 
        format.html { redirect_to(:action => :edit_node, :id => @node.id) }
        format.xml  { render :xml => @node, :status => :created, :location => @node }
      else
        @nms = @node.get_candidate_network_managers
        @cabs = @site.cabinets
        format.html { render :action => 'new_node' }
        format.xml  { render :xml => @node.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit_node
    @node = Node.find(params[:id])
    @nms = @node.get_candidate_network_managers
    @cabs = @node.site.cabinets
    @ports = @node.ports
    @ehs = @node.mtc_periods
    @states = @node.statefuls
  end

  def update_node
    @node = Node.find(params[:id])
    respond_to do |format|
      if @node.update_attributes(form_to_params(params, "node", ["id"]))
        flash[:notice] = "Node: #{@node.name} updated OK"
        format.html { redirect_to(:action => :edit_node, :id => @node.id )  }
      else
        @nms = @node.get_candidate_network_managers
        @cabs = @node.site.cabinets
        @ports = @node.ports
        @ehs = @node.mtc_periods
        @states = @node.statefuls
        format.html { render :action => 'edit_node' }
      end
    end
  end
  
  def seed_ports_node
    @node = Node.find(params[:id])
    @node.seed_ports
    @ports = @node.ports(true)
    render(:partial => 'list_any_type', :object => @ports,
      :locals => {
				:legend => 'Ports',
				:fcn_tail => 'port',
				:cols => %w{name phy\ type strands slot/port color_code connected_to},
                                :local_link => 'name',
                                :detail_link => nil,
				:attrs => %w{phy_type strands patch_panel_slot_port color_code },
				:details => %w{ all_connection_info_port}
			})
  end

  def destroy_node
    @node = Node.find(params[:id])
    @siteid = @node.site_id
    @node.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :edit_site, :id => @siteid) }
    end
  end 
  
#
# Port Management
#
def index_port

  #@ports = Port.paginate :page => params[:page] || 1
  #@port_pages, @ports = paginate(:ports, :order => 'node_id')
  @ports = Port.order("node_id, #{Port::STANDARD_ORDER}")
  @sites = Site.all
  @nodes = []
  respond_to do |format|
    format.html # index_port.html.erb
  end
end

def filter_site_port
  @site = Site.find(params[:id])
  @ports = @site.ports
  #@ports = Port.paginate @site.ports, :page => params[:page] || 1
  @nodes = @site.nodes
  render :partial => 'index_filter_port'
end

def filter_node_port
  @node = Node.find(params[:id])
  @ports = @node.ports
  #@ports = Port.paginate_by_node_id @node.id, :page => params[:page] || 1
  @nodes = @node.site.nodes
  render :partial => 'index_filter_port'
end


def new_port
  @port = Port.new
  @node = Node.find(params[:id])
  @port.node_id = @node.id
  @pps = @node.site.patch_panels
  
  respond_to do |format|
    format.html # new_port.html.erb
  end
end

def save_port
  @port = Port.new(params[:port])
  @node = Node.find(params[:id])
  @port.node_id = @node.id

  respond_to do |format|
    if @port.save
      flash[:notice] = "Port: #{@port.name} created OK on Node #{@node.name}" 
      @pps = @node.site.patch_panels
      @ufs = @port.uploaded_files
      format.html { redirect_to(:action => :edit_port, :id => @port.id) }
    else
      @pps = @node.site.patch_panels
      format.html { render :action => 'new_port' }
    end
  end
end

def edit_port
  @port = Port.find(params[:id])
  @node = Node.find(@port.node_id)
  @pps = @node.site.patch_panels
  @ufs = @port.uploaded_files
end

def update_port
  @port = Port.find(params[:id])
  @node = Node.find(@port.node_id)
  @pps = @node.site.patch_panels
  @ufs = @port.uploaded_files
  respond_to do |format|
    if @port.update_attributes(params[:port])
      flash[:notice] = "Port: #{@port.name} on #{@node.name} updated OK"
      format.html { redirect_to(:action => :edit_port, :id => @port.id) }
    else
      format.html { render :action => 'edit_port' }
    end
  end
end

def choose_port_port
    @port = Port.find(params[:id])
    @sites = @port.get_valid_site_list
    @nodes = []
    @ports = []
    respond_to do |format|
      format.html # choose_port_port.html.erb
    end
end

def link_port_port
  @port = Port.find(params[:id])

  if (!params[:port])||(params[:port][:id] == "")
    @sites = @port.get_valid_site_list
    @nodes = []
    @ports = []
    render_action_id_with_errors(
       @port, 'choose_port_port', @port.id,
       "Port info is invalid - try again" )
    return
  end

  @other_port = Port.find(params[:port][:id])

  if @other_port.is_connected_to_port
    @sites = @port.get_valid_site_list
    @nodes = []
    @ports = []
    render_action_id_with_errors(
       @port, 'choose_port_port', @port.id,
       "Port: #{@other_port.name} is already connected to #{@other_port.port_connection_info_port} - try again" )
    return
  end

  @other_port.connected_port_id = @port.id
  @port.connected_port_id = @other_port.id
  #TODO use transaction
  respond_to do |format|
    if @port.save
      @other_port.save
      flash[:notice] = "Port: #{@port.name} connected OK to Port: #{@other_port.name}"
      format.html { redirect_to(:action => 'edit_port', :id => @port.id ) }
    else
      flash[:error] = "Port: #{@port.name} connect failed to Port: #{@other_port.name}"
      @node = Node.find(@port.node_id)
      @pps = @node.site.patch_panels
      @ufs = @port.uploaded_files
      format.html { render :action => 'edit_port' }
    end
  end
end

def unlink_port_port
  @port = Port.find(params[:id])
  @other_port = @port.connected_port
  @port.connected_port_id = nil
  @other_port.connected_port_id = nil
  #TODO use transaction
  respond_to do |format|
    if @port.save
       @other_port.save
       flash[:notice] = "Port: #{@port.name} disconnected OK from Port #{@other_port.name}"
       format.html { redirect_to(:action => 'edit_port', :id => @port.id ) }
    else
      flash[:error] = "Port: #{@port.name} remove failed for from Port #{@other_port.name}"
      @node = Node.find(@port.node_id)
      @pps = @node.site.patch_panels
      @ufs = @port.uploaded_files
      format.html { render :action => 'edit_port' }
    end
  end
end

def choose_demarc_port
  @port = Port.find(params[:id])
  @demarcs = @port.get_candidate_demarcs
  #@ennis = Enni.where("site_group_id = ?", @port.node.site_group.id)
  respond_to do |format|
      format.html # choose_demarc_port.html.erb
  end
end

def link_demarc_port
  @port = Port.find(params[:id])

  if (!params[:demarc])||(params[:demarc][:id] == "")
    @demarcs = @port.get_candidate_demarcs
    render_action_id_with_errors(
       @port, 'choose_demarc_port', @port.id,
       "Enni info is invalid - try again" )
    return
  end

  @demarc = Demarc.find(params[:demarc][:id])

  if ! @demarc.new_port_can_be_linked
    @demarcs = @port.get_candidate_demarcs
    render_action_id_with_errors(
       @port, 'choose_demarc_port', @port.id,
       "#{@demarc.cenx_name} cannot be linked to any more ports - try again" )
    return
  end

  @port.demarc_id = @demarc.id
  respond_to do |format|
    if @port.save
      flash[:notice] = "Port: #{@port.name} linked OK to #{@demarc.cenx_name}"
      format.html { redirect_to(:action => 'edit_port', :id => @port.id ) }
    else
      flash[:error] = "Port: #{@port.name} link failed for #{@demarc.cenx_name}"
      @node = Node.find(@port.node_id)
      @pps = @node.site.patch_panels
      @ufs = @port.uploaded_files
      format.html { render :action => 'edit_port' }
    end
  end
end

def unlink_demarc_port
  @port = Port.find(params[:id])
  @demarc = Demarc.find(@port.demarc_id)
  @port.demarc_id = nil
  respond_to do |format|
    if @port.save
       flash[:notice] = "Port: #{@port.name} unlinked OK from #{@demarc.cenx_name}"
       format.html { redirect_to(:action => 'edit_port', :id => @port.id ) }
    else
      flash[:error] = "Port: #{@port.name} unlink failed for #{@demarc.cenx_name}"
      @node = Node.find(@port.node_id)
      @pps = @node.site.patch_panels
      @ufs = @port.uploaded_files
      format.html { render :action => 'edit_port' }
    end
  end
end

def destroy_port
  @port = Port.find(params[:id])
  @nodeid = @port.node_id
  if @port.is_connected_to_port
    @connected_port = Port.find(@port.connected_port_id)
    @connected_port.connected_port_id=nil
    @connected_port.save
  end
  @port.destroy

  respond_to do |format|
    format.html { redirect_to(:action => :edit_node, :id => @nodeid) }
  end
end

def get_port_mac_info
  @port = Port.find(params[:id])
  result_ok, mac = @port.get_mac_address
  @port.mac = mac

  if result_ok
    flash[:notice] = "Retrieved: mac = #{mac} - OK"
  else
    flash[:error] = "Retrieved: mac = #{mac} - FAILED"
  end
  render :partial => 'options_port_mac'
end


#
# Service Provider Management
#

  def index_sp
    @sps = ServiceProvider.find_rows_for_index

    respond_to do |format|
      format.html # index_sp.html.erb
      format.xml  { render :xml => @sps }
    end
  end
  
  def new_sp
    @sp = ServiceProvider.new
    @sp.cenx_id = "To Be Determined"
    @sp.cenx_cma = "To Be Determined"
    
    respond_to do |format|
      format.html # new_sp.html.erb
      format.xml  { render :xml => @sp }
    end
  end
  def recompute_sla_exceptions_with_lkp
    # MetricsCosTestVectors.delete_all
    CosTestVector.full_lkp_refresh
    respond_to do |format|
      format.html { redirect_to(:action => :home) }
    end
  end
  def recompute_utilization_exceptions
    ExceptionUtilizationEnni.delete_all
    ExceptionUtilizationCosEndPoint.delete_all
    SlaExceptionGenerator.compute_enni_utilization_exceptions Time.utc(Time.now.utc.year,1,1), Time.now.utc
    SlaExceptionGenerator.compute_cos_end_point_utilization_exceptions Time.utc(Time.now.utc.year,1,1), Time.now.utc
    respond_to do |format|
      format.html { redirect_to(:action => :home) }
    end
  end

  def save_sp
    @sp = ServiceProvider.new(params[:sp])

    respond_to do |format|
      if @sp.save
        flash[:notice] = "Service Provider: #{@sp.name} created OK" 
        format.html { redirect_to(:action => :index_sp) }
        format.xml  { render :xml => @sp, :status => :created, :location => @sp }
      else
        format.html { render :action => "new_sp" }
        format.xml  { render :xml => @sp.errors, :status => :unprocessable_entity }
      end
    end
  end   
  
  def edit_sp
    @sp = ServiceProvider.find(params[:id])
    @ons = @sp.operator_networks
    @onts = @sp.operator_network_types
    @paths = @sp.paths
    @contacts = @sp.contacts
    @ufs = @sp.uploaded_files
    @ennis = @sp.enni_news
    @ennis_on_behalf_of = @sp.ennis_on_behalf_of
    @users = @sp.users
  end    
  
  def update_sp
    @sp = ServiceProvider.find(params[:id])
    
    respond_to do |format|
      if @sp.update_attributes(params[:sp])
        flash[:notice] = "Service Provider: #{@sp.name} updated OK"
        format.html { redirect_to(:action => :index_sp)  }
        format.xml  { head :ok }
      else
        @ons = @sp.operator_networks
        @onts = @sp.operator_network_types
        @paths = @sp.paths
        @contacts = @sp.contacts
        @ufs = @sp.uploaded_files
        @ennis = @sp.enni_news
        @ennis_on_behalf_of = @sp.ennis_on_behalf_of
        @users = @sp.users
        format.html { render :action => 'edit_sp' }
        format.xml  { render :xml => @sp.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy_sp
    @sp = ServiceProvider.find(params[:id])
    @sp.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :index_sp) }
      format.xml  { head :ok }
    end
  end
  
  
  # 
  # User Service Provider Management
  # 

  def new_user_service_provider
    @sp = ServiceProvider.find(params[:id])
    @user = User.new(:service_provider_id => @sp.id)
    @users = User.order('email')
    respond_to do |format|
      format.html # new_user.html.erb
    end    
  end
  
  def save_user_service_provider
    @user = User.find(params[:user][:id])
    @sp = ServiceProvider.find(params[:id])
    @user.service_provider = @sp

    respond_to do |format|
      if @user.save
        flash[:notice] = "User sucessfully added"
        format.html { redirect_to(:action => :edit_sp, :id => @sp.id) }
      else
        format.html { render :action => 'new_user_service_provider' }
      end
    end
  end

  def destroy_user_service_provider
    @user = User.find(params[:id])
    @spid = @user.service_provider_id
    @user.service_provider_id = nil
    @user.save
    
    respond_to do |format|
      format.html { redirect_to(:action => :edit_sp, :id => @spid) }
    end
  end

  
  #
  # Contact Management
  #
  def new_contact
    @contact = Contact.new
    @sp = ServiceProvider.find(params[:id])
    respond_to do |format|
      format.html # new_contact.html.erb
    end
  end
  def save_contact
    @contact = Contact.new(params[:contact])
    @sp = ServiceProvider.find(params[:id])
    @contact.service_provider_id = @sp.id

    respond_to do |format|
      if @contact.save
        flash[:notice] = "Contact: #{@contact.name} created OK"
        format.html { redirect_to(:action => :edit_sp, :id => @sp.id) }
      else
        format.html { render :action => 'new_contact' }
      end
    end
  end

  def edit_contact
    @contact = Contact.find(params[:id])
  end

  def update_contact
    @contact = Contact.find(params[:id])

    respond_to do |format|
      if @contact.update_attributes(params[:contact])
        flash[:notice] = "Contact: #{@contact.name} updated OK"
        format.html { redirect_to(:action => :edit_contact, :id => @contact.id )  }
      else
        format.html { render :action => 'fcontact' }
      end
    end
  end

  def destroy_contact
    @contact = Contact.find(params[:id])
    @spid = @contact.service_provider_id
    @contact.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :edit_sp, :id => @spid) }
    end
  end
  #
  # Operator Network Type Management
  #  
  def index_ont
    @onts = OperatorNetworkType.all

    respond_to do |format|
      format.html # index_ont.html.erb
    end
  end
  
  def new_ont
    @ont = OperatorNetworkType.new
    @sp = ServiceProvider.find(params[:id])
    @ont.service_provider_id = @sp.id
    respond_to do |format|
      format.html # new_ont.html.erb
    end
  end
  
  def save_ont
    @ont = OperatorNetworkType.new(params[:ont])
    @sp = ServiceProvider.find(params[:id])
    @ont.service_provider_id = @sp.id
    
    respond_to do |format|
      if @ont.save
        flash[:notice] = "Operator Network Type: #{@ont.name} created OK" 
        format.html { redirect_to(:action => :edit_sp, :id => @sp.id) }
      else
        format.html { render :action => 'new_ont' }
      end
    end
  end
  
  def edit_ont
    @ont = OperatorNetworkType.find(params[:id])
    @ossis = @ont.oss_infos
    @costs = @ont.class_of_service_types
    @dts = @ont.demarc_types
    @pathts = @ont.path_types
    @ests = @ont.ethernet_service_types
    @segmentts = @ont.segment_types
    @segmentepts = @ont.segment_end_point_types
    @ots = @ont.order_types
  end    
  
  def update_ont
    @ont = OperatorNetworkType.find(params[:id])

    respond_to do |format|
      if @ont.update_attributes(params[:ont])
        flash[:notice] = "Operator Network Type: #{@ont.name} updated OK"
        format.html { redirect_to(:action => :edit_sp, :id => @ont.service_provider_id )  }
      else
        @ossis = @ont.oss_infos
        @costs = @ont.class_of_service_types
        @dts = @ont.demarc_types
        @pathts = @ont.path_types
        @ests = @ont.ethernet_service_types
        @segmentts = @ont.segment_types
        @segmentepts = @ont.segment_end_point_types
        @ots = @ont.order_types
        format.html { render :action => 'edit_ont' }
      end
    end
  end
  
  def destroy_ont
    @ont = OperatorNetworkType.find(params[:id])
    @spid = @ont.service_provider_id
    @ont.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :edit_sp, :id => @spid) }
    end
  end
  
  #
  # Operator Network Management
  #  
  def index_on
    @ons = OperatorNetwork.find_member_operator_networks

    respond_to do |format|
      format.html #index_on.html.erb
    end
  end
  
  def index_cenx_on
    @ons = OperatorNetwork.find_cenx_operator_networks

    respond_to do |format|
      format.html { render("index_on") }
    end
  end
  
  def new_on
    @sp = ServiceProvider.find(params[:id])
    @on = OperatorNetwork.new
    @onts = @sp.operator_network_types
    @on.service_provider_id = @sp.id
    respond_to do |format|
      format.html # new_on.html.erb
    end
  end
  
  def save_on
    @sp = ServiceProvider.find(params[:id])
    @on = OperatorNetwork.new(params[:on])
    @sp = ServiceProvider.find(params[:id])
    @on.service_provider_id = @sp.id
    
    @on.operator_network_type_id = params[:on][:operator_network_type_id]
    @on.set_member_attr_instances params

    respond_to do |format|
      if @on.save
        flash[:notice] = "Operator Network: #{@on.name} created OK" 
        format.html { redirect_to(:action => :edit_sp, :id => @sp.id) }
      else
        @onts = @sp.operator_network_types
        format.html { render :action => 'new_on' }
      end
    end
  end
  
  def edit_on
    @on = OperatorNetwork.find(params[:id])
    @onts = @on.service_provider.operator_network_types
    @ennins = @on.enni_news
    @paths = @on.paths
    @demarcs = @on.demarcs - @ennins
    @sp = @on.service_provider
    @ennis_on_behalf_of = @on.ennis_on_behalf_of
    @access_permissions = @on.access_permissions
  end    
  
  def update_on
    @on = OperatorNetwork.find(params[:id])
    
    @on.operator_network_type_id = params[:on][:operator_network_type_id]
    @on.set_member_attr_instances params

    respond_to do |format|
      if @on.update_attributes(params[:on])
        flash[:notice] = "Operator Network: #{@on.name} updated OK"
        format.html { redirect_to(:action => :edit_sp, :id => @on.service_provider_id )  }
      else
        @onts = @on.service_provider.operator_network_types
        @ennins = @on.enni_news
        @paths = @on.paths
        @demarcs = @on.demarcs - @ennins
        @sp = @on.service_provider
        @ennis_on_behalf_of = @on.ennis_on_behalf_of
        format.html { render :action => 'edit_on' }
      end
    end
  end
  
  def destroy_on
    @on = OperatorNetwork.find(params[:id])
    @spid = @on.service_provider_id
    @on.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :edit_sp, :id => @spid) }
    end
  end
  
  #
  #Member Attributes
  #

  def index_cma
    @cmas = MemberAttr.where("affected_entity_id is NULL").order("affected_entity_type")
    respond_to do |format|
      format.html # index_cma.html.erb
    end
  end

  def new_ma
    @ma = MemberAttr.new
    @ae_id = params[:id]
    @ae_type = params[:ae_type]
    
    if(@ae_id)
      @ae = parent_from_id_type( @ae_id,@ae_type)
      @ma.affected_entity = @ae
    end

    respond_to do |format|
      format.html # new_ma.html.erb
    end
  end
  
  def save_ma
    @ae_id = params[:id]
    @ae_type = params[:ae_type]
    if MemberAttrTypes::ATTR_OBJECT_TYPES.map{|disp, value| value.to_s }.include? params[:ma][:type]
      #@ma = Module.const_get(params[:ma][:type]).new(params[:ma])
      @ma = Module.const_get(params[:ma][:type]).new
      params[:ma].each { |k,v| @ma.send("#{k}=",v) }
      errors = []
    else
      @ma = MemberAttr.new(params[:ma])
      errors = {:type => "invalid, Use: #{MemberAttrTypes::ATTR_OBJECT_TYPES.map{|disp, value| value }.join(", ")}"}
    end
    if @ae_id
      @ae = parent_from_id_type(@ae_id, @ae_type)
      @ma.affected_entity = @ae
    end
    respond_to do |format|
      if @ma.save
        flash[:notice] = "Member Attribute Definition #{@ma.name} created OK"
        if @ae_id
          format.html { redirect_to(:action => "edit_#{@ae_type}", :id => @ae_id) }
        else
          format.html { redirect_to(:action => "index_cma") }
        end
      else
        errors.each{|key, value| @ma.errors.add(key, value)}
        format.html { render :action => 'new_ma' }
      end
    end
  end

  def edit_ma
    @ma = MemberAttr.find(params[:id])
    @macs = @ma.member_attr_controls
    @ae_type = parent_type_from_table(@ma.affected_entity_type)
  end

  def update_ma
    @ma = MemberAttr.find(params[:id])

    respond_to do |format|
      if @ma.update_attributes(params[:ma])
        flash[:notice] = "Attribute: #{@ma.name} updated OK"
        format.html { redirect_to(:action => :edit_ma, :id => @ma.id )  }
      else
        @ae_type = parent_type_from_table(@ma.affected_entity_type)
        @macs = @ma.member_attr_controls
        format.html { render :action => 'edit_ma' }
      end
    end
  end

  def destroy_ma
    @ma = MemberAttr.find(params[:id])
    @aeid = @ma.affected_entity_id
    @ae_type = parent_type_from_table(@ma.affected_entity_type)
    @ma.destroy

    respond_to do |format|
      if @aeid
        format.html { redirect_to(:action => "edit_#{@ae_type}", :id => @aeid) }
      else
        format.html { redirect_to(:action => "index_cma") }
      end
    end
  end

  def options_type_element_instance_element
    @ie = parent_from_id_type(params[:ie_id], params[:ie_type])
    @te = parent_from_id_type(params[:te_id], params[:te_type])
    @ie.type_element = @te
    params.reject{|key, value| ["ie_id", "ie_type", "te_id", "te_type", "use_tma", "use_cma", "action", "controller", "authenticity_token","utf8"].include? key.to_s}.each{|key, value|
      id = value.split(";")[0]
      type = value.split(";")[1]
      
      unless id == nil or id.empty? or type == nil or type.empty?
        @ie.send(key.to_s + "=", parent_from_id_type(id, type))
      else
        SW_ERR "options_type_element_instance_element contained invalid parameter \"#{key}\"-\"#{value}\""
      end
    }
    @ie.use_tma = (params[:use_tma] == "true") if params[:use_tma] != nil
    @ie.use_cma = (params[:use_cma] == "true") if params[:use_cma] != nil
    
    render(:partial => "options_instance_element", :locals => {:ie => @ie, :tail => params[:ie_type], :with => "type_element=#{params[:te_id]};#{params[:te_type]}" })
  end
  
  def update_mai
    mai_info = params[:observed_id].split("_")
    ie = parent_from_id_type(mai_info[3], parent_type_from_table(mai_info[2]))
    params.reject{|key, value| ["observed_id", "observed_value", "observer_id", "observer_value", "name", "action", "controller", "authenticity_token","utf8"].include? key.to_s}.each{|key, value|
      id = value.split(";")[0]
      type = value.split(";")[1]
      ie.send(key.to_s + "=", parent_from_id_type(id, type))
    }
    observered_mai = ie.get_member_attr_instances.select{|mai| mai.full_form_id == (mai_info[3] ? params[:observed_id] : params[:observed_id] + "#{ie.id}") }.first
    observing_mai = ie.get_member_attr_instances.select{|mai| mai.full_form_id == (mai_info[3] ? params[:observer_id] : params[:observer_id] + "#{ie.id}") }.first
    observing_mai.affected_entity = ie
    observered_mai.affected_entity = ie
    observered_mai[:value] = params[:observed_value]
    observing_mai[:value] = params[:observer_value]
    observing_mai.update_value
    render :partial => "input_mai", :locals => {:name => params[:name], :mai => observing_mai}
  end

  def new_mac
    @mac = MemberAttrControl.new
    @ma = MemberAttr.find(params[:id])
    @mac.member_attr = @ma

    respond_to do |format|
      format.html # new_mac.html.erb
    end
  end
  
  def save_mac
    @mac = MemberAttrControl.new(params[:mac])
    @ma = MemberAttr.find(params[:id])
    @mac.member_attr = @ma
    respond_to do |format|
      if @mac.save
        flash[:notice] = "Member Attribute Controle created OK"
        format.html { redirect_to(:action => "edit_ma", :id => @ma.id) }
      else
        format.html { render :action => 'new_mac' }
      end
    end
  end

  def edit_mac
    @mac = MemberAttrControl.find(params[:id])
  end

  def update_mac
    @mac = MemberAttrControl.find(params[:id])

    respond_to do |format|
      if @mac.update_attributes(params[:mac])
        flash[:notice] = "Member Attr Control updated OK"
        format.html { redirect_to(:action => :edit_mac, :id => @mac.id )  }
      else
        format.html { render :action => 'edit_mac' }
      end
    end
  end

  def destroy_mac
    @ma = MemberAttr.find(params[:id])
    @aeid = @ma.affected_entity_id
    @ae_type = parent_type_from_table(@ma.affected_entity_type)
    @ma.destroy

    respond_to do |format|
      if @aeid
        format.html { redirect_to(:action => "edit_#{@ae_type}", :id => @aeid) }
      else
        format.html { redirect_to(:action => "index_cma") }
      end
    end
  end
  
  #
  #OSS Info
  #

  #

  def index_ossi
    @ossi = OssInfo.all

    respond_to do |format|
      format.html # index_ossi.html.erb
      format.xml  { render :xml => @ossi }
    end
  end


  def new_ossi
    @ossi = OssInfo.new
    @ont = OperatorNetworkType.find(params[:id])
    @ossi.operator_network_type = @ont
    cenx = ServiceProvider.find_by_is_system_owner(true)
    @cenx_contacts = cenx ? cenx.contacts : []
    @member_contacts = @ont.service_provider.contacts
    respond_to do |format|
      format.html # new_ossi.html.erb
    end
  end

  def save_ossi
    @ossi = OssInfo.new(params[:ossi])
    @ont = OperatorNetworkType.find(params[:id])
    @ossi.operator_network_type = @ont

    respond_to do |format|
      if @ossi.save
        flash[:notice] = "Offered Services Study Info: #{@ossi.name} created OK"
        format.html { redirect_to(:action => :edit_ont, :id => @ont.id) }
      else
        cenx = ServiceProvider.find_by_is_system_owner(true)
        @cenx_contacts = cenx ? cenx.contacts : []
        @member_contacts = @ont.service_provider.contacts
        format.html { render :action => 'new_ossi' }
      end
    end
  end

  def edit_ossi
    @ossi = OssInfo.find(params[:id])
    @ont = @ossi.operator_network_type
    cenx = ServiceProvider.find_by_is_system_owner(true)
    @cenx_contacts = cenx ? cenx.contacts : []
    @member_contacts = @ont.service_provider.contacts
    @ufs = @ossi.uploaded_files
  end

  def update_ossi
    @ossi = OssInfo.find(params[:id])
    @ont = @ossi.operator_network_type

    respond_to do |format|
      if @ossi.update_attributes(params[:ossi])
        flash[:notice] = "Offered Services Study Info: #{@ossi.name} updated OK"
        format.html { redirect_to(:action => :edit_ossi, :id => @ossi.id )  }
      else
        cenx = ServiceProvider.find_by_is_system_owner(true)
        @cenx_contacts = cenx ? ServiceProvider.find_by_is_system_owner(true).contacts : []
        @member_contacts = @on.service_provider.contacts
        @ufs = @ossi.uploaded_files
        format.html { render :action => 'edit_ossi' }
      end
    end
  end

  def destroy_ossi
    @ossi = OssInfo.find(params[:id])
    @ontid = @ossi.operator_network_type_id
    @ossi.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :edit_ont, :id => @ontid) }
    end
  end

  #
  # Path Management
  #  
  
  def index_path
    @paths = Path.all.reject{|path| path.service_provider.is_system_owner}
    @ons = OperatorNetwork.find_rows_for_path_index.reject{|on| on.service_provider.is_system_owner}
    @ons = @ons.sort_by {|o| o.pull_down_name_on}

    respond_to do |format|
      format.html # index_path.html.erb
    end
  end
  
  def index_cenx_path
    @paths = Path.all.select{|path| path.service_provider.is_system_owner}
    @ons = OperatorNetwork.find_rows_for_index.select{|on| on.service_provider.is_system_owner}

    respond_to do |format|
      format.html { render("index_path") }
    end
  end
  
  def new_path
    @so = (params[:so_id]) ? ServiceOrder.find(params[:so_id]) : nil

    IpFlow.all
    Evc.all
    @path = Path.new
    @descendants = Path.descendants.map{ |d| d.to_s }

    @path.service_orders.push(@so) if @so
    @path.cenx_id = "To Be Determined"
    @on = OperatorNetwork.find(params[:id])
    @path.operator_network_id = @on.id
    @pathts = @path.get_candidate_path_types
    @site_groups = SiteGroup.find_rows_for_index
    respond_to do |format|
      format.html # new_path.html.erb
    end
    
  end 
  
  def save_path
    @path = Kernel.const_get(params[:class]).new(params[:path])
    @on = OperatorNetwork.find(params[:id])
    @path.operator_network_id = @on.id
    
    @path.path_type_id = params[:path][:path_type_id]
    @path.set_member_attr_instances params

    IpFlow.all
    Evc.all
    @descendants = Path.descendants.map{ |d| d.to_s }
    
    @so = (params[:so_id]) ? ServiceOrder.find(params[:so_id]) : nil
    
    respond_to do |format|
      if @path.save
        if @so                    
          @so.ordered_entity_id = @path.id
          @so.ordered_entity_snapshot = @path.attr_snapshot
          @so.save
          @path.reload  
        end      
        flash[:notice] = "#{@path.cenx_name} created OK" 
        format.html { redirect_to(:action => :edit_path, :id => @path.id) }
      else
        @pathts = @path.get_candidate_path_types
        format.html { render :action => 'new_path' }
      end
    end
  end  
  
  def edit_path
    @path = Path.find(params[:id])
    @pathts = @path.get_candidate_path_types
    @segments = @path.segments
    @onovcs = @path.on_net_ovcs
    @onrouters = @path.on_net_routers
    @offovcs = @path.off_net_ovcs

    @ies = @path.segments
    #Sprint Demo
    @ies += @path.demarcs
    @ies += @path.ennis
    @ies.flatten

    @ehs = @path.mtc_periods + @path.sm_histories
    @states = @path.statefuls
    @so = @path.get_latest_order
    @sos = @path.service_orders
    @hide = params[:hide]
  end
  
  def update_path
    @path = Path.find(params[:id])
    
    @path.path_type_id = params[:path][:path_type_id]
    @path.set_member_attr_instances params
    
    @ies = @path.segments
    #Sprint Demo
    @ies += @path.demarcs
    @ies += @path.ennis
    @ies.flatten

    if params.keys.select{|key| key.match(MemberAttrInstance.full_form_id_pattern)}.empty?
      mais = []
    else
      mais = @ies.collect{|ie| ie.get_member_attr_instances}.flatten.select{|mai| mai.is_a? MemberHandleInstance}
      mais.each{|mai| mai.value = params[mai.full_form_id]; mai.valid?; mai.errors.each_full{|msg| @segment.errors[:base] << msg}}
    end
    
    @ehs = @path.mtc_periods + @path.sm_histories
    
    respond_to do |format|
      if @path.update_attributes(params[:path])
        mais.each{|mai| mai.save}
        flash[:notice] = "#{@path.cenx_name} updated OK"
        format.html { redirect_to(:action => :edit_path, :id => @path.id, :hide => mais.empty?.to_s )  }
        format.xml  { head :ok }
      else
        @pathts = @path.get_candidate_path_types
        @onovcs = @path.on_net_ovcs
        @onrouters = @path.on_net_routers
        @offovcs = @path.off_net_ovcs
        @ehs = @path.mtc_periods + @path.sm_histories
        @states = @path.statefuls
        @so = @path.get_latest_order
        @sos = @path.service_orders

        @hide = mais.empty?.to_s
        format.html { render :action => 'edit_path' }
        format.xml  { render :xml => @path.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy_path
    @path = Path.find(params[:id])
    @path.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :index_path) }
    end
  end
  
  def generate_diagram
    begin
      path = Path.find(params[:path_id])
      path.layout_diagram
      render :text => "Successful"
    rescue
      render :text => "Error during diagram generation"
    end
  end
  
  def choose_segment_path
    @path = Path.find(params[:id])
    @segments = @path.get_candidate_reachable_segments
    if params[:segment_class]
      @segments.reject!{|segment| !segment.is_a? Kernel.const_get(params[:segment_class])}
      @segment_class = params[:segment_class]
    end
    respond_to do |format|
      format.html # choose_segmentep_demarc.html.erb
    end
  end
    
  def link_segment_path
    @path = Path.find(params[:id])  
    
    if (!params[:segment])||(params[:segment][:id] == "")
      @segments = @path.get_candidate_reachable_segments
      
      if params[:segment_class]
        @segments.reject!{|segment| !segment.is_a? Kernel.const_get(params[:segment_class])}
        @segment_class = params[:segment_class]
      end
      
      render_action_id_with_errors(
        @path, 'choose_segment_path', @path.id, 
        "Segment info is invalid - try again" )
      return
    end
    
    @segment = Segment.find(params[:segment][:id])
    result = @path.segments.push(@segment)
    @path.reload
    @segment.reload
    @segment.valid_paths?
    if result and @segment.errors.empty?
      respond_to do |format|
        flash[:notice] = "Segment: #{@segment.cenx_name} added OK to #{@path.cenx_name}"
        format.html { redirect_to(:action => 'edit_path', :id => @path.id ) }
      end
    else
      @path.segments.delete(@segment)
      @segments = @path.get_candidate_reachable_segments
      flash[:errors] = @segment.errors

      if params[:segment_class]
        @segments.reject!{|segment| !segment.is_a? Kernel.const_get(params[:segment_class])}
        @segment_class = params[:segment_class]
      end

      render_action_id_with_errors(
        @path, 'choose_segment_path', @path.id,
        "Segment: #{@segment.cenx_name} save failed - please ensure data was entered correctly"
       )
    end
  end
  
  def choose_path_segment
    @segment = Segment.find(params[:id])
    @paths = @segment.get_candidate_paths
    respond_to do |format|
      format.html # choose_segmentep_demarc.html.erb
    end
  end
    
  def link_path_segment
    @segment = Segment.find(params[:id])  
    
    if (!params[:path])||(params[:path][:id] == "")
      @paths = @segment.get_candidate_paths
      
      render_action_id_with_errors(
        @segment, 'choose_path_segment', @segment.id, 
        "Path info is invalid - try again" )
      return
    end
    
    @path = Path.find(params[:path][:id])
    result = @path.segments.push(@segment)
    @path.reload
    @segment.reload
    @segment.valid_paths?
    if result and @segment.errors.empty?
      respond_to do |format|
        flash[:notice] = "Segment: #{@segment.cenx_name} added OK to #{@path.cenx_name}"
        format.html { redirect_to(:action => 'edit_segment', :id => @segment.id ) }
      end
    else
      @path.segments.delete(@segment)
      @paths = @segment.get_candidate_paths
      flash[:errors] = @segment.errors

      render_action_id_with_errors(
        @segment, 'choose_path_segment', @segment.id,
        "Path: #{@path.cenx_name} save failed - please ensure data was entered correctly"
       )
    end
  end
  
  def unlink_segment_path
    if params[:segment_id]
      @segment = Segment.find(params[:segment_id])
      @path = Path.find(params[:id])
    else
      @segment = Segment.find(params[:id])
      @path = Path.find(params[:path_id])
    end
    respond_to do |format|
      if @segment.paths.length > 1 &&  @path.segments.delete(@segment)
        flash[:notice] = "Segment: #{@segment.cenx_name} removed OK from #{@path.cenx_name}"
        if params[:segment_id]
          format.html { redirect_to(:action => 'edit_segment', :id => @segment.id ) }
        else
          format.html { redirect_to(:action => 'edit_path', :id => @path.id ) }
        end
      else
        flash[:error] = "Segment: #{@segment.cenx_name} remove failed for #{@path.cenx_name}"
        flash[:error] += ", Segment must have at least one Path" if @segment.paths == [@path]
        if params[:segment_id]
          format.html { redirect_to(:action => 'edit_segment', :id => @segment.id ) }
        else
          format.html { redirect_to(:action => 'edit_path', :id => @path.id ) }
        end
      end
    end
  end

  #
  # Path Type
  #
  
  def new_patht
    @patht = PathType.new
    @ont = OperatorNetworkType.find(params[:id])

    respond_to do |format|
      format.html # new_dt.html.erb
    end
  end

  def save_patht
    @patht = PathType.new(params[:patht])
    @ont = OperatorNetworkType.find(params[:id])
    @patht.operator_network_type = @ont

    respond_to do |format|
      if @patht.save
        flash[:notice] = "Path Type: #{@patht.name} created OK"
        format.html { redirect_to(:action => :edit_ont, :id => @ont.id) }
      else
        format.html { render :action => 'new_patht' }
      end
    end
  end

  def edit_patht
    @patht = PathType.find(params[:id])
    @ont = @patht.operator_network_type
  end

  def update_patht
    @patht = PathType.find(params[:id])
    @ont = @patht.operator_network_type

    respond_to do |format|
      if @patht.update_attributes(params[:patht])
        flash[:notice] = "Path Type: #{@patht.name} updated OK"
        format.html { redirect_to(:action => :edit_patht, :id => @patht.id )  }
      else
        format.html { render :action => 'edit_patht' }
      end
    end
  end

  def destroy_patht
    @patht = PathType.find(params[:id])
    @ontid = @patht.operator_network_type.id
    @patht.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :edit_ont, :id => @ontid) }
    end
  end

  #
  # On Net OVC Management
  #
  def index_onovc
    @onovcs = OnNetOvc.all.reject{|onovc| onovc.buyer.is_system_owner}
    @show_order_button = true

    respond_to do |format|
      format.html # index_onovc.html.erb
    end
  end
  
  def index_onovc
    @onovcs = OnNetOvc.all.select{|onovc| onovc.buyer.is_system_owner}
    @paths = Path.all.select{|path| path.service_provider.is_system_owner}
    @show_order_button = false

    respond_to do |format|
      format.html { render("index_onovc") }
    end
  end

  def index_onovc_order
    @sos = OnNetOvcOrder.all.select{|onovc| onovc.kind_of? OnNetOvcOrder}
    @sps = ServiceProvider.find_rows_for_index
    @on = OperatorNetwork.new #hack - TODO Fix _select_sp to use collection_select
    respond_to do |format|
      format.html # index_onovc_order.html.erb
    end
  end

  def index_offovc
    @offovcs = OffNetOvc.all.reject{|offovc| offovc.buyer.is_system_owner}
    @show_order_button = true

    respond_to do |format|
      format.html # index_offovc.html.erb
    end

  end

  def index_cenx_offovc
    @offovcs = OffNetOvc.all.select{|offovc| offovc.buyer.is_system_owner}
    @paths = Path.all.select{|path| path.service_provider.is_system_owner}
    @show_order_button = false

    respond_to do |format|
      format.html { render("index_offovc") }
    end

  end

  def index_offovc_order
    @sos = OffNetOvcOrder.all.select{|offovc| offovc.kind_of? OffNetOvcOrder}
    @sps = ServiceProvider.find_rows_for_index
    @on = OperatorNetwork.new #hack - TODO Fix _select_sp to use collection_select
    respond_to do |format|
      format.html # index_offovc_order.html.erb
    end

  end


  def new_segment

    @so = (params[:so_id]) ? ServiceOrder.find(params[:so_id]) : nil
    
    @ons = []
    @sps = []
    @ests = []
    @sites = Site.find_rows_for_index
    
    if((!@so && params[:segment_class] && params[:segment_class] == "OnNetOvc") ||
       (@so && @so.ordered_entity_subtype  == "OnNetOvc"))
      @segment = OnNetOvc.new
      @site_groups = SiteGroup.find_rows_for_index
    elsif((!@so && params[:segment_class] && params[:segment_class] == "OnNetRouter") ||
       (@so && @so.ordered_entity_subtype  == "OnNetRouter"))
      @segment = OnNetRouter.new
      @site_groups = SiteGroup.find_rows_for_index
    elsif((!@so && params[:segment_class] && params[:segment_class] == "OffNetOvc") ||
      (@so && @so.ordered_entity_subtype  == "OffNetOvc"))
      @segment = OffNetOvc.new
      if @so
        @segment.service_orders.build(@so.attributes)
        @segment.segment_owner_role = "Seller"
        @segment.set_operator_network(@so.ordered_operator_network)
        @sps = [@segment.service_provider]
        @ons = [@segment.get_operator_network]
        if @so.ordered_object_type
          @segment.ethernet_service_type = @so.ordered_entity_group.ethernet_service_type 
          @ests = [@segment.ethernet_service_type]
          @segment.segment_type = @so.ordered_object_type
          @segmentts = [@segment.segment_type]
        else
          @ests = @segment.get_operator_network.get_candidate_ethernet_service_types
        end
      end
    end
    
    @segment.use_member_attrs = true if @so
    # set the Path ID for the new Segment
    if( @so )
      @segment.paths.push(Path.find(@so.path_id))
    elsif (params[:path_id])
      @segment.paths.push(Path.find(params[:path_id]))
    end
    @path = @segment.paths.first
    @segment.path_network = @path.operator_network
    @contacts = @segment.get_candidate_contacts

    @segment.cenx_id = "To Be Determined"
    @paths = @path ? [@path] : Path.all
    @segmentts = @segment.get_candidate_segment_types if @segmentts.nil?

    respond_to do |format|
      format.html # new_segment.html.erb
     end
  
  end

  # On Net OVC Partials - START

  def update_on_segment
    @segment = OnNetSegment.new
    @segment.site_id = params[:site_id]
    @ests = @segment.get_operator_network.get_candidate_ethernet_service_types
    render :partial => 'update_on', :object => @segment,
                        :locals => {
                          :prompt => '*Ethernet Service Type:',
                          :action => 'options_est_segment',
                          :update => 'est_select_field',
                          :with => "'onid=' + #{@segment.get_operator_network.id}" }
  end

  def suggest_service_id_onovc
    @segment = OnNetOvc.new
    if params[:site_id].to_i != 0
      @site = Site.find(params[:site_id])
      @segment.service_id = IdTools::ServiceIdGenerator.generate_on_net_ovc_service_id @site
    else
      @segment.errors.add(:base,"Selected SiteGroup")
    end

    render :partial => 'options_id_onovc'

  end

  # On Net OVC Partials - END

  # Off Net OVC Partials - START

  def options_type_element_segment
    @segment = Segment.where("id = ?", params[:id]).first if params[:id]
    if @segment == nil
      if params[:on_id] && params[:on_id] != "false"
        @segment = OffNetOvc.new()
      elsif params[:site_id] && params[:site_id] != "false"
        @segment = OnNetSegment.new()
      end
    end
    @path = Path.where("id = ?", params[:path_id]).first
    @segment.paths.push(@path)
    @segmentt = SegmentType.where("id = ?", params[:segmentt_id]).first
    @segment.segment_type = @segmentt
    @est = EthernetServiceType.where("id = ?", params[:est_id]).first
    @segment.ethernet_service_type = @est
    if params[:on_id] && params[:on_id] != "false"
      @on = OperatorNetwork.where("id = ?", params[:on_id]).first
      @segment.set_operator_network @on
    end
    if params[:site_id] && params[:site_id] != "false"
      @site = Site.where("id = ?", params[:site_id]).first
      @segment.site = @site
    end
    @segment.use_member_attrs = params[:use_ma]
    render(:partial => "options_instance_element", :locals => {:ie => @segment, :tail => 'segmentt' })
  end

  def options_instance_elements
    @ie = parent_from_id_type(params[:id], params[:tail])
    @ie.use_member_attrs = params[:use_ma] if params[:use_ma] != nil
    @ies = @ie.get_related_instance_elements
    render(:partial => "options_instance_elements", :locals => {:id => params[:id], :tail => params[:tail], :disable_handles => params[:disable_handles] == "true", :disable_non_handles => params[:disable_non_handles] == "true", :hide => params[:hide] != "false" })
  end
  
  def options_sp_segment
    @segment = OffNetOvc.new
    @path = Path.find(params[:pathid])
    if params[:role] == "Buyer"
      @sps = [@path.service_provider]
    else
      @sps = ServiceProvider.find_rows_for_index
    end
    render :partial => 'select_sp', :object => @segment,
           :locals => { :prompt => '*Member:',
                        :action => 'options_on_segment',
                        :update => 'on_select_field',
                        :with => "'spid=' + value"}
  end

  def options_on_segment
    @segment = OffNetOvc.new
    @sp = ServiceProvider.find(params[:spid])
    @ons = @sp.operator_networks
    render :partial => 'select_on', :object => @segment,
           :locals => { :prompt => '*Network:',
                        :action => 'options_est_segment',
                        :update => 'est_select_field',
                        :with => "'onid=' + value"}
  end

  def options_est_segment
    @segment = OffNetOvc.new
    #@path = Path.find(params[:pathid])
    @on = OperatorNetwork.find(params[:onid])
    @ests = @on.get_candidate_ethernet_service_types
    render :partial => 'select_est', :object => @segment,
                        :locals => { :prompt => '*Ethernet Service Type:',
                        :action => 'options_segmentt_segment',
                        :update => 'segmentt_select_field',
                        :with => "'estid=' + value"}
  end

  def options_segmentt_segment
    @segment = OffNetOvc.new
    #@path = Path.find(params[:pathid])
    @est = EthernetServiceType.find(params[:estid])
    @segmentts = @est.segment_types
    render :partial => 'select_segmentt', :object => @segment
  end

  def options_segmentt_segment
    @segment = OffNetOvc.new
    #@path = Path.find(params[:pathid])
    #@on = OperatorNetwork.find(params[:onid])
    @est = EthernetServiceType.where("id = ?", params[:estid]).first
    @segmentts = @est ? @est.segment_types : []
    render :partial => 'select_segmentt', :object => @segment
  end

  # Off Net OVC Partials - END

  def save_segment
    if(params[:segment_class] == "OnNetOvc")
    @segment = OnNetOvc.new(form_to_params(params, "segment", ["id","cenx_name","so_id","so_prim_contact","emergency_contact_phone_no", "segment_class", "path_id", MemberAttrInstance.form_id_pattern]))
      @segment.segment_owner_role = "Buyer"
    elsif(params[:segment_class] == "OnNetRouter")
      @segment = OnNetRouter.new(form_to_params(params, "segment", ["id","cenx_name","so_id","so_prim_contact","emergency_contact_phone_no","service_provider_id", "segment_class", "path_id", MemberAttrInstance.form_id_pattern]))
      @segment.segment_owner_role = "Buyer"
    elsif(params[:segment_class] == "OffNetOvc")
      @segment = OffNetOvc.new(form_to_params(params, "segment", ["id","cenx_name","so_id","so_prim_contact","emergency_contact_phone_no","service_provider_id", "segment_class", "path_id", MemberAttrInstance.form_id_pattern]))
    else
      SW_ERR "Trying to save Segment of type: #{params[:segment_class]}"
      @segment = Segment.new(form_to_params(params, "segment", ["id","cenx_name","so_id","so_prim_contact","emergency_contact_phone_no", "path_id", MemberAttrInstance.form_id_pattern]))
    end

    @so = (params[:so_id]) ? ServiceOrder.find(params[:so_id]) : nil
    @contacts = @segment.get_candidate_contacts

    # set the Path ID for the new Segment
    if( @so )
      @segment.paths.push(Path.find(@so.path_id))
    elsif (params[:path_id])
      @segment.paths.push(Path.find(params[:path_id]))
    end
    @path = @segment.paths.first
    @segment.path_network = @path.operator_network
    @segmenteps = @segment.segment_end_points

    @segment.set_member_attr_instances params
        
    respond_to do |format|
      if @segment.save
        if @so                    
          @so.ordered_entity_id = @segment.id
          @so.ordered_entity_snapshot = @segment.attr_snapshot
          @so.save
          @segment.reload          
        end
        flash[:notice] = "#{@segment.cenx_name} created OK - please add CoS Instances and Segment Endpoints "
        format.html { redirect_to(:action => :edit_segment, :id => @segment.id) }
      else
        @segment.cenx_id = "To Be Determined"
        @sites = Site.find_rows_for_index
        if @so && @so.ordered_entity_subtype  == "OffNetOvc"
          @segment.service_orders.build(@so.attributes)
          @segment.segment_owner_role = "Seller"
          @segment.set_operator_network(@so.ordered_operator_network)
          @sps = [@segment.service_provider]
          @ons = [@segment.get_operator_network]
          if @so.ordered_entity_group
            @segment.ethernet_service_type = @so.ordered_entity_group.ethernet_service_type 
            @ests = [@segment.ethernet_service_type]
            @segment.segment_type = @so.ordered_object_type
            @segmentts = [@segment.segment_type]
          else
            @ests = @segment.get_operator_network.get_candidate_ethernet_service_types
          end
        else
          @sps = []
          @ons = []
          @ests = []
        end
        @segmentts = @segment.get_candidate_segment_types if @segmentts.nil?
        format.html { render :action => 'new_segment', :path_id => @path.id }
      end
    end
  end

  def edit_segment
    @segment = Segment.find(params[:id])
    @onovces = @segment.on_net_ovc_end_point_ennis
    @segmentepes = @segment.ovc_end_point_ennis
    @segmentepus = @segment.ovc_end_point_unis
    @paths = @segment.paths
    @sites = Site.find_rows_for_index
    @ons = @segment.get_candidate_operator_networks
    @ests = @segment.get_candidate_ethernet_service_types
    @segmentts = @segment.get_candidate_segment_types
    @so = @segment.get_latest_order
    @sos = @segment.service_orders
    @contacts = @segment.get_candidate_contacts
    @ufs = @segment.uploaded_files
    @cosis = @segment.cos_instances
    @ies = @segment.get_related_instance_elements
    @ehs =  @segment.mtc_periods + @segment.alarm_histories + @segment.sm_histories
    @states = @segment.statefuls
    @hide = params[:hide]
  end

  def update_segment
    @segment = Segment.find(params[:id])
    @segmenteps = @segment.segment_end_points
    @contacts = @segment.get_candidate_contacts
    @segment.use_member_attrs = params[:segment][:use_member_attrs]
    @segment.set_member_attr_instances params
    @ies = @segment.get_related_instance_elements
    if params.keys.select{|key| key.match(MemberAttrInstance.full_form_id_pattern)}.empty?
      mais = []
    else
      mais = @ies.collect{|ie|
        ie.set_member_attr_instances params, true
        ie.get_member_attr_instances
      }.flatten
      mais.each{|mai| mai.valid?; mai.errors.each_full{|msg| @segment.errors[:base] << msg}}
    end
    
    respond_to do |format|
      if @segment.errors.empty? && @segment.update_attributes(form_to_params(params, "segment", ["id","cenx_name","so_id","so_prim_contact","emergency_contact_phone_no","service_provider_id","path_id", MemberAttrInstance.form_id_pattern]))
        mais.each{|mai| mai.save}
        flash[:notice] = "#{@segment.type} #{@segment.cenx_id} updated OK"
        format.html { redirect_to(:action => :edit_segment, :id => @segment.id, :hide => mais.empty?.to_s) }
      else
        @paths = @segment.paths
        @onovces = @segment.on_net_ovc_end_point_ennis
        @segmentepes = @segment.ovc_end_point_ennis
        @segmentepus = @segment.ovc_end_point_unis
        @sites = Site.find_rows_for_index
        @ons = @segment.get_candidate_operator_networks
        @ests = @segment.get_candidate_ethernet_service_types
        @segmentts = @segment.get_candidate_segment_types
        @so = @segment.get_latest_order
        @sos = @segment.service_orders
        @ufs = @segment.uploaded_files
        @cosis = @segment.cos_instances
        @ehs = @segment.alarm_histories + @segment.mtc_periods + @segment.sm_histories
        @states = @segment.statefuls
        @hide = mais.empty?.to_s
        format.html { render :action => 'edit_segment' }
      end
    end
  end

  def destroy_segment
    @segment = Segment.find(params[:id])
    @order = ServiceOrder.where('ordered_entity_type = ? and ordered_entity_id = ?', "Segment", @segment.id).first
    unless @order.nil?
      @order.ordered_entity = nil
      @order.save
    end
    #@path_id = @segment.path_id
    @segment.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :index_path) }
    end
  end

  def config_gen_segment
    @segment = Segment.find(params[:id])
    @config_text = @segment.generate_config
    @lines = @config_text.count '\n'
    respond_to do |format|
      format.html # config_gen_segment.html.erb
    end
  end

  #
  # CosInstance Management
  #

  def new_cosi
    @segment = Segment.find(params[:id])
    @cosi = (@segment.is_a? OnNetOvc) ? CenxCosInstance.new : CosInstance.new
    @cosi.segment = @segment
    @costs = @cosi.get_candidate_costs
    @cost = ClassOfServiceType.new
    respond_to do |format|
      format.html # new_cosi.html.erb
     end
  end

  def grab_cos_id_cosi
    @cost = ClassOfServiceType.find(params[:id])
    render :partial => 'input_cosi_cosid_value'
  end

  def save_cosi
    @segment = Segment.find(params[:id])
    @cosi = (@segment.is_a? OnNetOvc) ? CenxCosInstance.new(params[:cosi]) : CosInstance.new(params[:cosi])
    @cosi.segment = @segment

    respond_to do |format|
      if @cosi.save
        flash[:notice] = "CosInstance: #{@cosi.cos_name} created OK"
        format.html { redirect_to(:action => :edit_segment, :id => @segment.id) }
      else
        @costs = @cosi.get_candidate_costs
        @cost = ClassOfServiceType.new
        format.html { render :action => 'new_cosi' }
      end
    end
  end
  
  def edit_cosi
    @cosi = CosInstance.find(params[:id])
    @costs = [@cosi.class_of_service_type]
    @cost = @cosi.class_of_service_type
    @coses = @cosi.cos_end_points
  end

  def update_cosi
     @cosi = CosInstance.find(params[:id])

    respond_to do |format|
      if @cosi.update_attributes(params[:cosi])
        flash[:notice] = "Cos Instance: #{@cosi.cos_name} updated OK"
        format.html { redirect_to(:action => :edit_segment, :id => @cosi.segment_id )  }
      else
        @costs = [@cosi.class_of_service_type]
        @cost = @cosi.class_of_service_type
        @coses = @cosi.cos_end_points
        format.html { render :action => 'edit_cosi' }
      end
    end
  end

  def destroy_cosi
    @cosi = CosInstance.find(params[:id])
    @segment_id = @cosi.segment_id
    @cosi.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :edit_segment, :id => @segment_id) }
    end
  end

  def new_segmentep
    if(params[:segmentep_class] == "OnNetOvcEndPointEnni")
       @segmentep = OnNetOvcEndPointEnni.new
    elsif(params[:segmentep_class] == "OnNetRouterSubIf")
      @segmentep = OnNetRouterSubIf.new
    elsif(params[:segmentep_class] == "OvcEndPointEnni")
      @segmentep = OvcEndPointEnni.new
    elsif(params[:segmentep_class] == "OvcEndPointUni")
      @segmentep = OvcEndPointUni.new
    else
      SW_ERR "new_segment ERROR unexpected class: #{params[:segmentep_class]}"
    end

    @segment = Segment.find(params[:id])
    @segmentep.segment = @segment
    #@segment.segment_end_points.push(@segmentep) --- This adds an extra row in the table
    @segmentep.cenx_id = "To Be Determined"
    @segmentepts = @segmentep.get_candidate_segment_end_point_types

    respond_to do |format|
      format.html # new_segmentep.html.erb
    end
  end

  def suggest_stag_segmentep
    @segmentep = SegmentEndPoint.find(params[:segmentep_id])
    result_ok, valid_stag = @segmentep.suggest_valid_stag

    if !result_ok
      @segmentep.errors.add(:base,"BUG! - cant suggest outer tag - this endpoint is not connected to anything")
    else
      @segmentep.stags = valid_stag
    end

    render :partial => 'options_stag_segmentep'
  end
  
  def options_monitoring_segmentep
    @segmentep = SegmentEndPoint.where("id = ?", params[:segmentep_id]).first
    @segmentep = SegmentEndPoint.new if @segmentep.nil?
    @segmentep.is_monitored = params[:is_monitored]
    render(:partial=> 'options_monitoring_segmentep')
  end

  def save_segmentep
    if(params[:segmentep_class] == "OnNetOvcEndPointEnni")
       @segmentep = OnNetOvcEndPointEnni.new(form_to_params(params, "segmentep", ["id","cenx_name", "segmentep_class", "default_cos_handling", MemberAttrInstance.form_id_pattern]))
    elsif(params[:segmentep_class] == "OnNetRouterSubIf")
       @segmentep = OnNetRouterSubIf.new(form_to_params(params, "segmentep", ["id","cenx_name", "segmentep_class", "default_cos_handling", MemberAttrInstance.form_id_pattern]))
    elsif(params[:segmentep_class] == "OvcEndPointEnni")
      @segmentep = OvcEndPointEnni.new(form_to_params(params, "segmentep", ["id","cenx_name", "segmentep_class", "default_cos_handling", MemberAttrInstance.form_id_pattern]))
    elsif(params[:segmentep_class] == "OvcEndPointUni")
      @segmentep = OvcEndPointUni.new(form_to_params(params, "segmentep", ["id","cenx_name", "segmentep_class", "default_cos_handling", MemberAttrInstance.form_id_pattern]))
    else
      SW_ERR "save_segment ERROR unexpected class: #{params[:segmentep_class]}"
    end
    @segment = Segment.find(params[:id])
    @segmentep.segment = @segment
    
    @segmentep.segment_end_point_type_id = params[:segmentep][:segment_end_point_type_id]
    @segmentep.set_member_attr_instances params
    
    respond_to do |format|
      if @segmentep.save
        flash[:notice] = "#{@segmentep.type}: #{@segmentep.cenx_name} created OK - you can now link this endpoint to a demarc"
        format.html { redirect_to(:action => :edit_segmentep, :id => @segmentep.id )  }
      else
        @segmentepts = @segmentep.get_candidate_segment_end_point_types
        format.html { render :action => 'new_segmentep' }
      end
    end
  end

  def edit_segmentep
    @segmentep = SegmentEndPoint.find(params[:id])
    @segmenteps = @segmentep.segment_end_points
    @segmentepts = @segmentep.get_candidate_segment_end_point_types
    @coses = @segmentep.cos_end_points
    @ehs = @segmentep.alarm_histories + @segmentep.sm_histories
    @states = @segmentep.statefuls
    @costvs = @segmentep.cos_test_vectors
    @segment = @segmentep.segment
    if @segment.is_a? OnNetOvc
      @siblings = @segment.on_net_ovc_end_point_ennis.reject {|ep| ep == @segmentep}
    elsif @segment.is_a? OffNetOvc
      @enni_siblings = @segment.ovc_end_point_ennis.reject {|ep| ep == @segmentep}
      @uni_siblings = @segment.ovc_end_point_unis.reject {|ep| ep == @segmentep}
    end
  end

  def update_segmentep
    @segmentep = SegmentEndPoint.find(params[:id])
    @segment = @segmentep.segment
    
    @segmentep.segment_end_point_type_id = params[:segmentep][:segment_end_point_type_id]
    @segmentep.set_member_attr_instances params
    
    respond_to do |format|
      if @segmentep.update_attributes(form_to_params(params, "segmentep", ["id","cenx_name","cenx_id","segmentep_class", "default_cos_handling", MemberAttrInstance.form_id_pattern]))
        flash[:notice] = "#{@segmentep.type}: #{@segmentep.cenx_name} updated OK"
        format.html { redirect_to(:action => :edit_segmentep, :id => @segmentep.id )  }
      else
        @segmentepts = @segmentep.get_candidate_segment_end_point_types
        @segmenteps = @segmentep.segment_end_points
        @coses = @segmentep.cos_end_points
        @ehs = @segmentep.alarm_histories + @segmentep.sm_histories
        @states = @segmentep.statefuls
        @costvs = @segmentep.cos_test_vectors
        if @segment.is_a? OnNetOvc
          @siblings = @segment.on_net_ovc_end_point_ennis.reject {|ep| ep == @segmentep}
        elsif @segment.is_a? OffNetOvc
          @enni_siblings = @segment.ovc_end_point_ennis.reject {|ep| ep == @segmentep}
          @uni_siblings = @segment.ovc_end_point_unis.reject {|ep| ep == @segmentep}
        end
        format.html { render :action => 'edit_segmentep' }
      end
    end
  end

  def config_gen_segmentep
    @segmentep = SegmentEndPoint.find(params[:id])
    @config_text = @segmentep.generate_service_monitoring_config
    @lines = @config_text.count '\n'
    respond_to do |format|
      format.html # config_gen_segmentep.html.erb
    end
  end

  def choose_demarc_segmentep
   @segmentep = SegmentEndPoint.find(params[:id])
   @demarcs = @segmentep.get_candidate_demarcs
   respond_to do |format|
      format.html # choose_demarc_segmentep.html.erb
   end
  end

  def link_demarc_segmentep
   @segmentep = SegmentEndPoint.find(params[:id])

    if (!params[:demarc])||(params[:demarc][:id] == "")
      @demarcs = @segmentep.get_candidate_demarcs
      render_action_id_with_errors(
         @segmentep, 'choose_demarc_segmentep', @segmentep.id,
         "Denmarc info is invalid - try again" )
      return
    end

    @demarc = Demarc.find(params[:demarc][:id])
    @segmentep.demarc_id = @demarc.id
    @segmentep.generate_mep_id

    respond_to do |format|
      #We can not run validations due to new inner/outer tag validations that will clash on save
      # OnNetOvcEndPointEnni#valid_tag_presence?
      #TODO: Fix or Remove Else clause
      if @segmentep.save :validate => false
        flash[:notice] = "Endpoint: #{@segmentep.cenx_name} linked OK to #{@demarc.cenx_name}"
        format.html { redirect_to(:action => 'edit_segmentep', :id => @segmentep.id ) }
      else
        flash[:notice] = "Endpoint: #{@segmentep.cenx_name} link failed for #{@demarc.cenx_name}"
        @segmentep.demarc_id = nil
        @segmenteps = @segmentep.segment_end_points
        @segmentepts = @segmentep.get_candidate_segment_end_point_types
        @coses = @segmentep.cos_end_points
        @costvs = @segmentep.cos_test_vectors
        @ehs = (@segmentep.kind_of? OnNetOvcEndPointEnni) ? @segmentep.alarm_histories + @segmentep.sm_histories : []
        @states = @segmentep.statefuls
        @segment = @segmentep.segment
        if @segment.is_a? OnNetOvc
          @siblings = @segment.on_net_ovc_end_point_ennis.reject {|ep| ep == @segmentep}
        elsif @segment.is_a? OffNetOvc
          @enni_siblings = @segment.ovc_end_point_ennis.reject {|ep| ep == @segmentep}
          @uni_siblings = @segment.ovc_end_point_unis.reject {|ep| ep == @segmentep}
        end
        format.html { render :action => 'edit_segmentep' }
      end
    end
  end

  def unlink_demarc_segmentep
    @segmentep = SegmentEndPoint.find(params[:id])
    @demarc = Demarc.find(@segmentep.demarc_id)
    @segmentep.demarc_id = nil
#    @segmentep.stags = nil
#    @segmentep.ctags = nil
    @segmentep.generate_mep_id
    
    respond_to do |format|
      if @segmentep.save
        flash[:notice] = "Endpoint: #{@segmentep.cenx_name} unlinked OK from #{@demarc.cenx_name}"
        format.html { redirect_to(:action => 'edit_segmentep', :id => @segmentep.id ) }
      else
        @segmenteps = @segmentep.segment_end_points
        @segmentepts = @segmentep.get_candidate_segment_end_point_types
        @coses = @segmentep.cos_end_points
        @ehs = (@segmentep.kind_of? OnNetOvcEndPointEnni) ? @segmentep.alarm_histories + @segmentep.sm_histories : []
        @states = @segmentep.statefuls
        @costvs = @segmentep.cos_test_vectors
        @ehs = (@segmentep.kind_of? OnNetOvcEndPointEnni) ? @segmentep.alarm_histories + @segmentep.sm_histories : []
        @states = @segmentep.statefuls
        @segment = @segmentep.segment
        if @segment.is_a? OnNetOvc
          @siblings = @segment.on_net_ovc_end_point_ennis.reject {|ep| ep == @segmentep}
        elsif @segment.is_a? OffNetOvc
          @enni_siblings = @segment.ovc_end_point_ennis.reject {|ep| ep == @segmentep}
          @uni_siblings = @segment.ovc_end_point_unis.reject {|ep| ep == @segmentep}
        end
        flash[:error] = "Endpoint: #{@segmentep.cenx_name} unlink failed for #{@demarc.cenx_name}"
        format.html { render :action => 'edit_segmentep' }
      end
    end
  end
  
  def choose_segmentep_segmentep
    @segmentep = SegmentEndPoint.find(params[:id])
    @segmenteps = @segmentep.get_candidate_endpoints_to_link_with
    respond_to do |format|
      format.html # choose_port_port.html.erb
    end
  end

  def link_segmentep_segmentep
    @segmentep = SegmentEndPoint.find(params[:id])

    if (!params[:segmentep])||(params[:segmentep][:id] == "")
      @segmenteps = @segmentep.get_candidate_endpoints_to_link_with
      render_action_id_with_errors(
         @segmentep, 'choose_segmentep_segmentep', @segmentep.id,
         "Endpoint info is invalid - try again" )
      return
    end

    @other_segment = SegmentEndPoint.find(params[:segmentep][:id])

    if @other_segment.segment_end_points.include? @segmentep
      @segmenteps = @segmentep.get_candidate_endpoints_to_link_with
      render_action_id_with_errors(
         @segmentep, 'choose_segmentep_segmentep', @segmentep.id,
         "Endpoint: #{@other_segment.cenx_name} is already connected to #{@segmentep.cenx_name} - try again" )
      return
    end

    @segmentep.segment_end_points.push(@other_segment)
    #TODO use transaction
    respond_to do |format|
      if @segmentep.valid?
        flash[:notice] = "Endpoint #{@segmentep.cenx_name} connected OK to Endpoint #{@other_segment.cenx_name}"
        format.html { redirect_to(:action => 'edit_segmentep', :id => @segmentep.id ) }
      else
        @segmentep.segment_end_points.delete(@other_segment)
        @segmentep.valid?
        @segmentepts = @segmentep.get_candidate_segment_end_point_types
        @segmenteps = @segmentep.segment_end_points
        @coses = @segmentep.cos_end_points
        @ehs = (@segmentep.kind_of? OnNetOvcEndPointEnni) ? @segmentep.alarm_histories + @segmentep.sm_histories : []
        @states = @segmentep.statefuls
        @costvs = @segmentep.cos_test_vectors
        @ehs = (@segmentep.kind_of? OnNetOvcEndPointEnni) ? @segmentep.alarm_histories + @segmentep.sm_histories : []
        @states = @segmentep.statefuls
        @segment = @segmentep.segment
        if @segment.is_a? OnNetOvc
          @siblings = @segment.on_net_ovc_end_point_ennis.reject {|ep| ep == @segmentep}
        elsif @segment.is_a? OffNetOvc
          @enni_siblings = @segment.ovc_end_point_ennis.reject {|ep| ep == @segmentep}
          @uni_siblings = @segment.ovc_end_point_unis.reject {|ep| ep == @segmentep}
        end
        flash[:error] = "Endpoint #{@segmentep.cenx_name} connect failed to Port: #{@other_segment.cenx_name}"

        format.html { render :action => 'edit_segmentep' }
      end
    end
  end

  def unlink_segmentep_segmentep
    @segmentep = SegmentEndPoint.find(params[:segmentep_id])
    @other_segmentep = SegmentEndPoint.find(params[:id])
    @segmentep.segment_end_points.delete(@other_segmentep)
    #TODO use transaction
    respond_to do |format|
      if @segmentep.save
         @other_segmentep.save
         flash[:notice] = "Endpoint: #{@segmentep.cenx_name} disconnected OK from #{@other_segmentep.cenx_name}"
         format.html { redirect_to(:action => 'edit_segmentep', :id => @segmentep.id ) }
      else
        @segmenteps = @segmentep.segment_end_points
        @segmentepts = @segmentep.get_candidate_segment_end_point_types
        @coses = @segmentep.cos_end_points
        @ehs = (@segmentep.kind_of? OnNetOvcEndPointEnni) ? @segmentep.alarm_histories + @segmentep.sm_histories : []
        @states = @segmentep.statefuls
        @costvs = @segmentep.cos_test_vectors
        @ehs = (@segmentep.kind_of? OnNetOvcEndPointEnni) ? @segmentep.alarm_histories + @segmentep.sm_histories : []
        @states = @segmentep.statefuls
        @segment = @segmentep.segment
        if @segment.is_a? OnNetOvc
          @siblings = @segment.on_net_ovc_end_point_ennis.reject {|ep| ep == @segmentep}
        elsif @segment.is_a? OffNetOvc
          @enni_siblings = @segment.ovc_end_point_ennis.reject {|ep| ep == @segmentep}
          @uni_siblings = @segment.ovc_end_point_unis.reject {|ep| ep == @segmentep}
        end
        flash[:error] = "Endpoint: #{@segmentep.cenx_name} disconnected failed for from #{@other_segmentep.cenx_name}"
        format.html { render :action => 'edit_segmentep' }
      end
    end
  end


  def destroy_segmentep
    @segmentep = SegmentEndPoint.find(params[:id])
    @segment_id = @segmentep.segment_id
    @segmentep.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :edit_segment, :id => @segment_id) }
    end
  end

  #
  # COS End Point Management
  #
  def new_cose
    @segmentep = SegmentEndPoint.find(params[:id])
    @cose = (@segmentep.segment.kind_of? OnNetOvc) ? CenxCosEndPoint.new : CosEndPoint.new
    @cose.segment_end_point = @segmentep
    @cosis = @cose.get_candidate_cos_instances

    respond_to do |format|
      format.html # new_cose.html.erb
    end
  end
                                        
  def options_cose_cost_values
    @cose = (params[:cose_id] != nil && params[:cose_id] != "") ? CosEndPoint.find(params[:cose_id]) : CosEndPoint.new
    @segmentep = SegmentEndPoint.find(params[:segmentep_id])
    @cose.segment_end_point = @segmentep
    @cosi = CosInstance.find(params[:cosi_id])
    @cos_id_type = (@segmentep.kind_of? OvcEndPointEnni) ? @cosi.cos_id_type_enni : @cosi.cos_id_type_uni
    @cos_mapping_ingress = (@segmentep.kind_of? OvcEndPointEnni) ? @cosi.cos_mapping_enni_ingress : @cosi.cos_mapping_enni_ingress
    @cos_marking_egress = (@segmentep.kind_of? OvcEndPointEnni) ? @cosi.cos_marking_enni_egress : @cosi.cos_marking_uni_egress
    ingress_policing_locale = (@segmentep.kind_of? OvcEndPointUni) ? "UNI-INGRESS" : "ENNI-INGRESS"
    @ingress_bwpt = @cosi.get_bwp_at_locale ingress_policing_locale
    egress_policing_locale = (@segmentep.kind_of? OvcEndPointUni) ? "UNI-EGRESS" : "ENNI-EGRESS"
    @egress_bwpt = @cosi.get_bwp_at_locale egress_policing_locale

    if @segmentep.is_connected_to_test_port
      ServiceCategories::CENX_TEST_COS_VALUES[:rates].each do |method, value|
        @cose.send("#{method}=", value)
      end

      @cose.ingress_rate_limiting = ServiceCategories::CENX_TEST_COS_VALUES[:rlm]["Ingress"]
      @cose.egress_rate_limiting = ServiceCategories::CENX_TEST_COS_VALUES[:rlm]["Egress"]
      
      fwding_class = ServiceCategories::ON_NET_OVC_COS_TYPES[@cosi.class_of_service_type.name][:fwding_class]
      @cose.ingress_mapping = ServiceCategories::CENX_TEST_COS_VALUES[:mapping][fwding_class]
      @cose.egress_marking = ServiceCategories::CENX_TEST_COS_VALUES[:mapping][fwding_class]
    end

    render :partial => 'options_cose_cost_values'
  end

  def get_default_burst_rate
    cosi = CosInstance.find(params["cos_instance_id"])
    rate = params["rate"].to_i
    method = params["method"]
    value = CenxCosEndPoint.get_default_burst_rate(cosi, rate, method)
    element = "#{params["direction"]}_#{method == "commited" ? "cbs" : "ebs"}_kB"

    render :update do |page|
      page[element].value = value
    end
  end

  def save_cose
    @segmentep = SegmentEndPoint.find(params[:id])
    @cose = (@segmentep.segment.kind_of? OnNetOvc) ? CenxCosEndPoint.new(form_to_params(params, "cose", ["id", "name", "bwpt_type", "rate_limiting_mechanism"])) : CosEndPoint.new(form_to_params(params, "cose", ["id", "name", "bwpt_type", "rate_limiting_mechanism"]))
    @cose.segment_end_point = @segmentep

    respond_to do |format|
      if @cose.save
        flash[:notice] = "Cos End Point: #{@cose.cos_name} created OK"
        format.html { redirect_to(:action => :edit_segmentep, :id => @segmentep.id) }
      else
        @cosis = @cose.get_candidate_cos_instances
        @cos_id_type = "TBD"
        format.html { render :action => 'new_cose' }
      end
    end
  end

  def edit_cose
    @cose = CosEndPoint.find(params[:id])
    @segmentep = @cose.segment_end_point
    @cosis = @segmentep.cos_instances
    @cosi = @cose.cos_instance
    @coses = @cose.cos_end_points
    @cos_id_type = (@segmentep.kind_of? OvcEndPointEnni) ? @cosi.cos_id_type_enni : @cosi.cos_id_type_uni
    @cos_mapping_ingress = (@segmentep.kind_of? OvcEndPointEnni) ? @cosi.cos_mapping_enni_ingress : @cosi.cos_mapping_enni_ingress
    @cos_marking_egress = (@segmentep.kind_of? OvcEndPointEnni) ? @cosi.cos_marking_enni_egress : @cosi.cos_marking_uni_egress
    ingress_policing_locale = (@segmentep.kind_of? OvcEndPointUni) ? "UNI-INGRESS" : "ENNI-INGRESS"
    @ingress_bwpt = @cosi.get_bwp_at_locale ingress_policing_locale
    egress_policing_locale = (@segmentep.kind_of? OvcEndPointUni) ? "UNI-EGRESS" : "ENNI-EGRESS"
    @egress_bwpt = @cosi.get_bwp_at_locale egress_policing_locale
    @costvs = @cose.cos_test_vectors
  end

  def update_cose
     @cose = CosEndPoint.find(params[:id])
     @bwpt = @cose.cos_instance.get_bwp_at_locale(params[:bwpt_type])

    respond_to do |format|
      if @cose.update_attributes(form_to_params(params, "cose", ["id", "name", "bwpt_type", "rate_limiting_mechanism"]))
        flash[:notice] = "Cos End Point: #{@cose.cos_name} updated OK"
        format.html { redirect_to(:action => :edit_cose, :id => @cose.id )  }
      else
        @segmentep = @cose.segment_end_point
        @cosis = @segmentep.cos_instances
        @cosi = @cose.cos_instance
        @coses = @cose.cos_end_points
        @cos_id_type = (@segmentep.kind_of? OvcEndPointEnni) ? @cosi.cos_id_type_enni : @cosi.cos_id_type_uni
        @cos_mapping_ingress = (@segmentep.kind_of? OvcEndPointEnni) ? @cosi.cos_mapping_enni_ingress : @cosi.cos_mapping_enni_ingress
        @cos_marking_egress = (@segmentep.kind_of? OvcEndPointEnni) ? @cosi.cos_marking_enni_egress : @cosi.cos_marking_uni_egress
        ingress_policing_locale = (@segmentep.kind_of? OvcEndPointUni) ? "UNI-INGRESS" : "ENNI-INGRESS"
        @ingress_bwpt = @cosi.get_bwp_at_locale ingress_policing_locale
        egress_policing_locale = (@segmentep.kind_of? OvcEndPointUni) ? "UNI-EGRESS" : "ENNI-EGRESS"
        @egress_bwpt = @cosi.get_bwp_at_locale egress_policing_locale
        @costvs = @cose.cos_test_vectors
        format.html { render :action => 'edit_cose' }
      end
    end
  end
  
  def choose_cose_cose
    @cose = CosEndPoint.find(params[:id])
    @coses = @cose.get_candidate_cos_end_points
    respond_to do |format|
      format.html # choose_cose_cose.html.erb
    end
  end

  def link_cose_cose
    @cose = CosEndPoint.find(params[:id])

    if (!params[:cose])||(params[:cose][:id] == "")
      @coses = @cose.get_candidate_cos_end_points
      render_action_id_with_errors(
        @cose, 'choose_cose_cose', @cose.id,
         "Endpoint info is invalid - try again" )
      return
    end

    @other_cose = CosEndPoint.find(params[:cose][:id])

    if !(@cose.get_candidate_cos_end_points.include? @other_cose) || !(@other_cose.get_candidate_cos_end_points.include? @cose)
      @coses = @cose.get_candidate_cos_end_points
      render_action_id_with_errors(
        @cose, 'choose_cose_cose', @cose.id,
        "Endpoint: #{@cose.cos_name} may not be connected to #{@other_cose.cos_name} - try again" )
      return
    end

    @cose.cos_end_points.push(@other_cose)
    #TODO use transaction
    respond_to do |format|
      if @cose.valid?
        flash[:notice] = "Endpoint #{@cose.cos_name} connected OK to Endpoint #{@other_cose.cos_name}"
        format.html { redirect_to(:action => 'edit_cose', :id => @cose.id ) }
      else
        @cose.cos_end_points.delete(@other_cose)
        @segmentep = @cose.segment_end_point
        @cosis = @cose.get_candidate_cos_instances
        @cosi = @cose.cos_instance
        @coses = @cose.cos_end_points(true)
        @cos_id_type = (@segmentep.kind_of? OvcEndPointEnni) ? @cosi.class_of_service_type.cos_id_type_enni : @cosi.class_of_service_type.cos_id_type_uni
        @costvs = @cose.cos_test_vectors
        @ehs = @cose.alarm_histories + @cose.sm_histories
        flash[:error] = "Endpoint #{@cose.cos_name} connect failed to Endpoint: #{@other_cose.cos_name}"

        format.html { render :action => 'edit_cose' }
      end
    end
  end

  def unlink_cose_cose
    @cose = CosEndPoint.find(params[:cose_id])
    @other_cose = CosEndPoint.find(params[:id])
    @cose.cos_end_points.delete(@other_cose)
    #TODO use transaction
    respond_to do |format|
      if @cose.save
         @other_cose.save
         flash[:notice] = "Endpoint: #{@cose.cos_name} disconnected OK from #{@other_cose.cos_name}"
         format.html { redirect_to(:action => 'edit_cose', :id => @cose.id ) }
      else
        @coses = @cose.cos_end_points
        flash[:error] = "Endpoint: #{@cose.cos_name} disconnected failed for from #{@other_cose.cos_name}"
        format.html { render :action => 'edit_cose' }
      end
    end
  end

  def destroy_cose
    @cose = CosEndPoint.find(params[:id])
    @segmentep_id = @cose.segment_end_point_id
    @cose.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :edit_segmentep, :id => @segmentep_id) }
    end
  end
  
  #
  # CoS Test Vector Management
  #
  def index_costv
    @costvs = CosTestVector.all
    respond_to do |format|
      format.html # index_costv.html.erb
    end
  end
  
  def new_costv
    if params[:costv_class] == "BxCosTestVector"
      @costv = BxCosTestVector.new
    elsif params[:costv_class] == "SpirentCosTestVector"
      @costv = SpirentCosTestVector.new
    else
      @costv = CosTestVector.new
    end
    @circuit_formats = CircuitIdFormat.constants.collect {|c| c.to_s if CircuitIdFormat.const_get(c).is_a?(Class)}
    @cose = CosEndPoint.find(params[:id])
    @costv.cos_end_point = @cose
    @costv.test_instance_class_id = 'TBD'
    @slgts = @cose.get_candidate_service_level_guarantee_types
    @slgt = ServiceLevelGuaranteeType.new
    respond_to do |format|
      format.html # new_costv.html.erb
     end

  end

  def options_costv_slgt_values
    @costv = (params[:costv_id] != nil && params[:costv_id] != "") ? CosTestVector.find(params[:costv_id]) : CosTestVector.new
    @cose = CosEndPoint.find(params[:cose_id])
    @costv.cos_end_point = @cose
    @slgts = @cose.get_candidate_service_level_guarantee_types
    @slgt = ServiceLevelGuaranteeType.find(params[:slgt_id])

    render :partial => 'options_costv_slgt_values'
  end

  def save_costv
    if params[:costv_class] == "BxCosTestVector"
      @costv = BxCosTestVector.new(form_to_params(params, "costv", ["id","test_target_address","bx_event_filter","cenx_id","cenx_name","cenx_bx_sla_name", "costv_class"]))
    elsif params[:costv_class] == "SpirentCosTestVector"
      @costv = SpirentCosTestVector.new(form_to_params(params, "costv", ["id","test_target_address","bx_event_filter","cenx_id","cenx_name","cenx_bx_sla_name", "costv_class"]))
    else
      @costv = CosTestVector.new(form_to_params(params, "costv", ["id","test_target_address","bx_event_filter","cenx_id","cenx_name","cenx_bx_sla_name", "costv_class"]))
    end
    
    @cose = CosEndPoint.find(params[:id])
    @costv.cos_end_point = @cose

    respond_to do |format|
      if @costv.save
        flash[:notice] = "Cos Test Vector: #{@costv.cenx_name} created OK"
        format.html { redirect_to(:action => :edit_costv, :id => @costv.id) }
      else
        @circuit_formats = CircuitIdFormat.constants.collect {|c| c.to_s if CircuitIdFormat.const_get(c).is_a?(Class)}
        @costv.test_instance_class_id = 'TBD'
        @slgts = @cose.get_candidate_service_level_guarantee_types
        @slgt = ServiceLevelGuaranteeType.new
        format.html { render :action => 'new_costv' }
      end
    end
  end

  def edit_costv
    @costv = CosTestVector.find(params[:id])
    @circuit_formats = CircuitIdFormat.constants.collect {|c| c.to_s if CircuitIdFormat.const_get(c).is_a?(Class)}
    @cose = @costv.cos_end_point
    @slgts = @cose.get_candidate_service_level_guarantee_types
    @slgt = @costv.service_level_guarantee_type
    @sla_delay_threshold = @slgt.default_delay_us
    @sla_dv_threshold = @slgt.default_delay_variation_us
    @sla_flr_threshold = @slgt.default_frame_loss_ratio_percent
    @ehs = @costv.alarm_histories + @costv.sm_histories
  end

  def update_costv
    @costv = CosTestVector.find(params[:id])
    respond_to do |format|
      if @costv.update_attributes(form_to_params(params, "costv", ["id","test_target_address","bx_event_filter","cenx_id","cenx_name","cenx_bx_sla_name"]))
        flash[:notice] = "Cos Test Vector: #{@costv.cenx_name} updated OK"
        format.html { redirect_to(:action => :edit_costv, :id => @costv.id )  }
      else
        @circuit_formats = CircuitIdFormat.constants.collect {|c| c.to_s if CircuitIdFormat.const_get(c).is_a?(Class)}
        @cose = @costv.cos_end_point
        @slgts = @cose.get_candidate_service_level_guarantee_types
        @slgt = @costv.service_level_guarantee_type
        @ehs = @costv.alarm_histories + @costv.sm_histories
        format.html { render :action => 'edit_costv' }
      end
    end
  end

  def get_test_vector_info
    @costv = CosTestVector.find(params[:costv_id])
    result_ok, sla_id, service_instance_id, test_instance_id, test_instance_class_id = @costv.get_bx_vector_info
    @costv.sla_id = sla_id
    @costv.service_instance_id = service_instance_id
    @costv.test_instance_id = test_instance_id
    @costv.test_instance_class_id = test_instance_class_id

    #@costv.errors.add(:base,"sla_id = #{sla_id}, service_instance_id = #{service_instance_id}, test_instance_class_id = #{test_instance_class_id} - OK")
    if result_ok
      flash[:notice] = "Retrieved: sla_id = #{sla_id}, service_instance_id = #{service_instance_id}, test_instance_class_id = #{test_instance_class_id} - OK"
    else
      flash[:error] = "Retrieved: sla_id = #{sla_id}, service_instance_id = #{service_instance_id}, test_instance_class_id = #{test_instance_class_id} - FAILED"
    end

    render :partial => 'options_costv_test_ids'
    
  end

  def config_gen_costv
    @costv = CosTestVector.find(params[:id])
    @config_text = @costv.generate_config
    @lines = @config_text.count '\n'
    respond_to do |format|
      format.html # config_gen_costv.html.erb
    end
  end

  def destroy_costv
    @costv = CosTestVector.find(params[:id])
    @cose_id = @costv.cos_end_point_id
    @costv.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :edit_cose, :id => @cose_id) }
    end
  end

  def get_costv_default_warning
    error_threshold = params["value"]
    value = CosTestVector.get_default_warning_threshold(error_threshold)
    element = params["element"]
    render :update do |page|
      page[element].value = value
    end

  end
  
  #
  # Demarc Management
  #
  def filter_sp_enni_new
    sp = ServiceProvider.find(params[:spid])
    @ennis = sp.enni_news
    render :partial => 'index_filter_enni_new'
  end

  def index_member_demarc
    @demarcs = Demarc.find_member_demarcs
    @show_order_button = true
    respond_to do |format|
      format.html { render "index_demarc" }
    end
  end

  def index_cenx_demarc
    @demarcs = Demarc.find_cenx_demarcs
    @ons = OperatorNetwork.find_cenx_operator_networks
    @show_order_button = false
    respond_to do |format|
      format.html { render "index_demarc" }
    end
  end

  def index_member_enni
    @ennis = EnniNew.find_member_ennis
    @show_order_button = true
    respond_to do |format|
      format.html { render "index_enni" }
    end
  end

  def index_cenx_enni
    @ennis = EnniNew.find_cenx_ennis
    @ons = OperatorNetwork.find_cenx_operator_networks
    @show_order_button = false
    respond_to do |format|
      format.html { render "index_enni" }
    end
  end

  def index_enni_new_order
    @sos = EnniOrderNew.find_enni_order_rows_for_index
    @sps = ServiceProvider.find_rows_for_index
    @on = OperatorNetwork.new #hack - TODO Fix _select_sp to use collection_select
    respond_to do |format|
      format.html # index_enni_new_order.html.erb
    end
  end

  def index_demarc_order
    @sos = (DemarcOrder.find_demarc_order_rows_for_index).sort{|a,b| a.id <=> b.id}
    @sps = ServiceProvider.find_rows_for_index
    @on = OperatorNetwork.new #hack - TODO Fix _select_sp to use collection_select
    respond_to do |format|
      format.html # index_enni_new_order.html.erb
    end
  end
  
  def select_orders_from_type sos, type
    return case type
      when "demarc" then sos.select{|so| so.ordered_entity_type == "Demarc" && so.ordered_entity_subtype != "ENNI"}
      when "enni_new" then sos.select{|so| so.ordered_entity_type == "Demarc" && so.ordered_entity_subtype == "ENNI"}
      when "offovc" then sos.select{|so| so.ordered_entity_type == "Segment" && so.ordered_entity_subtype == "OffNetOvc"}
      when "onovc" then sos.select{|so| so.ordered_entity_type == "Segment" && so.ordered_entity_subtype == "OnNetOvc"}
      when "bulk" then sos.select{|so| so.is_a?(BulkOrder)}
    end
  end
  
  private :select_orders_from_type
  
  def filter_sp_so
    sp = ServiceProvider.find(params[:spid])
    sos = sp.service_orders
    puts "<<<<<<< GOT #{sos.size}"
    @sos = select_orders_from_type sos, params[:tail]
    render :partial => "index_filter_#{params[:tail]}_order"
  end

  def new_so
    if(params[:ordered_entity_type] == "ENNI")
      @so = EnniOrderNew.new
    elsif(params[:ordered_entity_type] == "Demarc")
      @so = DemarcOrder.new
    elsif(params[:ordered_entity_type] == "OnNetOvc")
      @so = OnNetOvcOrder.new
    elsif(params[:ordered_entity_type] == "OffNetOvc")
      @so = OffNetOvcOrder.new
    end

    @so.ordered_entity_type = params[:ordered_entity_type]
    @so.cenx_id = "To Be Determined"
    @sp = ServiceProvider.new
    @on = OperatorNetwork.new
    @oon = OperatorNetwork.new
    @sps = ServiceProvider.find_rows_for_index
    @oeg = nil
    @ons = []
    @contacts = []
    @oes = [] #ordered_entities
    @paths = []
    @oegs = []
    @ots = []
    
    if params[:oeg_id]
      @oeg = OrderedEntityGroup.find(params[:oeg_id])
      @ot = @oeg.order_type
      @osp = @ot.operator_network_type.service_provider
      @oon.service_provider = @osp
      @so.ordered_operator_network = @oon
      @so.ordered_entity_group = @oeg
      @oons = @ot.operator_network_type.operator_networks
    end
    
    if params[:oet_id]
      @so.ordered_object_type_id = params[:oet_id]
      @so.ordered_object_type_type = (@so.is_a? DemarcOrder) ? "DemarcType" : "SegmentType"
      @oets = nil
    else
      @oets = @so.get_candidate_ordered_object_types
    end
  end

  def options_order_so
    if(params[:ordered_entity_type] == "ENNI")
      @so = EnniOrderNew.new
    elsif(params[:ordered_entity_type] == "Demarc")
      @so = DemarcOrder.new
    elsif(params[:ordered_entity_type] == "OnNetOvc")
      @so = OnNetOvcOrder.new
    elsif(params[:ordered_entity_type] == "OffNetOvc")
      @so = OffNetOvcOrder.new
    end
    @so.ordered_entity_type = params[:ordered_entity_type]
    @sp = ServiceProvider.where("id = ?", params[:spid]).first
    @sp = ServiceProvider.new if(!@sp)
    @on = OperatorNetwork.new
    @oon = OperatorNetwork.new
    @so.operator_network = @on
    @so.ordered_operator_network = @oon
    @ons = @sp.operator_networks
    @contacts = @sp.contacts
    @paths = @sp.paths
    @oes = []
    render :partial => 'options_so'
  end

  def select_ordered_entity_so
    if(params[:ordered_entity_type] == "ENNI")
      @so = EnniOrderNew.new
    elsif(["Demarc", "UNI"].include? params[:ordered_entity_type])
      @so = DemarcOrder.new
    elsif(params[:ordered_entity_type] == "OnNetOvc")
      @so = OnNetOvcOrder.new
      @path = Path.find(params[:path_id])
    elsif(params[:ordered_entity_type] == "OffNetOvc")
      @so = OffNetOvcOrder.new
      @path = Path.find(params[:path_id])
    end
    @so.ordered_entity_type = params[:ordered_entity_type]
    @so.action = params[:so_action]
    @oes = []
    if params[:so_action] != "New"
      @oon = OperatorNetwork.where("id = ?", params[:on_id]).first
      @oon = OperatorNetwork.new if (!@oon)
      if(params[:ordered_entity_type] == "ENNI")
        @oes = @oon.enni_news
      elsif(params[:ordered_entity_type] == "Demarc")
        @oes = @oon.demarcs - @oon.enni_news
      elsif(params[:ordered_entity_type] == "OnNetOvc")
        @oes = @path.on_net_ovcs
      elsif(params[:ordered_entity_type] == "OffNetOvc")
        @oes = @path.off_net_ovcs.select{|offovc| offovc.operator_network_id = @oon.id}
      end
    end
    @oes.reject!{|oe| oe.get_latest_order.nil?}
    
    render :partial => 'select_ordered_entity_so'
  end
  
  def options_son_so
    @so = ServiceOrder.new
    @sp = ServiceProvider.find(params[:spid])
    @ons = @sp.operator_networks
    render( :partial=> 'select_on', :object => @so,
      :locals => {
        :prompt => '*Seller Network:',
        :action => 'options_oeg_so',
        :update => 'oeg_select_field',
        :prefix => 'ordered_',
        :with => "'onid=' + value + '&ordered_entity_type=' + $('ordered_entity_type').value" })
  end
  
  def options_ot_so
    @so = ServiceOrder.new
    @oon = OperatorNetwork.find(params[:onid])
    @so.ordered_operator_network = @oon
    @ots = @oon.operator_network_type.order_types
    render( :partial=> 'select_ot', :object => @so,
      :locals => {
        :prompt => '*Order Type:',
        :action => 'options_oeg_so',
        :update => 'oeg_select_field',
        :with => "'otid=' + value + '&ordered_entity_type=' + $('ordered_entity_type').value" })
  end
  
  def options_oeg_so
    if(params[:ordered_entity_type] == "ENNI")
      @so = EnniOrderNew.new
    elsif(params[:ordered_entity_type] == "Demarc")
      @so = DemarcOrder.new
    elsif(params[:ordered_entity_type] == "OnNetOvc")
      @so = OnNetOvcOrder.new
      @path = Path.find(params[:path_id])
    elsif(params[:ordered_entity_type] == "OffNetOvc")
      @so = OffNetOvcOrder.new
    end
    if params[:otid]
      @ot = OrderType.find(params[:otid])
      @oegs = @so.get_candidate_ordered_entity_groups @ot
    elsif params[:onid]
      @on = OperatorNetwork.find(params[:onid])
      @oegs = @on.operator_network_type.ordered_entity_groups
    end
    render(:partial=> 'select_oeg', :object => @so,
      :locals => {
        :prompt => 'Ordered Entity Group:',
        :action => 'options_oet_so',
        :update => 'service_order_oet_field',
        :with => "'oeg_id=' + value + '&ordered_entity_type=' + $('ordered_entity_type').value"
      })
  end
  
  def options_oet_so
    if(params[:ordered_entity_type] == "ENNI")
      @so = EnniOrderNew.new
    elsif(params[:ordered_entity_type] == "Demarc")
      @so = DemarcOrder.new
    elsif(params[:ordered_entity_type] == "OnNetOvc")
      @so = OnNetOvcOrder.new
      @path = Path.find(params[:path_id])
    elsif(params[:ordered_entity_type] == "OffNetOvc")
      @so = OffNetOvcOrder.new
    end
    @oeg = OrderedEntityGroup.where("id = ?", params[:oeg_id]).first
    @so.ordered_entity_group = @oeg
    @oets = @so.get_candidate_ordered_object_types
    render( :partial=> 'select_oet_so', :object => @so)
  end

  def save_so
    if(params[:ordered_entity_type] == "ENNI")
      @so = EnniOrderNew.new(form_to_params(params, "so", ["id", "cenx_id", "order_type_id", /(ordered_)?service_provider_id/, "affected_entity_id", "ordered_entity_type", MemberAttrInstance.form_id_pattern]))
      @so.ordered_entity_type = "Demarc"
      @so.ordered_entity_subtype = "ENNI"
      @so.ordered_object_type_type = "DemarcType" if @so.ordered_object_type_id
    elsif (["Demarc", "UNI"].include? params[:ordered_entity_type])
      @so = DemarcOrder.new(form_to_params(params, "so", ["id", "cenx_id", "order_type_id", /(ordered_)?service_provider_id/, "affected_entity_id", "ordered_entity_type", "oeg_id", "oet_id", MemberAttrInstance.form_id_pattern]))
      @so.ordered_entity_type = "Demarc"
      @so.ordered_object_type_type = "DemarcType" if @so.ordered_object_type_id

      #TODO fix this - need to handle both UNI and Demarc
#      @so.ordered_entity_subtype = (@so.ordered_object_type && @so.ordered_object_type.is_a?(UniType)) ? "UNI" : ""
      @so.ordered_entity_subtype = "UNI"
    elsif (params[:ordered_entity_type] == "OnNetOvc")
      @so = OnNetOvcOrder.new(form_to_params(params, "so", ["id", "cenx_id", "order_type_id", /(ordered_)?service_provider_id/, "affected_entity_id", "ordered_entity_type", "so_type", MemberAttrInstance.form_id_pattern]))
      @so.ordered_entity_type = "Segment"
      @so.ordered_entity_subtype = "OnNetOvc"
      @so.ordered_object_type_type = "SegmentType" if @so.ordered_object_type_id
    elsif(params[:ordered_entity_type] == "OffNetOvc")
      @so = OffNetOvcOrder.new(form_to_params(params, "so", ["id", "cenx_id", "order_type_id", /(ordered_)?service_provider_id/, "affected_entity_id", "ordered_entity_type", "so_type", "oeg_id", "oet_id", MemberAttrInstance.form_id_pattern]))
      @so.ordered_entity_type = "Segment"
      @so.ordered_entity_subtype = "OffNetOvc"
      @so.ordered_object_type_type = "SegmentType" if @so.ordered_object_type_id
    end
    
    if params[:affected_entity_id] && params[:affected_entity_id].to_i != 0
      if(params[:ordered_entity_type] == "ENNI")
        @so.ordered_entity = EnniNew.find(params[:affected_entity_id])
      elsif (params[:ordered_entity_type] == "Demarc")
        @so.ordered_entity = Demarc.find(params[:affected_entity_id])
      elsif (params[:ordered_entity_type] == "OnNetOvc")
        @so.ordered_entity = OnNetOvc.find(params[:affected_entity_id])
      elsif(params[:ordered_entity_type] == "OffNetOvc")
        @so.ordered_entity = OffNetOvc.find(params[:affected_entity_id])
      else
        SW_ERR "save_so ERROR unexpected ordered entity #{params[:ordered_entity_type]}"
        @so.ordered_entity_id = params[:affected_entity_id]
      end
    end
    
    @so.set_member_attr_instances params

    respond_to do |format|
      if @so.save
        if params[:affected_entity_id]
          flash[:notice] = "#{params[:ordered_entity_type]} Change Order created; Cenx ID of the order is #{@so.cenx_id} - please modify #{params[:ordered_entity_type]} data"
          if(@so.ordered_entity_type == "Demarc")
            format.html { redirect_to(:action => :edit_demarc, :id => params[:affected_entity_id] ) }
          else
            format.html { redirect_to(:action => :edit_segment, :id => params[:affected_entity_id] ) }
          end
        else
          flash[:notice] = "New #{params[:ordered_entity_type]} Order created; Cenx ID of the order is #{@so.cenx_id} - please enter #{params[:ordered_entity_type]} data"
          if(@so.ordered_entity_type == "Demarc")
            format.html { redirect_to(:action => :new_demarc, :id => @so.is_a?(EnniOrderNew) ? @so.operator_network_id : @so.ordered_operator_network_id, :so_id => @so.id ) }
          else
            format.html { redirect_to(:action => :new_segment, :id => @so.operator_network_id, :so_id => @so.id) }
          end

        end
      else
        @on = @so.operator_network ? @so.operator_network : OperatorNetwork.new
        @oon = @so.ordered_operator_network ? @so.ordered_operator_network : OperatorNetwork.new
        @sp = @so.service_provider ? @so.service_provider : ServiceProvider.new
        @sps = ServiceProvider.find_rows_for_index
        @ons = @sp.operator_networks
        @oons = @so.ordered_service_provider ? @so.ordered_service_provider.operator_networks : nil
        @contacts = @sp.contacts
        @paths = @sp.paths
        @oegs = @oon.new_record? ? [] : @oon.operator_network_type.ordered_entity_groups
        @ots = []
        
        @oes = []
        if params[:so_action] != "New"
          if(params[:ordered_entity_type] == "ENNI")
            @oes = @oon.enni_news
          elsif(params[:ordered_entity_type] == "Demarc")
            @oes = @oon.demarcs - @oon.enni_news
          elsif(params[:ordered_entity_type] == "OnNetOvc")
            @oes = @so.path ? @so.path.on_net_ovcs : []
          elsif(params[:ordered_entity_type] == "OffNetOvc")
            @oes = @so.path ? @so.path.off_net_ovcs.select{|offovc| offovc.operator_network_id = @oon.id} : []
          end
        end
        @oes.reject!{|oe| oe.get_latest_order.nil?}
    
        if params[:oeg_id]
          @oeg = OrderedEntityGroup.find(params[:oeg_id])
          @ot = @oeg.order_type
          @osp = @ot.operator_network_type.service_provider
          @oon.service_provider = @osp
          @so.ordered_operator_network = @oon
          @so.ordered_entity_group = @oeg
          @oons = @ot.operator_network_type.operator_networks
        end
        
    
        if params[:oet_id]
          @so.ordered_object_type_id = params[:oet_id]
          @oets = nil
        else
          @oets = @so.get_candidate_ordered_object_types
        end
        format.html { render :action => "new_so" }
      end
    end
  end

  def edit_so
    @so = ServiceOrder.find(params[:id])
    if @so.is_a?(BulkOrder)
      edit_bo
    else
      @on = @so.operator_network
      @oon = @so.ordered_operator_network
      @sps = ServiceProvider.find_rows_for_index
      @sp = @so.operator_network.service_provider
      @ons = @sp.operator_networks
      @oons = @so.ordered_service_provider ? @so.ordered_service_provider.operator_networks : nil
      @contacts = @sp.contacts
      @paths = @sp.paths
      @oes = @on.demarcs
      @oegs = @oon ? @oon.operator_network_type.ordered_entity_groups : []
      @oets = @so.get_candidate_ordered_object_types
      @states = @so.statefuls
      @ufs = @so.uploaded_files
    end
  end
  
  def options_oe_snapshot_so
    @so = ServiceOrder.find(params[:id])
    @so.ordered_entity_snapshot = @so.ordered_entity.attr_snapshot
    render :partial => "options_oe_snapshot_so"
  end

  def update_so
    @so = ServiceOrder.find(params[:id])
    @on = @so.operator_network
    @oon = @so.ordered_operator_network
    @states = @so.statefuls
    @ufs = @so.uploaded_files
    if @so.is_a?(DemarcOrder) && @so.ordered_object_type && params[:ordered_object_type_id]
      @so.ordered_object_type_id = params[:ordered_object_type_id]
      params[:ordered_entity_subtype] = (@so.ordered_object_type(false).is_a? UniType) ? "UNI" : ""
    end
    
    @so.set_member_attr_instances params
    $count = 0
    
    respond_to do |format|
      if(@so.update_attributes(form_to_params(params, "so", ["id", "cenx_id", "order_type_id", /(ordered_)?service_provider_id/, "ordered_entity_type", MemberAttrInstance.form_id_pattern])))
        if @so.ordered_entity_id
          flash[:notice] = "Order: #{@so.title} updated OK"
          format.html { redirect_to(:action => :edit_so, :id => @so.id) }
        else
          flash[:notice] = "Please Enter New #{@so.ordered_entity_subtype} data for Order #{@so.cenx_id}"
          if(@so.ordered_entity_type == "Demarc")
            format.html { redirect_to(:action => :new_demarc, :id => @so.is_a?(EnniOrderNew) ? @on.id : @oon.id, :so_id => @so.id ) }
          else
            format.html { redirect_to(:action => :new_segment, :id => @on.id, :so_id => @so.id) }
          end
        end
      else
        @sps = ServiceProvider.find_rows_for_index
        @sp = @so.operator_network.service_provider
        @ons = @sp.operator_networks
        @contacts = @sp.contacts
        @paths = @sp.paths
        @oes = @on.demarcs
        @oegs = @so.ordered_entity_group ? [@so.ordered_entity_group] : []
        @ots = @so.order_type ? [@so.order_type] : []
        @states = @so.statefuls
        format.html { render :action => 'edit_so' }
      end
    end
  end

    def destroy_so
     @so = ServiceOrder.find(params[:id])
     @so.destroy

     respond_to do |format|
       format.html { redirect_to(:back) }
     end
   end

# Order Annotation

def new_oa
  @oa = OrderAnnotation.new
  @so = ServiceOrder.find(params[:so_id])
end

def save_oa
  @oa = OrderAnnotation.new(form_to_params(params, "oa", ["so_id", "note", "date", "severity"]))
  @so = ServiceOrder.find(params[:so_id])
  @oa.service_order = @so
  respond_to do |format|
   if @oa.save
     flash[:notice] = "Order Annotation: #{@oa.note[0..20]} created OK" 
     format.html { redirect_to(:action => :edit_oa, :id => @oa.id) }
   else
     format.html { render :action => 'new_oa' }
   end
  end  
end

def edit_oa
  @oa = OrderAnnotation.find(params[:id])
  @so = @oa.service_order
end

def update_oa
  @oa = OrderAnnotation.find(params[:id])
  @so = @oa.service_order
  respond_to do |format|
    if @oa.update_attributes(form_to_params(params, "oa", ["note", "date", "severity"]))
      flash[:notice] = "Order Annotation updated OK"
      format.html { redirect_to(:action => :edit_oa, :id => @oa.id )  }
    else
      format.html { render :action => 'edit_oa' }
    end
  end
end  

def destroy_oa
  @oa = OrderAnnotation.find(params[:id])
  @oa.destroy

  respond_to do |format|
    format.html { redirect_to(:back)}
    format.xml  { head :ok }
  end
end

#================ Bulk Order ====================

def new_bo
  @so = BulkOrder.new

  @so.ordered_entity_type = params[:ordered_entity_type]
  @so.cenx_id = "To Be Determined"
  @sp = ServiceProvider.new
  @on = OperatorNetwork.new
  @oon = OperatorNetwork.new
  @sps = ServiceProvider.find_rows_for_index
  @oeg = nil
  @ons = []
  @contacts = []
  @oes = [] #ordered_entities
  @paths = []
  @oegs = []
  @ots = []
  
  @oets = @so.get_candidate_ordered_object_types
end

def index_bulk_order
  @sos = BulkOrder.all
  @sps = ServiceProvider.find_rows_for_index
  @on = OperatorNetwork.new #hack - TODO Fix _select_sp to use collection_select
  respond_to do |format|
    format.html # index_onovc_order.html.erb
  end
end

def options_order_bo
  @so = BulkOrder.new
  @so.ordered_entity_type = params[:ordered_entity_type]
  @sp = ServiceProvider.where("id = ?", params[:spid]).first
  @sp = ServiceProvider.new if(!@sp)
  @on = OperatorNetwork.new
  @oon = OperatorNetwork.new
  @so.operator_network = @on
  @so.ordered_operator_network = @oon
  @ons = @sp.operator_networks
  @contacts = @sp.contacts
  @paths = @sp.paths
  @oes = []
  render :partial => 'options_so'
end

def select_ordered_entity_bo
  @so = BulkOrder.new
  @so.ordered_entity_type = params[:ordered_entity_type]
  @so.action = params[:so_action]
  @so.bulk_order_type = params[:so_bulk_order_type]
  @oes = []
  if params[:so_action] != "New" 
    @oon = OperatorNetwork.where("id = ?", params[:on_id]).first
    @oon = OperatorNetwork.new if (!@oon)
    @oes = @oon.paths
  end
  @oes.reject!{|oe| oe.get_latest_order.nil?}
  
  render :partial => 'select_ordered_entity_bo'
end

def save_bo
  @so = BulkOrder.new
  attrs = form_to_params(params, "so", ["id", "cenx_id", "order_type_id", /(ordered_)?service_provider_id/, "affected_entity_id", "ordered_entity_type", MemberAttrInstance.form_id_pattern])
  attrs.each { |k,v| @so.send("#{k}=",v) }

  #@so = BulkOrder.new(form_to_params(params, "so", ["id", "cenx_id", "order_type_id", /(ordered_)?service_provider_id/, "affected_entity_id", "ordered_entity_type", MemberAttrInstance.form_id_pattern]))
  if @so.bulk_order_type == "Circuit Order"
    @so.ordered_entity_type = "Path"
    @so.ordered_entity_subtype = ""
    params[:ordered_entity_type] == "Path"
  end
  
  if params[:affected_entity_id] && params[:affected_entity_id].to_i != 0
    if (@so.ordered_entity_type == "Path")
      @so.ordered_entity = Path.find(params[:affected_entity_id])
    end
  end
  
  @so.set_member_attr_instances params

  respond_to do |format|
    if @so.save
      if params[:affected_entity_id]
        flash[:notice] = "#{@so.ordered_entity_type} Change Order created; Cenx ID of the order is #{@so.cenx_id} - please modify #{@so.ordered_entity_type} data"
        if(@so.ordered_entity_type == "Path")
          format.html { redirect_to(:action => :edit_path, :id => params[:affected_entity_id] ) }
        else
          format.html {redirect_to(:action => :edit_bo, :id => @so.id)}
        end
      else
        flash[:notice] = "New #{@so.ordered_entity_type} Order created; Cenx ID of the order is #{@so.cenx_id} - please enter #{@so.ordered_entity_type} data"
        if(@so.ordered_entity_type == "Path")
          format.html { redirect_to(:action => :new_path, :id => @so.is_a?(BulkOrder) ? @so.operator_network_id : @so.ordered_operator_network_id, :so_id => @so.id ) }
        else
          format.html {redirect_to(:action => :edit_bo, :id => @so.id)}
        end
      end
    else
      @on = @so.operator_network ? @so.operator_network : OperatorNetwork.new
      @oon = @so.ordered_operator_network ? @so.ordered_operator_network : OperatorNetwork.new
      @sp = @so.service_provider ? @so.service_provider : ServiceProvider.new
      @sps = ServiceProvider.find_rows_for_index
      @ons = @sp.operator_networks
      @oons = @so.ordered_service_provider ? @so.ordered_service_provider.operator_networks : nil
      @contacts = @sp.contacts
      @paths = @sp.paths
      @oegs = @oon.new_record? ? [] : @oon.operator_network_type.ordered_entity_groups
      @ots = []
      
      @oes = []
      if params[:so_action] != "New"
        if(@so.ordered_entity_type == "Path")
          @oes = @oon.paths
        end
      end
      @oes.reject!{|oe| oe.get_latest_order.nil?}     
      @oets = @so.get_candidate_ordered_object_types

      format.html { render :action => "new_bo" }
    end
  end
end


def edit_bo  
  @so = ServiceOrder.find(params[:id])
  @on = @so.operator_network
  @oon = @so.ordered_operator_network
  @sps = ServiceProvider.find_rows_for_index
  @sp = @so.operator_network.service_provider
  @ons = @sp.operator_networks
  @oons = @so.ordered_service_provider ? @so.ordered_service_provider.operator_networks : nil
  @contacts = @sp.contacts
  @paths = @sp.paths
  @oes = @on.demarcs
  @oegs = @oon ? @oon.operator_network_type.ordered_entity_groups : []
  @oets = @so.get_candidate_ordered_object_types
  @states = @so.statefuls
  @ufs = @so.uploaded_files
  respond_to do |format|
    format.html { render :action => "edit_bo" }
  end
end

def bo_add_order
  so = BulkOrder.find(params[:id])
  co_id = params[:post][:co_id]
  if !co_id.blank?  
    order_to_add = ServiceOrder.find(co_id)
    flash[:notice] = "Order #{order_to_add.title} successfully added"
     begin
       so.service_orders << order_to_add
     rescue ActiveRecord::RecordInvalid
       flash[:notice] = "#{$!.to_s}"     
     end
  end
  respond_to do |format|    
    format.html {redirect_to(:action => :edit_bo, :id => so.id)}
  end
end

def bo_remove_order
  so = BulkOrder.find(params[:so_id])
  co_id = params[:id]
  if !co_id.blank?
    order_to_remove = ServiceOrder.find(co_id)
    flash[:notice] = "Order #{order_to_remove.title} successfully removed"
    begin
      so.service_orders.delete(order_to_remove)
    rescue ActiveRecord::RecordInvalid
      flash[:notice] = "#{$!.to_s}"     
    end
  end
  respond_to do |format|
    format.html {redirect_to(:action => :edit_bo, :id => so.id)}
  end
end


def update_bo
  @so = BulkOrder.find(params[:id])
  @on = @so.operator_network
  @oon = @so.ordered_operator_network
  @states = @so.statefuls
  @ufs = @so.uploaded_files
  if params[:so][:bulk_order_type] == "Circuit Order"
    @so.ordered_entity_type = "Path"
    @so.ordered_entity_subtype = ""
  end
  
  @so.set_member_attr_instances params
  $count = 0
  
  respond_to do |format|
    if(@so.update_attributes(form_to_params(params, "so", ["id", "cenx_id", "order_type_id", /(ordered_)?service_provider_id/, "ordered_entity_type", MemberAttrInstance.form_id_pattern])))
      if @so.ordered_entity_id
        flash[:notice] = "Order: #{@so.title} updated OK"
        format.html { redirect_to(:action => :edit_bo, :id => @so.id) }
      else        
        if(@so.ordered_entity_type == "Path")
          flash[:notice] = "Please Enter New #{@so.ordered_entity_type} data for Order #{@so.cenx_id}"
          format.html { redirect_to(:action => :new_path, :id => @on.id, :so_id => @so.id ) }
        else
          flash[:notice] = "Order: #{@so.title} updated OK"
          format.html { redirect_to(:action => :edit_bo, :id => @so.id) }
        end
      end
    else      
      @sps = ServiceProvider.find_rows_for_index
      @sp = @so.operator_network.service_provider
      @ons = @sp.operator_networks
      @contacts = @sp.contacts
      @paths = @sp.paths
      @oes = @on.demarcs
      @ots = []
      @states = @so.statefuls
      format.html { render :action => 'edit_bo' }
    end
  end
end

  def destroy_bo
   @so = ServiceOrder.find(params[:id])
   @so.destroy

   respond_to do |format|
     format.html { redirect_to(:back) }
   end
 end
 
# ============== End Bulk Order =====================

  def new_demarc
    @on = nil
    @so = (params[:so_id]) ? ServiceOrder.find(params[:so_id]) : nil    
    if(@so && @so.ordered_entity_subtype == "UNI" || params[:demarc_type] == "UNI")
      @demarc = Uni.new
      @demarc.cenx_demarc_type = "UNI"
    elsif(@so && @so.ordered_entity_subtype  == "ENNI" || params[:demarc_type] == "ENNI")
      @demarc = EnniNew.new
      @demarc.cenx_demarc_type = "ENNI"
      if @so && !@so.ordered_service_provider.is_system_owner 
        @on = @so.ordered_operator_network
        @demarc.operator_network = @on
      end
    else
      @demarc = Demarc.new
    end
    @demarc.service_orders.push(@so) if @so
    @demarc.cenx_demarc_type = @so ? @so.ordered_entity_subtype : params[:demarc_type] || "Demarc"
    @demarc.cenx_id = "To Be Determined"

    unless @on
      @on = OperatorNetwork.find(params[:id])
      @demarc.operator_network = @on
    end

    if @so && @so.ordered_object_type      
      @demarc.demarc_type = @so.ordered_object_type
      @dts = [@demarc.demarc_type]
    else      
      @dts = @demarc.get_candidate_demarc_types
    end
    if(@demarc.demarc_type)
      @pms = @demarc.demarc_type.get_candidate_physical_mediums
      @nts = @demarc.demarc_type.get_candidate_connected_device_types
      if(@demarc.cenx_demarc_type == "UNI")
        @rms = @demarc.demarc_type.get_candidate_reflection_mechanisms
      elsif(@demarc.cenx_demarc_type == "ENNI")
       @ets = @demarc.demarc_type.get_candidate_ether_types
      end
    else
      @pms = (@demarc.cenx_demarc_type == "UNI") ? HwTypes::UNI_PHY_TYPES : HwTypes::PHY_TYPES
      @nts = HwTypes::DEMARC_DEVICE_TYPES
      @rms = FrameTypes::MONITORING_FRAME_REFLECTION
      @ets = FrameTypes::ETHER_TYPES
    end
    @contacts = @demarc.get_candidate_contacts

    @sites = Site.find_rows_for_index

    @sps = ServiceProvider.find_rows_for_index

    respond_to do |format|
      format.html # new_demarc.html.erb
    end
  end  

  def save_demarc
    @on = nil   
    @so = (params[:so_id]) ? ServiceOrder.find(params[:so_id]) : nil

    if(params[:demarc][:cenx_demarc_type] == "UNI")
      @demarc = Uni.new(form_to_params(params, "demarc", ["id", "cenx_name", "so_id", "so_prim_contact", "so_testing_contact", MemberAttrInstance.form_id_pattern]))
    elsif(params[:demarc][:cenx_demarc_type] == "ENNI")
      @demarc = EnniNew.new(form_to_params(params, "demarc", ["id", "cenx_name", "so_id", "so_prim_contact", "so_testing_contact", "emergency_contact_phone_no", MemberAttrInstance.form_id_pattern]))
      if @so
        unless @so.ordered_service_provider.is_system_owner
          @on = @so.ordered_operator_network
          @demarc.operator_network = @on
        end
      end
    else
      @demarc = Demarc.new(params[:demarc])
    end

    unless @on
      @on = OperatorNetwork.find(params[:id])
      @demarc.operator_network = @on
    end
    
    @demarc.demarc_type_id = params[:demarc][:demarc_type_id]
    @demarc.set_member_attr_instances params 
    @sps = ServiceProvider.find_rows_for_index  
    @demarc.service_orders.push(@so) if @so
    
    respond_to do |format|
      if @demarc.save
        if @so
          @so.ordered_entity_id = @demarc.id
          @so.ordered_entity_snapshot = @demarc.attr_snapshot
          @so.save
        end
        if(@demarc.type == "EnniNew")
           flash[:notice] = "#{@demarc.cenx_name} created OK - you can now link ports to this ENNI"
        else
          flash[:notice] = "Demarc: #{@demarc.member_handle} created OK"
        end
        format.html { redirect_to(:action => :edit_demarc, :id => @demarc.id) }
      else
        if @so && @so.ordered_object_type
          @demarc.demarc_type = @so.ordered_object_type
          @dts = [@demarc.demarc_type]
        else
          @dts = @demarc.get_candidate_demarc_types
        end
        
        @contacts = @demarc.get_candidate_contacts
        @sites = Site.find_rows_for_index
        
        if(@demarc.demarc_type)
          @pms = @demarc.demarc_type.get_candidate_physical_mediums
          @nts = @demarc.demarc_type.get_candidate_connected_device_types
          if(@demarc.cenx_demarc_type == "UNI")
            @rms = @demarc.demarc_type.get_candidate_reflection_mechanisms
          elsif(@demarc.cenx_demarc_type == "ENNI")
            @ets = @demarc.demarc_type.get_candidate_ether_types
          end
        else
          @pms = (@demarc.cenx_demarc_type == "UNI") ? HwTypes::UNI_PHY_TYPES : HwTypes::PHY_TYPES
          @nts = HwTypes::DEMARC_DEVICE_TYPES
          @rms = FrameTypes::MONITORING_FRAME_REFLECTION
          @ets = FrameTypes::ETHER_TYPES
        end
        format.html { render :action => 'new_demarc', :id => @on.id }
      end
    end
  end

  def options_demarc_type_demarc
    @demarc = Demarc.where("id = ?",params[:demarc_id]).first
    unless(@demarc)
      @demarc = case params[:demarc_type]
        when "UNI" then Uni.new
        when "ENNI" then EnniNew.new
        else Demarc.new
      end
    end
    @demarc.service_orders.push(ServiceOrder.find(params[:so_id])) if (params[:so_id])
    @so = @demarc.get_latest_order
    @demarc.operator_network_id = params[:on_id]
    @demarc.demarc_type_id = params[:dt_id]
    @dts = @demarc.get_candidate_demarc_types

    #@site_groups = @demarc.operator_network.site_group ? [@demarc.operator_network.site_group] : SiteGroup.find_rows_for_index
    @sites = Site.find_rows_for_index
    @contacts = @demarc.get_candidate_contacts
    @demarc.cenx_demarc_type = params[:demarc_type]
    if(@demarc.demarc_type)
      @pms = @demarc.demarc_type.get_candidate_physical_mediums
      @nts = @demarc.demarc_type.get_candidate_connected_device_types
      if(@demarc.cenx_demarc_type == "UNI")
        @rms = @demarc.demarc_type.get_candidate_reflection_mechanisms
      elsif(@demarc.cenx_demarc_type == "ENNI")
        @ets = @demarc.demarc_type.get_candidate_ether_types
      end      
    else
      @pms = (@demarc.cenx_demarc_type == "UNI") ? HwTypes::UNI_PHY_TYPES : HwTypes::PHY_TYPES
      @nts = HwTypes::DEMARC_DEVICE_TYPES
      @rms = FrameTypes::MONITORING_FRAME_REFLECTION
      @ets = FrameTypes::ETHER_TYPES
    end
    
    render :partial => 'options_subclass_demarc'
  end
  
  def edit_demarc
    @demarc = Demarc.find(params[:id])
    @sites = Site.find_rows_for_index
    @dts = @demarc.get_candidate_demarc_types
    @segmenteps = @demarc.segment_end_points
    @so = @demarc.get_latest_order
    @sos = @demarc.service_orders
    @ufs = @demarc.uploaded_files
    @dts = @demarc.get_candidate_demarc_types
    @contacts = @demarc.get_candidate_contacts
    @states = @demarc.statefuls
    
    if(@demarc.demarc_type)
      @pms = @demarc.demarc_type.get_candidate_physical_mediums
      @nts = @demarc.demarc_type.get_candidate_connected_device_types      
      if(@demarc.cenx_demarc_type == "UNI")
        @rms = @demarc.demarc_type.get_candidate_reflection_mechanisms
        @ehs = @demarc.mtc_periods + @demarc.alarm_histories + @demarc.sm_histories
      elsif(@demarc.cenx_demarc_type == "ENNI")
        @ets = @demarc.demarc_type.get_candidate_ether_types
        @ehs = @demarc.mtc_periods + @demarc.alarm_histories + @demarc.sm_histories
      end
    else
      @pms = (@demarc.cenx_demarc_type == "UNI") ? HwTypes::UNI_PHY_TYPES : HwTypes::PHY_TYPES
      @nts = HwTypes::DEMARC_DEVICE_TYPES
      @rms = FrameTypes::MONITORING_FRAME_REFLECTION
      @ets = FrameTypes::ETHER_TYPES
      @ehs = @demarc.mtc_periods +  @demarc.alarm_histories + @demarc.sm_histories
    end
  end
  
  def update_demarc
    @demarc = Demarc.find(params[:id])    

    @demarc.demarc_type_id = params[:demarc_type_id]
    @demarc.set_member_attr_instances params
        
    respond_to do |format|
      if @demarc.update_attributes(form_to_params(params, "demarc", ["id", "cenx_name", "so_id", "so_prim_contact", "so_testing_contact", "emergency_contact_phone_no", MemberAttrInstance.form_id_pattern]))
        flash[:notice] = "Demarc: #{@demarc.member_handle} updated OK"
        format.html { redirect_to(:action => :edit_demarc, :id => @demarc.id) }
      else
        @sites = Site.find_rows_for_index
        @dts = @demarc.get_candidate_demarc_types
        @segmenteps = @demarc.segment_end_points
        @so = @demarc.get_latest_order
        @sos = @demarc.service_orders
        @ufs = @demarc.uploaded_files
        @dts = @demarc.get_candidate_demarc_types
        @contacts = @demarc.get_candidate_contacts
        @states = @demarc.statefuls
        if(@demarc.demarc_type)
          @pms = @demarc.demarc_type.get_candidate_physical_mediums
          @nts = @demarc.demarc_type.get_candidate_connected_device_types
          if(@demarc.cenx_demarc_type == "UNI")
            @rms = @demarc.demarc_type.get_candidate_reflection_mechanisms
            @ehs = @demarc.mtc_periods + @demarc.alarm_histories + @demarc.sm_histories
          elsif(@demarc.cenx_demarc_type == "ENNI")
            @ets = @demarc.demarc_type.get_candidate_ether_types
            @ehs = @demarc.mtc_periods + @demarc.alarm_histories + @demarc.sm_histories
          end
        else
          @pms = (@demarc.cenx_demarc_type == "UNI") ? HwTypes::UNI_PHY_TYPES : HwTypes::PHY_TYPES
          @nts = HwTypes::DEMARC_DEVICE_TYPES
          @rms = FrameTypes::MONITORING_FRAME_REFLECTION
          @ets = FrameTypes::ETHER_TYPES
          @ehs = @demarc.mtc_periods +  @demarc.alarm_histories + @demarc.sm_histories
        end
        format.html { render :action => 'edit_demarc' }
      end
    end
  end
  
  def get_demarc_mac_info
    @demarc = Demarc.find(params[:id])
    if @demarc.is_a?(EnniNew)
      result_ok, lag_mac_primary, lag_mac_secondary = @demarc.get_enni_mac_addresses
      @demarc.lag_mac_primary = lag_mac_primary
      @demarc.lag_mac_secondary = lag_mac_secondary

      if result_ok
        flash[:notice] = "Retrieved: lag_mac_primary = #{lag_mac_primary}, lag_mac_secondary = #{lag_mac_secondary} - OK"
      else
        flash[:error] = "Retrieved: lag_mac_primary = #{lag_mac_primary}, lag_mac_secondary = #{lag_mac_secondary} - FAILED"
      end
    end
    
    render :partial => 'options_lag_id_demarc'
  end
  
  
  def options_protection_type_demarc
    @demarc = EnniNew.new
    @demarc.protection_type = params[:prot_type]
    render :partial => 'options_lag_id_demarc'
  end

  def suggest_lag_id_demarc
    @demarc = EnniNew.new
    if params[:site_id] && params[:site_id].to_i != 0
      @site = Site.find(params[:site_id])
      @demarc.lag_id = IdTools::LagIdGenerator.generate_enni_lag_id @site
    else
      @demarc.errors.add(:lag_id, "- Please select site_group first")
    end
    @demarc.protection_type = params[:protection_type]
    render :partial => 'options_lag_id_demarc'
  end
  
  def choose_segmentep_demarc
    @demarc = Demarc.find(params[:id])
    @segments = @demarc.get_valid_segment_list
    @segmenteps = []
    respond_to do |format|
      format.html # choose_segmentep_demarc.html.erb
    end
  end
    
  def link_segmentep_demarc
    @demarc = Demarc.find(params[:id])  
    
    if (!params[:segmentep])||(params[:segmentep][:id] == "")
      @segments = @demarc.get_valid_segment_list
      @segmenteps = []
      render_action_id_with_errors(
        @demarc, 'choose_segmentep_demarc', @demarc.id, 
        "Segment End Point info is invalid - try again" )
      return
    end
    
    @segmentep = SegmentEndPoint.find(params[:segmentep][:id])
    
    if @segmentep.demarc
      @segments = @demarc.get_valid_segment_list
      @segmenteps = []
      render_action_id_with_errors(
         @demarc, 'choose_segmentep_demarc', @demarc.id,
         "Segment End Point: #{@segmentep.cenx_name} already attached to #{@segmentep.demarc.cenx_name}  - try again"
       )
      return
    end
              
    @segmentep.demarc_id = @demarc.id
    #We can not run validations due to new inner/outer tag validations that will clash on save
    # OnNetOvcEndPointEnni#valid_tag_presence?
    # TODO: Fix or Remove else clause
    if @segmentep.save false
      respond_to do |format|
        flash[:notice] = "Segment End Point: #{@segmentep.cenx_name} added OK to #{@demarc.cenx_name}"
        format.html { redirect_to(:action => 'edit_demarc', :id => @demarc.id ) }
      end
    else
      @segments = @demarc.get_valid_segment_list
      @segmenteps = []
      render_action_id_with_errors(
        @demarc, 'choose_segmentep_demarc', @demarc.id,
        "Segment End Point: #{@segmentep.cenx_name} save failed - please ensure data was entered correctly"
       )
    end
  end
  
  def unlink_segmentep_demarc 
    @segmentep = SegmentEndPoint.find(params[:id])
    @demarc = Demarc.find(@segmentep.demarc_id)
    @segmentep.demarc_id = nil
    @segmentep.stags = nil
    @segmentep.ctags = nil
    
    respond_to do |format|
      if @segmentep.save
        flash[:notice] = "Segment End Point: #{@segmentep.cenx_name} removed OK from #{@demarc.cenx_name}"
        format.html { redirect_to(:action => 'edit_demarc', :id => @demarc.id ) }
      else
        flash[:error] = "Segment End Point: #{@segmentep.cenx_name} remove failed for #{@demarc.cenx_name}"
        @segmenteps = @demarc.segment_end_points
        format.html { render :action => 'edit_demarc' }
      end
    end
  end

  def choose_port_demarc
    @demarc = Demarc.find(params[:id])
    @sites = @demarc.get_valid_site_list
    @nodes = []
    @ports = []
    respond_to do |format|
      format.html # choose_port_demarc.html.erb
    end
  end

  def link_port_demarc
    @demarc = Demarc.find(params[:id])

    if (!params[:port])||(params[:port][:id] == "")
      @sites = @demarc.get_valid_site_list
      @nodes = []
      @ports = []
      render_action_id_with_errors(
         @demarc, 'choose_port_demarc', @demarc.id,
         "Port info is invalid - try again" )
      return
    end

    @port = Port.find(params[:port][:id])

    if @port.is_port_connected_to_demarc
      @sites = @demarc.get_valid_site_list
      @nodes = []
      @ports = []
      render_action_id_with_errors(
         @demarc, 'choose_port_demarc', @demarc.id,
         "Port: #{@port.name} already connected to #{@port.demarc_connection_info_port}  - try again"
       )
      return
    end

    @port.demarc = @demarc
    @demarc.ports.build(@port.attributes)
    if @demarc.valid_ports? && @port.save
      respond_to do |format|
        flash[:notice] = "Port: #{@port.name} added OK to #{@demarc.cenx_name}"
        format.html { redirect_to(:action => 'edit_demarc', :id => @demarc.id ) }
      end
    else
      @sites = @demarc.get_valid_site_list
      @nodes = []
      @ports = []
      @demarc.ports(true) #remove temporrary port
      if @demarc.errors.empty?
        render_action_id_with_errors(
         @demarc, 'choose_port_demarc', @demarc.id,
          "Port: #{@port.name} save failed - please ensure port data was entered correctly"
        )
      else
        respond_to do |format|
          format.html { render :action => 'choose_port_demarc', :id => @demarc.id }
        end
      end
    end
  end

  def unlink_port_demarc
    @port = Port.find(params[:id])
    @demarc = Demarc.find(@port.demarc_id)
    @port.demarc_id = nil
    respond_to do |format|
      if @port.save
         flash[:notice] = "Port: #{@port.name} removed OK from #{@demarc.cenx_name}"
         format.html { redirect_to(:action => 'edit_demarc', :id => @demarc.id ) }
      else
        flash[:error] = "Port: #{@port.name} remove failed for #{@demarc.cenx_name}"
        format.html { render :action => 'edit_demarc' }
      end
    end
  end
  
  def destroy_demarc
    @demarc = Demarc.find(params[:id])
    @onid = @demarc.operator_network_id

    if @demarc.segment_end_points.any?
      respond_to do |format|
        flash[:error] = "Can not delete demarcs with attached Segment End Points."
        format.html { redirect_to(:back) }
      end
    else

      #Unlink ports
      @demarc.ports.each do |port|
        port.demarc = nil
      end

      #Remove all related service orders
      ServiceOrder.all.select { |so| so.ordered_entity == @demarc}.each {|so| so.destroy }

      @demarc.destroy

      respond_to do |format|
        format.html { redirect_to(:back) }
      end
    end
  end

  def config_gen_demarc
    @demarc = Demarc.find(params[:id])
    @config_text = @demarc.generate_config
    @lines = @config_text.count '\n'
    respond_to do |format|
      format.html # config_gen_demarc.html.erb
    end
  end

  def _build_enni_utilization_results ennis
    results = Array.new
    titles = Array.new
    ennis.each do |enni|
      result = OpenStruct.new
      result.id = enni.id
      result.cenx_name = enni.cenx_name
      result.site_group = enni.site_group.name
      result.rate = enni.rate
      result.cir_limit = enni.cir_limit
      result.eir_limit = enni.eir_limit

      pbw = enni.get_provisioned_bandwidth
      result.cir = pbw[:grand][:cir_rate]
      result.eir = pbw[:grand][:eir_rate]

      puts pbw.inspect

      #Warning: The following is a nasty hack to add the COSTs without hard coding
      pbw.each do |name, data|
        next if name.is_a? Symbol
        sname = name.gsub(/\s+/, "")
        titles << name
        result.send("#{sname}=".to_sym, data[:rate])
      end
      results << result
    end
    titles.uniq!
    return results, titles
  end

  def index_member_enni_utilization
    @results, @titles = _build_enni_utilization_results Demarc.find_member_ennis
    respond_to do |format|
      format.html { render "index_enni_utilization" }
    end
  end

  def index_cenx_enni_utilization
    @results, @titles = _build_enni_utilization_results Demarc.find_cenx_ennis
    respond_to do |format|
      format.html { render "index_enni_utilization" }
    end
  end

  def show_enni_cac
    @enni = Demarc.find(params[:id])
    @provisioned_bandwidth = @enni.get_provisioned_bandwidth
    @onovces = @enni.get_on_net_ovc_end_point_ennis
    @costs = @enni.get_cenx_class_of_service_types
  end

  def cac_capacity_check

    input = params.reject { |k,v| ["commit", "action", "authenticity_token", "controller", "_","utf8"].include? k}
    @enni = Demarc.find(input.delete("enni_id"))
    @costs = @enni.get_cenx_class_of_service_types

    #Convert the input into the proper form { "Cos Name" => {:dir => <value>}}
    #Input is of form {"Name:Direction" => "Value"}
    additional = {}
    input.each do |field, value|
      name, dir = field.split(":")
      dir = dir.to_sym

      additional[name] = {} if additional[name].nil?
      additional[name][dir] = value.to_f
    end

    @results = @enni.get_provisioned_bandwidth additional

    errors = @enni.check_provisioned_bandwidth @results

    respond_to do |format|
      format.html { render( :partial => 'options_cac_provisioned_bandwidth',
          :object => @results,
          :locals => {:legend => "Resulting Provisioned Bandwidth", :errors => errors}) }
    end
  end

  def get_alarm_keys
    table = Kernel.const_get(params[:table])
    @object = table.find(params[:id])
    @result = []
    success = false
    
    results = @object.get_alarm_keys
    success = results[0]
    @result = results[1..-1].flatten

    if ! success
      @result << "ERROR!"
    end
    # Get the name not all obect eg CosEndPoint have a cenx_name
    if @object.respond_to?("cenx_name")
      name = @object.cenx_name
    elsif @object.respond_to?("name")
      name = @object.name
    else
      name = "#{@object.class}:#{@object.id}"
    end
    
    respond_to do |format|
      format.html { render( :partial => 'results_alarm_check',
          :object => @result,
          :locals => {:legend => "Alarm Keys for \'#{name}\' should be:"}) }
    end

  end

  def force_alarm_sync
    table = Kernel.const_get(params[:table])
    @object = table.find(params[:id])
    @result = ["OK"]
    success = @object.alarm_sync
    if ! success
      @result = ["ERROR!"]
    end
    
    # Get the name not all object eg CosEndPoint have a cenx_name
    if @object.respond_to?("cenx_name")
      name = @object.cenx_name
    elsif @object.respond_to?("name")
      name = @object.name
    else
      name = "#{@object.class}:#{@object.id}"
    end
    
    respond_to do |format|
      format.html { render( :partial => 'results_alarm_check',
          :object => @result,
          :locals => {:legend => "Alarm Sync \'#{name}\' Results:"}) }
    end
  end

  def move_objects
    @sites = Site.all
  end

  def move_info
    #Split the cenx ids by anything that isn't a digit (so you can have any separator)
    cenx_ids = params[:cenx_ids].split(/\D+/).map { |id| id.to_i }
    if params[:site_id] == "Select"
      @info = {:errors => "Must select Destination Site first.", :deletions => ""}
    else
      dest_site = Site.find(params[:site_id]) unless params[:site_id] == "Select"
      puts dest_site.inspect
      vpls_id = params[:vpls_id]
      @info = MoveHelper.get_move_info(cenx_ids, dest_site)
    end
    @form = {
      :cenx_ids => params[:cenx_ids],
      :vpls_id => params[:vpls_id],
      :site_id => params[:site_id]
    }

    render :partial => 'options_move_info', :locals => {:invalid => @info[:errors].any?}
  end

  def move_confirm
    #Split the cenx ids by anything that isn't a digit (so you can have any separator)
    cenx_ids = params[:cenx_ids].split(/\D+/).map { |id| id.to_i }
    dest_site = Site.find(params[:site_id]) unless params[:site_id] == "Select"
    vpls_id = params[:vpls_id]
    errors = MoveHelper.move cenx_ids, vpls_id, dest_site
    
    if errors.any?
      respond_to do |format|
        flash[:error] = errors
        format.html {redirect_to(:action => :move_objects)}
      end
    else
      respond_to do |format|
        flash[:notice] = "Objects moved to Site #{dest_site.name}"
        format.html {redirect_to(:action => :edit_site, :id => dest_site.id)}
      end
    end
    
    
  end

  def copy_ports
    @sites = Site.all
  end

  def copy_ports_results
    source_site = Site.find(params[:source_site_id])
    dest_site = Site.find(params[:dest_site_id])
    @results, errors = MoveHelper.copy_ports source_site, dest_site
    render :partial => 'copy_ports_results', :locals => {:errors => errors}
  end

  #
  # Demarc Type - Will replace Enni Profile and Uni Profile
  #
  
  def new_dt
    @dt = DemarcType.new
    @ont = OperatorNetworkType.find(params[:id])

    respond_to do |format|
      format.html # new_dt.html.erb
    end
  end

  def save_dt
    p = form_to_params(params, "dt", ["id", "sp_name"])
    @dt = case params[:dt][:demarc_type_type]
      when "ENNI Type" then EnniType.new(p)
      when "UNI Type" then UniType.new(p)
      else 
        dt = DemarcType.new(p.merge({:demarc_type_type => "Demarc Type"}))
    end
    @ont = OperatorNetworkType.find(params[:id])
    @dt.operator_network_type = @ont

    respond_to do |format|
      if @dt.save
        flash[:notice] = "Demarc Type: #{@dt.name} created OK"
        format.html { redirect_to(:action => :edit_ont, :id => @ont.id) }
      else
        format.html { render :action => 'new_dt' }
      end
    end
  end

  def edit_dt
    @dt = DemarcType.find(params[:id])
    @ont = @dt.operator_network_type
  end

  def update_dt
    @dt = DemarcType.find(params[:id])
    @ont = @dt.operator_network_type

    respond_to do |format|
      if @dt.update_attributes(form_to_params(params, "dt", ["id", "sp_name"]))
        flash[:notice] = "#{@dt.demarc_type_type}: #{@dt.name} updated OK"
        format.html { redirect_to(:action => :edit_dt, :id => @dt.id )  }
      else
        format.html { render :action => 'edit_dt' }
      end
    end
  end
  
  def options_demarc_type_type
    @dt = DemarcType.new
    @dt.demarc_type_type = params[:demarc_type_type]
    render :partial => 'options_demarc_type_type'
  end

  def destroy_dt
    @dt = DemarcType.find(params[:id])
    @ontid = @dt.operator_network_type.id
    @dt.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :edit_ont, :id => @ontid) }
    end
  end
    
  #
  # Ethernet Service Type Management
  #
  
  def new_est
    @est = EthernetServiceType.new
    @ont = OperatorNetworkType.find(params[:id])
    @est.operator_network_type = @ont
    
    respond_to do |format|
      format.html # new_est.html.erb
    end
  end
  
  def save_est
    @est = EthernetServiceType.new(params[:est])
    @ont = OperatorNetworkType.find(params[:id])
    @est.operator_network_type = @ont
    
    respond_to do |format|
      if @est.save
        flash[:notice] = "Ethernet Service Type: #{@est.name} created OK" 
        format.html { redirect_to(:action => :edit_ont, :id => @ont.id) }
      else
        format.html { render :action => 'new_est' }
      end
    end
  end
  
  def edit_est
    @est = EthernetServiceType.find(params[:id])
    @costs = @est.class_of_service_types
    @dts = @est.demarc_types
    @segmentts = @est.segment_types
    @segmentepts = @est.segment_end_point_types
  end
  
  def update_est
    @est = EthernetServiceType.find(params[:id])
    
    respond_to do |format|
      if @est.update_attributes(params[:est])
        flash[:notice] = "Ethernet Service Type: #{@est.name} updated OK" 
        format.html { redirect_to(:action => :edit_est, :id => @est.id )  }
        format.xml  { head :ok }
      else
        @costs = @est.class_of_service_types
        @dts = @est.demarc_types
        @segmentts = @est.segment_types
        @segmentepts = @est.segment_end_point_types
        format.html { render :action => 'edit_est' }
        format.xml  { render :xml => @est.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def add_any_type_est(est, params, list, table)
    if (!params)||(params[:id] == "")
      return false, nil, "invalid info"
    end
    
    obj = table.find(params[:id])
    
    if (list.include?(obj))
      return false, nil, "duplicate"
    end
    
    list.push(obj)
    
    if est.save
      return true, obj, ""
    else
      return false, obj, "save failed"
    end
  end
  
  def choose_segmentt_est
    @est = EthernetServiceType.find(params[:id])
    @segmentts = @est.get_candidate_segment_types
    respond_to do |format|
        format.html # choose_segmentt_est.html.erb
    end
  end
  
  def add_segmentt_est
    @est = EthernetServiceType.find(params[:id])
    success, @segmentt, errors = add_any_type_est(@est, params[:segmentt], @est.segment_types, SegmentType)
    if success
      respond_to do |format|
        flash[:notice] = "Segment Type: #{@segmentt.name} added OK to #{@est.name}"
        format.html { redirect_to(:action => 'edit_est', :id => @est.id ) }
      end
    else
      @segmentts = @est.get_candidate_segment_types
      err = "Segment Type - #{errors}"
      if errors == "invalid info"
        err = "Segment Type info is invalid - try again"
      elsif errors == "duplicate"
        err = "Segment Type is already in #{@est.name}"
      elsif error == "save failed"
        err = "Segment Type: #{@segmentt.name} save failed - please ensure segment type data was entered correctly"
      end
      render_action_id_with_errors(
         @est, 'choose_segmentt_est', @est.id,
         err
       )
    end
  end
  
  def remove_segmentt_est
    @est = EthernetServiceType.find(params[:est_id])
    @segmentt = SegmentType.find(params[:id])
    @est.segment_types.delete(@segmentt)
    respond_to do |format|
      if @est.save
        flash[:notice] = "Segment Type: #{@segmentt.name} removed OK from #{@est.name}" 
        format.html { redirect_to(:action => 'edit_est', :id => @est.id ) }
      else
        flash[:error] = "Segment Type: #{@segmentt.name} remove failed for #{@est.name}" 
        @segmentts = @est.segment_types
        format.html { render :action => 'edit_est' }
      end
    end
  end 
  
  def choose_segmentept_est
    @est = EthernetServiceType.find(params[:id])
    @segmentepts = @est.get_candidate_segment_end_point_types
    respond_to do |format|
        format.html # choose_segmentt_est.html.erb
    end
  end
  
  def add_segmentept_est
    @est = EthernetServiceType.find(params[:id])
    success, @segmentept, errors = add_any_type_est(@est, params[:segmentept], @est.segment_end_point_types, SegmentEndPointType)
    if success
      respond_to do |format|
        flash[:notice] = "Segment Type: #{@segmentept.name} added OK to #{@est.name}"
        format.html { redirect_to(:action => 'edit_est', :id => @est.id ) }
      end
    else
      @segmentts = @est.get_candidate_segment_end_point_types
      err = "Segment End Point Type - #{errors}"
      if errors == "invalid info"
        err = "Segment End Point Type info is invalid - try again"
      elsif errors == "duplicate"
        err = "Segment End Point Type is already in #{@est.name}"
      elsif error == "save failed"
        err = "Segment End Point Type: #{@segmentept.name} save failed - please ensure Segment End Point type data was entered correctly"
      end
      render_action_id_with_errors(
         @est, 'choose_segmentt_est', @est.id,
         err
       )
    end
  end
  
  def remove_segmentept_est
    @est = EthernetServiceType.find(params[:est_id])
    @segmentept = SegmentEndPointType.find(params[:id])
    @est.segment_end_point_types.delete(@segmentept)
    respond_to do |format|
      if @est.save
        flash[:notice] = "Segment End Point Type: #{@segmentept.name} removed OK from #{@est.name}" 
        format.html { redirect_to(:action => 'edit_est', :id => @est.id ) }
      else
        flash[:error] = "Segment End Point Type: #{@segmentept.name} remove failed for #{@est.name}" 
        @segmentts = @est.segment_end_point_types
        format.html { render :action => 'edit_est' }
      end
    end
  end 
  
  def choose_dt_est
    @est = EthernetServiceType.find(params[:id])
    @dts = @est.get_candidate_demarc_types
    respond_to do |format|
        format.html # choose_dt_est.html.erb
    end
  end
  
  def add_dt_est
    @est = EthernetServiceType.find(params[:id])
    success, @dt, errors = add_any_type_est(@est, params[:dt], @est.demarc_types, DemarcType)
    if success
      respond_to do |format|
        flash[:notice] = "Demarc Type: #{@dt.name} added OK to #{@est.name}"
        format.html { redirect_to(:action => 'edit_est', :id => @est.id ) }
      end
    else
      @dts = @est.get_candidate_demarc_types
      err = "Demarc Type - #{errors}"
      if errors == "invalid info"
        err = "Demarc Type info is invalid - try again"
      elsif errors == "duplicate"
        err = "Demarc Type is already in #{@est.name}"
      elsif error == "save failed"
        err = "Demarc Type: #{@dt.name} save failed - please ensure demarc type data was entered correctly"
      end
      render_action_id_with_errors(
         @est, 'choose_dt_est', @est.id,
         err
       )
    end
  end
  
  def remove_dt_est
    @est = EthernetServiceType.find(params[:est_id])
    @dt = DemarcType.find(params[:id])
    @est.demarc_types.delete(@dt)
    respond_to do |format|
      if @est.save
        flash[:notice] = "Demarc Type: #{@dt.name} removed OK from #{@est.name}" 
        format.html { redirect_to(:action => 'edit_est', :id => @est.id ) }
      else
        flash[:error] = "Demarc Type: #{@dt.name} remove failed for #{@est.name}" 
        @dts = @est.demarc_types
        format.html { render :action => 'edit_est' }
      end
    end
  end 
  
  def choose_cost_est
    @est = EthernetServiceType.find(params[:id])
    @costs = @est.get_candidate_class_of_service_types
    respond_to do |format|
        format.html # choose_cost_est.html.erb
    end
  end
  
  def add_cost_est
    @est = EthernetServiceType.find(params[:id])
    success, @cost, errors = add_any_type_est(@est, params[:cost], @est.class_of_service_types, ClassOfServiceType)
    if success
      respond_to do |format|
        flash[:notice] = "Class of Service Type: #{@cost.name} added OK to #{@est.name}"
        format.html { redirect_to(:action => 'edit_est', :id => @est.id ) }
      end
    else
      @costs = @est.get_candidate_class_of_service_types
      err = "Class of Service Type - #{errors}"
      if errors == "invalid info"
        err = "Class of Service Type info is invalid - try again"
      elsif errors == "duplicate"
        err = "Class of Service Type is already in #{@est.name}"
      elsif error == "save failed"
        err = "Class of Service Type: #{@cost.name} save failed - please ensure demarc type data was entered correctly"
      end
      render_action_id_with_errors(
         @est, 'choose_cost_est', @est.id,
         err
       )
    end
  end
  
  def remove_cost_est
    @est = EthernetServiceType.find(params[:est_id])
    @cost = ClassOfServiceType.find(params[:id])
    @est.class_of_service_types.delete(@cost)
    respond_to do |format|
      if @est.save
        flash[:notice] = "Class of Service Type: #{@cost.name} removed OK from #{@est.name}" 
        format.html { redirect_to(:action => 'edit_est', :id => @est.id ) }
      else
        flash[:error] = "Class of Service Type: #{@cost.name} remove failed for #{@est.name}" 
        @costs = @est.class_of_service_types
        format.html { render :action => 'edit_est' }
      end
    end
  end 
  
  def destroy_est
    @est = EthernetServiceType.find(params[:id])
    @ontid = @est.operator_network_type_id
    @est.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :edit_ont, :id => @ontid) }
      format.xml  { head :ok }
    end
  end
    
  #
  # Order Type Management
  #
  
  def new_ot
    @ot = OrderType.new
    @ont = OperatorNetworkType.find(params[:id])
    @ot.operator_network_type = @ont
    
    respond_to do |format|
      format.html # new_ot.html.erb
    end
  end
  
  def save_ot
    @ot = OrderType.new(params[:ot])
    @ont = OperatorNetworkType.find(params[:id])
    @ot.operator_network_type = @ont
    
    respond_to do |format|
      if @ot.save
        flash[:notice] = "Order Type: #{@ot.name} created OK" 
        format.html { redirect_to(:action => :edit_ont, :id => @ont.id) }
      else
        format.html { render :action => 'new_ot' }
      end
    end
  end
  
  def edit_ot
    @ot = OrderType.find(params[:id])
    @oegs = @ot.ordered_entity_groups
  end
  
  def update_ot
    @ot = OrderType.find(params[:id])
    
    respond_to do |format|
      if @ot.update_attributes(params[:ot])
        flash[:notice] = "Order Type: #{@ot.name} updated OK" 
        format.html { redirect_to(:action => :edit_ot, :id => @ot.id )  }
        format.xml  { head :ok }
      else
        @oegs = @ot.ordered_entity_groups
        format.html { render :action => 'edit_ot' }
        format.xml  { render :xml => @ot.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy_ot
    @ot = OrderType.find(params[:id])
    @ontid = @ot.operator_network_type_id
    @ot.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :edit_ont, :id => @ontid) }
      format.xml  { head :ok }
    end
  end
    
  #
  # Ordered Entity Group Management
  #
  
  def new_oeg
    @oeg = OrderedEntityGroup.new
    @ot = OrderType.find(params[:id])
    @oeg.order_type = @ot
    @ests = @oeg.get_candidate_ethernet_service_types
    
    respond_to do |format|
      format.html # new_oeg.html.erb
    end
  end
  
  def save_oeg
    @oeg = OrderedEntityGroup.new(params[:oeg])
    @ot = OrderType.find(params[:id])
    @oeg.order_type = @ot
    
    respond_to do |format|
      if @oeg.save
        flash[:notice] = "Ordered Entity Group: #{@oeg.name} created OK" 
        format.html { redirect_to(:action => :edit_ot, :id => @ot.id) }
      else
        @ests = @oeg.get_candidate_ethernet_service_types
        format.html { render :action => 'new_oeg' }
      end
    end
  end
  
  def edit_oeg
    @oeg = OrderedEntityGroup.find(params[:id])
    @ests = @oeg.get_candidate_ethernet_service_types
    @oetls = @oeg.ordered_entity_type_links
  end
  
  def update_oeg
    @oeg = OrderedEntityGroup.find(params[:id])
    
    respond_to do |format|
      if @oeg.update_attributes(params[:oeg])
        flash[:notice] = "Ordered Entity Group: #{@oeg.name} updated OK" 
        format.html { redirect_to(:action => :edit_oeg, :id => @oeg.id )  }
        format.xml  { head :ok }
      else
        @ests = @oeg.get_candidate_ethernet_service_types
        @oetls = @oeg.ordered_entity_type_links
        format.html { render :action => 'edit_oeg' }
        format.xml  { render :xml => @oeg.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def choose_type_oeg
    @oeg = OrderedEntityGroup.find(params[:id])
    @oets = @oeg.get_candidate_ordered_entity_types
    respond_to do |format|
      format.html # choose_type_oeg.html.erb
    end
  end
  
  def link_type_oeg
    @oeg = OrderedEntityGroup.find(params[:id])
    success = true
    if params[:oet].nil? || params[:oet][:id_class].nil? || params[:oet][:id_class].split(";").length != 2
      success = false
      err = "invalid info"
    end
    
    if success
      oet_id, oet_class = params[:oet][:id_class].split(";")
      @oet = parent_from_id_type(oet_id, parent_type_from_table(oet_class))
      if @oet.nil?
        success = false
        err = "invalid info"
      elsif @oeg.ordered_entity_types.include?(@oet)
        success = false
        err = "duplicate"
      end
    end
    
    ordered_entity_type_link = nil
    if success
    	ordered_entity_type_link = OrderedEntityTypeLink.create(:ordered_entity_type => @oet, :ordered_entity_group => @oeg)
    end
    if success && @oeg.save
      respond_to do |format|
        flash[:notice] = "Ordered Entity Type: #{@oet.name} added OK to #{@oeg.name}"
        format.html { redirect_to(:action => 'edit_oeg', :id => @oeg.id ) }
      end
    else
      ordered_entity_type_link.destroy if ordered_entity_type_link
      @oets = @oeg.get_candidate_ordered_entity_types
      error = "Ordered Entity Type - #{err}"
      if err == "invalid info"
        error = "Ordered Entity Type info is invalid - try again"
      elsif err == "duplicate"
        error = "Ordered Entity Type is already in #{@oeg.name}"
      else
        error = "Ordered Entity Type: #{@oet.name} save failed - please ensure data was entered correctly"
      end
      render_action_id_with_errors(
        @oeg, 'choose_type_oeg', @oeg.id,
         err
       )
    end
  end
  
  def unlink_type_oeg
    @oeg = OrderedEntityGroup.find(params[:oeg_id])
    @oetl = OrderedEntityTypeLink.find(params[:id])
    @oetl_copy = @oetl.clone
    @oetl_copy.id = params[:id]
    @oetl.destroy
    respond_to do |format|
      if @oeg.save
        flash[:notice] = "Ordered Entity Type: #{@oetl.oet_name} removed OK from #{@oeg.name}" 
        format.html { redirect_to(:action => 'edit_oeg', :id => @oeg.id ) }
      else
        @oetl_copy.save
        flash[:error] = "Ordered Entity Type: #{@oetl.oet_name} remove failed for #{@oeg.name}" 
        @oetls = @oeg.ordered_entity_type_links
        format.html { render :action => 'edit_oeg' }
      end
    end
  end 
  
  def destroy_oeg
    @oeg = OrderedEntityGroup.find(params[:id])
    @otid = @oeg.order_type_id
    @oeg.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :edit_ot, :id => @otid) }
      format.xml  { head :ok }
    end
  end
  
  def edit_oetl
    @oetl = OrderedEntityTypeLink.find(params[:id])
    respond_to do |format|
      format.html { redirect_to(:action => "edit_#{parent_type_from_table(@oetl.ordered_entity_type_type)}", :id => @oetl.ordered_entity_type_id) }
    end
  end
    
  #
  # Segment Type Management
  #
  
  def new_segmentt
    @segmentt = SegmentType.new
    @ont = OperatorNetworkType.find(params[:id])
    @segmentt.operator_network_type = @ont
    
    respond_to do |format|
      format.html # new_segmentt.html.erb
    end
  end
  
  def save_segmentt
    @segmentt = SegmentType.new(params[:segmentt])
    @ont = OperatorNetworkType.find(params[:id])
    @segmentt.operator_network_type = @ont
    
    respond_to do |format|
      if @segmentt.save
        flash[:notice] = "Segment Type: #{@segmentt.name} created OK" 
        format.html { redirect_to(:action => :edit_ont, :id => @ont.id) }
      else
        format.html { render :action => 'new_segmentt' }
      end
    end
  end
  
  def edit_segmentt
    @segmentt = SegmentType.find(params[:id])
  end
  
  def update_segmentt
    @segmentt = SegmentType.find(params[:id])
    
    respond_to do |format|
      if @segmentt.update_attributes(params[:segmentt])
        flash[:notice] = "Segment Type: #{@segmentt.name} updated OK"
        format.html { redirect_to(:action => :edit_segmentt, :id => @segmentt.id )  }
        format.xml  { head :ok }
      else
        format.html { render :action => 'edit_segmentt' }
        format.xml  { render :xml => @segmentt.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy_segmentt
    @segmentt = SegmentType.find(params[:id])
    @ontid = @segmentt.operator_network_type_id
    @segmentt.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :edit_ont, :id => @ontid) }
      format.xml  { head :ok }
    end
  end
    
  #
  # Segment End Point Type Management
  #
  
  def new_segmentept
    @segmentept = SegmentEndPointType.new
    @ont = OperatorNetworkType.find(params[:id])
    @segmentept.operator_network_type = @ont
    
    respond_to do |format|
      format.html # new_segmentept.html.erb
    end
  end
  
  def save_segmentept
    @segmentept = SegmentEndPointType.new(form_to_params(params, "segmentept", ["id","default_cos_type_info"]))
    @ont = OperatorNetworkType.find(params[:id])
    @segmentept.operator_network_type = @ont
    
    respond_to do |format|
      if @segmentept.save
        flash[:notice] = "Segment End Point Type: #{@segmentept.name} created OK" 
        format.html { redirect_to(:action => :edit_ont, :id => @ont.id) }
      else
        format.html { render :action => 'new_segmentept' }
      end
    end
  end
  
  def edit_segmentept
    @segmentept = SegmentEndPointType.find(params[:id])
  end
  
  def update_segmentept
    @segmentept = SegmentEndPointType.find(params[:id])
    
    respond_to do |format|
      if @segmentept.update_attributes(form_to_params(params, "segmentept", ["id","default_cos_type_info"]))
        flash[:notice] = "Segment End Point Type: #{@segmentept.name} updated OK"
        format.html { redirect_to(:action => :edit_segmentept, :id => @segmentept.id )  }
        format.xml  { head :ok }
      else
        format.html { render :action => 'edit_segmentept' }
        format.xml  { render :xml => @segmentept.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy_segmentept
    @segmentept = SegmentEndPointType.find(params[:id])
    @ontid = @segmentept.operator_network_type_id
    @segmentept.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :edit_ont, :id => @ontid) }
      format.xml  { head :ok }
    end
  end

#
# CoS Type Management
#

def new_cost
  @ont = OperatorNetworkType.find(params[:id])
  if @ont.service_provider.is_system_owner
    @cost = CenxClassOfServiceType.new
  else
    @cost = ClassOfServiceType.new
  end
  @cost.operator_network_type = @ont
  
  respond_to do |format|
    format.html # new_cost.html.erb
    format.xml  { render :xml => @cost }
  end
end

def save_cost
  @ont = OperatorNetworkType.find(params[:id])
  if @ont.service_provider.is_system_owner
    @cost = CenxClassOfServiceType.new(params[:cost])
  else
    @cost = ClassOfServiceType.new(params[:cost])
  end
  @cost.operator_network_type = @ont
  
  respond_to do |format|
    if @cost.save
      flash[:notice] = "Class of Service Type: #{@cost.name} created OK" 
      format.html { redirect_to(:action => :edit_ont, :id => @ont.id) }
      format.xml  { render :xml => @cost, :status => :created, :location => @cost }
    else
      format.html { render :action => 'new_cost' }
      format.xml  { render :xml => @cost.errors, :status => :unprocessable_entity }
    end
  end
end

def edit_cost

  @cost = ClassOfServiceType.find(params[:id])
  @slgts = @cost.service_level_guarantee_types
  @bwpts = @cost.bw_profile_types
  #@cosis = @cost.cos_instances
end

def update_cost
  #if params[:segment_class]
  @cost = ClassOfServiceType.find(params[:id])
  @slgts = @cost.service_level_guarantee_types
  @bwpts = @cost.bw_profile_types
  #@cosis = @cost.cos_instances
  
  respond_to do |format|
   if @cost.update_attributes(params[:cost])
    #if @ovcet.update_attributes(form_to_params(params, "ovcet", ["id","default_cos_type_info"]))
    #if @cost.update_attributes(form_to_params(params, "cost", ["id","fwding_class"]))
      flash[:notice] = "Class of Service Type: #{@cost.name} updated OK"
      format.html { redirect_to(:action => :edit_cost, :id => @cost.id )  }
      format.xml  { head :ok }
    else
      @slgts = @cost.service_level_guarantee_types
      @bwpts = @cost.bw_profile_types
      format.html { render :action => 'edit_cost' }
      format.xml  { render :xml => @cost.errors, :status => :unprocessable_entity }
    end
  end
end

def destroy_cost
  @cost = ClassOfServiceType.find(params[:id])
  @ontid = @cost.operator_network_type_id
  @cost.destroy

  respond_to do |format|
    format.html { redirect_to(:action => :edit_ont, :id => @ontid) }
    format.xml  { head :ok }
  end
end

#
# Service Level Guarantee Type Management (will replace SLG)
#

def new_slgt
  @slgt = ServiceLevelGuaranteeType.new
  @cost = ClassOfServiceType.find(params[:id])
  @slgt.class_of_service_type = @cost

  respond_to do |format|
    format.html # new_slgt.html.erb
    format.xml  { render :xml => @slgt }
  end
end

def save_slgt
  @slgt = ServiceLevelGuaranteeType.new(params[:slgt])
  @cost = ClassOfServiceType.find(params[:id])
  @slgt.class_of_service_type = @cost
  
  respond_to do |format|
    if @slgt.save
      flash[:notice] = "Service Level Guarantee Type: #{@slgt.name} created OK" 
      format.html { redirect_to(:action => :edit_cost, :id => @cost.id) }
      format.xml  { render :xml => @slgt, :status => :created, :location => @slgt }
    else
      format.html { render :action => 'new_slgt' }
      format.xml  { render :xml => @slgt.errors, :status => :unprocessable_entity }
    end
  end
end

def edit_slgt
  @slgt = ServiceLevelGuaranteeType.find(params[:id])
end

def update_slgt
  @slgt = ServiceLevelGuaranteeType.find(params[:id])
  
  respond_to do |format|
    if @slgt.update_attributes(params[:slgt])
      flash[:notice] = "Service Level Guarantee Type: #{@slgt.name} updated OK"
      format.html { redirect_to(:action => :edit_slgt, :id => @slgt.id )  }
    else
      format.html { render :action => 'edit_slgt' }
    end
  end
end

def destroy_slgt
  @slgt = ServiceLevelGuaranteeType.find(params[:id])
  @costid = @slgt.class_of_service_type_id
  @slgt.destroy

  respond_to do |format|
    format.html { redirect_to(:action => :edit_cost, :id => @costid) }
    format.xml  { head :ok }
  end
end

#
# Bandwidth Profile Type Management (will replace BWP)
#

def new_bwpt
  @bwpt = BwProfileType.new
  @cost = ClassOfServiceType.find(params[:id])
  @bwpt.class_of_service_type = @cost

  respond_to do |format|
    format.html # new_bwpt.html.erb
  end
end

def save_bwpt
  @bwpt = BwProfileType.new(params[:bwpt])
  @cost = ClassOfServiceType.find(params[:id])
  @bwpt.class_of_service_type = @cost
  
  respond_to do |format|
    if @bwpt.save
      flash[:notice] = "Bandwidth Profile Type: #{@bwpt.bwp_type} created OK" 
      format.html { redirect_to(:action => :edit_cost, :id => @cost.id) }
    else
      format.html { render :action => 'new_bwpt' }
    end
  end
end

def edit_bwpt
  @bwpt = BwProfileType.find(params[:id])
end

def update_bwpt
  @bwpt = BwProfileType.find(params[:id])
  
  respond_to do |format|
    if @bwpt.update_attributes(params[:bwpt])
      flash[:notice] = "Bandwidth Profile Type: #{@bwpt.bwp_type} updated OK"
      format.html { redirect_to(:action => :edit_bwpt, :id => @bwpt.id )  }
      format.xml  { head :ok }
    else
      format.html { render :action => 'edit_bwpt' }
      format.xml  { render :xml => @bwpt.errors, :status => :unprocessable_entity }
    end
  end
end

def destroy_bwpt
  @bwpt = BwProfileType.find(params[:id])
  @costid = @bwpt.class_of_service_type_id
  @bwpt.destroy

  respond_to do |format|
    format.html { redirect_to(:action => :edit_cost, :id => @costid) }
    format.xml  { head :ok }
  end
end

#
# Event History Management
#
def edit_eh
  @eh = EventHistory.find(params[:id])
  # Service Monitoring history is not ordered by time_of_event but by the order events were added
  if @eh.is_a?(SmHistory)
    @ers = @eh.event_records.all
  else
    @ers = @eh.event_records.order("time_of_event")
  end
end

def update_eh
  @eh = EventHistory.find(params[:id])
  respond_to do |format|
    if @eh.update_attributes(params[:eh])
      flash[:notice] = "Event History updated OK"
      format.html { redirect_to(:action => :edit_eh, :id => @eh.id )  }
    else
      format.html { render :action => 'edit_eh' }
    end
  end
end

def destroy_eh
  @eh = EventHistory.find(params[:id])
  @ers = @eh.event_records.order("time_of_event")
  @eh.destroy
  
  respond_to do |format|
    format.html { redirect_to(:back)}
    format.xml  { head :ok }
  end
  
end

def new_er
  @eh = EventHistory.find(params[:id])
  @er = (@eh.kind_of? AlarmHistory) ? AlarmRecord.new : EventRecord.new
  @er.event_history = @eh

  respond_to do |format|
    format.html # new_er.html.erb
  end
end

def save_er
  @eh = EventHistory.find(params[:id])
  @er = (@eh.kind_of? AlarmHistory) ? AlarmRecord.new(params[:er]) : EventRecord.new(params[:er])
  @er.event_history = @eh

  respond_to do |format|
    if @er.save
      # reevalulate the SM state if it's an alarm (SAM or Brix) or Mtc 
      if ([AlarmHistory, BrixAlarmHistory, MtcHistory].include?(@eh.class))
          @eh.event_ownerships.each {|eo| eo.eventable.eval_SM_state}
      end
      flash[:notice] = "Event Record created OK"
      format.html { redirect_to(:action => :edit_eh, :id => @eh.id) }
    else
      format.html { render :action => 'new_er' }
    end
  end
end

def edit_er
  @er = EventRecord.find(params[:id])
  @eh = @er.event_history
end

def update_er
  @er = EventRecord.find(params[:id])
  @eh = @er.event_history
  respond_to do |format|
    if @er.update_attributes(params[:er])
      # reevalulate the SM state if it's an alarm (SAM or Brix) or Mtc 
      if ([AlarmHistory, BrixAlarmHistory, MtcHistory].include?(@eh.class))
          @eh.event_ownerships.each {|eo| eo.eventable.eval_SM_state}
      end
      flash[:notice] = "Event Record updated OK"
      format.html { redirect_to(:action => :edit_er, :id => @er.id )  }
    else
      format.html { render :action => 'edit_er' }
    end
  end
end

def destroy_er
  @er = EventRecord.find(params[:id])
  @ehid = @er.event_history_id
  @er.destroy

  # reevalulate the SM state if it's an alarm (SAM or Brix) or Mtc 
  if ([AlarmHistory, BrixAlarmHistory, MtcHistory].include?(@eh.class))
      @eh.event_ownerships.each {|eo| eo.eventable.eval_SM_state}
  end
  
  respond_to do |format|
    format.html { redirect_to(:action => :edit_eh, :id => @ehid) }
    format.xml  { head :ok }
  end
end

def handle_maintenance_ehs
  @eh = MtcHistory.find_by_id(params[:id])
  @eh.add_event((Time.now.to_f*1000).to_i, params[:mtc_action], params[:mtc_action] == "maintenance" ? "In Maintenance" : "Out of Maintenance")
  respond_to do |format|
    format.html { redirect_to(:back)}
    format.xml  { head :ok }
  end
end
  
#
# State History Management
#
def edit_sh
  @sh = Stateful.find(params[:id])
  @states = @sh.states.all
end

def update_sh
  @sh = Stateful.find(params[:id])

  pre_result, post_result, pre_objs, post_objs = @sh.set_state(params[:sh][:states].constantize)
  error_string = ""
  if !pre_result
    error_string << "Failed to change state - pre conditions due to "
    pre_objs.each {|obj| error_string << "#{obj[:obj].class.to_s}:#{obj[:obj].cenx_id} #{obj[:reason]}\n"}
  elsif !post_result
    error_string << "Failed to change state - post conditions failed due to objects "
    post_objs.each {|obj| error_string << "#{obj[:obj].class.to_s}:#{obj[:obj].cenx_id} #{obj[:reason]}\n"}
  end
  @states = @sh.states.all
  
  respond_to do |format|
    if error_string.blank?
      flash[:notice] = "State updated OK"
      format.html { redirect_to(:action => :edit_sh, :id => @sh.id )  }
    else
      flash[:notice] = error_string
      format.html { render :action => 'edit_sh' }
    end
  end
end

def destroy_sh
  @sh = Stateful.find(params[:id])
  @states = @sh.states.all
  @sh.destroy
  
  respond_to do |format|
    format.html { redirect_to(:back)}
    format.xml  { head :ok }
  end
  
end

def new_state
  @sh = Stateful.find(params[:id])
  @state = State.new
  @state.stateful = @sh

  respond_to do |format|
    format.html
  end
end

def save_state
  @sh = Stateful.find(params[:id])
  @state = @sh.to_specific(params[:state][:type].constantize).new(:name => params[:state][:name], :notes => params[:state][:notes], :timestamp => params[:state][:timestamp])
  @state.stateful = @sh

  respond_to do |format|
    if @state.save
      flash[:notice] = "State created OK"
      format.html { redirect_to(:action => :edit_sh, :id => @sh.id) }
    else
      format.html { render :action => 'new_state' }
    end
  end
end

def edit_state
  @state = State.find(params[:id])
  @sh = @state.stateful
end

def update_state
  @state = State.find(params[:id])
  @sh = @state.stateful
  @state.type = params[:state][:type]
  params[:state].delete(:type)
  respond_to do |format|
  if @state.update_attributes(params[:state])
      flash[:notice] = "State Record updated OK"
      format.html { redirect_to(:action => :edit_state, :id => @state.id )  }
    else
      format.html { render :action => 'edit_state' }
    end
  end
end

def destroy_state
  @state = State.find(params[:id])
  @shid = @state.stateful
  @state.destroy

  respond_to do |format|
    format.html { redirect_to(:action => :edit_sh, :id => @shid) }
    format.xml  { head :ok }
  end
end

def sh_new_state
  @sh = Stateful.find(params[:id])
  @display_states = []
  @change_states = []
  @display_states = params[:display_states].collect {|ds| Stateful.find(ds)} if params[:display_states]
  @change_states = params[:change_states].collect {|ds| Stateful.find(ds)} if params[:change_states]
  
  pre_result, post_result, pre_objs, post_objs = @sh.set_state(params[:new_state].constantize)
  error_string = ""
  if !pre_result
    error_string << "Failed to change state.Pre-conditions failed due to objects "
    pre_objs.each do |obj|
      obj_identifier = ""
      obj_identifier = obj[:obj].name if obj[:obj].respond_to? :name
      obj_identifier = obj[:obj].cenx_id if obj[:obj].respond_to? :cenx_id

      error_string << "#{obj[:obj].class.to_s}:#{obj_identifier} #{obj[:reason]}\n"
    end
    #pre_objs.each {|obj| error_string << "#{obj[:obj].class.to_s}:#{obj[:obj].id} #{obj[:reason]}\n"}
  elsif !post_result
    error_string << "Failed to change state.Post-conditions failed due to objects "
    post_objs.each do |obj|
      obj_identifier = ""
      obj_identifier = obj[:obj].name if obj[:obj].respond_to? :name
      obj_identifier = obj[:obj].cenx_id if obj[:obj].respond_to? :cenx_id
      
      error_string << "#{obj[:obj].class.to_s}:#{obj[:obj].cenx_id} #{obj[:reason]}\n"
    end
  end
  
  respond_to do |format|
    if error_string.blank?
      flash[:notice] = "State updated OK"
    else
      flash[:error] = error_string
      flash.now[:state_error] = error_string
    end
    format.html {render(:partial => "update_state_info", :locals => {
                :display_states => @display_states,
                :change_states => @change_states} )}
  end
end

def sh_check_live_sanity
  @sh = Stateful.find(params[:id])
  warning_text = ""
  target_obj = @sh.stateful_ownership.stateable
  if target_obj != nil
    alarm = target_obj.alarm_state  
    if alarm != nil
      if alarm.state != AlarmSeverity::OK
        warning_text = "Warning: There are active alarm(s) on #{target_obj.class}:#{target_obj.cenx_id}: #{alarm.state.to_s}: #{alarm.details}"
      end
    end
  end
  if (request.xhr?)
    render :text => warning_text, :layout => false
  else
    SW_ERR "Not a Ajax request"
  end
end

#
# KPI Management
#

def edit_kpi
  @kpi = Kpi.find(params[:id])
end

def update_kpi
  @kpi = Kpi.find(params[:id])
  respond_to do |format|
    if @kpi.update_attributes(params[:kpi])
      flash[:notice] = "KPI updated OK"
      format.html { redirect_to(:action => :edit_kpi, :id => @kpi.id )  }
    else
      format.html { render :action => 'edit_kpi' }
    end
  end
end

def destroy_kpi
  @kpi = Kpi.find(params[:id])
  @kpi.destroy

  respond_to do |format|
    format.html { redirect_to(:back) }      
    format.xml  { head :ok }
  end
end

def new_kpi
  @object = params[:object]
  @obj_id = params[:id].to_i
  @obj = @object.constantize.find(@obj_id)
  @kpi = Kpi.new(:actual_kpi => false)

  respond_to do |format|
    format.html 
  end
end

def save_kpi
  @object = params[:object]
  @obj_id = params[:obj_id].to_s
  @obj = @object.constantize.find(@obj_id)
  @kpi = Kpi.new(params[:kpi])
  @kpi.actual_kpi = false
  @kpi_ownership = KpiOwnership.new(:kpiable => @obj, :kpi => @kpi)

  respond_to do |format|
    # make sure both are valid before saving as the 2 objects depend on each other
    if @kpi.valid? && @kpi_ownership.valid? && @kpi.save && @kpi_ownership.save
      flash[:notice] = "KPI Mask created OK"
      format.html { redirect_to(:action => :edit_kpi, :id => @kpi.id) }
    else      
      @kpi.delete
      @kpi_ownership.delete
      format.html { render :action => 'new_kpi' }
    end
  end
end

#
# Fallout Exception Management
#

def edit_fallout_exception
  @fe = FalloutException.find(params[:id])
end

def update_fallout_exception
  @fe = FalloutException.find(params[:id])
  respond_to do |format|
    if @fe.update_attributes(params[:fe])
      flash[:notice] = "Fallout Exception updated OK"
      format.html { redirect_to(:action => :edit_fe, :id => @fe.id )  }
    else
      format.html { render :action => 'edit_fe' }
    end
  end
end

#
# Uploaded File Management
#

def new_uf
  @parent_type = params[:parent_type]
  @parent = parent_from_id_type(params[:id], @parent_type)
  @uf = UploadedFile.new
  @uf.uploader = @parent
  respond_to do |format|
    format.html # new_uf.html.erb
  end
end

def save_uf
  @parent_type = params[:parent_type]
  @parent = parent_from_id_type(params[:id], @parent_type)
  @uf = UploadedFile.new(params[:uf])
  @uf.uploader = @parent
  if @uf.save
    flash[:notice] = "File: #{@uf.comment} uploaded OK against #{@parent.name}"
    redirect_to(:action => 'show_uf', 
                :parent_type => @parent_type,
                :parent_id => params[:id],
                :id => @uf.id)
  else
    @uri = params[:url]
    render(:action => :new_uf)
  end
end

def show_uf
  @uf = UploadedFile.find(params[:id])
  redirect_to(@uf.url) if @uf.url && !@uf.url.empty?
  @parent = @uf.uploader
  @parent_type = parent_type_from_table(@uf.uploader_type)
end

def get_uf
  @uf = UploadedFile.find(params[:id])
  disp = params[:disposition] ? params[:disposition] : "inline"
  send_data(@uf.data, :filename => @uf.name, :type => @uf.content_type, :disposition => disp)
end

def destroy_uf
  @uf = UploadedFile.find(params[:id])
  @parent = @uf.uploader
  @parent_type = parent_type_from_table(@uf.uploader_type)
  @uf.destroy

  respond_to do |format|
    format.html { redirect_to(:action => "edit_#{@parent_type}", :id => @parent.id) }
  end
end

#
# QoS Policy Management
#
def index_qosp
  @qosps = QosPolicy.all

  respond_to do |format|
    format.html # index_site_group.html.erb
    format.xml  { render :xml => @qosps }
  end
end

def edit_qosp
  @qosp = QosPolicy.find(params[:id])
  @config_text = @qosp.generate_config
end

def destroy_qosp
  @qosp = QosPolicy.find(params[:id])
  @site_group = @qosp.site_group
  @qosp.destroy

  respond_to do |format|
    format.html { redirect_to(:action => "edit_site_group", :id => @site_group.id) }
  end
end

def config_gen_qosp
  @segment = Segment.find(params[:id])
  @updates = @segment.configure_qos_policies
  respond_to do |format|
    format.html # config_gen_segment.html.erb
  end
end

#
# Useful tools to place everything Live etc
#

def make_all_live 
  # Fork a process because go_live.rb overrides classes and methods
  # which we don't want to affect the main Rails app
  config = ActiveRecord::Base.remove_connection
  pid = Process.fork do
    ActiveRecord::Base.establish_connection(config)
    load "db/go_live.rb"
    all_live
  end
  ActiveRecord::Base.establish_connection(config)
  Process.detach(pid) if pid != nil

  respond_to do |format|
    flash[:notice] = "The following process is running in the background #{pid} - Wait for this process to finish"
    format.html { redirect_to(:action => :home) }
  end
end

# Check email delivery is working on both regular rails and in JRuby
def input_email_delivery
  @result = ""
  respond_to do |format|
    format.html 
  end
end

def check_email_delivery
  email_address = params[:email_address]
  @result = EmailConfig.send_test_email(email_address)
  @result << "Check for 2 mails - one from each platform\n"
  render :text => @result, :layout => false
end


#
# Email Config Management
#

def edit_emailconfig
  @ec = EmailConfig.instance
  @mappings = email_mappings
  respond_to do |format|
    format.html
  end
end

def update_emailconfig
  @ec = EmailConfig.instance
  [:override_event_email_address,
   :override_order_email_address,
   :debug_bcc_event_email_addresses,
   :debug_bcc_order_email_addresses,
   :cenx_ops_request,
   :cenx_ops,
   :cenx_eng,
   :cenx_cust_ops,
   :cenx_prov,
   :cenx_billing].each {|attr| params[:ec][attr] = params[:ec][attr].split(" ")}
  
  respond_to do |format|
    if @ec.update_attributes(params[:ec])
      flash[:notice] = "Email Configuration updated OK"        
      format.html { redirect_to(:action => :edit_emailconfig, :id => @ec.id )  }
    else
      @mappings = email_mappings
      format.html { render :action => 'edit_emailconfig' }
      #format.html { redirect_to(:action => :edit_emailconfig, :id => @ec.id )  }
    end
  end
end

def email_mappings
  ec = EmailConfig.instance
  mappings = [OrderAccepted, OrderDesignComplete, OrderProvisioned, OrderTested, OrderCustomerAccepted].collect do |order_state|
    to, cc, bcc = ec.get_order_recipients(order_state.new)
    {:name => order_state.new.name, :to => to, :cc => cc, :bcc => bcc}
  end
  to, cc, bcc = ec.get_event_recepients(nil)
  mappings << {:name => "Alarms", :to => to, :cc => cc, :bcc => bcc}
  return mappings
end

#
#Pull down Menu Helpers
#
  def options_site
    unless request.xhr?
      flash[:notice] = "Bad parameter - try again"
      redirect_to :action => 'choose_port_demarc'
    else
      @site_group = SiteGroup.find(params[:id])
      @sites = @site_group.sites
      render :partial => 'select_site'
    end
  end

  def options_node
    unless request.xhr?
      flash[:notice] = "Bad parameter - try again"
      redirect_to :action => 'choose_port_demarc'
    else
      @site = Site.find(params[:id])
      @nodes = @site.nodes
      action = (params[:no_filter]) ? 'options_port' : 'options_unlinked_port'
      logger.info "Action is: #{action}"
      render :partial => 'select_node',
              :locals => {
                          :prompt => 'Node',
                          :action => action,
                          :update => 'port_select_field',
                          :with => '"id=" + value' }
    end
  end

  def options_service_router_node
    unless request.xhr?
      flash[:notice] = "Bad parameter - try again"
      redirect_to :action => 'choose_port_demarc'
    else
      @site = Site.find(params[:id])
      @nodes = @site.nodes.reject{|n| !n.is_service_router}
      action = (params[:no_filter]) ? 'options_port' : 'options_unlinked_port'
      logger.info "Action is: #{action}"
      render :partial => 'select_node',
              :locals => {
                          :prompt => 'Node',
                          :action => action,
                          :update => 'port_select_field',
                          :with => '"id=" + value' }
    end
  end

  def options_segmentep
    #logger.info "options_segmentep"
    unless request.xhr?
      flash[:notice] = "Bad parameter - try again"
      redirect_to :action => 'choose_segmentep_demarc'
    else
      @segment = Segment.find(params[:id])
      @demarc = Demarc.find(params[:demarc_id])
      @segmenteps = @segment.unlinked_segment_end_points
      render :partial => 'select_segmentep',
        :locals => { :segmentep_class => case "#{@demarc.type}"
          when "EnniNew" then (@segment.is_a? OnNetOvc) ? "OnNetOvcEndPointEnni" : "OvcEndPointEnni"
          when "Uni" then (@segment.is_a? OnNetOvc) ? "" : "OvcEndPointUni"
          # This is an ERROR - lets pass a clue about where the call came from
          else "AdminController::options_segmentep: #{@demarc.type}"
        end
      }
    end
  end

  def options_port
    #logger.info "options_port"
    unless request.xhr?
      flash[:notice] = "Bad parameter - try again"
      redirect_to :action => 'choose_port_demarc'
    else
      @node = Node.find(params[:id])
      @ports = @node.ports
      render :partial => 'select_port'
    end
  end

  def options_unlinked_port
    @node = Node.find(params[:id])
    @ports = @node.unlinked_ports
    render :partial => 'select_port'
  end

  def search_cenx_id
    row = nil
    @cenx_id = params[:cenx_id]
    @cenx_id.strip!
    #logger.info("Try ID: #{cenx_id}")
    @result_string = "\n"
    result_ok, rs, type_string, table = IdTools::CenxIdGenerator.decode_cenx_id @cenx_id
    @result_string += "---- Cenx_ID parsing errors ----\n"
    @result_string += rs
    #logger.info("Type str: #{type_string}")

    if result_ok
      row = table.where("cenx_id = ?", @cenx_id).first
      if row == nil
        @result_string += "* has valid format but not found;\n"
      end
    else
      result_ok, rs, type_string, table = CenxNameTools::CenxNameHelper.decode_type_string @cenx_id
      @result_string += "\n---- Cenx_Name parsing errors ----\n"
      @result_string += rs
      logger.info("Type str: #{type_string}")
      if result_ok
        table.send(:find, :all).each do |r|
          if r.cenx_name == @cenx_id
            row = r
          end
        end
        if row == nil
          @result_string += "* has valid format but not found;\n"
        end
      end
    end

    respond_to do |format|
      if result_ok && row != nil
       format.html { redirect_to(:action => "edit_#{type_string}", :id => row.id) }
      else
        format.html
      end
    end
  end

  def search_member_handle
    @mais = MemberAttrInstance.search(params[:member_handle])
    if @mais.one?
      type = CDBInfo::CLASS_SHORT_NAMES[@mais.first.affected_entity_type]
      respond_to do |format|
        format.html { redirect_to(:action => "edit_#{type}", :id => @mais.first.affected_entity_id)}
      end
    else
      @results = Hash.new
      @mais.each do |mai|
        result = OpenStruct.new
        result.id = mai.affected_entity_id
        result.value = mai.value
        result.name = mai.name
        type = CDBInfo::CLASS_SHORT_NAMES[mai.affected_entity_type]
        @results[type] = Array.new if @results[type].nil?
        @results[type] << result
      end
    end
    
  end
  
  def search_table
    table = table_from_type(params[:fcn_tail])
    attrs = params[:attrs] ? params[:attrs] : []
    details = params[:details] ? params[:details] : []
    if table
      @items = params[:items].collect{|id| table.find(id)}
    else
      @items = []
    end
    if(params[:search] && params[:search] != "")
      @matchnodes = []
      @items.each{|item|
        if(params[:local_link] && item.read_attribute(params[:local_link]).to_s.downcase.include?(params[:search].downcase))
          @matchnodes.push(item)
        elsif(params[:detail_link] && item.send(params[:detail_link]).to_s.downcase.include?(params[:search].downcase))
          @matchnodes.push(item)
        else
          push = false
          for a in attrs
            if (item.read_attribute(a) && item.read_attribute(a).to_s.downcase.include?(params[:search].downcase))
              push = true
              break
            end
          end
          if !push
            for d in details
              if (item.send(d) && item.send(d).to_s.downcase.include?(params[:search].downcase))
                push = true
                break
              end
            end
          end
          @matchnodes.push(item) if push
        end
      }
    else
      @matchnodes = @items
    end
    methods = (params[:methods] ? params[:methods] : {})
    confirm_methods = (params[:confirm_methods] ? params[:confirm_methods] : {})
    params[:legend].gsub!(/\+/, " ")
    params[:cols].each{|col| col.gsub!(/\+/, " ")}
    render( :partial => 'list_any_type', :object => @matchnodes,
      :locals => {
				:legend => params[:legend],
				:cols => params[:cols],
				:fcn_tail => params[:fcn_tail],
				:local_link => params[:local_link],
				:detail_link => params[:detail_link],
				:attrs => attrs,
				:details => details,
				:methods => methods,
				:confirm_methods => confirm_methods,
				:search => params[:search],
				:enable_search => params[:enable_search],
				:enable_delete => params[:enable_delete],
				:delete_on => params[:delete_on]
				} )
  end

  # Temporary EXFO debugging 

  def render_bx_auth
    render :file => "/Users/Dave/work/code/cenx-oss-db/lib/bx_stats_if/wsdl/Authentication.wsdl"
  end

  def render_bx_de
    render :file => "/Users/Dave/work/code/cenx-oss-db/lib/bx_stats_if/wsdl/DataExtraction.wsdl"
  end

   def render_bx_den
    render :file => "/Users/Dave/work/code/cenx-oss-db/lib/bx_stats_if/wsdl/DataExtractionNew.wsdl"
  end

  def render_bx_util
    render :file => "/Users/Dave/work/code/cenx-oss-db/lib/bx_stats_if/wsdl/Utils.wsdl"
  end

private
  def form_to_params(params, form, i=[])
    ignore = (i.kind_of? Array) ? i + [form, "commit", "action", "authenticity_token", "controller","utf8"] : [i, form, "commit", "action", "authenticity_token", "controller","utf8"]
    pass = params[form]
    params.each{|name, value|
      if !ignore.any?{|n| n === name}
        pass[name] = value
      end
    }
    return pass
  end
  
  def table_from_type(type)
    table = CDBInfo::CLASS_SHORT_NAMES.invert[type.to_s]
    return table ? table.constantize : nil
  end

  def parent_from_id_type(id, type)
    table = table_from_type type
    parent = table.where("id = ?", id).first
    return parent ? parent : table.new()
  end

  def parent_type_from_table(table)
    CDBInfo::CLASS_SHORT_NAMES[table.is_a?(ActiveRecord::Base) ? table.class_name : table.to_s]
  end

  def render_action_id_with_errors (obj, action, id, msg) 
    obj.errors.add "error:", msg
    respond_to do |format|
      format.html { render :action => "#{action}", :id => id }
    end
  end

end # ---- of class
