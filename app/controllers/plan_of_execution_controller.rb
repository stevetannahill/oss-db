class PlanOfExecutionController < ApplicationController 
  
  layout "admin"

  def index
    @all = Osl::PlanOfExecution.all(osl_app)
    @queued_tasks = queued_jobs
  end
  
  def new
  end
  
  def show
    @poe = Osl::PlanOfExecution.find(params[:id], params[:version], osl_app)
    @has_osl_workers = has_workers?
    @has_pending_jobs = has_job? && @has_osl_workers
  end

  def scheduled
    @poe = Osl::PlanOfExecution.find(params[:id], nil, osl_app)
  end
  
  def create
    
    if params[:file_upload].nil?
      did_create = false
      flash[:notice] = "Please provide a file"
    else
      @uploader = osl_app.cmd("uploader")[:data]
      was_uploaded = @uploader.upload(params[:file_upload][:poe])

      if was_uploaded
        create_method = params[:commit] == "Upload NEW VIP" ? "create" : "update"
        @poe = Osl::PlanOfExecution.new(:app => osl_app, :filename => @uploader.filename, :display => :string)
        did_create = @poe.send(create_method)
        flash[:notice] = extract_message # did_create ? "Successfully created POE (#{@poe.id})" : extract_message
      else
        did_create = false
        flash[:notice] = "Unable to upload file"  
      end
    end

    respond_to do |format|
      format.html { did_create ? redirect_to(:action => :show, :id => @poe.id, :version => @poe.current_version ) : render(:new) }
    end
  end
  
  def process_poe
    @poe = Osl::PlanOfExecution.find(params[:id], nil, osl_app)
    
    if @poe.nil?
      flash[:notice] = "Unable to locate POE (#{params[:id]})"
    else
      osl_app.cmd("pub send osl.poe.task_queue poe parse-again #{@poe.filename}")
      # Resque.enqueue(Osl::Worker,@poe.filename, { :id => osl_app }) unless @poe.nil?
      flash[:notice] = "Scheduled #{@poe.title} to be processed"
    end

    respond_to do |format|
      format.html { redirect_to(:action => :show, :id => params[:id] ) }
    end
  end
  
  

  private
  
    def extract_message
      @poe.console.infos.join(", ")
    end
    
    def queued_jobs
      osl_app.cmd("sub ls osl.poe.task_queue",true,true)[:data]  
    end
    
    def has_job?
      queued_jobs.each do |cmd|
        return true if cmd == "poe parse-again #{@poe.filename}"
      end
      false
    end
  
    def has_workers?
      osl_app.cmd("sub num osl.poe.task_queue")[:data] > 0
    end
  
end
