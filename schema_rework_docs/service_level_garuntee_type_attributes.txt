# Table name: service_level_guarantee_types
#
#  class_of_service_type_id :integer(4)
#  min_dist_km         :string(255)
#  max_dist_km         :string(255)
#  delay_ms            :string(255)
#  delay_variation_ms  :string(255)
#  is_unbounded        :boolean(1)
#  notes               :string(255)
#  created_at          :datetime
#  updated_at          :datetime