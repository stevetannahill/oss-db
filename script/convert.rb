options = ARGV.select{|a| a[0..0] == "-"}
params = Array.new(options.length)
i = -1
ARGV.each{|a|
  if a[0..0] == "-"
    i += 1
  elsif i >= 0 && params[i]
    params[i] = (params[i].is_a? Array) ? params[i] + [a] : [params[i], a]
  elsif i >= 0
    params[i] = a
  end
}
i = false
ARGV.reject!{|a| i = (i || a[0..0] == "-"); i}

db_type = (options.include? "--db") ? ((params[options.index("--db")].is_a? Array) ? params[options.index("--db")][0] : params[options.index("--db")]) : "development"

if !(["production", "development", "test"].include? db_type)
  raise "#{db_type} is not a valid database!"
end

if db_type != "development" && !(options.include? "--no-verify")
  puts "This will affect the #{db_type} database - are you sure?"
  choice = STDIN.gets.chomp

  if(!(["yes", "y"].include? choice.downcase))
    puts "Aborting...."
    return
  end
 
end
ENV["RAILS_ENV"] = db_type
require File.expand_path(File.dirname(__FILE__) + "/../config/environment") unless defined?(Rails.root)

puts ""


def valid_active_record?(class_name)
  klass = Module.const_get(class_name)
  return klass.is_a?(Class) && klass < ActiveRecord::Base
rescue NameError
  return false
end

if ARGV.empty?
  puts "Please enter an object to convert from: "
  input = STDIN.gets.strip
  ARGV.push(input)
end

unless valid_active_record? ARGV[0]
  raise "Object to convert from (#{ARGV[0]}) is not a valid ActiveRecord::Base."
end

if ARGV.length < 2
  puts "Please enter an object to convert to: "
  input = STDIN.gets.strip
  ARGV.push(input)
end

if !valid_active_record? ARGV[1]
  raise "Object to convert to (#{ARGV[1]}) is not a valid ActiveRecord::Base."
end

puts ""
puts "============================================="

from = Module.const_get(ARGV[0])
to = Module.const_get(ARGV[1])
direct_translation_names = from.column_names.select{|c| to.column_names.include? c }
direct_translations = Hash[direct_translation_names.collect{|c| [c, c]}]

puts "The following columns are set to be automatically copied from #{from.table_name} to #{to.table_name}:"
puts direct_translation_names.join(", ")

excluded = nil
until excluded && excluded.reject{|e| direct_translation_names.include? e }.length == 0
  if excluded
    puts excluded.reject{|e| direct_translation_names.include? e }.join(", ") + " are/is invalid."
  end
  puts "Please enter the names of any columns, seperated by commas, you would not like to be copied:"
  excluded = STDIN.gets.strip.split(/\s*,\s*/)
end
direct_translation_names.reject!{|n| excluded.include? n }
direct_translations.delete_if{|key, value| excluded.include? key }

puts ""

puts "The following columns will be copied from #{from.table_name} to #{to.table_name}:"
puts direct_translation_names.join(", ")

puts ""
puts "============================================="

to_columns = to.column_names.reject{|c| direct_translation_names.include? c }
from_columns = from.column_names.reject{|c| direct_translation_names.include? c }

puts "The following columns are left in #{from.class_name}:"
puts from_columns.join(", ")
puts ""
puts "The following columns are left in #{to.class_name}:"
puts to_columns.join(", ")
puts ""
translations = nil
until (translations && translations.reject{|t| tt = t.split(/\s*=>\s*/)
    (from_columns.include? tt[0]) && (to_columns.include? tt[1]) }.length == 0)
  if translations
    puts translations.reject{|t| tt = t.split(/\s*=>\s*/)
      (from_columns.include? tt[0]) && (to_columns.include? tt[1]) }.join(", ") + " are/is invalid."
  end
  puts "Please enter a list of translation between the columns using the syntax from => to, from => to:"
  translations = STDIN.gets.strip.split(/\s*,\s*/)
end
direct_translations.merge!(Hash[translations.collect{|t| [t.split(/\s*=>\s*/)[0], t.split(/\s*=>\s*/)[1]]}])

puts ""
puts "============================================="

to_columns = to_columns.reject{|c| direct_translations.value? c }
from_columns = from_columns.reject{|c| direct_translations.include? c }

puts "The following columns are available in #{from.class_name}:"
puts from.column_names.join(", ")
puts""
puts "The following columns are left in #{from.class_name}:"
puts from_columns.join(", ")
puts ""
puts "The following columns are available in #{to.class_name}:"
puts to.column_names.join(", ")
puts""
puts "The following columns are left in #{to.class_name}:"
puts to_columns.join(", ")
puts ""
translations = nil
until (translations && translations.reject{|t| to.column_names.include? t.split(/\s*=>\s*\{\s*/)[0] }.length == 0)
  if translations
    puts translations.reject{|t| to.column_names.include? t.split(/\s*=>\s*\{\s*/)[0] }.join(", ") + " are/is invalid."
  end
  puts "Please enter a list of code to set attributes formated like attr => {code}, attr => {code}:"
  puts "Use to and from as the objects and obj[\"attr\"] to access an attribute named :attr."
  translations = STDIN.gets.strip.chomp("}").strip.split(/\s*\}\s*,\s*/)
end
eval_translations = Hash[translations.collect{|t| [t.split(/\s*=>\s*\{\s*/)[0], t.split(/\s*=>\s*\{\s*/)[1]]}]

puts ""
puts "============================================="

froms = from.all
tos = Array.new(froms.length){to.new}
overwrite = nil
overwrite = "always overwrite" if ["-o", "--always-overwrite", "--overwrite-all"].select{|o| options.include? o}.pop
overwrite = (overwrite ? nil : "never overwrite") if ["-n", "--never-overwrite", "--overwrite-none"].select{|o| options.include? o}.pop
ignore = nil
ignore = "always ignore" if ["-a", "--always-ignore", "--ignore-all"].select{|o| options.include? o}.pop
ignore = (ignore ? nil : "never ignore") if ["-d", "--never-ignore", "--ignore-none"].select{|o| options.include? o}.pop
ignores = []
for i in 0..options.length - 1
  if ["-i", "--ignore"].include? options[i]
    if params[i].is_a? Array
      ignores = ignores + params[i]
    else
      ignores.push(params[i])
    end
  end
end

for i in 0..tos.length-1
  fattrs = froms[i].attributes
  tattrs = tos[i].attributes
  direct_translations.each{|fattr, tattr|
    if tattr == to.get_primary_key(to)
      tos[i].id = fattrs[fattr]
    else
      tattrs[tattr] = fattrs[fattr]
    end
  }
  eval_translations.each{|attr, code|
    trans_code = code.gsub(/to\["[a-zA-Z0-9_]*"\]/){|a| "tattrs" + a[2..-1]}.gsub(/from\["[a-zA-Z0-9_]*"\]/){|a| "fattrs" + a[4..-1]}
    if attr == to.get_primary_key(to)
      tos[i].id = eval(trans_code)
    else
      tattrs[attr] = eval(trans_code)
    end
  }
  if to.base_class.first(:conditions => ["#{to.get_primary_key(to)} = ?", tos[i].id])
    puts "A entry with primary key " + tos[i].id.to_s + " already exists."
    if overwrite != "always overwrite" && overwrite != "overwrite all" && overwrite != "never overwrite" && overwrite != "overwrite none"
      puts "Enter overwrite to replace with new entry, always overwrite to do this for every entry, never overwrite to do this for no entries."
      overwrite = STDIN.gets.strip
    end
    if overwrite == "overwrite" || overwrite == "always overwrite" || overwrite == "overwrite all"
      tos[i] = to.base_class.find(tos[i].id)
      tos[i].becomes(to)
    else
      tos[i] = to.new()
    end
    puts from.class_name + ":" + froms[i].id.to_s + " will be saved as " + to.class_name + ":" + tos[i].id.to_s + "."
  end
  tos[i].attributes = tattrs
  if tos[i].valid?
    tos[i].save
    tos[i].attributes = tattrs
    tos[i].save
  else
    puts from.class_name + ":" + froms[i].id.to_s + " could not be converted to " + to.class_name + "."
    puts "The following errors were encountered when attempting to save to " + to.class_name + ":"
    tos[i].errors.each{|attr,msg| puts "#{attr} - #{msg}" }
    num_of_errors = tos[i].errors.size()
    tos[i].errors.each_error{|attr, error| num_of_errors -= 1 if (ignores.include? attr)}
    if num_of_errors > 0  && ignore != "always ignore" && ignore != "ignore all" && ignore != "never ignore" && ignore != "ignore none"
      puts "Enter ignore to save anyways, ignore all to always save with errors, ignore never to never save with errors."
      ignore = STDIN.gets.strip
    end
    if num_of_errors == 0 || ignore == "ignore" || ignore == "always ignore" || ignore == "ignore all"
      puts from.class_name + ":" + froms[i].id.to_s + " will be saved as " + to.class_name + ":" + tos[i].id.to_s + "."
      tos[i].save(false)
      tos[i].attributes = tattrs
      tos[i].save(false)
    else
      puts from.class_name + ":" + froms[i].id.to_s + " will not be saved."
    end
    puts ""
  end
end

puts ""
