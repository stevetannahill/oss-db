require 'spec_helper'
require 'reporting/helper'
require 'reporting/test_report'

describe "Schedule" do

  before(:all) do
    load_database("CDB_EXPORT_For_Test_Suite").should == true unless ENV['SKIP_DB']
    Schedule.resync_crontab
  end
  
  after(:all) do
    # after all the tests there should be no crontab entries
    current_crontab = CronEdit::Crontab.List.keys
    current_crontab.delete_if {|name| name[/^#{Schedule::CRONTAB_PREFIX}/] == nil}
    current_crontab.size.should be(0)
  end

  before(:each) do
    delete_all_reports
  end
  
  after(:each) do
    delete_all_reports
  end

  it "create scheduled report" do
    ft, sr, r = create_objects("test")
    ft.valid?.should be(true)
    sr.valid?.should be(true)
    r.valid?.should be(true)
    # make sure crontab is updated    
    crontab_name = Schedule::CRONTAB_PREFIX + "test"
    current_crontab = CronEdit::Crontab.List.keys
    current_crontab.include?(crontab_name).should be(true)
    
    sr.enabled = false
    sr.save
    # make sure crontab is updated    
    current_crontab = CronEdit::Crontab.List.keys
    current_crontab.include?(crontab_name).should be(false)
    
    # Check validation of email fails
    sr.email_to = ""
    sr.email_distribution = true
    sr.enabled = true
    sr.valid?.should be(false)
    
    sr.email_distribution = false
  end
  
  it "Delete old reports" do
    ft, sr, r = create_objects("test")
    sr.retention_time = 13.months/1.day
    sr.save
    TestReport.generate_report(sr.id)
    sr.reload
    sr.reports.size.should be(2)
    rep = sr.reports.last

    # set date to -12 months - should not be deleted
    rep.run_date = rep.run_date - 12.months
    rep.save
    Schedule.remove_old_reports
    sr.reload
    sr.reports.size.should be(2)

    # set date to -14 months - should be deleted
    rep.run_date = rep.run_date - 14.months
    rep.save
    Schedule.remove_old_reports
    sr.reload
    sr.reports.size.should be(1)
  end
  
  it "check reports are deleted when scheduled report deleted" do
    ft, sr, r = create_objects("delete")
    # delete the schedule report All reports should be deleted
    sr.destroy
    passed = true
    begin
      sr.reload
      fail("Failed to destroy Schedule")
    rescue ActiveRecord::RecordNotFound
    end

    begin
      r.reload
      fail("Failed to destroy Reports")
    rescue ActiveRecord::RecordNotFound
    end
  end
  
  it "check Report generation" do
    ft, sr, r = create_objects("gen")
    num_reports = sr.reports.size
    TestReport.generate_report(sr.id)
    sr.reload
    sr.reports.size.should be(num_reports + 1)
  end

end
  
  
  
  