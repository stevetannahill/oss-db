class TestReport < Report

  def self.generate_report(sched_id)
    sr = Schedule.find(sched_id)
    rep = Report.create(:schedule => sr)
    # Get filter options
    fo = sr.scheduled_task.filter_options
    # do a query
    # and set 
    rep.clear_exceptions = 1
    rep.minor_exceptions = 1
    rep.major_exceptions = 1
    rep.critical_exceptions = 1
    rep.save
    
    if sr.email_distribution
      # generate pdf, csv
      # send email
      # attachments = {}
      # attachments['filename.pdf'] = {:content => "hello"}
      exceptions = []
      exception_finder = 'sla_exceptions'
      CdbMailer.scheduled_report(rep, exceptions, exception_finder).deliver
    end
  end
  
  def update_report
    SW_ERR "update_report should be overriddeen by subclasses"
  end
  
end
