def create_filter(name, type, filter_options, user, exe, report_type)
  ft = type.create(:name => name, :user => user, :filter_options => filter_options, :executable_script => exe, :report_type => report_type)
end

def create_schedule(filter_template, name, enabled, trigger, formats, retention_time, exec_time, email_dist, email_to, email_cc, email_bcc, email_subject, email_body)
  sr = Schedule.create(:name => name, :scheduled_task => filter_template, :enabled => enabled, :trigger => trigger, :formats => formats,
                       :retention_time => retention_time, :execution_time => exec_time, :email_distribution => email_dist,
                       :email_to => email_to, :email_cc => email_cc, :email_bcc => email_bcc, :email_subject => email_subject, :email_body => email_body, :timezone => "Eastern Time (US & Canada)")                      
end

def create_report(schedule)  
  rep = Report.create(:schedule => schedule)
  # Get filter options
  fo = schedule.scheduled_task.filter_options
  # do a query
  # and set 
  rep.clear_exceptions = 1
  rep.minor_exceptions = 1
  rep.major_exceptions = 1
  rep.critical_exceptions = 1
  rep.save
  
  if schedule.email_distribution
    # generate pdf, csv
    # send email
    # attachments = {}
    # attachments['filename.pdf'] = {:content => "hello"}
    exceptions = []
    exception_finder = 'sla_exceptions'
    CdbMailer.scheduled_report(rep, exceptions, exception_finder).deliver
  end
  return rep
end

def delete_all_reports
  Schedule.destroy_all
  ScheduledTask.destroy_all
  
  passed  = Schedule.all.size == 0 && ScheduledTask.all.size == 0
  return passed
end

def create_objects(name)
  ft = create_filter(name, FilterTemplate, {:my_sp => "hello", :my_node => "node"}, User.first, "TestReport.generate_report(%SR_ID%)", "sla")
  sr = create_schedule(ft, name, true, "monthly", {:pdf => true, :csv => true}, 30, "00:34", true, "bruce.wessels@cenx.com", nil, nil, "email_subject", "email_body")
  r = create_report(sr)
  ft.reload;sr.reload;r.reload
  return ft,sr,r
end
