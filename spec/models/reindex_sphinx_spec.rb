# == Schema Information
#
# Table name: reindex_sphinxes
#
#  id          :integer(4)      not null, primary key
#  started_at  :datetime
#  finished_at :datetime
#  scheduled   :boolean(1)
#  created_at  :datetime
#  updated_at  :datetime
#

require 'spec_helper'
require 'event/event'

describe ReindexSphinx do

  before(:all) do
    load_database("EMPTY_DB").should == true unless ENV['SKIP_DB']    
  end

  before(:each) do
    ReindexSphinx.reset
  end

  after(:each) do
    ReindexSphinx.reset
  end

  describe "#initialize" do
    
    it "empty" do
      reindex = ReindexSphinx.new
      reindex.started_at.should == nil
      reindex.finished_at.should == nil
      reindex.scheduled.should == nil
      reindex.scheduled?.should == false
    end

  end
  
  describe "#status" do
    
    it "should default to live" do
      ReindexSphinx.status.should == :live
    end
    
    it "should be settable" do
      ReindexSphinx.status = :ignore
      ReindexSphinx.status.should == :ignore
    end
    
  end
  
  describe "#schedule" do
  
    before(:each) do
      @master = ReindexSphinx.master_record
      @master.scheduled = false
      @master.save
    end
    
    it "should set schedule to true" do
      ReindexSphinx.schedule.should == true
      @master.scheduled?.should == true
      @master.reload
      @master.scheduled?.should == true
    end
    
    it "should be not update the information if being 'ignored'" do
      ReindexSphinx.status = :ignore
      @master.scheduled = false
      @master.save.should == true
      
      ReindexSphinx.schedule.should == false
      @master.reload
      @master.scheduled?.should == false
      
      ReindexSphinx.status = :live
      ReindexSphinx.schedule.should == true
      @master.reload
      @master.scheduled?.should == true
    end

  end
  
  describe "#master_record" do
    
    before(:each) do
      ReindexSphinx.delete_all
    end
    
    it "should find the first" do
      ActiveRecord::Base.connection.insert("insert into reindex_sphinxes (id) values (1)")
      ReindexSphinx.master_record.id.should == 1
      ReindexSphinx.count.should == 1
    end
    
    it "should create the first if it doesn't exist" do
      master = ReindexSphinx.master_record
      master.id.should == 1
      master.new_record?.should == false
    end
    
    it "should re-use the same record" do
      master = ReindexSphinx.master_record
      master.scheduled = true
      ReindexSphinx.master_record.scheduled?.should == true
      master.scheduled = false
      ReindexSphinx.master_record.scheduled?.should == false
    end
    
  end
  

end
