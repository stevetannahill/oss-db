require 'spec_helper'

describe Jruby do

  before(:all) do
    load_database("EMPTY_DB").should == true unless ENV['SKIP_DB']    
  end

  describe "#split_args" do
    
    it "should handle nil" do
      Jruby.split_args(nil).should == { :jruby_args => [], :ruby_args => [] }
    end

    it "should handle empty" do
      Jruby.split_args([]).should == { :jruby_args => [], :ruby_args => [] }
    end
    
    it "should filter out -J" do
      Jruby.split_args(["-J-Djgem.path=x","blah"]).should == { :jruby_args => ["-J-Djgem.path=x"], :ruby_args => ["blah"] }
    end

    it "should handle empty inputs" do
      Jruby.split_args(["","blah"]).should == { :jruby_args => [], :ruby_args => ["blah"] }
    end

    it "should handle nil inputs" do
      Jruby.split_args([nil,"blah"]).should == { :jruby_args => [], :ruby_args => ["blah"] }
    end
    
  end

end