# == Schema Information
#
# Table name: event_histories
#
#  id             :integer(4)      not null, primary key
#  event_filter   :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  alarm_mapping  :text
#  retention_time :integer(4)
#

require 'spec_helper'

describe BrixAlarmHistory do

  before(:all) do
    load_database("EMPTY_DB").should == true unless ENV['SKIP_DB']    
  end

  before(:each) do
    @history = BrixAlarmHistory.new(:event_filter => "three")
  end

  after(:all) do
    EventHistory.delete_all
  end
  
  describe "#alarm_mapping" do
    
    it "should default to map" do
      @history.save.should == true
      @history.alarm_mapping.should == BrixAlarmHistory::default_brix_alarm_mapping
    end
    
    it "should ignore if set to something else" do
      @history.alarm_mapping = { "custom" => "Custom"  }
      @history.save.should == true
      @history.alarm_mapping.should == { "custom" => "Custom"  }
    end
    
    it "should reset if set to nil" do
      @history.alarm_mapping = { "custom" => "Custom"  }
      @history.save.should == true
      @history.alarm_mapping = nil
      @history.save.should == true
      @history.alarm_mapping.should == BrixAlarmHistory::default_brix_alarm_mapping
    end
    
  end
  
  describe "#details_formatter" do

    it "should handle flr" do
      BrixAlarmHistory.details_formatter("flr","2","3","4","5").should == "FLR: 2\nMeasured Value: 3%\nSLA Guarantees: Error: 5%; Warning: 4%"
    end

    it "should handle dv" do
      BrixAlarmHistory.details_formatter("dv","2","3","4","5").should == "Delay Variation: 2\nMeasured Value: 3us\nSLA Guarantees: Error: 5us; Warning: 4us"
    end

    it "should handle delay" do
      BrixAlarmHistory.details_formatter("delay","2","3","4","5").should == "Delay: 2\nMeasured Value: 3ms\nSLA Guarantees: Error: 5ms; Warning: 4ms"
    end

    it "should handle result" do
      BrixAlarmHistory.details_formatter("result","2","3","4","5").should == "Overall result: 2\nMeasured Value: 3\nSLA Guarantees: Error: 5; Warning: 4"
    end

    it "should handle unknown" do
      BrixAlarmHistory.details_formatter("blah","2","3","4","5").should == "Unknown Test (blah): 2\nMeasured Value: 3\nSLA Guarantees: Error: 5; Warning: 4"
    end


    
  end
  
  describe "#alarm_time_correction" do
    
    it "should be 1000 minutes" do
      BrixAlarmHistory.alarm_time_correction.should == 1*60*1000
    end
    
  end

end
