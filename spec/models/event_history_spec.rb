# == Schema Information
#
# Table name: event_histories
#
#  id             :integer(4)      not null, primary key
#  event_filter   :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  alarm_mapping  :text
#  retention_time :integer(4)
#

require 'spec_helper'
require 'event/event'

describe EventHistory do

  before(:all) do
    load_database("EMPTY_DB").should == true unless ENV['SKIP_DB']    
  end

  before(:each) do
    @history = EventHistory.new
  end

  after(:all) do
    EventHistory.delete_all
  end

  describe "#initialize" do
    
    it "should default retention to 90 days" do
      @history.save.should == true
      @history.reload
      @history.retention_time.should == 90
    end
    
    it "should not overwrite if set" do
      @history.retention_time = 80
      @history.save.should == true
      @history.reload
      @history.retention_time.should == 80
    end
    
  end
  
  describe "#alarm_mappings" do
    
    it "should be serializable" do
      h = EventHistory.create(:event_filter => "three")
      h.alarm_mapping = {"a" => "A", "b" => "B"}
      h.save!
      
      h.reload
      
      new_h = EventHistory.find(h.id)
      new_h.alarm_mapping.should == {"a" => "A", "b" => "B"}
    end
    
  end
  
  # describe "#eventable_type" do
  #   
  #   it "should do only accept constantable entries" do
  #     @history.eventable_type = "EventHistory"
  #     @history.eventable_type.should == "EventHistory"
  #   end
  #   
  # end
  
  describe "#process_event" do
    
    it "should do nothing" do
      EventHistory.process_event(nil).should == nil
    end
    
  end
  
end
