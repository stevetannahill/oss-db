require 'spec_helper'
require 'event/event'

describe "Event" do

  before(:all) do
    load_database("EMPTY_DB").should == true unless ENV['SKIP_DB']    
  end

  describe "#initialize" do
    
    it "empty" do
      Event.new(nil, nil, nil, nil, nil, nil, nil)
    end
    
  end

  describe "#blank?" do
    
    it "should be true for nil" do
      Event.blank?(nil).should be_true
    end

    it "should be true for empty string" do
      Event.blank?('').should be_true
    end

    it "should be false for everything else" do
      Event.blank?('b').should be_false
      Event.blank?(String).should be_false
    end

    
  end

end