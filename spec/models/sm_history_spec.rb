# == Schema Information
#
# Table name: event_histories
#
#  id             :integer(4)      not null, primary key
#  event_filter   :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  alarm_mapping  :text
#  retention_time :integer(4)
#

require 'spec_helper'

describe SmHistory do

  before(:all) do
    load_database("EMPTY_DB").should == true unless ENV['SKIP_DB']    
  end

  before(:each) do
    @history = SmHistory.new(:event_filter => "two")
  end

  after(:all) do
    EventHistory.delete_all
  end
  
  describe "#alarm_mapping" do
    
    it "should default to map" do
      @history.save.should == true
      @history.alarm_mapping.should == SmHistory::default_sm_alarm_mapping
    end
    
    it "should ignore if set to something else" do
      @history.alarm_mapping = { "custom" => "Custom"  }
      @history.save.should == true
      @history.alarm_mapping.should == { "custom" => "Custom"  }
    end
    
    it "should reset if set to nil" do
      @history.alarm_mapping = { "custom" => "Custom"  }
      @history.save.should == true
      @history.alarm_mapping = nil
      @history.save.should == true
      @history.alarm_mapping.should == SmHistory::default_sm_alarm_mapping
    end
    
    it "should save and reload" do
      @history.save.should == true
      same = SmHistory.find(@history.id)
      same.alarm_mapping.should == SmHistory::default_sm_alarm_mapping
    end
    
  end

end
