require 'spec_helper'

describe Path do
  
  describe "cascade_id" do
    
    before(:each) do
      @path = Path.new
    end

    it "should handle nil" do
      @path.stub(:member_handle).and_return(nil)
      @path.cascade_id.should == nil
    end

    it "should unknown formats" do
      @path.stub(:member_handle).and_return("ABC")
      @path.cascade_id.should == "ABC"
    end
    
    it "should should leave non -P/-B alone" do
      @path.stub(:member_handle).and_return("CH123-1")
      @path.cascade_id.should == "CH123-1"
    end

    ["-P","-p","-B","-b"].each do |ext|
      it "should strip off the #{ext}from the member handle" do
        @path.stub(:member_handle).and_return("CH123#{ext}")
        @path.cascade_id.should == "CH123"
      end
    end

    it "should handl ERROR!" do
      @path.stub(:member_handle).and_return("ERROR!")
      @path.cascade_id.should == nil
    end

    it "should handle ERROR" do
      @path.stub(:member_handle).and_return("ERROR")
      @path.cascade_id.should == nil
    end
    
    it "should handle yoigo formats" do
      @path.stub(:member_handle).and_return("1-B2B_2656")
      @path.cascade_id.should == "1-B2B_2656"

      @path.stub(:member_handle).and_return("1-B2B_2656-P")
      @path.cascade_id.should == "1-B2B_2656"

      @path.stub(:member_handle).and_return("1-B2B_2656-B")
      @path.cascade_id.should == "1-B2B_2656"
    end

    ["a","ab","abc-","a-1"].each do |example|
      it "should handle small casacdes (#{example})" do
        @path.stub(:member_handle).and_return(example)
        @path.cascade_id.should == example
      end
    end
    
  end
  
end# == Schema Information
#
# Table name: paths
#
#  id                  :integer(4)      not null, primary key
#  operator_network_id :integer(4)
#  created_at          :datetime
#  updated_at          :datetime
#  cenx_id             :string(255)
#  description         :string(255)
#  path_type_id        :integer(4)
#  cenx_path_type      :string(255)     default("Standard")
#  cenx_name_prefix    :string(255)
#  event_record_id     :integer(4)
#  sm_state            :string(255)
#  sm_details          :string(255)
#  sm_timestamp        :integer(8)
#  prov_name           :string(255)
#  prov_notes          :text
#  prov_timestamp      :integer(8)
#  order_name          :string(255)
#  order_notes         :text
#  order_timestamp     :integer(8)
#  type                :string(255)
#

