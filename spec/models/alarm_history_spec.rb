# == Schema Information
#
# Table name: event_histories
#
#  id             :integer(4)      not null, primary key
#  event_filter   :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  type           :string(255)
#  alarm_mapping  :text
#  retention_time :integer(4)
#

require 'spec_helper'

describe AlarmHistory do

  before(:all) do
    load_database("EMPTY_DB").should == true unless ENV['SKIP_DB']    
  end

  before(:each) do
    @history = AlarmHistory.new(:event_filter => "two")
  end

  after(:all) do
    EventHistory.delete_all
  end
  
  describe "#alarm_mapping" do
    
    it "should default to map" do
      @history.save.should == true
      @history.alarm_mapping.should == AlarmSeverity::default_alu_alarm_mapping
    end
    
    it "should ignore if set to something else" do
      @history.alarm_mapping = { "custom" => "Custom"  }
      @history.save.should == true
      @history.alarm_mapping.should == { "custom" => "Custom"  }
    end
    
    it "should be able to call EventHistory before AlarmHistory" do
      EventHistory.new(:event_filter => "one").save.should == true
      @history.save.should == true
      @history.alarm_mapping.should == AlarmSeverity::default_alu_alarm_mapping
    end

    it "should reset if set to nil" do
      @history.alarm_mapping = { "custom" => "Custom"  }
      @history.save.should == true
      @history.alarm_mapping = nil
      @history.save.should == true
      @history.alarm_mapping.should == AlarmSeverity::default_alu_alarm_mapping
    end
    
  end

end
