require 'spec_helper'
require 'metrics_and_graphs/helper'

describe "metrics Tests" do

  before(:all) do
    load_database("CDB_EXPORT_spirent_archive_test").should == true unless ENV['SKIP_DB']
    load_data_archive_database("DATA_ARCHIVE_EXPORT_spirent_archive_test").should == true unless ENV['SKIP_DB']
  end



  describe "Metrics computation test" do
    it "should compute the correct metrics for the test circuit" do
      spirent_metrics_computation_test
    end
  end

end
