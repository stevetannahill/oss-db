
=begin
Requires:
- 3 CosTestVectors
circuit_id: "AU13XC092-DNR-B-V2502-CIR00250-P6",
ref_circuit_id: "AU54XC355-DNR-P-V2521-CIR00250-P6",
last_hop_circuit_id: "AU54XC226-RN-B-V2508-CIR00055-P6">

data for each CosTestVector:
- availability data
- kpi data

data was taken from ds7 using the following SELECT

select * from ethframedelaytest_archive  where circuitid='AU13XC092-DNR-B-V2502-CIR00250-P6' INTO OUTFILE '/tmp/ds7_kpi_total.csv' FIELDS TERMINATED BY ',';
select * from ethframedelaytest_archive  where circuitid='AU54XC355-DNR-P-V2521-CIR00250-P6' INTO OUTFILE '/tmp/ds7_kpi_ref.csv' FIELDS TERMINATED BY ',';
select * from ethframedelaytest_archive  where circuitid='AU54XC226-RN-B-V2508-CIR00055-P6' INTO OUTFILE '/tmp/ds7_kpi_last_hop.csv' FIELDS TERMINATED BY ',';

select * from ethframedelaytest_availability  where circuitid='AU13XC092-DNR-B-V2502-CIR00250-P6' INTO OUTFILE '/tmp/ds7_avail_total.csv' FIELDS TERMINATED BY ',';
select * from ethframedelaytest_availability  where circuitid='AU54XC355-DNR-P-V2521-CIR00250-P6' INTO OUTFILE '/tmp/ds7_avail_ref.csv' FIELDS TERMINATED BY ',';
select * from ethframedelaytest_availability  where circuitid='AU54XC226-RN-B-V2508-CIR00055-P6' INTO OUTFILE '/tmp/ds7_avail_last_hop.csv' FIELDS TERMINATED BY ',';

then loaded into the ethframedelaytest_archive and ethframedelaytest_availability
then exported to a sql file like this:
 mysqldump -u root spirent_archive > DATA_ARCHIVE_EXPORT_spirent_archive_test.sql

=end

def my_assert str, expr
  unless expr
    fail(str)
  end #raise "Assert failed" unless expr
end

def spirent_metrics_computation_test
  
  expected_values = []
  expected_values[0]= {
    'time_period' => [ 1325376000, 1328054399 ],
    'available_time_periods' => [[1325376000, 1327488840], [1327491241, 1328054399]],
    'sampleCount' => 7184.0,
    'ref1_sampleCount' => 7184.0,
    'delta1_sampleCount' => 0.0,
    'framesSent' => 10776000.0,
    'ref1_framesSent' => 10776000.0,
    'delta1_framesSent' => 0.0,
    'framesReceived' => 10746548.0,
    'ref1_framesReceived' => 10775380.0,
    'delta1_framesReceived' => -28832.0,
    'framesLost' => 29452.0,
    'ref1_framesLost' => 620.0,
    'delta1_framesLost' => 28832.0,
    'frameLossRatio' => 0.2733,
    'ref1_frameLossRatio' => 0.0058,
    'delta1_frameLossRatio' => 0.2675,
    'averageLatency' => 7775.72320434,
    'ref1_averageLatency' => 2992.97028118,
    'delta1_averageLatency' => 4782.75292316,
    'averageDelayVariation' => 2161.65249165,
    'ref1_averageDelayVariation' => 2.52693486,
    'delta1_averageDelayVariation' => 2159.12555679,
    'frame_loss_critical' => 6631,
    'frame_loss_major' => 6631,
    'frame_loss_minor' => 6631,
    'frame_loss_ratio_critical' => 6631,
    'frame_loss_ratio_major' => 6631,
    'frame_loss_ratio_minor' => 6631,
    'delay_critical' => 0,
    'delay_major' => 4345,
    'delay_minor' => 5012,
    'delay_variation_critical' => 5077,
    'delay_variation_major' => 5131,
    'delay_variation_minor' => 5138,
  }
  expected_values[1]= {
    'time_period' => [ 1328054400, 1330559999 ],
    'available_time_periods' => [[1328054400, 1328335200], [1328336101, 1328804280], [1328805781, 1328849580], [1328850181, 1328853480], [1328854081, 1329663900], [1329694801, 1329806100], [1329806701, 1329907440], [1329910141, 1329938340], [1329938941, 1329939840], [1329940441, 1329941340], [1329941941, 1329942540], [1329943141, 1329943740], [1329965641, 1330073940], [1330074841, 1330371840], [1330373041, 1330469340], [1330469941, 1330559999]],
    'sampleCount' => 7626.0,
    'ref1_sampleCount' => 7626.0,
    'delta1_sampleCount' => 0.0,
    'framesSent' => 11439000.0,
    'ref1_framesSent' => 11439000.0,
    'delta1_framesSent' => 0.0,
    'framesReceived' => 11415116.0,
    'ref1_framesReceived' => 11437667.0,
    'delta1_framesReceived' => -22551.0,
    'framesLost' => 23884.0,
    'ref1_framesLost' => 1333.0,
    'delta1_framesLost' => 22551.0,
    'frameLossRatio' => 0.2088,
    'ref1_frameLossRatio' => 0.0117,
    'delta1_frameLossRatio' => 0.1971,
    'averageLatency' => 9556.0190139,
    'ref1_averageLatency' => 7445.4956727,
    'delta1_averageLatency' => 2110.5233412,
    'averageDelayVariation' => 587.12765539,
    'ref1_averageDelayVariation' => 2.37772095,
    'delta1_averageDelayVariation' => 584.74993444,
    'frame_loss_critical' => 5623,
    'frame_loss_major' => 5623,
    'frame_loss_minor' => 5623,
    'frame_loss_ratio_critical' => 5623,
    'frame_loss_ratio_major' => 5623,
    'frame_loss_ratio_minor' => 5623,
    'delay_critical' => 0,
    'delay_major' => 0,
    'delay_minor' => 0,
    'delay_variation_critical' => 0,
    'delay_variation_major' => 0,
    'delay_variation_minor' => 25,
  }
  expected_values[2]= {
    'time_period' => [ 1330560000, 1333238399 ],
    'available_time_periods' => [[1330560000, 1331033590], [1331034191, 1331106259], [1331107160, 1331336831], [1331337432, 1333238399]],
    'sampleCount' => 3433.0,
    'ref1_sampleCount' => 3434.0,
    'delta1_sampleCount' => -1.0,
    'framesSent' => 5149500.0,
    'ref1_framesSent' => 5151000.0,
    'delta1_framesSent' => -1500.0,
    'framesReceived' => 5145676.0,
    'ref1_framesReceived' => 5151000.0,
    'delta1_framesReceived' => -5324.0,
    'framesLost' => 3824.0,
    'ref1_framesLost' => 0.0,
    'delta1_framesLost' => 3824.0,
    'frameLossRatio' => 0.0743,
    'ref1_frameLossRatio' => 0.0,
    'delta1_frameLossRatio' => 0.0743,
    'averageLatency' => 12293.18715409,
    'ref1_averageLatency' => 10543.06552126,
    'delta1_averageLatency' => 1750.12163283,
    'averageDelayVariation' => 330.39149432,
    'ref1_averageDelayVariation' => 2.06188119,
    'delta1_averageDelayVariation' => 328.32961313,
    'frame_loss_critical' => 1833,
    'frame_loss_major' => 1833,
    'frame_loss_minor' => 1833,
    'frame_loss_ratio_critical' => 1833,
    'frame_loss_ratio_major' => 1833,
    'frame_loss_ratio_minor' => 1833,
    'delay_critical' => 0,
    'delay_major' => 0,
    'delay_minor' => 0,
    'delay_variation_critical' => 0,
    'delay_variation_major' => 0,
    'delay_variation_minor' => 0,
  }

  ctv = CosTestVector.find_by_circuit_id "AU13XC092-DNR-B-V2502-CIR00250-P6"

  i =0
  expected_values.each do | hash |
    puts "expected_values[#{i}]= {"
    time_pair = hash['time_period']
    start_time = time_pair [0]
    end_time = time_pair[1]
    stats = ctv.stats_data(start_time, end_time)
    metric_types =[:sample_count, :frames_sent, :frames_received , :frame_loss, :frame_loss_ratio, :delay, :delay_variation,  ]
    puts "'time_period' => [ #{start_time.to_i}, #{end_time.to_i} ],"
    avail_data = stats.stats_availability_results(Stats::UNAVAILABLE_THRESHOLD)[:data]
    avail_time_periods_secs = stats.get_available_time_periods(avail_data)
    puts "'available_time_periods' => #{avail_time_periods_secs},"
    my_assert "avail_time_periods_secs mistmatch: (#{avail_time_periods_secs}!= #{hash['available_time_periods']})", avail_time_periods_secs == hash['available_time_periods']

    x = stats.compute_metric_data_points metric_types
    pp x

    x = stats.compute_metrics  metric_types
    metric_types.each do |metric_type|
      ['', 'ref1_', 'delta1_'].each do | value_type |
        value_name = value_type + Stats::METRIC_NAMES[metric_type]
        puts "'#{value_name}' => #{x[value_name]}, "
        my_assert "metric mismatch #{value_name}: (#{x[value_name]} != #{hash[value_name]})'#{value_name}' => #{x[value_name]}" , x[value_name] == hash[value_name]
     end
    end
    sla_metrics = [:frame_loss, :frame_loss_ratio, :delay, :delay_variation,  ]
    severity_levels = [:critical, :major, :minor]
    sla_metrics.each do |metric_type|
      severity_levels.each do | severity_level |
        count = stats.count_errored_periods  severity_level, metric_type
        count_name = "#{metric_type}_#{severity_level}"
        puts "'#{count_name}' => #{count},"
        my_assert "sample count mismatch #{count_name}: ( #{count}!= #{hash[count_name]})", count == hash[count_name]
      end
    end
    puts '}'
    i += 1
  end
end


# TODO rerun same test while setting last_hop_circuit_id to nil
# TODO rerun same test while setting last_hop_circuit_id to empty string
# TODO rerun same test while setting ref_circuit_id to nil
# TODO rerun same test while setting ref_circuit_id to empty string

