# Turning off coverage to see if issue is resolved
# from /home/deployer/.rvm/gems/ruby-1.9.2-p180@oss_db/gems/simplecov-0.5.3/lib/simplecov/result_merger.rb:18:in `resultset'
# if ENV['RUN_COVERAGE'] == "1"
#   require 'simplecov'
#   system "mkdir -p ../public/coverage" unless File.exists?("../public/coverage")
#   SimpleCov.configure do
#     coverage_dir '../public/coverage'
#   end
#   SimpleCov.start 'rails'
# end

# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'factory_girl'
Factory.find_definitions

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

SoftwareError.instance.mode = :TEST

# To deal redirect_to problems (see https://github.com/defunkt/cijoe/issues/62)
# The following can't be done in Rails 3.1
# ActionController::Base.relative_url_root = ''

RSpec.configure do |config|
  # == Mock Framework
  #
  # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
  #
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr
  config.mock_with :rspec

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/test/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true
  
  # If you're not using ActiveRecord you should remove these
  # lines, delete config/database.yml and disable :active_record
  # in your config/boot.rb
  config.use_instantiated_fixtures  = false
  
  #SW_ERR tests
  # Don't think this is required, SW_ERRs rails errors anyways in tests
  config.before(:each){
    @log_size = File.exists?("#{Rails.root}/log/test.log") ? File.size("#{Rails.root}/log/test.log") : 0
  }
  config.after(:each){
    if File.exists?("#{Rails.root}/log/test.log")
      file = File.open("#{Rails.root}/log/test.log", "r")
      file.pos = @log_size
      new_entries = file.gets(nil)
      if new_entries
        errors = new_entries.scan(/^SW_ERR\(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\): .*$/)
        raise errors.join("\n") unless errors.empty?
      end
    end
  }
end

# reindex sphinx table always needs a row with id=1
ReindexSphinx.create({:id=>1})

$debug = ENV["QUIET"] != "1"
$RSPEC_LOADED_DB = nil
def load_database(name)
  if $RSPEC_LOADED_DB != name
    puts "LOADING #{name}" if $debug
    results = `rake spec:seed_database:#{name}`  
    puts results
    $RSPEC_LOADED_DB = name
  else
    puts "RE-USING #{name}" if $debug
  end
  true
end

$RSPEC_ARCHIVE_LOADED_DB = nil
def load_data_archive_database(name)
  if $RSPEC_ARCHIVE_LOADED_DB != name
    puts "LOADING data_archive #{name}" if $debug
    results = `rake spec:seed_data_archive_database:#{name}`
    puts results
    $RSPEC_ARCHIVE_LOADED_DB = name
  else
    puts "RE-USING #{name}" if $debug
  end
  true
end

# Monkeypatch fixing missing tempfile handler in rack-test
class Rack::Test::UploadedFile
  def tempfile
    @tempfile
  end
end