
require 'tempfile'  
require "yaml"

def execute_sql(db,command,include_db)
  f = Tempfile.new('execute_sql')
  f.print(command)
  f.flush
  puts "CREATE SQL FILE WITH: #{command}"
  execute_sql_file(db,f.path,include_db)
  f.delete
end

def execute_sql_file(db,filename,include_db)
  password_string = db['password'] ? " -p#{db['password']}" : ''
  database_string = db['database'] && include_db ? " -D #{db['database']}" : ''
  command_string = "mysql -u #{db['username']}#{password_string}#{database_string} < #{filename}"
  puts "ABOUT TO RUN: #{command_string}"
  %x[#{command_string}]
end