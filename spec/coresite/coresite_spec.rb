require 'spec_helper'
require 'coresite/helper'

describe "CoreSite Build", :coresite_app => true do

  before(:all) do
    load_database("CDB_EXPORT_CoreSite_DB_For_Test").should == true unless ENV['SKIP_DB']
    Find.find("#{Rails.root}/lib/coresite/").each {|p| load p if FileTest.file?(p)}
    $sp_a = ServiceProvider.find_by_name("Tenant A").id
    $sp_z = ServiceProvider.find_by_name("Tenant Z").id
    $site_a = Site.find_by_name("CENX Playground").id
    $site_z = Site.find_by_name("CENX Playground").id
    $on_a = OperatorNetwork.find_by_name("Tenant A [CENX Playground]").id
    $on_z = OperatorNetwork.find_by_name("Tenant Z [CENX Playground]").id
    $node_a = Node.find_by_name("FAKE.IDC185.LAB.MLX16.01").id
    $node_z = Node.find_by_name("FAKE.IDC185.LAB.MLX16.02").id

    $buyer_member_attr = MemberAttrInstance.find_by_value_and_affected_entity_type("Tenant A MH", "Demarc")
    $seller_member_attr = MemberAttrInstance.find_by_value_and_affected_entity_type("Tenant Z MH", "Demarc")
    $nni_a = $buyer_member_attr.affected_entity.id
    $nni_z = $seller_member_attr.affected_entity.id
  end
  
  
  describe "Enni" do
    it "should be ok" do
      bc = CoresiteBuildCenter.new
      order_info = {:tenant_name => $on_a, :phy_type => "1000Base-LX; 1310nm; SMF", :node => $node_a, :port => "6/6", :work_order => "666", :external_name => "Tenant A MH large"}
      bl = bc.enni_order(order_info)
      check_result(bl, "Ok")
      
      buyer_large_member_attr = MemberAttrInstance.find_by_value_and_affected_entity_type("Tenant A MH large", "Demarc")
      nni_id = buyer_large_member_attr.affected_entity.id
      
      order_info = {:nni_id => nni_id, :work_order => "5768987590"}  
      bl = bc.enni_disconnect_order(order_info)
      check_result(bl, "Ok")
    end
    
    
    it "should fail" do
      bc = CoresiteBuildCenter.new
      order_info = {:tenant_name => $on_a, :phy_type => "junk", :node => $node_a, :port => "6/1", :work_order => "4567688745", :external_name => "Tenant A MH"}
      bl = bc.enni_order(order_info)
      check_result(bl, "Failed")
      
      order_info = {:tenant_name => $on_z, :phy_type => "10GigE LR; 1310nm; SMF", :node => $node_z, :port => "666", :work_order => "5768987591", :external_name => "Tenant Z MH"}  
      bl = bc.enni_order(order_info)
      check_result(bl, "Failed")
      
      order_info = {:tenant_name => $on_z, :phy_type => "junk", :node => $node_z, :port => "6/6", :work_order => "5768987591", :external_name => "Tenant Z MH"}  
      bl = bc.enni_order(order_info)
      check_result(bl, "Failed")
    end
    
  end
  
  describe "AutoAccept" do
    it "should be ok" do
      bc = CoresiteBuildCenter.new
      #enni_success

      # New
      order_info = tenant_a_order
      bl = bc.process_order(order_info)
      path = MemberHandleInstance.where(:owner_type => ServiceProvider, :owner_id => $sp_a, :affected_entity_type => "Path", :value => "Tenant A MH").first.affected_entity
      check_result(bl, "Ok", path, false)

      # Change
      change_order_info = tenant_a_change_order.merge({:path_id => path.id, :segment_id => path.id, :cir => 110})
      bl = bc.process_change_order(change_order_info)
      check_result(bl, "Ok", path, false)

      # Disconnect
      bl = bc.process_disconnect_order(change_order_info)
      check_result(bl, "Ok", path, false)
    end
  end
  
  describe "Non AutoAccept" do
    it "should be ok" do
      bc = CoresiteBuildCenter.new
      #enni_success

      # New
      order_info = tenant_a_order
      bl = bc.process_order(order_info, false)
      path = MemberHandleInstance.where(:owner_type => ServiceProvider, :owner_id => $sp_a, :affected_entity_type => "Path", :value => "Tenant A MH").first.affected_entity
      check_result(bl, "Ok", path, false)
      
      # New Accept
      accept_order_info = tenant_z_new_accept.merge({:path_id => path.id, :segment_id => path.id}) 
      bl = bc.accept_order(accept_order_info)
      check_result(bl, "Ok", path, false)
      
      # Change  
      change_order_info = tenant_a_change_order.merge({:path_id => path.id, :segment_id => path.id, :cir => 110})
      bl = bc.process_change_order(change_order_info, false)
      check_result(bl, "Ok", path, false)
      
      # Change Accept  
      bl = bc.accept_change_order(change_order_info)
      check_result(bl, "Ok", path, false)
      
      # Disconnect
      bl = bc.process_disconnect_order(change_order_info)
      check_result(bl, "Ok", path, false)
    end
  end
  
  describe "Abort Orders" do
    it "should be ok" do
      bc = CoresiteBuildCenter.new
      #enni_success

      # New
      order_info = tenant_a_order
      bl = bc.process_order(order_info, false)
      path = MemberHandleInstance.where(:owner_type => ServiceProvider, :owner_id => $sp_a, :affected_entity_type => "Path", :value => "Tenant A MH").first.affected_entity
      check_result(bl, "Ok", path, false)
      
      # New Cancel  
      abort_order_info = order_info.merge({:path_id => path.id, :segment_id => path.id})   
      bl = bc.cancel_order(abort_order_info)
      check_result(bl, "Ok", path, false)
      
      # New
      order_info = tenant_a_order
      bl = bc.process_order(order_info, false)
      path = MemberHandleInstance.where(:owner_type => ServiceProvider, :owner_id => $sp_a, :affected_entity_type => "Path", :value => "Tenant A MH").first.affected_entity
      check_result(bl, "Ok", path, false)
      
      # New Reject
      abort_order_info = order_info.merge({:path_id => path.id, :segment_id => path.id})   
      bl = bc.reject_order(abort_order_info)
      check_result(bl, "Ok", path, false)
      
      # New
      order_info = tenant_a_order
      bl = bc.process_order(order_info, false)
      path = MemberHandleInstance.where(:owner_type => ServiceProvider, :owner_id => $sp_a, :affected_entity_type => "Path", :value => "Tenant A MH").first.affected_entity
      check_result(bl, "Ok", path, false)
      
      # New Accept
      accept_order_info = tenant_z_new_accept.merge({:path_id => path.id, :segment_id => path.id})
      bl = bc.accept_order(accept_order_info)
      check_result(bl, "Ok", path, false)
      
      # Change
      order_info = tenant_a_change_order
      change_order_info = order_info.merge({:path_id => path.id, :segment_id => path.id, :cir => 110})
      bl = bc.process_change_order(change_order_info, false)
      check_result(bl, "Ok", path, false)
      
      # Change Cancel
      bl = bc.cancel_order(change_order_info)
      check_result(bl, "Ok", path, false)
      
      # Change
      bl = bc.process_change_order(change_order_info, false)
      check_result(bl, "Ok", path, false)

      # Change Reject
      bl = bc.reject_order(change_order_info)
      check_result(bl, "Ok", path, false)
      
      # Change
      bl = bc.process_change_order(change_order_info, false)
      check_result(bl, "Ok", path, false)
     
      # Change Accept
      bl = bc.accept_change_order(change_order_info)
      check_result(bl, "Ok", path, false)

      # Disconnect
      bl = bc.process_disconnect_order(change_order_info)
      check_result(bl, "Ok", path, false)
    end
  end
  
  describe "New Order errors" do
    it "should fail" do
      bc = CoresiteBuildCenter.new
      # New
      order_info = tenant_a_order
      order_info[:cir] = 10000
      bl = bc.process_order(order_info, false)
      check_result(bl, "Failed")

      order_info = tenant_a_order
      order_info[:vlan_a] = 10000
      bl = bc.process_order(order_info, false)
      check_result(bl, "Failed")

      order_info = tenant_a_order
      order_info[:tenant_a] = "junk"
      bl = bc.process_order(order_info, false)
      check_result(bl, "Failed")

      order_info = tenant_a_order
      order_info[:tenant_z] = "junk"
      bl = bc.process_order(order_info, false)
      check_result(bl, "Failed")

      order_info = tenant_a_order
      order_info[:nni_a] = 666
      bl = bc.process_order(order_info, false)
      check_result(bl, "Failed")

      order_info = tenant_a_order
      order_info[:member_handle_a] = "hello{there}"
      bl = bc.process_order(order_info, false)
      check_result(bl, "Failed")
    end
  end
  
  describe "Change Order errors" do
    it "should fail" do
      bc = CoresiteBuildCenter.new
       # New
       order_info = tenant_a_order
       bl = bc.process_order(order_info)
       check_result(bl, "Ok")
       path = MemberHandleInstance.where(:owner_type => ServiceProvider, :owner_id => $sp_a, :affected_entity_type => "Path", :value => "Tenant A MH").first.affected_entity

       order_info = tenant_a_change_order
       order_info[:cir] = 10000
       change_order_info = order_info.merge({:path_id => path.id, :segment_id => path.id})
       bl = bc.process_change_order(change_order_info, false)
       check_result(bl, "Failed")

       order_info = tenant_a_change_order
       order_info[:tenant_a] = "junk"
       change_order_info = order_info.merge({:path_id => path.id, :segment_id => path.id})
       bl = bc.process_change_order(change_order_info, false)
       check_result(bl, "Failed")

       order_info = tenant_a_change_order
       order_info[:tenant_z] = "junk"
       change_order_info = order_info.merge({:path_id => path.id, :segment_id => path.id})
       bl = bc.process_change_order(change_order_info, false)
       check_result(bl, "Failed")

       order_info = tenant_a_change_order
       order_info[:nni_a] = 666
       change_order_info = order_info.merge({:path_id => path.id, :segment_id => path.id})
       bl = bc.process_change_order(change_order_info, false)
       check_result(bl, "Failed")

       order_info = tenant_a_change_order
       order_info[:member_handle_a] = "hello{there}"
       change_order_info = order_info.merge({:path_id => path.id, :segment_id => path.id})
       bl = bc.process_change_order(change_order_info, false)
       check_result(bl, "Failed")
    end
  end
  
  describe "New Order Accept Errors" do
    it "should fail" do
      bc = CoresiteBuildCenter.new
      # New
      order_info = tenant_a_order
      bl = bc.process_order(order_info, false)
      check_result(bl, "Ok")
      path = MemberHandleInstance.where(:owner_type => ServiceProvider, :owner_id => $sp_a, :affected_entity_type => "Path", :value => "Tenant A MH").first.affected_entity
      accept_order_info = order_info.merge({:path_id => path.id, :segment_id => path.id})

      order_info = tenant_z_new_accept
      order_info[:tenant_a] = "junk"
      accept_order_info = order_info.merge({:path_id => path.id, :segment_id => path.id})
      bl = bc.accept_order(accept_order_info)
      check_result(bl, "Failed", path, false)

      order_info = tenant_z_new_accept
      order_info[:tenant_z] = "junk"
      accept_order_info = order_info.merge({:path_id => path.id, :segment_id => path.id})
      bl = bc.accept_order(accept_order_info)
      check_result(bl, "Failed", path, false)

      order_info = tenant_z_new_accept
      order_info[:nni_a] = 666
      accept_order_info = order_info.merge({:path_id => path.id, :segment_id => path.id})
      bl = bc.accept_order(accept_order_info)
      check_result(bl, "Failed", path, false)

      order_info = tenant_z_new_accept
      order_info[:member_handle_a] = "hello{there}"
      accept_order_info = order_info.merge({:path_id => path.id, :segment_id => path.id})
      bl = bc.accept_order(accept_order_info)
      check_result(bl, "Failed", path, false)

      order_info = tenant_z_new_accept
      bc.stub_options[:oss] = :failure
      accept_order_info = order_info.merge({:path_id => path.id, :segment_id => path.id})
      bl = bc.accept_order(accept_order_info)
      check_result(bl, "Failed", path, false)

      order_info = tenant_z_new_accept
      bc.stub_options[:oss] = :connect_fail
      accept_order_info = order_info.merge({:path_id => path.id, :segment_id => path.id})
      bl = bc.accept_order(accept_order_info)
      check_result(bl, "Failed", path, false)

      ActiveRecord::Base.transaction(:requires_new => true) do
        order_info = tenant_z_new_accept
        bc.stub_options[:oss] = :ok
        bc.stub_options[:router_config] = :failure
        accept_order_info = order_info.merge({:path_id => path.id, :segment_id => path.id})
        bl = bc.accept_order(accept_order_info)
        check_result(bl, "Fallout", path, true)
        raise ActiveRecord::Rollback 
      end
      bc.stub_options[:router_config] = :connect_fail
      bl = bc.accept_order(accept_order_info)
      check_result(bl, "Fallout", path, true)
    end
  end
  
  describe "Change Order Accept Errors" do
    it "should fail" do
      bc = CoresiteBuildCenter.new
      # New
      order_info = tenant_a_order
      bl = bc.process_order(order_info)
      check_result(bl, "Ok")
      path = MemberHandleInstance.where(:owner_type => ServiceProvider, :owner_id => $sp_a, :affected_entity_type => "Path", :value => "Tenant A MH").first.affected_entity
      
      # Change  
      change_order_info = tenant_a_change_order.merge({:path_id => path.id, :segment_id => path.id, :cir => 110})
      bc.process_change_order(change_order_info, false)
      check_result(bl, "Ok", path, false)
      
      order_info = change_order_info.clone
      order_info[:tenant_a] = "junk"
      bl = bc.accept_change_order(order_info)
      check_result(bl, "Failed", path, false)

      order_info = change_order_info.clone
      order_info[:tenant_z] = "junk"
      bl = bc.accept_change_order(order_info)
      check_result(bl, "Failed", path, false)

      order_info = change_order_info.clone
      order_info[:nni_a] = 666
      bl = bc.accept_change_order(order_info)
      check_result(bl, "Failed", path, false)

      order_info = change_order_info.clone
      order_info[:member_handle_a] = "hello{there}"
      bl = bc.accept_change_order(order_info)
      check_result(bl, "Failed", path, false)

      order_info = change_order_info.clone
      bc.stub_options[:oss] = :failure
      bl = bc.accept_change_order(order_info)
      check_result(bl, "Failed", path, false)

      order_info = change_order_info.clone
      bc.stub_options[:oss] = :connect_fail
      bl = bc.accept_change_order(order_info)
      check_result(bl, "Failed", path, false)
      bc.stub_options[:oss] = :ok

      order_info = change_order_info.clone
      ActiveRecord::Base.transaction(:requires_new => true) do
        bc.stub_options[:router_config] = :failure
        bl = bc.accept_change_order(change_order_info)
        check_result(bl, "Fallout", path, true)
        raise ActiveRecord::Rollback 
      end
            
      bc.stub_options[:router_config] = :connect_fail
      bl = bc.accept_change_order(change_order_info)
      check_result(bl, "Fallout", path, true)
    end
  end


  describe "Disconnect Fails router config" do
    it "should be ok" do
      bc = CoresiteBuildCenter.new

      # New
      order_info = tenant_a_order
      bl = bc.process_order(order_info)
      path = MemberHandleInstance.where(:owner_type => ServiceProvider, :owner_id => $sp_a, :affected_entity_type => "Path", :value => "Tenant A MH").first.affected_entity
      check_result(bl, "Ok", path, false)

      # Disconnect
      bc.stub_options[:router_config] = :failure
      change_order_info = tenant_a_change_order.merge({:path_id => path.id, :segment_id => path.id})
      bl = bc.process_disconnect_order(change_order_info)
      check_result(bl, "Fallout", path, true)  
    end
  end

end


