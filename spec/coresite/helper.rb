def tenant_a_order
  {:tenant_a => $on_a,
    :nni_a => $nni_a,
    :vlan_a => "466",
    :member_handle_a =>"Tenant A MH",
    :tenant_z => $on_z,
    :member_handle_z =>"Tenant Z MH",
    :cir => 100,
    :requested_service_date => "2012/10/12",
    :user => "david.schubert@cenx.com"
   }
end

def tenant_a_change_order
  {:path_id => 17,
   :segment_id => 17,
   :tenant_a => $on_a,
   :nni_a => $nni_a,
   :vlan_a => 466,
   :member_handle_a =>"Tenant A MH",
   :tenant_z => $on_z,
   :member_handle_z =>"Tenant Z MH",
   :nni_z => $nni_z,
   :vlan_z => 567,
   :cir => 100,
   :requested_service_date => "2013/10/12",
   :user => "david.schubert1@cenx.com"
  }
end
  
def tenant_z_new_accept
  {:path_id => 52,
   :segment_id => 52,
   :tenant_a => $on_a,
   :nni_a => $nni_a,
   :vlan_a => 466,
   :member_handle_a =>"Tenant A MH",
   :tenant_z => $on_z,
   :member_handle_z =>"Tenant Z MH",
   :nni_z => $nni_z,
   :vlan_z => 567,
   :cir => 100,
   :requested_service_date => "2012/10/12",
   :user => "bjw@coresite.com"
  }
end  

def check_result(bl, bl_exp_result, path = nil, path_exp_result = nil)
  if bl.summary != bl_exp_result
    puts bl.details
  end
  puts bl.summary_detail.green if !bl.summary_detail.empty?
  bl.summary.should == bl_exp_result
  if path
    path.reload
    if path.fallout? != path_exp_result
      puts bl.details
    end
    path.fallout?.should == path_exp_result
  end
end