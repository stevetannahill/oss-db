require 'spec_helper'

describe AutomatedBuildCenterController do
  render_views

  before(:all) do
    load_database("EMPTY_DB").should == true unless ENV['SKIP_DB']
  end
  
  after(:all) do
    `rm -rf ./tmp_circuit_order`    
  end
  
  before(:each) do
    controller.stub(:set_current_user, :authorize_admin_user)
    sp = mock_model(ServiceProvider, 'is_sprint?' => true)
    controller.current_user = mock_model(User, 'system_admin?' => true, 'roles' => [], 'privileges' => [], 'service_provider' => sp )

    @redis = MockRedis.new
    @app = Eve::Application.new(:display => :string, :redis => @redis)
    Osl::Application.new(@app).run([])
    @app.reset
    AutomatedBuildCenterController.any_instance.stub(:osl_app).and_return(@app)    
  end
  
  it "should have the right routes" do
    assert_routing({ :path => '/automated_build_center',:method => 'get' }, {:controller => 'automated_build_center', :action => 'index'})
  end

  describe "#index" do
    
    it "should load all cascades" do
      @app.cmd("cid new ch123 10")
      @app.cmd("cid new ch124 11")
      get :index
      response.should be_success
      #assigns(:all_cascades).should == [{"cascade"=>"ch123", "gci"=>"10"}, {"cascade"=>"ch124", "gci"=>"11"}]
    end
    
  end
  
end