require 'spec_helper'


describe DbController do
  render_views

  it "should have the right routes" do
    assert_routing({ :path => '/db',:method => 'get' }, {:controller => 'db', :action => 'index'})
    assert_routing({ :path => '/db/reset/abc_sql',:method => 'get' }, {:controller => 'db', :action => 'reset', :id => "abc_sql"})
    assert_routing({ :path => '/db/reset_orders',:method => 'get' }, {:controller => 'db', :action => 'reset_orders'})
    assert_routing({ :path => '/db/reset_grid',:method => 'get' }, {:controller => 'db', :action => 'reset_grid'})
    assert_routing({ :path => '/db/reset_lkp',:method => 'get' }, {:controller => 'db', :action => 'reset_lkp'})
  end

end