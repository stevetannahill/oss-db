require 'spec_helper'

describe AdminController do
  render_views

  before(:all) do
    load_database("EMPTY_DB").should == true unless ENV['SKIP_DB']    
  end

  before(:each) do
    controller.stub(:set_current_user, :authorize_admin_user)
    service_provider = mock_model(ServiceProvider, 'is_sprint?' => true)
    controller.current_user = mock_model(User, 'system_admin?' => true, 'roles' => [], 'privileges' => [], 'service_provider' => service_provider)

    socket = mock("socket")
    socket.stub!(:write).and_return {|msg| msg}
    socket.stub!(:recv).and_return "Hello World!"
    socket.stub!(:recv).with(8).and_return 0
    socket.stub!(:close).and_return nil
    TCPSocket.stub!(:open).and_return(socket)
    
    Marshal.stub!(:dump).and_return("Hello World!")
    Factory.sequence(:objectFullName){|n| "Object Name #{n}"}
    Marshal.stub!(:load).and_return{[{"objectFullName" => Factory.next(:objectFullName)}]}
    
    instance = mock("instance")
    instance.stub!(:post_event).and_return(nil)
    EventClient.stub!(:instance).and_return(instance)
    
    JmsAlarmParse.stub!(:extract_JMS_alarm_info).and_return([])
  end
  
  it "All the above passed with no SW_ERRs" do
    load "db/fake_go_in_service.rb"
    r = srand()
    srand(r)
    puts "  Seeded with #{r}" if $debug
    puts "  Creating Member Handles" if $debug
    ["Path", "Demarc", "Segment", "SegmentEndPoint"].each{|type|
      puts "    Creating handle for #{type}" if $debug
      get :new_ma
      response.should be_success
    
      get :save_ma, {
        :ma => Factory.build(:class_member_handle, :name => "ID for #{type}", :affected_entity_type => type, :syntax => "string", :allow_blank => ["Segment", "SegmentEndPoint"].include?(type)).attributes
      }
      response.should redirect_to(:action => :index_cma)
    }
      
    puts "  Creating Service Providers with Operator Network Type and Operator Network" if $debug
    service_providers = []
    Dir.foreach("#{Rails.root}/lib/oss_import/imported_oss_data"){|file|
      if file.match(/^[^\/?*:;{}\\]+\.txt$/) && !service_providers.map{|name| name.downcase }.include?(file[0..-5].downcase)
        service_providers.push(file[0..-5])
      end
    }
    
    service_providers.each{|sp_name|
      puts "    Creating #{sp_name}" if $debug
      
      get 'new_sp'
      response.should be_success
        
      get 'save_sp', {
        :sp => Factory.attributes_for(:service_provider, :name => sp_name, :is_system_owner => sp_name.downcase.include?('cenx'))
      }
      response.should redirect_to(:action => :index_sp)
      sp = assigns[:sp]
      # is_system_owner is protected so cannot do a mass-assignment
      sp.is_system_owner = sp_name.downcase.include?('cenx')
      sp.save
      
      2.times{
        get 'new_contact', { :id => sp.id}
        response.should be_success
        
        get 'save_contact', {
          :contact => Factory.attributes_for(:contact),
          :id => sp.id
        }
        response.should redirect_to(:action => :edit_sp, :id => sp.id)
      }
    
      ont_name = "#{sp_name} Operator Network Type"
      puts "       Creating Network Type #{ont_name}"
      get 'new_ont', { :id => sp.id }
      response.should be_success
    
      get 'save_ont', {
        :ont => Factory.attributes_for(:operator_network_type, :name => "#{ont_name}"),
        :id => sp.id
      }
      response.should redirect_to(:action => :edit_sp, :id => assigns[:sp].id)
      ont = assigns[:ont]

      on_name = "#{sp_name} Operator Network"
      puts "       Creating Network #{on_name}"
      get 'new_on', {:id => sp.id}
      response.should be_success
      
      get 'save_on', {
        :on => Factory.attributes_for(:operator_network, :operator_network_type_id => ont.id, :name => "#{on_name}"),
        :id => sp.id
      }
      response.should redirect_to(:action => :edit_sp, :id => sp.id)
    }
        
    puts "  Creating Network Managers" if $debug
    ["5620 SAM - Lab", "5620 SAM - Live", "BrixWorx - Live", "Sprint NEM #1", "Sprint Spirent Server"].each{|name|
      puts "    Creating Network Manager called #{name}" if $debug
      get :new_nm
      response.should be_success
      
      nm_type = nil
      case name
      when "5620 SAM - Lab", "5620 SAM - Live"
        nm_type = "5620Sam"
      when "BrixWorx - Live"
        nm_type = "BrixWorx"
      when "Sprint NEM #1"
        nm_type = "Sprint NEM"
      when "Sprint Spirent Server"
        nm_type = "eScout"
      end

      get :save_nm, {
        :nm => Factory.build(:network_manager, :name => name, :nm_type => nm_type).attributes
      }
      response.should redirect_to(:action => :edit_nm, :id => assigns[:nm].id)
    }
    
    puts "  Creating and seeding and Sites" if $debug
    #Create Sites, and then seed the Nodes, Ports, and Ennis

    sp = ServiceProvider.where(:name => 'CENX').first
    sp.is_system_owner = 1
    sp.save
    ont = sp.operator_network_types.first

    setups = {
      "SprintMSC_Samsung" => [["Primary"]],
      "SprintMSC_ALU" => [["Primary"]],
      "SprintMSC_Ericsson" => [["Primary"]],
      "StandardExchange7750" =>  [["Primary", "Secondary"], ["Primary"]],
      "StandardExchange7450" =>  [["Primary"]],
      "LightSquaredExchange7450_TAP" => [["Primary"]],
      "LightSquaredExchange7450_RSC" => [["Primary"]],
      "LightSquaredExchange7210_SGW" => [["Primary"]]
    }
    
    setups.each{|ext, info|
      valid_types = HwTypes::VALID_SITE_LAYOUT.collect do |site_layouts, network_type|
        network_type if site_layouts.include? ext
      end.compact
      ont_name = random_item(valid_types, "Operator Network Type", "PopHelper::VALID_SITE_LAYOUT")
      ont = OperatorNetworkType.find_by_name(ont_name)
      if ont.nil?
        puts "    Creating Operator Network Type: #{ont_name}"
        get 'new_ont', { :id => sp.id }
        response.should be_success

        get 'save_ont', {
          :ont => Factory.attributes_for(:operator_network_type, :name => ont_name),
          :id => sp.id
        }
        puts assigns[:ont].errors.full_messages unless assigns[:ont].errors.empty?
        response.should redirect_to(:action => :edit_sp, :id => assigns[:sp].id)
        ont = assigns[:ont]
      end

      info.each_with_index{|sites, i|
        puts "    Creating and seeding #{ext} with #{sites.join(" and ")} Site(s)" if $debug
        get 'new_on', {:id => sp.id}
        response.should be_success

        get 'save_on', {
          :on => Factory.attributes_for(:operator_network, :operator_network_type_id => ont.id, :name => "Cenx #{ext} Network #{i+1}"),
          :id => sp.id
        }
        puts assigns[:on].errors.full_messages unless assigns[:on].errors.empty?
        response.should redirect_to(:action => :edit_sp, :id => sp.id)
        on = assigns[:on]
        
        sites.each { |site|
          get 'new_site', {:site => {:type => "AggregationSite"}}
          response.should be_success
          
          get 'save_site', {
            :class => "AggregationSite",
            :site => Factory.attributes_for(:site, :operator_network_id => on.id, :site_type => site, :site_layout => ext.dup)
          }
          puts assigns[:site].errors.full_messages unless assigns[:site].errors.empty?
          response.should redirect_to(:action => :edit_site, :id => assigns[:site].id)
          site = assigns[:site]
          site.seed if site.can_seed # TODO BJW Should make a web call here
        }
      }
    }
=begin No longer works, need better method to deal with multiple sites types
    puts "    Checking sites seeded properly" if $debug
    Node.all.should have(2 * HwTypes::PORT_SEEDS.length).nodes
    Port.all.should have(2 * HwTypes::PORT_SEEDS.collect{|key, value| value.collect{|key, value| value}}.flatten.size).ports
=end

  puts "  Placing nodes in service" if $debug
  #Place each node in service
  Node.all.each{|node|
    puts "    Bringing #{node.name} into service" if $debug
    [ProvTesting, ProvReady, ProvLive].each do |state|        
      #set referer to deal with backs
      silence_stream(STDOUT) do
      	request.env["HTTP_REFERER"] = "http://test.com/admin/edit_node/#{node.id}"
        #request.env["HTTP_REFERER"] = ActionController::UrlRewriter.new(request, {}).rewrite_url(:action => :edit_node, :id => node.id)
        get 'sh_new_state', { :id => node.prov_state.id, :new_state => state.to_s }
      end
      response.should be_success
      node.reload
      raise "Node: #{node.name} failed change state from #{node.get_prov_state.class} to #{state.to_s}" unless node.get_prov_state.is_a?(state)
    end
  }

    puts "  Importing OSS Data" if $debug
    #Import OSS Data
    service_providers_d = service_providers.map{|sp| sp.downcase}
    Dir.foreach("#{Rails.root}/lib/oss_import/imported_oss_data"){|file|
      if service_providers_d.include? file[0..-5].downcase
        puts "    Importing data for #{file}" if $debug
        sp = ServiceProvider.first(:conditions => ["name = ?", service_providers[service_providers_d.index(file[0..-5].downcase)]])
        if sp
          $args = ["--file", "#{Rails.root}/lib/oss_import/imported_oss_data/#{file}",
            "--member", sp.name,
            "--network", sp.operator_network_types.first.name,
            "--db", "test"]
          silence_stream(STDOUT) do #So put statements don't show
            load "#{Rails.root}/lib/oss_import/oss_import_new.rb"
          end
        end
      end
    }

    #Convert all existing CENX CoS Types to the new sub class
    ons = ServiceProvider.find_by_is_system_owner(true).operator_networks

    ons.each do |on|
      on.operator_network_type.class_of_service_types.each do |cos|
        cos.type = "CenxClassOfServiceType"
        cos.save :validate => false
      end
    end

    puts "  Creating Order types and groups" if $debug
    EthernetServiceType.all.each{|est|
      puts "    Creating for #{est.name} on #{est.operator_network_type.name}" if $debug
      get 'new_ot', {:id => est.operator_network_type_id}
      response.should be_success
      
      get 'save_ot', {:id => est.operator_network_type_id, :ot => Factory.attributes_for(:order_type, :operator_network_type_id => est.operator_network_type_id)}
      response.should redirect_to(:action => :edit_ont, :id => est.operator_network_type_id)
      ot = assigns[:ot]
      
      get 'new_oeg', {:id => ot.id}
      response.should be_success
      
      get 'save_oeg', {:id => ot.id, :oeg => Factory.attributes_for(:ordered_entity_group, :order_type_id => ot.id, :ethernet_service_type_id => est.id)}
      response.should redirect_to(:action => :edit_ot, :id => ot.id)
      oeg = assigns[:oeg]
      
      est.segment_types.each{|segmentt|
        get 'choose_type_oeg', {:id => oeg.id}
        response.should be_success
        
        get 'link_type_oeg', {:id => oeg.id, :oet => {:id_class => "#{segmentt.id};SegmentType"}}
        response.should redirect_to(:action => 'edit_oeg', :id => oeg.id )
      }
    }
        
    puts "  Ordering ENNIs" if $debug
    #Order Ennis
    HwTypes::PROTECTION_TYPES.each{|pt_show, pt|

      #TODO need function for this
        lag_mode = nil
        case pt
        when "MCard-LAG", "SCard-LAG"
          lag_mode = "Active/Standby"
        when "MChassis-LAG"
          lag_mode = "Active/Standby"
        when "unprotected-LAG"
          lag_mode = "N/A"
        when "unprotected"
          lag_mode = nil
        end

      2.times{
        ons = OperatorNetwork.all.reject{|on| on.service_provider.is_system_owner}
        on = ons[rand(ons.length)]
        puts "    Ordering ENNI: #{pt_show} (#{lag_mode}) on #{on.name} - #{on.service_provider.is_system_owner}"
        params = {:ordered_entity_type => "ENNI"}
        get 'new_so', params
        response.should be_success
        
        params[:so] = Factory.build(:enni_order_new, :ordered_entity_id => nil, :operator_network_id => on.id).attributes
        get 'save_so', params
        response.should redirect_to(:action => :new_demarc, :id => on.id, :so_id => assigns[:so].id)
        
        params = {:id => assigns[:so].id}
        get 'edit_so', params
        response.should be_success
        
        params[:so] = assigns[:so].attributes
        get 'update_so', params
        response.should redirect_to(:action => :new_demarc, :id => on.id, :so_id => assigns[:so].id)

        assigns[:so].reload
        on.reload
        
        params = {:id => on.id, :so_id => assigns[:so].id}
        get 'new_demarc', params
        response.should be_success
        
        params[:demarc] = Factory.build(:enni_new, :protection_type => pt, :lag_mode => lag_mode, :service_orders => [assigns[:so]]).attributes
        if assigns[:demarc].member_handle_instance_exists MemberAttr.first(:conditions => ["affected_entity_id is NULL and affected_entity_type = 'Demarc'"]).name, assigns[:demarc].service_provider
          mh = assigns[:demarc].member_handle_instance MemberAttr.first(:conditions => ["affected_entity_id is NULL and affected_entity_type = 'Demarc'"]).name, assigns[:demarc].service_provider
          params[mh.form_id] = on.service_provider.is_system_owner ? nil : Factory.next(:demarc_member_handle)
        end
        get 'save_demarc', params
        raise assigns[:demarc].errors.full_messages.join("\n") unless assigns[:demarc].errors.empty?
        response.should redirect_to(:action => :edit_demarc, :id => assigns[:demarc].id)
      }
    }
    
    puts "  Testing ENNI creating with bad data" if $debug
    ons = OperatorNetwork.all.reject{|on| on.service_provider.is_system_owner}
    params = {:id => ons[rand(ons.length)].id, :demarc_type => "ENNI"}
    get 'new_demarc', params
    response.should be_success
    
    params = {:id => params[:id], :demarc => Factory.build(:enni_new, :operator_network_id => params[:id], :protection_type => "unprotected-LAG", :lag_mode => "N/A").attributes }
    #get 'save_demarc', params
    #response.should render_template("admin/edit_demarc.html.erb")
    
    puts "  Creating ENNI without an Order" if $debug
    #No order enni
    if assigns[:demarc].member_handle_instance_exists MemberAttr.first(:conditions => ["affected_entity_id is NULL and affected_entity_type = 'Demarc'"]).name, assigns[:demarc].service_provider
      mh = assigns[:demarc].member_handle_instance MemberAttr.first(:conditions => ["affected_entity_id is NULL and affected_entity_type = 'Demarc'"]).name, assigns[:demarc].service_provider
      params[mh.form_id] = assigns[:on].service_provider.is_system_owner ? nil : Factory.next(:demarc_member_handle)
    end
    get 'save_demarc', params
    response.should redirect_to(:action => :edit_demarc, :id => assigns[:demarc].id)
    demarc = assigns[:demarc]
    
    params = {:id => demarc.id}
    get 'edit_demarc', params
    response.should be_success
    
    params[:demarc] = demarc.attributes
    get 'update_demarc', params
    response.should redirect_to(:action => :edit_demarc, :id => demarc.id)
    
    params = {:id => demarc.id}
    request.env["HTTP_REFERER"] = "http://test.com/admin/new_demarc/#{demarc.operator_network.id}"
    get 'destroy_demarc', params
    response.should be_redirect
    
    #TODO add conected items to properly test gen config and in service
    puts "  Running ENNI Gen Configs" if $debug
    #ENNI genconfig
    EnniNew.all.each{|enni|
      if enni.ports.empty?   
        puts "    Adding Ports to #{enni.name}" if $debug
        ports = []
        while(enni.new_port_can_be_linked)
          get 'choose_port_demarc', {:id => enni.id}
          response.should be_success

          enni.reload
          ports = enni.get_valid_site_list.map{|site| site.nodes.map{|node| node.unlinked_ports}}.flatten
          ports.reject!{|port| !port.node.is_service_router}
          ports.reject!{|port| port.is_monitoring_port}
          ports.reject!{|port| port.is_port_connected_to_demarc}
          unless enni.protection_type.match(/^unprotected/)
            ports.reject!{|port| port.card_info.nil?}
            #ports = ports.reject{|port| port.is_test_port}
            unless enni.ports.empty?
              nodes = enni.ports.collect{|port| port.node}
              cards = enni.ports.collect{|port| port.card_info}
              case enni.protection_type
                when "MChassis-LAG" then
                  ports.reject!{|port| nodes.include?(port.node)}
                when "MCard-LAG" then
                  ports.reject!{|port| cards.include?(port.card_info) || !nodes.include?(port.node)}
                when "SCard-LAG" then
                  ports.reject!{|port| !cards.include?(port.card_info) || !nodes.include?(port.node)}
              end
            end
          end

          break if ports.empty?
          port = ports[rand(ports.length)]

          puts "      Adding Port: #{port.name}" if $debug
          get 'link_port_demarc', {:id => enni.id, :port => {:id => port.id}}
          port.reload
          raise assigns[:demarc].errors.full_messages.join("\n") unless assigns[:demarc].errors.empty?
          response.should redirect_to(:action => 'edit_demarc', :id => enni.id )
          enni.ports(true)
        end
      end
      puts "    Running Gen Config for #{enni.name}" if $debug
      params[:id] = enni.id
      get 'config_gen_demarc', params
      response.should be_success
    }

    puts "  Bringing ENNIS into service" if $debug
    #Bring ENNIs into service
    EnniNew.all.each{|enni|
      puts "    Bringing #{enni.name} into service" if $debug
      # If the order state is OrderCreated then move to OrderAccepted
      enni_order = enni.get_latest_order
      if enni_order != nil          
        if enni_order.get_order_state.is_a?(OrderCreated)
          [OrderReadyToOrder, OrderReceived, OrderAccepted].each do |order_state|
            silence_stream(STDOUT) do
              request.env["HTTP_REFERER"] = "http://test.com/admin/edit_so/#{enni.get_latest_order.id}"
              get 'sh_new_state', { :id => enni.get_latest_order.order_state.id, :new_state => order_state} 
            end
            response.should be_success
            enni_order.reload
            raise "ENNI Order: #{enni.name} failed change state from #{enni_order.get_order_state.class} to #{order_state.to_s}" unless enni_order.get_order_state.is_a?(order_state)
          end
        end
      end
      
      [ProvTesting, ProvReady, ProvLive].each do |state|   
        # When going to live and the enni has an order then the order must be set to Accepted before going Live
        if enni.get_latest_order != nil          
          order_state = nil
          if state == ProvTesting 
            order_state_should_be = OrderAccepted
            order_state = "OrderDesignComplete"
          elsif state == ProvLive
            order_state_should_be = OrderTested
            order_state = "OrderCustomerAccepted"
          end
      
          if order_state != nil
            if enni.get_latest_order.get_order_state.is_a?(order_state_should_be)
              silence_stream(STDOUT) do
                request.env["HTTP_REFERER"] = "http://test.com/admin/edit_so/#{enni.get_latest_order.id}"
                get 'sh_new_state', { :id => enni.get_latest_order.order_state.id, :new_state => order_state} 
              end
              response.should be_success
            end
          end       
        end
            
        silence_stream(STDOUT) do
          request.env["HTTP_REFERER"] = "http://test.com/admin/edit_demarc/#{enni.id}"
          get 'sh_new_state', { :id => enni.prov_state.id, :new_state => state.to_s } 
        end
        response.should be_success
        enni.reload
        raise "ENNI: #{enni.name} failed change state from #{enni.get_prov_state.class} to #{state.to_s}" unless enni.get_prov_state.is_a?(state)
      end
    }
  
    puts "  Creating Paths" if $debug
    OperatorNetwork.all.each{|on|
      puts "    Creating Path on #{on.name}" if $debug
      params = {:id => on.id}
      get 'new_path', params
      response.should be_success
      
      #fail with no member handle
#      params[:path] = Factory.attributes_for(:path, :operator_network_id => on.id)
#      get 'save_path', params
#      response.should render_template("admin/new_path.html.erb")
      
      params[:path] = Factory.attributes_for(:path, :operator_network_id => on.id, :cenx_path_type => "Standard", :member_handle => "Test Path")
      params[:class] = 'Evc'
      get 'save_path', params
      #response.should render_template("admin/new_path.html.erb")
      response.should redirect_to(:action => :edit_path, :id => assigns[:path].id)
    }
    
    puts "  Ordering On Net OVCs" if $debug
    Path.all.each{|path|
      puts "    Ordering On Net OVC on #{path.cenx_name}" if $debug
      params = {:ordered_entity_type => "OnNetOvc"}
      get 'new_so', params
      response.should be_success
      
      params[:so] = Factory.build(:on_net_ovc_order, :ordered_entity_id => nil, :path_id => path.id).attributes
      get 'save_so', params
      response.should redirect_to(:action => :new_segment, :id => assigns[:so].operator_network_id, :so_id => assigns[:so].id)
      
      params = {:id => assigns[:so].id}
      get 'edit_so', params
      response.should be_success
        
      params[:so] = assigns[:so].attributes
      get 'update_so', params
      response.should redirect_to(:action => :new_segment, :id => assigns[:so].operator_network_id, :so_id => assigns[:so].id)
      
      params = {:id => path.id, :so_id => assigns[:so].id}
      get 'new_segment', params
      response.should be_success
      
      segment = Factory.build(:on_net_ovc)
      params[:segment] = segment.attributes
      params[:segment_class] = "OnNetOvc"
      segment.paths.push(path)
      segment.get_member_attr_instances.each{|mai|
        params[mai.form_id] = segment.cenx_name
      }
      
      get 'save_segment', params
      raise assigns[:segment].errors.full_messages.join("\n") unless assigns[:segment].errors.empty?
      response.should redirect_to(:action => :edit_segment, :id => assigns[:segment].id)
    }
    
    puts "  Building On Net OVCs" if $debug
    OnNetOvc.all.each{|onovc|
      num_end_points = (rand(2) + 2)
      puts "    Build: #{onovc.name} add #{num_end_points}" if $debug
      
      types = CenxClassOfServiceType.find(:all)

      #Flip a coin, if 1 use a random type, if 0 use a random permutation of 3
      types = (rand(2) == 1 ? [random_item(types, "CenxClassOfServiceType", "Database")] :
        random_item(types.permutation(3).to_a, "CenxClassOfServiceType", "Permutation"))
    
      types.each do |type|
        puts "      -Adding: CosInstance #{type.name}" if $debug
        get "new_cosi", {:id => onovc.id}
        response.should be_success
        cosi = Factory.build(:cenx_cos_instance, :class_of_service_type_id => type.id)

        get "save_cosi", {:id => onovc.id, :cosi => cosi.attributes}
        response.should redirect_to(:action => :edit_segment, :id => onovc.id)

      end
      
      num_end_points.times{
        params = {:id => onovc.id, :segmentep_class => "OnNetOvcEndPointEnni"}
        get 'new_segmentep', params
        response.should be_success
        params[:segmentep] = Factory.build(:on_net_ovc_end_point_enni, :segment_id => onovc.id).attributes
        get 'save_segmentep', params
        segmentep = assigns[:segmentep]
        puts "      -Create: Endpoint #{segmentep.cenx_name}" if $debug
        raise assigns[:segmentep].errors.full_messages.join("\n") unless assigns[:segmentep].errors.empty?
        response.should redirect_to(:action => :edit_segmentep, :id => segmentep.id )
        
        get 'choose_demarc_segmentep', {:id => segmentep.id}
        response.should be_success
      
        demarcs = segmentep.get_candidate_demarcs
        demarc = demarcs[rand(demarcs.length)]
        puts "      -Attach: Endpoint #{segmentep.cenx_name} to #{demarc.cenx_name}" if $debug
        get 'link_demarc_segmentep', {:id => segmentep.id, :demarc => demarc.attributes}
        response.should redirect_to(:action => 'edit_segmentep', :id => segmentep.id )
        
        onovc.cos_instances.each do |cosi|
          puts "      -Add: CosEndpoint #{cosi.cos_name}" if $debug
          get 'new_cose', {:id => segmentep.id}
          response.should be_success
          
          cos = if demarc.is_connected_to_test_port
            Factory.build(:test_cos_end_point, :cos_instance_id => cosi.id)
          else
            Factory.build(:cos_end_point, :cos_instance_id => cosi.id)
          end
          
          get 'save_cose', {:cose => cos.attributes, :bwpt_type => 0,:id => segmentep.id}
          puts assigns["cose"].errors.full_messages unless assigns["cose"].errors.empty?
          response.should redirect_to(:action => :edit_segmentep, :id => segmentep.id)
        end
        
      }

      puts "      -Configuring QoS Policies for #{onovc.name}" if $debug
      params[:id] = onovc.id
      get 'config_gen_qosp', params
      response.should be_success

      puts "      -Running Gen Config for #{onovc.name}" if $debug
      params[:id] = onovc.id
      get 'config_gen_segment', params
      response.should be_success
    }

    puts "  Bringing On Net OVCs into service" if $debug
    OnNetOvc.all.each{|onovc|
      puts "    Bringing #{onovc.name} into service" if $debug
      [ProvTesting, ProvReady, ProvLive].each do |state|  
        # When going to live and the Segment has an order then the order must be set to Accepted before going Live
        if onovc.get_latest_order != nil          
          order_state = nil
          if state == ProvTesting 
            order_state_should_be = OrderAccepted
            order_state = "OrderDesignComplete"
          elsif state == ProvLive
            order_state_should_be = OrderTested
            order_state = "OrderCustomerAccepted"
          end
      
          if order_state != nil
            if onovc.get_latest_order.get_order_state.is_a?(order_state_should_be)
              silence_stream(STDOUT) do
                request.env["HTTP_REFERER"] = "http://test.com/admin/edit_so/#{onovc.get_latest_order.id}"
                get 'sh_new_state', { :id => onovc.get_latest_order.order_state.id, :new_state => order_state} 
              end
              response.should be_success
            end
          end       
        end 
                  
        silence_stream(STDOUT) do
          request.env["HTTP_REFERER"] = "http://test.com/admin/edit_segment/#{onovc.id}"
          get 'sh_new_state', { :id => onovc.prov_state.id, :new_state => state.to_s }      
        end
        response.should be_success
        onovc.reload
        raise "On Net OVC: #{onovc.name} failed change state from #{onovc.get_prov_state.class} to #{state.to_s}" unless onovc.get_prov_state.is_a?(state)        
      end
    }

    puts "  Ordering Off Net OVCs" if $debug
    Path.all.each{|path|
      puts "    Ordering Off Net OVC on #{path.cenx_name}" if $debug
      params = {:ordered_entity_type => "OffNetOvc"}
      get 'new_so', params
      response.should be_success
      
      params[:so] = Factory.build(:off_net_ovc_order, :ordered_entity_id => nil, :path_id => path.id).attributes
      get 'save_so', params
      response.should redirect_to(:action => :new_segment, :id => assigns[:so].operator_network_id, :so_id => assigns[:so].id)
      
      params = {:id => assigns[:so].id}
      get 'edit_so', params
      response.should be_success
        
      params[:so] = assigns[:so].attributes
      get 'update_so', params
      response.should redirect_to(:action => :new_segment, :id => assigns[:so].operator_network_id, :so_id => assigns[:so].id)
      
      params = {:id => path.id, :so_id => assigns[:so].id}
      get 'new_segment', params
      response.should be_success
      
      segment = Factory.build(:off_net_ovc)
      segment.paths.push(path)
      segment.get_member_attr_instances.each{|mai|
        params[mai.form_id] = segment.name
      }
      params[:segment] = segment.attributes
      params[:segment_class] = "OffNetOvc"
      get 'save_segment', params
      response.should redirect_to(:action => :edit_segment, :id => assigns[:segment].id)
    }
    
    # check all the KPIs have been created - Note the tests above do not create any BxCosTestVectors so can't check..
    (EnniNew.all + OnNetOvcEndPointEnni.all).each do |object|
      kpi_names = object.kpis.find_all_by_actual_kpi(true).collect {|kpi| kpi.name}
      (Kpi::KPI_BY_KLASS[object.class.name] - kpi_names).should eq([]), "Failed KPI for #{object.class}:#{object.id} - KPIs are #{kpi_names} should be #{Kpi::KPI_BY_KLASS[object.class.name]}"
    end

  end
  
end
