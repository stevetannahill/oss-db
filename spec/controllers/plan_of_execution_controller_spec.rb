require 'spec_helper'

# POE CONTROLLER OBSOLETE -- REMOVING SPECS
# describe PlanOfExecutionController do
#   render_views

#   before(:all) do
#     load_database("EMPTY_DB").should == true unless ENV['SKIP_DB']
#   end

#   after(:all) do
#     `rm -rf ./tmpupload`    
#   end

#   before(:each) do
#     controller.stub(:set_current_user, :authorize_admin_user)
#     controller.current_user = mock_model(User, 'system_admin?' => true, 'roles' => [], 'privileges' => [] )

#     @redis = MockRedis.new
#     @app = Eve::Application.new(:display => :string, :redis => @redis)
#     Osl::Application.new(@app).run([])
#     @app.cmd("cm dir ./tmp_poe/cm")
#     @app.reset
#     PlanOfExecutionController.any_instance.stub(:osl_app).and_return(@app)    
#     PlanOfExecutionController.any_instance.stub(:queued_jobs).and_return([])
#   end
  
#   it "should have the right routes" do
#     assert_routing({ :path => '/plan_of_execution',:method => 'get' }, {:controller => 'plan_of_execution', :action => 'index'})
#     assert_routing({ :path => '/plan_of_execution/new',:method => 'get' }, {:controller => 'plan_of_execution', :action => 'new'})
#     assert_routing({ :path => '/plan_of_execution/show/1',:method => 'post' }, {:controller => 'plan_of_execution', :action => 'show', :id => '1'})
#     assert_routing({ :path => '/plan_of_execution/scheduled/1',:method => 'post' }, {:controller => 'plan_of_execution', :action => 'scheduled', :id => '1'})
#     assert_routing({ :path => '/plan_of_execution/create',:method => 'post' }, {:controller => 'plan_of_execution', :action => 'create'})
#     assert_routing({ :path => '/plan_of_execution/process/1',:method => 'post' }, {:controller => 'plan_of_execution', :action => 'process_poe', :id => '1'})
#   end
  
#   describe "#index" do
    
#     it "should load all poes" do
#       poe = Osl::PlanOfExecution.new(:app => @app, :display => :string)
#       Osl::PlanOfExecution.stub!(:all).and_return([poe])
#       get :index
#       response.should be_success
#       assigns(:all).should == [poe]
#     end
    
#   end

#   describe "#new" do
    
#     it "should load" do
#       get :new
#       response.should be_success
#     end
    
#   end

#   describe "#create" do
  
#     before(:each) do
#       @uploaded_file = Rack::Test::UploadedFile.new(Rails.root.join("spec/support/poe.csv"), 'text/csv')
#       @uploader = Osl::Uploader.new(:base_dir => "./tmpupload")
#       Osl::Uploader.stub!(:new).and_return(@uploader)
#     end
    
#     it "should fail nicely if uploader crashes" do
#       @uploader.stub(:upload).and_return(false)
#       post :create, :file_upload => { :poe => @uploaded_file }
#       assigns[:poe].should == nil
#       flash[:notice].should == "Unable to upload file"
#       response.should render_template("new")
#     end
  
#     it "should fail if poe wasn't saved" do
#       Osl::PlanOfExecution.any_instance.stub(:create).and_return(false)
#       post :create, :file_upload => { :poe => @uploaded_file }, :commit => "Create NEW VIP"
#       assigns[:poe].id.should == nil
#       response.should render_template("new")
#     end
    
#     it "should create poe if everything ok" do
#       Osl::PlanOfExecution.any_instance.stub(:create).and_return(true)
#       Osl::PlanOfExecution.any_instance.stub(:id).and_return(99)
      
#       post :create, :file_upload => { :poe => @uploaded_file }, :commit => "Create NEW VIP"
#       assigns[:poe].id.should_not == nil
#       response.should redirect_to(:controller => :plan_of_execution, :action => :show, :id => 99)
#     end
    
#   end
  
#   describe "#show" do
    
#     it "should load the bulk_order" do
#       PlanOfExecutionController.any_instance.stub(:has_workers?).and_return(true)
#       bo = BulkOrder.new(:title => "Blah")
#       BulkOrder.stub!(:where).with("id = ?","99").and_return([bo])
#       get :show, :id => 99
#       response.should be_success
#       assigns(:poe).should_not == nil
#       assigns(:poe).bulk_order.should == bo
#     end
  
#   end
  
#   describe "#scheduled" do
    
#     it "should load the bulk_order" do
#       bo = BulkOrder.new(:title => "Blah")
#       BulkOrder.stub!(:where).with("id = ?","99").and_return([bo])
#       get :scheduled, :id => 99
#       response.should be_success
#       assigns(:poe).should_not == nil
#       assigns(:poe).bulk_order.should == bo
#     end
  
#   end

  
# end