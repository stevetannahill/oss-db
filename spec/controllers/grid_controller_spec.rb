require 'spec_helper'

describe GridController do
  render_views

  before(:all) do
    load_database("EMPTY_DB").should == true unless ENV['SKIP_DB']
  end
  
  before(:each) do
    controller.stub(:set_current_user, :authorize_admin_user)

    sp = mock_model(ServiceProvider, 'is_sprint?' => true)
    controller.current_user = mock_model(User, 'system_admin?' => true, 'roles' => [], 'privileges' => [], 'service_provider' => sp )

    @eve = GridApp.mock("grid_controller")
    GridController.any_instance.stub(:grid_app).and_return(GridApp.eve)    
  end
  
  it "should have the right routes" do
    assert_routing({ :path => '/grid',:method => 'get' }, {:controller => 'grid', :action => 'index'})
  end

  describe "#index" do
    
    it "should load" do
      get :index
      response.should be_success
    end
    
  end

end