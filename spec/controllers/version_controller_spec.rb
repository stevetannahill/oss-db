require 'spec_helper'


describe VersionController do
  render_views

  before(:all) do
    load_database("EMPTY_DB").should == true unless ENV['SKIP_DB']    
  end

  before(:each) do
    controller.stub(:set_current_user, :authorize_admin_user)
    sp = mock_model(ServiceProvider, 'is_sprint?' => true)
    controller.current_user = mock_model(User, 'system_admin?' => true, 'roles' => [], 'privileges' => [], 'service_provider' => sp)

    Appstats::Logger.reset
    Appstats::Logger.filename_template = "test_appstats_authenticated_system"
    Time.stub!(:now).and_return(Time.parse('2010-09-21 23:15:20 UTC'))
  end
  
  after(:each) do
    File.delete(Appstats::Logger.filename) if File.exists?(Appstats::Logger.filename)
    $cas_server_enabled = false
  end

  it "should have the right routes" do
    assert_routing({ :path => '/version',:method => 'get' }, {:controller => 'version', :action => 'show'})
  end
  
  it "should track the appstats" do
    session[:cas_last_valid_ticket] = @ticket
    session[:session_id] = "123"
    post :show
    expected = Appstats::Logger.entry_to_s('page-view',:controller => 'version', :action => 'show', :session_id => '123')
    Appstats::Logger.raw_read.should == [expected]
  end

  it "should ignore requests from monit" do
    post :show, :monit => '1'
    Appstats::Logger.raw_read.should == []
  end


end