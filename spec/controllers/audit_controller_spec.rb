require 'spec_helper'

describe AuditController do
  render_views

  before(:all) do
    load_database("EMPTY_DB").should == true unless ENV['SKIP_DB']
  end
  
  before(:each) do
    controller.stub(:set_current_user, :authorize_admin_user)
    controller.current_user = mock_model(User, 'system_admin?' => true, 'roles' => [], 'privileges' => [] )
    controller.current_user.stub(:service_provider).and_return(mock_model(User, 'is_sprint?' => false))

    @osl = OslApp.mock("audit_controller")
    @osl.cmd("pub queue-mode redis")
    @osl.cmd("pub queue-host mem")
    AuditController.any_instance.stub(:osl_app).and_return(OslApp.eve)    

    @grid = OslApp.mock("grid_controller")
    AuditController.any_instance.stub(:grid_audit_app).and_return(GridApp.eve)    
  end
  
  it "should have the right routes" do
    assert_routing({ :path => '/audit',:method => 'get' }, {:controller => 'audit', :action => 'index'})
  end

  describe "#index" do
    
    it "should load" do
      get :index
      response.should be_success
    end
    
  end

end