require 'spec_helper'
require 'automated_build_center/helper'

describe "Build Cicuit" do

  # There is no table DataArchiveRecord in the database as this is a remote
  # database which we want to stub so the .stub fails with
  # undefined method `any_instance' for DataArchiveIf(Table doesn't exist):Class
  # So override the ruby way ...  
  class DataArchiveRecord < ActiveRecord::Base
    self.abstract_class = true
  end  
  
  class DataArchiveIf < DataArchiveRecord
    def self.last_availability_for(class_name, time_name, columns, where, limit=1)
      []
    end
  end

  before(:all) do
    load_database("CDB_EXPORT_Sprint_For_ABC_Test_Suite").should == true unless ENV['SKIP_DB']
    @fake_grid_id = 666
    @osl_app = Eve::Application.new(:config => "#{Rails.root}/config/osl.poe.yml")
    Osl::Application.new(@osl_app).run([])
    @osl_app.cmd("cid daemon_init")
    AutomatedBuildCenter.standalone(true)
    PlayBook.csv_file = AutomatedBuildCenter.abc_root +  "playbook/test_playbook.csv"
    MscSite.csv_file = AutomatedBuildCenter.abc_root + "sat_sites/Test_Sat_Sites_Addres_MSC.csv"
    @supported_circuits = get_full_build_circuits
    $fake_output = false
  end
  
  it "All should be built OK" do
    @supported_circuits.each do |cid|
      ts = Time.now
      puts "Building #{cid}".green
      @osl_app.cmd("cid new #{cid[:circuit_id]} #{@fake_grid_id}")
      puts "Time #{Time.now-ts}".green
      @fake_grid_id += 1
      check_builder_log(cid[:circuit_id], "Ok")
    end
  end

end


