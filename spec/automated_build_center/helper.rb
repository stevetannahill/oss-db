def check_builder_log(cid, expected_summary)
  bl = BuilderLog.find_by_name(cid) 
  if bl
    fail("Builder log result (#{bl.summary}) does not match expected (#{expected_summary}) for #{cid}\nsummary detail:#{bl.summary_detail}\ndetails:\n#{bl.details}") if bl.summary != expected_summary
  else
    fail("No Builder log generated for #{cid}")
  end
end

def get_full_build_circuits
  # Get list of circuits to be built
  test_file = File.expand_path("../../../abc_dev/spirent_info/test_circuits.txt",__FILE__)
  test_circuits = File.open(test_file, "r").readlines("\n")
  test_circuits.map! do |circuit_id|
    circuit_id.chomp!
    decoded_cid = CircuitIdFormat::SpirentSprint.decode(circuit_id)
    pb = PlayBook.data(decoded_cid[:site_name])
    {:circuit_id => circuit_id, :oem => pb[:oem], :access_provider => pb[:access_provider], :site_type => decoded_cid[:site_type]}
  end

  return test_circuits
end

# ********** Update and run this in rails console every time a new OEM, AAV, or site type is added. **********
# instructions:
# *** this should only be run when a new OEM, AAV, or site type is added.  And will take an hour or two ***
# start a rails console.
# require 'automated_build_center/helper'
# go to [oss-db]/spec/spec_helper and copy the load_database definition into the console.
# also copy the $debug and $RSPEC_LOADED_DB definitions.
# call get_all_circuit_build_paths
def get_all_circuit_build_paths
  time_start = Time.now
  load_database("CDB_EXPORT_Sprint_For_ABC_Test_Suite")
  PlayBook.csv_file = AutomatedBuildCenter.abc_root +  "playbook/test_playbook.csv"
  MscSite.csv_file = AutomatedBuildCenter.abc_root + "sat_sites/Test_Sat_Sites_Addres_MSC.csv"
  #SingleSpirentInfo.csv_file = AutomatedBuildCenter.abc_root +  "spirent_info/partial_circuit_list.csv"

  junk = SingleSpirentInfo.data("junk") # make sure file is loaded
  csv_data = SingleSpirentInfo.csv_data

  @osl_app = Eve::Application.new(:config => "config/osl.poe.yml")
  Osl::Application.new(@osl_app).run([])
  @osl_app.cmd("cid daemon_init")
  AutomatedBuildCenter.standalone(true)

  
  # Get list of circuits to be built
  info = Hash.new{|h,k| h[k]=Hash.new{|h1,k1| h1[k1]=Hash.new(false)}}

  oems = ["Ericsson", "Samsung", "ALU"]
  #ETH and DNR build the same way and we will select DNRs to build to support the MW circuits
  site_types = AutomatedBuildCenter::SUPPORTED_SITE_TYPE
  mw_site_types = AutomatedBuildCenter::MICROWAVE_SITE_TYPES
  # MW circuits cannot be built untill after their DNR sites are built.
  access_providers = AutomatedBuildCenter::SUPPORTED_ACCESS_PROVIDER

  fake_grid_id = 666
  
  circuits = csv_data.keys
  selected_circuits = []

  puts "attempting to select successful builds from #{circuits.size} Circuits".green

  circuits.each do |cid|
    decoded_cid = CircuitIdFormat::SpirentSprint.decode(cid)
    if decoded_cid
      pb = PlayBook.data(decoded_cid[:site_name])
      if pb      
        # if this circuit oem/provider/type has not been built before
        # and it's not a DNR (ETH and DNR build the same and DNRs will be selected to support MW sites when a MW site is picked)
        if !info[pb[:oem]][pb[:access_provider]][decoded_cid[:site_type]] && decoded_cid[:site_type] != "DNR"

          # build dependency or complementary circuit first.
          if mw_site_types.include?(decoded_cid[:site_type])
            # MW circuits need their reference circuit to be built before them
            csv_spirent_data = SingleSpirentInfo.data(cid)
            if csv_spirent_data # return ref_circuit_id or nil (csv_spirent_data == nil)
              circuit_start = Time.now
              puts "Building #{csv_spirent_data[:ref_circuit_id]}".green

              if (build_results = @osl_app.cmd("cid new #{csv_spirent_data[:ref_circuit_id]} #{fake_grid_id}")) && build_results[:data][:summary] == "Ok"
                fake_grid_id = fake_grid_id + 1
                puts "Success!".green
                selected_circuits << csv_spirent_data[:ref_circuit_id]
              else
                fake_grid_id = fake_grid_id + 1
                puts "Failed!".red
                next # don't bother with mw if dnr doesn't build
              end
              puts "Build Time #{Time.now-circuit_start}\n".green
              temp = (Time.now-time_start).to_i
              puts "Circuits built: #{fake_grid_id - 666}, cumulative time: #{temp / 3600}h #{(temp % 3600) / 60}m #{(temp % 3600) % 60}s\n".green
            end
          else
            if pb[:access_provider] == "Sprint MW"
              next # "Sprint MW" is the access provider for MW sites so this build will fail.
            end
            # find the p (or b) of the current circuit and build it as well
            new_p_or_b = decoded_cid[:p_b] == "P" ? "B" : "P"
            
            regex = Regexp.new("#{cid.gsub(/-V.*-/, "-V.*-").gsub(/-.-V/, "-#{new_p_or_b}-V")}")
            #return other cid (or nil)
            other_cid = SingleSpirentInfo.csv_data.find {|key,value| key[regex]}
            if other_cid
              circuit_start = Time.now
              puts "Building #{other_cid[0]}".green

              if (build_results = @osl_app.cmd("cid new #{other_cid[0]} #{fake_grid_id}")) && build_results[:data][:summary] == "Ok"
                fake_grid_id = fake_grid_id + 1
                puts "Success!".green
                selected_circuits << other_cid[0]
              else
                fake_grid_id = fake_grid_id + 1
                puts "Failed!".red
                next #don't bother with the complementary circuit if this one didn't build.
              end

              puts "Build Time #{Time.now-circuit_start}\n".green
              temp = (Time.now-time_start).to_i
              puts "Circuits built: #{fake_grid_id - 666}, cumulative time: #{temp / 3600}h #{(temp % 3600) / 60}m #{(temp % 3600) % 60}s\n".green
            end
          end

          #now build selected circuit
          circuit_start = Time.now
          puts "Building #{cid}".green
          build_results = @osl_app.cmd("cid new #{cid} #{fake_grid_id}")
          fake_grid_id = fake_grid_id + 1
          if build_results[:data][:summary] == "Ok"
            puts "Success!".green
            info[pb[:oem]][pb[:access_provider]][decoded_cid[:site_type]] = true # don't build this type of circuit again.
            selected_circuits << cid # return value
          else
            puts "Failed!".red
            if selected_circuits.last =~ /DNR/ # don't keep the supporting DNR for a failed MW build
              puts "removing donor".red
              selected_circuits.pop
            end
          end
          puts "Build Time #{Time.now-circuit_start}\n".green
          temp = (Time.now-time_start).to_i
          puts "Circuits built: #{fake_grid_id - 666}, cumulative time: #{temp / 3600}h #{(temp % 3600) / 60}m #{(temp % 3600) % 60}s\n".green
        end
      end
    end
  end

  File.open("abc_dev/spirent_info/test_circuits.txt", "w") do |f|
    selected_circuits.each{ |circuit| f.write("#{circuit}\n")}
  end

  temp = (Time.now-time_start).to_i
  puts "Total Run Time #{temp / 3600}h #{(temp % 3600) / 60}m #{(temp % 3600) % 60}s\n".green
end


