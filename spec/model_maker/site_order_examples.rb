module ModelMaker
  class OrderExamples

    def self.site_7750_order
      order = {}

      order[:bulk_orders] = {
        "Ennis" => {
          :bulk_order_type => "Circuit Order",
          :title => "Bulk Order for Test Ennis",
          :operator_network => 32,
          :primary_contact => 16,
          :testing_contact => 17,
          :technical_contact => 18,
          :local_contact => 18,
          :order_created_date => "25-05-2011",
          :order_received_date => "26-05-2011",
          :requested_service_date => "27-05-2011"
        }
      }

      order[:sites] = [
        {
          :builder_type => "AggregationSite",
          :site_groups => ["Ottawa"],
          :name => "ON-Ottawa: Downtown",
          :operator_network => {:name => "Ottawa Generated ON", :service_provider => 1, :operator_network_type => 10},
          :address => "123 Main St",
          :contact_info => "Merlin already foresaw the issue and fixed it!",
          :site_host => "Cenx",
          :floor => "1",
          :clli => "clli",
          :icsc => "icsc",
          :network_address => "192.168.0.1/24",
          :broadcast_address => "192.168.0.255",
          :host_range => "192.168.0.1 - 192.168.0.254",
          :hosts_per_subnet => "510",
          :node_prefix => "DTOWN",
          :postal_zip_code => "12345",
          :emergency_contact_info => "Yell really loudly!",
          :site_access_notes => "Click heels together three times",
          :notes => "Look out for Grues",
          :city => "Ottawa",
          :state_province => "ON",
          :site_type => "Primary",
          :country => "Canada",
          :service_id_low => "12340",
          :service_id_high => "12345",
          :site_layout => "StandardExchange7750",
        }
      ]

      enni_a_order = {
        :title => "Test ENNI in Ottawa",
        :operator_network => 32,
        :ordered_operator_network => 17,
        :primary_contact => 16,
        :testing_contact => 17,
        :technical_contact => 18,
        :local_contact => 18,
        :order_created_date => "25-05-2011",
        :order_received_date => "26-05-2011",
        :requested_service_date => "27-05-2011",
        :ordered_entity_group => 0,
        :bulk_orders => ["Ennis"]
      }

      order[:demarcs] = {
        "enni_a" => {
          :service_order => enni_a_order,
          :builder_type => "Enni",
          :cenx_demarc_type => "ENNI",
          :operator_network => 17,
          :demarc_type => 53,
          :address => "123 Charter St",
          :demarc_icon => "enni",
          :site => "ON-Ottawa: Downtown",
          :protection_type => "unprotected-LAG",
          :fiber_handoff_type => "SC",
          :lag_mode => "N/A",
          :physical_medium => "10GigE LR; 1310nm; SMF",
          :ether_type => "0x8100",
          :ports => [["DTOWNALU01", "1/1/1"]],
          :member_handles => [
            [1, "OMG, OMG YOU GUYS! THIS ONE'S PERFECT AND IT'S JUST MY SIZE!"],
            [11, "IF THERE EVER WAS A PERFECT COUPLE THIS WOULD QUALIFY!"],
          ],
        }

      }


      return order
    end

    def self.site_alu_order
      order = {}

      order[:sites] = [
        {
          :name => "ON-OttawaALU",
          :builder_type => "AggregationSite",
          :operator_network => 33,
          :address => "123 Main St",
          :contact_info => "Merlin already foresaw the issue and fixed it!",
          :site_host => "Cenx",
          :floor => "1",
          :clli => "clli",
          :icsc => "icsc",
          :network_address => "192.168.1.1/24",
          :broadcast_address => "192.168.1.255",
          :host_range => "192.168.1.1 - 192.168.1.254",
          :hosts_per_subnet => "510",
          :node_prefix => "ALU",
          :postal_zip_code => "12345",
          :emergency_contact_info => "Yell really loudly!",
          :site_access_notes => "Click heels together three times",
          :notes => "Look out for Grues",
          :city => "Ottawa",
          :state_province => "ON",
          :site_type => "Primary",
          :country => "Canada",
          :service_id_low => "22340",
          :service_id_high => "22345",
          :site_layout => "SprintMSC_ALU",
        }
      ]

      return order
    end

    def self.site_ericsson_order
      order = {}

      order[:sites] = [
        {
          :name => "ON-OttawaEricsson",
          :builder_type => "AggregationSite",
          :name => "Ericsson",
          :operator_network => 34,
          :address => "123 Main St",
          :contact_info => "Merlin already foresaw the issue and fixed it!",
          :site_host => "Cenx",
          :floor => "1",
          :clli => "clli",
          :icsc => "icsc",
          :network_address => "192.168.3.1/24",
          :broadcast_address => "192.168.3.255",
          :host_range => "192.168.3.1 - 192.168.3.254",
          :hosts_per_subnet => "510",
          :node_prefix => "ERIC",
          :postal_zip_code => "12345",
          :emergency_contact_info => "Yell really loudly!",
          :site_access_notes => "Click heels together three times",
          :notes => "Look out for Grues",
          :city => "Ottawa",
          :state_province => "ON",
          :site_type => "Primary",
          :country => "Canada",
          :service_id_low => "32340",
          :service_id_high => "32345",
          :site_layout => "SprintMSC_Ericsson",
        }
      ]

      return order
    end

  end

end
