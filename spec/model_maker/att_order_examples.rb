module ModelMaker
  class OrderExamples
    def self.att_order
      order = {}
      #Build Demarc Section of Circuit Order
      att_uni_order = {
        :title => "UNI: AT&T Chicago CH03XC081",
        :ordered_operator_network => 25,
        :primary_contact => 16,
        :testing_contact => 17,
        :technical_contact => 18,
        :local_contact => 17,
        :order_created_date => "25-05-2011",
        :order_received_date => "27-11-2011",
        :requested_service_date => "27-12-2011",
        :ordered_entity_group => 0
      }

      order[:demarcs] = {

        "att_uni" => {
          :builder_type => "Uni",
          :service_order => att_uni_order,
          :cenx_demarc_type => "UNI",
          :operator_network => 25,
          :demarc_type => 75,
          :physical_medium  => "1000Base-LX",
          :port_name => "1/1/1",
          :reflection_mechanism => "DMM/DMR",
          :mon_loopback_address => "68:bd:ab:db:16:99",
          :connected_device_type => "Ciena CN3911",
          :address => "Park View Towers\n 5110 S Dr Martin Luther King Jr. Dr\n Chicago, 60615",
          :demarc_icon => "enni",
          :member_handles => [
          [16, "UNI for Sprint CH03XC081"], # ATTs name
          [1, "Access UNI: CH03XC081"]], # Sprints name
        },

        "sprint_enb" => {
          :builder_type => "Uni",
          :cenx_demarc_type => "UNI",
          :operator_network => 24,
          :demarc_type => 78,
          :physical_medium  => "1000Base-LX",
          :port_name => "1/1",
          :connected_device_type => "Samsung SmartMBS",
          :address => "Park View Towers\n 5110 S Dr Martin Luther King Jr. Dr\n Chicago, 60615",
          :demarc_icon => "cell site",
          :member_handles => [
          [16, "-"],
          [1, "ENodeB: CH03XC081"]],
        }
      }

      #############################################
      #                                           #
      #        BEGIN Standard Path Order           #
      #                                           #
      #############################################

      path = Hash.new
      path[:cenx_path_type] = "Sprint Samsung AT&T"
      path[:operator_network] = 24
      path[:service_order] = {
        :bulk_order_type => "Circuit Order",
        :title => "Circuit Order:Chi-Bridgeview/AT&T/Cell Site CH03XC081",
        :primary_contact => 16,
        :testing_contact => 17,
        :technical_contact => 18,
        :local_contact => 17,
        :order_created_date => "25-05-2011",
        :order_received_date => "27-11-2011",
        :requested_service_date => "27-12-2011"
      }

      #Path Member Handles
      #Sprint = 1
      #AT&T = 16
      path[:member_handles] = [
        [1, "CH03XC081"],
        [16, "AT&T's name for CH03XC081 Path"]
      ]

      aav_order = {
        :title => "Access Segment: AT&T Chicago CH03XC081",
        :ordered_operator_network => 25,
        :primary_contact => 16,
        :testing_contact => 17,
        :technical_contact => 18,
        :local_contact => 17,
        :order_created_date => "25-05-2011",
        :order_received_date => "27-11-2011",
        :requested_service_date => "27-12-2011",
        :ordered_entity_group => 0
      }

      #Create the Segment sections involved

      sprint_cs = {
        :builder_type => "Off Net OVC",
        :operator_network => 24,
        :segment_owner_role => "Buyer",
        :ethernet_service_type => 53,
        :segment_type => 53,
        :emergency_contact => 17,
        :member_handles => [
          [1, "CSR Circuit: CH03XC081"],
          [16, "AT&T's name for CH03XC081 CSR"]
        ],

        :segment_endpoints => {
          "sprint_cs:a" => {
            :connected => ["aav:uni"],
            :demarc_order => {
              :builder_type => "Uni",
              :name => "att_uni",
            },
            :ctags => "1,2,3,4",
            :cos_endpoints =>{
              "Real Time" => {
                :ingress_mapping => "5",
                :egress_marking => "5",
                :ingress_rate_limiting => "Policing",
                :ingress_cir_kbps => "50000",
                :ingress_cbs_kB => "30",
                :ingress_eir_kbps => "",
                :ingress_ebs_kB => "",
                :egress_rate_limiting => "None",
                :egress_cir_kbps => "",
                :egress_cbs_kB => "",
                :egress_eir_kbps => "",
                :egress_ebs_kB => "",
                :connected => [["aav:uni", "Real Time - Multipoint"]]
              }
            }
          },
          "sprint_cs:z" => {
            :demarc_order => {
              :builder_type => "Uni",
              :name => "sprint_enb",
            },
            :ctags => "N/A",
          }
        }
      }

      aav = {
        :builder_type => "Off Net OVC",
        :operator_network => 25,
        :service_order => aav_order,
        :segment_owner_role => "Seller",
        :ethernet_service_type => 52,
        :segment_type => 52,
        :emergency_contact => 44,
        :member_handles => [
          [1, "Access Circuit: CH03XC081"],
          [16, "AT&T's name for CH03XC081 Access Circuit"]
        ],

        :segment_endpoints => {
          "aav:uni" => {
            :connected => ["sprint_cs:a"],
            :demarc_order => {
              :builder_type => "Uni",
              :name => "att_uni",
            },
            :segment_end_point_type => 96,
            :is_monitored => 1,
            :reflection_enabled => 1,
            :ctags => "*",
            :mep_id => "1",
            :md_format => "Y.1731 (ITU-T)",
            :md_level => "4",
            :md_name_ieee => "none",
            :ma_format => "icc-based",
            :ma_name => "4{Path_ID}",


            :cos_endpoints => {
              "Real Time - Multipoint" => {
                :ingress_mapping => "5",
                :egress_marking => "-",
                :ingress_rate_limiting => "Policing",
                :ingress_cir_kbps => "50000",
                :ingress_cbs_kB => "1024",
                :ingress_eir_kbps => "",
                :ingress_ebs_kB => "",
                :egress_rate_limiting => "None",
                :egress_cir_kbps => "",
                :egress_cbs_kB => "",
                :egress_eir_kbps => "",
                :egress_ebs_kB => "",
                :connected => [["sprint_cs:a", "Real Time"]],
                :cos_test_vectors => [{
                    :builder_type => "Brix CosTV",
                    :test_type => "Ethernet Frame Delay Test",
                    :delay_error_threshold => "5",
                    :delay_warning_threshold => "4",
                    :dv_error_threshold => "3000",
                    :dv_warning_threshold => "2400",
                    :flr_error_threshold => "0.005",
                    :flr_warning_threshold => "0.004",
                    :availability_guarantee => "99.99",
                    :service_level_guarantee_type => 112,
                    :circuit_id_format => "None"
                  }]
              }
            }
          },
          "aav:enni_1" => {
            :connected => ["sprint_msc_1:a"],
            :demarc_order => {:builder_type => "Enni", :id => 119},
            :cos_endpoints =>{
              "Real Time - Multipoint" => {
                :ingress_mapping => "5",
                :egress_marking => "5",
                :ingress_rate_limiting => "Policing",
                :ingress_cir_kbps => "50000",
                :ingress_cbs_kB => "1024",
                :ingress_eir_kbps => "",
                :ingress_ebs_kB => "",
                :egress_rate_limiting => "None",
                :egress_cir_kbps => "",
                :egress_cbs_kB => "",
                :egress_eir_kbps => "",
                :egress_ebs_kB => "",
                :connected => [["sprint_msc_1:a", "Real Time"]]
              }
            }
          },
          "aav:enni_2" => {
            :connected => ["sprint_msc_2:a"],
            :demarc_order => {:builder_type => "Enni", :id => 120},
            :cos_endpoints =>{
              "Real Time - Multipoint" => {
                :ingress_mapping => "5",
                :egress_marking => "5",
                :ingress_rate_limiting => "Policing",
                :ingress_cir_kbps => "50000",
                :ingress_cbs_kB => "1024",
                :ingress_eir_kbps => "",
                :ingress_ebs_kB => "",
                :egress_rate_limiting => "None",
                :egress_cir_kbps => "",
                :egress_cbs_kB => "",
                :egress_eir_kbps => "",
                :egress_ebs_kB => "",
                :connected => [["sprint_msc_2:a", "Real Time"]]
              }
            }
          }
        }
      }

      sprint_msc_1 = {
        :builder_type => "On Net OVC",
        #:pop => 10,
        :site => 12,
        :ethernet_service_type => 25,
        :segment_type => 25,
        :emergency_contact => 17,
        :member_handles => [
          [1, "SubIF: CH03XC081-1"],
          [16, "AT&T's name for CH03XC081-1 On Net OVC"]
        ],

        :segment_endpoints => {
          "sprint_msc_1:a" => {
            :connected => ["aav:enni_1"],
            :demarc_order => {:builder_type => "Enni", :id => 119},
            :stags => "88",
            :ctags => "*",
            :cos_endpoints =>{
              "Real Time" => {
                :ingress_mapping => "5",
                :egress_marking => "5",
                :ingress_rate_limiting => "Policing",
                :ingress_cir_kbps => "50000",
                :ingress_cbs_kB => "30",
                :ingress_eir_kbps => "",
                :ingress_ebs_kB => "",
                :egress_rate_limiting => "None",
                :egress_cir_kbps => "",
                :egress_cbs_kB => "",
                :egress_eir_kbps => "",
                :egress_ebs_kB => "",
                :connected => [["aav:enni_1", "Real Time - Multipoint"]]
              }
            }
          },
          "sprint_msc_1:Mon" => {
            :demarc_order => {:builder_type => "Enni", :id => 115},
            :stags => "101",
            :ctags => "*",
          },
          "sprint_msc_1:z" => {
            :demarc_order => {:builder_type => "Enni", :id => 123},
            :stags => "N/A",
            :ctags => "*",
          }
        }
      }

      sprint_msc_2 = {
        :builder_type => "On Net OVC",
        #:pop => 10,
        :site => 12,
        :ethernet_service_type => 25,
        :segment_type => 25,
        :emergency_contact => 17,
        :member_handles => [
          [1, "SubIF: CH03XC081-2"],
          [16, "AT&T's name for CH03XC081-2 On Net OVC"]
        ],

        :segment_endpoints => {
          "sprint_msc_2:a" => {
            :connected => ["aav:enni_2"],
            :demarc_order => {:builder_type => "Enni", :id => 120},
            :stags => "88",
            :ctags => "*",
            :cos_endpoints =>{
              "Real Time" => {
                :ingress_mapping => "5",
                :egress_marking => "5",
                :ingress_rate_limiting => "Policing",
                :ingress_cir_kbps => "50000",
                :ingress_cbs_kB => "30",
                :ingress_eir_kbps => "",
                :ingress_ebs_kB => "",
                :egress_rate_limiting => "None",
                :egress_cir_kbps => "",
                :egress_cbs_kB => "",
                :egress_eir_kbps => "",
                :egress_ebs_kB => "",
                :connected => [["aav:enni_2", "Real Time - Multipoint"]]
              }
            }
          },
          "sprint_msc_2:Mon" => {
            :demarc_order => {:builder_type => "Enni", :id => 117},
            :stags => "101",
            :ctags => "*",
          },
          "sprint_msc_2:z" => {
            :demarc_order => {:builder_type => "Enni", :id => 124},
            :stags => "N/A",
            :ctags => "*",
          }
        }
      }

      path[:segments] = {
        "CS" => sprint_cs,
        "AAV" => aav,
        "MSC_1" => sprint_msc_1,
        "MSC_2" => sprint_msc_2
      }
      #############################################
      #                                           #
      #        END Standard Path Order             #
      #                                           #
      #############################################

      order[:paths] = {"Path" => path}

      return order
    end
  end
end
