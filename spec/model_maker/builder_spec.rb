require "spec_helper"
require 'model_maker/requires'

describe ModelMaker do

  before(:all) do
    load_database("CDB_EXPORT_For_Test_Suite").should == true unless ENV['SKIP_DB']
  end

  before(:each) do
    ActsAsValidationsOnDemand.run_validations = true
    ReindexSphinx.reset
  end

  after(:each) do
    ActsAsValidationsOnDemand.run_validations = true
    ReindexSphinx.reset
  end

  it "should support quick" do
    ModelMaker.quick = true
    ActsAsValidationsOnDemand.run_validations?.should == false
    ReindexSphinx.status.should == :ignore
    ModelMaker.quick?.should == true

    ModelMaker.quick = false
    ActsAsValidationsOnDemand.run_validations?.should == true
    ReindexSphinx.status.should == :live
    ModelMaker.quick?.should == false
  end
  
  # DO NOT ADD THE FACTORY HERE UNTIL THE TESTS HAVE BEEN WRITTEN!
  known_tests = [
    :Factory,
    :PathFactory,
    :EvcFactory,
    :SegmentFactory,
    :OnNetSegmentFactory,
    :OvcFactory,
    :OnNetOvcFactory,
    :OnNetRouterFactory,
    :OffNetOvcFactory,
    :SegmentEndPointFactory,
    :OvcEndPointFactory,
    :OvcEndPointUniFactory,
    :OvcEndPointEnniFactory,
    :RouterSubIfFactory,
    :OnNetRouterSubIfFactory,
    :OnNetOvcEndPointEnniFactory,
    :OvcEndPointUniFactory,
    :CosInstanceFactory,
    :CenxCosInstanceFactory,
    :CosEndPointFactory,
    :CenxCosEndPointFactory,
    :MonitorCosEndPointFactory,
    :CosTestVectorFactory,
    :BxCosTestVectorFactory,
    :SpirentCosTestVectorFactory,
    :DemarcFactory,
    :UniFactory,
    :EnniFactory,
    :ServiceOrderFactory,
    :OnNetOvcOrderFactory,
    :OffNetOvcOrderFactory,
    :BulkOrderFactory,
    :DemarcOrderFactory,
    :EnniOrderFactory,
    :SiteFactory,
    :AggregationSiteFactory,
    :OperatorNetworkFactory,
    :SiteGroupFactory,
    :PortFactory,
  ]

  ModelMaker.constants.each do |const|
    begin
      klass = ModelMaker.const_get(const)
      object = klass.new if klass.respond_to?(:new)
    rescue ArgumentError
      #Some Constants in ModelMaker take parameters ... Factories don't
    end

    if object.is_a? ModelMaker::Factory
      it "should have a #{const} test" do
        known_tests.should include(const)
      end
    end
  end
end
