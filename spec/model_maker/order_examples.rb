module ModelMaker
  class OrderExamples

    def self.standard_order
      order = {}
      #Build Demarc Section of Circuit Order
      demarc_z_order = {
        :title => "Time Warner Cable order for Uni on Time Warner Cable Los Angeles MSO Network",
        :ordered_operator_network => 16, #TWC
        :primary_contact => 33,
        :testing_contact => 33,
        :technical_contact => 33,
        :local_contact => 33,
        :order_created_date => "25-05-2011",
        :order_received_date => "26-05-2011",
        :requested_service_date => "27-05-2011",
        :ordered_entity_group => 0,
        :bulk_orders => ["Path1"]
      }

      order[:demarcs] = {
        "a" => {
          :builder_type => "Uni",
          :cenx_demarc_type => "UNI",
          :operator_network => 17, #Charter
          :demarc_type => 54,
          :address => "123 Charter St",
          :demarc_icon => "building",
          :member_handles => [
            [11, "CC Uni-CC"],
            [10, "TWC Uni-CC"]
          ],
        },

        "z" => {
          :builder_type => "Uni",
          :service_order => demarc_z_order,
          :cenx_demarc_type => "UNI",
          :operator_network => 16,  #TWC
          :demarc_type => 48,
          :address => "123 TWC St",
          :demarc_icon => "building"
        }
      }

      #############################################
      #                                           #
      #        BEGIN Standard Path Order           #
      #                                           #
      #############################################

      path_order = Hash.new
      path_order[:builder_type] = "Evc"
      path_order[:cenx_path_type] = "Standard"
      path_order[:operator_network] = 17 #TWC
      path_order[:service_order] = {
        :bulk_order_type => "Circuit Order",
        :title => "Path Order for New Jersey Lab - 7750 Exchange",
        :primary_contact => 30,
        :testing_contact => 30,
        :technical_contact => 30,
        :local_contact => 30,
        :order_created_date => "25-05-2011",
        :order_received_date => "26-05-2011",
        :requested_service_date => "27-05-2011"
      }
      

      #Path Member Handles
      #Charter Communications SP = 11
      #Time Warner Cable SP = 10
      path_order[:member_handles] = [
        [11, "CC Path"],
        [10, "TWC Path"]
      ]

      offovc_z_order = {
        :title => "Charter Communications Order for Path on TWC Los Angeles MSO Network",
        :ordered_operator_network => 16,
        :primary_contact => 28,
        :testing_contact => 28,
        :technical_contact => 28,
        :local_contact => 28,
        :order_created_date => "25-05-2011",
        :order_received_date => "26-05-2011",
        :requested_service_date => "27-05-2011",
        :ordered_entity_group => 0,
        :bulk_orders => ["Path1"]
      }

      #Create the On Net OVC Service Order hash
      cenx_order = {
        :title => "OnNetOvc Order for Path on New Jersey Lab - 7750 Exchange",
        :primary_contact => 29,
        :testing_contact => 29,
        :technical_contact => 29,
        :local_contact => 29,
        :order_created_date => "25-05-2011",
        :order_received_date => "26-05-2011",
        :requested_service_date => "27-05-2011",
      }

      #Create the Segment sections involved
      a_segment = {
        :builder_type => "Off Net OVC",
        :operator_network => 17,
        :segment_owner_role => "Buyer",
        :ethernet_service_type => 35,
        :segment_type => 35,
        :member_handles => [
          [11, "CC Off Net OVC-CC"],
          [10, "TWC Off Net OVC-CC"]
        ],

        :segment_endpoints => {
          "A:a" => {
            :demarc_order => {:builder_type => "Uni", :name => "a"},
            :ctags => "1",
            :md_format => "802.1ag (IEEE)",
            :md_level => "4",
            :md_name_ieee => "",
            :ma_format => "icc-based",
            :ma_name => "",
            :is_monitored => 1,
            :member_handles => [
              [11, "CC CC:a"],
              [10, "TWC CC:a"]
            ],
            :cos_endpoints => {
              "SLA 1" => {
                :ingress_mapping => "5",
                :egress_marking => "-",
                :ingress_rate_limiting => "Policing",
                :ingress_cir_kbps => "10000",
                :ingress_cbs_kB => "54",
                :ingress_eir_kbps => "",
                :ingress_ebs_kB => "",
                :egress_rate_limiting => "None",
                :egress_cir_kbps => "",
                :egress_cbs_kB => "",
                :egress_eir_kbps => "",
                :egress_ebs_kB => "",
                :cos_test_vectors => [{
                    :builder_type => "Brix CosTV",
                    :test_type => "Ethernet Frame Delay Test",
                    :delay_error_threshold => "1",
                    :dv_error_threshold => "1",
                    :flr_error_threshold => "1",
                    :flr_sla_guarantee => "1",
                    :dv_sla_guarantee => "1",
                    :delay_sla_guarantee => "1",
                    :availability_guarantee => "99.99",
                    :service_level_guarantee_type => 41,
                    :circuit_id_format => "None"
                  }]
              }
            }
          },
          "A:z" => {
            :connected => ["Cenx:a"],
            :demarc_order => {:builder_type => "Enni", :id => 69},
            :member_handles => [
              [11, "CC CC:z"],
              [10, "TWC CC:z"]
            ],
            :cos_endpoints =>{
              "SLA 1" => {
                :ingress_mapping => "*",
                :egress_marking => "6",
                :ingress_rate_limiting => "Policing",
                :ingress_cir_kbps => "10000",
                :ingress_cbs_kB => "54",
                :ingress_eir_kbps => "",
                :ingress_ebs_kB => "",
                :egress_rate_limiting => "None",
                :egress_cir_kbps => "",
                :egress_cbs_kB => "",
                :egress_eir_kbps => "",
                :egress_ebs_kB => "",
                :connected => [["Cenx:a", "Real Time"]]
              }
            }
          }
        }
      }

      on_net_ovc = {
        :builder_type => "On Net OVC",
        :service_order => cenx_order,
        #:pop => 7,
        :site => 8,
        :ethernet_service_type => 36,
        :segment_type => 36,
        :member_handles => [
          [11, "CC On Net OVC"],
          [10, "TWC On Net OVC"]
        ],

        :segment_endpoints => {
          "Cenx:a" => {
            :connected => ["A:z"],
            :demarc_order => {:builder_type => "Enni", :id => 69},
            :stags => "2",
            :member_handles => [
              [11, "CC Cenx:a"],
              [10, "TWC Cenx:a"]
            ],
            :cos_endpoints =>{
              "Real Time" => {
                :ingress_mapping => "6",
                :egress_marking => "6",
                :ingress_rate_limiting => "Policing",
                :ingress_cir_kbps => "10000",
                :ingress_cbs_kB => "54",
                :ingress_eir_kbps => "",
                :ingress_ebs_kB => "",
                :egress_rate_limiting => "None",
                :egress_cir_kbps => "",
                :egress_cbs_kB => "",
                :egress_eir_kbps => "",
                :egress_ebs_kB => "",
                :connected => [["A:z", "SLA 1"]]
              }
            }
          },
          "Cenx:Mon" => {
            :demarc_order => {:builder_type => "Enni", :id => 68},
            :stags => "3",
            :member_handles => [
              [11, "CC Cenx:Mon"],
              [10, "TWC Cenx:Mon"]
            ],
          },
          "Cenx:z" => {
            :connected => ["Z:a"],
            :demarc_order => {:builder_type => "Enni", :id => 70},
            :stags => "4",
            :member_handles => [
              [11, "CC Cenx:z"],
              [10, "TWC Cenx:z"]
            ],
            :cos_endpoints =>{
              "Real Time" => {
                :ingress_mapping => "0",
                :egress_marking => "0",
                :ingress_rate_limiting => "Policing",
                :ingress_cir_kbps => "10000",
                :ingress_cbs_kB => "54",
                :ingress_eir_kbps => "",
                :ingress_ebs_kB => "",
                :egress_rate_limiting => "None",
                :egress_cir_kbps => "",
                :egress_cbs_kB => "",
                :egress_eir_kbps => "",
                :egress_ebs_kB => "",
                :connected => [["Z:a", "Expedite / Forwarding"]]
              }
            }
          }
        }
      }

      z_segment = {
        :builder_type => "Off Net OVC",
        :operator_network => 16,
        :service_order => offovc_z_order,
        :segment_owner_role => "Seller",
        :ethernet_service_type => 30,
        :segment_type => 30,
        :member_handles => [
          [11, "CC Off Net OVC-TWC"],
          [10, "TWC Off Net OVC-TWC"]
        ],

        :segment_endpoints => {
          "Z:a" => {
            :connected => ["Cenx:z"],
            :demarc_order => {:builder_type => "Enni", :id => 70},
            :member_handles => [
              [11, "CC TWC:a"],
              [10, "TWC TWC:a"]
            ],
            :cos_endpoints =>{
              "Expedite / Forwarding" => {
                :ingress_mapping => "*",
                :egress_marking => "0",
                :ingress_rate_limiting => "Policing",
                :ingress_cir_kbps => "10000",
                :ingress_cbs_kB => "54",
                :ingress_eir_kbps => "",
                :ingress_ebs_kB => "",
                :egress_rate_limiting => "Shaping",
                :egress_cir_kbps => "10000",
                :egress_cbs_kB => "54",
                :egress_eir_kbps => "",
                :egress_ebs_kB => "",
                :connected => [["Cenx:z", "Real Time"]]
              }
            }
          },
          "Z:z" => {
            :demarc_order => {
              :builder_type => "Uni",
              :name => "z",
              :member_handles => [
                [11, "CC Uni-TWC"],
                [10, "TWC Uni-TWC"]
              ],
            },
            :ctags => "5",
            :md_format => "802.1ag (IEEE)",
            :md_level => "4",
            :md_name_ieee => "",
            :ma_format => "icc-based",
            :ma_name => "",
            :is_monitored => 1,
            :member_handles => [
              [11, "CC TWC:z"],
              [10, "TWC TWC:z"]
            ],
            :cos_endpoints => {
              "Expedite / Forwarding" => {
                :ingress_mapping => "5",
                :egress_marking => "-",
                :ingress_rate_limiting => "Shaping",
                :ingress_cir_kbps => "10000",
                :ingress_cbs_kB => "54",
                :ingress_eir_kbps => "",
                :ingress_ebs_kB => "",
                :egress_rate_limiting => "None",
                :egress_cir_kbps => "",
                :egress_cbs_kB => "",
                :egress_eir_kbps => "",
                :egress_ebs_kB => "",
                :cos_test_vectors => [{
                    :builder_type => "Brix CosTV",
                    :test_type => "Ethernet Frame Delay Test",
                    :delay_error_threshold => "1",
                    :dv_error_threshold => "1",
                    :flr_error_threshold => "1",
                    :flr_sla_guarantee => "1",
                    :dv_sla_guarantee => "1",
                    :delay_sla_guarantee => "1",
                    :availability_guarantee => "99.99",
                    :service_level_guarantee_type => 41,
                    :circuit_id_format => "None"
                  }]
              }
            }
          }
        }
      }

      path_order[:segments] = {
        "A" => a_segment,
        "Cenx" => on_net_ovc,
        "Z" => z_segment
      }
      #############################################
      #                                           #
      #        END Standard Path Order             #
      #                                           #
      #############################################

      order[:paths] = {"Path1" => path_order}

      return order
    end

    def self.tunnel_order
      order = {}

      order[:demarcs] = {
      }

      #############################################
      #                                           #
      #        BEGIN Tunnel Path Order             #
      #                                           #
      #############################################

      path_order = Hash.new
      path_order[:builder_type] = "Evc"
      path_order[:cenx_path_type] = "Tunnel"
      path_order[:operator_network] = 5

      #Create the Segment sections involved
      segment = {
        :builder_type => "Off Net OVC",
        :operator_network => 5,
        :segment_owner_role => "Seller",
        :ethernet_service_type => 15,
        :segment_type => 15,

        :segment_endpoints => {
          "a" => {
            :demarc_order => {
              :builder_type => "Enni",
              :id => 112
            },
            :cos_endpoints => {
              "Wavelength" => {
                :ingress_mapping => "*",
                :egress_marking => "-",
                :ingress_rate_limiting => "None",
                :egress_rate_limiting => "None"
              }
            }
          },

          "z" => {
            :demarc_order => {
              :builder_type => "Enni",
              :id => 113
            },
            :md_format => "802.1ag (IEEE)",
            :md_level => "4",
            :md_name_ieee => "",
            :ma_format => "icc-based",
            :ma_name => "4100487309468",
            :is_monitored => 1,
            :cos_endpoints => {
              "Wavelength" => {
                :ingress_mapping => "*",
                :egress_marking => "-",
                :ingress_rate_limiting => "None",
                :egress_rate_limiting => "None",
                :cos_test_vectors => [{
                    :builder_type => "Brix CosTV",
                    :test_type => "Ethernet Frame Delay Test",
                    :delay_error_threshold => "10",
                    :dv_error_threshold => "100",
                    :flr_error_threshold => "1",
                    :availability_guarantee => "99.999",
                    :service_level_guarantee_type => 32,
                    :circuit_id_format => "None"
                  }]
              }
            }
          }
        }
      }


      path_order[:segments] = {
        "segment" => segment
      }
      #############################################
      #                                           #
      #        END Tunnel Path Order               #
      #                                           #
      #############################################

      order[:paths] = {"Path1" => path_order}

      return order
    end

    def self.core_transport_order(tunnel_path)
      tunnel_path = Path.find(tunnel_path) if tunnel_path.is_a? Integer

      order = {}

      order[:demarcs] = {
      }

      # On Net OVC A to Off Net OVC Demarc ID
      demarc_a = 112
      # Off Net OVC to On Net OVC Z Demarc ID
      demarc_z = 113

      #############################################
      #                                           #
      #    BEGIN Core Transport Path Order         #
      #                                           #
      #############################################

      path_order = Hash.new
      path_order[:builder_type] = "Evc"
      path_order[:cenx_path_type] = "Light Squared Core Transport"
      path_order[:operator_network] = 5

      onovc_a = {
        :builder_type => "On Net OVC",
        #:pop => 5,
        :site => 5,
        :ethernet_service_type => 25,
        :segment_type => 25,

        :segment_endpoints => {
          "Cenx:A:a" => {
            :demarc_order => {:builder_type => "Enni", :id => 111},
            :stags => "10",
            :cos_endpoints =>{
              "Real Time" => {
                :ingress_mapping => "*",
                :egress_marking => "5",
                :ingress_rate_limiting => "None",
                :egress_rate_limiting => "None"
              }
            }
          },
          "Cenx:Z:Mon" => {
            :demarc_order => {:builder_type => "Enni", :id => 53},
            :stags => "19",
          },
          "Cenx:A:z" => {
            :connected => ["M:a"],
            :demarc_order => {:builder_type => "Enni", :id => demarc_a},
            :stags => "*",
            :cos_endpoints =>{
              "Real Time" => {
                :ingress_mapping => "*",
                :egress_marking => "5",
                :ingress_rate_limiting => "None",
                :egress_rate_limiting => "None",
                :connected => [["M:a", "Wavelength"]]
              }
            }
          }
        }
      }

      tunnel_segment = tunnel_path.segments.first
      segmentep_a = tunnel_segment.segment_end_points.select {|segmentep| segmentep.enni.id == demarc_a}.first
      segmentep_z = tunnel_segment.segment_end_points.select {|segmentep| segmentep.enni.id == demarc_z}.first
      cose_a = segmentep_a.cos_end_points.first
      cose_z = segmentep_z.cos_end_points.first

      #Create the Segment sections involved
      offovc = {
        :id => tunnel_segment.id,

        :segment_endpoints => {
          "M:a" => {
            :id => segmentep_a.id,
            :connected => ["Cenx:A:z"],

            :cos_endpoints => {
              "Wavelength" => {
                :id => cose_a.id,
                :connected => [["Cenx:A:z", "Real Time"]]
              }
            }
          },
          "M:z" => {
            :id => segmentep_z.id,
            :connected => ["Cenx:Z:a"],

            :cos_endpoints => {
              "Wavelength" => {
                :id => cose_a.id,
                :connected => [["Cenx:Z:a", "Real Time"]]
              }
            }
          }
        }
      }

      onovc_z = {
        :builder_type => "On Net OVC",
        #:pop => 9,
        :site => 9,
        :ethernet_service_type => 25,
        :segment_type => 25,

        :segment_endpoints => {
          "Cenx:Z:a" => {
            :connected => ["M:z"],
            :demarc_order => {:builder_type => "Enni", :id => demarc_z},
            :stags => "*",
            :cos_endpoints =>{
              "Real Time" => {
                :ingress_mapping => "*",
                :egress_marking => "5",
                :ingress_rate_limiting => "None",
                :egress_rate_limiting => "None",
                :connected => [["M:z", "Wavelength"]]
              }
            }
          },

          "Cenx:Z:z" => {
            :demarc_order => {:builder_type => "Enni", :id => 114},
            :stags => "13",
            :cos_endpoints =>{
              "Real Time" => {
                :ingress_mapping => "*",
                :egress_marking => "5",
                :ingress_rate_limiting => "None",
                :egress_rate_limiting => "None"
              }
            }
          }
        }
      }

      path_order[:segments] = {
        "onovc_a" => onovc_a,
        "offovc" => offovc,
        "onovc_z" => onovc_z

      }
      #############################################
      #                                           #
      #     END Core Transport Path Order          #
      #                                           #
      #############################################

      order[:paths] = {"Path1" => path_order}

      return order
    end

  end

end
