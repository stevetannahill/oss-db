module ModelMaker
  class OrderExamples

	def self.layer3_order
      order = {}

      path_order = Hash.new
      path_order[:builder_type] = "Evc"
      path_order[:cenx_path_type] = "Generic IP Flow"
      path_order[:operator_network] = 8

      a_segment = {
        :builder_type => "On Net Router",
        :segment_owner_role => "Buyer",
        :ethernet_service_type => 14,
        :segment_type => 14,
        :site => 5,
        :as_id => "3651",
        :router_type => "VRF",
        :router_id => "10.99.100.1",
        :routing_domain => "cdn-infra",

        :segment_endpoints => {
          "A:z" => {
            :connected => ["OffNetOvc:a"],
            :demarc_order => {:builder_type => "Enni", :id => 94},
            :stags => "234",
            :is_monitored => 0,
            :eth_cfm_configured => 1,
            :ip_address => "192.168.1.22/30",
            :routing_cost => 100,

            :cos_endpoints => {
              "Hi Priority" => {
                :ingress_mapping => "*",
                :egress_marking => "5",
                :ingress_rate_limiting => "None",
                :egress_rate_limiting => "Shaping",
                :egress_cir_kbps => "1000000",
                :egress_cbs_kB => "32",
                :egress_eir_kbps => "",
                :egress_ebs_kB => "",
                :connected => [["OffNetOvc:a", "Real Time"]]
              }
            }
          }
        }
      }

      off_net_ovc = {
        :builder_type => "Off Net OVC",
        :operator_network => 8,
        :segment_owner_role => "Buyer",
        :ethernet_service_type => 16,
        :segment_type => 16,

        :segment_endpoints => {
          "OffNetOvc:a" => {
            :connected => ["A:z"],
            :demarc_order => {:builder_type => "Enni", :id => 94},
            :eth_cfm_configured => 1,

            :cos_endpoints =>{
              "Real Time" => {
                :ingress_mapping => "*",
                :egress_marking => "5",
                :ingress_rate_limiting => "None",
                :egress_rate_limiting => "None",
                :connected => [["A:z", "Hi Priority"]]
              }
            }
          },
          "OffNetOvc:z" => {
            :demarc_order => {:builder_type => "Enni", :id => 105},
            :eth_cfm_configured => 1,
            :is_monitored => 1,
            :cos_endpoints =>{
              "Real Time" => {
                :ingress_mapping => "*",
                :egress_marking => "5",
                :ingress_rate_limiting => "None",
                :egress_rate_limiting => "None",
                :connected => [["Z:a", "Real Time"]]
              }
            }
          }
        }
      }

      z_segment = {
        :builder_type => "On Net Router",
        :segment_owner_role => "Buyer",
        :ethernet_service_type => 7,
        :segment_type => 7,
        :site => 2,
        :as_id => "654xx",
        :router_type => "VRF",
        :router_id => "10.27.105.1",
        :routing_domain => "cdn",

        :segment_endpoints => {
          "Z:a" => {
            :connected => ["OffNetOvc:z"],
            :demarc_order => {:builder_type => "Enni", :id => 105},
            :eth_cfm_configured => 1,
            :is_monitored => 1,
            :stags => "234",
            :is_monitored => 0,
            :eth_cfm_configured => 1,
            :ip_address => "192.18.1.23/30",
            :routing_cost => 100,

            :cos_endpoints => {
              "Real Time" => {
                :ingress_mapping => "*",
                :egress_marking => "5",
                :ingress_rate_limiting => "None",
                :egress_rate_limiting => "Shaping",
                :egress_cir_kbps => "1000000",
                :egress_cbs_kB => "32",
                :egress_eir_kbps => "",
                :egress_ebs_kB => "",
                :connected => [["OffNetOvc:z", "Real Time"]]
              }
            }
          }
        }
      }

      path_order[:segments] = {
        "A" => a_segment,
        "OffNetOvc" => off_net_ovc,
        "Z" => z_segment
      }
      #############################################
      #                                           #
      #        END Standard Path Order             #
      #                                           #
      #############################################

      order[:paths] = {"Path1" => path_order}

      return order
    end
  end
end
