require "spec_helper"
require 'model_maker/requires'

module ModelMaker

  describe Factory do

    before(:all) do
      load_database("CDB_EXPORT_For_Test_Suite").should == true unless ENV['SKIP_DB']
      class FactoryModel < Hash
        def self.column_names
          []
        end
      end
      class ChildFactory < Factory
        model FactoryModel
        attributes [:attr1, :attr2]
        relations [:rel1, :rel2]
      end
      class GrandChildFactory < ChildFactory
        model FactoryModel
        attributes [:attr3]
        relations [:rel3]
      end

      class NephewFactory < ChildFactory
        model FactoryModel
        attributes [:attr3]
        relations [:rel3]
      end
      
      class GrandNephewFactory < NephewFactory
        model FactoryModel
      end
    end
    
    describe "#save" do

      it "should allow a validate data" do
        o = OpenStruct.new
        o.stub(:errors).and_return([])
        
        o.stub(:save).and_return(true)
        f = ChildFactory.new
        f.instance_variable_set(:@object,o)
        
        f.save(false)
      end

    end

    it "class should store model" do
      class Factory
        model :model
      end
      Factory.model.should == :model
    end

    it "should have child data" do
      ChildFactory.attributes.should =~ [:attr1, :attr2] | [:id, :created_at, :updated_at, :cenx_id, :cenx_name_prefix, :type]
      ChildFactory.relations.should =~ [:rel1, :rel2]
    end

    it "should support inheritance" do
      GrandChildFactory.attributes.should =~ [:attr1, :attr2, :attr3] | [:id, :created_at, :updated_at, :cenx_id, :cenx_name_prefix, :type]
      GrandChildFactory.relations.should =~ [:rel1, :rel2, :rel3]
    end

    it "should generate params" do
      @factory = ChildFactory.new({:attr1 => 1, :attr2 => 2, :rel1 => 1, :rel2 => 2})
      @factory.params.should == {
        :attr1 => 1, :attr2 => 2,
        :rel1_id => 1, :rel2_id => 2,
        }
    end

    it "should prevent erroneous params" do
      lambda {ChildFactory.new({:attr1 => 1, :attr2 => 2, :attr3 => 3, :rel1 => 1, :rel2 => 2})}.should raise_error(BuilderError)
    end
    
    describe "#make" do
      
      it "should default hook" do
        ChildFactory.any_instance.should_receive(:create).with(true)
        ChildFactory.any_instance.should_receive(:save).with(true)
        ChildFactory.make({})
      end
      
    end

  end
end

