require "spec_helper"
require 'model_maker/requires'
require File.expand_path(File.dirname(__FILE__) + '/../order_examples')

module ModelMaker

  describe EvcBuilder do

    before(:all) do
      load_database("CDB_EXPORT_For_Test_Suite").should == true unless ENV['SKIP_DB']
    end

    before(:each) do
      name = "a"
      order = { :operator_network => 99, :segments => [ ] }
      demarcs = []
      bulk_order_manager = nil
      @builder = EvcBuilder.new(name,order,demarcs,bulk_order_manager)
    end

  end
end

