require "spec_helper"
require 'model_maker/requires'
require File.expand_path(File.dirname(__FILE__) + '/../order_examples')

module ModelMaker
  describe "CircuitBuilder::Circuits" do

    before(:all) do
      load_database("CDB_EXPORT_For_Test_Suite").should == true unless ENV['SKIP_DB']
    end

    it 'should build a standard Path order error free' do
      @circuit_builder = CircuitBuilder.new(OrderExamples.standard_order)
      @circuit = @circuit_builder.build
    end

    it 'should build a tunnel Path order error free' do
      @circuit_builder = CircuitBuilder.new(OrderExamples.tunnel_order)
      @circuit = @circuit_builder.build
    end

    it 'should build a core transport Path order error free' do
      @circuit_builder = CircuitBuilder.new(OrderExamples.tunnel_order)
      @circuit = @circuit_builder.build
      tunnel_path = @circuit.paths.first
      @circuit_builder = CircuitBuilder.new(OrderExamples.core_transport_order(tunnel_path))
      @circuit = @circuit_builder.build
    end

  end
end

