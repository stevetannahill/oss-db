require "spec_helper"
require 'model_maker/requires'
require File.expand_path(File.dirname(__FILE__) + '/../site_order_examples')

module ModelMaker
  describe "ModelMaker::Sites" do

    before(:all) do
      load_database("CDB_EXPORT_For_Test_Suite").should == true unless ENV['SKIP_DB']
    end

    it 'should build a Standard 7750 Site order error free' do
      @circuit_builder = CircuitBuilder.new(OrderExamples.site_7750_order)
      @circuit = @circuit_builder.build
    end

    it 'should build a Sprint ALU Site order error free' do
      @circuit_builder = CircuitBuilder.new(OrderExamples.site_alu_order)
      @circuit = @circuit_builder.build
    end

    it 'should build a Sprint Ericsson Site order error free' do
      @circuit_builder = CircuitBuilder.new(OrderExamples.site_ericsson_order)
      @circuit = @circuit_builder.build
    end

  end
end

