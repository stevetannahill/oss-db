require "spec_helper"
require 'model_maker/requires'

describe "SegmentEndPointBuilderSpec" do

  before(:all) do
    load_database("CDB_EXPORT_For_Test_Suite").should == true unless ENV['SKIP_DB']
  end

  before(:each) do
    name = "a"
    segment_type = "Off Net OVC"
    order = { :demarc_order => { :builder_type => "Uni" } }
    segment_factory = nil
    connection_manager = nil
    handle_manager = nil
    @builder = ModelMaker::SegmentEndPointBuilder.new(name, segment_type, order, segment_factory, connection_manager, handle_manager)
  end

  describe "#build_segmentep" do
    
    it "should pass validate to the entities" do
      p = {}
      segment_factory = OpenStruct.new
      global_f = OpenStruct.new
      f = OpenStruct.new

      @builder.instance_variable_set(:@segment_factory,segment_factory)
      @builder.instance_variable_set(:@segment_factory,f)
      @builder.instance_variable_set(:@params,p)
      
      @builder.stub(:factory).and_return(global_f)
      global_f.stub(:make).with(p).and_return(f)
      f.should_receive(:save).with(true)

      @builder.build_segmentep
    end
    
  end

end