require "spec_helper"
require 'model_maker/requires'

describe "ConnectionManager" do

  before(:all) do
    load_database("CDB_EXPORT_For_Test_Suite").should == true unless ENV['SKIP_DB']
  end

  before(:each) do
    @path_order = { :segments => [] }
  end

  describe "#build_demarc_connections" do
    
    it "should pass validate to the entities" do
      segmentep = OpenStruct.new
      demarc = OpenStruct.new
      segmentep.stub(:save).and_return(true)
      
      m = ModelMaker::ConnectionManager.new(@path_order, :connections => [ [ "name", { :object => segmentep, :demarc => demarc } ]  ])
      m.send("build_demarc_connections")
    end
    
  end

end
