require "spec_helper"
require 'model_maker/requires'

module ModelMaker
  
  describe PortFactory do

    it 'should create a sane Port' do
      factory = PortFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(Port) }
    end
  end
end
