require "spec_helper"
require 'model_maker/requires'

module ModelMaker
  
  describe PathFactory do

    it 'should create a sane Path' do
      factory = PathFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(Path) }
    end
  end

  describe EvcFactory do

    it 'should create a sane Evc' do
      factory = EvcFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(Evc) }
    end
  end
end
