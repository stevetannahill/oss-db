require "spec_helper"
require 'model_maker/requires'

module ModelMaker
  
  describe OperatorNetworkFactory do

    it 'should create a sane Operator Network' do
      factory = OperatorNetworkFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(OperatorNetwork) }
    end
  end
end
