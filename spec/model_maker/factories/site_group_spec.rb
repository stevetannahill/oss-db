require "spec_helper"
require 'model_maker/requires'

module ModelMaker
  
  describe SiteGroupFactory do

    it 'should create a sane SiteGroup' do
      factory = SiteGroupFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(SiteGroup) }
    end
  end
end
