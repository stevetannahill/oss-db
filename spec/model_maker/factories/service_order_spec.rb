require "spec_helper"
require 'model_maker/requires'

module ModelMaker

  describe ServiceOrderFactory do
    
    it 'should create a sane ServiceOrder' do
      factory = ServiceOrderFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(ServiceOrder) }
    end
  end

  describe OnNetOvcOrderFactory do

    it 'should create a sane OnNetOvcOrder' do
      factory = OnNetOvcOrderFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(OnNetOvcOrder) }
    end
  end

  describe OffNetOvcOrderFactory do

    it 'should create a sane OffNetOvcOrderNew' do
      factory = OffNetOvcOrderFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(OffNetOvcOrder) }
    end
  end

  describe BulkOrderFactory do

    it 'should create a sane OffNetOvcOrderNew' do
      factory = BulkOrderFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(BulkOrder) }
    end
  end

  describe DemarcOrderFactory do

    it 'should create a sane DemarcOrder' do
      factory = DemarcOrderFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(DemarcOrder) }
    end
  end

  describe EnniOrderFactory do

    it 'should create a sane DemarcOrder' do
      factory = EnniOrderFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(EnniOrderNew) }
    end
  end
end