require "spec_helper"
require 'model_maker/requires'

module ModelMaker

  describe CosInstanceFactory do

    it 'should create a sane Cos Instance' do
      factory = CosInstanceFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(CosInstance) }
    end
  end

  describe CenxCosInstanceFactory do

    it 'should create a sane Cenx Cos Instance' do
      factory = CenxCosInstanceFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(CenxCosInstance) }
    end
  end

  describe CosEndPointFactory do

    it 'should create a sane Cos Endpoint' do
      factory = CosEndPointFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(CosEndPoint) }
    end
  end

  describe CenxCosEndPointFactory do

    it 'should create a sane Cenx Cos Endpoint' do
      factory = CenxCosEndPointFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(CenxCosEndPoint) }
    end
  end

  describe MonitorCosEndPointFactory do

    it 'should create a sane Cenx Cos Endpoint' do
      factory = MonitorCosEndPointFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(CenxCosEndPoint) }
    end
  end

  describe CosTestVectorFactory do

    it 'should create a sane Cos Test Vector' do
      factory = CosTestVectorFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(CosTestVector) }
    end
  end

  describe BxCosTestVectorFactory do

    it 'should create a sane Brix Cos Test Vector' do
      factory = BxCosTestVectorFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(BxCosTestVector) }
    end
  end
  
  describe SpirentCosTestVectorFactory do

    it 'should create a sane Spirent Cos Test Vector' do
      factory = SpirentCosTestVectorFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(SpirentCosTestVector) }
    end
  end
end