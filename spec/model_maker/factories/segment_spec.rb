require "spec_helper"
require 'model_maker/requires'

module ModelMaker

  describe SegmentFactory do

    it 'should create a sane Segment' do
      factory = SegmentFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(Segment) }
    end
  end

  describe OnNetSegmentFactory do

    it 'should create a sane OnNetSegment' do
      factory = OnNetSegmentFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(OnNetSegment) }
    end
  end
  
  describe OnNetRouterFactory do

    it 'should create a sane OnNetRouter' do
      factory = OnNetRouterFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(OnNetRouter) }
    end
  end

  describe OvcFactory do

    it 'should create a sane Ovc' do
      factory = OvcFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(Ovc) }
    end
  end

  describe OnNetOvcFactory do

    it 'should create a sane OnNetOvc' do
      factory = OnNetOvcFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(OnNetOvc) }
    end
  end

  describe OffNetOvcFactory do

    it 'should create a sane OffNetOVC' do
      factory = OffNetOvcFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(Segment) }
    end
  end
end