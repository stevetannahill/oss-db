require "spec_helper"
require 'model_maker/requires'

module ModelMaker

  describe SegmentEndPointFactory do

    it 'should create a sane Segment Endpoint' do
      factory = SegmentEndPointFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(SegmentEndPoint) }
    end
  end

  describe OvcEndPointEnniFactory do

    it 'should create a sane Ovc Endpoint Enni' do
      factory = OvcEndPointEnniFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(OvcEndPointEnni) }
    end
  end

  describe OnNetOvcEndPointEnniFactory do

    it 'should create a sane On Net OVC Endpoint Enni' do
      factory = OnNetOvcEndPointEnniFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(OnNetOvcEndPointEnni) }
    end
  end

  describe RouterSubIfFactory do

    it 'should create a sane Router Sub If' do
      factory = RouterSubIfFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(RouterSubIf) }
    end
  end

  describe OnNetRouterSubIfFactory do

    it 'should create a sane On Net Router Sub If' do
      factory = OnNetRouterSubIfFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(OnNetRouterSubIf) }
    end
  end
  
  describe OvcEndPointUniFactory do

    it 'should create a sane Ovc Endpoint Uni' do
      factory = OvcEndPointUniFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(OvcEndPointUni) }
    end
  end
end