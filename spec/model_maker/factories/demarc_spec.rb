require "spec_helper"
require 'model_maker/requires'

module ModelMaker

  describe DemarcFactory do

    it 'should create a sane Demarc' do
      factory = DemarcFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(Demarc) }
    end
  end
  
  describe UniFactory do

    it 'should create a sane Uni' do
      factory = UniFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(Uni) }
    end
  end

  describe EnniFactory do

    it 'should create a sane EnniNew' do
      factory = EnniFactory.new
      factory.create(false)
      factory.operate { |object| object.should be_a(EnniNew) }
    end
  end
end