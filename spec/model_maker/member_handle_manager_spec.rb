require "spec_helper"
require 'model_maker/requires'

describe "MemberHandleManager" do

  before(:all) do
    load_database("CDB_EXPORT_For_Test_Suite").should == true unless ENV['SKIP_DB']
  end

  describe "#build_handles" do
    
    it "should pass validate to the entities" do
      e = OpenStruct.new
      o = OpenStruct.new
      mh = OpenStruct.new
      
      e.stub(:member_handle_instance_by_owner).with(o).and_return(mh)
      ServiceProvider.stub(:find).with(123).and_return(o)
      e.should_receive(:save).twice
      e.should_receive(:reload)
      mh.should_receive("value=").with("abc")
      mh.stub(:save).and_return(true)
      
      m = ModelMaker::MemberHandleManager.new(:member_handles => { e => [ [123,"abc"] ] })
      m.build_handles
    end
    
  end

end