module Spec
  module Runner
    class GroupRunner
      def initialize(options, arg)
        @options = options
        @arg = arg
      end

      def load_files(files)
        $KCODE = 'u' if RUBY_VERSION.to_f < 1.9
        # It's important that loading files (or choosing not to) stays the
        # responsibility of the ExampleGroupRunner. Some implementations (like)
        # the one using DRb may choose *not* to load files, but instead tell
        # someone else to do it over the wire.
        files.each do |file|
          load file
        end
      end

      def run
        prepare
        success = true
        example_groups.each do |example_group|
          if (@arg.select{|a| example_group.description().match(/.* -.*#{a}.*\z/)}.length > 0)
            if ((example_group.methods.include? "description") && example_group.methods.include?("set_description"))
              description = example_group.nested_descriptions
              description[description.length - 1] = description[description.length - 1][0..(description[description.length - 1].rindex(" ")-1)]
              example_group.set_description(description)
            end
            success = success & example_group.run(@options)
          end
        end
        finish
        success
      end

    protected

      def prepare
        reporter.start(number_of_examples)
        example_groups.reverse! if reverse
      end

      def finish
        reporter.end
        reporter.dump
      end

      def reporter
        @options.reporter
      end

      def reverse
        @options.reverse
      end

      def example_groups
        @options.example_groups
      end

      def number_of_examples
        @options.number_of_examples
      end
    end
  end
end
