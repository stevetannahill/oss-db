require 'spec_helper'

describe "sw_err" do

  describe SoftwareError do
    
    
    before(:each) do
      @err = SoftwareError.instance
      @err.mode = :MEMORY
    end
    
    after(:all) do
      SoftwareError.instance.mode = :TEST
      File.delete("deleteme.log") if File.exists?("deleteme.log")
    end


    describe "#initialize" do
      
      it "should default the mode to :FILE (cheating and calling 'new')" do
        my = SoftwareError.send(:new)
        my.mode.should == :FILE
      end
      
    end

    describe "#reset" do
      
      it "should default to logger" do
        @err.mode = :SCREEN
        @err.reset
        @err.mode.should == :FILE
      end
      
    end
    
    describe "SW_ERR" do
      
      it "should delete to SoftwareError" do
        SW_ERR("no error", :NEVER_IGNORE)
      end
      
    end
    
    describe "#mode" do

      [:FILE,:SCREEN,:MEMORY].each do |mode|
        it "should support :#{mode}" do
          @err.mode = mode
          @err.mode.should == mode
        end
      end

      it "should support :BLAH" do
        @err.mode = :BLAH
        @err.mode.should == :FILE
      end
      
    end
    
    describe "#log" do
      
      before(:each) do
        @err.stub!(:relevant_trace_to_s).and_return("trace")
        Time.stub!(:now).and_return(Time.parse("2011-01-02 03:04:05"))
      end
      
      it "should return the log" do
        @err.log("blah").should == "SW_ERR(2011-01-02 03:04:05): blah trace\n"
      end
      
      it "should log normally" do
        @err.log("blah",:NEVER_IGNORE).should == "SW_ERR(2011-01-02 03:04:05): blah trace\n"
        @err.log("blah").should == "SW_ERR(2011-01-02 03:04:05): blah trace\n"
      end

      it "should ignore if requested" do
        @err.log("blah",:TEMPORARILY_IGNORE).should == ""
      end

      it "should not ignore if not test" do
        @err.log("blah",:IGNORE_IF_TEST).should == "SW_ERR(2011-01-02 03:04:05): blah trace\n"
        @err.mode = :TEST
        @err.log("blah",:IGNORE_IF_TEST).should == ""
      end
      
      it "should log an invalid flag" do
        @err.log("blah",:UNKNOWN_FLAG).should == "SW_ERR(2011-01-02 03:04:05): Undefined SW_ERR Flag (UNKNOWN_FLAG) trace\nSW_ERR(2011-01-02 03:04:05): blah trace\n"
      end

      it "should log a nil flag" do
        @err.log("blah",nil).should == "SW_ERR(2011-01-02 03:04:05): Undefined SW_ERR Flag () trace\nSW_ERR(2011-01-02 03:04:05): blah trace\n"
      end
      
      describe ":TEST mode" do
        
        before(:each) do
          @err.mode = :TEST
        end
        
        it "should ignore if testing" do
          @err.log("blah",:IGNORE_IF_TEST).should == ""
        end
        
        it "shouldn't log if temporarily ignoring" do
          @err.log("blah",:TEMPORARILY_IGNORE).should == ""
        end
        
        it "should raise an excpetion normally" do
          lambda {@err.log("blah",:NEVER_IGNORE)}.should raise_error(RuntimeError)
        end
        
      end
      
      describe ":SCREEN mode" do

        before(:each) do
          @err.mode = :SCREEN
        end

        it "should output to the screen" do
          STDOUT.should_receive(:puts).with("SW_ERR(2011-01-02 03:04:05): blah trace\n")
          @err.log("blah") 
        end
        
      end
      
      describe ":FILE mode" do
        
        before(:each) do
          @err.mode = :FILE
          @err.stub!(:logger).and_return(Logger.new("deleteme.log"))
        end
        
        it "should write to a file" do
          @err.log("blah")
          IO.read("deleteme.log").strip.split("\n").last.should == "SW_ERR(2011-01-02 03:04:05): blah trace"
        end
        
      end
      
    end
    
    describe "#relevant_trace_to_s" do
      
      it "should return all" do
        @err.relevant_trace_to_s(["a","b","c"]).should == "a\n b\n c"
      end
      
      it "should stop when it hits /lib/action_controller/routing/route_set.rb" do
        @err.relevant_trace_to_s(["a","b","/lib/action_controller/routing/route_set.rb","c"]).should == "a\n b\n /lib/action_controller/routing/route_set.rb"
      end
      
    end

    describe "#logger" do
      
      it "should return okay" do
        @err.logger.should_not == nil
        @err.logger.should == @err.logger
      end
      
    end
    
    
  end
  

end
