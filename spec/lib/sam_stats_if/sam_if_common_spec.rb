require 'spec_helper'

module SamIfCommon

  describe SamIfMsg do

    before(:all) do
      load_database("EMPTY_DB").should == true unless ENV['SKIP_DB']    
    end

    describe "#initialize" do

      it "empty" do
        msg = SamIfMsg.new(nil, nil, nil, nil)
        msg.get_nms_id.should == nil
        msg.get_command.should == nil
        msg.get_filters.should == nil
        msg.get_result_filters.should == nil
      end
      
      it "set" do
        msg = SamIfMsg.new('a', 'b', 'c', 'd')
        msg.get_nms_id.should == 'a'
        msg.get_command.should == 'b'
        msg.get_filters.should == 'c'
        msg.get_result_filters.should == 'd'
      end

    end
    
  end


end