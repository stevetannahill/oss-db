require 'spec_helper'

module SamIf

  describe StatsApi do

    before(:all) do
      load_database("EMPTY_DB").should == true unless ENV['SKIP_DB']    
    end

    describe "#initialize" do
      
      it "should fail on nil" do
        SoftwareError.instance.should_receive(:log).with("NMS is nil", :NEVER_IGNORE)
        StatsApi.new(nil)
      end

    end
    
  end


end