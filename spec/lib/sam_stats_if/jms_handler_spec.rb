require 'spec_helper'

describe JmsHandler do

  before(:all) do
    load_database("EMPTY_DB").should == true unless ENV['SKIP_DB']    
  end

  before(:each) do
    @handler = JmsHandler.new
  end

  describe "#on_message" do

    it "should do nothing" do
      @handler.on_message(nil,nil).should == nil
    end
    
  end

  describe "#resync" do
    
    it "should do nothing" do
      @handler.resync(nil).should == nil
    end
    
  end

end