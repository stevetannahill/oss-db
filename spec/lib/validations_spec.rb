require 'spec_helper'

class TestModel
	include ActiveModel::Validations

	attr_accessor :cenx_id,:attributes,:entity_types,:valid_entity_types,:entity_types2,:valid_entity_types2,
								:connected_device_type,:connected_device_type2,:service_id_low,:service_id_high,
								:ordered_entity,:ordered_entity2

	validates_as_cenx_id :cenx_id
	
	validate_inclusion_of_each :entity_types, :in => :valid_entity_types
	validate_inclusion_of_each :entity_types2, :in => :valid_entity_types2, :message => "custom message"
	
	validates_as_list :connected_device_type, :in => ["A","B","C","D"]
	validates_as_list :connected_device_type2, :in => ["A","B","C","D"], :delimiter => /\s*\n\s*/, :allow_blank => true

	validates_class_of :ordered_entity, :is_a => "Hash"
	validates_class_of :ordered_entity2, :not_a => "Hash"

	def initialize(hash)
		self.entity_types = []
		self.valid_entity_types = []
		self.entity_types2 = []
		self.valid_entity_types2 = []
		hash.each do |key,val|
			send("#{key}=".to_sym,val)
		end
	end
end

# Used to easily bypass normal validation and callbacks on an Evc
class SimpleSite < ActiveRecord::Base
	self.table_name = "sites"
	
	validates_range_of :service_id_low, :service_id_high, :overlapping => false, :greater_than_or_equal_to => 0, :only_integer => true, :unique_ends => true
end

class SimpleServiceLevelGuaranteeType < ActiveRecord::Base
  self.table_name = 'service_level_guarantee_types'  

	validates_range_of :min_dist_km, :max_dist_km,  :unbounded => ["~", "*"], :overlapping => false, :scope => :class_of_service_type_id, :greater_than_or_equal_to => 0
end

class HashSubClass < Hash
end

describe "Validations" do

	before(:all) do
    load_database("EMPTY_DB").should == true unless ENV['SKIP_DB']    
	  SoftwareError.instance.mode = :MEMORY
	end

	after(:all) do
	  SoftwareError.instance.mode = :TEST
	end
	
	describe "validates_as_cenx_id" do
		it "should be valid for an Path" do
			model = Evc.new(:cenx_id => IdTools::CenxIdGenerator.generate_cenx_id(Evc))
			model.valid?
			model.errors[:cenx_id].should == []
		end
	
		it "should be invalid for an empty string" do
			model = TestModel.new(:cenx_id => "")
			model.valid?
			model.errors[:cenx_id].should == ["* missing type ID:  - 3rd and 4th numbers;\n* invalid length - not 12 numbers long;\n"]
		end
		
		it "should be invalid for a cenx_id generated using a different class then the model's" do
			model = TestModel.new(:cenx_id => IdTools::CenxIdGenerator.generate_cenx_id(Evc))
			model.valid?
			model.errors[:cenx_id].should == ["ID invalid type identifer"]
		end
		
		it "should be invalid for a duplicate id" do
			#Fast version
			#Path.stub(:find) {[Path.new(:cenx_id => "100493066429")]}
			
			#Slow but stronger test
			#SimpleEvc.create!(:cenx_id => "100493066429")
            ma = MemberHandle.create!(Factory.build(:class_member_handle, :name => "Member Handle for Cenx Test Evc", :affected_entity_type => 'Path', :syntax => "string").attributes)
            sp = ServiceProvider.create!(Factory.build(:service_provider).attributes)
            ont = OperatorNetworkType.create!(Factory.build(:operator_network_type, :service_provider_id => sp.id).attributes)
            on = OperatorNetwork.create!(Factory.build(:operator_network, :service_provider_id => sp.id, :operator_network_type_id => ont.id).attributes)
            evc = Evc.create!(Factory.build(:evc, :operator_network_id => on.id).attributes)
            evc.cenx_id = "100493066429"
            evc.save

		
			#Bit of a hack around the usual triggers
			simple_model = Evc.create!(Factory.build(:evc, :operator_network_id => on.id,).attributes)
			model = Path.find(simple_model.id)
			model.cenx_id = "100493066429"
			
			model.valid?
			model.errors[:cenx_id].should == ["ID has already been taken"]
		end
	end
	
	describe "validate_inclusion_of_each" do
		it "should be valid if it is within the list" do
			model = TestModel.new(:entity_types => ["A","C","D"], :valid_entity_types => ["A","B","C","D"])
			model.valid?
			model.errors[:entity_types].should == []
		end
		
		it "should be not be valid if it is not within the list" do
			model = TestModel.new(:entity_types => ["A","Z","D"], :valid_entity_types => ["A","B","C","D"])
			model.valid?
			model.errors[:entity_types].should == [I18n.translate('errors.messages.inclusion')]
		end
		
		it "should provide a custom message if it is not within the list" do
			model = TestModel.new(:entity_types2 => ["A","Z","D"], :valid_entity_types2 => ["A","B","C","D"])
			model.valid?
			model.errors[:entity_types2].should == ["custom message"]
		end
	end
	
	describe "validates_as_list" do
		it "should be valid for a proper list" do
			model = TestModel.new(:connected_device_type => "B;D")
			model.valid?
			model.errors[:connected_device_type].should == []
		end
		
		it "should be invalid for an empty list" do
			model = TestModel.new(:connected_device_type => "")
			model.valid?
			model.errors[:connected_device_type].should == ["can't be blank."]
		end
		
		it "should be invalid for a list with unacceptable item" do
			model = TestModel.new(:connected_device_type => "D;Z")
			model.valid?
			model.errors[:connected_device_type].should == ["must be a list of A, B, C, D."]
		end
		
		it "should be invalid for a list with a duplicate" do
			model = TestModel.new(:connected_device_type => "A;A")
			model.valid?
			model.errors[:connected_device_type].should == ["may not list the same item twice."]
		end
		
		it "should be valid for a list that is blank with the allow_blank option" do
			model = TestModel.new(:connected_device_type2 => "")
			model.valid?
			model.errors[:connected_device_type2].should == []
		end
		
		it "should be valid for a proper list with a different delimiter" do
			model = TestModel.new(:connected_device_type2 => "B\nD")
			model.valid?
			model.errors[:connected_device_type2].should == []
		end
		
		it "should be invalid for a list with unacceptable item with a different delimiter" do
			model = TestModel.new(:connected_device_type => "D\nZ")
			model.valid?
			model.errors[:connected_device_type].should == ["must be a list of A, B, C, D."]
		end
	end
	
	describe "validates_range_of" do
		it "should be valid for a simple range" do
			model = SimpleSite.new(:service_id_low => 5, :service_id_high => 10)
			model.valid?
			model.errors[:service_id_low].should == []
			model.errors[:service_id_high].should == []
		end
		
		it "should be invalid for a negative value" do
			model = SimpleSite.new(:service_id_low => -1, :service_id_high => 10)
			model.valid?
			model.errors[:service_id_low].should == ["must be greater than or equal to 0"]
			model.errors[:service_id_high].should == []
		end
		
		it "should be invalid for a non integer" do
			model = SimpleSite.new(:service_id_low => 2.4, :service_id_high => 10)
			model.valid?
			model.errors[:service_id_low].should == ["must be an integer"]
			model.errors[:service_id_high].should == []
		end
		
		it "should be invalid for an overlapping range" do
			SimpleSite.create(:service_id_low => 2, :service_id_high => 8)
			model = SimpleSite.new(:service_id_low => 5, :service_id_high => 10)
			model.valid?
			model.errors[:service_id_low].should == ["is within another range."]
			model.errors[:service_id_high].should == []
		end
		
		it "should be invalid for an overlap with unique ends" do
			SimpleSite.create(:service_id_low => 1, :service_id_high => 5)
			model = SimpleSite.new(:service_id_low => 5, :service_id_high => 10)
			model.valid?
			model.errors[:service_id_low].should == ["is within another range."]
			model.errors[:service_id_high].should == []
		end
		
		it "should be invalid for an overlapping range" do
			SimpleServiceLevelGuaranteeType.create(:min_dist_km => 8, :max_dist_km => 12)
			model = SimpleServiceLevelGuaranteeType.new(:min_dist_km => 5, :max_dist_km => 10)
			model.valid?
			model.errors[:min_dist_km].should == []
			model.errors[:max_dist_km].should == ["is within another range."]
		end
		
		it "should be invalid for an overlapping range" do
			SimpleServiceLevelGuaranteeType.create(:min_dist_km => 8, :max_dist_km => 12)
			model = SimpleServiceLevelGuaranteeType.new(:min_dist_km => 5, :max_dist_km => 10)
			model.valid?
			model.errors[:min_dist_km].should == []
			model.errors[:max_dist_km].should == ["is within another range."]
		end
		
		it "should be valid for an overlapping endpoint range" do
			SimpleServiceLevelGuaranteeType.create(:min_dist_km => 1, :max_dist_km => 5)
			model = SimpleServiceLevelGuaranteeType.new(:min_dist_km => 5, :max_dist_km => 10)
			model.valid?
			model.errors[:min_dist_km].should == []
			model.errors[:max_dist_km].should == []
		end
		
		it "should be invalid for an overlapping unbounded ranges" do
			SimpleServiceLevelGuaranteeType.create(:min_dist_km => 7, :max_dist_km => "*")
			model = SimpleServiceLevelGuaranteeType.new(:min_dist_km => 5, :max_dist_km => 10)
			model.valid?
			model.errors[:min_dist_km].should == ["encapsulates another range."]
			model.errors[:max_dist_km].should == ["is within another range.", "encapsulates another range."]
		end
		
		it "should be invalid for an overlapping within unbounded range" do
			SimpleServiceLevelGuaranteeType.create(:min_dist_km => 1, :max_dist_km => "*")
			model = SimpleServiceLevelGuaranteeType.new(:min_dist_km => 5, :max_dist_km => 10)
			model.valid?
			model.errors[:min_dist_km].should == ["is within another range."]
			model.errors[:max_dist_km].should == ["is within another range."]
		end
		
		it "should be invalid for an overlapping with and exsiting bounded range" do
			SimpleServiceLevelGuaranteeType.create(:min_dist_km => 1, :max_dist_km => 10)
			model = SimpleServiceLevelGuaranteeType.new(:min_dist_km => 5, :max_dist_km => "~")
			model.valid?
			model.errors[:min_dist_km].should == ["is within another range."]
			model.errors[:max_dist_km].should == []
		end
		
		it "should be invalid for an overlapping unbounded with and exsiting bounded range" do
			SimpleServiceLevelGuaranteeType.create(:min_dist_km => 5, :max_dist_km => 10)
			model = SimpleServiceLevelGuaranteeType.new(:min_dist_km => 1, :max_dist_km => "~")
			model.valid?
			model.errors[:min_dist_km].should == ["encapsulates another range."]
			model.errors[:max_dist_km].should == ["encapsulates another range."]
		end
	end
	
	describe "validates_class_of" do
		it "should be valid with the right class" do
			model = TestModel.new(:ordered_entity => {})
			model.valid?
			model.errors[:ordered_entity].should == []
		end
		
		it "should be invalid with the wrong class" do
			model = TestModel.new(:ordered_entity => [])
			model.valid?
			model.errors[:ordered_entity].should == ["must be a kind of Hash (is_a)"]
		end
		
		it "should be valid with the right class for not_a" do
			model = TestModel.new(:ordered_entity2 => [])
			model.valid?
			model.errors[:ordered_entity2].should == []
		end
		
		it "should be invalid with the wrong class for not_a" do
			model = TestModel.new(:ordered_entity2 => {})
			model.valid?
			model.errors[:ordered_entity2].should == ["must not be a kind of Hash"]
		end
		
		it "should be valid with the right class even if it's a subclass" do
			model = TestModel.new(:ordered_entity => HashSubClass.new)
			model.valid?
			model.errors[:ordered_entity].should == []
		end
	end
end
