require 'spec_helper'
require 'service_monitoring/helper'

describe "Light Squared Circuit Service Monitoring Tests" do

  before(:all) do
    load_database("CDB_EXPORT_For_Test_Suite").should == true unless ENV['SKIP_DB']
  end

  before(:each) do
    @path = Path.find_by_cenx_id(100487309468)
    @off_net_ovc = Segment.find_by_cenx_id(101206634959)
    set_path_to_ok(@path)
  end

  describe "Alarms" do

    it "should give correct alarms" do
      cenx_monitoring_segment = @path.segments.collect {|segment| segment if segment.segment_end_points.size == 3}.compact.first
      cenx_other = (@path.segments - [@off_net_ovc, cenx_monitoring_segment]).first

      check_path_live(@path)
      # EP fault on the cenx monitoring segment whcih is connected to the Off Net OVC  
      fault_ep = cenx_monitoring_segment.segment_end_points.detect {|ep| ep.segment_end_points.size != 0}
      all_results_ok = all_ok(@path)
      expected_results = all_results_ok.clone 
      expected_results[@path] = AlarmSeverity::UNAVAILABLE
      expected_results[cenx_monitoring_segment] = AlarmSeverity::UNAVAILABLE
      expected_results[fault_ep] = AlarmSeverity::UNAVAILABLE

      puts "EP #{fault_ep.class}:#{fault_ep.id} Critical" if $debug
      fault_ep.alarm_histories[0].add_event(Time.now.to_f*1000, "critical", "critical")
      check_results(expected_results)

      # Will get Remote MEPs on both the member endpoints - Off Net OVC & EPs => Dependency
      @off_net_ovc.segment_end_points.each do |ep|
        puts "EP #{ep.class}:#{ep.id} minor -> missing remote MEP" if $debug
        ep.alarm_histories[0].add_event(Time.now.to_f*1000, "minor", "minor")
        expected_results[ep] = AlarmSeverity::DEPENDENCY
      end
      expected_results[@off_net_ovc] = AlarmSeverity::DEPENDENCY
      check_results(expected_results)

      # brix will now fail - No extra state changes
      brix_ep = (@off_net_ovc.segment_end_points - [fault_ep.segment_end_points[0]]).first
      puts "Brix #{brix_ep.class}:#{brix_ep.id} Failed" if $debug
      brix_ep.cos_end_points[0].cos_test_vectors[0].alarm_histories[0].add_event(Time.now.to_f*1000, "failed", "failed")
      check_results(expected_results)

      # Now start to clear everything
      # Cenx EP back up
      # On Net OVC will be OK but the Path will be Dependency
      expected_results[@path] = AlarmSeverity::DEPENDENCY
      expected_results[cenx_monitoring_segment] = AlarmSeverity::OK
      expected_results[fault_ep] = AlarmSeverity::OK
      puts "EP #{fault_ep.class}:#{fault_ep.id} Cleared" if $debug
      fault_ep.alarm_histories[0].add_event(Time.now.to_f*1000, "cleared", "")
      check_results(expected_results)

      # Clear MEP alarms
      # Only the non-monitored EP will be OK
      @off_net_ovc.segment_end_points.each do |ep|
        puts "EP #{ep.class}:#{ep.id} Cleared" if $debug
        ep.alarm_histories[0].add_event(Time.now.to_f*1000, "cleared", "")
      end
      non_mon_ep = (@off_net_ovc.segment_end_points - [brix_ep])[0]
      expected_results[non_mon_ep] = AlarmSeverity::OK
      check_results(expected_results)

      # Clear Brix alarms
      # Everything should be OK
      puts "Brix #{brix_ep.class}:#{brix_ep.id} Cleared" if $debug
      brix_ep.cos_end_points[0].cos_test_vectors[0].alarm_histories[0].add_event(Time.now.to_f*1000, "cleared", "")
      check_results(all_results_ok)
    end        

  end

  describe "ENNI Alarms" do

    it "should give correct alarms" do
      cenx_monitoring_segment = @path.segments.collect {|segment| segment if segment.segment_end_points.size == 3}.compact.first
      cenx_non_mon_segment = @path.on_net_ovcs - [cenx_monitoring_segment]
      cenx_other_segment = (@path.segments - [@off_net_ovc, cenx_monitoring_segment]).first

      check_path_live(@path)
      # EP fault on the cenx monitoring segment whcih is connected to the Off Net OVC  
      fault_ep = cenx_monitoring_segment.segment_end_points.detect {|ep| ep.segment_end_points.size != 0}
      fault_enni = fault_ep.demarc

      all_results_ok = all_ok(@path)
      expected_results = all_results_ok.clone 

      # ENNI down
      # Enni =>UA, EP on ENNI => Dependency, On Net OVC => Dependency, Path => Dependency
      puts "ENNI #{fault_enni.class}:#{fault_enni.id} Critical" if $debug
      fault_enni.alarm_histories[0].add_event(Time.now.to_f*1000, "critical", "critical")  
      expected_results[fault_enni] = AlarmSeverity::UNAVAILABLE      
      expected_results[@path] = AlarmSeverity::DEPENDENCY
      expected_results[cenx_monitoring_segment] = AlarmSeverity::DEPENDENCY
      expected_results[fault_ep] = AlarmSeverity::DEPENDENCY
      check_results(expected_results)      

      # ENNI on the other Cenx/Sprint Segment will go down
      # The other On Net OVC ENNI => Unavailable, Segment => Dependency
      other_cenx_ep = cenx_other_segment.segment_end_points.detect {|ep| ep.segment_end_points.size != 0}
      other_cenx_enni = other_cenx_ep.demarc
      puts "ENNI #{other_cenx_enni.class}:#{other_cenx_enni.id} Critical" if $debug
      other_cenx_enni.alarm_histories[0].add_event(Time.now.to_f*1000, "critical", "critical")  
      expected_results[other_cenx_enni] = AlarmSeverity::UNAVAILABLE      
      expected_results[other_cenx_ep] = AlarmSeverity::DEPENDENCY
      expected_results[cenx_other_segment] = AlarmSeverity::DEPENDENCY      
      check_results(expected_results)

      # EP on Cenx monitoring Segment will go down
      # No state change
      puts "Endpoint #{fault_ep.class}:#{fault_ep.id} Critical" if $debug
      fault_ep.alarm_histories[0].add_event(Time.now.to_f*1000, "critical", "critical")
      check_results(expected_results)

      # EP on other On Net OVC will go down
      # No state change
      puts "Endpoint #{other_cenx_ep.class}:#{other_cenx_ep.id} Critical" if $debug
      other_cenx_ep.alarm_histories[0].add_event(Time.now.to_f*1000, "critical", "critical")
      check_results(expected_results)     

      # Will get Remote MEPs on both the member endpoints - Off Net OVC => Dependency, EPs => Dependency
      brix_ep = (@off_net_ovc.segment_end_points - [fault_ep.segment_end_points[0]]).first
      @off_net_ovc.segment_end_points.each do |ep|
        puts "EP #{ep.class}:#{ep.id} minor -> missing remote MEP" if $debug
        ep.alarm_histories[0].add_event(Time.now.to_f*1000, "minor", "minor")
        expected_results[ep] = AlarmSeverity::DEPENDENCY
      end
      expected_results[@off_net_ovc] = AlarmSeverity::DEPENDENCY
      check_results(expected_results)

      # brix will now fail - Off Net OVC EPs => Dependency
      puts "Brix #{brix_ep.class}:#{brix_ep.id} Failed" if $debug
      brix_ep.cos_end_points[0].cos_test_vectors[0].alarm_histories[0].add_event(Time.now.to_f*1000, "failed", "failed")
      @off_net_ovc.segment_end_points.each do |ep|
        expected_results[ep] = AlarmSeverity::DEPENDENCY
      end
      check_results(expected_results)

      ##### Now start to clear everything  #######

      # Cenx ENNI back up
      # Enni => OK, EP on ENNI => UA, On Net OVC => UA, Path => UA
      puts "ENNI #{fault_enni.class}:#{fault_enni.id} Cleared" if $debug
      fault_enni.alarm_histories[0].add_event(Time.now.to_f*1000, "cleared", "")  
      expected_results[fault_enni] = AlarmSeverity::OK      
      expected_results[@path] = AlarmSeverity::UNAVAILABLE
      expected_results[cenx_monitoring_segment] = AlarmSeverity::UNAVAILABLE
      expected_results[fault_ep] = AlarmSeverity::UNAVAILABLE
      check_results(expected_results)      

      # ENNI on the other Cenx/Sprint Segment will come back
      # Enni => OK, EP on ENNI => UA, On Net OVC => UA, Path => UA
      puts "ENNI #{other_cenx_enni.class}:#{other_cenx_enni.id} Cleared" if $debug
      other_cenx_enni.alarm_histories[0].add_event(Time.now.to_f*1000, "cleared", "")  
      expected_results[other_cenx_enni] = AlarmSeverity::OK      
      expected_results[other_cenx_ep] = AlarmSeverity::UNAVAILABLE
      expected_results[cenx_other_segment] = AlarmSeverity::UNAVAILABLE    
      check_results(expected_results)

      # EP on Cenx monitoring Segment will come back
      # EP => OK, Segment => OK
      puts "Endpoint #{fault_ep.class}:#{fault_ep.id} Cleared" if $debug
      fault_ep.alarm_histories[0].add_event(Time.now.to_f*1000, "cleared", "")
      expected_results[cenx_monitoring_segment] = AlarmSeverity::OK
      expected_results[fault_ep] = AlarmSeverity::OK
      check_results(expected_results)

      # EP on other On Net OVC will come back
      # EP => OK, Segment => OK
      puts "Endpoint #{other_cenx_ep.class}:#{other_cenx_ep.id} Cleared" if $debug
      other_cenx_ep.alarm_histories[0].add_event(Time.now.to_f*1000, "cleared", "")
      expected_results[cenx_other_segment] = AlarmSeverity::OK
      expected_results[other_cenx_ep] = AlarmSeverity::OK
      # Why did the Path go UA => Dependency now ???
      expected_results[@path] = AlarmSeverity::DEPENDENCY    
      check_results(expected_results)

      # Remote MEPs on both the member endpoints will clear
      # Off Net OVC => Dependency, EPs on non-monitored => OK, Mon EP => Dependency
      @off_net_ovc.segment_end_points.each do |ep|
        puts "EP #{ep.class}:#{ep.id} Cleared -> missing remote MEP" if $debug
        ep.alarm_histories[0].add_event(Time.now.to_f*1000, "cleared", "")
        expected_results[ep] = AlarmSeverity::OK
      end
      expected_results[@off_net_ovc] = AlarmSeverity::DEPENDENCY
      expected_results[brix_ep] = AlarmSeverity::DEPENDENCY   
      check_results(expected_results)

      # brix will now fail - Off Net OVC EPs => Dependency
      puts "Brix #{brix_ep.class}:#{brix_ep.id} Cleared" if $debug
      brix_ep.cos_end_points[0].cos_test_vectors[0].alarm_histories[0].add_event(Time.now.to_f*1000, "cleared", "") 
      check_results(all_results_ok)   
    end
        
  end

  describe "Tunnel alarms" do
    
    it "should give correct alarms" do
      puts "The Tardis is moving time to 100 seconds in the future" if $debug
      t = Time.now+100.seconds    
      Time.stub!(:now).and_return {t+=1.seconds}

      cenx_monitoring_segment = @path.segments.collect {|segment| segment if segment.segment_end_points.size == 3}.compact.first
      cenx_non_mon_segment = @path.on_net_ovcs - [cenx_monitoring_segment]
      cenx_other_segment = (@path.segments - [@off_net_ovc, cenx_monitoring_segment]).first

      check_path_live(@path)
      # EP fault on the cenx monitoring segment whcih is connected to the Off Net OVC  
      fault_ep = cenx_monitoring_segment.segment_end_points.detect {|ep| ep.segment_end_points.size != 0}

      all_results_ok = all_ok(@path)
      expected_results = all_results_ok.clone 

      # Will get Remote MEPs on the other EP => Warning, Off Net OVC => Warning, Path => Dependency
      brix_ep = (@off_net_ovc.segment_end_points - [fault_ep.segment_end_points[0]]).first
      other_ep = (@off_net_ovc.segment_end_points  - [brix_ep]).first
      puts "EP #{other_ep.class}:#{other_ep.id} Missing remote MEP -> minor" if $debug
      other_ep.alarm_histories[0].add_event(Time.now.to_f*1000, "minor", "minor")
      expected_results[other_ep] = AlarmSeverity::WARNING
      expected_results[@off_net_ovc] = AlarmSeverity::WARNING
      expected_results[@path] = AlarmSeverity::DEPENDENCY
      check_results(expected_results)


      # brix fails - Off Net OVC => Failed, brix_ep => Failed, Other ep => Warning(due to missing remote mep)?? Path => Failed
      brix_ep = (@off_net_ovc.segment_end_points - [fault_ep.segment_end_points[0]]).first
      puts "Brix #{brix_ep.class}:#{brix_ep.id} Failed" if $debug
      brix_ep.cos_end_points[0].cos_test_vectors[0].alarm_histories[0].add_event(Time.now.to_f*1000, "failed", "failed")
      expected_results[brix_ep] = AlarmSeverity::FAILED
      expected_results[@off_net_ovc] = AlarmSeverity::FAILED

      check_results(expected_results)

      ##### Now start to clear everything  #######

      # Remote MEPs will clear on the other EP => OK
      puts "EP #{other_ep.class}:#{other_ep.id} Missing remote MEP -> cleared" if $debug
      other_ep.alarm_histories[0].add_event(Time.now.to_f*1000, "cleared", "")
      expected_results[other_ep] = AlarmSeverity::OK
      check_results(expected_results)


      # brix cleared - All OK 
      puts "Brix #{brix_ep.class}:#{brix_ep.id} Cleared" if $debug
      brix_ep.cos_end_points[0].cos_test_vectors[0].alarm_histories[0].add_event(Time.now.to_f*1000, "cleared", "")    
      check_results(all_results_ok)  
    end
    
  end
  
end
