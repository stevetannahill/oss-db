require 'spec_helper'
require 'service_monitoring/helper'

describe "Standard Circuit Service Monitoring Tests" do

  before(:all) do
    load_database("CDB_EXPORT_For_Test_Suite").should == true unless ENV['SKIP_DB']
  end
  
  before(:each) do
    @path = Segment.find_by_service_id(100052).paths[0]
    set_path_to_ok(@path)
  end

  it "Standard Circuit Alarms should give correct alarms" do
    # Make Cenx EP unavailable
    # Cenx EP = UA, Segment = US, Path = UA rest is all Ok

    on_net_ovc = Segment.find_by_service_id(100052)
    off_net_ovcs = @path.segments - [on_net_ovc]
    check_path_live(@path)
    results_all_ok = all_ok(@path)
    eps = on_net_ovc.segment_end_points.reject {|ep| ep.is_connected_to_test_port}

    # Make Cenx non MChassis EP unavailable
    # Cenx EP = UA, Segment = US, Path = UA rest is all Ok
    ep_to_fault = eps.find {|ep| !ep.demarc.is_multi_chassis}
    puts "#{ep_to_fault.class}:#{ep_to_fault.id} Critical" if $debug
    ep_to_fault.alarm_histories[0].add_event(Time.now.to_f*1000, "critical", "critical")

    expected_results = results_all_ok.clone
    expected_results[@path] = AlarmSeverity::UNAVAILABLE
    expected_results[on_net_ovc] = AlarmSeverity::UNAVAILABLE
    expected_results[ep_to_fault] = AlarmSeverity::UNAVAILABLE
    check_results(expected_results)

    # Now make a Brix alarm 
    # Off Net OVC & endpoints should be Dependency
    fault_off_net_ovc = ep_to_fault.segment_end_points[0].segment
    uni_ep = fault_off_net_ovc.segment_end_points.find_by_type("OvcEndPointUni")
    puts "#{uni_ep.cos_end_points[0].class}:#{uni_ep.cos_end_points[0].id} Brix Failed" if $debug
    uni_ep.cos_end_points[0].cos_test_vectors[0].alarm_histories[0].add_event(Time.now.to_f*1000, "failed", "failed")

    expected_results[fault_off_net_ovc] = AlarmSeverity::DEPENDENCY
    expected_results[uni_ep] = AlarmSeverity::DEPENDENCY
    check_results(expected_results)

    # Now clear the Cenx alarm
    puts "#{ep_to_fault.class}:#{ep_to_fault.id} Cleared" if $debug
    ep_to_fault.alarm_histories[0].add_event(Time.now.to_f*1000, "cleared", "")

    expected_results[@path] = AlarmSeverity::DEPENDENCY
    expected_results[on_net_ovc] = AlarmSeverity::OK
    expected_results[ep_to_fault] = AlarmSeverity::OK
    check_results(expected_results)

    # Now clear Brix alarm
    puts "#{uni_ep.cos_end_points[0].class}:#{uni_ep.cos_end_points[0].id} Brix Cleared" if $debug
    uni_ep.cos_end_points[0].cos_test_vectors[0].alarm_histories[0].add_event(Time.now.to_f*1000, "cleared", "")

    expected_results[@path] = AlarmSeverity::OK
    expected_results[fault_off_net_ovc] = AlarmSeverity::OK
    expected_results[uni_ep] = AlarmSeverity::OK
    check_results(expected_results)

    # Now raise brix alarm - it should be Dependency as it's less that 1.5 minutes since the brix alarm was cleared
    puts "#{uni_ep.cos_end_points[0].class}:#{uni_ep.cos_end_points[0].id} Brix Failed" if $debug
    uni_ep.cos_end_points[0].cos_test_vectors[0].alarm_histories[0].add_event(Time.now.to_f*1000, "failed", "failed")

    expected_results[@path] = AlarmSeverity::DEPENDENCY
    expected_results[fault_off_net_ovc] = AlarmSeverity::DEPENDENCY
    expected_results[uni_ep] = AlarmSeverity::DEPENDENCY
    check_results(expected_results)

    # Now clear Brix alarm
    puts "#{uni_ep.cos_end_points[0].class}:#{uni_ep.cos_end_points[0].id} Brix cleared" if $debug
    uni_ep.cos_end_points[0].cos_test_vectors[0].alarm_histories[0].add_event(Time.now.to_f*1000, "cleared", "")

    expected_results[@path] = AlarmSeverity::OK
    expected_results[fault_off_net_ovc] = AlarmSeverity::OK
    expected_results[uni_ep] = AlarmSeverity::OK
    check_results(expected_results)
    check_results(results_all_ok)
  end  
  
  it "Standard Circuit Maintenance should give correct alarms" do
    # Make On Net OVC Mtc
    # Cenx EP = Mtc, Segment = Mtc, Path = Mtc rest is all Ok
    on_net_ovc = Segment.find_by_service_id(100052)
    off_net_ovcs = @path.segments - [on_net_ovc]
    check_path_live(@path)

    results_all_ok = all_ok(@path)

    eps = on_net_ovc.segment_end_points.reject {|ep| ep.is_connected_to_test_port}
    ep_to_fault = eps.find {|ep| !ep.demarc.is_multi_chassis}

    # Make On Net OVC Mtc
    # Cenx EP = Mtc, Segment = Mtc, Path = Mtc rest is all Ok
    puts "On Net OVC Mtc" if $debug
    on_net_ovc.set_prov_state(ProvMaintenance)  
    expected_results = results_all_ok.clone

    expected_results[@path] = AlarmSeverity::MTC
    expected_results[on_net_ovc] = AlarmSeverity::MTC
    eps.each {|ep| expected_results[ep] = AlarmSeverity::MTC}
    check_results(expected_results)

    # Now make a Brix alarm 
    # Off Net OVC & endpoints should be Dependency as On Net OVC is in Mtc
    puts "Brix Alarm" if $debug

    fault_off_net_ovc = ep_to_fault.segment_end_points[0].segment
    uni_ep = fault_off_net_ovc.segment_end_points.find_by_type("OvcEndPointUni")
    uni_ep.cos_end_points[0].cos_test_vectors[0].alarm_histories[0].add_event(Time.now.to_f*1000, "failed", "failed")

    expected_results[fault_off_net_ovc] = AlarmSeverity::DEPENDENCY
    expected_results[uni_ep] = AlarmSeverity::DEPENDENCY
    check_results(expected_results)

    # Now clear Brix alarm
    puts "Clear Brix Alarm" if $debug
    uni_ep.cos_end_points[0].cos_test_vectors[0].alarm_histories[0].add_event(Time.now.to_f*1000, "cleared", "")

    expected_results[fault_off_net_ovc] = AlarmSeverity::OK
    expected_results[uni_ep] = AlarmSeverity::OK
    check_results(expected_results)

    # Now Set On Net OVC to Live
    puts "On Net OVC Live" if $debug
    on_net_ovc.set_prov_state(ProvLive)  
    check_results(results_all_ok)

    # Now set Off Net OVC to MTC
    puts "Off Net OVC Mtc"  if $debug
    fault_off_net_ovc.set_prov_state(ProvMaintenance)
    expected_results = results_all_ok.clone 
    expected_results[@path] = AlarmSeverity::MTC
    expected_results[fault_off_net_ovc] = AlarmSeverity::MTC
    fault_off_net_ovc.segment_end_points.each {|ep| expected_results[ep] = AlarmSeverity::MTC}
    check_results(expected_results)

    # Now set Off Net OVC to Live
    puts "Off Net OVC Live" if $debug
    fault_off_net_ovc.set_prov_state(ProvLive) 
    check_results(results_all_ok)    
  end
  
  it "Standard Circuit Monitoring should give correct alarms" do
    # Test making a Off Net OVC monitorired => Not monitored and back
    on_net_ovc = Segment.find_by_service_id(100052)
    off_net_ovc = (@path.segments - [on_net_ovc]).first
    mon_ep = off_net_ovc.segment_end_points.find_by_is_monitored(true)
    check_path_live(@path)
    results_all_ok = all_ok(@path)

    # All EPs => NOT_MONITORED, Segment => NOT_MONITORED, Path => OK
    mon_ep.is_monitored = false
    mon_ep.save
    expected_results = results_all_ok.clone
    expected_results[off_net_ovc] = AlarmSeverity::NOT_MONITORED
    off_net_ovc.segment_end_points.each {|ep| expected_results[ep] = AlarmSeverity::NOT_MONITORED}
    check_results(expected_results)

    # All EPs => OK, Segment => OK, Path => OK
    mon_ep.is_monitored = true
    mon_ep.save
    check_results(results_all_ok)
  end
  
end


