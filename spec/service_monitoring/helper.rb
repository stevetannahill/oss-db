def clear_alarms(obj)
  if obj.get_SM_state.state != AlarmSeverity::OK
    obj.alarm_histories.each {|ah| ah.add_event(Time.now.to_f*1000, "cleared", "")}
    obj.update_alarm([], true) if obj.respond_to?("update_alarm") # Needed if the sql database was before the new performance changes
  end
end

def set_path_to_ok(path)
  path.segments.each do |segment|
    segment.segment_end_points.each do |ep|
      ep.cos_end_points.each {|cep| cep.cos_test_vectors.each {|ctv| clear_alarms(ctv)}}
      clear_alarms(ep.demarc)
      clear_alarms(ep)
    end
    clear_alarms(segment)
  end
  clear_alarms(path)
end  

def all_ok(path)
  results_all_ok = {}
  results_all_ok[path] = AlarmSeverity::OK
  path.segments.each do |segment|
    results_all_ok[segment] = AlarmSeverity::OK
    segment.segment_end_points.each do |ep|
      results_all_ok[ep] = AlarmSeverity::OK
      results_all_ok[ep.demarc] = AlarmSeverity::OK
    end
  end
  return results_all_ok
end
      
def check_path_live(path)
  if path.get_prov_state.is_a?(ProvLive) && path.get_SM_state.state == AlarmSeverity::OK
    if path.segments.all? {|segment| segment.get_prov_state.is_a?(ProvLive) && segment.get_SM_state.state == AlarmSeverity::OK} 
      eps = path.segments.collect {|segment| segment.segment_end_points}.flatten
      eps.reject! {|ep| ep.is_connected_to_test_port}
      return if eps.all? {|ep| ep.get_prov_state.is_a?(ProvLive) && ep.get_SM_state.state == AlarmSeverity::OK}
      failure_message = "FAILED TO check_path_live (eps):\n"
      eps.all? {|ep| failure_message += "#{ep.class}:#{ep.id} #{ep.get_SM_state.state}\n"}
    else
      failure_message = "FAILED TO check_path_live (segment):\n"
      path.segments.all? {|segment| failure_message += "#{segment.class}:#{segment.id} #{segment.get_SM_state.state}\n"}
    end
  else
    failure_message = "FAILED TO check_path_live (path):\n"
    failure_message += "#{path.class}:#{path.id} #{path.get_SM_state.state}\n"
  end
  fail(failure_message)
end

def check_results(results_to_check)
  results_to_check.each do |object, result|
    object.reload
    object.get_SM_state.state.should eq(result), "Failed #{object.class}:#{object.id} Expected state #{result} Got #{object.get_SM_state.state}"
  end
end