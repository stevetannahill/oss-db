def check_for_database_leak  
  # Check the number of KPIs match with the value before the test ran  
  if Kpi.all.size != $number_of_kpis_at_start
    fail("Failed - the number of Kpis has changed. Before:#{$number_of_kpis_at_start} After:#{Kpi.all.size}")
  elsif KpiOwnership.all.size != $number_of_kpi_ownerships_at_start
    fail("Failed - the number of Kpi_Ownerships has changed. Before:#{$number_of_kpi_ownerships_at_start} After:#{KpiOwnership.all.size}")
  end
end

# For the passed test_object get the hierarchy for the masks
# Then create a mask at each level and verify the result on the object
# Then delete the masks and check the result on the test object
def test_kpi_mask(kpi_to_test, test_object)  
  kpi_object_to_test = test_object.class.name
  test_object_hierarchy = test_object.kpi_hierarchy(kpi_to_test)
  test_kpi_unchanged = test_object.get_kpi(kpi_to_test, kpi_object_to_test, nil).attributes
  %w(id created_at updated_at warning_time).each {|attr| test_kpi_unchanged.delete(attr)}
  
  # Mask is already created for the ServiceProvider so set warning_time to 1
  test_value = 1000
  sp_mask = test_object_hierarchy.last[0].get_kpi_mask(kpi_to_test, kpi_object_to_test)
  sp_mask.warning_time = test_value
  sp_mask.save
  test_value += 1
  
  test_object_hierarchy[0..-2].reverse_each do |mask_object|
    new_mask = Kpi.create(:name => kpi_to_test, :for_klass => kpi_object_to_test, :warning_time => test_value, :actual_kpi => false)
    mask_object[0].kpis << new_mask
    # Check the test object has the changed kpi
    kpi = test_object.get_kpi(kpi_to_test, kpi_object_to_test, nil)
    if kpi == nil
      fail("Could not get kpi for #{test_object.class}:#{test_object.id} when setting mask on #{mask_object[0].class}#{mask_object[0].id}")
    else
      new_kpi_unchanged = kpi.attributes
      %w(id created_at updated_at warning_time).each {|attr| new_kpi_unchanged.delete(attr)}
      if new_kpi_unchanged != test_kpi_unchanged
        fail("Attributes that should be unchanged have changed for #{test_object.class}:#{test_object.id} New:#{new_kpi_unchanged} Old:#{test_kpi_unchanged} when setting mask on #{mask_object[0].class}:#{mask_object[0].id}")
      elsif kpi.warning_time != test_value
        fail("Kpi did not set correctly for #{test_object.class}:#{test_object.id} when setting mask on #{mask_object[0].class}:#{mask_object[0].id} Actual #{kpi.warning_time} should be #{test_value}")
      end
    end 
    test_value += 1
  end
  
  # Now delete the masks in reverse order
  test_object_hierarchy[0..-2].each do |mask_object|
    old_mask = mask_object[0].get_kpi_mask(kpi_to_test, kpi_object_to_test)
    old_test_value = test_object.get_kpi(kpi_to_test, kpi_object_to_test, nil).warning_time
    old_mask.destroy
    # Check the test object has the changed kpi
    kpi = test_object.get_kpi(kpi_to_test, kpi_object_to_test, nil)
    if kpi == nil
      fail("Failed to get kpi for #{test_object.class}:#{test_object.id} KPI #{kpi_to_test}")
    else
      new_kpi_unchanged = kpi.attributes
      %w(id created_at updated_at warning_time).each {|attr| new_kpi_unchanged.delete(attr)}
      
      if kpi.warning_time == old_test_value
        fail("KPI did not change for #{test_object.class}:#{test_object.id}")
      elsif new_kpi_unchanged != test_kpi_unchanged
        fail("Attributes that should be unchanged have changed for #{test_object.class}:#{test_object.id} New:#{new_kpi_unchanged} Old:#{test_kpi_unchanged} when deleting mask on #{mask_object[0].class}:#{mask_object[0].id}")          
      elsif kpi.warning_time == old_test_value-1
        # It's ok
      else
        fail("Kpi did not set correctly for #{test_object.class}:#{test_object.id} when deleting mask on #{mask_object[0].class}:#{mask_object[0].id} Actual #{kpi.warning_time} should be #{test_value}")
      end
    end
  end
  check_for_database_leak   
end

# For the passed test_object get the hierarchy for the masks
# Then create a mask at each level and verify the result on the object
# Then delete the masks from the top down. The KPI for the test_object should remain unchanged until
# it's mask is deleted and then it should take on the value of the top level mask
def test_kpi_mask_reverse(kpi_to_test, test_object)
  kpi_object_to_test = test_object.class.name
  test_object_hierarchy = test_object.kpi_hierarchy(kpi_to_test)
  test_kpi_unchanged = test_object.get_kpi(kpi_to_test, kpi_object_to_test, nil).attributes
  %w(id created_at updated_at warning_time).each {|attr| test_kpi_unchanged.delete(attr)}
  
  # Mask is already created for the ServiceProvider so set warning_time to 1
  test_value = 1000
  sp_mask = test_object_hierarchy.last[0].get_kpi_mask(kpi_to_test, kpi_object_to_test)
  sp_mask.warning_time = test_value
  sp_mask.save
  test_value += 1
  
  test_object_hierarchy[0..-2].reverse_each do |mask_object|
    new_mask = Kpi.create(:name => kpi_to_test, :for_klass => kpi_object_to_test, :warning_time => test_value, :actual_kpi => false)
    mask_object[0].kpis << new_mask
    # Check the test object has the changed kpi
    kpi = test_object.get_kpi(kpi_to_test, kpi_object_to_test, nil)
    if kpi == nil
      fail("Could not get kpi for #{test_object.class}:#{test_object.id} when setting mask on #{mask_object[0].class}:#{mask_object[0].id}")
    else
      new_kpi_unchanged = kpi.attributes
      %w(id created_at updated_at warning_time).each {|attr| new_kpi_unchanged.delete(attr)}
      if new_kpi_unchanged != test_kpi_unchanged
        fail("Attributes that should be unchanged have changed for #{test_object.class}:#{test_object.id} New:#{new_kpi_unchanged} Old:#{test_kpi_unchanged} when setting mask on #{mask_object[0].class}:#{mask_object[0].id}")
      elsif kpi.warning_time != test_value
        fail("Kpi did not set correctly for #{test_object.class}:#{test_object.id} when setting mask on #{mask_object[0].class}:#{mask_object[0].id} Actual #{kpi.warning_time} should be #{test_value}")
      end
    end 
    test_value += 1
  end
  
  kpi_after_added_masks = test_object.get_kpi(kpi_to_test, kpi_object_to_test, nil)
  # Now delete the masks in order
  test_object_hierarchy[1..-2].reverse_each do |mask_object|
    old_mask = mask_object[0].get_kpi_mask(kpi_to_test, kpi_object_to_test)
    old_test_value = test_object.get_kpi(kpi_to_test, kpi_object_to_test, nil).warning_time
    old_mask.destroy
    
    # Check the test object has not changed it's kpi 
    kpi = test_object.get_kpi(kpi_to_test, kpi_object_to_test, nil)
    if kpi == nil
      fail("Failed to get kpi for #{test_object.class}:#{test_object.id} KPI #{kpi_to_test}")
    elsif kpi_after_added_masks != kpi      
      fail("KPI should be unchanged but have changed for #{test_object.class}:#{test_object.id} New:#{kpi_after_added_masks.inspect} Old:#{kpi.inspect} deleting mask on #{mask_object[0].class}:#{mask_object[0].id}")          
    end
  end
  
  # Now delete the last mask and the KPI should now change
  mask_object = test_object_hierarchy[0]
  old_mask = mask_object[0].get_kpi_mask(kpi_to_test, kpi_object_to_test)
  top_level_kpi = test_object_hierarchy[-1][0].get_kpi(kpi_to_test, kpi_object_to_test, test_object_hierarchy[-1][0])
  old_mask.destroy
  kpi = test_object.get_kpi(kpi_to_test, kpi_object_to_test, nil)
  if kpi == nil
    fail("Failed to get kpi for #{test_object.class}:#{test_object.id} KPI #{kpi_to_test}")
  elsif top_level_kpi != kpi
    fail("KPI should be match the top level KPI for #{test_object.class}:#{test_object.id} Top level KPI:#{top_level_kpi} Old:#{kpi} when deleting mask on #{mask_object.class}:#{mask_object[0].id}")          
  end
  
  check_for_database_leak   
end

def test_utilization_apis(test_object)
  # Test the API's
  test_object.get_utilization_errored_period_delta_time_secs.should_not be_nil
  
  [:ingress, :egress].each do |direction|
    [:minor, :major].each do |severity|
      [:get_utilization_threshold_mbps, :get_utilization_threshold_percent, :get_utilization_errored_period_threshold, :get_utilization_errored_period_max_errored_count].each do |api|     
        if test_object.send(api, direction, severity) == nil
          fail("KPI api #{api} #{direction} #{severity} for #{test_object.class}:#{test_object.id} should not be nil")   
        end
      end
    end
  end
end

def test_sla_apis(test_object)
  # Test the API's
  test_object.get_errored_period_delta_time_secs.should_not be_nil
   
  [:minor, :major, :critical].each do |severity|
    [:delay, :delay_variation, :frame_loss].each do |metric|
      [:get_errored_period_threshold, :get_errored_period_max_errored_count].each do |api|
        # get_errored_period_max_errored_count does not take :critical severity
        next if api == :get_errored_period_max_errored_count && severity == :critical
        if test_object.send(api, severity, metric) == nil
          fail("KPI api #{api} #{metric} #{severity} for #{test_object.class}:#{test_object.id} should not be nil")
        end
      end
    end
  end
end



