require 'spec_helper'
require 'kpi/helper'

describe "KPI Tests" do

  before(:all) do
    load_database("CDB_EXPORT_For_Test_Suite").should == true unless ENV['SKIP_DB']
    # Delete all KPIs so we start with a clean slate
    KpiOwnership.delete_all
    Kpi.delete_all
    # Now add all the default KPIs
    ServiceProvider.all.each {|sp| sp.create_default_kpis}
    
    # check all the KPIs have been created
    (EnniNew.all + OnNetOvcEndPointEnni.all + BxCosTestVector.all).each do |object|
      kpi_names = object.kpis.find_all_by_actual_kpi(true).collect {|kpi| kpi.name}
      (Kpi::KPI_BY_KLASS[object.class.name] - kpi_names).should eq([]), "Failed KPI for #{object.class}:#{object.id} - KPIs are #{kpi_names} should be #{Kpi::KPI_BY_KLASS[object.class.name]}"
    end
    
    # Count and save the number of KPIs and KpiOwnerships so we can check for database leaks
    $number_of_kpis_at_start = Kpi.all.size
    $number_of_kpi_ownerships_at_start = KpiOwnership.all.size
  end

  describe "Enni KPI" do
    it "should set correct KPIs" do
      test_object = EnniNew.find_by_prov_name(ProvStateful::LIVE)
      kpi = Kpi::KPI_BY_KLASS[test_object.class.name].first
      test_kpi_mask(kpi, test_object)
      test_kpi_mask_reverse(kpi, test_object)
      
      # Test the API's
      test_utilization_apis(test_object)
    end
  end
      
  describe "BxCosTestVector KPI" do
    it "should set correct KPIs" do
      test_object = BxCosTestVector.first
      kpi = Kpi::KPI_BY_KLASS[test_object.class.name].first
      test_kpi_mask(kpi, test_object)
      test_kpi_mask_reverse(kpi, test_object)
      
      # Test the API's
      test_sla_apis(test_object)
    end
  end
  
  describe "SpirentCosTestVector KPI" do
    it "should set correct KPIs" do
      test_object = SpirentCosTestVector.first
      if test_object != nil
        kpi = Kpi::KPI_BY_KLASS[test_object.class.name].first
        test_kpi_mask(kpi, test_object)
        test_kpi_mask_reverse(kpi, test_object)
      
        # Test the API's
        test_sla_apis(test_object)
      end
    end
  end
  
  describe "OnNetOvcEndPointEnni KPI" do
    it "should set correct KPIs" do
      test_object = OnNetOvcEndPointEnni.find_by_prov_name(ProvStateful::LIVE)
      kpi = Kpi::KPI_BY_KLASS[test_object.class.name].last
      test_kpi_mask(kpi, test_object)
      test_kpi_mask_reverse(kpi, test_object)
      
      # Test the API's
      test_utilization_apis(test_object)
    end
  end
  
end
