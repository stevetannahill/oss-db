require 'spec_helper'

describe FalloutException do

  it "should set default if nil" do
    FalloutException.exception_types = nil
    FalloutException.exception_types.should == FalloutException::EXCEPTION_TYPES
  end

end
