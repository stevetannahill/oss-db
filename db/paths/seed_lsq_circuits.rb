require File.dirname(__FILE__) + '/seed_functions.rb'
require File.dirname(__FILE__) + '/seed_lsq_functions.rb'
require File.dirname(__FILE__) + '/cenx_definitions.rb'
require File.dirname(__FILE__) + '/lightsquared_definitions.rb'
require File.dirname(__FILE__) + '/cox_definitions.rb'
require File.dirname(__FILE__) + '/centurylink_definitions.rb'
require File.dirname(__FILE__) + '/zayo_definitions.rb'
require File.dirname(__FILE__) + '/sprintericsson_definitions.rb'
require File.dirname(__FILE__) + '/level3_definitions.rb'

################################################################################
# LSQ CIRCUITS
################################################################################
CXID_VGS_TUNNEL_1 = "101253634923"
CXID_VGS_TUNNEL_2 = "101260291836"
CXID_VGS_TUNNEL_3 = "101220745849"
CXID_VGS_TUNNEL_4 = "101219057606"
CXID_VGS_TUNNEL_5 = "101291350338"
CXID_VGS_TUNNEL_6 = "101297763417"
CXID_DLS_TUNNEL_1 = "101263393871"
CXID_DLS_TUNNEL_2 = "101277979157"
CXID_DLS_TUNNEL_3 = "101212974473"
CXID_DLS_TUNNEL_4 = "101247656644"
CXID_DLS_TUNNEL_5 = "101245309196"
CXID_DLS_TUNNEL_6 = "101224690078"
TUNNEL_USE_WEIGHTS = { :live => 3, :ready => 1, :testing => 1, :pending => 1 }
LSQ_SPEEDS=[ 50000, 100000 ]
DDD="2012/01/01"

lsq_path_ctr=0
cox_ctag=2000
cnl_ctag=2250
zyo_ctag=2500
ser_ctag=2750

# The OVC tunnels are organized in protection pairs of 1&4, 2&5, and 3&6.
vgsbkhl_tnls = get_weighted_bkhl_tunnel_pairs([[ CXID_VGS_TUNNEL_1, CXID_VGS_TUNNEL_4 ],
                                               [ CXID_VGS_TUNNEL_2, CXID_VGS_TUNNEL_5 ],
                                               [ CXID_VGS_TUNNEL_3, CXID_VGS_TUNNEL_6 ]])

dlsbkhl_tnls = get_weighted_bkhl_tunnel_pairs([[ CXID_DLS_TUNNEL_1, CXID_DLS_TUNNEL_4 ],
                                               [ CXID_DLS_TUNNEL_2, CXID_DLS_TUNNEL_5 ],
                                               [ CXID_DLS_TUNNEL_3, CXID_DLS_TUNNEL_6 ]])

max_val = [COX_ACCS2_ADDRS.size, CNL_ACCS_ADDRS.size, ZYO_ACCS_ADDRS.size, SER_ACCS_ADDRS.size].max-1
# max_val = 5


#cox_scos_map = { CENX_EXC_COS_RT => [4] }
#cnl_scos_map = { CENX_EXC_COS_RT => [5] }
#zyo_scos_map = { CENX_EXC_COS_RT => [5] }
#lv3_scos_map = { CENX_EXC_COS_RT => [5] }
#lv3_mcos_map = [ CENX_EXC_COS_STD => { :pcp => [1], :cir => "",       :cbs => "", :eir => "900000", :ebs => "" },
#                 CENX_EXC_COS_RT =>  { :pcp => [5], :cir => "100000", :cbs => "", :eir => "",       :ebs => "" } ]
lv3_mcos_map = [ { CENX_EXC_COS_STD => { :pcp => [1] } },
                 { CENX_EXC_COS_RT =>  { :pcp => [5] } } ]
#ser_mcos_map = [ CENX_EXC_COS_STD => { :pcp => [1], :cir => "",       :cbs => "", :eir => "900000", :ebs => "" },
#                 CENX_EXC_COS_RT =>  { :pcp => [6], :cir => "100000", :cbs => "", :eir => "",       :ebs => "" } ]
ser_mcos_map = [ { CENX_EXC_COS_STD => { :pcp => [1] } },
                 { CENX_EXC_COS_RT =>  { :pcp => [6] } } ]

ser_mcos_types = [ SER_ACCS_COS_RT,
                   SER_ACCS_COS_STD ]
ser_mcos_bandwidths = [ { :cir => 10000, :cbs => 30 },
                        { :eir => 90000, :ebs => 150 } ]




# Create a multi-segment Path; four extensions OVCs
ser_segment = Segment.find_by_cenx_id("101255906226")
ser_mwr = ser_segment.demarcs.first
ser_xt_ctag = 3000
lsq_mseg_path_ctr=1

[ "30 Enterprise Way, Houston, TX",
  "30 Discovery Blvd, Houston, TX",
  "30 Endeavour Rd, Houston, TX",
  "30 Atlantis St, Houston, TX" ].each do |uni_addr|

  xt_path = unified_create_path(LSQ_NTWK, "Path LSQ-MULTISEG-#{lsq_mseg_path_ctr+=1}", LSQ_NCELL_Path, DDD,
                              XDS, CENX_EXC_ESVCTYPE_NAME, CENX_MSO_COS_NAMES,
                              load_balance_tunnel_pairs(dlsbkhl_tnls), lv3_mcos_map, LV3_BKHL_NTWK, LV3_BKHL_OVCTYPE, LV3_BKHL_ESVC, LV3_BKHL_COSES, [{ :cir => 100000 }],
                                   nil, ser_xt_ctag+=1, LV3_BKHL_UNITYPE, LV3_BKHL_PHYTYPE_NAME, nil, nil,
                              [ser_segment], ser_mcos_map, SER_ACCS_NTWK, SER_ACCS_OVCTYPE, SER_ACCS_ESVC, ser_mcos_types, ser_mcos_bandwidths,
                                   "12", ser_xt_ctag, SER_ACCS_UNITYPE, SER_ACCS_PHYTYPE_NAME, nil, nil)

  create_path_extension(xt_path, ser_segment, ser_mwr, ser_xt_ctag, DDD, LSQ_SPEEDS[rand(LSQ_SPEEDS.size)], 54, 0, 0,
                       nil, ser_mcos_map, SER, SER_ACCS_NTWK, SER_ACCS_OVCTYPE, SER_ACCS_ESVC, ser_mcos_types, ser_mcos_bandwidths,
                         SER_ACCS_UNITYPE, ser_xt_ctag, SER_ACCS_PHYTYPE_NAME, gen_uni_id(uni_addr), uni_addr)
end






(1..max_val).each do |i|
  scos_bws = [ { :cir => LSQ_SPEEDS[rand(LSQ_SPEEDS.size)], :cbs => 54 } ]

  uni_addr=COX_ACCS2_ADDRS[i]
  if uni_addr
    unified_create_path(LSQ_NTWK, "Path LSQ-#{lsq_path_ctr+=1}", LSQ_1CELL_Path, DDD,
                       XLV, CENX_EXC_ESVCTYPE_NAME, CENX_MSO_COS_NAMES,
                       load_balance_tunnel_pairs(vgsbkhl_tnls), nil, LV3_BKHL_NTWK, LV3_BKHL_OVCTYPE, LV3_BKHL_ESVC, LV3_BKHL_COSES, scos_bws,
                            nil, cox_ctag+=1, LV3_BKHL_UNITYPE, LV3_BKHL_PHYTYPE_NAME, nil, nil,
                       nil, nil, COX_ACCS_NTWK, COX_ACCS_OVCTYPE, COX_ACCS_ESVC, [COX_ACCS_COS_RT], scos_bws,
                            nil, cox_ctag, COX_ACCS_UNITYPE, COX_ACCS_PHYTYPE_NAME, gen_uni_id(uni_addr), uni_addr)
  end

  uni_addr=CNL_ACCS_ADDRS[i]
  if uni_addr
    unified_create_path(LSQ_NTWK, "Path LSQ-#{lsq_path_ctr+=1}", LSQ_1CELL_Path, DDD,
                       XLV, CENX_EXC_ESVCTYPE_NAME, CENX_MSO_COS_NAMES,
                       load_balance_tunnel_pairs(vgsbkhl_tnls), nil, LV3_BKHL_NTWK, LV3_BKHL_OVCTYPE, LV3_BKHL_ESVC, LV3_BKHL_COSES, scos_bws,
                           nil, cnl_ctag+=1, LV3_BKHL_UNITYPE, LV3_BKHL_PHYTYPE_NAME, nil, nil,
                       nil, nil, CNL_ACCS_NTWK, CNL_ACCS_OVCTYPE, CNL_ACCS_ESVC, CNL_ACCS_COSES, scos_bws,
                           nil, cnl_ctag+=1, CNL_ACCS_UNITYPE, CNL_ACCS_PHYTYPE_NAME, gen_uni_id(uni_addr), uni_addr)
  end

  uni_addr=ZYO_ACCS_ADDRS[i]
  if uni_addr
    unified_create_path(LSQ_NTWK, "Path LSQ-#{lsq_path_ctr+=1}", LSQ_1CELL_Path, DDD,
                       XLV, CENX_EXC_ESVCTYPE_NAME, CENX_MSO_COS_NAMES,
                       load_balance_tunnel_pairs(vgsbkhl_tnls), nil, LV3_BKHL_NTWK, LV3_BKHL_OVCTYPE, LV3_BKHL_ESVC, LV3_BKHL_COSES, scos_bws,
                           nil, zyo_ctag+=1, LV3_BKHL_UNITYPE, LV3_BKHL_PHYTYPE_NAME, nil, nil,
                       nil, nil, ZYO_ACCS_NTWK, ZYO_ACCS_OVCTYPE, ZYO_ACCS_ESVC, ZYO_ACCS_COSES, scos_bws,
                           nil, zyo_ctag, ZYO_ACCS_UNITYPE, ZYO_ACCS_PHYTYPE_NAME, gen_uni_id(uni_addr), uni_addr)
  end

  uni_addr=SER_ACCS_ADDRS[i]
  if uni_addr
    unified_create_path(LSQ_NTWK, "Path LSQ-#{lsq_path_ctr+=1}", LSQ_1CELL_Path, DDD,
                       XDS, CENX_EXC_ESVCTYPE_NAME, CENX_MSO_COS_NAMES,
                       load_balance_tunnel_pairs(dlsbkhl_tnls), lv3_mcos_map, LV3_BKHL_NTWK, LV3_BKHL_OVCTYPE, LV3_BKHL_ESVC, LV3_BKHL_COSES, ser_mcos_bandwidths,
                           12, ser_ctag+=1, LV3_BKHL_UNITYPE, LV3_BKHL_PHYTYPE_NAME, nil, nil,
                       nil, ser_mcos_map, SER_ACCS_NTWK, SER_ACCS_OVCTYPE, SER_ACCS_ESVC, ser_mcos_types, ser_mcos_bandwidths,
                           nil, ser_ctag, SER_ACCS_UNITYPE, SER_ACCS_PHYTYPE_NAME, gen_uni_id(uni_addr), uni_addr)
  end
end

