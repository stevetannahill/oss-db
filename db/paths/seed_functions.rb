require 'factory_girl'
require File.dirname(__FILE__) + '/order_functions.rb'
require File.dirname(__FILE__) + '/component_functions.rb'
require File.dirname(__FILE__) + '/state_functions.rb'
Factory.find_definitions

# Turn state provisioning on
SET_PROV_STATE=true

# in memory hash of ennis and an associated array of used stags
# (required to prevent overlaps)
STAG_SPACE = {}

def create_stag_space(enni)
  stags = []
  (100..4095).each { |tag| stags.push(tag) }

  STAG_SPACE[enni] = stags
end


def get_next_stag(enni)
  if STAG_SPACE[enni].nil?
    create_stag_space(enni)
  end

  STAG_SPACE[enni].delete(STAG_SPACE[enni].first)
end


def gen_uni_id(address)
  pieces = address.gsub(" ", "").upcase.split(",")
  uni_id = "#{pieces[0][0..7]}#{pieces[pieces.size-2][0..1]}#{pieces[pieces.size-1][0..1]}"

  return uni_id
end


def load_balance_ennis(ennis, candidate_cir_kbps)
  if ennis.empty?
    return nil
  end

  good_ennis = ennis.find(:all, :conditions => ['pop_id != ?', Pop.find(:first, :conditions => 'name LIKE "%Jersey%"').id])
  return good_ennis[rand(good_ennis.size)]

  min_enni = nil
  min_cir = -1

  ennis.each do |enni|
  
    # NJ ENNIs were unexpectedly added to the infrastructure, and we do not want...
    unless enni.pop.name.include?("New Jersey")
      # EnniNew.rate reports in Mbps...and yes I know that Mbps to kbps converts by
      # 2^10 and not 10^3, but no one else seems that fussed, and this is more
      # conservative, so...
      enni_available_kbps = (enni.rate * 1000) - enni.get_provisioned_bandwidth[:grand][:cir].to_i

      # the ENNI must at least be able to serve the requested bandwidth
      unless candidate_cir_kbps.to_i > enni_available_kbps
        if min_cir > enni_available_kbps or 0 > min_cir
          min_enni = enni
          min_cir = enni_available_kbps
        end
      end
    end
  end

  return min_enni
end


def random_mac
  mac = Array.new

  # yes MAC octets go to ff
  for i in 1..6
    mac.push(random_number(2))
  end

  return mac.join(":")
end


def assign_member_handle(owner, entity, type, handle)
  inst = entity.member_handle_instance_by_owner owner
  #return if inst.nil?
  inst.member_attr = MemberHandle.find(:first, :conditions => [ "affected_entity_type = ?", type ])
  inst.value = handle
  inst.save!

  return inst
end


def create_display_entry(path, entity, column, row, position)
  d = PathDisplay.new
  d.path = path
  d.entity = entity
  d.column = column
  d.row = row
  d.relative_position = position
  d.save!

  return d
end


def create_path_handles(path_name, owners)
  path_handles = []

  owners.each do |owner|
    path_handles.push({ :owner => owner, :value => "#{owner.name}:#{path_name}" })
  end

  return path_handles
end


def overlay_path_FOR_DEMO(path_owner, path_ntwk, owner_path_name, reference_path, due_date)
  off_net_ovcs = reference_path.off_net_ovcs
  on_net_ovcs = reference_path.on_net_ovcs

  puts "\nCreating Circuit: #{owner_path_name}..."
  ownrfolks = path_owner.contacts
  path_order = place_path_order(path_ntwk, "Circuit Order:#{owner_path_name}", due_date, random_from_list(ownrfolks), random_from_list(ownrfolks), random_from_list(ownrfolks), random_from_list(ownrfolks))

  # assign handles to Path for all component members
  path_handles = create_path_handles(owner_path_name,
                                   [path_owner, off_net_ovcs.map(&:service_provider), on_net_ovcs.map(&:service_provider)].flatten.compact.uniq)
  path = create_path(path_order, path_ntwk, reference_path.cenx_path_type)

  # assign Path to Off Net OVCs
  off_net_ovcs.each do |offovc|
    offovc.paths.push(path)
  end

  # assign Path to On Net OVCs
  on_net_ovcs.each do |onovc|
    onovc.paths.push(path)
  end

end


#
# owner_ntwk_name
# owner_path_name
# path_type
# prov_thresholds
# due_date
# cir_kbps
# cbs_kB
# eir_kbps
# ebs_kB
# pop_name
# on_net_stype_name
# on_net_cosname
# a_segments
# ntwk_a_name
# segment_type_a_name
# stype_a_name
# cosnames_a
# uni_type_a_name
# uni_a_vlan
# uni_a_phy
# uni_a_id
# uni_a_addr
# z_segments
# ntwk_z_name
# segment_type_z_name
# stype_z_name
# cosnames_a
# uni_type_z_name
# uni_z_vlan
# uni_z_phy
# uni_z_id
# uni_z_addr
def unified_create_path(path_ntwk, owner_path_name, path_type, due_date,
                       pop, on_net_stype_name, on_net_cosnames,
                       a_segments, a_cosmaps, ntwk_a, segment_type_a, stype_a, costypes_a, cos_bws_a,
                               a_retag, uni_a_vlan, uni_type_a, uni_a_phy, uni_a_id, uni_a_addr,
                       z_segments, z_cosmaps, ntwk_z, segment_type_z, stype_z, costypes_z, cos_bws_z,
                               z_retag, uni_z_vlan, uni_type_z, uni_z_phy, uni_z_id, uni_z_addr)

  a_segments = a_segments.nil? ? [] : a_segments
  z_segments = z_segments.nil? ? [] : z_segments

  # we'll need to remember whether the A/Z side infrastructure was created in this function or not
  puts "a_side_new = #{a_segments.nil?} or #{a_segments.empty?}"
  puts "z_side_new = #{z_segments.nil?} or #{z_segments.empty?}"
  a_side_new = (a_segments.nil? or a_segments.empty?)
  z_side_new = (z_segments.nil? or z_segments.empty?)

  path_owner = path_ntwk.service_provider
  member_a = ntwk_a.service_provider
  member_z = ntwk_z.service_provider

  puts "\nCreating Circuit: #{owner_path_name}..."
  ownrfolks = path_owner.contacts.compact
  path_order = place_path_order(path_ntwk, "Circuit Order:#{owner_path_name}", due_date, random_from_list(ownrfolks), random_from_list(ownrfolks), random_from_list(ownrfolks), random_from_list(ownrfolks))

  # assign handles to Path for all component members
  path_handles = create_path_handles(owner_path_name,
                                   [member_a, member_z, path_owner, a_segments.map(&:service_provider), z_segments.map(&:service_provider)].flatten.compact.uniq)

  path = create_path(path_order, path_ntwk, path_type)


  if a_side_new
    ennis_a = ntwk_a.enni_news.find(:all, :conditions => ['pop_id = ?', pop.id])

    puts "Creating A-side UNI: #{uni_a_id}..."
    uni_a_order = place_uni_order(path_order, due_date, ntwk_a, uni_a_addr, uni_a_vlan, path_ntwk, random_from_list(ownrfolks), random_from_list(ownrfolks), random_from_list(ownrfolks))

    uni_a_handles = [ { :owner => member_a, :value => "#{member_a.name}:Circuit #{owner_path_name}:UNI-A #{uni_a_id}" },
                      { :owner => member_z, :value => "#{member_z.name}:Circuit #{owner_path_name}:UNI-A #{uni_a_id}" } ]
    if ( path_owner.id != member_a.id ) and ( path_owner.id != member_z.id )
      uni_a_handles.push({:owner => path_owner, :value => "#{path_owner.name}:Circuit #{owner_path_name}:UNI-A #{uni_a_id}" })
    end
    uni_a = create_uni(uni_a_order, path_owner, ntwk_a, uni_type_a, uni_a_addr)

    puts "Creating A-side OVC..."
    segment_a_order = place_off_net_ovc_order(path_order, due_date, ntwk_a, uni_a, random_from_list(ownrfolks), random_from_list(ownrfolks), random_from_list(ownrfolks))
    offovc_a_handles = [ { :owner => member_a, :value => "#{member_a.name}:Circuit #{owner_path_name}:Off Net OVC-A" },
                       { :owner => member_z, :value => "#{member_z.name}:Circuit #{owner_path_name}:Off Net OVC-A" }]
    if ( path_owner.id != member_a.id ) and ( path_owner.id != member_z.id )
      offovc_a_handles.push({:owner => path_owner, :value => "#{path_owner.name}:Circuit #{owner_path_name}:Off Net OVC-A" })
    end
    offovc_a = create_off_net_ovc(segment_a_order, uni_a, uni_a_vlan, ennis_a[rand(ennis_a.size)], nil, segment_type_a, stype_a, costypes_a, cos_bws_a)

    a_segments = [offovc_a]

  else
    a_segments.each do |a_segment|
      a_segment.paths.push(path)
    end
  end


  if z_side_new
    ennis_z = ntwk_z.enni_news.find(:all, :conditions => ['pop_id = ?', pop.id])

    puts "Creating Z-side UNI: #{uni_z_id}..."
    uni_z_order = place_uni_order(path_order, due_date, ntwk_z, uni_z_addr, uni_z_vlan, path_ntwk, random_from_list(ownrfolks), random_from_list(ownrfolks), random_from_list(ownrfolks))

    uni_z_handles = [ { :owner => member_a, :value => "#{member_a.name}:Circuit #{owner_path_name}:UNI-Z #{uni_z_id}" },
                      { :owner => member_z, :value => "#{member_z.name}:Circuit #{owner_path_name}:UNI-Z #{uni_z_id}" } ]
    if ( path_owner.id != member_a.id ) and ( path_owner.id != member_z.id )
      uni_z_handles.push({:owner => path_owner, :value => "#{path_owner.name}:Circuit #{owner_path_name}:UNI-Z #{uni_z_id}" })
    end
    uni_z = create_uni(uni_z_order, path_owner, ntwk_z, uni_type_z, uni_z_addr)

    puts "Creating Z-side OVC..."
    segment_z_order = place_off_net_ovc_order(path_order, due_date, ntwk_z, uni_z, random_from_list(ownrfolks), random_from_list(ownrfolks), random_from_list(ownrfolks))
    offovc_z_handles = [ { :owner => member_a, :value => "#{member_a.name}:Circuit #{owner_path_name}:Off Net OVC-Z" },
                       { :owner => member_z, :value => "#{member_z.name}:Circuit #{owner_path_name}:Off Net OVC-Z" }]
    if ( path_owner.id != member_a.id ) and ( path_owner.id != member_z.id )
      offovc_z_handles.push({:owner => path_owner, :value => "#{path_owner.name}:Circuit #{owner_path_name}:Off Net OVC-Z" })
    end
    offovc_z = create_off_net_ovc(segment_z_order, uni_z, uni_z_vlan, ennis_z[rand(ennis_z.size)], nil, segment_type_z, stype_z, costypes_z, cos_bws_z)

    z_segments = [offovc_z]

  else
    z_segments.each do |z_segment|
      z_segment.paths.push(path)
    end
  end


  puts "Creating On Net OVC..."
  on_net_ovc_order = place_on_net_ovc_order(path_order, due_date, pop, random_from_list(ownrfolks), random_from_list(ownrfolks), random_from_list(ownrfolks))
  onovc_handles = [ { :owner => member_a, :value => "#{member_a.name}:Circuit #{owner_path_name}:On Net Ovc" },
                    { :owner => member_z, :value => "#{member_z.name}:Circuit #{owner_path_name}:On Net Ovc" } ]
  if ( path_owner.id != member_a.id ) and ( path_owner.id != member_z.id )
    onovc_handles.push({:owner => path_owner, :value => "#{path_owner.name}:Circuit #{owner_path_name}:On Net Ovc" })
  end
  onovc = create_on_net_ovc(on_net_ovc_order, on_net_stype_name, on_net_cosnames,
                          uni_a_vlan, a_retag, a_segments, cos_bws_a, a_cosmaps,
                          z_cosmaps, cos_bws_z, z_segments, z_retag, uni_z_vlan)
#                          cir_kbps, cbs_kB, eir_kbps, ebs_kB)



  puts "Assigning Member Handles"
  #We must save and reload so the associations can generate the Member Handles
  entities = [path, uni_a, uni_z, a_segments, z_segments, onovc].flatten
  entities.each { |e| next if e.nil?; e.save; e = e.reload}
  
  path_handles.each do |handle|
    assign_member_handle(handle[:owner], path, "Path", handle[:value])
  end
  if a_side_new
    uni_a_handles.each do |handle|
      assign_member_handle(handle[:owner], uni_a, "Demarc", handle[:value])
    end 
    offovc_a_handles.each do |handle|
      assign_member_handle(handle[:owner], offovc_a, "Segment", handle[:value])
    end
  end
  if z_side_new
    uni_z_handles.each do |handle|
      assign_member_handle(handle[:owner], uni_z, "Demarc", handle[:value])
    end
    offovc_z_handles.each do |handle|
      assign_member_handle(handle[:owner], offovc_z, "Segment", handle[:value])
    end
  end
  onovc_handles.each do |handle|
    assign_member_handle(handle[:owner], onovc, "Segment", handle[:value])
  end

  if SET_PROV_STATE
    puts "Assigning provisioning states..."
    # only set prov state on what we own
    my_a_uni = a_side_new ? uni_a : nil
    my_a_segments = a_side_new ? a_segments : []
    my_z_uni = z_side_new ? uni_z : nil
    my_z_segments = z_side_new ? z_segments : []
  
    assign_path_prov_states(0.05, 0.15, 0.25, my_a_uni, my_a_segments, [onovc], my_z_segments, my_z_uni)
  end

  # Recall the layout_diagram function, since certain Paths were missing their OffNetOvc on the layout
  # (we don't know why)
  Path.find(path.id).layout_diagram

  return path.reload
end


def create_path_extension(path, path_segment, path_uni, path_uni_vlan, due_date, cir_kbps, cbs_kB, eir_kbps, ebs_kB,
                         xt_segments, xt_cosmaps, xt_member, xt_ntwk, xt_segmenttype, xt_stype, xt_costypes, xt_cosbws,
                           xt_unitype, xt_uni_vlan, xt_uni_phy, xt_uni_id, xt_xt_uniddr)

  xt_segments = xt_segments.nil? ? [] : xt_segments
  path_owner = path.service_provider
  path_order = path.get_latest_order

  # we'll need to remember whether the extended side of the infrastructure was created in this function or not
  xt_new_segment = (xt_segments.nil? or xt_segments.empty?)

  if xt_new_segment
    ownrfolks = path_owner.contacts

    puts "Creating Extension UNI: #{xt_uni_id}..."
    xt_uni_order = place_uni_order(path_order, due_date, xt_ntwk, xt_xt_uniddr, xt_uni_vlan, path.operator_network, random_from_list(ownrfolks), random_from_list(ownrfolks), random_from_list(ownrfolks))

    xt_uni_handles = [ { :owner => xt_member, :value => "#{xt_member.name}:Circuit #{path.member_handle}:UNI-X #{xt_uni_id}" } ]
    if ( path_owner.id != xt_member.id )
      xt_uni_handles.push({:owner => path_owner, :value => "#{path_owner.name}:Circuit #{path.member_handle}:UNI-X #{xt_uni_id}" })
    end
    xt_uni = create_uni(xt_uni_order, path_owner, xt_ntwk, xt_unitype, xt_xt_uniddr)

    puts "Creating Extension OVC..."
    xt_segment_order = place_off_net_ovc_order(path_order, due_date, xt_ntwk, xt_uni, random_from_list(ownrfolks), random_from_list(ownrfolks), random_from_list(ownrfolks))
    xt_offovc_handles = [ { :owner => xt_member, :value => "#{xt_member.name}:Circuit #{path.member_handle}:Off Net OVC-X" } ]
    if ( path_owner.id != xt_member.id )
      xt_offovc_handles.push({:owner => path_owner, :value => "#{path_owner.name}:Circuit #{path.member_handle}:Off Net OVC-X" })
    end
    xt_offovc = create_off_net_ovc(xt_segment_order, xt_uni, xt_uni_vlan, path_uni, path_uni_vlan, xt_segmenttype, xt_stype, xt_costypes, xt_cosbws)

    # connect extension OVC to peer OVC on the other side of the shared UNI
    path_segment.ovc_end_point_unis.find(:all, :conditions => [ 'demarc_id = ?', path_uni.id ]).each do |peer_segmentep|
      xt_offovc.ovc_end_point_unis.first.segment_end_points.push(peer_segmentep)
    end

    xt_segments = [xt_offovc]

  else
    xt_segments.each do |xt_segment|
      xt_segment.paths.push(path)
    end
  end


  puts "Assigning Member Handles"
  #We must save and reload so the associations can generate the Member Handles
  entities = [xt_uni, xt_segments].flatten
  entities.each { |e| next if e.nil?; e.save; e = e.reload}
  
  if xt_new_segment
    xt_uni_handles.each do |handle|
      assign_member_handle(handle[:owner], xt_uni, "Demarc", handle[:value])
    end 
    xt_offovc_handles.each do |handle|
      assign_member_handle(handle[:owner], xt_offovc, "Segment", handle[:value])
    end
  end

#  if SET_PROV_STATE
#    puts "Assigning provisioning states..."
#    # only set prov state on what we own
#    my_a_uni = a_side_new ? uni_a : nil
#    my_a_segments = a_side_new ? a_segments : []
#    my_z_uni = z_side_new ? uni_z : nil
#    my_z_segments = z_side_new ? z_segments : []
#  
#    assign_path_prov_states(0.05, 0.15, 0.25, my_a_uni, my_a_segments, [onovc], my_z_segments, my_z_uni)
#  end

  # Recall the layout_diagram function, since certain Paths were missing their OffNetOvc on the layout
  # (we don't know why)
  Path.find(path.id).layout_diagram

  puts "Extend complete!"
end

