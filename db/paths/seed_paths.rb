require File.dirname(__FILE__) + '/seed_lsq_functions.rb'
require File.dirname(__FILE__) + '/seed_functions.rb'

# script will build a "set" of a half-dozen circuits;
#   Charter/Cox, Charter/TWC, Cox/Charter, Cox/TWC, TWC/Charter, and TWC/Cox
# you can easily set how many "sets" you want.
#   num_of_paths = num_of_sets * 6
#
# Note that I've only loaded 100 addresses per member, and 4 will be used for each set
NUM_OF_HALF_DOZEN_SETS=1

# Set weightings for balancing Paths over Live vs. Not-Live backhaul tunnel pairs
TUNNEL_USE_WEIGHTS = { :live => 3, :ready => 1, :testing => 1, :pending => 1 }

CXID_VGS_TUNNEL_1 = "101253634923"
CXID_VGS_TUNNEL_2 = "101260291836"
CXID_VGS_TUNNEL_3 = "101220745849"
CXID_VGS_TUNNEL_4 = "101219057606"
CXID_VGS_TUNNEL_5 = "101291350338"
CXID_VGS_TUNNEL_6 = "101297763417"
CXID_DLS_TUNNEL_1 = "101263393871"
CXID_DLS_TUNNEL_2 = "101277979157"
CXID_DLS_TUNNEL_3 = "101212974473"
CXID_DLS_TUNNEL_4 = "101247656644"
CXID_DLS_TUNNEL_5 = "101245309196"
CXID_DLS_TUNNEL_6 = "101224690078"

# The OVC tunnels are organized in protection pairs of 1&4, 2&5, and 3&6.
vgsbkhl_tnls = get_weighted_bkhl_tunnel_pairs([[ CXID_VGS_TUNNEL_1, CXID_VGS_TUNNEL_4 ],
                                               [ CXID_VGS_TUNNEL_2, CXID_VGS_TUNNEL_5 ],
                                               [ CXID_VGS_TUNNEL_3, CXID_VGS_TUNNEL_6 ]])

dlsbkhl_tnls = get_weighted_bkhl_tunnel_pairs([[ CXID_DLS_TUNNEL_1, CXID_DLS_TUNNEL_4 ],
                                               [ CXID_DLS_TUNNEL_2, CXID_DLS_TUNNEL_5 ],
                                               [ CXID_DLS_TUNNEL_3, CXID_DLS_TUNNEL_6 ]])

DDD="2012/01/01"
LSQ = "LightSquared"
LV = "Las Vegas"
DLS = "Dallas"
XLA="Los Angeles"
SPEEDS=[ "10000", "100000" ]

CHR="Charter Communications"
CHR_NTWK="Charter Los Angeles MSO Network"
CHR_ESVC="EVPL"
CHR_COS=["SLA 2"]
CHR_UNITYPE="ETH-COPPER"
CHR_OVCTYPE="EVPL OVC"
chr_ctag=1200
chr_addr_ctr=0
CHR_UNI_ADDRS = [ "2000 HWY 100, CRESCENT CITY, CA",
                  "16147 HIGHWAY 101 S, BROOKINGS, OR",
                  "875 HWY 101, FLORENCE, OR",
                  "45525 HWY 101, SIXES, OR",
                  "202 S 12TH ST, LA GRANDE, OR",
                  "18350 HIGHWAY 18, APPLE VALLEY, CA",
                  "1924 E 19TH, THE DALLES, OR",
                  "321 S 1ST ST, BURBANK, CA",
                  "237 SW 1ST ST, PENDLETON, OR",
                  "1 E. 1ST ST, RENO, NV",
                  "1175 HIGHWAY 2, WRIGHTWOOD, CA",
                  "105 INTERSTATE 20 W, MARSHALL, TX",
                  "11084 W 21ST ST, PASO ROBLES, CA",
                  "2420 NE 22ND ST, LINCOLN CITY, OR",
                  "7590 HIGHWAY 238, JACKSONVILLE, OR",
                  "701 E 28TH ST, LONG BEACH, CA",
                  "3043 NORTHEAST 28TH ST., LINCOLN CITY, OR",
                  "600 SOUTH 2ND ST, CENTRAL POINT, OR",
                  "2500 E 2ND ST, RENO, NV",
                  "1500 E. 2ND ST., RENO, NV",
                  "1312 SW 2ND, PENDLETON, OR",
                  "73326 HIGHWAY 331, PENDLETON, OR",
                  "2004 NORTHWEST 36TH ST, LINCOLN CITY, OR",
                  "1107 HWY 395 GARDNERVILLE, GARDNERVILLE, NV",
                  "323 E 3RD AVE, SUTHERLIN, OR",
                  "730 W 3RD ST, LONG BEACH, CA",
                  "7200 HWY 50 EAST, CARSON CITY, NV",
                  "1050 HWY 50 EAST, SILVER SPRINGS, NV",
                  "155 HIGHWAY 50, STATELINE, NV",
                  "HARBOR 555 5TH ST, BROOKINGS, OR",
                  "333 SW 5TH STREET, GRANTS PASS, OR",
                  "500 NW 6TH ST # A, GRANTS PASS, OR",
                  "1600 NW 6TH ST, GRANTS PASS, OR",
                  "4315 S 6TH ST, KLAMATH FALLS, OR",
                  "1060 E. 70TH ST, LONG BEACH, CA",
                  "1014 NE 7TH ST, GRANTS PASS, OR",
                  "855 W 7TH ST, RENO, NV",
                  "877 NE 7TH STREET, GRANTS PASS, OR",
                  "5227 FM 813, WAXAHACHIE, TX",
                  "1360 NE 9TH ST, GRANTS PASS, OR",
                  "1001 E 9TH ST, RENO, NV",
                  "101 NW A ST, GRANTS PASS, OR",
                  "331 W. AARDEN AVE, GLENDALE, CA",
                  "3310 E AIRPORT WAY, LONG BEACH, CA",
                  "100 W ALAMEDA AVE, BURBANK, CA",
                  "828 WS ALSBURY BLVD, BURLESON, TX",
                  "202 LOS ALTOS, SPARKS, NV",
                  "212 WEST ANN ST, CARSON CITY, NV",
                  "5405 MAE ANNE AVE, RENO, NV",
                  "10813 EL ARCO DR, WHITTIER, CA",
                  "645 N ARLINGTON AVE, RENO, NV",
                  "2501 GENOA ASPEN DR, GENOA, NV",
                  "749 COMMERCIAL, ASTORIA, OR",
                  "5855 CAPISTRANO, ATASCADERO, CA",
                  "4401 ATLANTIC AVE, 3RD FLOOR, LONG BEACH, CA",
                  "19794 RIVERSIDE AVE, ANDERSON, CA",
                  "1653 JEROME AVE, ASTORIA, OR",
                  "625 CHETCO AVE, BROOKINGS, OR",
                  "1350 TEAKWOOD AVE, COOS BAY, OR",
                  "632 THOMPSON AVE, GLENDALE, CA",
                  "29620 ELLENSBURG AVE, GOLD BEACH, OR",
                  "215 INDUSTRIAL AVE, GRANBURY, TX",
                  "1780 NEBRASKA AVE, GRANTS PASS, OR",
                  "9650 7TH AVE, HESPERIA, CA",
                  "3100 CASCADE AVE, HOOD RIVER, OR",
                  "2865 DAGGETT AVE, KLAMATH FALLS, OR",
                  "1333 SWAN AVE, LIVINGSTON, CA",
                  "1057 PINE AVE, LONG BEACH, CA",
                  "2632 PACIFIC AVE, LOS ANGELES, CA",
                  "1090 KNUDSEN AVE, MEDFORD, OR",
                  "350 WOODVIEW AVE, MORGAN HILL, CA",
                  "2727 HAMNER AVE, NORCO, CA",
                  "230 POMEROY AVE, PISMO BEACH, CA",
                  "9589 MILLIKEN AVE, RANCHO CUCAMONGA, CA",
                  "1100 PARKVIEW AVE, REDDING, CA",
                  "605 SYLVAN AVE, RIVERBANK, CA",
                  "4383 TEQUESQUITE AVE, RIVERSIDE, CA",
                  "503 CENTRAL AVE, SAN BERNARDINO, CA",
                  "1194 PACIFIC AVE, SAN LUIS OBISPO, CA",
                  "2645 GUNDRY AVE, SIGNAL HILL, CA",
                  "2170 SOUTH AVE, SOUTH LAKE TAHOE, CA",
                  "2170 SOUTH AVE, SOUTH LAKE TAHOE, NV",
                  "1125 VICTORIAN AVE, SPARKS, NV",
                  "1801 COLORADO AVE, TURLOCK, CA",
                  "14344 CAJON AVE, VICTORVILLE, CA",
                  "1100 GRAND AVE, WALNUT, CA",
                  "1609 CAMERON AVE, WEST COVINA, CA",
                  "7800 PACIFIC AVE, WHITE CITY, OR",
                  "3959 SHERIDAN AVE., COOS BAY, OR",
                  "1760 TERMINO AVE., LONG BEACH, CA",
                  "777 CYPRESS AVE., REDDING, CA",
                  "1855 CHICAGO AVE., RIVERSIDE, CA",
                  "814 AIRWAY AVE., SUTHERLIN, OR",
                  "10232 ARROYO AVENUE, HESPERIA, CA",
                  "3030 WALNUT AVENUE, LONG BEACH, CA",
                  "1400 UNIVERSITY AVENUE, RIVERSIDE, CA",
                  "3527 NORTH BANK RD, LINCOLN CITY, OR",
                  "1333 E BARNETT RD, MEDFORD, OR",
                  "3736 E. BARNETT RD., MEDFORD, OR",
                  "63466 BOAT BASIN RD, COOS BAY, OR" ]

COX="Cox"
COX_NTWK="Cox Los Angeles MSO Network"
COX_ESVC="EWAN"
COX_COS=["Basic"]
COX_UNITYPE="UNI"
COX_OVCTYPE="EWAN OVC"
cox_ctag=1500
cox_addr_ctr=0
COX_UNI_ADDRS = [ "20701 S 164TH ST, CHANDLER, AZ",
                  "7101 N 19TH AVE, PHOENIX, AZ",
                  "24250 N 23RD AVE, PHOENIX, AZ",
                  "7050 S 24TH ST, PHOENIX, AZ",
                  "7611 S 36TH ST, PHOENIX, AZ",
                  "2620 N 3RD ST, PHOENIX, AZ",
                  "14435 N 48TH ST, PHOENIX, AZ",
                  "34308 N 60TH ST, PHOENIX, AZ",
                  "200 N 83RD AVE, TOLLESON, AZ",
                  "6855 N ALIANTE PKWY, NORTH LAS VEGAS, NV",
                  "1287 N ALMA SCHOOL RD, CHANDLER, AZ",
                  "41551 N ANTHEM HILLS DR, NEW RIVER, AZ",
                  "200 N ARROYO GRANDE BLVD, HENDERSON, NV",
                  "5141 CALIFORNIA AVE, IRVINE, CA",
                  "5151 CALIFORNIA AVE, IRVINE, CA",
                  "5171 CALIFORNIA AVE, IRVINE, CA",
                  "5251 CALIFORNIA AVE, IRVINE, CA",
                  "3240 PALM AVE, SAN DIEGO, CA",
                  "750 W BASELINE RD, TEMPE, AZ",
                  "1832 S BOULDER HWY, HENDERSON, NV",
                  "6740 N BOULDER HWY, HENDERSON, NV",
                  "920 N BOULDER HWY, HENDERSON, NV",
                  "1600 W BROADWAY RD, TEMPE, AZ",
                  "6111 EL CAMINO REAL, CARLSBAD, CA",
                  "100 SHADY CANYON DR, IRVINE, CA",
                  "14804 N CAVE CREEK RD, PHOENIX, AZ",
                  "2555 W CENTENNIAL PKWY, NORTH LAS VEGAS, NV",
                  "43 AUTO CENTER DR, IRVINE, CA",
                  "653 TOWN CENTER DR, LAS VEGAS, NV",
                  "1999 W CITRACADO PKWY, ESCONDIDO, CA",
                  "3880 W DEER SPRINGS WAY, NORTH LAS VEGAS, NV",
                  "9444 WAPLES, SAN DIEGO, CA",
                  "2000 E DIVISION ST, NATIONAL CITY, CA",
                  "601 VETERANS DR, BOULDER CITY, NV",
                  "101 ACADEMY DR, IRVINE, CA",
                  "4199 CAMPUS DR, IRVINE, CA",
                  "4255 CAMPUS DR, IRVINE, CA",
                  "1002 S DURANGO DR, LAS VEGAS, NV",
                  "13960 W EAGLE ST @ (BLDG 176 ), GLENDALE, AZ",
                  "711 N EVERGREEN RD, TEMPE, AZ",
                  "250 E FLAMINGO RD, PARADISE, NV",
                  "2910 S GREENFIELD RD, GILBERT, AZ",
                  "2151 S HWY 92, SIERRA VISTA, AZ",
                  "1613 N JACKRABBIT TRL, BUCKEYE, AZ",
                  "1871 N LANCASTER DR, BUCKEYE, AZ",
                  "3145 S LAS VEGAS BLVD, LAS VEGAS, NV",
                  "750 E MAIN ST, EL CAJON, CA",
                  "4022 DEAN MARTIN DR, LAS VEGAS, NV",
                  "3959 DEAN MARTIN DR, PARADISE, NV",
                  "7525 DEAN MARTIN DR, PARADISE, NV",
                  "10701 E MARY ANN CLEVELAND WAY, VAIL, AZ",
                  "3970 E MISTRAL AVE, PARADISE, NV",
                  "5757 WAYNE NEWTON BLVD, LAS VEGAS, NV",
                  "1771 E PALOMAR ST, CHULA VISTA, CA",
                  "3201 W PEORIA AVE, PHOENIX, AZ",
                  "8421 E PIMA CENTER PKWY, SCOTTSDALE, AZ",
                  "11 RANCHO PKWY S, LAKE FOREST, CA",
                  "28000 MARGUERITE PKWY, MISSION VIEJO, CA",
                  "1901 N RANCHO DR, LAS VEGAS, NV",
                  "1999 N RANCHO DR, LAS VEGAS, NV",
                  "4005 N RANCHO DR, LAS VEGAS, NV",
                  "2 ORCHARD RD, LAKE FOREST, CA",
                  "6996 E RUSSELL RD, PARADISE, NV",
                  "15044 N SCOTTSDALE RD, SCOTTSDALE, AZ",
                  "15169 N SCOTTSDALE RD, SCOTTSDALE, AZ",
                  "16435 N SCOTTSDALE RD, SCOTTSDALE, AZ",
                  "2550 N SCOTTSDALE RD, SCOTTSDALE, AZ",
                  "6710 N SCOTTSDALE RD, SCOTTSDALE, AZ",
                  "10561 JEFFREYS ST, PARADISE, NV",
                  "9698 ESCONDIDO ST, PARADISE, NV",
                  "16485 W STADIUM WAY, PEORIA, AZ",
                  "10401 W THUNDERBIRD, SUN CITY, AZ",
                  "6800 S TORREY PINES DR, LAS VEGAS, NV",
                  "7000 LAS VEGAS BLVD N, NORTH LAS VEGAS, NV",
                  "1120 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "125 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "2000 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3300 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3355 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3400 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3475 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3535 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3555 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3570 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3595 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3600 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3645 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3655 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3667 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3725 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3750 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3770 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3790 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3799 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3801 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3850 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3900 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "3950 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "5191 LAS VEGAS BLVD S, LAS VEGAS, NV",
                  "8020 LAS VEGAS BLVD S, LAS VEGAS, NV" ]

TWC="Time Warner Cable"
TWC_NTWK="TWC Los Angeles MSO Network"
TWC_ESVC="Metro E (EVPL)"
TWC_COS=["Expedite / Forwarding"]
TWC_UNITYPE="Copper"
TWC_OVCTYPE="Metro E (EVPL) OVC"
twc_ctag=1800
twc_addr_ctr=0
TWC_UNI_ADDRS = [ "100 PIER 1 PL, FORT WORTH, TX",
                  "3019 FM 1092 RD, MISSOURI CITY, TX",
                  "113 SW 10TH ST, AMARILLO, TX",
                  "306 E 10TH ST, AUSTIN, TX",
                  "6280 FM 1102, NEW BRAUNFELS, TX",
                  "610 E 11TH ST, AUSTIN, TX",
                  "2501 S 121 BUS HWY, LEWISVILLE, TX",
                  "3535 W 12TH ST, HOUSTON, TX",
                  "201 E 14TH ST, AUSTIN, TX",
                  "601 E 15TH ST, AUSTIN, TX",
                  "1105 W 15TH ST, PLANO, TX",
                  "4400 W 18TH ST, HOUSTON, TX",
                  "4300 FM 1960 DR W, HOUSTON, TX",
                  "3707 FM 1960, HOUSTON, TX",
                  "5601 E 1ST ST, FORT WORTH, TX",
                  "1519 FM 2234 RD, MISSOURI CITY, TX",
                  "715 W 23RD ST, AUSTIN, TX",
                  "304 E 24TH ST, AUSTIN, TX",
                  "411 SW 24TH ST, SAN ANTONIO, TX",
                  "5316 W 290 HWY, AUSTIN, TX",
                  "919 E 32ND ST, AUSTIN, TX",
                  "801 W 34TH ST, AUSTIN, TX",
                  "5512 W 34TH ST, HOUSTON, TX",
                  "805 W 37TH ST, AUSTIN, TX",
                  "1600 W 38TH ST, AUSTIN, TX",
                  "1503 W 3RD ST, AUSTIN, TX",
                  "421 W 3RD ST, FORT WORTH, TX",
                  "1100 W 49TH ST, AUSTIN, TX",
                  "500 E 4TH ST, AUSTIN, TX",
                  "13738 FM 529 RD, HOUSTON, TX",
                  "902 E 5TH ST, AUSTIN, TX",
                  "1650 HWY 6, SUGAR LAND, TX",
                  "828 W 6TH ST, AUSTIN, TX",
                  "114 W 7TH ST, AUSTIN, TX",
                  "2821 W 7TH ST, FORT WORTH, TX",
                  "203 SW 8TH AVE, AMARILLO, TX",
                  "205 W 9TH ST, AUSTIN, TX",
                  "1101 E 9TH ST, FORT WORTH, TX",
                  "101 W ABRAM ST, ARLINGTON, TX",
                  "15555 STUEBNER AIRLINE RD, HOUSTON, TX",
                  "11104 W AIRPORT BLVD, STAFFORD, TX",
                  "12808 W AIRPORT BLVD, SUGAR LAND, TX",
                  "4441 W AIRPORT FWY, IRVING, TX",
                  "400 S AKARD ST, DALLAS, TX",
                  "3810 W ALABAMA ST, HOUSTON, TX",
                  "1302 S ALAMO ST, SAN ANTONIO, TX",
                  "850 E ANDERSON LN, AUSTIN, TX",
                  "100 MICHAEL ANGELO WAY, AUSTIN, TX",
                  "100 MICHAEL ANGELO WAY, ROUND ROCK, TX",
                  "5311 SHERRI ANN RD, SAN ANTONIO, TX",
                  "707 E ARAPAHO RD, RICHARDSON, TX",
                  "2911 MEDICAL ARTS ST, AUSTIN, TX",
                  "919 CONGRESS AVE, AUSTIN, TX",
                  "11910 GREENVILLE AVE, DALLAS, TX",
                  "6842 INDUSTRIAL AVE, EL PASO, TX",
                  "2705 RASTUS AVE, FIFTH STREET, TX",
                  "108 ROSS AVE, FORT WORTH, TX",
                  "11011 RICHMOND AVE, HOUSTON, TX",
                  "1845 SUMMIT AVE, PLANO, TX",
                  "400 TEXAS AVE, ROUND ROCK, TX",
                  "1310 MCCULLOUGH AVE, SAN ANTONIO, TX",
                  "2400 GRAND AVENUE PKWY, WELLS BRANCH, TX",
                  "3311 CLAY AVENUE, WACO, TX",
                  "1605 LYNDON B JOHNSON FWY, FARMERS BRANCH, TX",
                  "105 W BAGDAD AVE, ROUND ROCK, TX",
                  "15990 N BARKERS LANDING RD, HOUSTON, TX",
                  "108 WILD BASIN RD S, AUSTIN, TX",
                  "200 E BASSE RD, SAN ANTONIO, TX",
                  "1285 JOE BATTLE BLVD, EL PASO, TX",
                  "3218 E BELKNAP ST, FORT WORTH, TX",
                  "11109 A BELLAIRE BLVD, HOUSTON, TX",
                  "4059 W BELLFORT ST, HOUSTON, TX",
                  "2101 W BEN WHITE BLVD, AUSTIN, TX",
                  "2200 PARK BEND DR, AUSTIN, TX",
                  "2538 E BITTERS RD, SAN ANTONIO, TX",
                  "1510 COLUMBIA BLUE DR, MISSOURI CITY, TX",
                  "3501 ED BLUESTEIN BLVD, AUSTIN, TX",
                  "5900 GATEWAY BLVD E, EL PASO, TX",
                  "5959 GATEWAY BLVD W, EL PASO, TX",
                  "2400 GROVE BLVD, AUSTIN, TX",
                  "3000 IRVING BLVD, DALLAS, TX",
                  "1000 HAWKINS BLVD, EL PASO, TX",
                  "2801 NETWORK BLVD, FRISCO, TX",
                  "3400 MONTROSE BLVD, HOUSTON, TX",
                  "4301 REGENT BLVD, IRVING, TX",
                  "1555 INDEPENDENCE BLVD, MISSOURI CITY, TX",
                  "2221 LAKESIDE BLVD, RICHARDSON, TX",
                  "2401 GREENLAWN BLVD, ROUND ROCK, TX",
                  "5606 RANDOLPH BLVD, SAN ANTONIO, TX",
                  "225 INDUSTRIAL BLVD, SUGAR LAND, TX",
                  "3150 PAT BOOKER RD, UNIVERSAL CITY, TX",
                  "151 N BOONE ST, EL PASO, TX",
                  "8001 BENT BRANCH DR, IRVING, TX",
                  "1250 WOOD BRANCH PARK DR, HOUSTON, TX",
                  "2800 WELLS BRANCH PKWY, WELLS BRANCH, TX",
                  "4150 RIO BRAVO ST, EL PASO, TX",
                  "1310 S BRAZOS ST, SAN ANTONIO, TX",
                  "121 S BRIERY RD, IRVING, TX",
                  "6215 W BY NORTHWEST BLVD, HOUSTON, TX",
                  "1001 E CAMPBELL RD, RICHARDSON, TX" ]

def gen_uni_id(address)
  pieces = address.gsub(" ", "").split(",")
  uni_id = "#{pieces[0][0..7]}#{pieces[pieces.size-2][0..1]}#{pieces[pieces.size-1][0..1]}"

  return uni_id
end

#def unified_create_path(owner_name, owner_ntwk_name, owner_path_name, path_type,
#                       due_date, cir_kbps, cbs_kB, eir_kbps, ebs_kB,
#                       pop_name, on_net_stype_name, on_net_cosnames,
#                       a_segments, member_a_name, ntwk_a_name, segment_type_a_name, stype_a_name, cosnames_a,
#                               uni_type_a_name, uni_a_vlan, uni_a_phy, uni_a_id, uni_a_addr,
#                       z_segments, member_z_name, ntwk_z_name, segment_type_z_name, stype_z_name, cosnames_z,
#                               uni_type_z_name, uni_z_vlan, uni_z_phy, uni_z_id, uni_z_addr)

# MAINLINE
#
# From here on, we're creating our Paths.
path_ctr=0

#(1..NUM_OF_HALF_DOZEN_SETS).each do |i|
  unified_create_path(CHR, CHR_NTWK, "Path #{path_ctr+=1}", "Standard",
                     DDD, "50000", "54", "0", "0",
                     XLA, "Cenx Exchange", "Real Time",
                     nil, CHR, CHR_NTWK, CHR_OVCTYPE, CHR_ESVC, CHR_COS, CHR_UNITYPE, chr_ctag+=1, "1000Base-T", gen_uni_id(CHR_UNI_ADDRS[chr_addr_ctr+=1]), CHR_UNI_ADDRS[chr_addr_ctr],
                     [], COX, COX_NTWK, COX_OVCTYPE, COX_ESVC, COX_COS, COX_UNITYPE, cox_ctag+=1, "1000Base-T", gen_uni_id(COX_UNI_ADDRS[cox_addr_ctr+=1]), COX_UNI_ADDRS[cox_addr_ctr])

  unified_create_path(LSQ, "Nationwide LongHaul Network", "Path 1000", "LightSquared Single Cell",
                     DDD, "100000", "54", "0", "0",
                     LV, "Cenx Exchange", "Real Time",
                     load_balance_tunnel_pairs(vgsbkhl_tnls), "Level 3", "Nationwide LongHaul Network", "DWDM Long Haul Service OVC", "DWDM Long Haul Service", ["DWDM CoS"], 
                          "DWDM Handoff", 1000, "1000Base-T", "UNIA-ID", "UNIA-ADDRESS",
                     nil, COX, "Cox Las Vegas Network", "Cox Ethernet Backhaul OVC", "Cox Ethernet Backhaul", ["Real Time"],
                          "UNI A (for Light Squared)", 1000, "1000Base-T", "TMUSNVLVGS0001-C3", "4978 Boulder Highway, Las Vegas" )

#create_sc_path(LSQ,  LV,  "5/21/2011",  "60 mo",  "TMUSNVLVGS0001-C3", "4978 Boulder Highway, Las Vegas",            "Cox",             "1000", "Cox Ethernet Backhaul",             "Real Time",        "Cox Ethernet Backhaul OVC",         "100000", "54", load_balance_tunnel_pairs(vgsbkhl_tnls))

#create_sc_path(LSQ,  LV,  "8/22/2011",  "60 mo",  "TMUSNVLVGS0002-C2", "200 E Fremont Street, Las Vegas",            "Zayo",            "2000", "Dedicated Ethernet",                "Real Time Class",  "Dedicated Ethernet OVC",            "100000", "54", load_balance_tunnel_pairs(vgsbkhl_tnls))

#create_sc_path(LSQ,  LV,  "6/25/2011",  "84 mo",  "TMUSNVLVGS0004-C1", "7375 Tule Springs Road, Las Vegas",          "CenturyLink",     "4",    "EVPL",                              "Real-Time (Gold)", "EVPL OVC",                          "100000", "54", load_balance_tunnel_pairs(vgsbkhl_tnls))


#
#
#  create_core_path(CHR, CHR_NTWK, "Path #{path_ctr+=1}", DDD, XLA, SPEEDS[rand(SPEEDS.size)], "54", "0", "0",
#                    CHR, CHR_NTWK, CHR_ESVC,               CHR_COS,                 CHR_UNITYPE,  gen_uni_id(CHR_UNI_ADDRS[chr_addr_ctr+=1]), CHR_UNI_ADDRS[chr_addr_ctr], chr_ctag+=1, CHR_OVCTYPE,
#                    COX, COX_NTWK, COX_ESVC,               COX_COS,                 COX_UNITYPE,  gen_uni_id(COX_UNI_ADDRS[cox_addr_ctr+=1]), COX_UNI_ADDRS[cox_addr_ctr], cox_ctag+=1, COX_OVCTYPE)
#  
#  create_core_path(CHR, CHR_NTWK, "Path #{path_ctr+=1}", DDD, XLA, SPEEDS[rand(SPEEDS.size)], "54", "0", "0",
#                    CHR, CHR_NTWK, CHR_ESVC,               CHR_COS,                 CHR_UNITYPE,  gen_uni_id(CHR_UNI_ADDRS[chr_addr_ctr+=1]), CHR_UNI_ADDRS[chr_addr_ctr], chr_ctag+=1, CHR_OVCTYPE,
#                    TWC, TWC_NTWK, TWC_ESVC,               TWC_COS,                 TWC_UNITYPE,  gen_uni_id(TWC_UNI_ADDRS[twc_addr_ctr+=1]), TWC_UNI_ADDRS[twc_addr_ctr], twc_ctag+=1, TWC_OVCTYPE)
#  
#  create_core_path(COX, COX_NTWK, "Path #{path_ctr+=1}", DDD, XLA, SPEEDS[rand(SPEEDS.size)], "54", "0", "0",
#                    COX, COX_NTWK, COX_ESVC,               COX_COS,                 COX_UNITYPE,  gen_uni_id(COX_UNI_ADDRS[cox_addr_ctr+=1]), COX_UNI_ADDRS[cox_addr_ctr], cox_ctag+=1, COX_OVCTYPE,
#                    CHR, CHR_NTWK, CHR_ESVC,               CHR_COS,                 CHR_UNITYPE,  gen_uni_id(CHR_UNI_ADDRS[chr_addr_ctr+=1]), CHR_UNI_ADDRS[chr_addr_ctr], chr_ctag+=1, CHR_OVCTYPE)
#  
#  create_core_path(COX, COX_NTWK, "Path #{path_ctr+=1}", DDD, XLA, SPEEDS[rand(SPEEDS.size)], "54", "0", "0",
#                    COX, COX_NTWK, COX_ESVC,               COX_COS,                 COX_UNITYPE,  gen_uni_id(COX_UNI_ADDRS[cox_addr_ctr+=1]), COX_UNI_ADDRS[cox_addr_ctr], cox_ctag+=1, COX_OVCTYPE,
#                    TWC, TWC_NTWK, TWC_ESVC,               TWC_COS,                 TWC_UNITYPE,  gen_uni_id(TWC_UNI_ADDRS[twc_addr_ctr+=1]), TWC_UNI_ADDRS[twc_addr_ctr], twc_ctag+=1, TWC_OVCTYPE)
#  
#  create_core_path(TWC, TWC_NTWK, "Path #{path_ctr+=1}", DDD, XLA, SPEEDS[rand(SPEEDS.size)], "54", "0", "0",
#                    TWC, TWC_NTWK, TWC_ESVC,               TWC_COS,                 TWC_UNITYPE,  gen_uni_id(TWC_UNI_ADDRS[twc_addr_ctr+=1]), TWC_UNI_ADDRS[twc_addr_ctr], twc_ctag+=1, TWC_OVCTYPE,
#                    CHR, CHR_NTWK, CHR_ESVC,               CHR_COS,                 CHR_UNITYPE,  gen_uni_id(CHR_UNI_ADDRS[chr_addr_ctr+=1]), CHR_UNI_ADDRS[chr_addr_ctr], chr_ctag+=1, CHR_OVCTYPE)
#  
#  create_core_path(TWC, TWC_NTWK, "Path #{path_ctr+=1}", DDD, XLA, SPEEDS[rand(SPEEDS.size)], "54", "0", "0",
#                    TWC, TWC_NTWK, TWC_ESVC,               TWC_COS,                 TWC_UNITYPE,  gen_uni_id(TWC_UNI_ADDRS[twc_addr_ctr+=1]), TWC_UNI_ADDRS[twc_addr_ctr], twc_ctag+=1, TWC_OVCTYPE,
#                    COX, COX_NTWK, COX_ESVC,               COX_COS,                 COX_UNITYPE,  gen_uni_id(COX_UNI_ADDRS[cox_addr_ctr+=1]), COX_UNI_ADDRS[cox_addr_ctr], chr_ctag+=1, COX_OVCTYPE)
#end
