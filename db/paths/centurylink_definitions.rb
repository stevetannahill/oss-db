CNL_NAME="CenturyLink"
CNL_ACCS_NTWK_NAME="CenturyLink Las Vegas Network"
CNL_ACCS_ESVC_NAME="EVPL"
CNL_ACCS_COS_NAMES=["Real-Time (Gold)"]
CNL_ACCS_UNITYPE_NAME="1G UNI (for Light Squared)"
CNL_ACCS_PHYTYPE_NAME="1000Base-LX"
CNL_ACCS_OVCTYPE_NAME="EVPL OVC"

CNL           = (defined? CNL).nil?           ? ServiceProvider.find_by_name(CNL_NAME) : CNL
CNL_ACCS_NTWK      = (defined? CNL_ACCS_NTWK).nil?      ? CNL.operator_networks.find_by_name(CNL_ACCS_NTWK_NAME) : CNL_ACCS_NTWK
CNL_ACCS_NTWKTYPE  = (defined? CNL_ACCS_NTWKTYPE).nil?  ? CNL_ACCS_NTWK.operator_network_type : CNL_ACCS_NTWKTYPE
CNL_ACCS_ESVC      = (defined? CNL_ACCS_ESVC).nil?      ? CNL_ACCS_NTWKTYPE.ethernet_service_types.find_by_name(CNL_ACCS_ESVC_NAME) : CNL_ACCS_ESVC
CNL_ACCS_COSES     = (defined? CNL_ACCS_COSES).nil?     ? CNL_ACCS_ESVC.class_of_service_types.find(:all, :conditions => [ "name IN (?)", CNL_ACCS_COS_NAMES ]) : CNL_ACCS_COSES
CNL_ACCS_UNITYPE   = (defined? CNL_ACCS_UNITYPE).nil?   ? CNL_ACCS_ESVC.demarc_types.find_by_name(CNL_ACCS_UNITYPE_NAME) : CNL_ACCS_UNITYPE
CNL_ACCS_OVCTYPE   = (defined? CNL_ACCS_OVCTYPE).nil?   ? CNL_ACCS_ESVC.segment_types.find_by_name(CNL_ACCS_OVCTYPE_NAME) : CNL_ACCS_OVCTYPE

CNL_ACCS_ADDRS = [ "7375 Tule Springs Road, Las Vegas",
                   "5380 Novak Street, Las Vegas",
                   "5050 Steptoe Street, Las Vegas",
                   "1234 N. Boulder Hwy, Henderson",
                   "81 N. Gibson, Henderson",
                   "2072 Desert Shadow Trail, Henderson",
                   "975 W. Galleria, Henderson",
                   "2402 Atchley Drive, Las Vegas",
                   "7450 W. Lake Mead, Las Vegas",
                   "2995 Mt. Hood Street, Las Vegas",
                   "8601 Helena Avenue, Las Vegas",
                   "310 S. Decatur, Las Vegas",
                   "3603 N. Las Vegas Blvd, Las Vegas",
                   "2022 Spring Gate Lane, Las Vegas",
                   "807 Cadiz Avenue, Henderson",
                   "741 S. Rainbow, las vegas",
                   "1940 Ramrod Lane, Henderson",
                   "3695 W. Lake Mead Blvd., Henderson",
                   "3500 West Cheyenne Avenue, North Las Vegas",
                   "5219 La Concha Drive, Las Vegas",
                   "7727 Mustang, Las Vegas",
                   "9278 W. Maule Ave, Las Vegas",
                   "1455 N. jones, N. Las Vegas",
                   "8646 W. Agate, Las Vegas",
                   "5907 Willis Drive, North Las Vegas",
                   "936 E. Sahara Ave, Las Vegas",
                   "3350 Fremont Street, Las Vegas",
                   "2555 Windmill Parkway, Henderson",
                   "North East Corner Horizon Ridge, Henderson",
                   "2372 Ione Rd, Las Vegas",
                   "361 Julia Street, Henderson",
                   "1673 W. NeaL Ave., Las Vegas",
                   "6175 W. Tropicana Avenue, Las Vegas",
                   "6724 W. Lone Mtn., Las Vegas",
                   "4996 Palo Verde, Las Vegas",
                   "8250 S. Maryland Pkwy, Las Vegas",
                   "8040 S. Las Vegas Blvd., Las Vegas",
                   "1003 Industrial Road, Boulder City",
                   "6830 Erie Ave, Las Vegas",
                   "10247 Bermuda Rd., Las Vegas",
                   "415 Perlite Way, Henderson",
                   "2620 W. Horizon Ridge Pkwy., Henderson",
                   "5125 E. Tropicana, Las Vegas",
                   "4851 E. Bonanza, Las Vegas",
                   "4850 N. JonesBlvd, Las vegas",
                   "8649 S. Rainbow Blvd., Las Vegas",
                   "7258 Hauck Street, Las Vegas",
                   "1350 Richard Bunker Ave, Henderson",
                   "4767 Tropical Parkway, Las Vegas",
                   "3032 S. Durango Drive, Las Vegas",
                   "2500 Anthem Drive, Henderson",
                   "467 E. Silverado Ranch Blvd, Las Vegas",
                   "8390 N. Decatur Boulevard, N. Las Vegas",
                   "10411 Ensworth Street, Las Vegas",
                   "3811 W. Charleston Boulevard, Las Vegas",
                   "2250 Red Springs Drive, Las Vegas",
                   "4608 S. Hualapai Way, Las Vegas",
                   "5438 Camino Al Norte, North Las Vegas",
                   "6960 W. Robindale Road, Las Vegas",
                   "270 N. Valle Verde Drive, Henderson",
                   "8925 S Eastern Avenue, Las Vegas",
                   "3187 S. Rainbow Blvd., Las Vegas",
                   "10452 S. Decatur Blvd., Las Vegas",
                   "4095 E. Flamingo Road, Las Vegas",
                   "6555 Boulder Hwy, Las Vegas",
                   "1665 North Lamb, Las Vegas",
                   "3840 N. Commerce St., North Las Vegas",
                   "4122 S. Hollywood Blvd., Las Vegas",
                   "2213 N. Green Valley Pkwy, Henderson NV",
                   "5825 W. Sahara  Ave, Las Vegas",
                   "7455 W. Washington, Las Vegas",
                   "933 Greenway Rd., Henderson",
                   "300 S. 4th Street, Las Vegas",
                   "4960 rogers street, Las Vegas",
                   "4350 S. Maryland Pkwy, Las Vegas",
                   "3850 LeonAve, N. Las Vegas",
                   "550 Conestoga, Henderson",
                   "1600 W. Sunset, Henderson" ]
