require 'factory_girl'
require File.dirname(__FILE__) + '/seed_functions.rb'
Factory.find_definitions

# in memory hash of ennis and an associated array of used stags
# (required to prevent overlaps)
STAG_SPACE = {}

def get_weighted_bkhl_tunnel_pairs(on_net_id_pairs)
  bkhl_segment_pairs = []
  weighted_bkhl_segment_pairs = []

  on_net_id_pairs.each do |on_net_id_pair|
    segment1 = Segment.find_by_cenx_id(on_net_id_pair[0])
    segment2 = Segment.find_by_cenx_id(on_net_id_pair[1])

    bkhl_segment_pairs.push([segment1,segment2])
  end

  bkhl_segment_pairs.each do |bkhl_pair|
    bkhl_pair_1st_state = bkhl_pair[0].get_prov_state.name
    bkhl_pair_2nd_state = bkhl_pair[1].get_prov_state.name

    if "Pending".eql?(bkhl_pair_1st_state) or "Pending".eql?(bkhl_pair_2nd_state)
      (1..TUNNEL_USE_WEIGHTS[:pending]).each do |i|
        puts "Pending: adding #{bkhl_pair}"
        weighted_bkhl_segment_pairs.push(bkhl_pair)
      end

    elsif "Testing".eql?(bkhl_pair_1st_state) or "Testing".eql?(bkhl_pair_2nd_state)
      (1..TUNNEL_USE_WEIGHTS[:testing]).each do |i|
        puts "Testing: adding #{bkhl_pair}"
        weighted_bkhl_segment_pairs.push(bkhl_pair)
      end

    elsif "Ready".eql?(bkhl_pair_1st_state) or "Ready".eql?(bkhl_pair_2nd_state)
      (1..TUNNEL_USE_WEIGHTS[:ready]).each do |i|
        puts "Ready: adding #{bkhl_pair}"
        weighted_bkhl_segment_pairs.push(bkhl_pair)
      end

    elsif "Live".eql?(bkhl_pair_1st_state) or "Live".eql?(bkhl_pair_2nd_state)
      (1..TUNNEL_USE_WEIGHTS[:live]).each do |i|
        puts "Live: adding #{bkhl_pair}"
        weighted_bkhl_segment_pairs.push(bkhl_pair)
      end

    # shouldn't be any other states, but will fall back on pending
    else
      (1..TUNNEL_USE_WEIGHTS[:pending]).each do |i|
        puts "Else: adding #{bkhl_pair}"
        weighted_bkhl_segment_pairs.push(bkhl_pair)
      end
    end
  end

  return weighted_bkhl_segment_pairs
end


def load_balance_tunnel_pairs(segment_tunnel_pairs)
  # we shall assume that the tunnel pair is equally used on the primary and secondary,
  # so only a check of the primary is necessary:"34
  return segment_tunnel_pairs[rand(segment_tunnel_pairs.size)]
end


#def create_ls_enni(ntwk, pop, address)
#  enni = Factory.create(:enni_new, 
#                                   :operator_network => ntwk,
#                                   :pop => pop,
#                                   :address => address,
#                                   :physical_medium => "10GigE LR; 1310nm; SMF")
#
#  return enni
#end


#def create_ls_on_net_ovc(order, uni_vlan, svc_model, cir_kbps, cbs_kB, accs_segments, bkhl_segments, member_handles)
#
#  pop = accs_segments.first.ovc_end_point_ennis.first.demarc.pop
#
#  cenx_ntwk = pop.cenx_operator_network
#  cenx_ntwk_type = cenx_ntwk.operator_network_type
#  eth_svc_type = cenx_ntwk_type.ethernet_service_types.first
#  path = Path.find(order.path_id)
#
#  on_net_ovc = Factory.create(:on_net_ovc, :paths => [path],
#                                           :path_network_id => path.operator_network.id,
#                                           :pop => pop,
#                                           #:service_id => IdTools::ServiceIdGenerator.generate_on_net_ovc_service_id(pop),
#                                           :ethernet_service_type => eth_svc_type)
#
#  member_handles.each do |handle|
#    assign_member_handle(handle[:owner], on_net_ovc, "Segment", handle[:value])
#  end
#
#  order.ordered_entity = on_net_ovc
#  order.save!
#
#  bkhl_enni_eps = []
#  accs_enni_eps = []
#  row_ctr_a = 0
#  row_ctr_z = 0
#
#  # build cos infrastructure - use Real Time CoS for LSQ
#  cos_type = eth_svc_type.class_of_service_types.find(:first, :conditions => [ "name = ?", "Real Time" ])
#  cos = Factory.create(:cenx_cos_instance, :class_of_service_type => cos_type,
#                                           :segment => on_net_ovc)
#
#  cos_type_condition = "class_of_service_type_id = #{cos_type.id}"
#  bwp_type_enni_ingress = BwProfileType.find(:all, :conditions => "#{cos_type_condition} AND bwp_type = 'ENNI-INGRESS'").first
#
#
#  bkhl_segments.each do |bkhl_segment|
#    bkhl_segment.ovc_end_point_ennis.each do |bkhl_ep|
#      row_ctr_a += 1
#
#      # create On Net Segment End Point, and associate with its peer on the
#      # backhaul OVC
#      on_net_ep = Factory.create(:on_net_ovc_end_point_enni, :demarc => bkhl_ep.demarc,
#                                                      :segment_end_point_type => cenx_ntwk_type.segment_end_point_types.find(:first, :conditions => "name LIKE 'ENNI%'"),
#                                                      :stags => uni_vlan,
#                                                      :segment => on_net_ovc)
#      on_net_ep.segment_end_points.push(bkhl_ep)
#
#      # create attached CoS end point, and associate with its peer on the
#      # backhaul Segment End Point
#      on_net_ep_cos = Factory.create(:cos_end_point, :cos_instance => cos,
#                                                :type => "CenxCosEndPoint",
#                                                :segment_end_point => on_net_ep,
#                                                :ingress_rate_limiting => "Policing",
#                                                :egress_rate_limiting => "Policing", # supposed to be None, but getting errors
#                                                :ingress_mapping => "*",
#                                                :egress_marking => "5", # default for LSQ - would like a better way to get this value
##                                                :egress_eir_kbps => "",
##                                                :egress_ebs_kB => "",
##                                                :egress_eir_kbps => "",
##                                                :egress_ebs_kB => "",
#                                                :ingress_cir_kbps => cir_kbps,
#                                                :ingress_cbs_kB =>   cbs_kB,
#                                                :egress_cir_kbps => cir_kbps,
#                                                :egress_cbs_kB =>   cbs_kB)
#      bkhl_ep.cos_end_points.each { |cos_ep| on_net_ep_cos.cos_end_points.push(cos_ep) }
#
#      # while putting in the display entries for the OnNetOvc endpoints and associated
#      # backhaul Exchanges, I've been asked to put in display entries for the backhaul
#      # circuits and the far-side demarcs.  This logic will not work if the backhaul
#      # circuit is multi-point on the far-side
#      create_display_entry(path, bkhl_segment.ovc_end_point_unis.first.demarc, "A", row_ctr_a, nil)
#      create_display_entry(path, bkhl_segment.ovc_end_point_unis.first,        "B", row_ctr_a, "Left")
#      create_display_entry(path, bkhl_segment,                                 "B", row_ctr_a, nil)
#      create_display_entry(path, bkhl_segment.ovc_end_point_ennis.first,       "B", row_ctr_a, "Right")
#      create_display_entry(path, bkhl_ep.demarc,                           "C", row_ctr_a, nil)
#      create_display_entry(path, on_net_ep,                                     "D", row_ctr_a, "Left")
#      create_display_entry(path, on_net_ovc,                                 "D", row_ctr_a, nil)
#    end
#  end
#
#  accs_segments.each do |accs_segment|
#    accs_segment.ovc_end_point_ennis.each do |accs_ep|
#      row_ctr_z += 1
#
#      # create On Net Segment End Point, and associate with its peer on the
#      # access OVC
#      on_net_ep = Factory.create(:on_net_ovc_end_point_enni, :demarc => accs_ep.demarc,
#                                                      :segment_end_point_type => cenx_ntwk_type.segment_end_point_types.find(:first, :conditions => "name LIKE 'ENNI%'"),
#                                                      :ctags => "UNI-ENNI".eql?(svc_model) ? uni_vlan : "*",
#                                                      :stags => "UNI-ENNI".eql?(svc_model) ? get_next_stag(accs_ep.demarc) : uni_vlan,
#                                                      :segment => on_net_ovc)
#      on_net_ep.segment_end_points.push(accs_ep)
#
#      # create attached CoS end point, and associate with its peer on the
#      # access Segment End Point
#      on_net_ep_cos = Factory.create(:cos_end_point, :cos_instance => cos,
#                                                :type => "CenxCosEndPoint",
#                                                :segment_end_point => on_net_ep,
#                                                :ingress_rate_limiting => "Policing",
#                                                :egress_rate_limiting => "Policing",
#                                                :ingress_mapping => "*",
#                                                :egress_marking => accs_ep.cos_end_points.first.class_of_service_type.cos_marking_uni_egress,
##                                                :egress_eir_kbps => "",
##                                                :egress_ebs_kB => "",
##                                                :egress_eir_kbps => "",
##                                                :egress_ebs_kB => "",
#                                                :ingress_cir_kbps => cir_kbps,
#                                                :ingress_cbs_kB =>   cbs_kB,
#                                                :egress_cir_kbps => cir_kbps,
#                                                :egress_cbs_kB =>   cbs_kB)
#      accs_ep.cos_end_points.each { |cos_ep| on_net_ep_cos.cos_end_points.push(cos_ep) }
#
#      create_display_entry(path, accs_ep.demarc, "E", row_ctr_z, nil)
#      create_display_entry(path, on_net_ep,           "D", row_ctr_z, "Right")
#      create_display_entry(path, on_net_ovc,       "D", row_ctr_z, nil) if row_ctr_z > row_ctr_a
#    end
#  end
#end


#def create_ls_off_net_ovc(order, uni, uni_vlan, enni, svc_model, ctr, eth_svc_type, cos_types, cir_kbps, cbs_kB, member_handles)
#  member_ntwk = OperatorNetwork.find(uni.operator_network_id)
#  member_ntwk_type = member_ntwk.operator_network_type
#
#
#  segment_type = member_ntwk_type.segment_types.first
#  path = Path.find(order.path_id)
#
#  if enni.operator_network_id.to_i != uni.operator_network_id.to_i
#    # demarcs not on the same network - can't link with OVC
#  end
#
#  # Create OVC
#  off_net_ovc = Factory.create(:off_net_ovc, :paths => [path],
#                                           :path_network_id => path.operator_network_id,
#                                           :operator_network => member_ntwk,
#                                           :segment_owner_role => "Seller",
#                                           :segment_type => segment_type,
#                                           :ethernet_service_type => eth_svc_type,
#                                           :use_member_attrs => !segment_type.member_attrs.blank?)
#  member_handles.each do |handle|
#    assign_member_handle(handle[:owner], off_net_ovc, "Segment", handle[:value])
#  end
#
#  # add the OVC to the order
#  order.ordered_entity = off_net_ovc
#  order.save!
#
#  create_display_entry(path, off_net_ovc, "F", ctr, nil)
#  create_display_entry(path, uni, "G", ctr, nil)
#  
#  off_net_segmentep_enni = Factory.create(:ovc_end_point_enni, :demarc => enni,
#                                                           :segment_end_point_type => member_ntwk_type.segment_end_point_types.find(:first, :conditions => "name LIKE 'ENNI%'"),
## delete this line                                         :stags => "UNI-ENNI".eql?(svc_model) ? get_next_stag(enni) : nil,
#                                                           :segment => off_net_ovc)
#  create_display_entry(path, off_net_segmentep_enni, "F", ctr, "Left")
#
#
#  off_net_segmentep_uni = Factory.create(:ovc_end_point_uni, :demarc => uni,
#                                                         :segment_end_point_type => member_ntwk_type.segment_end_point_types.find(:first, :conditions => "name LIKE 'UNI%'"),
#                                                         :ctags => uni_vlan,
#                                                         :is_monitored => true,
#                                                         :reflection_enabled => true,
#                                                         :md_level => "6",
#                                                         :md_format => "Y.1731 (ITU-T)",
#                                                         :ma_format => "icc-based (ITU-T)",
#                                                         :ma_name => "60000000",
#                                                         :segment => off_net_ovc)
#  create_display_entry(path, off_net_segmentep_uni, "F", ctr, "Right")
#
#
#  cos_types.each do |cos_type|
#    cos = Factory.create(:cos_instance, :class_of_service_type => cos_type,
#                                        :segment => off_net_ovc)
#
#    cos_type_condition = "class_of_service_type_id = #{cos_type.id}"
#    bwp_type_uni_ingress  = BwProfileType.find(:all, :conditions => "#{cos_type_condition} AND bwp_type = 'UNI-INGRESS'").first
#    bwp_type_uni_egress   = BwProfileType.find(:all, :conditions => "#{cos_type_condition} AND bwp_type = 'UNI-EGRESS'").first
#    bwp_type_enni_ingress = BwProfileType.find(:all, :conditions => "#{cos_type_condition} AND bwp_type = 'ENNI-INGRESS'").first
#    bwp_type_enni_egress  = BwProfileType.find(:all, :conditions => "#{cos_type_condition} AND bwp_type = 'ENNI-EGRESS'").first
#
#    member_cos_at_uni = Factory.create(:cos_end_point, :cos_instance => cos,
#                                                       :type => "CosEndPoint",
#                                                       :segment_end_point => off_net_segmentep_uni,
#                                                       :ingress_rate_limiting => "Policing", #bwp_type_uni_ingress.rate_limiting_mechanism,
#                                                       :egress_rate_limiting => "Policing", #bwp_type_uni_egress.rate_limiting_mechanism,
#                                                       :ingress_mapping => cos_type.cos_mapping_uni_ingress,
#                                                       :egress_marking => cos_type.cos_marking_uni_egress,
#                                                       :ingress_cir_kbps => cir_kbps,
#                                                       :ingress_cbs_kB => cbs_kB,
#                                                       :ingress_eir_kbps => "",
#                                                       :ingress_ebs_kB => "",
#                                                       :egress_eir_kbps => "",
#                                                       :egress_ebs_kB => "",
#                                                       :egress_cir_kbps => cir_kbps,
#                                                       :egress_cbs_kB => cbs_kB)
#
#    member_cos_at_enni = Factory.create(:cos_end_point, :cos_instance => cos,
#                                                        :type => "CosEndPoint",
#                                                        :segment_end_point => off_net_segmentep_enni,
#                                                        :ingress_rate_limiting => "Policing", #bwp_type_enni_ingress.rate_limiting_mechanism,
#                                                        :egress_rate_limiting => "Policing", #bwp_type_enni_egress.rate_limiting_mechanism,
#                                                        :ingress_mapping => cos_type.cos_mapping_uni_ingress,
#                                                        :egress_marking => cos_type.cos_marking_enni_egress,
#                                                        :ingress_cir_kbps => cir_kbps,
#                                                        :ingress_cbs_kB => cbs_kB,
#                                                        :ingress_eir_kbps => "",
#                                                        :ingress_ebs_kB => "",
#                                                        :egress_eir_kbps => "",
#                                                        :egress_ebs_kB => "",
#                                                        :egress_cir_kbps => cir_kbps,
#                                                        :egress_cbs_kB => cbs_kB)
#  end
#
#  return off_net_ovc
#end


#def create_ls_path(path_order, path_ntwk, handles)
#  path = Factory.create(:path, :cenx_path_type => "LightSquared Single Cell",
#                             :operator_network => path_ntwk)
#  handles.each do |handle|
#    assign_member_handle(handle[:owner], path, "Path", handle[:value])
#  end
#
#  # associate service order with Path...seems I need to do it both ways
#  path.service_orders.push(path_order)
#  path_order.path_id = path.id
#  path_order.save!
#
#  return path
#end


def create_sc_path(owner, market, due_date, svc_term, cell_id, uni_addr, accs_seller, uni_vlan, stype_name, cosnames, segment_type_name, cir_kbps, cbs_kB, bkhl_tunnels)
  # Get owner network
  path_owner = ServiceProvider.find_by_name(owner)
  path_ntwk = path_owner.operator_networks.first
  path_contacts = path_owner.contacts

  # Get objects from accs provider
  accs_seller = ServiceProvider.find_by_name(accs_seller)
  accs_ntwk = accs_seller.operator_networks.first
  accs_tap = load_balance_ennis(accs_ntwk.enni_news, cir_kbps)
  accs_stype = accs_ntwk.operator_network_type.ethernet_service_types.find(:first, :conditions => [ "name = ?", stype_name ])
  accs_segment_type = accs_stype.segment_types.find(:first, :conditions => [ "name = ?", segment_type_name ])
  accs_coses = []
  cosnames.each do |cosname|
    accs_coses.push(accs_stype.class_of_service_types.find(:first, :conditions => [ "name = ?", cosname ]))
  end
  svc_model = accs_tap.demarc_type.ls_access_solution_model

  # Create an STAG space for ENNI if needed
  if "UNI-ENNI".eql?(svc_model) and not STAG_SPACE[accs_tap]
    create_stag_space(accs_tap)
  end

  cenx_path_name = "#{market}/#{accs_seller.name}/Cell Site ##{uni_vlan}"
  cenx_offovc_name = "Access OVC:#{cenx_path_name}"
  cenx_onovc_name = "On Net OVC:#{cenx_path_name}"
  
  # Create Path
  hierarchical_name = "#{market}/#{accs_seller.name}/Cell Site ##{uni_vlan}"
  puts "\nCreating new Path #{hierarchical_name}..."
  path_order = place_path_order(path_ntwk, "Circuit Order:#{market}-#{accs_seller.name}-VLAN#{uni_vlan}", due_date)
  path_handles = [ { :owner => path_owner,   :value => cenx_path_name },
                  { :owner => accs_seller, :value => "#{market} Cell Site ##{uni_vlan}" } ]
#  path = create_ls_path(path_order, path_ntwk, path_handles)
  path = create_path(path_order, path_ntwk, "LightSquared Single Cell", path_handles)

#  path = Factory.create(:path, #:member_handle => cenx_path_name,
#                             :cenx_path_type => "LightSquared Single Cell",
#                             :operator_network => path_ntwk)
#  assign_member_handle(path_owner, path, "Path", cenx_path_name)
#  assign_member_handle(accs_seller, path, "Path", "#{market}/Cell Site ##{uni_vlan}")

#  # associate service order with Path...seems I need to do it both ways
#  path.service_orders.push(path_order)
#  path_order.path_id = path.id
#  path_order.save!

  puts "Creating UNI..."
  uni_order = place_uni_order(path_order, due_date, accs_ntwk, uni_addr, uni_vlan, path_ntwk, path_contacts)
  uni_handles = [ { :owner => path_owner,   :value => cell_id },
                  { :owner => accs_seller, :value => "#{market} Cell Site ##{uni_vlan}" } ]
  uni = create_uni(uni_order, path_owner, accs_ntwk, uni_addr, uni_handles)
  

  puts "Creating Access Circuit..."
  segment_order = place_off_net_ovc_order(path_order, due_date, accs_ntwk, path_contacts)
  offovc_handles = [ { :owner => path_owner,   :value => cenx_offovc_name },
                   { :owner => accs_seller, :value => "#{market}/Cell Site ##{uni_vlan}" } ]
#  accs_ckt = create_ls_off_net_ovc(segment_order, uni, uni_vlan, accs_tap, svc_model, 1, accs_stype, accs_coses, cir_kbps, cbs_kB, offovc_handles)
#  offovc_a = create_off_net_ovc(segment_a_order, "west", 1, uni_a, uni_a_vlan, ennis_a[rand(ennis_a.size)], segment_type_a, stype_a, costypes_a, cir_kbps, cbs_kB, offovc_a_handles)
  accs_ckt = create_off_net_ovc(segment_order, "east", 1, uni, uni_vlan, accs_tap, accs_segment_type, accs_stype, accs_coses, cir_kbps, cbs_kB, offovc_handles)


  puts "Creating On Net OVC..."
  on_net_ovc_order = place_on_net_ovc_order(path_order, due_date, accs_tap.pop, path_contacts)
  onovc_handles = [ { :owner => path_owner,   :value => cenx_onovc_name },
                    { :owner => accs_seller, :value => "#{market}/Cell Site ##{uni_vlan}" } ]
#  create_ls_on_net_ovc(on_net_ovc_order, uni_vlan, svc_model, cir_kbps, cbs_kB, [accs_ckt], bkhl_tunnels, onovc_handles)
  create_on_net_ovc(on_net_ovc_order, svc_model, nil, bkhl_tunnels, uni_vlan, [accs_ckt], cir_kbps, cbs_kB, onovc_handles)

  puts "Adding Tunnels to Path..."
  bkhl_tunnels.each do |tunnel|
    tunnel.paths.push(path)
  end

  return path
end


