def place_path_order(ntwk, name, due_date, prime, tester, tech, local)
  contacts = ntwk.service_provider.contacts

  order = BulkOrder.new
  order.title = name
  order.action = "New"
  order.bulk_order_type = "Circuit Order"
  order.ordered_entity_type = "Path"
  order.ordered_entity_subtype = ""
  order.order_created_date = "2010/11/11"
  order.order_received_date = "2011/01/01"
  order.operator_network_id = ntwk.id
  order.primary_contact_id = prime.id
  order.testing_contact_id = tester.id
  order.technical_contact_id = tech.id
  order.local_contact_id = local.id
  order.requested_service_date = due_date
  order.ordered_operator_network_id = ntwk.id

  order.save!

  return order
end


def place_uni_order(bulk_order, due_date, ntwk, address, vlan, order_ntwk, prime, tester, local)
  order = DemarcOrder.new
  order.action = "New"
  order.ordered_entity_type = "Demarc"
  order.ordered_entity_subtype = "UNI"
  order.title = "Uni Order for #{address}; VLAN #{vlan}"
  order.operator_network = ntwk
  order.ordered_entity_group_id = 0
  order.primary_contact_id = prime.id
  order.local_contact_id = local.id
  order.testing_contact_id = tester.id
  order.ordered_operator_network_id = order_ntwk
  order.order_created_date = "2010/11/11"
  order.order_received_date = "2012-01-01"
  order.requested_service_date = due_date
  order.save!
#  bulk_order.service_orders.push(order)

  return order
end


def place_on_net_ovc_order(bulk_order, due_date, pop, prime, tester, local)
  path = Path.find(bulk_order.path_id)
  path_ntwk = path.operator_network
  path_owner = path_ntwk.service_provider

  order = OnNetOvcOrder.new
  order.action = "New"
  order.ordered_entity_type = "Segment"
  order.ordered_entity_subtype = "OnNetOvc"
  order.title = "OnNetOvc Order for Path(#{path.cenx_id}) on #{pop.name} Pop"
  order.path = path
  order.operator_network = path_ntwk
  order.ordered_entity_group_id = 0
  order.primary_contact_id = prime.id
  order.local_contact_id = local.id
  order.testing_contact_id = tester.id
  order.ordered_operator_network_id = pop.operator_network.id
  order.order_created_date = "2010/11/11"
  order.order_received_date = "2012-01-01"
  order.requested_service_date = due_date
  order.save!
  bulk_order.service_orders.push(order)

  return order
end


def place_off_net_ovc_order(bulk_order, due_date, member_ntwk, member_demarc, prime, tester, local)
  path = Path.find(bulk_order.path_id)
  path_ntwk = path.operator_network
  path_owner = path_ntwk.service_provider
  member = member_ntwk.service_provider
  eth_svc_type = member_ntwk.operator_network_type.ethernet_service_types.first

  order_num = rand(1000)

  order = OffNetOvcOrder.new
  order.action = "New"
  order.ordered_entity_type = "Segment"
  order.ordered_entity_subtype = "OffNetOvc"
  order.title = "OffNetOVC Order for Path(#{path.cenx_id}) at Demarc(#{member_demarc.cenx_id}) in #{member_ntwk.name}"
  order.path = path
  order.operator_network = path_ntwk
  order.ordered_entity_group_id = 0
  order.primary_contact_id = prime.id
  order.local_contact_id = local.id
  order.testing_contact_id = tester.id
  order.ordered_operator_network_id = member_ntwk.id
  order.order_created_date = "2010/11/11"
  order.order_received_date = "2012-01-01"
  order.requested_service_date = due_date
  order.save!
  bulk_order.service_orders.push(order)

  return order
end
