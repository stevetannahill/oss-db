USS_NAME="US Signal"
USS_ACCS_NTWK_NAME="US Signal Chicago Network"
USS_ACCS_ESVC_NAME="US Signal Ethernet Backhaul"
USS_ACCS_COS_PRM_NAME="Premium"
USS_ACCS_COS_BEF_NAME="Best Effort"
USS_ACCS_UNITYPE_NAME="Alu eNodeB"
USS_ACCS_PHYTYPE_NAME="1000Base-LX"
USS_ACCS_OVCTYPE_NAME="US Signal Ethernet Backhaul OVC"

USS           = (defined? USS).nil?           ? ServiceProvider.find_by_name(USS_NAME) : USS
USS_ACCS_NTWK      = (defined? USS_ACCS_NTWK).nil?      ? USS.operator_networks.find_by_name(USS_ACCS_NTWK_NAME) : USS_ACCS_NTWK
USS_ACCS_NTWKTYPE  = (defined? USS_ACCS_NTWKTYPE).nil?  ? USS_ACCS_NTWK.operator_network_type : USS_ACCS_NTWKTYPE
USS_ACCS_ESVC      = (defined? USS_ACCS_ESVC).nil?      ? USS_ACCS_NTWKTYPE.ethernet_service_types.find_by_name(USS_ACCS_ESVC_NAME) : USS_ACCS_ESVC
USS_ACCS_COS_PRM   = (defined? USS_ACCS_COS_PRM).nil?   ? USS_ACCS_ESVC.class_of_service_types.find_by_name(USS_ACCS_COS_PRM_NAME) : USS_ACCS_COS_PRM
USS_ACCS_COS_BEF   = (defined? USS_ACCS_COS_BEF).nil?   ? USS_ACCS_ESVC.class_of_service_types.find_by_name(USS_ACCS_COS_BEF_NAME) : USS_ACCS_COS_BEF
USS_ACCS_UNITYPE   = (defined? USS_ACCS_UNITYPE).nil?   ? USS_ACCS_ESVC.demarc_types.find_by_name(USS_ACCS_UNITYPE_NAME) : USS_ACCS_UNITYPE
USS_ACCS_OVCTYPE   = (defined? USS_ACCS_OVCTYPE).nil?   ? USS_ACCS_ESVC.segment_types.find_by_name(USS_ACCS_OVCTYPE_NAME) : USS_ACCS_OVCTYPE

