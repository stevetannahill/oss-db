# Prevent state changes from accessing real hardware/SAM which would cause 
# state change failures
load "db/fake_go_in_service.rb"

PENDING = "Pending"
TESTING = "Testing"
READY = "Ready"
LIVE = "Live"
PROV_STATES = [ PENDING, TESTING, READY, LIVE ]


def conv_to_obj_state(str_state)
  case str_state
  when PENDING
    return ProvPending
  when TESTING
    return ProvTesting
  when READY
    return ProvReady
  when LIVE
    return ProvLive
  else
    return nil
  end
end


# return string version of the prov states the dependencies will allow
def get_state_limit(dependencies)
  puts "...#{dependencies.map { |d| "#{d.class}(#{d.cenx_id})" }}"
  min_prov_state = PROV_STATES[dependencies.empty? ? PROV_STATES.size-1 : dependencies.map { |d| d.get_prov_state.name }.map { |s| PROV_STATES.index(s) }.min]
  puts "...maximum prov state is #{min_prov_state}"
  return min_prov_state
end


def set_object_state(obj, state)
  candidate_state_index = PROV_STATES.index(state)
  current_state = obj.get_prov_state.name
  current_state_index = PROV_STATES.index(current_state)

  puts "Assign #{obj.class}(#{obj.cenx_id}) to #{state}..."
  puts "...current state is #{obj.get_prov_state.name}, index #{current_state_index}"
  puts "...pending state is #{state}, index #{candidate_state_index}"

  while ( candidate_state_index > current_state_index )
    pending_state_index = current_state_index + 1
    puts "...move #{obj.class} to \"#{PROV_STATES[pending_state_index]}\"..."
    pending_state = conv_to_obj_state(PROV_STATES[pending_state_index])

    if ProvPending.eql?(current_state) and obj.get_latest_order != nil
      current_order_state = obj.get_latest_order.get_order_state
      if current_order_state.is_a?(OrderCreated)
        [OrderReceived, OrderAccepted].each do |new_order_state|
          obj.get_latest_order.set_order_state(new_order_state)
        end
      end
    end

    puts "...pending_state is #{pending_state} and last service order is #{obj.get_latest_order}."
    if ProvTesting.eql?(pending_state) and obj.service_orders.last
      obj.get_latest_order.set_order_state(OrderDesignComplete)
    end
    if ProvLive.eql?(pending_state) and obj.service_orders.last
      obj.get_latest_order.set_order_state(OrderCustomerAccepted)
    end
    obj.set_prov_state(pending_state)
      
    current_state_index+=1
  end

  puts "...#{obj.class} provisioned as #{obj.get_prov_state.name}."
end


def assign_offovc_prov_states(thr_testing, thr_ready, thr_live, uni, segments)
  segments.each do |segment|
    puts "Determine final prov state for #{segment.class}(#{segment.cenx_id})..."
    ennis = segment.ovc_end_point_ennis.map(&:demarc).uniq
    # Re-calculate percentage thresholds for Segment provisioning states.
    # Thresholds set to the nth-root of the original value, where n is the number of Segments,
    # subject to the limits of what the ENNIs will allow
    max_segment_state_index = PROV_STATES.index(get_state_limit(ennis))
    thr_testing_asegments = max_segment_state_index >= PROV_STATES.index(TESTING) ? thr_testing ** (1.0/segments.size).to_f : 1.01
    thr_ready_asegments   = max_segment_state_index >= PROV_STATES.index(READY)   ? thr_ready   ** (1.0/segments.size).to_f : 1.01
    thr_live_asegments    = max_segment_state_index >= PROV_STATES.index(LIVE)    ? thr_live    ** (1.0/segments.size).to_f : 1.01

    p = rand
    puts "...test random value #{p} against test/ready/live thresholds of #{thr_testing_asegments}/#{thr_ready_asegments}/#{thr_live_asegments}"
    segment_target_state = PENDING
    case
    when p > thr_live_asegments
       segment_target_state = LIVE
    when p > thr_ready_asegments
       segment_target_state = READY
    when p > thr_testing_asegments
       segment_target_state = TESTING
    end
    puts "...target for final prov state is #{segment_target_state}"

    # assign UNI to random state of z_segment_target_state or better (up to LIVE)
    valid_uni_index_lo = PROV_STATES.index(segment_target_state)
    valid_uni_index_hi = PROV_STATES.size-1
    puts "...associated #{uni.class}(#{uni.cenx_id}) can be provisioned from #{PROV_STATES[valid_uni_index_lo]} to #{PROV_STATES[valid_uni_index_hi]}"

    valid_uni_states = (valid_uni_index_lo..valid_uni_index_hi).to_a
    target_uni_prov_state = PROV_STATES[valid_uni_states[rand(valid_uni_states.size)]]
    set_object_state(uni, target_uni_prov_state)

    # assign Off Net OVC to segment_target_state
    set_object_state(segment, segment_target_state)
  end
end


def assign_path_prov_states(thr_testing, thr_ready, thr_live, a_uni, a_segments, onovcs, z_segments, z_uni)
  assign_offovc_prov_states(thr_testing, thr_ready, thr_live, a_uni, a_segments)

  assign_offovc_prov_states(thr_testing, thr_ready, thr_live, z_uni, z_segments)

  # provision OnNetOvc as far as ENNIs will allow
  onovcs.each do |onovc|
    set_object_state(onovc, get_state_limit(onovc.on_net_ovc_end_point_ennis.map(&:demarc).uniq))
  end
end

