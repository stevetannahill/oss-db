require File.dirname(__FILE__) + '/seed_lsq_functions.rb'
require File.dirname(__FILE__) + '/seed_functions.rb'
require File.dirname(__FILE__) + '/cenx_definitions.rb'
#require File.dirname(__FILE__) + '/cox_definitions.rb'
#require File.dirname(__FILE__) + '/centurylink_definitions.rb'
#require File.dirname(__FILE__) + '/zayo_definitions.rb'
#require File.dirname(__FILE__) + '/level3_definitions.rb'
require File.dirname(__FILE__) + '/verizon_definitions.rb'
require File.dirname(__FILE__) + '/xo_definitions.rb'
require File.dirname(__FILE__) + '/covad_definitions.rb'
require File.dirname(__FILE__) + '/att_definitions.rb'
require File.dirname(__FILE__) + '/ussignal_definitions.rb'

CITIES=[ "Phoenix, AZ",
         "Scottsdale, AZ",
         "Mesa, AZ",
         "Chandler, AZ",
         "Avondale, AZ",
         "Peoria, AZ",
         "Surprise, AZ",
         "Casa Grande, AZ",
         "Tuscon, AZ",
         "Casas Adobes, AZ",
         "Catalina Foothills, AZ",
         "Sierra Vista, AZ",
         "Yuma, AZ",
         "Prescott, AZ",
         "Flagstaff, AZ",
         "Las Vegas, NV",
         "Henderson, NV",
         "St George, UT",
         "Provo, UT",
         "Sandy, UT",
         "Salt Lake City, UT",
         "Layton, UT",
         "Ogden, UT",
         "Grand Junction, CO",
         "Colorado Springs, CO",
         "Farmington, NM",
         "Santa Fe, NM",
         "Albuquerque, NM",
         "Alamogordo, NM",
         "Las Cruces, NM"
       ]

# MAINLINE
#
# From here on, we're creating our Paths.

# The network architecture includes six OVC Tunnels over Level 3's backhaul network.  I don't
# know a better (ie. more data-driven) way to retrieve them than retrieval by CENX ID.

# Set weightings for balancing Paths over Live vs. Not-Live backhaul tunnel pairs
TUNNEL_USE_WEIGHTS = { :live => 3, :ready => 1, :testing => 1, :pending => 1 }

CXID_PHX_TUNNEL_1 = "101263391637"
CXID_PHX_TUNNEL_2 = "101229804573"
CXID_PHX_TUNNEL_3 = "101261952999"
CXID_PHX_TUNNEL_4 = "101215514690"
CXID_PHX_TUNNEL_5 = "101211920023"
CXID_PHX_TUNNEL_6 = "101217453193"
CXID_CHI_TUNNEL_1 = "101224725719"
CXID_CHI_TUNNEL_2 = "101222761816"
CXID_CHI_TUNNEL_3 = "101246765171"
CXID_CHI_TUNNEL_4 = "101253234457"
CXID_CHI_TUNNEL_5 = "101231405196"
CXID_CHI_TUNNEL_6 = "101295899584"

# I'd like to set a couple of constants for the sake of brevity
LSQ = "LightSquared"
LSQ_NTWK="Nationwide LongHaul Network"

SPEEDS=[ 50000, 100000 ]
DDD="2011/08/25"
cvd_ctag=2100
att_ctag=2400
att_ctag=2700

ctr=1000

# The OVC tunnels are organized in protection pairs of 1&4, 2&5, and 3&6.
phxbkhl_tnls = get_weighted_bkhl_tunnel_pairs([[ CXID_PHX_TUNNEL_1, CXID_PHX_TUNNEL_4 ],
                                               [ CXID_PHX_TUNNEL_2, CXID_PHX_TUNNEL_5 ],
                                               [ CXID_PHX_TUNNEL_3, CXID_PHX_TUNNEL_6 ]])

chibkhl_tnls = get_weighted_bkhl_tunnel_pairs([[ CXID_CHI_TUNNEL_1, CXID_CHI_TUNNEL_4 ],
                                               [ CXID_CHI_TUNNEL_2, CXID_CHI_TUNNEL_5 ],
                                               [ CXID_CHI_TUNNEL_3, CXID_CHI_TUNNEL_6 ]])

#def unified_create_path(owner_name, owner_ntwk_name, owner_path_name, path_type,
#                       due_date, cir_kbps, cbs_kB, eir_kbps, ebs_kB,
#                       pop_name, on_net_stype_name, on_net_cosnames,
#                       a_segments, member_a_name, ntwk_a_name, segment_type_a_name, stype_a_name, cosnames_a,
#                               uni_type_a_name, uni_a_vlan, uni_a_phy, uni_a_id, uni_a_addr,
#                       z_segments, member_z_name, ntwk_z_name, segment_type_z_name, stype_z_name, cosnames_z,
#                               uni_type_z_name, uni_z_vlan, uni_z_phy, uni_z_id, uni_z_addr)

(0..1).each do |i|
#  uni_addr="#{random_street_address}, #{random_from_list(CITIES)}"
  uni_addr=XO_ACCS_ADDRS[i]
  unified_create_path(VZN, VZN_OWNR_NTWK, "Path #{ctr+=1}", LSQ_NCELL_Path,
                     DDD, SPEEDS[rand(SPEEDS.size)], 54, 0, 0,
                     XPH, CENX_MSO_ESVC, CENX_MSO_COS,
                     load_balance_tunnel_pairs(phxbkhl_tnls), VZN, VZN_BKHL_NTWK, VZN_BKHL_OVCTYPE, VZN_BKHL_ESVC, VZN_BKHL_COS,
                          VZN_BKHL_UNITYPE, att_ctag+=1, VZN_BKHL_PHYTYPE, nil, nil,
                     nil, XO, XO_ACCS_NTWK, XO_ACCS_OVCTYPE, XO_ACCS_ESVC, XO_ACCS_COS,
                          XO_ACCS_UNITYPE, att_ctag, XO_ACCS_PHYTYPE, gen_uni_id(uni_addr), uni_addr)

  uni_addr=CVD_ACCS_ADDRS[i]
  unified_create_path(VZN, VZN_OWNR_NTWK, "Path #{ctr+=1}", LSQ_NCELL_Path,
                     DDD, SPEEDS[rand(SPEEDS.size)], 54, 0, 0,
                     XPH, CENX_MSO_ESVC, CENX_MSO_COS,
                     load_balance_tunnel_pairs(phxbkhl_tnls), VZN, VZN_BKHL_NTWK, VZN_BKHL_OVCTYPE, VZN_BKHL_ESVC, VZN_BKHL_COS,
                          VZN_BKHL_UNITYPE, cvd_ctag+=1, VZN_BKHL_PHYTYPE, nil, nil,
                     nil, CVD, CVD_ACCS_NTWK, CVD_ACCS_OVCTYPE, CVD_ACCS_ESVC, CVD_ACCS_COS,
                          CVD_ACCS_UNITYPE, cvd_ctag, CVD_ACCS_PHYTYPE, gen_uni_id(uni_addr), uni_addr)

  uni_addr=ATT_ACCS_ADDRS[i]
  unified_create_path(VZN, VZN_OWNR_NTWK, "Path #{ctr+=1}", LSQ_NCELL_Path,
                     DDD, SPEEDS[rand(SPEEDS.size)], 54, 0, 0,
                     XPH, CENX_MSO_ESVC, CENX_MSO_COS,
                     load_balance_tunnel_pairs(phxbkhl_tnls), VZN, VZN_BKHL_NTWK, VZN_BKHL_OVCTYPE, VZN_BKHL_ESVC, VZN_BKHL_COS,
                          VZN_BKHL_UNITYPE, att_ctag+=1, VZN_BKHL_PHYTYPE, nil, nil,
                     nil, ATT, ATT_ACCS_NTWK, ATT_ACCS_OVCTYPE, ATT_ACCS_ESVC, ATT_ACCS_COS,
                          ATT_ACCS_UNITYPE, att_ctag, ATT_ACCS_PHYTYPE, gen_uni_id(uni_addr), uni_addr)


  uni_addr="#{random_street_address}, #{random_from_list(CITIES)}"
  unified_create_path(VZN, VZN_OWNR_NTWK, "Path #{ctr+=1}", LSQ_NCELL_Path,
                     DDD, SPEEDS[rand(SPEEDS.size)], 54, 0, 0,
                     XCH, CENX_MSO_ESVC, CENX_MSO_COS,
                     load_balance_tunnel_pairs(chibkhl_tnls), VZN, VZN_BKHL_NTWK, VZN_BKHL_OVCTYPE, VZN_BKHL_ESVC, VZN_BKHL_COS,
                          VZN_BKHL_UNITYPE, att_ctag+=1, VZN_BKHL_PHYTYPE, nil, nil,
                     nil, USS, USS_ACCS_NTWK, USS_ACCS_OVCTYPE, USS_ACCS_ESVC, USS_ACCS_COS,
                          USS_ACCS_UNITYPE, att_ctag, USS_ACCS_PHYTYPE, gen_uni_id(uni_addr), uni_addr)
end

