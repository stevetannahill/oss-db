LV3_NAME="Level 3 Communications"
LV3_BKHL_NTWK_NAME="Nationwide LongHaul Network"
LV3_BKHL_ESVC_NAME="Wavelength Service"
LV3_BKHL_COS_NAMES=["Wavelength"]
LV3_BKHL_UNITYPE_NAME="DWDM Handoff"
LV3_BKHL_PHYTYPE_NAME="10GBase-LR"
LV3_BKHL_OVCTYPE_NAME="Wavelength Service OVC"

LV3           = (defined? LV3).nil?           ? ServiceProvider.find_by_name(LV3_NAME) : LV3
LV3_BKHL_NTWK      = (defined? LV3_BKHL_NTWK).nil?      ? LV3.operator_networks.find_by_name(LV3_BKHL_NTWK_NAME) : LV3_BKHL_NTWK
LV3_BKHL_NTWKTYPE  = (defined? LV3_BKHL_NTWKTYPE).nil?  ? LV3_BKHL_NTWK.operator_network_type : LV3_BKHL_NTWKTYPE
LV3_BKHL_ESVC      = (defined? LV3_BKHL_ESVC).nil?      ? LV3_BKHL_NTWKTYPE.ethernet_service_types.find_by_name(LV3_BKHL_ESVC_NAME) : LV3_BKHL_ESVC
LV3_BKHL_COSES     = (defined? LV3_BKHL_COSES).nil?     ? LV3_BKHL_ESVC.class_of_service_types.find(:all, :conditions => [ "name IN (?)", LV3_BKHL_COS_NAMES ]) : LV3_BKHL_COSES
LV3_BKHL_UNITYPE   = (defined? LV3_BKHL_UNITYPE).nil?   ? LV3_BKHL_ESVC.demarc_types.find_by_name(LV3_BKHL_UNITYPE_NAME) : LV3_BKHL_UNITYPE
LV3_BKHL_OVCTYPE   = (defined? LV3_BKHL_OVCTYPE).nil?   ? LV3_BKHL_ESVC.segment_types.find_by_name(LV3_BKHL_OVCTYPE_NAME) : LV3_BKHL_OVCTYPE

