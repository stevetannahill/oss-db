LSQ_NAME="LightSquared"
LSQ_NTWK_NAME="Nationwide LTE Network"

LSQ           = (defined? LSQ).nil?           ? ServiceProvider.find_by_name(LSQ_NAME) : LSQ
LSQ_NTWK      = (defined? LSQ_NTWK).nil?      ? LSQ.operator_networks.find_by_name(LSQ_NTWK_NAME) : LSQ_NTWK

