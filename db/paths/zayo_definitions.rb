ZYO_NAME="Zayo"
ZYO_ACCS_NTWK_NAME="Zayo Las Vegas Network"
ZYO_ACCS_ESVC_NAME="Dedicated Ethernet"
ZYO_ACCS_COS_NAMES=["Real Time Class"]
ZYO_ACCS_UNITYPE_NAME="Dedicated Ethernet - UNI (for Light Squared)"
ZYO_ACCS_PHYTYPE_NAME="1000Base-LX"
ZYO_ACCS_OVCTYPE_NAME="Dedicated Ethernet OVC"

ZYO           = (defined? ZYO).nil?           ? ServiceProvider.find_by_name(ZYO_NAME) : ZYO
ZYO_ACCS_NTWK      = (defined? ZYO_ACCS_NTWK).nil?      ? ZYO.operator_networks.find_by_name(ZYO_ACCS_NTWK_NAME) : ZYO_ACCS_NTWK
ZYO_ACCS_NTWKTYPE  = (defined? ZYO_ACCS_NTWKTYPE).nil?  ? ZYO_ACCS_NTWK.operator_network_type : ZYO_ACCS_NTWKTYPE
ZYO_ACCS_ESVC      = (defined? ZYO_ACCS_ESVC).nil?      ? ZYO_ACCS_NTWKTYPE.ethernet_service_types.find_by_name(ZYO_ACCS_ESVC_NAME) : ZYO_ACCS_ESVC
ZYO_ACCS_COSES     = (defined? ZYO_ACCS_COSES).nil?     ? ZYO_ACCS_ESVC.class_of_service_types.find(:all, :conditions => [ "name IN (?)", ZYO_ACCS_COS_NAMES ]) : ZYO_ACCS_COSES
ZYO_ACCS_UNITYPE   = (defined? ZYO_ACCS_UNITYPE).nil?   ? ZYO_ACCS_ESVC.demarc_types.find_by_name(ZYO_ACCS_UNITYPE_NAME) : ZYO_ACCS_UNITYPE
ZYO_ACCS_OVCTYPE   = (defined? ZYO_ACCS_OVCTYPE).nil?   ? ZYO_ACCS_ESVC.segment_types.find_by_name(ZYO_ACCS_OVCTYPE_NAME) : ZYO_ACCS_OVCTYPE

ZYO_ACCS_ADDRS = [ "200 E Fremont Street, Las Vegas",
                   "6620 W. Flamingo, Las Vegas",
                   "8180 W. Russell, Las Vegas",
                   "5868 S. Eastern Ave, Las Vegas",
                   "3280 N. Cimarron Road, Las Vegas",
                   "4375 E Sahara, Las Vegas",
                   "8650 W.Tropicana, Las Vegas",
                   "4975 Industrial Road, Las Vegas",
                   "4897 Smoke Ranch Rd., Las Vegas",
                   "1640 E. Flamingo Rd., Las Vegas",
                   "8485 W. Sunset Road, Las Vegas",
                   "3560 South Jones Blvd, Las Vegas",
                   "2110 Fremont St., Las Vegas",
                   "4120 E. Craig Rd., Las Vegas",
                   "4530 Nevso, Las Vegas",
                   "5011 East Charleston Blvd., Las Vegas",
                   "1900 N. Nellis Blvd., Las Vegas",
                   "2120 Paradise Road, Las Vegas",
                   "3304 Coleman Street, Las Vegas",
                   "2600 E. Flamingo Road, Las Vegas",
                   "4337 N. Las Vegas Blvd, Las Vegas",
                   "2499 N. Jones, Las Vegas",
                   "2740 Losee Road, North Las Vegas",
                   "6045 Hauck St., Las Vegas",
                   "4130 Solteros St, Las Vegas",
                   "1550 Helm Drive, Las Vegas",
                   "2321 W. Desert Inn Road, Las Vegas",
                   "3400 Paradise Road, Las Vegas",
                   "3478 S. Valley view, Las Vegas",
                   "4220 Donovan Way, Las Vegas",
                   "2020 Gold Ring Drive, Las Vegas",
                   "2716 N. Tenaya, Las Vegas",
                   "1002 S. Durango Drive, Las Vegas",
                   "3011 E. Desert Inn, Las Vegas",
                   "6380 S Annie Oakley, Henderson",
                   "4440 E. Washington, Las Vegas",
                   "1000 1030 Stephanie Place, Henderson",
                   "9140 W. Russell Rd, Las Vegas",
                   "2303 E. Sahara, Las Vegas",
                   "9350 w. Flamingo Rd., Las Vegas",
                   "2415 N Martin Luther King Blvd, Las Vegas",
                   "1200 N. Eastern, Las Vegas",
                   "3440 E Russell Raod, Las Vegas",
                   "160 E Flamingo, Las Vegas",
                   "200 Spectrum, Las Vegas" ]
