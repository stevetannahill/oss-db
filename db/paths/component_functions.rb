def find_cos_endpoint(segmentep, pbit)
  cos_ep = nil
  cos_eps = segmentep.cos_end_points

  # prefer an egress marking with the relevant pbit
  cos_ep = cos_eps.find(:first, :conditions => [ "egress_marking LIKE '%?%'", pbit ])

  # if none, then look for an ingress mapping including the pbit and a marking of Preserve
  if cos_ep.nil?
    cos_ep = cos_eps.find(:first, :conditions => [ "egress_marking = 'Preserve' AND ingress_mapping LIKE '%?%'", pbit ])
  end

  # if none, then look for an ingress mapping of all and a marking of Preserve
  if cos_ep.nil?
    cos_ep = cos_eps.find(:first, :conditions => "egress_marking = 'Preserve' AND ingress_mapping = '*'")
  end

  # if none, then look for an ingress mapping of all and a marking of Preserve
  if cos_ep.nil?
    cos_ep = cos_eps.find(:first, :conditions => "egress_marking = 'Copy C-TAG PCP' AND ingress_mapping = '*'")
  end

  return cos_ep
end


def create_path(path_order, path_ntwk, path_type)
  path = Factory.create(:path, :cenx_path_type => path_type,
                             :operator_network => path_ntwk)

  # associate service order with Path...seems I need to do it both ways
  path.service_orders.push(path_order)
  path_order.path_id = path.id
  path_order.save!

  return path
end


# TODO: a better way of determining the connected device type
def create_uni(order, path_owner, ntwk, uni_type, address)
  random_port_names = [ "1-1", "1/1/1" ]
  uni_type = ntwk.operator_network_type.uni_types.first

  case ntwk.service_provider.name
    when "AT&T"
      cdt="Ericcson SE-800"
    when "CenturyLink"
      cdt="RAD ETX-202A"
    when "Charter Communications"
      cdt="Hatteras"
    when "Covad"
      cdt="FWS"
    when "Cox"
      cdt="ADVA FSP150cc-825"
    when "Sprint/Ericsson"
      cdt="Ericcson SE-800"
    when "Time Warner Cable"
      cdt="ALU 6250 -8M"
    when "US Signal"
      cdt="ALU7705"
    when "XO Communications"
      cdt="RAD ETX-202A"
    else
      cdt="TBD"
  end

  member_uni = Factory.create(:uni_full, :address => address,
                                         :operator_network => ntwk,
                                         :demarc_type => uni_type,
                                         :cenx_demarc_type => "UNI",
                                         :port_name => random_port_names[rand(random_port_names.size)],
                                         :reflection_mechanism => "DMM/DMR",
                                         :mon_loopback_address => random_mac,
                                         :connected_device_type => cdt,
                                         :demarc_icon => "cell site",
                                         :physical_medium => "1000Base-T")

  # add uni to assocated order
  order.ordered_entity = member_uni
  order.save!
  
  return member_uni.reload
end


def create_segment_end_point(ntwk_type, segment, demarc, demarc_vlan, is_monitored)

  endpoint = nil

  if demarc.is_a?(EnniNew)
    endpoint = Factory.create(:ovc_end_point_enni, :demarc => demarc,
                                               :is_monitored => false,
                                               :segment_end_point_type => ntwk_type.segment_end_point_types.find(:first, :conditions => "name LIKE '%ENNI%'"),
                                               :segment => segment)

  elsif demarc.is_a?(Uni)
    endpoint =  Factory.create(:ovc_end_point_uni, :demarc => demarc,
                                              :is_monitored => false,
                                              :segment_end_point_type => ntwk_type.segment_end_point_types.find(:first, :conditions => "name LIKE '%UNI%'"),
                                              :ctags => demarc_vlan,
                                              #:is_monitored => true,
                                              #:reflection_enabled => true,
                                              #:md_level => "6",
                                              #:md_format => "Y.1731 (ITU-T)",
                                              #:ma_format => "icc-based (ITU-T)",
                                              #:ma_name => "60000000",
                                              :segment => segment)
  end

  if not endpoint.nil? and is_monitored
    endpoint.is_monitored = true
    endpoint.reflection_enabled = true
    endpoint.md_level = "6"
    endpoint.md_format = "Y.1731 (ITU-T)"
    endpoint.ma_format = "icc-based (ITU-T)"
    endpoint.ma_name = "60000000"

    endpoint.save!
  end

  return endpoint
end


def create_off_net_ovc(order, accs_demarc, accs_demarc_vlan, ntwk_demarc, ntwk_demarc_vlan, segment_type, eth_svc_type, cos_types, cos_bws)
  member_ntwk = OperatorNetwork.find(accs_demarc.operator_network_id)
  member_ntwk_type = member_ntwk.operator_network_type
  member = member_ntwk.service_provider
  member_contacts = member.contacts

  path = Path.find(order.path_id)
  path_owner = path.operator_network.service_provider

  if ntwk_demarc.operator_network_id.to_i != accs_demarc.operator_network_id.to_i
    # demarcs not on the same network - can't link with Segment
  end

  # Create Segment
  off_net_ovc = Factory.create(:off_net_ovc, :paths => [path],
                                           :path_network_id => path.operator_network_id,
                                           :operator_network => member_ntwk,
                                           :segment_owner_role => (path_owner.id == member.id) ? "Buyer" : "Seller",
                                           :segment_type => segment_type,
                                           :ethernet_service_type => eth_svc_type,
                                           :emergency_contact => member_contacts[rand(member_contacts.length)],
                                           :use_member_attrs => !segment_type.member_attrs.blank?)

  # add the Segment to the order
  order.ordered_entity = off_net_ovc
  order.save!

  # create Segment endpoints
  off_net_segmentep_enni = create_segment_end_point(member_ntwk_type, off_net_ovc, ntwk_demarc, ntwk_demarc_vlan, false)
  off_net_segmentep_uni = create_segment_end_point(member_ntwk_type, off_net_ovc, accs_demarc, accs_demarc_vlan, true)

  (0..cos_types.length-1).each do |cos_index|
    cos_type = cos_types[cos_index]
    cos_bw = cos_bws[cos_index].nil? ? cos_bws[0] : cos_bws[cos_index]

    cos = Factory.create(:cos_instance, :class_of_service_type => cos_type,
                                        :segment => off_net_ovc)

    cos_type_condition = "class_of_service_type_id = #{cos_type.id}"
    bwp_type_uni_ingress  = BwProfileType.find(:all, :conditions => "#{cos_type_condition} AND bwp_type = 'UNI-INGRESS'").first
    bwp_type_uni_egress   = BwProfileType.find(:all, :conditions => "#{cos_type_condition} AND bwp_type = 'UNI-EGRESS'").first
    bwp_type_enni_ingress = BwProfileType.find(:all, :conditions => "#{cos_type_condition} AND bwp_type = 'ENNI-INGRESS'").first
    bwp_type_enni_egress  = BwProfileType.find(:all, :conditions => "#{cos_type_condition} AND bwp_type = 'ENNI-EGRESS'").first

    valid_mapping = ("1".."7").to_a << "*"
    valid_marking = ("1".."7").to_a << "-"
    mapping = cos_type.cos_mapping_uni_ingress.split(';').shuffle.first
    unless valid_mapping.include? mapping
      mapping = "5"
    end
    marking = cos_type.cos_marking_uni_egress
    unless valid_marking.include? marking                             
      marking = "5"                                                             
    end

    member_cos_at_uni = Factory.create(:cos_end_point, :cos_instance => cos,
                                                       :type => "CosEndPoint",
                                                       :segment_end_point => off_net_segmentep_uni,
                                                       :ingress_rate_limiting => "Policing", #bwp_type_uni_ingress.rate_limiting_mechanism,
                                                       :egress_rate_limiting => "Policing", #bwp_type_uni_egress.rate_limiting_mechanism,
                                                       :ingress_mapping => mapping,
                                                       :egress_marking => marking, 
                                                       :ingress_cir_kbps => cos_bw[:cir].to_s,
                                                       :ingress_cbs_kB => cos_bw[:cbs].to_s,
                                                       :ingress_eir_kbps => cos_bw[:eir].to_s,
                                                       :ingress_ebs_kB => cos_bw[:ebs].to_s,
                                                       :egress_eir_kbps => cos_bw[:eir].to_s,
                                                       :egress_ebs_kB => cos_bw[:ebs].to_s,
                                                       :egress_cir_kbps => cos_bw[:cir].to_s,
                                                       :egress_cbs_kB => cos_bw[:cbs].to_s)

    mapping = cos_type.cos_mapping_enni_ingress.split(';').shuffle.first
    unless valid_mapping.include? mapping
      mapping = "5"
    end
    marking = cos_type.cos_marking_enni_egress
    unless valid_marking.include? marking                             
      marking = "5"                                                             
    end
    puts "cos_type.cos_marking_enni_egress is #{cos_type.cos_marking_enni_egress}"
    member_cos_at_enni = Factory.create(:cos_end_point, :cos_instance => cos,
                                                        :type => "CosEndPoint",
                                                        :segment_end_point => off_net_segmentep_enni,
                                                        :ingress_rate_limiting => "Policing", #bwp_type_enni_ingress.rate_limiting_mechanism,
                                                        :egress_rate_limiting => "Policing", #bwp_type_enni_egress.rate_limiting_mechanism,
                                                        # commenting out result of above mapping/marking logic
                                                        :ingress_mapping => mapping,
                                                        :egress_marking => marking,
                                                        #:ingress_mapping => "*",
                                                        #:egress_marking => cos_type.cos_marking_enni_egress,
                                                        :ingress_cir_kbps => cos_bw[:cir].to_s,
                                                        :ingress_cbs_kB => cos_bw[:cbs].to_s,
                                                        :ingress_eir_kbps => cos_bw[:eir].to_s,
                                                        :ingress_ebs_kB => cos_bw[:ebs].to_s,
                                                        :egress_eir_kbps => cos_bw[:eir].to_s,
                                                        :egress_ebs_kB => cos_bw[:ebs].to_s,
                                                        :egress_cir_kbps => cos_bw[:cir].to_s,
                                                        :egress_cbs_kB => cos_bw[:cbs].to_s)
  end

  return off_net_ovc.reload
end


# TODO: Ultimately the number of cos_maps on a side should match the number of offovcs - and should be a paired list
# TODO: Validate cos maps - a cos on one side must be on the other side
def create_on_net_ovc(order, on_net_esvc_name, on_net_cos_names, uni_a_vlan, retag_a, offovcs_a, cos_bws_a, cos_maps_a, cos_maps_z, cos_bws_z, offovcs_z, retag_z, uni_z_vlan)#, cir_kbps, cbs_kB, eir_kbps, ebs_kB)

  pop = offovcs_a.first.ovc_end_point_ennis.first.demarc.pop

  operator_network = pop.operator_network
  cenx_ntwk_type = operator_network.operator_network_type
  cenx_folks = operator_network.service_provider.contacts
  eth_svc_type = cenx_ntwk_type.ethernet_service_types.find_by_name(on_net_esvc_name)
  path = Path.find(order.path_id)

  on_net_ovc = Factory.create(:on_net_ovc, :paths => [path],
                                           :path_network_id => path.operator_network.id,
                                           :pop => pop,
                                           :emergency_contact => cenx_folks[rand(cenx_folks.length)],
                                           #:service_id => IdTools::ServiceIdGenerator.generate_on_net_ovc_service_id(pop),
                                           :ethernet_service_type => eth_svc_type)

  order.ordered_entity = on_net_ovc
  order.save!

  # build cos infrastructure - create a cos instance for each type in the map
  a_cos_map_passed_in = (not cos_maps_a.nil?)
  unless a_cos_map_passed_in
    cos_maps_a = [{ eth_svc_type.class_of_service_types.find_by_name("Real Time") => { :pcp => [5] } }]
  end

  z_cos_map_passed_in = (not cos_maps_z.nil?)
  unless a_cos_map_passed_in
    cos_maps_z = [{ eth_svc_type.class_of_service_types.find_by_name("Real Time") => { :pcp => [5] } }]
  end

  onovc_coses = {}  # we'll keep a hash of cos types with their instance to avoid duplicates


  offovcs_a.each do |a_segment|
    a_segment.ovc_end_point_ennis.each do |a_ep|
      tag_mapping = a_ep.demarc.demarc_type.ls_access_solution_model

      #retag_a = 12
      stag = nil
      ctag = nil
      if "UNI-UNI".eql?(tag_mapping)
        stags = uni_a_vlan
        ctags = "*"

      elsif "UNI-ENNI".eql?(tag_mapping)
        puts "RETAG_A is <#{retag_a}>"
        stags = retag_a.nil? ? get_next_stag(a_ep.demarc) : retag_a
        ctags = uni_a_vlan

      elsif "N/A".eql?(tag_mapping)
        stags = uni_a_vlan

      else
        puts "Demarc(#{a_ep.demarc.cenx_id}).ls_access_solution_model value of <#{tag_mapping}> is not recognized!!!!!"
        stags = retag_a.nil? ? get_next_stag(a_ep.demarc) : retag_a
        ctags = uni_a_vlan
      end

      # create On Net Segment End Point, and associate with its peer on the
      # backhaul Segment
      on_net_ep = Factory.create(:on_net_ovc_end_point_enni, :demarc => a_ep.demarc,
                                                      :segment_end_point_type => cenx_ntwk_type.segment_end_point_types.find(:first, :conditions => "name LIKE '%ENNI%'"),
                                                      :stags => stags,
                                                      :ctags => ctags,
                                                      :segment => on_net_ovc)
      on_net_ep.segment_end_points.push(a_ep)


      # create attached CoS end point for each pbit in cos map,
      # and associate with its peer on the backhaul Segment End Point
      (0..cos_maps_a.length-1).each do |cos_index|
      #cos_maps_a.keys.each do |a_cos_type|
        a_cos_type = cos_maps_a[cos_index].keys.first
        cos = onovc_coses[a_cos_type]
        if cos.nil?
          cos = Factory.create(:cenx_cos_instance, :class_of_service_type => a_cos_type,
                                                   :segment => on_net_ovc)
          onovc_coses[a_cos_type] = cos
        end

        cos_type_condition = "class_of_service_type_id = #{a_cos_type.id}"
        bwp_type_enni_ingress = BwProfileType.find(:all, :conditions => "#{cos_type_condition} AND bwp_type = 'ENNI-INGRESS'").first

        cos_prop = cos_maps_a[cos_index][a_cos_type]
        cos_bw = cos_bws_a[cos_index].nil? ? cos_bws_a[0] : cos_bws_a[cos_index]
        cos_prop[:pcp].each do |pcp|

          on_net_ep_cos = Factory.create(:cos_end_point, :cos_instance => cos,
                                                    :type => "CenxCosEndPoint",
                                                    :segment_end_point => on_net_ep,
                                                    :ingress_rate_limiting => "Policing",
                                                    :egress_rate_limiting => "Policing", # supposed to be None, but getting errors
                                                    :ingress_mapping =>  pcp.to_s,
                                                    :egress_marking =>   pcp.to_s,
#                                                    :egress_marking => "5", # default for LSQ - would like a better way to get this value
                                                    :ingress_cir_kbps => cos_bw[:cir],
                                                    :ingress_cbs_kB =>   cos_bw[:cbs],
                                                    :ingress_eir_kbps => cos_bw[:eir],
                                                    :ingress_ebs_kB =>   cos_bw[:ebs],
                                                    :egress_eir_kbps =>  cos_bw[:eir],
                                                    :egress_ebs_kB =>    cos_bw[:ebs],
                                                    :egress_cir_kbps =>  cos_bw[:cir],
                                                    :egress_cbs_kB =>    cos_bw[:cbs])

          if a_cos_map_passed_in
            connected_cos_ep = find_cos_endpoint(a_ep, pcp)
            if connected_cos_ep.nil?
              puts "No CoS end point found for A Endpoint(#{a_ep}:#{a_ep.cenx_id}) with a pbit of #{pcp}."
            end
            on_net_ep_cos.cos_end_points.push(connected_cos_ep)
          else
            a_ep.cos_end_points.each { |cos_ep| on_net_ep_cos.cos_end_points.push(cos_ep) }
          end

        end # a_pcp
      end # a_ep
    end # a_segment
  end # a_cos_type


  offovcs_z.each do |z_segment|
    z_segment.ovc_end_point_ennis.each do |z_ep|
      tag_mapping = z_ep.demarc.demarc_type.ls_access_solution_model

      #retag_z = 12
      stag = nil
      ctag = nil
      if "UNI-UNI".eql?(tag_mapping)
        stags = uni_z_vlan
        ctags = "*"

      elsif "UNI-ENNI".eql?(tag_mapping)
        puts "RETAG_Z is <#{retag_z}>"
        stags = retag_z.nil? ? get_next_stag(z_ep.demarc) : retag_z
        ctags = uni_z_vlan

      elsif "N/A".eql?(tag_mapping)
        stags = uni_z_vlan

      else
        puts "Demarc(#{z_ep.demarc.cenx_id}).ls_access_solution_model value of <#{tag_mapping}> is not recognized!!!!!"
        stags = retag_z.nil? ? get_next_stag(z_ep.demarc) : retag_z
        ctags = uni_z_vlan
      end

      # create On Net Segment End Point, and associate with its peer on the
      # access Segment
      on_net_ep = Factory.create(:on_net_ovc_end_point_enni, :demarc => z_ep.demarc,
                                                      :segment_end_point_type => cenx_ntwk_type.segment_end_point_types.find(:first, :conditions => "name LIKE '%ENNI%'"),
                                                      :stags => stags,
                                                      :ctags => ctags,
                                                      :segment => on_net_ovc)
      on_net_ep.segment_end_points.push(z_ep)

      # create attached CoS end point for each pbit in cos map
      # and associate with its peer on the access Segment End Point
      (0..cos_maps_z.length-1).each do |cos_index|
      #cos_maps_z.keys.each do |z_cos_type|
        z_cos_type = cos_maps_z[cos_index].keys.first
        cos = onovc_coses[z_cos_type]
        if cos.nil?
          cos = Factory.create(:cenx_cos_instance, :class_of_service_type => z_cos_type,
                                                   :segment => on_net_ovc)
          onovc_coses[z_cos_type] = cos
        end

        cos_type_condition = "class_of_service_type_id = #{z_cos_type.id}"
        bwp_type_enni_ingress = BwProfileType.find(:all, :conditions => "#{cos_type_condition} AND bwp_type = 'ENNI-INGRESS'").first

        cos_prop = cos_maps_z[cos_index][z_cos_type]
        cos_bw = cos_bws_z[cos_index].nil? ? cos_bws_z[0] : cos_bws_z[cos_index]

        cos_prop[:pcp].each do |pcp|
###          valid_mapping = ("1".."7").to_a << "*"
###          valid_marking = ("1".."7").to_a << "-"
###          marking = z_ep.cos_end_points.first.class_of_service_type.cos_marking_uni_egress
###          unless valid_marking.include? marking                             
###            marking = "5"                                                             
###          end
          on_net_ep_cos = Factory.create(:cos_end_point, :cos_instance => cos,
                                                    :type => "CenxCosEndPoint",
                                                    :segment_end_point => on_net_ep,
                                                    :ingress_rate_limiting => "Policing",
                                                    :egress_rate_limiting => "Policing",
                                                    :ingress_mapping =>  pcp.to_s,
                                                    :egress_marking =>   pcp.to_s,
                                                    #:egress_marking => marking,
                                                    :ingress_cir_kbps => cos_bw[:cir],
                                                    :ingress_cbs_kB =>   cos_bw[:cbs],
                                                    :ingress_eir_kbps => cos_bw[:eir],
                                                    :ingress_ebs_kB =>   cos_bw[:ebs],
                                                    :egress_eir_kbps =>  cos_bw[:eir],
                                                    :egress_ebs_kB =>    cos_bw[:ebs],
                                                    :egress_cir_kbps =>  cos_bw[:cir],
                                                    :egress_cbs_kB =>    cos_bw[:cbs])

          if z_cos_map_passed_in
            connected_cos_ep = find_cos_endpoint(z_ep, pcp)
            if connected_cos_ep.nil?
              puts "No CoS end point found for A Endpoint(#{z_ep}:#{z_ep.cenx_id}) with a pbit of #{pcp}."
            end
            on_net_ep_cos.cos_end_points.push(connected_cos_ep)
          else
            z_ep.cos_end_points.each { |cos_ep| on_net_ep_cos.cos_end_points.push(cos_ep) }
          end

        end # z_pbit
      end # z_ep
    end # z_segment
  end # z_cos_type


  # create monitoring endpoint
  monitoring_enni = pop.demarcs.find_by_lag_id(199)
  on_net_ep = Factory.create(:on_net_ovc_end_point_enni, :demarc => monitoring_enni,
                                                  :segment_end_point_type => cenx_ntwk_type.segment_end_point_types.find(:first, :conditions => "name LIKE '%ENNI%'"),
                                                  :stags => "*",
                                                  :ctags => "*",
                                                  :segment => on_net_ovc)
  rez, on_net_ep.stags = on_net_ep.suggest_valid_stag
  on_net_ep.save!
  on_net_ep.segment_end_points.push(on_net_ep)
  cenx_cos_type_rt = eth_svc_type.class_of_service_types.find_by_name("Real Time")
  cenx_cos_rt = onovc_coses[cenx_cos_type_rt]
  if cenx_cos_rt.nil?
    cenx_cos_rt = Factory.create(:cenx_cos_instance, :class_of_service_type => cenx_cos_type_rt,
                                                     :segment => on_net_ovc)
  end
  on_net_ep_cos = Factory.create(:cos_end_point, :cos_instance => cenx_cos_rt,
                                            :type => "CenxCosEndPoint",
                                            :segment_end_point => on_net_ep,
                                            :ingress_rate_limiting => "Policing",
                                            :egress_rate_limiting => "Policing",
                                            :ingress_mapping =>  "5",
                                            :egress_marking =>   "5",
                                            :ingress_cir_kbps => "1000",
                                            :ingress_cbs_kB =>   "",
                                            :ingress_eir_kbps => "",
                                            :ingress_ebs_kB =>   "",
                                            :egress_eir_kbps =>  "",
                                            :egress_ebs_kB =>    "",
                                            :egress_cir_kbps =>  "1000",
                                            :egress_cbs_kB =>    "")

  return on_net_ovc.reload
end
