require File.dirname(__FILE__) + '/seed_functions.rb'
require File.dirname(__FILE__) + '/cenx_definitions.rb'
require File.dirname(__FILE__) + '/charter_definitions.rb'
require File.dirname(__FILE__) + '/cox_definitions.rb'
require File.dirname(__FILE__) + '/twc_definitions.rb'
################################################################################
# MSO CIRCUITS
################################################################################
DDD="2012/01/01"
MSO_SPEEDS=[ 10000, 100000 ]
mso_path_ctr=0
chr_ctag=1250
chr_addr_ctr=0
cox_ctag=1500
cox_addr_ctr=0
twc_ctag=1750
twc_addr_ctr=0

#chr_scos_map = { CENX_EXC_COS_RT => [4] }
#cox_scos_map = { CENX_EXC_COS_RT => [1] }
#twc_scos_map = { CENX_EXC_COS_RT => [0] }
##chr_scos_map = { CENX_EXC_COS_RT => [5] }
##cox_scos_map = { CENX_EXC_COS_RT => [5] }
##twc_scos_map = { CENX_EXC_COS_RT => [5] }


NUM_OF_HALF_DOZEN_SETS=47

(1..NUM_OF_HALF_DOZEN_SETS).each do |i|
  scos_bws = [ { :cir => MSO_SPEEDS[rand(MSO_SPEEDS.size)], :cbs => 54 } ]

  unified_create_path(CHR_MSO_NTWK, "Path MSO-#{mso_path_ctr+=1}", STANDARD_Path, DDD,
                     XLA, CENX_EXC_ESVCTYPE_NAME, CENX_MSO_COS_NAMES,
                     nil, nil, CHR_MSO_NTWK, CHR_MSO_OVCTYPE, CHR_MSO_ESVC, [CHR_MSO_COS_SLA2], scos_bws,
                          nil, chr_ctag+=1, CHR_MSO_UNITYPE, CHR_MSO_PHYTYPE_NAME, gen_uni_id(CHR_MSO_UNI_ADDRS[chr_addr_ctr+=1]), CHR_MSO_UNI_ADDRS[chr_addr_ctr],
                     nil, nil, COX_MSO_NTWK, COX_MSO_OVCTYPE, COX_MSO_ESVC, [COX_MSO_COS_BSC], scos_bws,
                          nil, cox_ctag+=1, COX_MSO_UNITYPE, COX_MSO_PHYTYPE_NAME, gen_uni_id(COX_MSO_UNI_ADDRS[cox_addr_ctr+=1]), COX_MSO_UNI_ADDRS[cox_addr_ctr])
  
  unified_create_path(CHR_MSO_NTWK, "Path MSO-#{mso_path_ctr+=1}", STANDARD_Path, DDD,
                     XLA, CENX_EXC_ESVCTYPE_NAME, CENX_MSO_COS_NAMES,
                     nil, nil, CHR_MSO_NTWK, CHR_MSO_OVCTYPE, CHR_MSO_ESVC, [CHR_MSO_COS_SLA2], scos_bws,
                          nil, chr_ctag+=1, CHR_MSO_UNITYPE, CHR_MSO_PHYTYPE_NAME, gen_uni_id(CHR_MSO_UNI_ADDRS[chr_addr_ctr+=1]), CHR_MSO_UNI_ADDRS[chr_addr_ctr],
                     nil, nil, TWC_MSO_NTWK, TWC_MSO_OVCTYPE, TWC_MSO_ESVC, [TWC_MSO_COS_EXF], scos_bws,
                          nil, twc_ctag+=1, TWC_MSO_UNITYPE, TWC_MSO_PHYTYPE_NAME, gen_uni_id(TWC_MSO_UNI_ADDRS[twc_addr_ctr+=1]), TWC_MSO_UNI_ADDRS[twc_addr_ctr])
  
  unified_create_path(COX_MSO_NTWK, "Path MSO-#{mso_path_ctr+=1}", STANDARD_Path, DDD,
                     XLA, CENX_EXC_ESVCTYPE_NAME, CENX_MSO_COS_NAMES,
                     nil, nil, COX_MSO_NTWK, COX_MSO_OVCTYPE, COX_MSO_ESVC, [COX_MSO_COS_BSC], scos_bws,
                          nil, cox_ctag+=1, COX_MSO_UNITYPE, COX_MSO_PHYTYPE_NAME, gen_uni_id(COX_MSO_UNI_ADDRS[cox_addr_ctr+=1]), COX_MSO_UNI_ADDRS[cox_addr_ctr],
                     nil, nil, CHR_MSO_NTWK, CHR_MSO_OVCTYPE, CHR_MSO_ESVC, [CHR_MSO_COS_SLA2], scos_bws,
                          nil, chr_ctag+=1, CHR_MSO_UNITYPE, CHR_MSO_PHYTYPE_NAME, gen_uni_id(CHR_MSO_UNI_ADDRS[chr_addr_ctr+=1]), CHR_MSO_UNI_ADDRS[chr_addr_ctr])
  
  unified_create_path(COX_MSO_NTWK, "Path MSO-#{mso_path_ctr+=1}", STANDARD_Path, DDD,
                     XLA, CENX_EXC_ESVCTYPE_NAME, CENX_MSO_COS_NAMES,
                     nil, nil, COX_MSO_NTWK, COX_MSO_OVCTYPE, COX_MSO_ESVC, [COX_MSO_COS_BSC], scos_bws,
                          nil, cox_ctag+=1, COX_MSO_UNITYPE, COX_MSO_PHYTYPE_NAME, gen_uni_id(COX_MSO_UNI_ADDRS[cox_addr_ctr+=1]), COX_MSO_UNI_ADDRS[cox_addr_ctr],
                     nil, nil, TWC_MSO_NTWK, TWC_MSO_OVCTYPE, TWC_MSO_ESVC, [TWC_MSO_COS_EXF], scos_bws,
                          nil, twc_ctag+=1, TWC_MSO_UNITYPE, TWC_MSO_PHYTYPE_NAME, gen_uni_id(TWC_MSO_UNI_ADDRS[twc_addr_ctr+=1]), TWC_MSO_UNI_ADDRS[twc_addr_ctr])
  
  unified_create_path(TWC_MSO_NTWK, "Path MSO-#{mso_path_ctr+=1}", STANDARD_Path, DDD,
                     XLA, CENX_EXC_ESVCTYPE_NAME, CENX_MSO_COS_NAMES,
                     nil, nil, TWC_MSO_NTWK, TWC_MSO_OVCTYPE, TWC_MSO_ESVC, [TWC_MSO_COS_EXF], scos_bws,
                          nil, twc_ctag+=1, TWC_MSO_UNITYPE, TWC_MSO_PHYTYPE_NAME, gen_uni_id(TWC_MSO_UNI_ADDRS[twc_addr_ctr+=1]), TWC_MSO_UNI_ADDRS[twc_addr_ctr],
                     nil, nil, CHR_MSO_NTWK, CHR_MSO_OVCTYPE, CHR_MSO_ESVC, [CHR_MSO_COS_SLA2], scos_bws,
                          nil, chr_ctag+=1, CHR_MSO_UNITYPE, CHR_MSO_PHYTYPE_NAME, gen_uni_id(CHR_MSO_UNI_ADDRS[chr_addr_ctr+=1]), CHR_MSO_UNI_ADDRS[chr_addr_ctr])
  
  unified_create_path(TWC_MSO_NTWK, "Path MSO-#{mso_path_ctr+=1}", STANDARD_Path, DDD,
                     XLA, CENX_EXC_ESVCTYPE_NAME, CENX_MSO_COS_NAMES,
                     nil, nil, TWC_MSO_NTWK, TWC_MSO_OVCTYPE, TWC_MSO_ESVC, [TWC_MSO_COS_EXF], scos_bws,
                          nil, twc_ctag+=1, TWC_MSO_UNITYPE, TWC_MSO_PHYTYPE_NAME, gen_uni_id(TWC_MSO_UNI_ADDRS[twc_addr_ctr+=1]), TWC_MSO_UNI_ADDRS[twc_addr_ctr],
                     nil, nil, COX_MSO_NTWK, COX_MSO_OVCTYPE, COX_MSO_ESVC, [COX_MSO_COS_BSC], scos_bws,
                          nil, cox_ctag+=1, COX_MSO_UNITYPE, COX_MSO_PHYTYPE_NAME, gen_uni_id(COX_MSO_UNI_ADDRS[cox_addr_ctr+=1]), COX_MSO_UNI_ADDRS[cox_addr_ctr])
end
