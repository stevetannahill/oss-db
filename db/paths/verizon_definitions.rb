VZN_NAME="Verizon Telecom"
VZN_OWNR_NTWK_NAME="Nationwide LTE Network"
VZN_BKHL_NTWK_NAME="Nationwide LongHaul Network"
VZN_BKHL_ESVC_NAME="DWDM Long Haul Service"
VZN_BKHL_COS_WDM_NAME="DWDM CoS"
VZN_BKHL_UNITYPE_NAME="DWDM Handoff"
VZN_BKHL_PHYTYPE_NAME="10GBase-LR"
VZN_BKHL_OVCTYPE_NAME="DWDM Long Haul Service OVC"

VZN               = (defined? VZN).nil?                ? ServiceProvider.find_by_name(VZN_NAME) : VZN
VZN_OWNR_NTWK     = (defined? VZN_OWNR_NTWK).nil?      ? VZN.operator_networks.find_by_name(VZN_OWNR_NTWK_NAME) : VZN_OWNR_NTWK
VZN_BKHL_NTWK     = (defined? VZN_BKHL_NTWK).nil?      ? VZN.operator_networks.find_by_name(VZN_BKHL_NTWK_NAME) : VZN_BKHL_NTWK
VZN_BKHL_NTWKTYPE = (defined? VZN_BKHL_NTWKTYPE).nil?  ? VZN_BKHL_NTWK.operator_network_type : VZN_BKHL_NTWKTYPE
VZN_BKHL_ESVC     = (defined? VZN_BKHL_ESVC).nil?      ? VZN_BKHL_NTWKTYPE.ethernet_service_types.find_by_name(VZN_BKHL_ESVC_NAME) : VZN_BKHL_ESVC
VZN_BKHL_COS_WDM  = (defined? VZN_BKHL_COS_WDM).nil?   ? VZN_BKHL_ESVC.class_of_service_types.find_by_name(VZN_BKHL_COS_WDM_NAME) : VZN_BKHL_COS_WDM
VZN_BKHL_UNITYPE  = (defined? VZN_BKHL_UNITYPE).nil?   ? VZN_BKHL_ESVC.demarc_types.find_by_name(VZN_BKHL_UNITYPE_NAME) : VZN_BKHL_UNITYPE
VZN_BKHL_OVCTYPE  = (defined? VZN_BKHL_OVCTYPE).nil?   ? VZN_BKHL_ESVC.segment_types.find_by_name(VZN_BKHL_OVCTYPE_NAME) : VZN_BKHL_OVCTYPE
