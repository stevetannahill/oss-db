require File.dirname(__FILE__) + '/seed_functions.rb'
require File.dirname(__FILE__) + '/seed_lsq_functions.rb'
require File.dirname(__FILE__) + '/cenx_definitions.rb'
require File.dirname(__FILE__) + '/lightsquared_definitions.rb'
require File.dirname(__FILE__) + '/verizon_definitions.rb'
require File.dirname(__FILE__) + '/covad_definitions.rb'
require File.dirname(__FILE__) + '/xo_definitions.rb'
require File.dirname(__FILE__) + '/att_definitions.rb'
require File.dirname(__FILE__) + '/ussignal_definitions.rb'
################################################################################
# ITW CIRCUITS
################################################################################
CXID_PHX_TUNNEL_1 = "101263391637"
CXID_PHX_TUNNEL_2 = "101229804573"
CXID_PHX_TUNNEL_3 = "101261952999"
CXID_PHX_TUNNEL_4 = "101215514690"
CXID_PHX_TUNNEL_5 = "101211920023"
CXID_PHX_TUNNEL_6 = "101217453193"
CXID_CHI_TUNNEL_1 = "101224725719"
CXID_CHI_TUNNEL_2 = "101222761816"
CXID_CHI_TUNNEL_3 = "101246765171"
CXID_CHI_TUNNEL_4 = "101253234457"
CXID_CHI_TUNNEL_5 = "101231405196"
CXID_CHI_TUNNEL_6 = "101295899584"
TUNNEL_USE_WEIGHTS = { :live => 3, :ready => 1, :testing => 1, :pending => 1 }
ITW_SPEEDS=[ 50000, 100000 ]
DDD="2012/01/01"

itw_path_ctr=0
xo_ctag=3000
cvd_ctag=3250
att_ctag=3500
uss_ctag=3750

# The OVC tunnels are organized in protection pairs of 1&4, 2&5, and 3&6.
phxbkhl_tnls = get_weighted_bkhl_tunnel_pairs([[ CXID_PHX_TUNNEL_1, CXID_PHX_TUNNEL_4 ],
                                               [ CXID_PHX_TUNNEL_2, CXID_PHX_TUNNEL_5 ],
                                               [ CXID_PHX_TUNNEL_3, CXID_PHX_TUNNEL_6 ]])

chibkhl_tnls = get_weighted_bkhl_tunnel_pairs([[ CXID_CHI_TUNNEL_1, CXID_CHI_TUNNEL_4 ],
                                               [ CXID_CHI_TUNNEL_2, CXID_CHI_TUNNEL_5 ],
                                               [ CXID_CHI_TUNNEL_3, CXID_CHI_TUNNEL_6 ]])

#xo_scos_map = { CENX_EXC_COS_RT => [5] }
#cvd_scos_map = { CENX_EXC_COS_RT => [5] }
#att_scos_map = { CENX_EXC_COS_RT => [6] }
#uss_scos_map = { CENX_EXC_COS_RT => [5] }
#vzn_scos_map = { CENX_EXC_COS_RT => [5] }

(0..99).each do |i|
  scos_bws = [ { :cir => ITW_SPEEDS[rand(ITW_SPEEDS.size)], :cbs => 54 } ]

  uni_addr=XO_ACCS_ADDRS[i]
  unified_create_path(VZN_OWNR_NTWK, "Path ITW-#{itw_path_ctr+=1}", LSQ_NCELL_Path, DDD,
                     XPH, CENX_EXC_ESVCTYPE_NAME, CENX_MSO_COS_NAMES,
                     load_balance_tunnel_pairs(phxbkhl_tnls), nil, VZN_BKHL_NTWK, VZN_BKHL_OVCTYPE, VZN_BKHL_ESVC, [VZN_BKHL_COS_WDM], scos_bws,
                          nil, xo_ctag+=1, VZN_BKHL_UNITYPE, VZN_BKHL_PHYTYPE_NAME, nil, nil,
                     nil, nil, XO_ACCS_NTWK, XO_ACCS_OVCTYPE, XO_ACCS_ESVC, [XO_ACCS_COS_RT], scos_bws,
                          nil, xo_ctag, XO_ACCS_UNITYPE, XO_ACCS_PHYTYPE_NAME, gen_uni_id(uni_addr), uni_addr)
  
  uni_addr=CVD_ACCS_ADDRS[i]
  unified_create_path(VZN_OWNR_NTWK, "Path ITW-#{itw_path_ctr+=1}", LSQ_NCELL_Path, DDD,
                     XPH, CENX_EXC_ESVCTYPE_NAME, CENX_MSO_COS_NAMES,
                     load_balance_tunnel_pairs(phxbkhl_tnls), nil, VZN_BKHL_NTWK, VZN_BKHL_OVCTYPE, VZN_BKHL_ESVC, [VZN_BKHL_COS_WDM], scos_bws,
                          nil, cvd_ctag+=1, VZN_BKHL_UNITYPE, VZN_BKHL_PHYTYPE_NAME, nil, nil,
                     nil, nil, CVD_ACCS_NTWK, CVD_ACCS_OVCTYPE, CVD_ACCS_ESVC, [CVD_ACCS_COS_GLD], scos_bws,
                          nil, cvd_ctag, CVD_ACCS_UNITYPE, CVD_ACCS_PHYTYPE_NAME, gen_uni_id(uni_addr), uni_addr)
  
  uni_addr=ATT_ACCS_ADDRS[i]
  unified_create_path(VZN_OWNR_NTWK, "Path ITW-#{itw_path_ctr+=1}", LSQ_NCELL_Path, DDD,
                     XPH, CENX_EXC_ESVCTYPE_NAME, CENX_MSO_COS_NAMES,
                     load_balance_tunnel_pairs(phxbkhl_tnls), nil, VZN_BKHL_NTWK, VZN_BKHL_OVCTYPE, VZN_BKHL_ESVC, [VZN_BKHL_COS_WDM], scos_bws,
                          nil, att_ctag+=1, VZN_BKHL_UNITYPE, VZN_BKHL_PHYTYPE_NAME, nil, nil,
                     nil, nil, ATT_ACCS_NTWK, ATT_ACCS_OVCTYPE, ATT_ACCS_ESVC, [ATT_ACCS_COS_RT], scos_bws,
                          nil, att_ctag, ATT_ACCS_UNITYPE, ATT_ACCS_PHYTYPE_NAME, gen_uni_id(uni_addr), uni_addr)
  
  uni_addr="#{random_street_address}, #{random_from_list(["Phoenix, AZ", "Las Vegas, NV", "Salt Lake City, UT", "Albuquerque, NM"])}"
  unified_create_path(VZN_OWNR_NTWK, "Path ITW-#{itw_path_ctr+=1}", LSQ_NCELL_Path, DDD,
                     XCH, CENX_EXC_ESVCTYPE_NAME, CENX_MSO_COS_NAMES,
                     load_balance_tunnel_pairs(chibkhl_tnls), nil, VZN_BKHL_NTWK, VZN_BKHL_OVCTYPE, VZN_BKHL_ESVC, [VZN_BKHL_COS_WDM], scos_bws,
                          nil, att_ctag+=1, VZN_BKHL_UNITYPE, VZN_BKHL_PHYTYPE_NAME, nil, nil,
                     nil, nil, USS_ACCS_NTWK, USS_ACCS_OVCTYPE, USS_ACCS_ESVC, [USS_ACCS_COS_PRM], scos_bws,
                          nil, uss_ctag, USS_ACCS_UNITYPE, USS_ACCS_PHYTYPE_NAME, gen_uni_id(uni_addr), uni_addr)
end
