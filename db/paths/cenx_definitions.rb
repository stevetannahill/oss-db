CENX_NAME = "CENX"
CENX_EXC_NTWKTYPE_NAME="Cenx Exchange"
CENX_LSQ_NTWKTYPE_NAME="Cenx Light Squared Exchange"

CENX_ESVCTYPE_NAME = "Cenx Exchange"
CENX_OVCTYPE_NAME  = "Cenx Exchange OVC"
CENX_ENNITYPE_NAME = "ENNI"
CENX_COS_PRM_NAME  = "Premium Data"
CENX_COS_RT_NAME   = "Real Time"
CENX_COS_STD_NAME  = "Standard Data"

CENX_EXC_ESVCTYPE_NAME = CENX_ESVCTYPE_NAME
CENX_EXC_OVCTYPE_NAME  = CENX_OVCTYPE_NAME
CENX_EXC_ENNITYPE_NAME = CENX_ENNITYPE_NAME
CENX_EXC_COS_PRM_NAME  = CENX_COS_PRM_NAME
CENX_EXC_COS_RT_NAME   = CENX_COS_RT_NAME
CENX_EXC_COS_STD_NAME  = CENX_COS_STD_NAME

CENX_LSQ_ESVCTYPE_NAME = CENX_ESVCTYPE_NAME
CENX_LSQ_OVCTYPE_NAME  = CENX_OVCTYPE_NAME
CENX_LSQ_ENNITYPE_NAME = CENX_ENNITYPE_NAME
CENX_LSQ_COS_PRM_NAME  = CENX_COS_PRM_NAME
CENX_LSQ_COS_RT_NAME   = CENX_COS_RT_NAME
CENX_LSQ_COS_STD_NAME  = CENX_COS_STD_NAME

CENX_MSO_COS_NAMES=["Real Time"]


CENX               = (defined? CENX).nil?              ? ServiceProvider.find_by_name(CENX_NAME) : CENX

CENX_EXC_NTWKTYPE  = (defined? CENX_EXC_NTWKTYPE).nil? ? CENX.operator_network_types.find_by_name(CENX_EXC_NTWKTYPE_NAME)          : CENX_EXC_NTWKTYPE
CENX_EXC_ESVCTYPE  = (defined? CENX_EXC_ESVCTYPE).nil? ? CENX_EXC_NTWKTYPE.ethernet_service_types.find_by_name(CENX_ESVCTYPE_NAME) : CENX_EXC_ESVCTYPE
CENX_EXC_OVCTYPE   = (defined? CENX_EXC_OVCTYPE).nil?  ? CENX_EXC_ESVCTYPE.segment_types.find_by_name(CENX_OVCTYPE_NAME)               : CENX_EXC_OVCTYPE
CENX_EXC_ENNITYPE  = (defined? CENX_EXC_ENNITYPE).nil? ? CENX_EXC_ESVCTYPE.demarc_types.find_by_name(CENX_ENNITYPE_NAME)           : CENX_EXC_ENNITYPE
CENX_EXC_COS_PRM   = (defined? CENX_EXC_COS_PRM).nil?  ? CENX_EXC_ESVCTYPE.class_of_service_types.find_by_name(CENX_COS_PRM_NAME)  : CENX_EXC_COS_PRM
CENX_EXC_COS_RT    = (defined? CENX_EXC_COS_RT).nil?   ? CENX_EXC_ESVCTYPE.class_of_service_types.find_by_name(CENX_COS_RT_NAME)   : CENX_EXC_COS_RT
CENX_EXC_COS_STD   = (defined? CENX_EXC_COS_STD).nil?  ? CENX_EXC_ESVCTYPE.class_of_service_types.find_by_name(CENX_COS_STD_NAME)  : CENX_EXC_COS_STD

CENX_LSQ_NTWKTYPE  = (defined? CENX_LSQ_NTWKTYPE).nil? ? CENX.operator_network_types.find_by_name(CENX_LSQ_NTWKTYPE_NAME)          : CENX_LSQ_NTWKTYPE
CENX_LSQ_ESVCTYPE  = (defined? CENX_LSQ_ESVCTYPE).nil? ? CENX_LSQ_NTWKTYPE.ethernet_service_types.find_by_name(CENX_ESVCTYPE_NAME) : CENX_LSQ_ESVCTYPE
CENX_LSQ_OVCTYPE   = (defined? CENX_LSQ_OVCTYPE).nil?  ? CENX_LSQ_ESVCTYPE.segment_types.find_by_name(CENX_OVCTYPE_NAME)               : CENX_LSQ_OVCTYPE
CENX_LSQ_ENNITYPE  = (defined? CENX_LSQ_ENNITYPE).nil? ? CENX_LSQ_ESVCTYPE.demarc_types.find_by_name(CENX_ENNITYPE_NAME)           : CENX_LSQ_ENNITYPE
CENX_LSQ_COS_PRM   = (defined? CENX_LSQ_COS_PRM).nil?  ? CENX_LSQ_ESVCTYPE.class_of_service_types.find_by_name(CENX_COS_PRM_NAME)  : CENX_LSQ_COS_PRM
CENX_LSQ_COS_RT    = (defined? CENX_LSQ_COS_RT).nil?   ? CENX_LSQ_ESVCTYPE.class_of_service_types.find_by_name(CENX_COS_RT_NAME)   : CENX_LSQ_COS_RT
CENX_LSQ_COS_STD   = (defined? CENX_LSQ_COS_STD).nil?  ? CENX_LSQ_ESVCTYPE.class_of_service_types.find_by_name(CENX_COS_STD_NAME)  : CENX_LSQ_COS_STD

XCH_NAME="Chicago"
XDS_NAME="TAUSTXDFWT0001"
XLA_NAME="Los Angeles"
XLV_NAME="TAUSNVVEGS0001"
XPH_NAME="Phoenix"

XCH = (defined? XCH).nil? ? Pop.find_by_name(XCH_NAME) : XCH
XDS = (defined? XDS).nil? ? Pop.find_by_name(XDS_NAME) : XDS
XLA = (defined? XLA).nil? ? Pop.find_by_name(XLA_NAME) : XLA
XLV = (defined? XLV).nil? ? Pop.find_by_name(XLV_NAME) : XLV
XPH = (defined? XPH).nil? ? Pop.find_by_name(XPH_NAME) : XPH

STANDARD_Path="Standard"
MEMBER_LINK_Path="Member Link"
LSQ_1CELL_Path="LightSquared Single Cell"
LSQ_NCELL_Path="LightSquared Multi Cell"
TUNNEL_Path="Tunnel"

