# This should be loaded before creating an fake Path,Segments etc
# NEVER create a real Paths,Segments etc if this has been run - quit the console and
# restart the console if you want to create real Path's


# I don't know why I have make sure the object are referenced but if I don't something
# funky happens with OnNetOvcEndPointEnni. The calling of the monitired? method calls
# the parent one - not the one defined in OnNetOvcEndPointEnni !!!
OnNetRouter
OnNetRouterSubIf
OnNetOvc
OnNetOvcEndPointEnni
SegmentEndPoint
OvcEndPointUni
EnniNew
CosTestVector
BxCosTestVector
SpirentCosTestVector

$first_alarm_time = Time.now if !defined?($first_alarm_time)
$fake_output = true if !defined?($fake_output)
puts "Loading Fake go inservice with alarm time #{$first_alarm_time}" if $fake_output

class OnNetOvc < OnNetSegment
  def go_in_service
    puts "Fake #{self.class.to_s}:#{self.id} go_in_service" if $fake_output
    return {:result => false, :info => "Failed to initialise"} if on_net_ovc_end_point_ennis.size == 0
    
    info = ""
    result = true
    if result
      sm_histories << SmHistory.create(:event_filter => "SM Segment:#{cenx_id}") if sm_histories.empty?
      # Create a new Maintenance with a record being cleared ie not in maintenance period
      if mtc_periods.empty?
        self.mtc_periods = [MtcHistory.create(:event_filter => "Mtc Segment:#{cenx_id}")]
      end
      self.set_mtc(false, "go_in_service") if mtc_periods.first.alarm_records.size == 0
    end  
    info = "Failed to initialise" unless result  
    return {:result => result, :info => info}
  end
end

class OnNetRouter < OnNetSegment
  def go_in_service
    puts "Fake #{self.class.to_s}:#{self.id} go_in_service" if $fake_output
    info = ""
    return {:result => false, :info => info} if on_net_ovc_end_point_ennis.size == 0
    
    result = true
    if result
      sm_histories << SmHistory.create(:event_filter => "SM Segment:#{cenx_id}") if sm_histories.empty?
      # Create a new Maintenance with a record being cleared ie not in maintenance period
      if mtc_periods.empty?
        self.mtc_periods = [MtcHistory.create(:event_filter => "Mtc Segment:#{cenx_id}")]
      end
      self.set_mtc(false, "go_in_service") if mtc_periods.first.alarm_records.size == 0
    end
    return {:result => result, :info => info} 
  end
end


class BxCosTestVector < CosTestVector  
  
  def go_in_service
    puts "Fake #{self.class.to_s}:#{self.id} go_in_service" if $fake_output
    info = ""
    return_value = true 
    if cos_end_point.segment_end_point.is_monitored
      if alarm_histories.empty?
        alarm_histories << BrixAlarmHistory.create(:event_filter => "Fake CTV #{self.id}")
        alarm_histories.first.add_event(($first_alarm_time.to_f*1000).to_i.to_s, "cleared", "")
      end      
    else
      # It's not monitored so delete any history
      alarm_histories.destroy_all
    end  
    info = "Failed to initialise" unless return_code  
    return {:result => return_value, :info => info}
  end
end

class SpirentCosTestVector < CosTestVector  
=begin  
  def go_in_service
    puts "Fake #{self.class.to_s}:#{self.id} go_in_service" if $fake_output
    return_value = true 
    if cos_end_point.segment_end_point.is_monitored
      if alarm_histories.empty?
        if AvailabilityHistory.exists?(:event_filter => "#{circuit_id}-Availability")
          alarm_histories << AvailabilityHistory.find_by_event_filter("#{circuit_id}-Availability")
        else
          alarm_histories << AvailabilityHistory.create(:event_filter => "#{circuit_id}-Availability")
          alarm_histories.first.add_event(($first_alarm_time.to_f*1000).to_i.to_s, "cleared", "")
        end
      end      
    else
      # It's not monitored so delete any history
      alarm_histories.destroy_all
    end  
    
    return return_value
  end
=end
end

class OnNetOvcEndPointEnni < OvcEndPointEnni
  
  def go_in_service
    info = ""
    puts "Fake #{self.class.to_s}:#{self.id} go_in_service" if $fake_output
    return_code = true
    if is_connected_to_test_port 
      # Delete any existing histories
      alarm_histories.destroy_all
      sm_histories.destroy_all
      info = "Failed to initialise" unless return_code  
      return {:result => return_code, :info => info}
    end

    sm_histories << SmHistory.create(:event_filter => "SM EndPoint:#{cenx_id}") if sm_histories.empty?

    if alarm_histories.empty?
      if enni.is_multi_chassis && !is_connected_to_test_port
        # for MultiChassis LAG add the alarm histroies on each node and then OR the two together
        fstate = node_names.collect do |node|        
          ip = Node.find_by_name(node).loopback_ip
          ah = AlarmHistory.create(:event_filter => "Fake EP#{cenx_id}-#{ip}-#{node}", :alarm_mapping => AlarmSeverity.default_alu_alarm_mapping)          
          alarm_histories << ah          
          ah.add_event(($first_alarm_time.to_f*1000).to_i.to_s, "cleared", "")        
        end
      else
        alarm_histories << AlarmHistory.create(:event_filter => "Fake EP#{cenx_id}", :alarm_mapping => AlarmSeverity.default_alu_alarm_mapping)
        alarm_histories.first.add_event(($first_alarm_time.to_f*1000).to_i.to_s, "cleared", "")
      end
    end

    info = "Failed to initialise" unless return_code
    return {:result => return_code, :info => info}
  end  
  
end

class OnNetRouterSubIf < RouterSubIf
  def go_in_service
    puts "Fake #{self.class.to_s}:#{self.id} go_in_service" if $fake_output
    info = ""
    return_code = true
    if is_connected_to_test_port
      # Delete any existing histories
      alarm_histories.destroy_all
      sm_histories.destroy_all
      return {:result => return_code, :info => info}
    end

    sm_histories << SmHistory.create(:event_filter => "SM EndPoint:#{cenx_id}") if sm_histories.empty?

    if alarm_histories.empty?
      if enni.is_multi_chassis && !is_connected_to_test_port
        # for MultiChassis LAG add the alarm histroies on each node and then OR the two together
        fstate = node_names.collect do |node|
          ip = Node.find_by_name(node).loopback_ip
          ah = AlarmHistory.create(:event_filter => "Fake EP#{cenx_id}-#{ip}-#{node}", :alarm_mapping => AlarmSeverity.default_alu_alarm_mapping)
          alarm_histories << ah
          ah.add_event(($first_alarm_time.to_f*1000).to_i.to_s, "cleared", "")
        end
      else
        alarm_histories << AlarmHistory.create(:event_filter => "Fake EP#{cenx_id}", :alarm_mapping => AlarmSeverity.default_alu_alarm_mapping)
        alarm_histories.first.add_event(($first_alarm_time.to_f*1000).to_i.to_s, "cleared", "")
      end
    end

    return {:result => return_code, :info => info}
  end
end
class EnniNew < Demarc
  def go_in_service    
    puts "Fake #{self.class.to_s}:#{self.id} go_in_service" if $fake_output
    info = ""
    return_code = true
    if is_connected_to_test_port
      # delete alarm and mtc histories
      mtc_periods.destroy_all
      alarm_histories.destroy_all
      sm_histories.destroy_all
      return {:result => true, :info => info}
    end
        
    # create alarm and service monitoring histories
    sm_histories << SmHistory.create(:event_filter => "SM ENNI:#{cenx_id}") if sm_histories.empty?
    
    # create the Maintenance history    
    if return_code && mtc_periods.empty?
      mtc_periods << MtcHistory.create(:event_filter => "Mtc ENNI:#{cenx_id}")
      self.set_mtc(false, "go_in_service")
    end
    
    if alarm_histories.empty?      
      if is_lag
        if !is_connected_to_test_port          
          alarm_histories << AlarmHistory.create(:event_filter => "Fake ENNI network:10.10.100.15:lag:interface-2 #{cenx_id}", :alarm_mapping => AlarmSeverity.default_alu_alarm_mapping)
          alarm_histories.each {|a| a.add_event(($first_alarm_time.to_f*1000).to_i.to_s, "cleared", "")}
          
          alarm_histories << AlarmHistory.create(:event_filter => "Fake ENNI network:10.10.100.15:lag:interface-2:port-1/2/10 #{cenx_id}", :alarm_mapping => AlarmSeverity.default_alu_alarm_mapping)
          alarm_histories.each {|a| a.add_event(($first_alarm_time.to_f*1000).to_i.to_s, "cleared", "")}          
        end
      else
        alarm_histories << AlarmHistory.create(:event_filter => "Fake ENNI network:10.10.100.15:shelf-1:cardSlot-1:card:daughterCardSlot-2:daughterCard:port-17 #{cenx_id}", :alarm_mapping => AlarmSeverity.default_alu_alarm_mapping)
        alarm_histories.each {|a| a.add_event(($first_alarm_time.to_f*1000).to_i.to_s, "cleared", "")}
      end      
    end
    
    info = "Failed to initialise" unless return_code
    return {:result => return_code, :info => info}
  end
end


class OvcEndPointUni < OvcEndPoint

  def get_alarm_keys
    # Get the MEP alarm from the OnNetOvc connected to this endpoint
    meg_level = [md_level.to_i]
    new_mep_alarms = []
    return_code = true

    if cos_end_points.size != 0
      cos_end_points_to_cenx = cos_end_points.first.get_remote_sibling_cos_end_points
      if !cos_end_points_to_cenx.empty?
        cenx_ep = cos_end_points_to_cenx.first.segment_end_point
        if cenx_ep != nil && cenx_ep.is_a?(OnNetOvcEndPointEnni)
          ip_ping_monitoring_is_required, ccm_monitoring_is_required, lbm_monitoring_is_required, dmm_monitoring_is_required, monitored_remote_endpoint = cenx_ep.segment.get_service_monitoring_params
          if ccm_monitoring_is_required && monitored_remote_endpoint == self
            return_code, new_mep_alarms = [true, ["Fake EP #{self.class.name}:#{cenx_id}"]]          
            if !return_code
              SW_ERR "#{self.class}:#{self.id} Failed to get MEP alarms from On Net OVC(#{cenx_ep.segment.class}:#{cenx_ep.segment.id})"
            elsif new_mep_alarms.empty?
              SW_ERR "#{self.class}:#{self.id} Expected to get MEP alarms from On Net OVC(#{cenx_ep.segment.class}:#{cenx_ep.segment.id}) but got none"
              return_code = false
            end
          end
        else
          return_code = false
          SW_ERR "Cannot find connected Cenx Endpoint for #{self.class}:#{self.id}", :IGNORE_IF_TEST
          # Temp for Sprint
          return_code, new_mep_alarms = [true, ["Fake EP #{self.class.name}:#{cenx_id}"]]                    
        end
      else
        SW_ERR "#{self.class}:#{self.id} no remote sibling cos endpoints found (#{cos_end_points.inspect})", :IGNORE_IF_TEST
        return_code = false  
        return_code = true # BJW temp for sprint demo        
              
      end
    end
    
    return return_code, new_mep_alarms
  end

  def go_in_service
    puts "Fake #{self.class.to_s}:#{self.id} go_in_service" if $fake_output
    return_code = super
          
    if return_code[:result]
      if alarm_histories.empty?
        return_code[:result], new_mep_alarms = get_alarm_keys
        if return_code[:result]          
          mep_mapping = AlarmSeverity.default_alu_alarm_mapping
          mep_mapping["major"] = AlarmSeverity::FAILED
          new_alarms = {} 
          new_mep_alarms.each {|filter|
            alarm_histories << AlarmHistory.create(:event_filter => filter, :alarm_mapping => mep_mapping)
            alarm_histories.last.add_event(($first_alarm_time.to_f*1000).to_i.to_s, "cleared", "")          
          }
        end
      end
    end
    resturn_code[:info] = "Failed to initialise" if !return_code[:result] && return_code[:info].blank?  
    return return_code
  end
end

class SegmentEndPoint < InstanceElement
  
  def go_in_service
    puts "Fake #{self.class.to_s}:#{self.id} go_in_service"  if $fake_output
    info = ""
    sm_histories << SmHistory.create(:event_filter => "SM #{self.class.name}:#{cenx_id}") if sm_histories.empty?
    
    return_code = true
    if alarm_histories.empty?    
      if segment.is_in_ls_core_transport_path 
        mep_mapping = AlarmSeverity.default_alu_alarm_mapping
        mep_mapping["major"] = AlarmSeverity::WARNING
        
        alarm_histories << AlarmHistory.create(:event_filter => "Fake EP #{self.class.name}:#{cenx_id}", :alarm_mapping => mep_mapping)
        alarm_histories.first.add_event(($first_alarm_time.to_f*1000).to_i.to_s, "cleared", "")
      end
    end
    
    if return_code
      return_code = cos_end_points.all? {|cep|
        cep_result = cep.go_in_service
        info << cep_result[:info]
        cep_result[:result]
      }
    end
    info = "Failed to initialise" if !return_code && info.blank?  
    return {:result => return_code, :info => info}
  end
end



