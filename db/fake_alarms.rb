# Set alarms on Random live On Net and Off Net OVC's
# The list do_no_touch_evs is an array of path id's which should not be considered
def fake_alarms(do_not_touch_paths, on_net_ovc_percent, off_net_ovc_percent)
  live_paths = Path.all.collect {|p| p if p.get_prov_name == "Live" && p.cenx_path_type != "Tunnel"}.compact
  live_paths.reject! {|path| do_not_touch_paths.include?(path.id)}

  on_net_ovcs = live_paths.collect {|path| path.on_net_ovcs}.flatten
  member_ovs = live_paths.collect {|path| path.off_net_ovcs}.flatten

  num_alarmed_on_net_ovcs = (on_net_ovcs.size*on_net_ovc_percent)/100
  num_alarmed_off_net_ovcs = (member_ovs.size*off_net_ovc_percent)/100
  alarmed_on_net_ovcs = (0..num_alarmed_on_net_ovcs*10).collect {|i| on_net_ovcs[rand(on_net_ovcs.size)]}.uniq[0..num_alarmed_on_net_ovcs-1]
  alarmed_off_net_ovcs = (0..num_alarmed_off_net_ovcs*10).collect {|i| member_ovs[rand(member_ovs.size)]}.uniq[0..num_alarmed_off_net_ovcs-1]

  puts "number of On Net OVCs #{on_net_ovcs.size} Number to be alarmed #{num_alarmed_on_net_ovcs} actual #{alarmed_on_net_ovcs.size}"
  puts "number of Off Net OVCs #{member_ovs.size} Number to be alarmed #{num_alarmed_off_net_ovcs} actual #{alarmed_off_net_ovcs.size}"
  
  # Set random non monitoring ep to critical
  alarmed_on_net_ovcs.each {|segment|
    puts "On Net OVC setting fault #{segment.id}"
    eps = segment.segment_end_points.collect {|ep| ep if !ep.is_connected_to_test_port}.compact
    eps[rand(eps.size)].alarm_histories.first.add_event((Time.now.to_f*1000).to_i.to_s, "critical", "")
  }

  # Set random cosep that has an alarm history
  alarmed_off_net_ovcs.each {|segment|
    next if segment.paths.size > 1
    puts "Memver Segment setting fault #{segment.id}" 
    cosep = [] 
    segment.segment_end_points.each {|ep|
      cosep = cosep + ep.cos_end_points.collect {|cosep| cosep if cosep.alarm_histories.size != 0}
    }
    cosep.compact!    
    cosep[rand(cosep.size)].alarm_histories.first.add_event((Time.now.to_f*1000).to_i.to_s, "unavailable", "") if cosep.size != 0
  }
end

# Set select path_percent of Live Paths and make cenx_percent of them Unavailable and the rest will have
# the Off Net OVC set to unavailable
def fake_alarms1(do_not_touch_paths, path_percent, on_net_ovc_percent)
  live_paths = Path.all.collect {|p| p if p.get_prov_name == "Live" && p.cenx_path_type != "Tunnel"}.compact
  live_paths.reject! {|path| do_not_touch_paths.include?(path.id)}
  #live_paths.reject! {|path| path.off_net_ovcs.first.paths.size > 1} # don't include Off Net OVCs that belong to a tunnel
  num_alarmed_paths = (live_paths.size*path_percent)/100
  alarmed_paths = (0..num_alarmed_paths*10).collect {|i| live_paths[rand(live_paths.size)]}.uniq[0..num_alarmed_paths-1]

  num_alarmed_on_net_ovcs = (alarmed_paths.size*on_net_ovc_percent)/100
  alarmed_on_net_ovcs = (0..num_alarmed_on_net_ovcs*10).collect {|i| alarmed_paths[rand(alarmed_paths.size)]}.uniq[0..num_alarmed_on_net_ovcs-1]
  alarmed_off_net_ovcs = alarmed_paths - alarmed_on_net_ovcs 
  
  puts "number of On Net OVCs to be alarmed #{alarmed_on_net_ovcs.size} "
  puts "number of Off Net OVCs to be alarmed #{alarmed_off_net_ovcs.size}"
  
  # Set random non monitoring ep to critical
  alarmed_on_net_ovcs.each {|path|
    puts "Path:#{path.id} #{path.cenx_name} On Net OVC setting fault #{path.on_net_ovcs.first.id}"
    eps = path.on_net_ovcs.first.segment_end_points.collect {|ep| ep if !ep.is_connected_to_test_port}.compact
    eps[rand(eps.size)].alarm_histories.first.add_event((Time.now.to_f*1000).to_i.to_s, "critical", "")
  }

  # Set random cosep that has an alarm history
  alarmed_off_net_ovcs.each {|path|
    #next if path.off_net_ovcs.first.paths.size > 1
    cosep = [] 
    path.off_net_ovcs.each do |segment|
      next if segment.paths.size > 1
      segment.segment_end_points.each {|ep|
        puts "Path:#{path.id} #{path.cenx_name} Memver Segment setting fault #{segment.id}" 
        cosep = cosep + ep.cos_end_points.collect {|cosep| cosep if cosep.alarm_histories.size != 0}
      }
    end
    cosep.compact!    
    cosep[rand(cosep.size)].alarm_histories.first.add_event((Time.now.to_f*1000).to_i.to_s, "unavailable", "") if cosep.size != 0
  }
end  


do_not_touch_paths_cenx_ids = ["100482568696", "100440329409", "100495555250", "100494084734", "100414336106", "100403192895", "100474648281", "100458730514", "100444475825", "100464690164", "100423777185", "100433326718", "100429304865", "100433388669", "100410559054", "100470117421", "100400356058", "100426968266", "100452692612", "100461615323", "100439402763", "100409529858", "100489024381", "100484385578", "100407489223", "100448694135", "100411749750", "100479584358", "100448706111", "100468502856", "100410951982", "100443377741", "100487309468"]
=begin
do_not_touch_paths_cenx_ids = [
"100482568696", 
"100440329409", 
"100495555250", 
"100494084734", 
"100414336106", 
"100403192895", 
"100474648281", 
"100458730514", 
"100444475825", 
"100464690164", 
"100423777185", 
"100433326718", 
"100429304865", 
"100433388669", 
"100410559054", 
"100470117421", 
"100400356058", 
"100426968266", 
"100452692612", 
"100461615323", 
"100439402763", 
"100409529858", 
"100489024381", 
"100484385578", 
"100407489223", 
"100448694135", 
"100411749750", 
"100479584358", 
"100448706111", 
"100468502856"
]
=end
do_not_touch_paths = do_not_touch_paths_cenx_ids.collect {|e| Path.find_by_cenx_id(e).id if Path.find_by_cenx_id(e) != nil}.compact

if do_not_touch_paths_cenx_ids.size == do_not_touch_paths.size
  # 10% of Live Paths and 25% of them will have On Net OVC failing
  fake_alarms1(do_not_touch_paths, 10,25)
else
  puts "Could not find some of the do not touch Path's!!!"
end
