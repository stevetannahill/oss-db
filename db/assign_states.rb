
def set_object_state(obj, dependencies, test_thr, ready_thr, live_thr)
  puts ""
  puts ""
  puts "Testing threshold #{test_thr}, Ready threshold #{ready_thr}, Live threshold #{live_thr}."
  p = rand
  puts "Random state assign value is #{p.to_f}."

  name = "#{obj.class}(#{obj.cenx_id})"

  # Among the dependencies, get the provisioned states, and determine via PROV_STATES
  # index placement the "lowest" provisioned state of the dependencies, which represents
  # the highest state we can set the candidate object to.
  #
  # No dependencies mean there is no state limit
  puts "Checking dependencies..."
  dependencies.each do |dep|
    puts "Dependency #{dep.class}(#{dep.cenx_id}) is provisioned to #{dep.get_prov_state.name}(#{PROV_STATES.index(dep.get_prov_state.name)})."
  end
  candidate_state_limit = dependencies.empty? ? PROV_STATES.size : dependencies.map { |d| d.get_prov_state.name }.map { |s| PROV_STATES.index(s) }.min
  puts "Minimum dependency index is #{candidate_state_limit}"

  if ( p.to_f < test_thr.to_f )
    puts "#{name} is PENDING"

  else
    if PROV_STATES.index(TESTING) <= candidate_state_limit
      puts "Set #{name} to TESTING."
      obj.set_prov_state(ProvTesting)
    end

    if ( p.to_f >= ready_thr.to_f )
      if PROV_STATES.index(READY) <= candidate_state_limit
        puts "Set #{name} to READY."
        obj.set_prov_state(ProvReady)
      end

      if ( p.to_f >= live_thr.to_f )
        if PROV_STATES.index(LIVE) <= candidate_state_limit
          puts "Set #{name} to LIVE."
          obj.service_orders.last.set_order_state(OrderCustomerAccepted)
          obj.set_prov_state(ProvLive)
        end
      end
    end
  end

  return obj
end


def dependencies_suoort_state(dependencies, state)
  candidate_state_value = PROV_STATES.index(state)

  # check object dependencies, and return if they are in a "lower" prov state than
  # the object's candidate state
  dependencies.each do |dependency|
    if candidate_state_value.to_i > PROV_STATES.index(dependency.get_prov_state.name).to_i
      return false
    end
  end

  return true
end


def set_states_for_path(tunnel_state, uni, accs_segment, onovc, path)

  uni_test_thr   = 1.01
  uni_ready_thr  = 1.01
  uni_live_thr   = 1.01

  asegment_test_thr  = 1.01
  asegment_ready_thr = 1.01
  asegment_live_thr  = 1.01

  xsegment_test_thr  = 1.01
  xsegment_ready_thr = 1.01
  xsegment_live_thr  = 1.01

  if PENDING.eql?(tunnel_state)
    uni_test_thr  = 0.50
    uni_ready_thr = 0.80
    uni_live_thr  = 1.01
  elsif TESTING.eql?(tunnel_state)
    uni_test_thr  = 0.25
    uni_ready_thr = 0.65
    uni_live_thr  = 1.01
  elsif LIVE.eql?(tunnel_state)
    uni_test_thr  = 0.05
    uni_ready_thr = 0.10
    uni_live_thr  = 0.15
  end

  uni = set_object_state(uni, [], uni_test_thr, uni_ready_thr, uni_live_thr)

  uni_state = uni.get_prov_state.name
  if PENDING.eql?(uni_state)
    asegment_test_thr  = 1.01
    xsegment_test_thr  = 0.50
    xsegment_ready_thr = 0.90
    xsegment_live_thr  = 1.01

  elsif TESTING.eql?(uni_state)
    asegment_test_thr  = 0.00
    asegment_ready_thr = 1.01
    xsegment_test_thr  = 0.20
    xsegment_ready_thr = 0.60
    xsegment_live_thr  = 1.01

  elsif READY.eql?(uni_state)
    asegment_test_thr  = 0.00
    asegment_ready_thr = 0.00
    asegment_live_thr  = 1.01
    xsegment_test_thr  = 0.00
    xsegment_ready_thr = 0.00
    xsegment_live_thr  = 1.01

  elsif LIVE.eql?(uni_state)
    asegment_test_thr  = 0.00
    asegment_ready_thr = 0.00
    asegment_live_thr  = 0.00
    xsegment_test_thr  = 0.00
    xsegment_ready_thr = 0.00
    xsegment_live_thr  = 0.00
  end

  accs_segment_dependencies = accs_segment.ovc_end_point_ennis.map(&:demarc)
  accs_segment_dependencies.push(uni)
  accs_segment = set_object_state(accs_segment, accs_segment_dependencies, asegment_test_thr, asegment_ready_thr, asegment_live_thr)

  onovc_dependencies = onovc.on_net_ovc_end_point_ennis.map(&:demarc)
  onovc = set_object_state(onovc, onovc_dependencies, xsegment_test_thr, xsegment_ready_thr, xsegment_live_thr)
end


# Prevent state changes from accessing real hardware/SAM which would cause 
# state change failures
load "db/fake_go_in_service.rb"

PENDING = "Pending"
TESTING = "Testing"
READY = "Ready"
LIVE = "Live"
PROV_STATES = [ PENDING, TESTING, READY, LIVE ]

vgs_tnl_1 = Segment.find_by_cenx_id("101253634923")
vgs_tnl_2 = Segment.find_by_cenx_id("101260291836")
vgs_tnl_3 = Segment.find_by_cenx_id("101220745849")
dls_tnl_1 = Segment.find_by_cenx_id("101263393871")
dls_tnl_2 = Segment.find_by_cenx_id("101277979157")
dls_tnl_3 = Segment.find_by_cenx_id("101212974473")

#vgs_tnl_4 = Segment.find_by_cenx_id("101219057606")
#vgs_tnl_5 = Segment.find_by_cenx_id("101291350338")
#vgs_tnl_6 = Segment.find_by_cenx_id("101297763417")
#dls_tnl_4 = Segment.find_by_cenx_id("101247656644")
#dls_tnl_5 = Segment.find_by_cenx_id("101245309196")
#dls_tnl_6 = Segment.find_by_cenx_id("101224690078")

[ vgs_tnl_1, vgs_tnl_2, vgs_tnl_3, dls_tnl_1, dls_tnl_2, dls_tnl_3 ].each do |tunnel|

  tunnel_state = tunnel.get_prov_state.name

  tunnel.paths.each do |path|
    # exclude Dave's Paths
    daves_paths = [ "100474648281", "100458730514", "100470117421", "100400356058", "100410559054" ]
    unless daves_paths.include?(path.cenx_id)
      asegment = path.segments.find(:first, :conditions => [ "operator_network_id != ? and type = 'OffNetOvc'", tunnel.operator_network_id ])

      unless ( asegment.nil? )
        xsegment = path.on_net_ovcs.first
        uni = asegment.ovc_end_point_unis.first.demarc

        set_states_for_path(tunnel_state, uni, asegment, xsegment, path)
      end #unless
    end
  end #each path
  
end #each tunnel
