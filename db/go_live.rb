$first_alarm_time = Time.parse("1 Jan 2011 00:00")
$fake_output = false

load "#{Rails.root}/db/fake_go_in_service.rb"

puts "
Helper to make things go live
all_live - makes all Nodes, Ennis, Unis and Segments Live
nodes_live - make all Node live
service_provider_live sp - For the given ServiceProvider all all Nodes/Ennis/Unis and Segments Live
operator_network_live on - For the given OperatorNetwork all all Nodes/Ennis/Unis and Segments Live
path_live path - For the given Path all all Nodes/Ennis/Unis and Segments Live

If you hit Control-C ONCE the script will exit in a clean manner - If you hit it more than once then the script
will exit straight away
"

# Use a signal handler to catch control C but let code exit when it's in a clean state
$abort = 0
trap("SIGINT") {
  if $abort > 2
    puts "Exiting in an unclean state"
    exit!
  end
  $abort += 1
  puts "Please wait for script to exit cleanly. Hit Control-C again to exit now"
}

def cleanup_sm_states
  #a = SmHistory.all.collect {|ah| ah.alarm_records.collect {|o| Time.at(o.time_of_event/1000)}}.flatten.sort
  SmHistory.find_each do |ah|
    ar = ah.alarm_records.first
    ar_2nd = ah.alarm_records[1]
    num_records = ah.alarm_records.size
    if ar.state == AlarmSeverity::NOT_LIVE
      ar.time_of_event = $first_alarm_time.to_i*1000
      ar.save
      if ar_2nd != nil && (ar_2nd.state == AlarmSeverity::NOT_MONITORED || ar_2nd.state == AlarmSeverity::OK)
        ar_2nd.time_of_event = ($first_alarm_time+1.hour).to_i*1000
        ar_2nd.save
      else
        ar_2nd = nil
      end
      ah.reload
      if ar_2nd == nil
        ah.alarm_records[1..-1].each {|ar| ar.destroy}
      else
        ah.alarm_records[2..-1].each {|ar| ar.destroy}
      end
    else
      ah.alarm_records.each {|ar| ar.destroy}
      ah.add_event((($first_alarm_time+1.hours).to_f*1000).to_i, "ok", "")
    end
    
    ah.reload
    ar = ah.alarm_records.last
    
    ah.sm_ownerships.each do |eo|
      obj = eo.smable      
      obj.class.where("id = ?", obj.id).update_all(:sm_state => ar.state.to_s, :sm_details => ar.details, :sm_timestamp => ar.time_of_event)    
    end
  end
end
      
def check_ctv_grid_ids
  CosTestVector.find_each do |ctv|
    if !ctv.circuit_id.blank?
      if !ctv.grid_circuit_id
        puts "CTV #{ctv.id} #{ctv.circuit_id} does not have a grid_id"
      else
        if !ctv.ref_circuit_id.blank?
          if !ctv.ref_grid_circuit_id
            puts "CTV  #{ctv.id} #{ctv.circuit_id} does not have a ref_grid_id for #{ctv.ref_circuit_id}"
          else
            ref_ctv = CosTestVector.find_by_grid_circuit_id(ctv.ref_grid_circuit_id)
            if !ref_ctv 
              puts "CTV  #{ctv.id} #{ctv.circuit_id} with grid id #{ctv.ref_grid_circuit_id} does not ref an valid CTV"
            else
              ref_ctv = CosTestVector.find_by_circuit_id(ctv.ref_circuit_id)
              if !ref_ctv
                puts "CTV  #{ctv.id} #{ctv.circuit_id} with ref circuit id #{ctv.ref_circuit_id} has no CTV called #{ctv.ref_circuit_id} "
              elsif ref_ctv.grid_circuit_id != ctv.ref_grid_circuit_id
                puts "CTV  #{ctv.id} #{ctv.circuit_id} with ref grid id #{ctv.ref_grid_circuit_id} has a CTV but it does not match the grid id of the ref circuit"
              end
            end
          end
        end
      end
    end
  end
  nil
end


def reeval_not_live
  s = Segment.find_all_by_sm_state("Not Live")
  ahs = s.collect {|ss| ss.segment_end_points.collect {|ep| ep.cos_end_points[0].cos_test_vectors.collect {|ctv| ctv.alarm_histories}}}
  ahs.flatten!  
  ahs.each {|ah| ah.event_ownerships.each {|eo| eo.eventable.update_alarm([],true) if eo.eventable != nil}}
end

def reset_availability
  puts "Deleting Avail history"
  AvailabilityHistory.find_each {|ah| ah.alarm_records.destroy_all}
  puts "Adding cleared"; i = 0
  AvailabilityHistory.find_each {|ah| puts i;i=i+1;ah.add_event((($first_alarm_time+2.hours).to_f*1000).to_i.to_s, "cleared", "")}
  puts "Updating alarms"; i = 0
  AvailabilityHistory.find_each {|ah| puts i;i=i+1;ah.event_ownerships.each {|eo| eo.eventable.update_alarm([],true) if eo.eventable != nil}} 
  puts "Cleanup SM state" 
  cleanup_sm_states
  AvailabilityTime.destroy_all
end

def fix_alarm_times
  AlarmHistory.all.each {|p| z = p.alarm_records[0]; z.time_of_event = $first_alarm_time.to_i*1000;z.save }
  BrixAlarmHistory.all.each {|p| z = p.alarm_records[0]; z.time_of_event = $first_alarm_time.to_i*1000;z.save }
end


def reset_kpis
  ServiceProvider.all.each do |sp|
     ["delay", "delay_variation", "flr", "egress_util", "ingress_util"].each do |k|
       sp.kpis.find_all_by_name_and_actual_kpi(k, false).each {|kpi| kpi.destroy}
     end
   end
   ServiceProvider.all.each {|sp| sp.create_default_kpis}
end
  
def fix_kpis
  default_kpis = [    
    {:name => "delay", :for_klass => BxCosTestVector.name, :warning_time => 30.minutes, :error_time => 10.minute, :delta_t => 59.seconds, :warning_threshold => nil, :error_threshold => nil, :units => "ms", :actual_kpi => false},
    {:name => "delay_variation", :for_klass => BxCosTestVector.name, :warning_time => 30.minutes, :error_time => 10.minute, :delta_t => 59.seconds, :warning_threshold => nil, :error_threshold => nil, :units => "us", :actual_kpi => false},
    {:name => "flr", :for_klass => BxCosTestVector.name, :warning_time => 30.minutes, :error_time => 10.minute, :delta_t => 59.seconds, :warning_threshold => nil, :error_threshold => nil, :units => "%", :actual_kpi => false},
  
    {:name => "delay", :for_klass => SpirentCosTestVector.name, :warning_time => 30.minutes, :error_time => 10.minute, :delta_t => 5.minutes, :warning_threshold => nil, :error_threshold => nil, :units => "ms", :actual_kpi => false},
    {:name => "delay_variation", :for_klass => SpirentCosTestVector.name, :warning_time => 30.minutes, :error_time => 10.minute, :delta_t => 5.minutes, :warning_threshold => nil, :error_threshold => nil, :units => "us", :actual_kpi => false},
    {:name => "flr", :for_klass => SpirentCosTestVector.name, :warning_time => 30.minutes, :error_time => 10.minute, :delta_t => 5.minutes, :warning_threshold => nil, :error_threshold => nil, :units => "%", :actual_kpi => false}
  ]

  ServiceProvider.all.each do |sp|
    ["delay", "delay_variation", "flr"].each do |k|
      sp.kpis.find_all_by_name_and_for_klass_and_actual_kpi(k, BxCosTestVector.name, false).each {|kpi| kpi.destroy}
      sp.kpis.find_all_by_name_and_for_klass_and_actual_kpi(k, SpirentCosTestVector.name, false).each {|kpi| kpi.destroy}
      
    end
  end
  
  default_kpis.each {|kpi| ServiceProvider.all.each {|sp| sp.kpis << Kpi.create(kpi)}}
end

def check_graphs_by_month
  s = Time.parse("1 jan 2012")
  (0..2).each {|i| puts "Time #{s+i.months}"; check_graphs((s+i.months), ((s+i.months)+1.month))}
end

def check_graphs(start_time = nil, end_time = nil)
  
  start_time = Time.parse("13 Jan 2012") if start_time == nil
  end_time = Time.parse("16 Jan 2012") if end_time == nil

  SpirentCosTestVector.find_each do |ctv|
    stats = ctv.stats_data(start_time, end_time)
    stats.pad_results = false
    stats.stats_set_collection_interval(5.minutes)
    delay_attributes = stats.get_delay_attributes(:sla)
    delay_attributes += stats.get_delay_attributes(:troubleshooting)
    
    data = stats.stats_availability_results
    puts "#{ctv.circuit_id} Availabilty SLA #{data[:sla_monthly]} Actual #{data[:aggregate_avail]} data size #{data[:data].flatten.size}"
    avail_size = data[:data].flatten.size
     
    normalized_data = stats.stats_get_normalized_flr_data
    flr_data = stats.stats_flr_results(normalized_data)   
    flr_data.each do |cos_name, results|
      results[:data].each do |name, data|
        puts "#{ctv.circuit_id} #{cos_name} #{name} FLR SLA #{results[:sla][:sla_threshold]} Actual #{results[:aggregate_flr]} data size #{data.flatten.size}"
      end
    end

    t = Time.now; raw_data = stats.stats_get_delay_data(delay_attributes); puts "BJW timing #{Time.now-t}"
    [:delay, :delay_variation].each do |delay_type|
      stats.stats_delay_attribute_subset(delay_type, delay_attributes).each do |delay_attribute|               
        data = stats.stats_delay_results(raw_data, delay_attribute, true)  
        puts "#{ctv.circuit_id} No data Avail size #{avail_size}" if data[:data].blank?        
        if data[:sla].size != 0          
          data[:sla].each_pair do |cos_name, sla_info|            
            data[:data].each_pair do |test_name, results|              
              if results.has_key?(cos_name) && results[cos_name].size != 0      
                thresholds = sla_info.collect {|sla, value| [sla.to_s.gsub(/sla_/, "").humanize, value]}.flatten     
                #avg = results[cos_name].flatten.inject(0.0) {|r, d| r + d[:y]}/results[cos_name].flatten.size
                #warn_count = results[cos_name].flatten.collect {|d| d[:y] if d[:y] > sla_info[:sla_warning_threshold]}.compact.size if sla_info[:sla_warning_threshold]
                avg = 0.0
                warn_count = 0
                puts "#{ctv.circuit_id} #{cos_name} #{delay_attribute} #{test_name} SLA #{thresholds.inspect} Actual data size #{results[cos_name].flatten.size} warn_count #{warn_count} Avg: #{avg}"
              else
                puts "#{ctv.circuit_id} No data for #{delay_attribute}"  
              end
            end
          end
        else
          puts "#{ctv.circuit_id} No data for #{delay_attribute}"
        end
      end
    end

    puts
    if $abort > 0
      $abort = 0
      raise
    end
  end
  return nil
end
   

def change_prov_state(object, target_state)  
  object.reload
  success = false
  current_state = object.prov_state.to_generic(object.get_prov_state)
  states = object.prov_state.state_hierarchy - [ProvMaintenance, ProvDeleted]  
  start_idx = states.index(current_state)
  target_idx = states.index(target_state)
  
  if start_idx != nil && target_idx != nil
    success = true
    if start_idx < target_idx    
      # The object needs to be moved
      print "Changing state of #{object.class}:#{object.id} to " 
      states[start_idx+1..target_idx].each do |state|
        pre, post, pre_reason, post_reason = object.set_prov_state(state)
        if !pre
          puts "\nFailed to change state of #{object.class}:#{object.id} to #{state} pre condition #{pre_reason.inspect}"
          success = false
        elsif !post
          puts "\nFailed to change state of #{object.class}:#{object.id} to #{state} post condition #{pre_reason.inspect}"
          success = false
        else
          print "#{state}.."
        end
      end
      puts
    end
  else
    puts "Failed to find state #{current_state} or #{target_state} in #{states}"
  end
  return success
end

def change_order_state(object, target_state)
  object.reload
  success = false
  order = object.get_latest_order
  current_state = order.order_state.to_generic(order.get_order_state)
  states = order.order_state.state_hierarchy
  start_idx = states.index(current_state)
  target_idx = states.index(target_state)
  
  if start_idx != nil && target_idx != nil
    success = true
    if start_idx < target_idx    
      # The object needs to be moved
      print "Changing state of #{object.class}:#{object.id} to " 
      states[start_idx+1..target_idx].each do |state|
        pre, post, pre_reason, post_reason = order.set_order_state(state)
        if !pre
          puts "Failed to change state of #{object.class}:#{object.id} to #{state} pre condition #{pre_reason.inspect}"
          success = false
        elsif !post
          puts "Failed to change state of #{object.class}:#{object.id} to #{state} post condition #{pre_reason.inspect}"
          success = false
        else
          print "#{state}.."
        end
      end
      puts
    end
  else
    puts "Failed to find state #{current_state} or #{target_state} in #{states}"
  end
  return success
end

def bring_object_with_order_live(object)  
  if object.get_latest_order != nil    
    change_order_state(object, OrderDesignComplete)
    change_prov_state(object, ProvReady) 
    change_order_state(object, OrderCustomerAccepted)       
    change_prov_state(object, ProvLive)    
  else
    change_prov_state(object, ProvLive)
  end
  if $abort > 0
    $abort = 0
    puts "Aborting cleanly due to control C"
    raise
  end
end

def nodes_live
  puts "Bringing Node Insv"
  Node.find_each {|obj| change_prov_state(obj, ProvLive)}
  
  return nil
end

def all_live
  nodes_live

  puts "Bringing ENNIs in service"
  EnniNew.find_each {|obj| bring_object_with_order_live(obj)}

  puts "Bringing UNI in service"
  Uni.find_each {|obj| bring_object_with_order_live(obj)}

  puts "Bringing Segments in service"
  Segment.find_each {|obj| bring_object_with_order_live(obj)}
  
  return nil
end

def service_provider_live(sp)
  nodes = sp.enni_news.collect {|e| e.site.nodes}.flatten.uniq
  puts "Bringing Node Insv"
  nodes.each {|obj| change_prov_state(obj, ProvLive)}

  puts "Bringing ENNIs in service"
  sp.enni_news.each {|obj| bring_object_with_order_live(obj)}

  puts "Bringing UNI in service"
  sp.demarcs.where(:type => "Uni").each {|obj| bring_object_with_order_live(obj)}

  puts "Bringing Segments in service"
  sp.segments.each {|obj| bring_object_with_order_live(obj)}  
  
  return nil
end

def operator_network_live(on)
  nodes = on.enni_news.collect {|e| e.site.nodes}.flatten.uniq
  puts "Bringing Node Insv"
  nodes.each {|obj| change_prov_state(obj, ProvLive)}

  puts "Bringing ENNIs in service"
  on.enni_news.each {|obj| bring_object_with_order_live(obj)}

  puts "Bringing UNI in service"
  on.demarcs.where(:type => "Uni").each {|obj| bring_object_with_order_live(obj)}

  puts "Bringing Segments in service"
  on.segments.each {|obj| bring_object_with_order_live(obj)}
 
  return nil
end

def path_live(path)
  nodes = path.ennis.collect {|e| e.site.nodes}.flatten.uniq
  puts "Bringing Node Insv"
  nodes.each {|obj| change_prov_state(obj, ProvLive)}

  puts "Bringing ENNIs in service"
  path.ennis.each {|obj| bring_object_with_order_live(obj)}

  puts "Bringing UNI in service"
  path.demarcs.each {|obj| bring_object_with_order_live(obj)}

  puts "Bringing Segments in service"
  path.segments.each {|obj| bring_object_with_order_live(obj)}
 
  return nil
end

# Input param (units)
# num_frames_sent_per_sample (frames)
# flr_sla_guarantee_threshold (%)
#
# Output param (units)
# num_frames_sent_per_sample (frames)
# minor_threshold (%)
# minor_time (seconds)
# major_threshold (%)
# major_time (seconds)
NUM_SECONDS_PER_MONTH = 2592000
def minor_major_thresholds(num_frames_sent_per_sample, flr_sla_guarantee_threshold)
  minor_threshold = 1 / num_frames_sent_per_sample.to_f * 100
  major_threshold = minor_threshold * 2
  minor_time = NUM_SECONDS_PER_MONTH * num_frames_sent_per_sample * flr_sla_guarantee_threshold/100.0 / 8
  major_time = NUM_SECONDS_PER_MONTH * num_frames_sent_per_sample * flr_sla_guarantee_threshold/100.0 / 4
  return minor_threshold, minor_time, major_threshold, major_time
end

def fix_minor_major_thresholds
  # assuming same numbers for all SpirentCosTestVectors:
  minor_threshold, minor_time, major_threshold, major_time =
    minor_major_thresholds(num_frames_sent_per_sample=1500, flr_sla_guarantee_threshold=0.005)
  ServiceProvider.all.each do |sp|
    #sp = ServiceProvider.all.first
    sp.kpis.find_all_by_actual_kpi_and_for_klass(false, 'SpirentCosTestVector').each do |k|
      k.warning_time = minor_time.to_i
      k.error_time = major_time.to_i
      k.save!
    end
  end
  SpirentCosTestVector.find_each do |ctv|
    ctv.flr_warning_threshold = minor_threshold.to_s
    ctv.flr_error_threshold   = major_threshold.to_s
    ctv.save
  end

end

