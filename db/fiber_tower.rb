require 'factory_girl'
require 'faster_csv'

def process_template(template_file)
  FCSV.open(template_file) do |csv|
    csv.each do |row|
      colA = row[0]
      colC = row[2]

      # header section
      if colA.downcase.include?("service order template")

      # template inputs;
      # check that column B is blank to protect against an entity attribute
      # called "Inputs"
      elsif colA.downcase.eql?("inputs") and colC.blank?

      # provider section
      elsif colA.downcase.eql?("provider section")

      # ordered entity section
      elsif colA.downcase.eql?("ordered entity section")

      # entity definition section
      # "Entity" as a column A title is overused, so a check on column C to
      # ensure this is the Entity you are looking for.
      elsif colA.downcase.eql?("entity") and colC.blank?

      # constants section
      elsif colA.downcase.eql?("constants") and colC.blank?
      end unless colA.blank?
      puts "#{csv.lineno}::#{row[0]}"
    end
  end
end


def create_service_provider(prov_name)
  prov = Factory.create(:service_provider,
                        :prov_name => prov_name)

  ntwk_type = Factory.create(:operator_network_type,
                             :prov_name => "#{prov_name} Default Network Type",
                             :service_provider => prov)

end


process_template("/Users/paterson/FiberTower Sample Service Order Template.csv")

