
def set_object_state(path, obj, dependencies, test_thr, ready_thr, live_thr)
  name = "#{obj.class}(#{obj.cenx_id})"

  puts "Setting object state for #{name}"
  puts "Testing threshold #{test_thr}, Ready threshold #{ready_thr}, Live threshold #{live_thr}."
  p = rand
  puts "Random state assign value is #{p.to_f}."

  # Among the dependencies, get the provisioned states, and determine via PROV_STATES
  # index placement the "lowest" provisioned state of the dependencies, which represents
  # the highest state we can set the candidate object to.
  #
  # No dependencies mean there is no state limit
  puts "Checking dependencies..."
  dependencies.each do |dep|
    puts "Dependency #{dep.class}(#{dep.cenx_id}) is provisioned to #{dep.get_prov_state.name}(#{PROV_STATES.index(dep.get_prov_state.name)})."
  end
  candidate_state_limit = dependencies.empty? ? PROV_STATES.size : dependencies.map { |d| d.get_prov_state.name }.map { |s| PROV_STATES.index(s) }.min
  puts "Minimum dependency index is #{candidate_state_limit}"

  if ( p.to_f < test_thr.to_f )
    puts "#{name} is PENDING"
    puts "Path ORDER STATE is currently #{path.get_latest_order.get_order_state.name}!"


  else
    if PROV_STATES.index(TESTING) <= candidate_state_limit
      puts "Set #{name} to TESTING."
      obj.set_prov_state(ProvTesting)
      puts "Path ORDER STATE is currently #{path.get_latest_order.get_order_state.name}!"
    end

    if ( p.to_f >= ready_thr.to_f )
      if PROV_STATES.index(READY) <= candidate_state_limit
        puts "Set #{name} to READY."
        obj.set_prov_state(ProvReady)
        puts "Path ORDER STATE is currently #{path.get_latest_order.get_order_state.name}!"
      end

      if ( p.to_f >= live_thr.to_f )
        if PROV_STATES.index(LIVE) <= candidate_state_limit
          puts "Set #{name} to LIVE."
          obj.service_orders.last.set_order_state(OrderCustomerAccepted)
          puts "Path ORDER STATE is currently #{path.get_latest_order.get_order_state.name}!"

          obj.set_prov_state(ProvLive)
          puts "Path ORDER STATE is currently #{path.get_latest_order.get_order_state.name}!"
        end
      end
    end
  end

  return obj
end


def dependencies_suoort_state(dependencies, state)
  candidate_state_value = PROV_STATES.index(state)

  # check object dependencies, and return if they are in a "lower" prov state than
  # the object's candidate state
  dependencies.each do |dependency|
    if candidate_state_value.to_i > PROV_STATES.index(dependency.get_prov_state.name).to_i
      return false
    end
  end

  return true
end


def set_states_for_path(path)

  puts ""
  puts ""
  puts "Setting states for Path #{path.cenx_id}"
  puts "Path ORDER STATE is currently #{path.get_latest_order.get_order_state.name}!"

  path_test_thr   = 0.10
  path_ready_thr  = 0.20
  path_live_thr   = 0.30

  uni_test_thr   = 1.01
  uni_ready_thr  = 1.01
  uni_live_thr   = 1.01

  asegment_test_thr  = 1.01
  asegment_ready_thr = 1.01
  asegment_live_thr  = 1.01

  xsegment_test_thr  = 1.01
  xsegment_ready_thr = 1.01
  xsegment_live_thr  = 1.01

  puts "Testing threshold #{path_test_thr}, Ready threshold #{path_ready_thr}, Live threshold #{path_live_thr}."
  p = rand
  puts "Random state assign value is #{p.to_f}."

#  if PENDING.eql?(tunnel_state)
#    uni_test_thr  = 0.50
#    uni_ready_thr = 0.80
#    uni_live_thr  = 1.01
#  elsif TESTING.eql?(tunnel_state)
#    uni_test_thr  = 0.25
#    uni_ready_thr = 0.65
#    uni_live_thr  = 1.01
#  elsif LIVE.eql?(tunnel_state)
#    uni_test_thr  = 0.05
#    uni_ready_thr = 0.10
#    uni_live_thr  = 0.15
#  end

  if ( p.to_f < path_test_thr.to_f )
    puts "Set Path to PENDING"

  else
    puts "Set Path to TESTING."
    uni_test_thr   = 0.00

    asegment_test_thr  = 0.00

    xsegment_test_thr  = 0.00

    if ( p.to_f >= path_ready_thr.to_f )
      puts "Set Path to READY."
      uni_ready_thr  = 0.00

      asegment_ready_thr = 0.00

      xsegment_ready_thr = 0.00

      if ( p.to_f >= path_live_thr.to_f )
        puts "Set Path to LIVE."
        uni_live_thr   = 0.00

        asegment_live_thr  = 0.00

        xsegment_live_thr  = 0.00
      end
    end
  end


  path.off_net_ovcs.each do |offovc|
    uni = set_object_state(path, offovc.ovc_end_point_unis.first.demarc, [], uni_test_thr, uni_ready_thr, uni_live_thr)
    puts "Path ORDER STATE is currently #{path.get_latest_order.get_order_state.name}!"

    exchange = offovc.ovc_end_point_ennis.first.demarc

    offovc = set_object_state(path, offovc, [uni, exchange], asegment_test_thr, asegment_ready_thr, asegment_live_thr)
    puts "Path ORDER STATE is currently #{path.get_latest_order.get_order_state.name}!"

  end

  onovc = path.on_net_ovcs.first
  onovc_dependencies = onovc.on_net_ovc_end_point_ennis.map(&:demarc)
  onovc = set_object_state(path, onovc, onovc_dependencies, xsegment_test_thr, xsegment_ready_thr, xsegment_live_thr)
  puts "Path ORDER STATE is currently #{path.get_latest_order.get_order_state.name}!"

end


# Prevent state changes from accessing real hardware/SAM which would cause 
# state change failures

# This setting results in the Path order state reflecting correctly, but the Path
# provisioning state is always pending
load "db/fake_go_in_service.rb"

# This setting results in the Path order state moving when the first component is advanced,
# but the Path prov state is correct
#Stateful.test_env = true


PENDING = "Pending"
TESTING = "Testing"
READY = "Ready"
LIVE = "Live"
PROV_STATES = [ PENDING, TESTING, READY, LIVE ]

#vgs_tnl_1 = Segment.find_by_cenx_id("101253634923")
#vgs_tnl_2 = Segment.find_by_cenx_id("101260291836")
#vgs_tnl_3 = Segment.find_by_cenx_id("101220745849")
#dls_tnl_1 = Segment.find_by_cenx_id("101263393871")
#dls_tnl_2 = Segment.find_by_cenx_id("101277979157")
#dls_tnl_3 = Segment.find_by_cenx_id("101212974473")
#
##vgs_tnl_4 = Segment.find_by_cenx_id("101219057606")
##vgs_tnl_5 = Segment.find_by_cenx_id("101291350338")
##vgs_tnl_6 = Segment.find_by_cenx_id("101297763417")
##dls_tnl_4 = Segment.find_by_cenx_id("101247656644")
##dls_tnl_5 = Segment.find_by_cenx_id("101245309196")
##dls_tnl_6 = Segment.find_by_cenx_id("101224690078")
#
#[ vgs_tnl_1, vgs_tnl_2, vgs_tnl_3, dls_tnl_1, dls_tnl_2, dls_tnl_3 ].each do |tunnel|
#
#  tunnel_state = tunnel.get_prov_state.name

  Path.all.each do |path|
    # exclude Dave's Paths
    daves_paths = ["100482568696", "100440329409", "100495555250", "100494084734", "100414336106", "100403192895", "100474648281", "100458730514", "100444475825", "100464690164", "100423777185", "100433326718", "100429304865", "100433388669", "100410559054", "100470117421", "100400356058", "100426968266"]
    unless daves_paths.include?(path.cenx_id)
      set_states_for_path(path)
    end
  end #each path
  
#end #each tunnel
