# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130411132231) do

  create_table "access_permissions", :force => true do |t|
    t.integer  "operator_network_id"
    t.integer  "target_id"
    t.string   "permission"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "archive_statements", :force => true do |t|
    t.string "template",    :limit => 4096
    t.string "statement",   :limit => 4096
    t.string "stack_trace", :limit => 4096
  end

  create_table "automated_build_logs", :force => true do |t|
    t.string   "name"
    t.string   "summary"
    t.text     "summary_detail"
    t.text     "details"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.integer  "grid_id"
    t.string   "type"
    t.integer  "associated_object_id"
    t.string   "associated_object_type"
  end

  add_index "automated_build_logs", ["grid_id"], :name => "index_builder_logs_on_grid_id"
  add_index "automated_build_logs", ["name"], :name => "index_builder_logs_on_name"
  add_index "automated_build_logs", ["updated_at"], :name => "index_builder_logs_on_updated_at"

  create_table "availability_times", :force => true do |t|
    t.integer  "time",       :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "builder_logs", :force => true do |t|
    t.string   "name"
    t.string   "summary"
    t.text     "summary_detail"
    t.text     "details"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "grid_id"
  end

  add_index "builder_logs", ["grid_id"], :name => "index_builder_logs_on_grid_id"
  add_index "builder_logs", ["name"], :name => "index_builder_logs_on_name"
  add_index "builder_logs", ["updated_at"], :name => "index_builder_logs_on_updated_at"

  create_table "bulk_orders_service_orders", :force => true do |t|
    t.integer "service_order_id"
    t.integer "bulk_order_id"
  end

  add_index "bulk_orders_service_orders", ["bulk_order_id"], :name => "index_bulk_orders_service_orders_on_bulk_order_id"
  add_index "bulk_orders_service_orders", ["service_order_id"], :name => "index_bulk_orders_service_orders_on_service_order_id"

  create_table "bw_profile_types", :force => true do |t|
    t.string   "name"
    t.string   "bwp_type"
    t.integer  "class_of_service_type_id"
    t.string   "cir_range_notes"
    t.string   "cbs_range_notes"
    t.string   "eir_range_notes"
    t.string   "ebs_range_notes"
    t.string   "color_mode"
    t.string   "coupling_flag"
    t.text     "notes"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.boolean  "rate_limiting_performed"
    t.string   "rate_limiting_mechanism"
    t.string   "rate_limiting_layer"
    t.string   "rate_limiting_notes"
  end

  add_index "bw_profile_types", ["class_of_service_type_id"], :name => "index_bw_profile_types_on_class_of_service_type_id"

  create_table "cabinets", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "site_id"
  end

  add_index "cabinets", ["site_id"], :name => "index_cabinets_on_site_id"

  create_table "class_of_service_types", :force => true do |t|
    t.string   "name"
    t.integer  "operator_network_type_id"
    t.string   "cos_id_type_enni"
    t.string   "cos_mapping_enni_ingress"
    t.string   "cos_mapping_uni_ingress"
    t.string   "availability"
    t.string   "frame_loss_ratio"
    t.string   "mttr_hrs"
    t.text     "notes"
    t.datetime "created_at",                                        :null => false
    t.datetime "updated_at",                                        :null => false
    t.string   "cos_id_type_uni",                :default => "pcp"
    t.string   "cos_marking_enni_egress"
    t.string   "cos_marking_uni_egress"
    t.string   "cos_marking_enni_egress_yellow", :default => "N/A"
    t.string   "cos_marking_uni_egress_yellow",  :default => "N/A"
    t.string   "type"
  end

  add_index "class_of_service_types", ["operator_network_type_id"], :name => "index_class_of_service_types_on_operator_network_type_id"

  create_table "class_of_service_types_ethernet_service_types", :id => false, :force => true do |t|
    t.integer "ethernet_service_type_id"
    t.integer "class_of_service_type_id"
  end

  add_index "class_of_service_types_ethernet_service_types", ["class_of_service_type_id"], :name => "index_costs_ests_on_cost_id"
  add_index "class_of_service_types_ethernet_service_types", ["ethernet_service_type_id"], :name => "index_costs_ests_on_est_id"

  create_table "contacts", :force => true do |t|
    t.string   "name"
    t.string   "position"
    t.string   "work_phone"
    t.string   "mobile_phone"
    t.string   "e_mail"
    t.string   "fax"
    t.integer  "service_provider_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "address"
  end

  add_index "contacts", ["service_provider_id"], :name => "index_contacts_on_service_provider_id"

  create_table "cos_end_points", :force => true do |t|
    t.integer  "segment_end_point_id"
    t.integer  "cos_instance_id"
    t.string   "ingress_mapping"
    t.string   "ingress_cir_kbps"
    t.string   "ingress_eir_kbps"
    t.string   "ingress_cbs_kB"
    t.string   "ingress_ebs_kB"
    t.datetime "created_at",                                                      :null => false
    t.datetime "updated_at",                                                      :null => false
    t.integer  "connected_cos_end_point_id"
    t.string   "type"
    t.string   "egress_marking"
    t.string   "egress_cir_kbps"
    t.string   "egress_eir_kbps"
    t.string   "egress_cbs_kB"
    t.string   "egress_ebs_kB"
    t.string   "ingress_rate_limiting",                   :default => "Policing"
    t.string   "egress_rate_limiting",                    :default => "None"
    t.integer  "event_record_id"
    t.string   "sm_state"
    t.string   "sm_details"
    t.integer  "sm_timestamp",               :limit => 8
    t.string   "public_id",                                                       :null => false
  end

  add_index "cos_end_points", ["connected_cos_end_point_id"], :name => "index_cos_end_points_on_connected_cos_end_point_id"
  add_index "cos_end_points", ["cos_instance_id"], :name => "index_cos_end_points_on_cos_instance_id"
  add_index "cos_end_points", ["event_record_id"], :name => "index_cos_end_points_on_event_record_id"
  add_index "cos_end_points", ["public_id"], :name => "index_cos_end_points_on_public_id", :unique => true
  add_index "cos_end_points", ["segment_end_point_id"], :name => "index_cos_end_points_on_ovc_end_point_new_id"

  create_table "cos_end_points_cos_end_points", :id => false, :force => true do |t|
    t.integer "cos_end_point_id"
    t.integer "self_id"
  end

  add_index "cos_end_points_cos_end_points", ["cos_end_point_id"], :name => "index_cos_end_points_cos_end_points_on_cos_end_point_id"
  add_index "cos_end_points_cos_end_points", ["self_id"], :name => "index_cos_end_points_cos_end_points_on_self_id"

  create_table "cos_instances", :force => true do |t|
    t.integer  "class_of_service_type_id"
    t.integer  "segment_id"
    t.string   "notes"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.string   "type"
  end

  add_index "cos_instances", ["class_of_service_type_id"], :name => "index_cos_instances_on_class_of_service_type_id"
  add_index "cos_instances", ["segment_id"], :name => "index_cos_instances_on_ovc_new_id"

  create_table "cos_test_vectors", :force => true do |t|
    t.string   "type"
    t.string   "test_type"
    t.string   "test_instance_class_id"
    t.string   "notes"
    t.integer  "cos_end_point_id"
    t.integer  "service_level_guarantee_type_id"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.string   "delay_error_threshold"
    t.string   "dv_error_threshold"
    t.string   "delay_warning_threshold"
    t.string   "dv_warning_threshold"
    t.string   "service_instance_id"
    t.string   "sla_id"
    t.string   "test_instance_id"
    t.string   "flr_error_threshold"
    t.string   "flr_warning_threshold"
    t.string   "availability_guarantee"
    t.string   "cenx_id"
    t.string   "flr_sla_guarantee"
    t.string   "dv_sla_guarantee"
    t.string   "delay_sla_guarantee"
    t.string   "circuit_id"
    t.string   "ref_circuit_id"
    t.integer  "event_record_id"
    t.string   "sm_state"
    t.string   "sm_details"
    t.integer  "sm_timestamp",                    :limit => 8
    t.string   "last_hop_circuit_id"
    t.string   "flr_reference"
    t.string   "delay_reference"
    t.string   "delay_min_reference"
    t.string   "delay_max_reference"
    t.string   "dv_reference"
    t.string   "dv_min_reference"
    t.string   "dv_max_reference"
    t.string   "loopback_address"
    t.string   "circuit_id_format"
    t.integer  "grid_circuit_id"
    t.integer  "protection_path_id"
    t.integer  "ref_grid_circuit_id"
    t.string   "public_id",                                    :null => false
  end

  add_index "cos_test_vectors", ["circuit_id"], :name => "cos_test_vector_by_circuit_id"
  add_index "cos_test_vectors", ["cos_end_point_id"], :name => "index_cos_test_vectors_on_cos_end_point_id"
  add_index "cos_test_vectors", ["event_record_id"], :name => "index_cos_test_vectors_on_event_record_id"
  add_index "cos_test_vectors", ["grid_circuit_id"], :name => "cos_test_vector_by_grid_circuit_id"
  add_index "cos_test_vectors", ["last_hop_circuit_id"], :name => "index_cos_test_vectors_on_last_hop_circuit_id"
  add_index "cos_test_vectors", ["protection_path_id"], :name => "index_cos_test_vectors_on_protection_path_id"
  add_index "cos_test_vectors", ["public_id"], :name => "index_cos_test_vectors_on_public_id", :unique => true
  add_index "cos_test_vectors", ["ref_circuit_id"], :name => "index_cos_test_vectors_on_ref_circuit_id"
  add_index "cos_test_vectors", ["ref_grid_circuit_id"], :name => "index_cos_test_vectors_on_ref_grid_circuit_id"
  add_index "cos_test_vectors", ["service_instance_id"], :name => "index_cos_test_vectors_on_service_instance_id"
  add_index "cos_test_vectors", ["service_level_guarantee_type_id"], :name => "index_cos_test_vectors_on_service_level_guarantee_type_id"
  add_index "cos_test_vectors", ["sla_id"], :name => "index_cos_test_vectors_on_sla_id"
  add_index "cos_test_vectors", ["test_instance_class_id"], :name => "index_cos_test_vectors_on_test_instance_class_id"
  add_index "cos_test_vectors", ["test_instance_id"], :name => "index_cos_test_vectors_on_test_instance_id"
  add_index "cos_test_vectors", ["type", "cenx_id"], :name => "index_cos_test_vectors_on_type_and_cenx_id"
  add_index "cos_test_vectors", ["type", "circuit_id"], :name => "index_cos_test_vectors_on_type_and_circuit_id"

  create_table "demarc_types", :force => true do |t|
    t.string   "type"
    t.string   "name"
    t.string   "demarc_type_type"
    t.integer  "operator_network_type_id"
    t.string   "physical_medium"
    t.string   "physical_medium_notes"
    t.string   "auto_negotiate"
    t.integer  "mtu"
    t.string   "max_num_segments"
    t.string   "max_num_segments_notes"
    t.string   "connected_device_type"
    t.string   "remarks"
    t.string   "lldp_untagged"
    t.string   "stp_untagged"
    t.string   "rstp_untagged"
    t.string   "mstp_untagged"
    t.string   "evst_untagged"
    t.string   "rpvst_untagged"
    t.string   "eaps_untagged"
    t.string   "pause_untagged"
    t.string   "lacp_untagged"
    t.string   "garp_untagged"
    t.string   "port_auth_untagged"
    t.string   "lacp_notes_untagged"
    t.string   "lamp_untagged"
    t.string   "link_oam_untagged"
    t.string   "mrp_b_untagged"
    t.string   "cisco_bpdu_untagged"
    t.string   "default_l2cp_untagged"
    t.string   "lldp_tagged"
    t.string   "stp_tagged"
    t.string   "rstp_tagged"
    t.string   "mstp_tagged"
    t.string   "pause_tagged"
    t.string   "lacp_tagged"
    t.string   "garp_tagged"
    t.string   "port_auth_tagged"
    t.string   "lacp_notes_tagged"
    t.string   "lamp_tagged"
    t.string   "link_oam_tagged"
    t.string   "mrp_b_tagged"
    t.string   "cisco_bpdu_tagged"
    t.string   "default_l2cp_tagged"
    t.string   "frame_format"
    t.string   "frame_format_notes"
    t.boolean  "consistent_ethertype"
    t.string   "outer_tag_segment_mapping"
    t.boolean  "lag_supported"
    t.string   "lag_type"
    t.boolean  "lacp_supported"
    t.boolean  "lacp_priority_support"
    t.string   "protection_notes"
    t.boolean  "ah_supported"
    t.boolean  "ag_supported"
    t.string   "oam_notes"
    t.boolean  "multi_link_support"
    t.string   "max_links_supported"
    t.boolean  "bundling"
    t.boolean  "ato_bundling"
    t.boolean  "cfm_supported"
    t.boolean  "service_multiplexing"
    t.string   "reflection_mechanisms"
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
    t.string   "lag_control"
    t.string   "ls_access_solution_model",          :default => "N/A"
    t.string   "turnup_reflection_mechanisms"
    t.boolean  "service_rate_reflection_supported"
    t.string   "max_reflection_rate"
    t.string   "public_id",                                            :null => false
  end

  add_index "demarc_types", ["operator_network_type_id"], :name => "index_demarc_types_on_operator_network_type_id"
  add_index "demarc_types", ["public_id"], :name => "index_demarc_types_on_public_id", :unique => true

  create_table "demarc_types_ethernet_service_types", :id => false, :force => true do |t|
    t.integer "ethernet_service_type_id"
    t.integer "demarc_type_id"
  end

  add_index "demarc_types_ethernet_service_types", ["demarc_type_id"], :name => "index_dts_ests_on_dt_id"
  add_index "demarc_types_ethernet_service_types", ["ethernet_service_type_id"], :name => "index_dts_ests_on_est_id"

  create_table "demarcs", :force => true do |t|
    t.string   "type"
    t.string   "cenx_id"
    t.string   "cenx_demarc_type"
    t.text     "notes"
    t.integer  "operator_network_id"
    t.string   "address"
    t.string   "port_name"
    t.string   "physical_medium"
    t.string   "connected_device_type"
    t.string   "connected_device_sw_version"
    t.string   "reflection_mechanism"
    t.string   "mon_loopback_address"
    t.string   "end_user_name"
    t.boolean  "is_new_construction"
    t.string   "contact_info"
    t.datetime "created_at",                                                                                          :null => false
    t.datetime "updated_at",                                                                                          :null => false
    t.integer  "demarc_type_id"
    t.integer  "max_num_segments"
    t.string   "ether_type"
    t.string   "protection_type"
    t.integer  "lag_id"
    t.string   "fiber_handoff_type"
    t.boolean  "auto_negotiate"
    t.boolean  "ah_supported"
    t.string   "lag_mac_primary"
    t.integer  "emergency_contact_id"
    t.string   "lag_mac_secondary"
    t.string   "port_enap_type",                                                              :default => "QINQ"
    t.string   "lag_mode"
    t.integer  "cir_limit",                                                                   :default => 100
    t.integer  "eir_limit",                                                                   :default => 300
    t.string   "cenx_name_prefix"
    t.string   "demarc_icon",                                                                 :default => "building"
    t.integer  "on_behalf_of_service_provider_id"
    t.integer  "event_record_id"
    t.string   "sm_state"
    t.string   "sm_details"
    t.integer  "sm_timestamp",                     :limit => 8
    t.string   "prov_name"
    t.text     "prov_notes"
    t.integer  "prov_timestamp",                   :limit => 8
    t.string   "order_name"
    t.text     "order_notes"
    t.integer  "order_timestamp",                  :limit => 8
    t.string   "near_side_clli",                                                              :default => "TBD"
    t.string   "far_side_clli",                                                               :default => "TBD"
    t.decimal  "latitude",                                      :precision => 9, :scale => 6, :default => 0.0
    t.decimal  "longitude",                                     :precision => 9, :scale => 6, :default => 0.0
    t.integer  "site_id"
    t.string   "public_id",                                                                                           :null => false
  end

  add_index "demarcs", ["cenx_id"], :name => "index_demarcs_on_cenx_id"
  add_index "demarcs", ["demarc_type_id"], :name => "index_demarcs_on_demarc_type_id"
  add_index "demarcs", ["emergency_contact_id"], :name => "index_demarcs_on_emergency_contact_id"
  add_index "demarcs", ["event_record_id"], :name => "index_demarcs_on_event_record_id"
  add_index "demarcs", ["lag_id"], :name => "index_demarcs_on_lag_id"
  add_index "demarcs", ["on_behalf_of_service_provider_id"], :name => "index_demarcs_on_on_behalf_of_service_provider_id"
  add_index "demarcs", ["operator_network_id"], :name => "index_demarcs_on_operator_network_id"
  add_index "demarcs", ["public_id"], :name => "index_demarcs_on_public_id", :unique => true
  add_index "demarcs", ["site_id"], :name => "index_demarcs_on_site_id"

  create_table "email_configs", :force => true do |t|
    t.string  "override_event_email_address",    :limit => 1024, :default => "--- []\n\n",                             :null => false
    t.string  "override_order_email_address",    :limit => 1024, :default => "--- []\n\n",                             :null => false
    t.string  "debug_bcc_event_email_addresses", :limit => 1024, :default => "--- []\n\n",                             :null => false
    t.string  "debug_bcc_order_email_addresses", :limit => 1024, :default => "--- []\n\n",                             :null => false
    t.string  "cenx_ops_request",                :limit => 1024, :default => "--- \n- ops-request@support.cenx.com\n", :null => false
    t.string  "cenx_ops",                        :limit => 1024, :default => "--- \n- support@cenx.com\n",             :null => false
    t.string  "cenx_eng",                        :limit => 1024, :default => "--- \n- eng@support.cenx.com\n",         :null => false
    t.string  "cenx_cust_ops",                   :limit => 1024, :default => "--- \n- customerops@support.cenx.com\n", :null => false
    t.string  "cenx_prov",                       :limit => 1024, :default => "--- \n- Provisioning@CENX.com\n",        :null => false
    t.string  "cenx_billing",                    :limit => 1024, :default => "--- \n- \"\"\n",                         :null => false
    t.boolean "enable_delivery",                                 :default => false,                                    :null => false
    t.boolean "development_enable_delivery",                     :default => false,                                    :null => false
    t.boolean "enable_order_email",                              :default => false
    t.boolean "enable_fault_email",                              :default => false
  end

  create_table "ethernet_service_types", :force => true do |t|
    t.integer  "operator_network_type_id"
    t.string   "name"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.string   "notes"
    t.string   "public_id",                :null => false
  end

  add_index "ethernet_service_types", ["operator_network_type_id"], :name => "index_ethernet_service_types_on_operator_network_type_id"
  add_index "ethernet_service_types", ["public_id"], :name => "index_ethernet_service_types_on_public_id", :unique => true

  create_table "ethernet_service_types_segment_end_point_types", :id => false, :force => true do |t|
    t.integer "ethernet_service_type_id"
    t.integer "segment_end_point_type_id"
  end

  add_index "ethernet_service_types_segment_end_point_types", ["ethernet_service_type_id"], :name => "index_ests_ovctes_on_est_id"
  add_index "ethernet_service_types_segment_end_point_types", ["segment_end_point_type_id"], :name => "index_ests_ovctes_on_ovcet_id"

  create_table "ethernet_service_types_segment_types", :id => false, :force => true do |t|
    t.integer "ethernet_service_type_id"
    t.integer "segment_type_id"
  end

  add_index "ethernet_service_types_segment_types", ["ethernet_service_type_id"], :name => "index_ests_ovcts_on_est_id"
  add_index "ethernet_service_types_segment_types", ["segment_type_id"], :name => "index_ests_ovcts_on_ovct_id"

  create_table "event_histories", :force => true do |t|
    t.string   "event_filter"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type"
    t.text     "alarm_mapping"
    t.integer  "retention_time"
  end

  add_index "event_histories", ["event_filter"], :name => "index_event_histories_on_event_filter"

  create_table "event_ownerships", :force => true do |t|
    t.integer "eventable_id"
    t.string  "eventable_type"
    t.integer "event_history_id"
  end

  add_index "event_ownerships", ["event_history_id"], :name => "index_event_ownerships_on_event_history_id"
  add_index "event_ownerships", ["eventable_id", "eventable_type"], :name => "index_event_ownerships_on_eventable_id_and_eventable_type"

  create_table "event_records", :force => true do |t|
    t.string   "state"
    t.string   "details"
    t.integer  "event_history_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type"
    t.string   "original_state"
    t.integer  "time_of_event",    :limit => 8, :null => false
  end

  add_index "event_records", ["event_history_id", "time_of_event"], :name => "index_event_records_on_event_history_id_and_time_of_event"
  add_index "event_records", ["event_history_id", "type"], :name => "index_event_records_on_event_history_id_and_type"

  create_table "exception_sla_network_types", :force => true do |t|
    t.integer "time_declared"
    t.string  "network_type"
    t.float   "flr"
    t.float   "fd"
    t.float   "fdv"
    t.integer "operator_network_type_id"
    t.string  "severity"
  end

  add_index "exception_sla_network_types", ["operator_network_type_id"], :name => "index_exception_sla_network_types_on_operator_network_type_id"
  add_index "exception_sla_network_types", ["time_declared"], :name => "index_exception_sla_network_types_on_time_declared"

  create_table "exception_times", :force => true do |t|
    t.integer  "time",       :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "exception_utilization_cos_end_points", :force => true do |t|
    t.integer "time_declared"
    t.float   "value"
    t.integer "cos_end_point_id"
    t.integer "time_over"
    t.float   "average_rate_while_over_threshold_mbps"
    t.string  "severity"
    t.string  "direction"
  end

  add_index "exception_utilization_cos_end_points", ["cos_end_point_id"], :name => "index_exception_utilization_cos_end_points_on_cos_end_point_id"
  add_index "exception_utilization_cos_end_points", ["time_declared"], :name => "index_exception_utilization_cos_end_points_on_time_declared"

  create_table "exception_utilization_ennis", :force => true do |t|
    t.integer "time_declared"
    t.float   "value"
    t.integer "enni_new_id"
    t.integer "time_over"
    t.float   "average_rate_while_over_threshold_mbps"
    t.string  "severity"
    t.string  "direction"
  end

  add_index "exception_utilization_ennis", ["enni_new_id"], :name => "index_exception_utilization_ennis_on_enni_new_id"
  add_index "exception_utilization_ennis", ["time_declared"], :name => "index_exception_utilization_ennis_on_time_declared"

  create_table "exchange_locations", :force => true do |t|
    t.string   "name"
    t.string   "address"
    t.integer  "exchange_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "contact_info"
    t.string   "site_host"
    t.string   "floor"
    t.string   "clli"
    t.string   "icsc"
    t.string   "network_address"
    t.string   "broadcast_address"
    t.string   "host_range"
    t.string   "hosts_per_subnet"
    t.text     "notes"
    t.string   "node_prefix"
    t.string   "postal_zip_code"
    t.string   "emergency_contact_info"
    t.text     "site_access_notes"
    t.string   "city"
    t.string   "state_province"
    t.string   "location_type"
  end

  add_index "exchange_locations", ["exchange_id"], :name => "index_exchange_locations_on_exchange_id"

  create_table "exchanges", :force => true do |t|
    t.string   "name"
    t.string   "country"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "service_id_low"
    t.integer  "service_id_high"
    t.integer  "cenx_operator_network_id"
    t.string   "exchange_type",            :default => "StandardExchange7750"
  end

  add_index "exchanges", ["cenx_operator_network_id"], :name => "index_exchanges_on_cenx_operator_network_id"

  create_table "fallout_exceptions", :force => true do |t|
    t.integer  "timestamp",         :limit => 8
    t.integer  "clear_timestamp",   :limit => 8
    t.integer  "fallout_item_id"
    t.string   "fallout_item_type"
    t.string   "severity"
    t.string   "exception_type"
    t.string   "data_source"
    t.text     "details"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.string   "category"
  end

  add_index "fallout_exceptions", ["fallout_item_id"], :name => "index_fallout_exceptions_on_fallout_item_id"

  create_table "faye_subscribe_keys", :force => true do |t|
    t.string   "channel"
    t.string   "key"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "faye_subscribe_keys", ["channel", "key"], :name => "index_faye_subscribe_keys_on_channel_and_key", :unique => true

  create_table "inventory_items", :force => true do |t|
    t.string   "description"
    t.string   "cenx_naming_std"
    t.string   "vendor"
    t.string   "cabinet_position"
    t.string   "make_model"
    t.string   "serial_number"
    t.integer  "site_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "cenx_po_number"
    t.date     "service_start_date"
    t.date     "service_end_date"
    t.string   "notes"
  end

  add_index "inventory_items", ["site_id"], :name => "index_inventory_items_on_site_id"

  create_table "kpi_ownerships", :force => true do |t|
    t.integer "kpiable_id"
    t.string  "kpiable_type"
    t.integer "kpi_id"
    t.integer "parent_id"
    t.string  "parent_type"
  end

  add_index "kpi_ownerships", ["kpi_id"], :name => "index_kpi_ownerships_on_kpi_id"
  add_index "kpi_ownerships", ["kpiable_id", "kpiable_type", "parent_id", "parent_type"], :name => "index_kpi_ownerships"

  create_table "kpis", :force => true do |t|
    t.string   "name"
    t.string   "for_klass"
    t.string   "units"
    t.float    "warning_threshold"
    t.float    "error_threshold"
    t.integer  "warning_time"
    t.integer  "error_time"
    t.integer  "delta_t"
    t.boolean  "actual_kpi"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "kpis", ["name", "for_klass", "actual_kpi"], :name => "index_kpi_name_klass_actual"

  create_table "member_attr_controls", :force => true do |t|
    t.integer  "member_attr_id"
    t.string   "description"
    t.integer  "execution_order"
    t.string   "action"
    t.text     "code"
    t.string   "info"
    t.text     "notes"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "member_attr_controls", ["member_attr_id"], :name => "index_member_attr_controls_on_member_attr_id"

  create_table "member_attr_instances", :force => true do |t|
    t.integer  "member_attr_id"
    t.integer  "affected_entity_id"
    t.string   "affected_entity_type"
    t.string   "value",                :default => "TBD"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.string   "type"
    t.integer  "owner_id"
    t.string   "owner_type"
  end

  add_index "member_attr_instances", ["affected_entity_id"], :name => "index_member_attr_instances_on_affected_entity_id"
  add_index "member_attr_instances", ["affected_entity_type", "type", "value"], :name => "index_aff_type_and_type_and_value"
  add_index "member_attr_instances", ["member_attr_id"], :name => "index_member_attr_instances_on_member_attr_id"
  add_index "member_attr_instances", ["owner_id"], :name => "index_member_attr_instances_on_owner_id"

  create_table "member_attrs", :force => true do |t|
    t.string   "name"
    t.integer  "affected_entity_id"
    t.string   "affected_entity_type"
    t.string   "type"
    t.string   "syntax"
    t.string   "value_range"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.boolean  "allow_blank"
    t.boolean  "disabled"
    t.string   "default_value"
  end

  add_index "member_attrs", ["affected_entity_id"], :name => "index_member_attrs_on_affected_entity_id"

  create_table "metrics_monthly_cos_test_vectors", :force => true do |t|
    t.integer "period_start_epoch",  :limit => 8
    t.integer "time_declared_epoch", :limit => 8
    t.float   "metric"
    t.integer "minor_time_over"
    t.integer "major_time_over"
    t.integer "grid_circuit_id"
    t.string  "metric_type"
    t.string  "severity"
  end

  add_index "metrics_monthly_cos_test_vectors", ["grid_circuit_id", "metric_type", "period_start_epoch"], :name => "metrics_cos_test_vector_monthly_search", :unique => true
  add_index "metrics_monthly_cos_test_vectors", ["grid_circuit_id", "time_declared_epoch", "severity"], :name => "index_rollup_metrics_cos_test_vectors_monthly_grid_time_sev"
  add_index "metrics_monthly_cos_test_vectors", ["grid_circuit_id", "time_declared_epoch"], :name => "index_rollup_metrics_cos_test_vectors_monthly_grid_time"

  create_table "metrics_weekly_cos_test_vectors", :force => true do |t|
    t.integer "period_start_epoch",  :limit => 8
    t.integer "time_declared_epoch", :limit => 8
    t.float   "metric"
    t.integer "minor_time_over"
    t.integer "major_time_over"
    t.integer "grid_circuit_id"
    t.string  "metric_type"
    t.string  "severity"
  end

  add_index "metrics_weekly_cos_test_vectors", ["grid_circuit_id", "metric_type", "period_start_epoch"], :name => "metrics_cos_test_vector_weekly_search", :unique => true
  add_index "metrics_weekly_cos_test_vectors", ["grid_circuit_id", "time_declared_epoch", "severity"], :name => "index_rollup_metrics_cos_test_vectors_weekly_grid_time_sev"
  add_index "metrics_weekly_cos_test_vectors", ["grid_circuit_id", "time_declared_epoch"], :name => "index_rollup_metrics_cos_test_vectors_weekly_grid_time"

  create_table "mtc_ownerships", :force => true do |t|
    t.integer "mtcable_id"
    t.string  "mtcable_type"
    t.integer "mtc_history_id"
  end

  add_index "mtc_ownerships", ["mtc_history_id"], :name => "index_mtc_ownerships_on_mtc_history_id"
  add_index "mtc_ownerships", ["mtcable_id", "mtcable_type"], :name => "index_mtc_ownerships_on_mtcable_id_and_mtcable_type"

  create_table "network_management_servers", :force => true do |t|
    t.string   "name"
    t.integer  "network_manager_id"
    t.string   "primary_ip"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "network_management_servers", ["network_manager_id"], :name => "index_network_management_servers_on_network_manager_id"

  create_table "network_managers", :force => true do |t|
    t.string   "name"
    t.string   "nm_type"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.string   "url"
    t.integer  "data_archive_id", :default => 1
    t.integer  "event_record_id"
    t.string   "password"
    t.string   "username"
  end

  add_index "network_managers", ["data_archive_id"], :name => "index_network_managers_on_data_archive_id"
  add_index "network_managers", ["event_record_id"], :name => "index_network_managers_on_event_record_id"

  create_table "nodes", :force => true do |t|
    t.string   "name"
    t.string   "node_type"
    t.string   "primary_ip"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.integer  "site_id"
    t.string   "secondary_ip"
    t.string   "net_mask"
    t.string   "gateway_ip"
    t.string   "notes"
    t.integer  "cabinet_id"
    t.string   "loopback_ip"
    t.integer  "network_manager_id"
    t.integer  "event_record_id"
    t.string   "sm_state"
    t.string   "sm_details"
    t.integer  "sm_timestamp",       :limit => 8
    t.string   "prov_name"
    t.text     "prov_notes"
    t.integer  "prov_timestamp",     :limit => 8
    t.string   "order_name"
    t.text     "order_notes"
    t.integer  "order_timestamp",    :limit => 8
    t.string   "public_id",                       :null => false
  end

  add_index "nodes", ["cabinet_id"], :name => "index_nodes_on_cabinet_id"
  add_index "nodes", ["event_record_id"], :name => "index_nodes_on_event_record_id"
  add_index "nodes", ["network_manager_id"], :name => "index_nodes_on_network_manager_id"
  add_index "nodes", ["public_id"], :name => "index_nodes_on_public_id", :unique => true
  add_index "nodes", ["site_id"], :name => "index_nodes_on_site_id"

  create_table "on_net_ovc_end_point_ennis_qos_policies", :id => false, :force => true do |t|
    t.integer "qos_policy_id"
    t.integer "on_net_ovc_end_point_enni_id"
  end

  add_index "on_net_ovc_end_point_ennis_qos_policies", ["on_net_ovc_end_point_enni_id"], :name => "index_qosps_ovces_on_ovce_id"
  add_index "on_net_ovc_end_point_ennis_qos_policies", ["qos_policy_id"], :name => "index_qosps_ovces_on_qosp_id"

  create_table "operator_network_types", :force => true do |t|
    t.string   "name"
    t.integer  "service_provider_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "notes"
    t.string   "public_id",           :null => false
  end

  add_index "operator_network_types", ["public_id"], :name => "index_operator_network_types_on_public_id", :unique => true
  add_index "operator_network_types", ["service_provider_id"], :name => "index_operator_network_types_on_service_provider_id"

  create_table "operator_network_types_users", :id => false, :force => true do |t|
    t.integer "operator_network_type_id"
    t.integer "user_id"
  end

  add_index "operator_network_types_users", ["operator_network_type_id", "user_id"], :name => "operator_network_types_users_index", :unique => true

  create_table "operator_networks", :force => true do |t|
    t.string   "name"
    t.boolean  "is_processing"
    t.string   "job_type"
    t.integer  "job_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.integer  "operator_network_type_id"
    t.string   "public_id",                :null => false
  end

  add_index "operator_networks", ["job_id"], :name => "index_operator_networks_on_job_id"
  add_index "operator_networks", ["operator_network_type_id"], :name => "index_operator_networks_on_operator_network_type_id"
  add_index "operator_networks", ["public_id"], :name => "index_operator_networks_on_public_id", :unique => true

  create_table "order_annotations", :force => true do |t|
    t.datetime "date"
    t.text     "note"
    t.string   "severity"
    t.integer  "service_order_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "order_annotations", ["service_order_id"], :name => "index_order_annotations_on_service_order_id"

  create_table "order_types", :force => true do |t|
    t.string   "name"
    t.integer  "operator_network_type_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "order_types", ["operator_network_type_id"], :name => "index_order_types_on_operator_network_type_id"

  create_table "ordered_entity_groups", :force => true do |t|
    t.string   "name"
    t.string   "order_type_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.integer  "ethernet_service_type_id"
  end

  add_index "ordered_entity_groups", ["ethernet_service_type_id"], :name => "index_ordered_entity_groups_on_ethernet_service_type_id"
  add_index "ordered_entity_groups", ["order_type_id"], :name => "index_ordered_entity_groups_on_order_type_id"

  create_table "ordered_entity_type_links", :force => true do |t|
    t.integer  "ordered_entity_group_id"
    t.integer  "ordered_entity_type_id"
    t.string   "ordered_entity_type_type"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "ordered_entity_type_links", ["ordered_entity_group_id"], :name => "index_ordered_entity_type_links_on_ordered_entity_group_id"
  add_index "ordered_entity_type_links", ["ordered_entity_type_id"], :name => "index_ordered_entity_type_links_on_ordered_entity_type_id"

  create_table "oss_infos", :force => true do |t|
    t.string   "name"
    t.date     "completion_date"
    t.integer  "operator_network_type_id"
    t.integer  "cenx_contact_id"
    t.integer  "member_contact_id"
    t.string   "reference_documents"
    t.string   "notes"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "oss_infos", ["cenx_contact_id"], :name => "index_oss_infos_on_cenx_contact_id"
  add_index "oss_infos", ["member_contact_id"], :name => "index_oss_infos_on_member_contact_id"
  add_index "oss_infos", ["operator_network_type_id"], :name => "index_oss_infos_on_operator_network_type_id"

  create_table "ovc_monitors", :force => true do |t|
    t.string   "name"
    t.string   "status"
    t.integer  "ovc_id"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "patch_panels", :force => true do |t|
    t.string   "name"
    t.integer  "site_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "floor"
  end

  add_index "patch_panels", ["site_id"], :name => "index_patch_panels_on_site_id"

  create_table "path_displays", :force => true do |t|
    t.integer  "path_id",                                                 :null => false
    t.integer  "entity_id",                                               :null => false
    t.string   "entity_type",                                             :null => false
    t.integer  "row",                                                     :null => false
    t.string   "column",                                                  :null => false
    t.datetime "created_at",                                              :null => false
    t.datetime "updated_at",                                              :null => false
    t.string   "relative_position"
    t.integer  "service_provider_id"
    t.boolean  "redacted"
    t.decimal  "latitude",                  :precision => 9, :scale => 6
    t.decimal  "longitude",                 :precision => 9, :scale => 6
    t.string   "site_type"
    t.string   "demarc_icon"
    t.integer  "owner_service_provider_id"
    t.string   "component_type"
    t.integer  "site_id"
  end

  add_index "path_displays", ["path_id", "entity_type", "entity_id"], :name => "evc_entity_index"
  add_index "path_displays", ["path_id", "service_provider_id", "column", "row", "relative_position"], :name => "row_column_constraint", :unique => true
  add_index "path_displays", ["service_provider_id", "owner_service_provider_id"], :name => "owner_service_provider_search"
  add_index "path_displays", ["service_provider_id", "site_id"], :name => "site_search"

  create_table "path_types", :force => true do |t|
    t.string   "name"
    t.integer  "operator_network_type_id"
    t.string   "path_type"
    t.integer  "max_num_unis"
    t.integer  "mtu"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "path_types", ["operator_network_type_id"], :name => "index_path_types_on_operator_network_type_id"

  create_table "paths", :force => true do |t|
    t.integer  "operator_network_id"
    t.datetime "created_at",                                                  :null => false
    t.datetime "updated_at",                                                  :null => false
    t.string   "cenx_id"
    t.string   "description"
    t.integer  "path_type_id"
    t.string   "cenx_path_type",                      :default => "Standard"
    t.string   "cenx_name_prefix"
    t.integer  "event_record_id"
    t.string   "sm_state"
    t.string   "sm_details"
    t.integer  "sm_timestamp",           :limit => 8
    t.string   "prov_name"
    t.text     "prov_notes"
    t.integer  "prov_timestamp",         :limit => 8
    t.string   "order_name"
    t.text     "order_notes"
    t.integer  "order_timestamp",        :limit => 8
    t.string   "type"
    t.integer  "fallout_timestamp",      :limit => 8, :default => 0
    t.string   "fallout_severity",                    :default => "ok"
    t.string   "fallout_exception_type",              :default => ""
    t.text     "fallout_details"
    t.string   "fallout_data_source"
    t.integer  "fallout_exception_id"
    t.string   "fallout_category"
    t.string   "public_id",                                                   :null => false
  end

  add_index "paths", ["cenx_id"], :name => "index_paths_on_cenx_id"
  add_index "paths", ["event_record_id"], :name => "index_paths_on_event_record_id"
  add_index "paths", ["fallout_exception_id"], :name => "index_paths_on_fallout_exception_id"
  add_index "paths", ["operator_network_id"], :name => "index_evcs_on_operator_network_id"
  add_index "paths", ["path_type_id"], :name => "index_paths_on_path_type_id"
  add_index "paths", ["public_id"], :name => "index_paths_on_public_id", :unique => true

  create_table "paths_segments", :id => false, :force => true do |t|
    t.integer "path_id"
    t.integer "segment_id"
  end

  add_index "paths_segments", ["path_id"], :name => "index_evcs_ovc_news_on_evc_id"
  add_index "paths_segments", ["segment_id"], :name => "index_evcs_ovc_news_ovc_new_id"

  create_table "paths_site_lists", :force => true do |t|
    t.integer "path_id"
    t.integer "site_list_id"
  end

  add_index "paths_site_lists", ["path_id", "site_list_id"], :name => "index_evcs_site_lists_on_evc_id_and_site_list_id", :unique => true

  create_table "permissions", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "permissions", ["name"], :name => "index_permissions_on_name"

  create_table "permissions_roles", :id => false, :force => true do |t|
    t.integer "permission_id"
    t.integer "role_id"
  end

  add_index "permissions_roles", ["permission_id"], :name => "index_permissions_roles_on_permission_id"
  add_index "permissions_roles", ["role_id"], :name => "index_permissions_roles_on_role_id"

  create_table "ports", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.integer  "node_id"
    t.string   "patch_panel_slot_port"
    t.string   "strands"
    t.string   "color_code"
    t.text     "notes"
    t.integer  "patch_panel_id"
    t.integer  "connected_port_id"
    t.string   "mac"
    t.string   "phy_type"
    t.string   "other_phy_type"
    t.integer  "demarc_id"
    t.string   "public_id",             :null => false
  end

  add_index "ports", ["connected_port_id"], :name => "index_ports_on_connected_port_id"
  add_index "ports", ["demarc_id"], :name => "index_ports_on_demarc_id"
  add_index "ports", ["node_id"], :name => "index_ports_on_node_id"
  add_index "ports", ["patch_panel_id"], :name => "index_ports_on_patch_panel_id"
  add_index "ports", ["public_id"], :name => "index_ports_on_public_id", :unique => true

  create_table "privileges", :force => true do |t|
    t.string   "name",       :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "privileges", ["name"], :name => "index_roles_on_name", :unique => true

  create_table "privileges_roles", :id => false, :force => true do |t|
    t.integer "role_id"
    t.integer "privilege_id"
  end

  add_index "privileges_roles", ["role_id", "privilege_id"], :name => "index_privileges_roles_on_privilege_id_and_role_id", :unique => true

  create_table "qos_policies", :force => true do |t|
    t.string   "type"
    t.string   "policy_name"
    t.integer  "policy_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "site_id"
  end

  add_index "qos_policies", ["policy_id"], :name => "index_qos_policies_on_policy_id"
  add_index "qos_policies", ["site_id"], :name => "index_qos_policies_on_site_id"

  create_table "reindex_sphinxes", :force => true do |t|
    t.datetime "started_at"
    t.datetime "finished_at"
    t.boolean  "scheduled"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "reports", :force => true do |t|
    t.string   "type"
    t.integer  "schedule_id"
    t.datetime "run_date"
    t.datetime "report_period_start"
    t.datetime "report_period_end"
    t.integer  "minor_exceptions"
    t.integer  "major_exceptions"
    t.integer  "critical_exceptions"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.boolean  "run_manually",        :default => false
    t.integer  "clear_exceptions"
  end

  add_index "reports", ["schedule_id"], :name => "index_reports_on_schedule_id"

  create_table "roles", :force => true do |t|
    t.string   "name",       :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "roles_users", :id => false, :force => true do |t|
    t.integer "role_id"
    t.integer "user_id"
  end

  add_index "roles_users", ["role_id"], :name => "index_roles_users_on_role_id"
  add_index "roles_users", ["user_id"], :name => "index_roles_users_on_user_id"

  create_table "rollup_ids", :force => true do |t|
    t.integer "cos_test_vector_id"
    t.integer "grid_circuit_id"
    t.integer "demarc_id"
    t.integer "protection_path_id"
    t.integer "site_id"
    t.string  "circuit_id"
  end

  add_index "rollup_ids", ["circuit_id"], :name => "index_rollup_ids_on_circuit_id"
  add_index "rollup_ids", ["cos_test_vector_id"], :name => "index_rollup_ids_on_cos_test_vector_id"
  add_index "rollup_ids", ["demarc_id"], :name => "index_rollup_ids_on_demarc_id"
  add_index "rollup_ids", ["grid_circuit_id"], :name => "index_rollup_ids_on_grid_circuit_id"
  add_index "rollup_ids", ["protection_path_id"], :name => "index_rollup_ids_on_protection_path_id"
  add_index "rollup_ids", ["site_id"], :name => "index_rollup_ids_on_site_id"

  create_table "rollup_report_demarcs", :force => true do |t|
    t.integer "period_start_epoch"
    t.integer "site_id"
    t.integer "demarc_id"
    t.integer "metric_count"
    t.integer "metric_minor_count"
    t.integer "metric_major_count"
    t.string  "severity"
    t.integer "protection_path_count"
    t.integer "path_minor_count"
    t.integer "path_major_count"
    t.integer "path_clear_count"
    t.integer "metric_clear_count"
  end

  add_index "rollup_report_demarcs", ["demarc_id"], :name => "index_rollup_report_demarcs_on_demarc_id"
  add_index "rollup_report_demarcs", ["site_id", "period_start_epoch", "severity"], :name => "index_rollup_report_demarcs_site_period_sev"
  add_index "rollup_report_demarcs", ["site_id", "period_start_epoch"], :name => "index_rollup_report_demarcs_site_period"
  add_index "rollup_report_demarcs", ["site_id"], :name => "index_rollup_report_demarcs_on_site_id"

  create_table "rollup_report_paths", :force => true do |t|
    t.integer "period_start_epoch"
    t.integer "site_id"
    t.integer "demarc_id"
    t.integer "protection_path_id"
    t.integer "metric_count"
    t.integer "metric_minor_count"
    t.integer "metric_major_count"
    t.string  "severity"
    t.integer "metric_clear_count"
  end

  add_index "rollup_report_paths", ["demarc_id"], :name => "index_rollup_report_paths_on_demarc_id"
  add_index "rollup_report_paths", ["protection_path_id"], :name => "index_rollup_report_paths_on_protection_path_id"
  add_index "rollup_report_paths", ["site_id", "period_start_epoch", "severity"], :name => "index_rollup_report_paths_site_period_sev"
  add_index "rollup_report_paths", ["site_id", "period_start_epoch"], :name => "index_rollup_report_paths_site_period"
  add_index "rollup_report_paths", ["site_id"], :name => "index_rollup_report_paths_on_site_id"

  create_table "rollup_report_site_groups", :force => true do |t|
    t.integer "period_start_epoch"
    t.integer "site_group_id"
    t.string  "severity"
    t.integer "site_count"
    t.integer "site_minor_count"
    t.integer "site_major_count"
    t.integer "demarc_count"
    t.integer "demarc_minor_count"
    t.integer "demarc_major_count"
    t.integer "metric_count"
    t.integer "metric_minor_count"
    t.integer "metric_major_count"
    t.integer "site_clear_count"
    t.integer "demarc_clear_count"
    t.integer "metric_clear_count"
  end

  add_index "rollup_report_site_groups", ["site_group_id", "period_start_epoch", "severity"], :name => "index_rollup_report_site_groups_site_period_sev"
  add_index "rollup_report_site_groups", ["site_group_id", "period_start_epoch"], :name => "index_rollup_report_site_groups_site_period"
  add_index "rollup_report_site_groups", ["site_group_id"], :name => "index_rollup_report_site_groups_on_site_group_id"

  create_table "rollup_report_sites", :force => true do |t|
    t.integer "period_start_epoch"
    t.integer "site_id"
    t.string  "severity"
    t.integer "metric_count"
    t.integer "metric_minor_count"
    t.integer "metric_major_count"
    t.integer "demarc_count"
    t.integer "minor_demarc_count"
    t.integer "major_demarc_count"
    t.integer "clear_demarc_count"
    t.integer "metric_clear_count"
  end

  add_index "rollup_report_sites", ["site_id", "period_start_epoch", "severity"], :name => "rollup_report_sites_site_period_sev"
  add_index "rollup_report_sites", ["site_id", "period_start_epoch"], :name => "rollup_report_sites_site_period"
  add_index "rollup_report_sites", ["site_id"], :name => "index_rollup_report_sites_on_site_id"

  create_table "scheduled_tasks", :force => true do |t|
    t.string   "name",                                 :null => false
    t.integer  "user_id"
    t.text     "filter_options"
    t.string   "executable_script"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.string   "report_type"
    t.string   "type"
    t.string   "environment",       :default => "sin"
    t.boolean  "is_public",         :default => false
  end

  add_index "scheduled_tasks", ["user_id"], :name => "index_scheduled_tasks_on_user_id"

  create_table "schedules", :force => true do |t|
    t.string   "name",               :null => false
    t.integer  "scheduled_task_id"
    t.boolean  "enabled"
    t.string   "trigger"
    t.text     "formats"
    t.datetime "last_run"
    t.integer  "retention_time"
    t.datetime "execution_time"
    t.boolean  "email_distribution"
    t.string   "email_to"
    t.string   "email_cc"
    t.string   "email_bcc"
    t.string   "email_subject"
    t.string   "email_body"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "timezone"
  end

  add_index "schedules", ["scheduled_task_id"], :name => "index_schedules_on_scheduled_task_id"

  create_table "segment_end_point_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.integer  "operator_network_type_id"
    t.integer  "class_of_service_type_id"
    t.string   "notes"
  end

  add_index "segment_end_point_types", ["class_of_service_type_id"], :name => "index_segment_end_point_types_on_class_of_service_type_id"
  add_index "segment_end_point_types", ["operator_network_type_id"], :name => "index_segment_end_point_types_on_operator_network_type_id"

  create_table "segment_end_points", :force => true do |t|
    t.string   "type"
    t.string   "cenx_id"
    t.integer  "segment_id"
    t.integer  "demarc_id"
    t.integer  "segment_end_point_type_id"
    t.string   "md_format"
    t.string   "md_level"
    t.string   "md_name_ieee"
    t.string   "ma_format"
    t.string   "ma_name"
    t.boolean  "is_monitored"
    t.string   "notes"
    t.string   "stags"
    t.string   "ctags"
    t.string   "oper_state"
    t.datetime "created_at",                                                :null => false
    t.datetime "updated_at",                                                :null => false
    t.boolean  "eth_cfm_configured",                     :default => true
    t.boolean  "reflection_enabled"
    t.string   "mep_id",                                 :default => "TBD"
    t.integer  "event_record_id"
    t.string   "sm_state"
    t.string   "sm_details"
    t.integer  "sm_timestamp",              :limit => 8
    t.string   "prov_name"
    t.text     "prov_notes"
    t.integer  "prov_timestamp",            :limit => 8
    t.string   "order_name"
    t.text     "order_notes"
    t.integer  "order_timestamp",           :limit => 8
    t.string   "ip_address"
    t.string   "routing_cost"
    t.string   "public_id",                                                 :null => false
  end

  add_index "segment_end_points", ["cenx_id"], :name => "index_segment_end_points_on_cenx_id"
  add_index "segment_end_points", ["demarc_id"], :name => "index_ovc_end_point_news_on_demarc_id"
  add_index "segment_end_points", ["event_record_id"], :name => "index_segment_end_points_on_event_record_id"
  add_index "segment_end_points", ["mep_id"], :name => "index_segment_end_points_on_mep_id"
  add_index "segment_end_points", ["public_id"], :name => "index_segment_end_points_on_public_id", :unique => true
  add_index "segment_end_points", ["segment_end_point_type_id"], :name => "index_segment_end_points_on_segment_end_point_type_id"
  add_index "segment_end_points", ["segment_id"], :name => "index_ovc_end_point_news_on_ovc_new_id"

  create_table "segment_end_points_segment_end_points", :id => false, :force => true do |t|
    t.integer "segment_end_point_id"
    t.integer "self_id"
  end

  add_index "segment_end_points_segment_end_points", ["segment_end_point_id"], :name => "ovc_end_point_new_id_index"
  add_index "segment_end_points_segment_end_points", ["self_id"], :name => "self_id_index"

  create_table "segment_types", :force => true do |t|
    t.string   "name"
    t.string   "segment_type"
    t.string   "mef9_cert"
    t.string   "mef14_cert"
    t.integer  "max_mtu"
    t.boolean  "c_vlan_cos_preservation"
    t.string   "c_vlan_id_preservation"
    t.boolean  "s_vlan_cos_preservation"
    t.string   "s_vlan_id_preservation"
    t.string   "color_marking"
    t.string   "color_marking_notes"
    t.string   "unicast_frame_delivery_conditions"
    t.string   "multicast_frame_delivery_conditions"
    t.string   "broadcast_frame_delivery_conditions"
    t.string   "unicast_frame_delivery_details"
    t.string   "multicast_frame_delivery_details"
    t.string   "broadcast_frame_delivery_details"
    t.text     "notes"
    t.datetime "created_at",                                            :null => false
    t.datetime "updated_at",                                            :null => false
    t.integer  "operator_network_type_id"
    t.string   "underlying_technology"
    t.string   "equipment_vendor"
    t.string   "service_attribute_notes"
    t.string   "max_num_cos"
    t.string   "redundancy_mechanism"
    t.boolean  "is_frame_aware",                      :default => true
    t.string   "public_id",                                             :null => false
  end

  add_index "segment_types", ["operator_network_type_id"], :name => "index_segment_types_on_operator_network_type_id"
  add_index "segment_types", ["public_id"], :name => "index_segment_types_on_public_id", :unique => true

  create_table "segments", :force => true do |t|
    t.string   "type"
    t.string   "status"
    t.text     "notes"
    t.string   "segment_owner_role"
    t.integer  "segment_type_id"
    t.string   "cenx_id"
    t.integer  "operator_network_id"
    t.string   "service_id"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.integer  "ethernet_service_type_id"
    t.boolean  "use_member_attrs"
    t.integer  "path_network_id"
    t.integer  "emergency_contact_id"
    t.integer  "event_record_id"
    t.string   "sm_state"
    t.string   "sm_details"
    t.integer  "sm_timestamp",             :limit => 8
    t.string   "prov_name"
    t.text     "prov_notes"
    t.integer  "prov_timestamp",           :limit => 8
    t.string   "order_name"
    t.text     "order_notes"
    t.integer  "order_timestamp",          :limit => 8
    t.integer  "site_id"
    t.string   "as_id"
    t.string   "router_type"
    t.string   "router_id"
    t.string   "routing_domain"
    t.string   "public_id",                             :null => false
  end

  add_index "segments", ["cenx_id"], :name => "index_segments_on_cenx_id"
  add_index "segments", ["emergency_contact_id"], :name => "index_segments_on_emergency_contact_id"
  add_index "segments", ["ethernet_service_type_id"], :name => "index_segments_on_ethernet_service_type_id"
  add_index "segments", ["event_record_id"], :name => "index_segments_on_event_record_id"
  add_index "segments", ["operator_network_id"], :name => "index_ovc_news_on_operator_network_id"
  add_index "segments", ["path_network_id"], :name => "index_segments_on_path_network_id"
  add_index "segments", ["public_id"], :name => "index_segments_on_public_id", :unique => true
  add_index "segments", ["segment_type_id"], :name => "index_segments_on_segment_type_id"
  add_index "segments", ["service_id"], :name => "index_segments_on_service_id"
  add_index "segments", ["site_id"], :name => "index_segments_on_site_id"

  create_table "service_level_guarantee_types", :force => true do |t|
    t.integer  "class_of_service_type_id"
    t.string   "min_dist_km"
    t.string   "max_dist_km"
    t.string   "delay_us"
    t.string   "delay_variation_us"
    t.boolean  "is_unbounded"
    t.string   "notes"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "service_level_guarantee_types", ["class_of_service_type_id"], :name => "index_service_level_guarantee_types_on_class_of_service_type_id"

  create_table "service_orders", :force => true do |t|
    t.string   "type"
    t.string   "title"
    t.string   "cenx_id"
    t.string   "action"
    t.integer  "primary_contact_id"
    t.integer  "testing_contact_id"
    t.date     "requested_service_date"
    t.date     "order_received_date"
    t.date     "order_acceptance_date"
    t.date     "order_completion_date"
    t.date     "customer_acceptance_date"
    t.date     "billing_start_date"
    t.integer  "operator_network_id"
    t.boolean  "expedite"
    t.string   "status"
    t.text     "notes"
    t.integer  "ordered_entity_id"
    t.string   "ordered_entity_type"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.string   "ordered_entity_subtype"
    t.integer  "path_id"
    t.integer  "ordered_entity_group_id"
    t.integer  "ordered_operator_network_id"
    t.string   "order_state"
    t.integer  "ordered_object_type_id"
    t.string   "ordered_object_type_type"
    t.text     "ordered_entity_snapshot"
    t.integer  "technical_contact_id"
    t.integer  "local_contact_id"
    t.date     "foc_date"
    t.string   "bulk_order_type"
    t.date     "design_complete_date"
    t.date     "order_created_date"
    t.string   "order_name"
    t.text     "order_notes"
    t.integer  "order_timestamp",             :limit => 8
    t.date     "ready_to_order_date"
  end

  add_index "service_orders", ["cenx_id"], :name => "index_service_orders_on_cenx_id"
  add_index "service_orders", ["local_contact_id"], :name => "index_service_orders_on_local_contact_id"
  add_index "service_orders", ["operator_network_id"], :name => "index_service_orders_on_operator_network_id"
  add_index "service_orders", ["ordered_entity_group_id"], :name => "index_service_orders_on_ordered_entity_group_id"
  add_index "service_orders", ["ordered_entity_type", "ordered_entity_id"], :name => "order_entity_index"
  add_index "service_orders", ["ordered_object_type_id"], :name => "index_service_orders_on_ordered_object_type_id"
  add_index "service_orders", ["ordered_operator_network_id"], :name => "index_service_orders_on_ordered_operator_network_id"
  add_index "service_orders", ["path_id"], :name => "index_service_orders_on_path_id"
  add_index "service_orders", ["primary_contact_id"], :name => "index_service_orders_on_primary_contact_id"
  add_index "service_orders", ["technical_contact_id"], :name => "index_service_orders_on_technical_contact_id"
  add_index "service_orders", ["testing_contact_id"], :name => "index_service_orders_on_testing_contact_id"
  add_index "service_orders", ["title"], :name => "index_service_orders_on_title"
  add_index "service_orders", ["type", "bulk_order_type"], :name => "service_type"

  create_table "service_providers", :force => true do |t|
    t.string   "name"
    t.string   "website"
    t.boolean  "is_system_owner"
    t.integer  "password_expiry_interval"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.string   "logo_url"
    t.string   "address"
    t.string   "cenx_cma"
    t.string   "cenx_id"
    t.string   "notes"
    t.text     "terms"
    t.boolean  "is_system_logo",           :default => false
    t.string   "public_id",                                   :null => false
  end

  add_index "service_providers", ["cenx_id"], :name => "index_service_providers_on_cenx_id"
  add_index "service_providers", ["public_id"], :name => "index_service_providers_on_public_id", :unique => true

  create_table "sessions", :force => true do |t|
    t.string   "session_id",                     :null => false
    t.text     "data",       :limit => 16777215
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "settings", :force => true do |t|
    t.string   "var",                       :null => false
    t.text     "value"
    t.integer  "target_id"
    t.string   "target_type", :limit => 30
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "settings", ["target_type", "target_id", "var"], :name => "index_settings_on_target_type_and_target_id_and_var", :unique => true

  create_table "sin_enni_service_provider_sphinxes", :force => true do |t|
    t.integer  "enni_id"
    t.integer  "service_provider_id"
    t.boolean  "delta",               :default => true, :null => false
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
  end

  add_index "sin_enni_service_provider_sphinxes", ["enni_id"], :name => "sin_enni_service_provider_enni"
  add_index "sin_enni_service_provider_sphinxes", ["service_provider_id", "enni_id"], :name => "sin_enni_service_provider_unique", :unique => true

  create_table "sin_path_service_provider_sphinxes", :force => true do |t|
    t.integer  "path_id"
    t.integer  "service_provider_id"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.boolean  "delta",               :default => true, :null => false
    t.string   "member_handle"
  end

  add_index "sin_path_service_provider_sphinxes", ["path_id", "service_provider_id"], :name => "sin_evc_service_provider_unique", :unique => true

  create_table "sin_sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sin_sessions", ["session_id"], :name => "index_sin_sessions_on_session_id"
  add_index "sin_sessions", ["updated_at"], :name => "index_sin_sessions_on_updated_at"

  create_table "site_groups", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "type"
    t.string   "public_id",  :null => false
  end

  add_index "site_groups", ["public_id"], :name => "index_site_groups_on_public_id", :unique => true

  create_table "site_groups_sites", :id => false, :force => true do |t|
    t.integer "site_id"
    t.integer "site_group_id"
    t.integer "id"
  end

  add_index "site_groups_sites", ["site_group_id"], :name => "index_site_groups_sites_on_site_group_id"
  add_index "site_groups_sites", ["site_id"], :name => "index_site_groups_sites_on_site_id"

  create_table "site_lists", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "site_lists", ["user_id", "name"], :name => "index_site_lists_on_user_id_and_name", :unique => true

  create_table "sites", :force => true do |t|
    t.string   "name"
    t.string   "address"
    t.datetime "created_at",                                                                               :null => false
    t.datetime "updated_at",                                                                               :null => false
    t.string   "contact_info"
    t.string   "site_host"
    t.string   "floor"
    t.string   "clli"
    t.string   "icsc"
    t.string   "network_address"
    t.string   "broadcast_address"
    t.string   "host_range"
    t.string   "hosts_per_subnet"
    t.text     "notes"
    t.string   "node_prefix"
    t.string   "postal_zip_code"
    t.string   "emergency_contact_info"
    t.text     "site_access_notes"
    t.string   "city"
    t.string   "state_province"
    t.string   "site_type"
    t.decimal  "latitude",               :precision => 9, :scale => 6, :default => 0.0
    t.decimal  "longitude",              :precision => 9, :scale => 6, :default => 0.0
    t.string   "type"
    t.integer  "operator_network_id"
    t.string   "country"
    t.integer  "service_id_low"
    t.integer  "service_id_high"
    t.string   "site_layout",                                          :default => "StandardExchange7750"
    t.string   "loopback_address"
    t.string   "public_id",                                                                                :null => false
  end

  add_index "sites", ["operator_network_id"], :name => "index_sites_on_operator_network_id"
  add_index "sites", ["public_id"], :name => "index_sites_on_public_id", :unique => true

  create_table "sm_ownerships", :force => true do |t|
    t.integer "smable_id"
    t.string  "smable_type"
    t.integer "sm_history_id"
  end

  add_index "sm_ownerships", ["sm_history_id"], :name => "index_sm_ownerships_on_sm_history_id"
  add_index "sm_ownerships", ["smable_id", "smable_type"], :name => "index_sm_ownerships_on_smable_id_and_smable_type"

  create_table "stateful_ownerships", :force => true do |t|
    t.integer "stateable_id"
    t.string  "stateable_type"
    t.integer "stateful_id"
  end

  add_index "stateful_ownerships", ["stateable_id", "stateable_type"], :name => "index_stateful_ownerships_on_stateable_id_and_stateable_type"
  add_index "stateful_ownerships", ["stateful_id"], :name => "index_stateful_ownerships_on_stateful_id"

  create_table "statefuls", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type"
    t.string   "name"
  end

  create_table "states", :force => true do |t|
    t.integer  "timestamp",   :limit => 8
    t.string   "name"
    t.text     "notes"
    t.integer  "stateful_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type"
  end

  add_index "states", ["stateful_id", "timestamp"], :name => "index_states_on_stateful_id_and_timestamp"

  create_table "top_talkers", :force => true do |t|
    t.string  "type"
    t.integer "enni_id"
    t.integer "cos_end_point_id"
    t.float   "octets_count"
    t.string  "direction"
    t.integer "time_period_start"
    t.integer "time_period_end"
  end

  add_index "top_talkers", ["cos_end_point_id"], :name => "index_top_talkers_on_cos_end_point_id"
  add_index "top_talkers", ["enni_id"], :name => "index_top_talkers_on_enni_id"

  create_table "uploaded_files", :force => true do |t|
    t.string   "comment"
    t.string   "name"
    t.string   "content_type"
    t.binary   "data",          :limit => 16777215
    t.integer  "uploader_id"
    t.string   "uploader_type"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.string   "url"
  end

  add_index "uploaded_files", ["uploader_id"], :name => "index_uploaded_files_on_uploader_id"

  create_table "user_service_providers", :force => true do |t|
    t.integer  "user_id",             :null => false
    t.integer  "service_provider_id", :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_service_providers", ["service_provider_id"], :name => "index_user_service_providers_on_service_provider_id"
  add_index "user_service_providers", ["user_id"], :name => "index_user_service_providers_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "email",                                               :null => false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "crypted_password",       :limit => 40
    t.string   "salt",                   :limit => 40
    t.string   "remember_token"
    t.string   "phone"
    t.integer  "service_provider_id"
    t.integer  "role_id"
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
    t.text     "settings"
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
    t.datetime "last_login_at"
    t.integer  "login_count",                          :default => 0, :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["role_id"], :name => "index_users_on_privilege_id"
  add_index "users", ["service_provider_id"], :name => "index_users_on_service_provider_id"

  create_table "utilization_thresholds", :force => true do |t|
    t.float   "sla_warning_threshold",      :default => 0.5
    t.integer "utilization_high_time_over", :default => 7200
    t.integer "utilization_low_time_over",  :default => 86400
    t.float   "utilization_high_threshold", :default => 0.5
    t.float   "utilization_low_threshold",  :default => 0.25
  end

end
