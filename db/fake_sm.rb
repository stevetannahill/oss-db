
def fake_alarm_on_net_ovc(segment)
  puts "On Net OVC #{segment.id}"
  
  if segment.mtc_periods.empty?
    segment.mtc_periods = [MtcHistory.create(:event_filter => "Mtc Segment:#{segment.cenx_id}")]
    segment.mtc_periods.first.alarm_records << AlarmRecord.create(:time_of_event => (Time.now.to_f*1000).to_i, :state => "Ok", :details => "Out of Maintenance", :original_state => "cleared")
  end
  
  if segment.sm_histories.size == 0
     segment.sm_histories << SmHistory.create(:event_filter => "SM FAKE On Net OVC:#{segment.cenx_id}")
  end
     
  segment.segment_end_points.each {|ep|
    next if ep.is_connected_to_test_port
    if ep.sm_histories.size == 0
      ep.sm_histories << SmHistory.create(:event_filter => "SM EP:#{ep.cenx_id}")
    end
      
    if ep.alarm_histories.size == 0
      if ep.enni.is_multi_chassis && !ep.is_connected_to_test_port
        # for MultiChassis LAG add the alarm histroies on each node and then OR the two together
        fstate = ep.node_names.collect do |node|        
          ip = Node.find_by_name(node).loopback_ip
          ah = AlarmHistory.create(:event_filter => "Fake EP#{ep.cenx_id}-#{ip}-#{node}", :alarm_mapping => AlarmSeverity.default_alu_alarm_mapping)          
          ep.alarm_histories << ah
          ah.add_event((Time.now.to_f*1000).to_i.to_s, "cleared", "")        
        end
      else
        ep.alarm_histories << AlarmHistory.create(:event_filter => "Fake EP#{ep.cenx_id}", :alarm_mapping => AlarmSeverity.default_alu_alarm_mapping)
        ep.alarm_histories.first.add_event((Time.now.to_f*1000).to_i.to_s, "cleared", "")
      end
    end
  }
end

def fake_alarm_demarc(demarc)
    puts "Demarc #{demarc.id}"
    return if demarc.class == EnniNew && demarc.is_connected_to_test_port
    
    if demarc.mtc_periods.empty?
      demarc.mtc_periods = [MtcHistory.create(:event_filter => "Mtc Demarc:#{demarc.cenx_id}")]
      demarc.mtc_periods.first.alarm_records << AlarmRecord.create(:time_of_event => (Time.now.to_f*1000).to_i, :state => "Ok", :details => "Out of Maintenance", :original_state => "cleared")
    end
    
    if demarc.sm_histories.size == 0
      demarc.sm_histories << SmHistory.create(:event_filter => "SM FAKE ENNI:#{demarc.cenx_id}")
    end
    if demarc.alarm_histories.size == 0 && demarc.class != Uni
      demarc.alarm_histories << AlarmHistory.create(:event_filter => "Fake ENNI network:10.10.100.7:lag:interface-2 #{demarc.cenx_id}", :alarm_mapping => AlarmSeverity.default_alu_alarm_mapping)
      demarc.alarm_histories.each {|a| a.add_event((Time.now.to_f*1000).to_i.to_s, "cleared", "")}
    end
end

$cos_id = 0
def fake_alarm_cos_ep(cosep)
    if cosep.alarm_histories.size == 0
      $cos_id = $cos_id + 1
      cosep.alarm_histories << BrixAlarmHistory.create(:event_filter => "Fake COSEP #{$cos_id}")
      cosep.alarm_histories.first.add_event((Time.now.to_f*1000).to_i.to_s, "cleared", "")
    end
end

def fake_off_net_ovc(segment)
  puts "Off Net OVC #{segment.id}"
  
  if segment.mtc_periods.empty?
    segment.mtc_periods = [MtcHistory.create(:event_filter => "Mtc Segment:#{segment.cenx_id}")]
    segment.mtc_periods.first.alarm_records << AlarmRecord.create(:time_of_event => (Time.now.to_f*1000).to_i, :state => "Ok", :details => "Out of Maintenance", :original_state => "cleared")
  end
  
  if segment.sm_histories.size == 0
     segment.sm_histories << SmHistory.create(:event_filter => "SM FAKE Off Net OVC:#{segment.cenx_id}")
  end
  
  segment.segment_end_points.each {|ep|
    if ep.sm_histories.size == 0
      ep.sm_histories << SmHistory.create(:event_filter => "SM EP:#{ep.cenx_id}")
    end
    if ep.is_monitored
      ep.cos_end_points.each {|cosep|
        fake_alarm_cos_ep(cosep)
      }
    end
  }
end


do_not_touch_paths = [16,7,8]
Path.all.each {|path| path.sm_histories << SmHistory.create(:event_filter => "SM Path:#{path.cenx_id}") if path.sm_histories.size == 0}
Demarc.all.each {|demarc| fake_alarm_demarc(demarc)}
OnNetOvc.all.each {|segment| fake_alarm_on_net_ovc(segment)}
OffNetOvc.all.each {|segment| fake_off_net_ovc(segment)}

=begin
live_paths = Path.all.collect {|p| p if p.get_prov_name == "Live" && p.cenx_path_type != "Tunnel"}.compact
live_paths.reject! {|path| do_not_touch_paths.include?(path.id)}

# make approx last 10% fail
failed_start = live_paths.size - (live_paths.size/10)
failed_paths = live_paths[failed_start..-1]

on_net_ovcs = failed_paths.collect {|path| path.on_net_ovcs}.flatten.uniq
on_net_ovcs.each {|segment|
  puts "On Net OVC setting fault #{segment.id}"
  segment.segment_end_points.each {|ep|
    ep.alarm_histories.first.add_event((Time.now.to_f*1000).to_i.to_s, "critical", "") if !ep.is_connected_to_test_port
  }
}


off_net_ovcs = failed_paths.collect {|path| path.off_net_ovcs}.flatten.uniq
off_net_ovcs.each {|segment|
  next if segment.paths.size > 1
  puts "Memver Segment setting fault #{segment.id}"  
  segment.segment_end_points.each {|ep|
    ep.cos_end_points.each {|cosep|
      cosep.alarm_histories.first.add_event((Time.now.to_f*1000).to_i.to_s, "unavailable", "") if cosep.alarm_histories.size != 0
    }
  }
}
  
=end



