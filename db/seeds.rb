# In this seed, we'll create a relatively small number of Paths, representing the various
# demos and circuit models we exhibited in the spring of 2011.

DDD="2012/12/21"
SET_PROV_STATE=true

EventClient #Loads the EventClient class from app/models/event_client.rb
class EventClient
  #Redefine methods so it doesn't even try to comunicate with the event server during seeding
  def initialize
  end
  def post_event(members,evt_tag, evt_objtype, evt_objid, timestamp, evt_notif, evt_details)
  end
end


################################################################################
# MSO CIRCUITS
################################################################################
require File.dirname(__FILE__) + '/paths/seed_mso_circuits.rb'


################################################################################
# LSQ CIRCUITS
################################################################################
require File.dirname(__FILE__) + '/paths/seed_lsq_circuits.rb'


################################################################################
# ITW CIRCUITS
################################################################################
require File.dirname(__FILE__) + '/paths/seed_itw_circuits.rb'


