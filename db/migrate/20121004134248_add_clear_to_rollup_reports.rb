class AddClearToRollupReports < ActiveRecord::Migration
  def change
    add_column :rollup_report_paths, :metric_clear_count, :integer

    add_column :rollup_report_demarcs, :path_clear_count, :integer
    add_column :rollup_report_demarcs, :metric_clear_count, :integer

    add_column :rollup_report_sites, :clear_demarc_count, :integer
    add_column :rollup_report_sites, :metric_clear_count, :integer

    add_column :rollup_report_site_groups, :site_clear_count, :integer
    add_column :rollup_report_site_groups, :demarc_clear_count, :integer
    add_column :rollup_report_site_groups, :metric_clear_count, :integer
  end
end
