class FixOutstandingMsVsUsNumbers < ActiveRecord::Migration
  class InstanceElement < ActiveRecord::Base
    self.abstract_class = true
    self.inheritance_column = :_type_disabled
  end
  
  class TypeElement < ActiveRecord::Base
    self.abstract_class = true
    self.inheritance_column = :_type_disabled
  end  
  
  class CosTestVector < InstanceElement
    self.inheritance_column = :_type_disabled
  end
  
  class ServiceLevelGuaranteeType < TypeElement
    self.inheritance_column = :_type_disabled
  end

  def up
    ServiceLevelGuaranteeType.reset_column_information
    # some data was migrated into the 5,000,000 range (5000 * 10000) so let's fix it
    ServiceLevelGuaranteeType.find_each do |slgt|
      delay_str = slgt.delay_us.gsub(/[^\d.]/, '')
      if delay_str.to_i >= 1000000 && is_num(delay_str)
        delay_f = delay_str.to_i / 1000 
        slgt.delay_us = slgt.delay_us.sub("#{delay_str}", delay_f.to_s)
        slgt.save(:validate => false)
      end
    end
    CosTestVector.find_each do |ctv|
      ctv.delay_error_threshold   = ctv.delay_error_threshold.to_i   / 1000 if ctv.delay_error_threshold.to_i >= 1000000
      ctv.delay_sla_guarantee     = ctv.delay_sla_guarantee.to_i     / 1000 if ctv.delay_sla_guarantee.to_i >= 1000000
      ctv.delay_warning_threshold = ctv.delay_warning_threshold.to_i / 1000 if ctv.delay_warning_threshold.to_i >= 1000000
      ctv.save(:validate => false)
    end
  end

  def down
    # the old state is bad, so don't fix it
    # and the 'up' can be run multiple times without affecting anything
  end
  
  def is_num(input)
    input.to_s.match(/\A[+-]?\d+?(\.\d+)?\Z/) != nil
  end
end
