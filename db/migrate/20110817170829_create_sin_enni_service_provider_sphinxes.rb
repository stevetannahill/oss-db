class CreateSinEnniServiceProviderSphinxes < ActiveRecord::Migration
  def self.up
    create_table :sin_enni_service_provider_sphinxes do |t|
      t.integer :enni_id
      t.integer :service_provider_id
      t.boolean :delta, :default => true, :null => false
      t.timestamps
    end

    add_index :sin_enni_service_provider_sphinxes, [:service_provider_id, :enni_id,], :name => 'sin_enni_service_provider_unique', :unique => true
    add_index :sin_enni_service_provider_sphinxes, [:enni_id], :name => 'sin_enni_service_provider_enni'


    # Populate on behalf of for ennis linked to LightSquared exhanges
    lightsquared = ServiceProvider.find_by_name('LightSquared')
    if lightsquared
      ennis = Demarc.find_all_ennis
      ennis.each do |enni|
        if enni.exchange.exchange_type =~ /^lightsquared/i && enni.service_provider.id != lightsquared.id && !enni.is_connected_to_monitoring_port
          enni.on_behalf_of_service_provider_id = lightsquared.id 
          enni.save
        end
      end
    else
      puts 'no lightsquared service provider found'
    end
    
    # Populdate access table with all existing ennis and service providers who 'own' them
    ennis = Demarc.find_all_ennis
    ennis.each do |enni|
      SinEnniServiceProviderSphinx.find_or_create_by_enni_id_and_service_provider_id(enni.id, enni.service_provider.id) if enni.service_provider
      SinEnniServiceProviderSphinx.find_or_create_by_enni_id_and_service_provider_id(enni.id, enni.on_behalf_of_service_provider.id) if enni.on_behalf_of_service_provider
      ep = enni.ovc_end_point_news
      ep.each do |ep|
        ovc = ep.ovc_new
        SinEnniServiceProviderSphinx.find_or_create_by_enni_id_and_service_provider_id(enni.id, ovc.service_provider.id)
        ovc.evcs.each do |evc|
          SinEnniServiceProviderSphinx.find_or_create_by_enni_id_and_service_provider_id(enni.id, evc.service_provider.id)
        end
      end
    end

  end

  def self.down
    drop_table :sin_enni_service_provider_sphinxes
  end
end