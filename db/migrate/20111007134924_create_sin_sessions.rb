class CreateSinSessions < ActiveRecord::Migration
  def self.up
    create_table :sin_sessions do |t|
      t.string :session_id, :null => false
      t.text :data
      t.timestamps
    end

    add_index :sin_sessions, :session_id
    add_index :sin_sessions, :updated_at
  end

  def self.down
    drop_table :sin_sessions
  end
end
