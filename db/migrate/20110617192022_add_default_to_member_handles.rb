class AddDefaultToMemberHandles < ActiveRecord::Migration
  def self.up
    change_column_default :member_attr_instances, :value, "TBD"

    MemberAttrInstance.all.each do |member_handle|
      member_handle.value = "TBD" if member_handle.value.nil? or member_handle.value.strip.empty?
      member_handle.save false
    end
  end

  def self.down
    change_column_default :member_attr_instances, :value, nil
  end
end
