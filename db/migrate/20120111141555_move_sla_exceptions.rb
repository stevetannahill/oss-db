class SlaException   < ActiveRecord::Base
  def severity
    read_attribute(:severity).to_sym
  end
  def severity= (value)
    write_attribute(:severity, value.to_s)
  end
end
class SlaExceptionCosEndPoint < SlaException
  belongs_to :cos_test_vector
end
class SlaExceptionCenxOvc < SlaException
  belongs_to :cos_end_point
end
class SlaExceptionEnni < SlaException
  belongs_to :enni_new
end
class MoveSlaExceptions < ActiveRecord::Migration

  class ExceptionUtilizationEnni < ActiveRecord::Base
    self.inheritance_column = :_type_disabled
  end
  
  class SlaException < ActiveRecord::Base
    self.inheritance_column = :_type_disabled
  end

  def up
    SlaException.all.each do |ex|
      case ex.type
      when 'SlaExceptionEnni'
        new_ex = ExceptionUtilizationEnni.new
        new_ex.time_declared                         = ex.time_declared
        new_ex.value                                 = ex.value
        new_ex.severity                              = ex.severity
        new_ex.enni_new_id                           = ex.enni_new_id
        new_ex.direction                             = ex.direction
        new_ex.time_over                             = ex.time_over
        new_ex.average_rate_while_over_threshold_mbps= ex.average_rate_while_over_threshold_mbps
        new_ex.save
      when 'SlaExceptionCosEndPoint'
        new_ex = ExceptionSlaCosTestVector.new
        new_ex.time_declared      = ex.time_declared
        new_ex.value              = ex.value
        new_ex.severity           = ex.severity
        new_ex.cos_test_vector_id = ex.cos_test_vector_id
        new_ex.sla_type           = ex.sla_type
        new_ex.time_over          = ex.time_over
        new_ex.save
      when 'SlaExceptionCenxOvc'
        new_ex = ExceptionUtilizationCosEndPoint.new
        new_ex.time_declared                         = ex.time_declared
        new_ex.value                                 = ex.value
        new_ex.severity                              = ex.severity
        new_ex.cos_end_point_id                      = ex.cos_end_point_id
        new_ex.direction                             = ex.direction
        new_ex.time_over                             = ex.time_over
        new_ex.average_rate_while_over_threshold_mbps= ex.average_rate_while_over_threshold_mbps
        new_ex.save
      end
    end
    SlaException.delete_all
  end

  def down
  end
end
