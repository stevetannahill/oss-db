class AddEnvironmentToScheduledTask < ActiveRecord::Migration
  
  class ScheduledTask < ActiveRecord::Base
    self.inheritance_column = :_type_disabled
  end
  
  def up
    add_column :scheduled_tasks, :environment, :string, :default => "sin"
    ScheduledTask.reset_column_information
    ScheduledTask.all.each {|st| st.update_attribute(:environment, "sin")}
  end
  
  def down
    remove_column :scheduled_tasks, :environment
  end
end
