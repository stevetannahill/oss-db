class MoreOssChanges < ActiveRecord::Migration
  def self.up
    remove_column :service_types, :c_vlan_id_preservation
    add_column :service_types, :c_vlan_id_preservation, :string
    add_column :uni_profiles , :lamp, :string
    add_column :uni_profiles , :link_oam, :string
    add_column :uni_profiles , :e_lmi, :string
    add_column :uni_profiles , :mrp_b, :string
    add_column :uni_profiles , :cisco_bpdu, :string
    add_column :uni_profiles , :default_l2cp, :string
    add_column :service_types, :cos_marking_uni, :string
    add_column :class_of_services, :cos_value_uni, :string
  end

  def self.down
    remove_column :service_types, :c_vlan_id_preservation
    add_column :service_types, :c_vlan_id_preservation, :boolean
    remove_column :uni_profiles , :lamp
    remove_column :uni_profiles , :link_oam
    remove_column :uni_profiles , :e_lmi
    remove_column :uni_profiles , :mrp_b
    remove_column :uni_profiles , :cisco_bpdu
    remove_column :uni_profiles , :default_l2cp
    remove_column :service_types, :cos_marking_uni
    remove_column :class_of_services, :cos_value_uni
  end
end


# Table name: service_types
#
#  id                                  :integer(4)      not null, primary key
#  name                                :string(255)
#  role                                :string(255)
#  operator_network_id                 :integer(4)
#  max_mtu                             :string(255)
#  c_vlan_id_preservation              :boolean(1)
#  c_vlan_cos_preservation             :boolean(1)
#  color_marking                       :string(255)
#  service_category                    :string(255)
#  unicast_frame_delivery_conditions   :string(255)
#  multicast_frame_delivery_conditions :string(255)
#  broadcast_frame_delivery_conditions :string(255)
#  created_at                          :datetime
#  updated_at                          :datetime
#  notes                               :string(255)
#  mef9_cert                           :string(255)
#  mef14_cert                          :string(255)
#  unicast_frame_delivery_details      :string(255)
#  multicast_frame_delivery_details    :string(255)
#  broadcast_frame_delivery_details    :string(255)
#  cos_marking                         :string(255)
#