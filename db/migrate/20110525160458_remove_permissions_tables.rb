class RemovePermissionsTables < ActiveRecord::Migration
  def self.up
    drop_table :permissions
    drop_table :permissions_roles
  end

  def self.down
    create_table "permissions", :force => true do |t|
      t.string   "name"
      t.datetime "created_at"
      t.datetime "updated_at"
    end

    add_index "permissions", ["name"], :name => "index_permissions_on_name"

    create_table "permissions_roles", :id => false, :force => true do |t|
      t.integer "permission_id"
      t.integer "role_id"
    end

    add_index "permissions_roles", ["permission_id"], :name => "index_permissions_roles_on_permission_id"
    add_index "permissions_roles", ["role_id"], :name => "index_permissions_roles_on_role_id"

  end
end
