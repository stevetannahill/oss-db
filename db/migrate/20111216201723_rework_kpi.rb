class Kpi_2011_12_28_10_10 < ActiveRecord::Base
  set_table_name 'kpis'
end

class ReworkKpi < ActiveRecord::Migration
  def up
    change_table :kpis do |t|
      t.remove_references :kpiable, :polymorphic => true
      t.change :warning_threshold, :float     
      t.change :error_threshold, :float       
    end
    # delete all the old KPI
    Kpi_2011_12_28_10_10.delete_all
    
    create_table "kpi_ownerships", :force => true do |t|
      t.references "kpiable", :polymorphic => true
      t.references "kpi"       
      t.references :parent, :polymorphic => true
    end
    
    add_index(:kpi_ownerships, [:kpiable_id, :kpiable_type, :parent_id, :parent_type], :name => "index_kpi_ownerships")
    add_index(:kpis, [:name, :for_klass, :actual_kpi], :name => "index_kpi_name_klass_actual")
  end
  
  def down
    change_table :kpis do |t|
      t.references :kpiable, :polymorphic => true
      t.change :warning_threshold, :integer     
      t.change :error_threshold, :integer
    end
    remove_index(:kpi_ownerships, :name => "index_kpi_ownerships")
    remove_index(:kpis, :name => "index_kpi_name_klass_actual")
    drop_table "kpi_ownerships"  
  end
end

