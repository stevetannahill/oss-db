class AddRunManuallyToReports < ActiveRecord::Migration
  def change
    add_column :reports, :run_manually, :boolean, :default => false
  end
end
