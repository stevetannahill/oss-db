class AddDemarcIconToDemarc < ActiveRecord::Migration
  def self.up
    add_column :demarcs, :demarc_icon, :string, :default => "building"
  end

  def self.down
    remove_column :demarcs, :demarc_icon
  end
end
