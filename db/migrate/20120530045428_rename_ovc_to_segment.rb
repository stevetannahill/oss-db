class OvcNew < InstanceElement
end
class CenxOvcNew < OvcNew
  self.table_name = 'segments'
end
class MemberOvc < OvcNew
  self.table_name = 'segments'
end

class CenxOvcEndPointEnni < OvcEndPointEnni
end

class CenxOvcOrderNew < OvcOrder
end
class MemberOvcOrder < OvcOrder
end


class OvcOrderStateful < OrderStateful
end
class OrderAcceptedOvc < OrderAccepted
end
class OrderCreatedOvc < OrderCreated
end
class OrderCustomerAcceptedOvc < OrderCustomerAccepted
end
class OrderDeliveredOvc < OrderDelivered
end
class OrderDesignCompleteOvc < OrderDesignComplete
end
class OrderProvisionedOvc < OrderProvisioned
end
class OrderReceivedOvc < OrderReceived
end
class OrderTestedOvc < OrderTested
end

class OvcProvStateful < ProvStateful
end
class ProvDeletedOvc < ProvDeleted
end
class ProvLiveOvc < ProvLive
end
class ProvMaintenanceOvc < ProvMaintenance
end
class ProvPendingOvc < ProvPending
end
class ProvReadyOvc < ProvReady
end
class ProvTestingOvc < ProvTesting
end





class RenameOvcToSegment < ActiveRecord::Migration
  def up
    #Rename CenxOvc
    rename_column :cenx_ovc_end_point_ennis_qos_policies, :cenx_ovc_end_point_enni_id, :on_net_ovc_end_point_enni_id
    rename_table :cenx_ovc_end_point_ennis_qos_policies, :on_net_ovc_end_point_ennis_qos_policies

    #Rename Ovc
    rename_column :cos_end_points, :ovc_end_point_new_id, :segment_end_point_id
    rename_column :cos_instances, :ovc_new_id, :segment_id
    rename_column :demarc_types, :max_num_ovcs, :max_num_segments
    rename_column :demarc_types, :max_num_ovcs_notes, :max_num_segments_notes
    rename_column :demarc_types, :outer_tag_ovc_mapping, :outer_tag_segment_mapping
    rename_column :ethernet_service_types_ovc_end_point_types, :ovc_end_point_type_id, :segment_end_point_type_id
    rename_column :ethernet_service_types_ovc_types, :ovc_type_id, :segment_type_id
    rename_column :evcs_ovc_news, :ovc_new_id, :segment_id
    rename_column :ovc_end_point_news, :ovc_end_point_type_id, :segment_end_point_type_id
    rename_column :ovc_end_point_news, :ovc_new_id, :segment_id
    rename_column :ovc_end_point_news_ovc_end_point_news, :ovc_end_point_new_id, :segment_end_point_id
    #rename_column :ovc_monitors, :ovc_id, :segment_id
    rename_column :ovc_news, :ovc_owner_role, :segment_owner_role
    rename_column :ovc_types, :ovc_type, :segment_type
    rename_column :ovc_news, :ovc_type_id, :segment_type_id
    rename_column :demarcs, :max_num_ovcs, :max_num_segments

    rename_table :ethernet_service_types_ovc_end_point_types, :ethernet_service_types_segment_end_point_types
    rename_table :ethernet_service_types_ovc_types, :ethernet_service_types_segment_types
    rename_table :evcs_ovc_news, :evcs_segments
    rename_table :ovc_end_point_news, :segment_end_points
    rename_table :ovc_end_point_news_ovc_end_point_news, :segment_end_points_segment_end_points
    rename_table :ovc_end_point_types, :segment_end_point_types
    #rename_table :ovc_monitors, :segment_monitors
    rename_table :ovc_types, :segment_types
    rename_table :ovc_news, :segments

    #Update CenxOvc
    CenxOvcNew.update_all(:type => 'OnNetOvc')
    CenxOvcEndPointEnni.update_all(:type => 'OnNetOvcEndPointEnni')
    CenxOvcOrderNew.update_all({:type => 'OnNetOvcOrder', :ordered_entity_type => 'Segment', :ordered_entity_subtype => 'OnNetOvc'})

    #Update MemberOvc
    MemberOvc.update_all(:type => 'OffNetOvc')
    MemberOvcOrder.update_all({:type => 'OffNetOvcOrder', :ordered_entity_type => 'Segment', :ordered_entity_subtype => 'OffNetOvc'})

    #Update Ovc Statefuls
    OvcOrderStateful.update_all(:type => "SegmentOrderStateful")
    OrderAcceptedOvc.update_all(:type => "OrderAcceptedSegment")
    OrderCreatedOvc.update_all(:type => "OrderCreatedSegment")
    OrderCustomerAcceptedOvc.update_all(:type => "OrderCustomerAcceptedSegment")
    OrderDeliveredOvc.update_all(:type => "OrderDeliveredSegment")
    OrderDesignCompleteOvc.update_all(:type => "OrderDesignCompleteSegment")
    OrderProvisionedOvc.update_all(:type => "OrderProvisionedSegment")
    OrderReceivedOvc.update_all(:type => "OrderReceivedSegment")
    OrderTestedOvc.update_all(:type => "OrderTestedSegment")

    OvcProvStateful.update_all(:type => "SegmentProvStateful")
    ProvDeletedOvc.update_all(:type => "ProvDeletedSegment")
    ProvLiveOvc.update_all(:type => "ProvLiveSegment")
    ProvMaintenanceOvc.update_all(:type => "ProvMaintenanceSegment")
    ProvPendingOvc.update_all(:type => "ProvPendingSegment")
    ProvReadyOvc.update_all(:type => "ProvReadySegment")
    ProvTestingOvc.update_all(:type => "ProvTestingSegment")

    #Update Ownerships
    StatefulOwnership.update_all({:stateable_type => "Segment"}, "stateable_type = 'OvcNew'")
    StatefulOwnership.update_all({:stateable_type => "SegmentEndPoint"}, "stateable_type = 'OvcEndPointNew'")
    EventOwnership.update_all({:eventable_type => "Segment"}, "eventable_type = 'OvcNew'")
    EventOwnership.update_all({:eventable_type => "SegmentEndPoint"}, "eventable_type = 'OvcEndPointNew'")
    MtcOwnership.update_all({:mtcable_type => "Segment"}, "mtcable_type = 'OvcNew'")
    MtcOwnership.update_all({:mtcable_type => "SegmentEndPoint"}, "mtcable_type = 'OvcEndPointNew'")
    SmOwnership.update_all({:smable_type => "Segment"}, "smable_type = 'OvcNew'")
    SmOwnership.update_all({:smable_type => "SegmentEndPoint"}, "smable_type = 'OvcEndPointNew'")
    KpiOwnership.update_all({:kpiable_type => "Segment"}, "kpiable_type = 'OvcNew'")
    KpiOwnership.update_all({:kpiable_type => "SegmentEndPoint"}, "kpiable_type = 'OvcEndPointNew'")
    KpiOwnership.update_all({:parent_type => "Segment"}, "parent_type = 'OvcNew'")
    KpiOwnership.update_all({:parent_type => "SegmentEndPoint"}, "parent_type = 'OvcEndPointNew'")

    #Update Member Attrs
    MemberAttr.update_all({:affected_entity_type => "Segment"}, "affected_entity_type = 'OvcNew'")
    MemberAttr.update_all({:affected_entity_type => "SegmentEndPoint"}, "affected_entity_type = 'OvcEndPointNew'")
    MemberAttrInstance.update_all({:affected_entity_type => "Segment"}, "affected_entity_type = 'OvcNew'")
    MemberAttrInstance.update_all({:affected_entity_type => "SegmentEndPoint"}, "affected_entity_type = 'OvcEndPointNew'")
    
  end

  def down
    rename_table :ethernet_service_types_segment_end_point_types, :ethernet_service_types_ovc_end_point_types
    rename_table :ethernet_service_types_segment_types, :ethernet_service_types_ovc_types
    rename_table :evcs_segments, :evcs_ovc_news
    rename_table :segment_end_points, :ovc_end_point_news
    rename_table :segment_end_points_segment_end_points, :ovc_end_point_news_ovc_end_point_news
    rename_table :segment_end_point_types, :ovc_end_point_types
    rename_table :segment_monitors, :ovc_monitors
    rename_table :segment_types, :ovc_types
    rename_table :segments, :ovc_news

    rename_column :cos_end_points, :segment_end_point_id, :ovc_end_point_new_id
    rename_column :cos_instances, :segment_id, :ovc_new_id
    rename_column :demarc_types, :max_num_segments, :max_num_ovcs
    rename_column :demarc_types, :max_num_segments_notes, :max_num_ovcs_notes
    rename_column :demarc_types, :outer_tag_segment_mapping, :outer_tag_ovc_mapping
    rename_column :ethernet_service_types_ovc_end_point_types, :segment_end_point_type_id, :ovc_end_point_type_id
    rename_column :ethernet_service_types_ovc_types, :segment_type_id, :ovc_type_id
    rename_column :evcs_ovc_news, :segment_id, :ovc_new_id
    rename_column :ovc_end_point_news, :segment_end_point_type_id, :ovc_end_point_type_id
    rename_column :ovc_end_point_news, :segment_id, :ovc_new_id
    rename_column :ovc_end_point_news_ovc_end_point_news, :segment_end_point_id, :ovc_end_point_new_id
    rename_column :ovc_monitors, :segment_id, :ovc_id
    rename_column :ovc_news, :segment_owner_role, :ovc_owner_role
    rename_column :ovc_news, :segment_type_id, :ovc_type_id
    rename_column :ovc_types, :segment_type, :ovc_type
    rename_column :demarcs, :max_num_segments, :max_num_ovcs

    rename_table :on_net_ovc_end_point_ennis_qos_policies, :cenx_ovc_end_point_ennis_qos_policies
    rename_column :cenx_ovc_end_point_ennis_qos_policies, :on_net_ovc_end_point_enni_id, :cenx_ovc_end_point_enni_id

    OffNetOvc.update_all(:type => 'MemberOvc')
    OffNetOvcOrder.update_all(:type => 'MemberOvcOrder')

    OnNetOvc.update_all(:type => 'CenxOvcNew')
    OnNetOvcEndPointEnni.update_all(:type => 'CenxOvcEndPointEnni')
    OnNetOvcOrder.update_all(:type => 'CenxOvcOrderNew')
  end
end
