class LsEvcExchangeMods < ActiveRecord::Migration
  def self.up
    add_column :exchange_locations, :site_access_notes, :text
    add_column :evcs, :cenx_evc_type, :string, :default => "Standard"
  end

  def self.down
    remove_column :exchange_locations, :site_access_notes
    remove_column :evcs, :cenx_evc_type
  end
end
