class AddReportingRoles < ActiveRecord::Migration
  def up
    reports_role = Role.find_by_name('Reports')

    sla_report =            Role.find_or_create_by_name(:name => 'SLA Reports')
    circuit_util_report =   Role.find_or_create_by_name(:name => 'Circuit Utilization Reports')
    enni_util_report =      Role.find_or_create_by_name(:name => 'ENNI Utilization Reports')
    scheduled_report =      Role.find_or_create_by_name(:name => 'Scheduled Reports')

    reports_role.privileges.each do |p|
      p.roles << [sla_report, circuit_util_report, enni_util_report, scheduled_report]
    end
  
    reports_role.delete
  end

  def down
    Role.destroy_all(:name => ['SLA Reports', 'Circuit Utilization Reports', 'ENNI Utilization Reports', 'Scheduled Reports'])
    Role.find_or_create_by_name('Reports')
  end
end
