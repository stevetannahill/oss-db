class ChangeNetworkManagerNames < ActiveRecord::Migration

  class NetworkManager < ActiveRecord::Base
  end


  def self.up
    conversion = {
      "5620 SAM - Standard" => "5620 SAM - Live",
      "5620 SAM - LightSquared" => "5620 SAM - Lab"
    }
    NetworkManager.all.each do |nm|
      if conversion[nm.name]
        nm.name = conversion[nm.name]
        nm.save(:validate => false)
      end
    end
  end

  def self.down
  end
end
