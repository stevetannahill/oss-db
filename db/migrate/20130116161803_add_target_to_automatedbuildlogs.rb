class AddTargetToAutomatedbuildlogs < ActiveRecord::Migration
  def up
    change_table :automated_build_logs do |t|
      t.references :associated_object, :polymorphic => true      
    end
  end
  
  def down
    change_table :automated_build_logs do |t|
      t.remove_references :associated_object, :polymorphic => true      
    end
  end
  
end
