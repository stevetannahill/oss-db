class RemoveObsoleteExceptionTables < ActiveRecord::Migration
  def up
    drop_table :data_archives
    drop_table :sla_exceptions
  end

  def down
    create_table data_archives do |t|
      t.string :adapter
      t.string :host
      t.string :username
      t.string :password
      t.integer :port
      t.integer :number_of_ctvs_per_shard
    end
    create_table sla_exceptions do |t|
      t.string :type
      t.integer :time_declared
      t.float :value
      t.integer :enni_new_id
      t.integer :cos_end_point_id
      t.integer :time_over
      t.float :average_rate_while_over_threshold_mbps
      t.integer :cos_test_vector_id
      t.string :sla_type
      t.string :direction
      t.string :severity
    end
  end
end


=begin
CREATE TABLE `data_archives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adapter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `host` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `number_of_ctvs_per_shard` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

  CREATE TABLE `sla_exceptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_declared` int(11) DEFAULT NULL,
  `value` float DEFAULT NULL,
  `enni_new_id` int(11) DEFAULT NULL,
  `cos_end_point_id` int(11) DEFAULT NULL,
  `time_over` int(11) DEFAULT NULL,
  `average_rate_while_over_threshold_mbps` float DEFAULT NULL,
  `cos_test_vector_id` int(11) DEFAULT NULL,
  `sla_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direction` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `severity` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sla_exceptions_on_enni_new_id` (`enni_new_id`),
  KEY `index_sla_exceptions_on_cos_end_point_id` (`cos_end_point_id`),
  KEY `index_sla_exceptions_on_cos_test_vector_id` (`cos_test_vector_id`)
) ENGINE=InnoDB;

=end

