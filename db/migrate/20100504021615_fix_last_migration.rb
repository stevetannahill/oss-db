class FixLastMigration < ActiveRecord::Migration
  def self.up
    add_column :service_providers, :notes, :string
  end

  def self.down
    remove_column :service_providers, :notes
  end
end
