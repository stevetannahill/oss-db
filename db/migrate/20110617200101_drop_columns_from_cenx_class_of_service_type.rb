class DropColumnsFromCenxClassOfServiceType < ActiveRecord::Migration
  def self.up
    remove_column :class_of_service_types, :short_name
    remove_column :class_of_service_types, :fwding_class
    remove_column :class_of_service_types, :cir_cac_limit
    remove_column :class_of_service_types, :eir_cac_limit
  end

  def self.down
    add_column :class_of_service_types, :short_name, :string
    add_column :class_of_service_types, :fwding_class, :string
    add_column :class_of_service_types, :cir_cac_limit, :string
    add_column :class_of_service_types, :eir_cac_limit, :string
  end
end
