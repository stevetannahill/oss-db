class RenameDelayMs2DelayUs < ActiveRecord::Migration
  
  class InstanceElement < ActiveRecord::Base
    self.abstract_class = true
    self.inheritance_column = :_type_disabled
  end
  
  class TypeElement < ActiveRecord::Base
    self.abstract_class = true
    self.inheritance_column = :_type_disabled
  end  
  
  class CosTestVector < InstanceElement
    self.inheritance_column = :_type_disabled
  end
  
  class ServiceLevelGuaranteeType < TypeElement
    self.inheritance_column = :_type_disabled
  end  
      
  def up
    rename_column :service_level_guarantee_types, :delay_ms, :delay_us
    ServiceLevelGuaranteeType.reset_column_information
    # old data may have empty mandatory columns and we have no way to datafill
    ServiceLevelGuaranteeType.find_each do |slgt|
      delay_str = slgt.delay_us.gsub(/[^\d.]/, '')
      if is_num(delay_str)
        delay_i = (delay_str.to_f * 1000.0).to_i
        slgt.delay_us = slgt.delay_us.sub("#{delay_str}", delay_i.to_s)
        slgt.save(:validate => false)
      end
    end
    CosTestVector.find_each do |ctv|
      ctv.delay_error_threshold   = ctv.delay_error_threshold.to_i   * 1000
      ctv.delay_sla_guarantee     = ctv.delay_sla_guarantee.to_i     * 1000
      ctv.delay_warning_threshold = ctv.delay_warning_threshold.to_i * 1000
      ctv.save(:validate => false)
    end
  end

  def down
    rename_column :service_level_guarantee_types, :delay_us, :delay_ms
    ServiceLevelGuaranteeType.reset_column_information
    ServiceLevelGuaranteeType.find_each do |slgt|
      if is_num(delay_str)
        slgt.delay_ms = slgt.delay_ms.to_f / 1000.0
        slgt.save(:validate => false)
      end
    end
    CosTestVector.find_each do |ctv|
      ctv.delay_error_threshold   = ctv.delay_error_threshold.to_f   / 1000.0
      ctv.delay_sla_guarantee     = ctv.delay_sla_guarantee.to_f     / 1000.0
      ctv.delay_warning_threshold = ctv.delay_warning_threshold.to_f / 1000.0
      ctv.save(:validate => false)
    end
  end

  def is_num(input)
    input.to_s.match(/\A[+-]?\d+?(\.\d+)?\Z/) != nil
  end

end
