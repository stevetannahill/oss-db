class IndexEvents < ActiveRecord::Migration
  def self.up
    add_index :event_ownerships, [:eventable_id, :eventable_type]
    add_index :event_records, [:event_history_id, :type]
    add_index :mtc_ownerships, [:mtcable_id, :mtcable_type]
  end

  def self.down
    remove_index :event_ownerships, [:eventable_id, :eventable_type]
    remove_index :event_records, [:event_history_id, :type]
    remove_index :mtc_ownerships, [:mtcable_id, :mtcable_type]
  end
end
