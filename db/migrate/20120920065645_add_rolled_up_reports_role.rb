class AddRolledUpReportsRole < ActiveRecord::Migration
  def up
    Role.find_or_create_by_name(:name => 'Rolled Up Reports')
  end

  def down
    Role.destroy_all(:name => 'Rolled Up Reports')
  end
end