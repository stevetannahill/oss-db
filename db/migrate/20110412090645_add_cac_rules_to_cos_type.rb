class AddCacRulesToCosType < ActiveRecord::Migration
  def self.up
    add_column :class_of_service_types, :cir_cac_limit, :integer
    add_column :class_of_service_types, :eir_cac_limit, :integer

    CenxClassOfServiceType.find(:all).each do |cost|
      if cost.name == "Standard Data"
        cost.cir_cac_limit = 0
        cost.eir_cac_limit = 300
      else
        cost.cir_cac_limit = 90
        cost.eir_cac_limit = 0
      end
      cost.save false
    end
  end

  def self.down
    remove_column :class_of_service_types, :cir_cac_limit
    remove_column :class_of_service_types, :eir_cac_limit
  end
end
