class FixEvcDisplayEntityIndex < ActiveRecord::Migration
  def self.up
    remove_index :evc_displays, :name => 'evc_entity_index'
    
    #Just flipping the order of the index to increase lookups speed on sphinx index
    add_index :evc_displays, [:evc_id, :entity_type, :entity_id], :name => 'evc_entity_index', :unique => true
  end

  def self.down
    remove_index :evc_displays, :name => 'evc_entity_index'
    
    add_index :evc_displays, [:evc_id, :entity_id, :entity_type], :name => 'evc_entity_index', :unique => true
  end
end
