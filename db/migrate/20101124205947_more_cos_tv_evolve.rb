class MoreCosTvEvolve < ActiveRecord::Migration
  def self.up
    add_column :cos_test_vectors, :flr_error_threshold, :string
    add_column :cos_test_vectors, :flr_warning_threshold, :string
  end

  def self.down
    remove_column :cos_test_vectors, :flr_error_threshold
    remove_column :cos_test_vectors, :flr_warning_threshold
  end
end
