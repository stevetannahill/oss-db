class FixesForNewOvcEndPoints < ActiveRecord::Migration
  def self.up
    add_column :ovc_end_point_news, :connected_end_point_id, :integer
  end

  def self.down
    remove_column :ovc_end_point_news, :connected_end_point_id
  end
end
