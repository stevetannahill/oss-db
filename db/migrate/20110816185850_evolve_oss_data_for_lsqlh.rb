class EvolveOssDataForLsqlh < ActiveRecord::Migration
  def self.up
    change_column :ovc_types, :notes, :text
  end

  def self.down
    change_column :ovc_types, :notes, :string
  end
end
