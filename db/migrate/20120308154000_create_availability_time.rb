class CreateAvailabilityTime < ActiveRecord::Migration
  def change
    create_table :availability_times do |t|
      t.integer :time, :null => false
      t.timestamps
    end
  end
end
