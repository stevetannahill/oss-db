class AddTurnupResultsToCosTestVector < ActiveRecord::Migration
  class InstanceElement < ActiveRecord::Base
    self.abstract_class = true
    self.inheritance_column = :_type_disabled
  end
  
  class CosTestVector < InstanceElement
    self.inheritance_column = :_type_disabled
  end
  
  def up
    change_table :cos_test_vectors, :force => true do |t|
      t.string   "flr_reference"
      t.string   "delay_reference"
      t.string   "delay_min_reference"
      t.string   "delay_max_reference"
      t.string   "dv_reference"
      t.string   "dv_min_reference"
      t.string   "dv_max_reference"
      t.string   "loopback_address"
      t.string   "circuit_id_format"
    end
    
    CosTestVector.connection.schema_cache.clear!
    CosTestVector.reset_column_information
    CosTestVector.inheritance_column = :_type_disabled

    CosTestVector.find_each do |ctv|
      if ctv.type == "SpirentCosTestVector"
        if ctv.circuit_id[/^SPIRENT:/]
          ctv.circuit_id_format = "SpirentAtt"
        elsif ctv.circuit_id[/^WIPM:/]
          ctv.circuit_id_format = "WipmAtt"
        else
          ctv.circuit_id_format = "SpirentSprint"
        end
      else
        ctv.circuit_id_format = "None"
      end
      ctv.save!
    end
  end
  
  def down
    remove_column :cos_test_vectors, :flr_reference, :delay_reference, :delay_min_reference, 
                  :delay_max_reference, :dv_reference, :dv_min_reference, :dv_max_reference, :loopback_address, :circuit_id_format
  end
end
