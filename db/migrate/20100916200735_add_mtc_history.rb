class AddMtcHistory < ActiveRecord::Migration
  def self.up
  
    create_table "mtc_ownerships", :force => true do |t|
      t.references "mtcable", :polymorphic => true
      t.references "mtc_history"       
    end

  end

  def self.down
   drop_table "mtc_ownerships"
  end
end
