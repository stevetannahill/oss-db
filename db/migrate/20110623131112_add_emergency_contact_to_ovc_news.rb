class AddEmergencyContactToOvcNews < ActiveRecord::Migration
  def self.up
    add_column :ovc_news, :emergency_contact_id, :integer
  end

  def self.down
    remove_column :ovc_news, :emergency_contact_id
  end
end
