class FixOssFields < ActiveRecord::Migration
  def self.up
    add_column :enni_profiles, :optical_if_info, :string
    add_column :enni_profiles, :phy_layer_notes, :string
    remove_column :enni_profiles, :tx_power
    remove_column :enni_profiles, :rx_power
    remove_column :enni_profiles, :threasholds
    add_column :enni_profiles, :frame_format_notes, :string
    add_column :enni_profiles, :protection_notes, :string
    add_column :enni_profiles, :oam_notes, :string
    add_column :enni_profiles, :lacp_notes, :string
    change_column :service_types, :max_mtu, :string
    add_column :service_types, :notes, :string
    add_column :class_of_services, :notes, :string
    add_column :service_level_guarantees, :notes, :string
    change_column :service_level_guarantees, :min_dist_km, :string
    change_column :service_level_guarantees, :max_dist_km, :string
    change_column :service_level_guarantees, :availability, :string
    change_column :service_level_guarantees, :frame_loss_ratio, :string
    change_column :service_level_guarantees, :delay_ms, :string
    change_column :service_level_guarantees, :delay_variation_ms, :string
    change_column :service_level_guarantees, :mttr_days, :string
    add_column :bandwidth_profiles, :notes, :string
    add_column :bandwidth_profiles, :name, :string
    add_column :bandwidth_profiles, :cir_range_notes, :string
    add_column :bandwidth_profiles, :cbs_range_notes, :string
    add_column :bandwidth_profiles, :eir_range_notes, :string
    add_column :bandwidth_profiles, :ebs_range_notes, :string
    change_column :bandwidth_profiles, :min_cir_kbps, :string
    change_column :bandwidth_profiles, :max_cir_kbps, :string
    change_column :bandwidth_profiles, :min_cbs_kb, :string
    change_column :bandwidth_profiles, :max_cbs_kb, :string
    change_column :bandwidth_profiles, :min_eir_kbps, :string
    change_column :bandwidth_profiles, :max_eir_kbps, :string
    change_column :bandwidth_profiles, :min_ebs_kb, :string
    change_column :bandwidth_profiles, :max_ebs_kb, :string
    rename_column :uni_profiles, :nid_type, :device_Type
    add_column :uni_profiles, :lacp_notes, :string
  end

  def self.down
    remove_column :enni_profiles, :optical_if_info
    remove_column :enni_profiles, :phy_layer_notes
    add_column :enni_profiles, :tx_power, :float
    add_column :enni_profiles, :rx_power, :float
    add_column :enni_profiles, :threasholds,:string
    remove_column :enni_profiles, :frame_format_notes
    remove_column :enni_profiles, :protection_notes
    remove_column :enni_profiles, :oam_notes
    remove_column :enni_profiles, :lacp_notes
    change_column :service_types, :max_mtu, :integer
    remove_column :service_types, :notes
    remove_column :class_of_services, :notes
    remove_column :service_level_guarantees, :notes
    change_column :service_level_guarantees, :min_dist_km, :integer
    change_column :service_level_guarantees, :max_dist_km, :integer
    change_column :service_level_guarantees, :availability, :decimal
    change_column :service_level_guarantees, :frame_loss_ratio, :decimal
    change_column :service_level_guarantees, :delay_ms, :decimal
    change_column :service_level_guarantees, :delay_variation_ms, :decimal
    change_column :service_level_guarantees, :mttr_days, :decimal
    remove_column :bandwidth_profiles, :notes
    remove_column :bandwidth_profiles, :name
    remove_column :bandwidth_profiles, :cir_range_notes
    remove_column :bandwidth_profiles, :cbs_range_notes
    remove_column :bandwidth_profiles, :eir_range_notes
    remove_column :bandwidth_profiles, :ebs_range_notes
    change_column :bandwidth_profiles, :min_cir_kbps, :integer
    change_column :bandwidth_profiles, :max_cir_kbps, :integer
    change_column :bandwidth_profiles, :min_cbs_kb, :integer
    change_column :bandwidth_profiles, :max_cbs_kb, :integer
    change_column :bandwidth_profiles, :min_eir_kbps, :integer
    change_column :bandwidth_profiles, :max_eir_kbps, :integer
    change_column :bandwidth_profiles, :min_ebs_kb, :integer
    change_column :bandwidth_profiles, :max_ebs_kb, :integer
    rename_column :uni_profiles, :device_Type, :nid_type
    remove_column :uni_profiles, :lacp_notes
  end
end

