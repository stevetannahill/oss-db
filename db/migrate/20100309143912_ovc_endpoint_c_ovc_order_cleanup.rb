class OvcEndpointCOvcOrderCleanup < ActiveRecord::Migration
  def self.up
    add_column :cenx_ovc_orders, :operations_ticket_id, :string
    remove_column :ovc_end_points, :sp_name
    remove_column :ovc_end_points, :cenx_name
  end

  def self.down
    remove_column :cenx_ovc_orders, :operations_ticket_id
    add_column :ovc_end_points, :sp_name, :string
    add_column :ovc_end_points, :cenx_name, :string
  end
end