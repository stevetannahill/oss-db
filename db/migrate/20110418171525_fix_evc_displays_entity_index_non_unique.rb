class FixEvcDisplaysEntityIndexNonUnique < ActiveRecord::Migration
  def self.up
    remove_index :evc_displays, :name => 'evc_entity_index'
    # Non unique index so Cenx OVC can be in row 1 and row 2
    add_index :evc_displays, [:evc_id, :entity_type, :entity_id], :name => 'evc_entity_index', :unique => false
  end

  def self.down
    remove_index :evc_displays, :name => 'evc_entity_index'
    add_index :evc_displays, [:evc_id, :entity_type, :entity_id], :name => 'evc_entity_index', :unique => true
  end
end