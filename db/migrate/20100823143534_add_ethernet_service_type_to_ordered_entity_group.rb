class AddEthernetServiceTypeToOrderedEntityGroup < ActiveRecord::Migration
  def self.up
    add_column :ordered_entity_groups, :ethernet_service_type_id, :integer
  end

  def self.down
    remove_column :ordered_entity_groups, :ethernet_service_type_id
  end
end
