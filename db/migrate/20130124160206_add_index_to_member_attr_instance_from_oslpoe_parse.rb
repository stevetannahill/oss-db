class AddIndexToMemberAttrInstanceFromOslpoeParse < ActiveRecord::Migration
  def change
    add_index :member_attr_instances, [:affected_entity_type, :type, :value], :name => 'index_aff_type_and_type_and_value'
  end
end
