class ChangeMepAlarmToWarning < ActiveRecord::Migration
  def self.up
    AlarmHistory.all.each {|ah|
      if ah.event_filter["mep-"] != nil
        tmp = AlarmSeverity::default_alu_alarm_mapping
        tmp["major"] = AlarmSeverity::WARNING
        ah.alarm_mapping = tmp
        ah.save
      end
    }
  end

  def self.down
    AlarmHistory.all.each {|ah|
      if ah.event_filter["mep-"] != nil
        tmp = AlarmSeverity::default_alu_alarm_mapping
        ah.alarm_mapping = tmp
        ah.save
      end
    }
  end
end
