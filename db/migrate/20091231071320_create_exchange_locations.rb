class CreateExchangeLocations < ActiveRecord::Migration
  def self.up
    create_table :exchange_locations do |t|
      t.string :name
      t.string :address
      t.integer :exchange_id

      t.timestamps
    end
    
     add_column :nodes, :exchange_location_id, :integer
  end

  def self.down
    remove_column :nodes, :exchange_location_id
    drop_table :exchange_locations
  end
end
