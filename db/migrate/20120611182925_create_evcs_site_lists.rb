class CreateEvcsSiteLists < ActiveRecord::Migration
  def change
  	create_table :evcs_site_lists, :force => true do |t|
      t.references :evc, :site_list
    end

    add_index :evcs_site_lists, [:evc_id, :site_list_id], :unique => true
  end

end
