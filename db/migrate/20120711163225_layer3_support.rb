class Layer3Support < ActiveRecord::Migration
  def up
    add_column :segments, :as_id, :string
    add_column :segments, :router_type, :string
    add_column :segments, :router_id, :string
    add_column :segments, :routing_domain, :string
    add_column :segment_end_points, :ip_address, :string
    add_column :segment_end_points, :routing_cost, :string
  end

  def down
    remove_column :segments, :as_id
    remove_column :segments, :router_type
    remove_column :segments, :router_id
    remove_column :segments, :routing_domain
    remove_column :segment_end_points, :ip_address
    remove_column :segment_end_points, :routing_cost
  end
end
