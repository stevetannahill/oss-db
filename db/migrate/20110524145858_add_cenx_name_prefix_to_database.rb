class AddCenxNamePrefixToDatabase < ActiveRecord::Migration
  def self.up
    add_column :demarcs, :cenx_name_prefix, :string
    add_column :evcs, :cenx_name_prefix, :string

    puts "Generating all CENX Names, this may take a while."
    Evc.all.each { |e| e.save false }
    Demarc.all.each { |d| d.save false }
  end

  def self.down
    remove_column :demarcs, :cenx_name_prefix
    remove_column :evcs, :cenx_name_prefix
  end
end
