class CreateScheduledReport < ActiveRecord::Migration
  def change
    create_table :scheduled_reports, :force => true do |t|
      t.string :name, :null => false
      t.references :filter_template
      t.references :exclusion_list
      t.boolean :enabled
      t.string :trigger
      t.text :formats
      t.datetime :last_run
      t.integer :retention_time
      t.string :execution_time
      t.boolean :email_distribution
      t.string :email_to
      t.string :email_cc
      t.string :email_bcc
      t.string :email_subject
      t.string :email_body
      t.timestamps
    end
    
    create_table :filter_templates, :force => true do |t|
      t.string :type
      t.string :name, :null => false
      t.references :user
      t.text :filter_options
      t.string :executable_script
      t.timestamps
    end
    
    create_table :reports, :force => true do |t|
      t.string :type
      t.references :scheduled_report
      t.datetime :run_date
      t.datetime :report_period_start
      t.datetime :report_period_end
      t.integer :minor_exceptions
      t.integer :major_exceptions
      t.integer :critical_exceptions
      t.timestamps
    end
    
    create_table :exclusion_lists, :force => true do |t|
      t.string :name
      t.references :user
      t.timestamps
    end
    
    create_table :excluded_items, :force => true do |t|
      t.references :exclusion_list
      t.string :item
    end
    
  end

end
