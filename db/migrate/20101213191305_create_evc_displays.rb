class CreateEvcDisplays < ActiveRecord::Migration
  def self.up
    create_table :evc_displays do |t|
      t.integer :evc_id, :null => false
      t.integer :entity_id, :null => false
      t.string :entity_type, :null => false
      t.integer :row, :null => false
      t.string :column, :null => false

      t.timestamps
    end
    
    add_index :evc_displays, [:evc_id, :entity_id, :entity_type], :name => 'evc_entity_index', :unique => true
    add_index :evc_displays, [:evc_id, :row, :column], :name => 'row_column_constraint', :unique => true
  end

  def self.down
    drop_table :evc_displays
  end
end
