class AddIsPublicToScheduledTasks < ActiveRecord::Migration
  def change
    add_column :scheduled_tasks, :is_public, :boolean, :default => false
  end
end
