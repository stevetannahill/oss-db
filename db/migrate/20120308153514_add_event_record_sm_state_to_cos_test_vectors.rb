class EventOwnership_21_02_2012_10_10 < ActiveRecord::Base
  set_table_name 'event_ownerships'
end

class InstanceElement_21_02_2012_10_10 < ActiveRecord::Base
  set_inheritance_column :_type_disabled
  self.abstract_class = true  
end

class OvcEndPointNew_21_02_2012_10_10 < InstanceElement_21_02_2012_10_10
  set_table_name 'ovc_end_point_news'
  set_inheritance_column :_type_disabled
end

class CosEndPoint_21_02_2012_10_10 < ActiveRecord::Base
  set_table_name 'cos_end_points'
  set_inheritance_column :_type_disabled
  has_many :event_ownerships, :as => :eventable, :dependent => :destroy, :class_name => "EventOwnership_21_02_2012_10_10"
  belongs_to :ovc_end_point_new, :class_name => "OvcEndPointNew_21_02_2012_10_10"
  belongs_to :event_record, :class_name => "AlarmRecord_21_02_2012_10_10"
end

class AvailabilityHistory_21_02_2012_10_10 < ActiveRecord::Base
  set_table_name 'event_histories'
  set_inheritance_column :_type_disabled
  has_many :alarm_records, :foreign_key => :event_history_id, :dependent => :destroy, :order => "time_of_event", :class_name => "AlarmRecord_21_02_2012_10_10"
end

class AlarmHistory_21_02_2012_10_10 < ActiveRecord::Base
  set_table_name 'event_histories'
  set_inheritance_column :_type_disabled
  has_many :alarm_records, :foreign_key => :event_history_id, :dependent => :destroy, :order => "time_of_event", :class_name => "AlarmRecord_21_02_2012_10_10"
end


class AlarmRecord_21_02_2012_10_10 < ActiveRecord::Base
  set_table_name 'event_records'
  set_inheritance_column :_type_disabled
end

class CosTestVector_21_02_2012_10_10 < ActiveRecord::Base
  set_table_name 'cos_test_vectors'
  set_inheritance_column :_type_disabled
  belongs_to :cos_end_point, :class_name => "CosEndPoint_21_02_2012_10_10"
  belongs_to :event_record, :dependent => :destroy, :class_name => "AlarmRecord_21_02_2012_10_10"
  has_many :alarm_histories, :through => :event_ownerships, :source => :event_history, :dependent => :destroy
  has_many :event_ownerships, :as => :eventable, :dependent => :destroy,  :class_name => "EventOwnership_21_02_2012_10_10"
end

DEFAULT_AVAIABILITY_ALARM_MAPPING = {"cleared"     => AlarmSeverity::OK,
        "warning"     => AlarmSeverity::WARNING,
        "failed"      => AlarmSeverity::FAILED,
        "unavailable" => AlarmSeverity::UNAVAILABLE}

class AddEventRecordSmStateToCosTestVectors < ActiveRecord::Migration
  def self.up
    add_column :cos_test_vectors, :event_record_id, 'integer'
    add_column :cos_test_vectors, :sm_state, :string
    add_column :cos_test_vectors, :sm_details, :string
    add_column :cos_test_vectors, :sm_timestamp, :bigint
    
    CosTestVector_21_02_2012_10_10.reset_column_information
    
    objects = CosTestVector_21_02_2012_10_10.all
    
    objects.each do |object|
      if object.type == "BxCosTestVector" || object.type == "SpirentCosTestVector" 
        # Copy the event reocrd and sm state from the cos_end_point to the
        # Brix CTV as currently there is only one Brix CTV per CosEndPoint
        # For Spirent - nothing is currently live so just copy the alarm history
        old_er = object.cos_end_point.event_record
        sm_state = object.cos_end_point.sm_state
        sm_details = object.cos_end_point.sm_details
        sm_timestamp = object.cos_end_point.sm_timestamp
        # Move the alarm history from the CosEndPoint to The CTV
        event_owner = object.cos_end_point.event_ownerships[0]
        if event_owner != nil
          event_owner.eventable_type = "CosTestVector"
          event_owner.eventable_id = object.id
          if !event_owner.save
            puts "Failed to save #{object.class}:#{object.id} #{event_owner.id} #{object.errors.inspect}"
          end
          # Change any Fake Spirent alarm histroies to AvailabilityHistory
          object.reload
          if object.type == "SpirentCosTestVector" && event_owner != nil
            ah = AlarmHistory_21_02_2012_10_10.find(event_owner.event_history_id)            
            if ah != nil && ah.event_filter[/Fake COSEP/]
              ah.event_filter = "#{object.circuit_id}-Availability"
              ah.type = "AvailabilityHistory"
              puts "Failed to save alarm history #{ah.inspect} for #{object.class}:#{object.id}" if !ah.save
            end
          end
        elsif object.type == "SpirentCosTestVector" 
          # There is no alarm so create one only if it's >= Tested
          case object.cos_end_point.ovc_end_point_new.prov_name
          when "Testing", "Ready", "Live", "Maintenance"
            ah = AvailabilityHistory_21_02_2012_10_10.create(:event_filter => "#{object.circuit_id}-Availability", :alarm_mapping => DEFAULT_AVAIABILITY_ALARM_MAPPING, :type => "AvailabilityHistory")
            ar = AlarmRecord_21_02_2012_10_10.create(:original_state => "cleared", :state => "Ok", :details => "", :time_of_event => Time.now.to_f*1000, :type => "AlarmRecord")
            ah.alarm_records << ar
            eo = EventOwnership_21_02_2012_10_10.create(:eventable_id => object.id, :eventable_type => "CosTestVector", :event_history_id => ah.id)
            ah.save;ar.save;eo.save
          end   
        end
      else
        old_er = AlarmRecord_21_02_2012_10_10.new(:state => AlarmSeverity::OK, :time_of_event => Time.now.to_f*1000, :original_state => "cleared", :details => "", :event_history_id => nil, :type => "AlarmRecord")
        sm_state = Eventable::OK.to_s
        sm_details = ""
        sm_timestamp = Time.now.to_f*1000
      end
      
      if object.event_record_id == nil
        er = AlarmRecord_21_02_2012_10_10.create(:state => old_er.state.to_s, :time_of_event => old_er.time_of_event, :original_state => old_er.original_state, :details => old_er.details, :event_history_id => nil, :type => "AlarmRecord")
        er.save(:validate => false)
        object.update_attribute(:event_record_id, er.id)
        object.class.where("id = ?", object.id).update_all(:sm_state => sm_state, :sm_details => sm_details, :sm_timestamp => sm_timestamp)        
      end
    end
      
  end

  def self.down
    
    objects = CosTestVector_21_02_2012_10_10.all
    
    objects.each do |object|
      object.event_record.destroy if object.event_record != nil
      EventOwnership_21_02_2012_10_10.where(:eventable_id => object.id, :eventable_type => "CosTestVector").each do |event_owner|
        ah = AvailabilityHistory_21_02_2012_10_10.find_by_id(event_owner.event_history_id)
        if ah && ah.event_filter == "#{object.circuit_id}-Availability"
          # delete the created Availability history
          ah.destroy
          event_owner.destroy
        else
          # Move the alarm history back to the CosEndPoint
          event_owner.eventable_type = "CosEndPoint"
          event_owner.eventable_id = object.cos_end_point.id
          event_owner.save
        end
      end
    end
     
    remove_column :cos_test_vectors, :event_record_id
    remove_column :cos_test_vectors, :sm_state
    remove_column :cos_test_vectors, :sm_details
    remove_column :cos_test_vectors, :sm_timestamp  
  end
end
