class AddFalloutReportsRole < ActiveRecord::Migration
  def up
    Role.find_or_create_by_name(:name => 'Inventory Exception Reports')
  end

  def down
    Role.destroy_all(:name => 'Inventory Exception Reports')
  end
end