class RemoveCenxOperatorNetwork < ActiveRecord::Migration
  def up
    remove_column :operator_networks, :type
    rename_column :pops, :cenx_operator_network_id, :operator_network_id
  end

  def down
    add_column :operator_networks, :type, :string
    rename_column :pops, :operator_network_id, :cenx_operator_network_id
  end
end
