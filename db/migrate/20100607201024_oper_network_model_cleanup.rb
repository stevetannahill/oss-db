class OperNetworkModelCleanup < ActiveRecord::Migration
  def self.up
    add_column :operator_networks, :operator_network_type_id, :integer
  end

  def self.down
    remove_column :operator_networks, :operator_network_type_id
  end
end
