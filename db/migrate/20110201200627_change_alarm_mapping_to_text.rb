class ChangeAlarmMappingToText < ActiveRecord::Migration
  def self.up
    change_column :event_histories, :alarm_mapping, :text
  end

  def self.down
    change_column :event_histories, :alarm_mapping, :string
  end
end
