class AddCenxAuth < ActiveRecord::Migration
  def self.up

  create_table "permissions", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "permissions", ["name"], :name => "index_permissions_on_name"

  create_table "permissions_roles", :id => false, :force => true do |t|
    t.integer "permission_id"
    t.integer "role_id"
  end

  add_index "permissions_roles", ["permission_id"], :name => "index_permissions_roles_on_permission_id"
  add_index "permissions_roles", ["role_id"], :name => "index_permissions_roles_on_role_id"

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "service_provider_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "roles_users", :id => false, :force => true do |t|
    t.integer "role_id"
    t.integer "user_id"
  end

  add_index "roles_users", ["role_id"], :name => "index_roles_users_on_role_id"
  add_index "roles_users", ["user_id"], :name => "index_roles_users_on_user_id"


  create_table "users", :force => true do |t|
    t.string   "email",                             :null => false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "crypted_password",    :limit => 40
    t.string   "salt",                :limit => 40
    t.string   "activation_code",     :limit => 40
    t.datetime "last_login_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "service_provider_id"
    t.datetime "password_expiry"
    t.integer  "contact_id"
  end

  add_index "users", ["email"], :name => "index_users_on_email"
  add_index "users", ["service_provider_id"], :name => "index_users_on_service_provider_id"

  end
    def self.down
    drop_table :users
    drop_table :roles
    drop_table :permissions
    drop_table :roles_users
    drop_table :permissions_roles
  end

end
