class CreateRacks < ActiveRecord::Migration
  def self.up
    create_table :racks do |t|
      t.string :name

      t.timestamps
    end
  end

  def self.down
    drop_table :racks
  end
end
