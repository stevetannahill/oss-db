
class AlarmRecord_11_11_2011_10_10 < ActiveRecord::Base
  set_table_name 'event_records'
  set_inheritance_column :_type_disabled
end

class Demarc_11_11_2011_10_10 < ActiveRecord::Base
  set_table_name 'demarcs'
  set_inheritance_column :_type_disabled
end


class DemarcAddAlarmRecord < ActiveRecord::Migration
  def up
    Demarc_11_11_2011_10_10.reset_column_information
    objects = (Demarc_11_11_2011_10_10.all).collect {|object| object if object.event_record_id == nil}.compact
    
    objects.each do |object|
      if object.event_record_id == nil
        er = AlarmRecord_11_11_2011_10_10.create(:state => AlarmSeverity::OK, :time_of_event => Time.now.to_f*1000, :original_state => "cleared", :details => "", :event_history_id => nil, :type => "AlarmRecord")
        er.save(:validate => false)
        object.update_attribute(:event_record_id, er.id)
      end
    end
    
  end

  def down
  end
  
end
