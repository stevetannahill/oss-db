class AddExchangeTypeToExchange < ActiveRecord::Migration
  def self.up
    add_column :exchanges, :exchange_type, :string, :default => "Standard Exchange"
  end

  def self.down
    remove_column :exchanges, :exchange_type
  end
end
