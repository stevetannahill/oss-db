class EnniPortTableCleanupFeb42010 < ActiveRecord::Migration
  def self.up
    add_column :ennis, :phy_type, :string
    add_column :ennis, :other_phy_type, :string
    add_column :ennis, :other_ether_type, :string
    add_column :ennis, :auto_negotiate,  :boolean
    add_column :ennis, :ag_supported,  :boolean
    remove_column :ennis, :fiber_type
    remove_column :ennis, :port_type

    add_column :ports, :phy_type, :string
    add_column :ports, :other_phy_type, :string
    remove_column :ports, :fiber_type
    remove_column :ports, :port_type
    remove_column :ports, :sfp_type

    add_column :enni_orders, :notes, :string
  end

  def self.down
    remove_column :ennis, :phy_type
    remove_column :ennis, :other_phy_type
    remove_column :ennis, :other_ether_type
    remove_column :ennis, :auto_negotiate
    remove_column :ennis, :ag_supported
    add_column :ennis, :fiber_type, :string
    add_column :ennis, :port_type, :string

    remove_column :ports, :phy_type
    remove_column :ports, :other_phy_type
    add_column :ports, :fiber_type, :string
    add_column :ports, :port_type, :string
    add_column :ports, :sfp_type, :string

    remove_column :enni_orders, :notes
  end
end
