class CreateMemberServiceOrderAttrs < ActiveRecord::Migration
  def self.up
    create_table :member_service_order_attrs do |t|
      t.string :type

      # common attributes
      t.string :name
      t.string :attr_value_type
      t.string :attr_value_range
      t.string :attr_table_name
      t.integer :operartor_network_type_id
      t.integer :member_service_order_attr_group_id

      # attributes for type=MemberServiceControlAttr

      t.timestamps
    end

    create_table :member_service_order_attr_groups_member_service_order_attrs, :id => false do |t|
      t.integer :member_service_order_attr_group_id
      t.integer :member_service_order_attr_id
    end

  end

  def self.down
    drop_table :member_service_order_attrs
    drop_table :member_service_order_attr_groups_member_service_order_attrs
    
  end
end