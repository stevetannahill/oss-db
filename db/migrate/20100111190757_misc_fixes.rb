class MiscFixes < ActiveRecord::Migration
  def self.up
    remove_column :ennis, :sp_name
    add_column :ports, :fiber_type, :string
    add_column :ports, :sfp_type, :string
    add_column :ports, :patch_panel_slot_port, :string
    add_column :ports, :strands, :string
    add_column :ports, :color_code, :string
    add_column :ports, :notes, :string
    add_column :ports, :patch_panel_id, :integer #tie_to
    add_column :nodes, :notes, :string
    
  end

  def self.down
    remove_column :nodes, :notes
    remove_column :ports, :patch_panel_id #tie_to
    remove_column :ports, :notes
    remove_column :ports, :color_code
    remove_column :ports, :strands
    remove_column :ports, :patch_panel_slot_port
    remove_column :ports, :sfp_type
    remove_column :ports, :fiber_type
    add_column :ennis, :sp_name, :string
  end
end
