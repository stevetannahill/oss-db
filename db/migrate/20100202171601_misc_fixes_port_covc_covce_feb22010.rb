class MiscFixesPortCovcCovceFeb22010 < ActiveRecord::Migration
  def self.up
    add_column :ports, :mac, :string
    add_column :cenx_ovcs, :exchange_id, :integer
    add_column :cenx_ovc_end_points , :port_id, :integer
  end

  def self.down
    remove_column :ports, :mac
    remove_column :cenx_ovcs, :exchange_id
    remove_column :cenx_ovc_end_points , :port_id
  end
end
