class UpdateCoresiteProductManagementName < ActiveRecord::Migration
  def up
    if contact = Contact.find_by_name("Open Cloud Exchange Product Manager")
      contact.name = "Open Cloud Exchange Support Personnel"
      contact.save
    end
  end

  def down
    if contact = Contact.find_by_name("Open Cloud Exchange Support Personnel")
      contact.name = "Open Cloud Exchange Product Manager"
      contact.save
    end
  end
end
