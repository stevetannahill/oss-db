class MigrateCosTestVectorToBxCosTestVector < ActiveRecord::Migration
  def self.up
    CosTestVector.all.each do |costv|
      costv[:type] = "BxCosTestVector"
      costv.save false
    end
  end

  def self.down
  end
end
