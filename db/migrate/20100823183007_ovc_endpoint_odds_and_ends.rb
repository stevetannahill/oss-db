class OvcEndpointOddsAndEnds < ActiveRecord::Migration
  def self.up
    add_column :ovc_end_point_news, :eth_cfm_configured , :boolean, :default => true
    add_column :operator_network_types, :notes , :string
    add_column :ethernet_service_types, :notes , :string
  end

  def self.down
    remove_column :ovc_end_point_news, :eth_cfm_configured
    remove_column :operator_network_types, :notes
    remove_column :ethernet_service_types, :notes
  end
end
