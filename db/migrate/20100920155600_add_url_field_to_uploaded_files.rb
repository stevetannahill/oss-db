class AddUrlFieldToUploadedFiles < ActiveRecord::Migration
  def self.up
    add_column :uploaded_files, :url, :string
  end

  def self.down
    remove_column :uploaded_files, :url
  end
end
