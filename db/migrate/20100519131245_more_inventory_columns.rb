class MoreInventoryColumns < ActiveRecord::Migration
  def self.up
    add_column :inventory_items, :cenx_po_number, :string
    add_column :inventory_items, :service_start_date, :date
    add_column :inventory_items, :service_end_date, :date
    add_column :inventory_items, :notes, :string
  end

  def self.down
    remove_column :inventory_items, :cenx_po_number
    remove_column :inventory_items, :service_start_date
    remove_column :inventory_items, :service_end_date
    remove_column :inventory_items, :notes
  end
end