class DropPandBfromBtsName < ActiveRecord::Migration
  def up
    conn = ActiveRecord::Base.connection
    conn.select_all("select id,value from member_attr_instances where value like 'BTS%-%'").each do |record|
      id = record["id"]
      old_value = record["value"]
      new_value = old_value.gsub(/-(P|B)/,"")
      conn.update("update member_attr_instances set value = '#{new_value}' where id = '#{id}'")
    end

  end

  def down
    #nothing to unmigrate
  end
end
