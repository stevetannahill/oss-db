class AddNameToStatefuls < ActiveRecord::Migration
  def self.up
    add_column :statefuls, :name, :string
  end

  def self.down
    remove_column :statefuls, :name
  end
end
