class AddRetentionTimeToEventHistory < ActiveRecord::Migration
  def self.up
    add_column :event_histories, :retention_time, :integer
    AlarmHistory.all.each {|ah|
       ah.alarm_mapping = AlarmSeverity::default_alu_alarm_mapping
       ah.save
    }
    MtcHistory.all.each {|ah|
       ah.alarm_mapping = AlarmSeverity::default_alu_alarm_mapping
       ah.save
    }
    BrixAlarmHistory.all.each {|ah|
      ah.alarm_mapping = BrixAlarmHistory::default_brix_alarm_mapping
      ah.save
    }
    EventHistory.find_each {|eh| eh.save}
  end

  def self.down
    remove_column :event_histories, :retention_time
  end
end
