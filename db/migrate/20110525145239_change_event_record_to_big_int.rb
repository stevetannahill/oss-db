class EventRecord < ActiveRecord::Base
end  

class ChangeEventRecordToBigInt < ActiveRecord::Migration
  def self.up
      puts "This may take some time"
      add_column :event_records, :timestamp_bi, 'bigint', :null => false
      EventRecord.all.each {|r| r.timestamp_bi = r.time_of_event.to_i;r.save }
      remove_column :event_records, :time_of_event
      rename_column :event_records, :timestamp_bi, :time_of_event
  end

  def self.down
    puts "This may take some time"
    add_column :event_records, :time_of_event_str, :string
    EventRecord.all.each {|r| r.time_of_event_str  = r.time_of_event.to_s; r.save}
    remove_column :event_records, :time_of_event
    rename_column :event_records, :time_of_event_str, :time_of_event
  end
end
