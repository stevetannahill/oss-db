class NewOssChanges < ActiveRecord::Migration
  def self.up
    change_column :demarc_types, :auto_negotiate, :string
    add_column :demarc_types, :lag_control, :string
    rename_column :demarc_types, :reflection_supported, :cfm_supported

    add_column :ovc_types, :underlying_technology, :string
    add_column :ovc_types, :equipment_vendor, :string
    add_column :ovc_types, :service_attribute_notes, :string
    add_column :ovc_types, :max_num_cos, :string

    rename_column :class_of_service_types, :cos_value_enni, :cos_mapping_enni_ingress
    add_column :class_of_service_types, :cos_marking_enni_egress, :string
    rename_column :class_of_service_types, :cos_value_uni, :cos_mapping_uni_ingress
    add_column :class_of_service_types, :cos_marking_uni_egress, :string
    
    add_column :bw_profile_types, :rate_limiting_performed, :boolean
    add_column :bw_profile_types, :rate_limiting_mechanism,  :string
    add_column :bw_profile_types, :rate_limiting_layer,  :string
    add_column :bw_profile_types, :rate_limiting_notes, :string
  end

  def self.down
    change_column :demarc_types, :auto_negotiate, :boolean
    remove_column :demarc_types, :lag_control
    rename_column :demarc_types, :cfm_supported, :reflection_supported

    remove_column :ovc_types, :underlying_technology
    remove_column :ovc_types, :equipment_vendor
    remove_column :ovc_types, :service_attribute_notes
    remove_column :ovc_types, :max_num_cos

    rename_column :class_of_service_types, :cos_mapping_enni_ingress, :cos_value_enni
    remove_column :class_of_service_types, :cos_marking_enni_egress
    rename_column :class_of_service_types, :cos_mapping_uni_ingress, :cos_value_uni
    remove_column :class_of_service_types, :cos_marking_uni_egress

    remove_column :bw_profile_types, :rate_limiting_performed
    remove_column :bw_profile_types, :rate_limiting_mechanism
    remove_column :bw_profile_types, :rate_limiting_layer
    remove_column :bw_profile_types, :rate_limiting_notes

  end
end
