class AddUsernamePasswordToNetworkManager < ActiveRecord::Migration
  def change
    add_column :network_managers, :password, :string
    add_column :network_managers, :username, :string
  end
end
