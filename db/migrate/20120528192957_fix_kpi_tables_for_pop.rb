class FixKpiTablesForPop < ActiveRecord::Migration
  class Kpi < ActiveRecord::Base
    self.inheritance_column = :_type_disabled
  end
  
  class KpiOwnership < ActiveRecord::Base
    self.inheritance_column = :_type_disabled
  end
  
  
  def up
    KpiOwnership.all.each do |eo|
      if eo.kpiable_type == "Exchange"
        eo.kpiable_type = "Pop"
      end
      if eo.parent_type == "Exchange"
        eo.parent_type = "Pop"
      end
      eo.save!
    end
  end

  def down
    KpiOwnership.all.each do |eo|
      if eo.kpiable_type == "Pop"
        eo.kpiable_type = "Exchange"
      end
      if eo.parent_type == "Pop"
        eo.parent_type = "Exchange"
      end
      eo.save!
    end
  end
end
