class AddReportingIndexes < ActiveRecord::Migration
  def change
    # Add compound rolled up report indexes
    add_index :rollup_report_site_groups, [:site_group_id, :period_start_epoch], :name => 'index_rollup_report_site_groups_site_period'
    add_index :rollup_report_site_groups, [:site_group_id, :period_start_epoch, :severity], :name => 'index_rollup_report_site_groups_site_period_sev'

    add_index :rollup_report_sites, [:site_id, :period_start_epoch], :name => 'rollup_report_sites_site_period'
    add_index :rollup_report_sites, [:site_id, :period_start_epoch, :severity], :name => 'rollup_report_sites_site_period_sev'

    add_index :rollup_report_paths, [:site_id, :period_start_epoch], :name => 'index_rollup_report_paths_site_period'
    add_index :rollup_report_paths, [:site_id, :period_start_epoch, :severity], :name => 'index_rollup_report_paths_site_period_sev'

    add_index :rollup_report_demarcs, [:site_id, :period_start_epoch], :name => 'index_rollup_report_demarcs_site_period'
    add_index :rollup_report_demarcs, [:site_id, :period_start_epoch, :severity], :name => 'index_rollup_report_demarcs_site_period_sev'
    
    add_index :metrics_cos_test_vectors, [:grid_circuit_id, :time_declared_epoch], :name => 'index_rollup_metrics_cos_test_vectors_grid_time'
    add_index :metrics_cos_test_vectors, [:grid_circuit_id, :time_declared_epoch, :severity], :name => 'index_rollup_metrics_cos_test_vectors_grid_time_sev'

    add_index :exception_sla_cos_test_vectors, [:grid_circuit_id, :time_declared], :name => 'index_exception_sla_cos_test_vectors_grid_time'
    add_index :exception_sla_cos_test_vectors, [:grid_circuit_id, :time_declared, :severity], :name => 'index_exception_sla_cos_test_vectors_grid_time_sev'

    # Add missing foreign-key indexes
    add_index :bulk_orders_service_orders, :service_order_id
    add_index :bulk_orders_service_orders, :bulk_order_id
    add_index :cabinets, :site_id
    add_index :class_of_service_types, :operator_network_type_id
    add_index :contacts, :service_provider_id
    add_index :cos_end_points, :cos_instance_id
    add_index :cos_end_points, :connected_cos_end_point_id
    add_index :cos_end_points, :event_record_id
    add_index :cos_test_vectors, :test_instance_class_id
    add_index :cos_test_vectors, :cos_end_point_id
    add_index :cos_test_vectors, :service_level_guarantee_type_id
    add_index :cos_test_vectors, :service_instance_id
    add_index :cos_test_vectors, :sla_id
    add_index :cos_test_vectors, :test_instance_id
    add_index :cos_test_vectors, :event_record_id
    add_index :cos_test_vectors, :last_hop_circuit_id
    add_index :cos_test_vectors, :protection_path_id
    add_index :cos_test_vectors, :ref_grid_circuit_id
    add_index :demarc_types, :operator_network_type_id
    add_index :demarcs, :cenx_id
    add_index :demarcs, :operator_network_id
    add_index :demarcs, :demarc_type_id
    add_index :demarcs, :lag_id
    add_index :demarcs, :emergency_contact_id
    add_index :demarcs, :on_behalf_of_service_provider_id
    add_index :demarcs, :event_record_id
    add_index :demarcs, :site_id
    add_index :ethernet_service_types, :operator_network_type_id
    add_index :exception_sla_cos_test_vectors, :grid_circuit_id
    add_index :exception_sla_network_types, :operator_network_type_id
    add_index :exception_utilization_cos_end_points, :cos_end_point_id
    add_index :exception_utilization_ennis, :enni_new_id
    add_index :fallout_exceptions, :fallout_item_id
    add_index :inventory_items, :site_id
    add_index :kpi_ownerships, :kpi_id
    add_index :member_attr_controls, :member_attr_id
    add_index :mtc_ownerships, :mtc_history_id
    add_index :network_management_servers, :network_manager_id
    add_index :network_managers, :data_archive_id
    add_index :network_managers, :event_record_id
    add_index :nodes, :site_id
    add_index :nodes, :cabinet_id
    add_index :nodes, :network_manager_id
    add_index :nodes, :event_record_id
    add_index :operator_network_types, :service_provider_id
    add_index :operator_networks, :job_id
    add_index :operator_networks, :operator_network_type_id
    add_index :order_annotations, :service_order_id
    add_index :order_types, :operator_network_type_id
    add_index :ordered_entity_groups, :order_type_id
    add_index :ordered_entity_groups, :ethernet_service_type_id
    add_index :ordered_entity_type_links, :ordered_entity_group_id
    add_index :ordered_entity_type_links, :ordered_entity_type_id
    add_index :oss_infos, :operator_network_type_id
    add_index :oss_infos, :cenx_contact_id
    add_index :oss_infos, :member_contact_id
    add_index :patch_panels, :site_id
    add_index :path_types, :operator_network_type_id
    add_index :paths, :cenx_id
    add_index :paths, :path_type_id
    add_index :paths, :event_record_id
    add_index :paths, :fallout_exception_id
    add_index :ports, :node_id
    add_index :ports, :patch_panel_id
    add_index :ports, :connected_port_id
    add_index :ports, :demarc_id
    add_index :qos_policies, :policy_id
    add_index :qos_policies, :site_id
    add_index :reports, :schedule_id
    add_index :rollup_ids, :circuit_id
    add_index :scheduled_tasks, :user_id
    add_index :schedules, :scheduled_task_id
    add_index :segment_end_point_types, :operator_network_type_id
    add_index :segment_end_point_types, :class_of_service_type_id
    add_index :segment_end_points, :cenx_id
    add_index :segment_end_points, :segment_end_point_type_id
    add_index :segment_end_points, :mep_id
    add_index :segment_end_points, :event_record_id
    add_index :segment_types, :operator_network_type_id
    add_index :segments, :segment_type_id
    add_index :segments, :cenx_id
    add_index :segments, :service_id
    add_index :segments, :ethernet_service_type_id
    add_index :segments, :path_network_id
    add_index :segments, :emergency_contact_id
    add_index :segments, :event_record_id
    add_index :segments, :site_id
    add_index :service_level_guarantee_types, :class_of_service_type_id
    add_index :service_orders, :primary_contact_id
    add_index :service_orders, :testing_contact_id
    add_index :service_orders, :operator_network_id
    add_index :service_orders, :path_id
    add_index :service_orders, :ordered_entity_group_id
    add_index :service_orders, :ordered_operator_network_id
    add_index :service_orders, :ordered_object_type_id
    add_index :service_orders, :technical_contact_id
    add_index :service_orders, :local_contact_id
    add_index :service_providers, :cenx_id
    add_index :site_groups_sites, :site_id
    add_index :site_groups_sites, :site_group_id
    add_index :sites, :operator_network_id
    add_index :sla_exceptions, :enni_new_id
    add_index :sla_exceptions, :cos_end_point_id
    add_index :sla_exceptions, :cos_test_vector_id
    add_index :stateful_ownerships, :stateful_id
    add_index :top_talkers, :enni_id
    add_index :top_talkers, :cos_end_point_id
    add_index :uploaded_files, :uploader_id
  end
end
