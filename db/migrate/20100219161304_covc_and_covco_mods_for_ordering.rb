class CovcAndCovcoModsForOrdering < ActiveRecord::Migration
  def self.up
    add_column :cenx_ovc_orders, :expedite, :bool
    add_column :enni_orders, :expedite, :bool
    add_column :cenx_ovcs, :service_id, :integer
    add_column :cenx_ovcs, :service_type, :string
    remove_column :cenx_ovcs, :on_switch_name
    add_column :ennis, :lag_id, :integer
    add_column :nodes, :loopback_ip, :string
    change_column :cenx_ovc_end_points, :stag, :string
  end

  def self.down
    remove_column :cenx_ovc_orders, :expedite
    remove_column :enni_orders, :expedite
    remove_column :cenx_ovcs, :service_id
    remove_column :cenx_ovcs, :service_type
    add_column :cenx_ovcs, :on_switch_name, :string
    remove_column :ennis, :lag_id
    remove_column :nodes, :loopback_ip
    change_column :cenx_ovc_end_points, :stag, :integer
  end
end
