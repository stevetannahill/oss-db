class CreateBwProfileTypes < ActiveRecord::Migration
  def self.up
    create_table :bw_profile_types do |t|
      t.string :name
      t.string :bwp_type
      t.integer :class_of_service_type_id
      t.string :min_cir_kbps
      t.string :max_cir_kbps
      t.string :cir_range_notes
      t.string :min_cbs_kb
      t.string :max_cbs_kb
      t.string :cbs_range_notes
      t.string :min_eir_kbps
      t.string :max_eir_kbps
      t.string :eir_range_notes
      t.string :min_ebs_kb
      t.string :max_ebs_kb
      t.string :ebs_range_notes
      t.string :color_mode
      t.string :coupling_flag
      t.string :notes

      t.timestamps
    end
  end

  def self.down
    drop_table :bw_profile_types
  end
end
