class AddTermsToServiceProviders < ActiveRecord::Migration
  def change
    add_column :service_providers, :terms, :text
  end
end
