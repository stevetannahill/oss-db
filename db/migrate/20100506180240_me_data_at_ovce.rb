class MeDataAtOvce < ActiveRecord::Migration
  def self.up
    add_column :ovc_end_points, :md_format, :string
    add_column :ovc_end_points, :md_level, :string
    add_column :ovc_end_points, :md_name_ieee, :string
    add_column :ovc_end_points, :ma_format, :string
    add_column :ovc_end_points, :ma_name, :string
    add_column :ovc_end_points, :reflection_mechanism, :string
  end

  def self.down
    remove_column :ovc_end_points, :md_format
    remove_column :ovc_end_points, :md_level
    remove_column :ovc_end_points, :md_name_ieee
    remove_column :ovc_end_points, :ma_format
    remove_column :ovc_end_points, :ma_name
    remove_column :ovc_end_points, :reflection_mechanism
  end
end
