class AddEmergencyContactToDemarc < ActiveRecord::Migration
  def self.up
    add_column :demarcs, :emergency_contact_id, :integer
  end

  def self.down
    remove_column :demarcs, :emergency_contact_id
  end
end
