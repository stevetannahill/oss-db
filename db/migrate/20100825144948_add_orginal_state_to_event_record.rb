class AddOrginalStateToEventRecord < ActiveRecord::Migration
  def self.up
    add_column :event_records, :original_state, :string
  end

  def self.down
    remove_column :event_records, :original_state
  end
end
