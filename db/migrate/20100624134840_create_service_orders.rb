class CreateServiceOrders < ActiveRecord::Migration
  def self.up
    create_table :service_orders do |t|
      t.string :type
      t.string :title
      t.string :cenx_id
      t.string :order_type
      t.integer :primary_contact_id
      t.integer :testing_contact_id
      t.date :requested_service_date
      t.date :order_received_date
      t.date :order_acceptance_date
      t.date :order_completion_date
      t.date :customer_acceptance_date
      t.date :billing_start_date
      t.integer :operator_network_id
      t.boolean :expedite
      t.string :operations_ticket_id
      t.string :status
      t.string :notes
      t.integer :ordered_entity_id # whatever is being ordered
      t.string :ordered_entity_type

      t.timestamps
    end
  end

  def self.down
    drop_table :service_orders
  end
end
