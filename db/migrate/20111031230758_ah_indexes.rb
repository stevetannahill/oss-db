class AhIndexes < ActiveRecord::Migration
  def self.up
      add_index :event_records, [:event_history_id, :time_of_event]
  end

  def self.down
    remove_index :event_records, :column => [:event_history_id, :time_of_event]    
  end
end
