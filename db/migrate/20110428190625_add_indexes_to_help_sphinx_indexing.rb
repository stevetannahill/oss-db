class AddIndexesToHelpSphinxIndexing < ActiveRecord::Migration
  def self.up
    add_index :stateful_ownerships, [:stateable_type, :stateable_id]
    add_index :service_orders, [:ordered_entity_type, :ordered_entity_id], :name => "order_entity_index"
  end

  def self.down
    remove_index :stateful_ownerships, [:stateable_type, :stateable_id]
    remove_index :service_orders, "order_entity_index"
  end
end
