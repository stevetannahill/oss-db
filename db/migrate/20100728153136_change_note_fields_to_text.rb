class ChangeNoteFieldsToText < ActiveRecord::Migration
  def self.up
    change_column :demarcs, :notes, :text
    change_column :ovc_news, :notes, :text
    change_column :service_orders, :notes, :text
  end

  def self.down
    change_column :demarcs, :notes, :string
    change_column :ovc_news, :notes, :string
    change_column :service_orders, :notes, :string
  end
end
