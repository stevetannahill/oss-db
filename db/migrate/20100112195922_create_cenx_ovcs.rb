class CreateCenxOvcs < ActiveRecord::Migration
  def self.up
    create_table :cenx_ovcs do |t|
      t.string :cenx_name
      t.string :buyer_name
      t.string :seller_name
      t.string :on_switch_name
      t.string :status
      t.string :description
      t.integer :evc_id

      t.timestamps
    end
  end

  def self.down
    drop_table :cenx_ovcs
  end
end
