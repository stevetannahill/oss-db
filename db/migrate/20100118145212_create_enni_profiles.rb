class CreateEnniProfiles < ActiveRecord::Migration
  def self.up
    create_table :enni_profiles do |t|
      t.string :name
      t.string :fiber_type
      t.string :phy_type
      t.float :tx_power
      t.float :rx_power
      t.string :threasholds
      t.string :ether_type
      t.boolean :consistent_ethertype
      t.boolean :outer_tag_ovc_mapping
      t.string :connected_device_type
      t.boolean :lag_supported
      t.string :lag_type
      t.boolean :lacp_supported
      t.boolean :lacp_priority_support
      t.integer :mtu
      t.integer :max_num_ovcs
      t.boolean :ah_supported
      t.boolean :ag_supported
      t.string :lldp
      t.string :stp
      t.string :rstp
      t.string :mstp
      t.string :evst
      t.string :rpvst
      t.string :eaps
      t.string :pause
      t.string :lacp
      t.string :garp
      t.string :port_auth
      t.integer :operator_network_id
      t.string :notes

      t.timestamps
    end
  end

  def self.down
    drop_table :enni_profiles
  end
end
