class UpdateCenxLogoServiceProviders < ActiveRecord::Migration
  def up
    begin
      cenx = ServiceProvider.find(1)
      cenx.logo_url = 'ef75b97ed5aebaa255ce4f1bc87a480f' if cenx.respond_to?(:logo_url) && cenx.logo_url.blank?
      cenx.save
    rescue
    end
  end

  def down
    begin
      cenx = ServiceProvider.find(1)
      cenx.logo_url = nil if cenx.respond_to?(:logo_url)
      cenx.save
    rescue
    end
  end
end
