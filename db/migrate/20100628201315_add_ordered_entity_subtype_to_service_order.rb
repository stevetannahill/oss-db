class AddOrderedEntitySubtypeToServiceOrder < ActiveRecord::Migration
  def self.up
    add_column :service_orders, :ordered_entity_subtype, :string
  end

  def self.down
    remove_column :service_orders, :ordered_entity_subtype
  end
end
