class FixEvcDisplaysIndex < ActiveRecord::Migration
  def change
    remove_index "evc_displays", :name => "row_column_constraint"
    add_index "evc_displays", ["evc_id", "service_provider_id", "column", "row", "relative_position"], :name => "row_column_constraint", :unique => true
  end
end
