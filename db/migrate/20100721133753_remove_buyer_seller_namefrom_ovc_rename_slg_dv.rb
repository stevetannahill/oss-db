class RemoveBuyerSellerNamefromOvcRenameSlgDv < ActiveRecord::Migration
  def self.up
     rename_column :service_level_guarantee_types, :delay_variation_ms, :delay_variation_us
     remove_column :ovc_news, :buyer_name
     remove_column :ovc_news, :seller_name
  end

  def self.down
    rename_column :service_level_guarantee_types, :delay_variation_us, :delay_variation_ms
    add_column :ovc_news, :buyer_name, :string
    add_column :ovc_news, :seller_name, :string
  end
end
