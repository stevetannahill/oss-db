class RemovePeriodLength < ActiveRecord::Migration
  def up
    remove_column :metrics_cos_test_vectors, :period_length_secs
  end

  def down
    add_column :metrics_cos_test_vectors, :period_length_secs, :integer
  end
end
