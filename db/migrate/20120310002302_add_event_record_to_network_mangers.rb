
class AddEventRecordToNetworkMangers < ActiveRecord::Migration
  DEFAULT_ALARM_MAPPING = {"cleared"     => AlarmSeverity::OK,
          "warning"     => AlarmSeverity::WARNING,
          "failed"      => AlarmSeverity::FAILED,
          "unavailable" => AlarmSeverity::UNAVAILABLE}

  class EventOwnership < ActiveRecord::Base
    set_table_name 'event_ownerships'
    belongs_to :eventable, :polymorphic => true, :class_name => "event_ownership"
    belongs_to :event_history, :class_name => "system_alarm_history"
  end

  class SystemAlarmHistory < ActiveRecord::Base
    set_table_name 'event_histories'
    set_inheritance_column :_type_disabled
    has_many :alarm_records, :foreign_key => :event_history_id, :dependent => :destroy, :order => "time_of_event", :class_name => "AlarmRecord"
  end

  class AlarmRecord < ActiveRecord::Base
    set_table_name 'event_records'
    set_inheritance_column :_type_disabled
  end

  class NetworkManager < ActiveRecord::Base
    set_table_name 'network_managers'
    belongs_to :event_record, :dependent => :destroy, :class_name => "AlarmRecord"
    has_many :alarm_histories, :through => :event_ownerships, :source => :event_history, :dependent => :destroy, :class_name => "system_alarm_history"
    has_many :event_ownerships, :as => :eventable, :dependent => :destroy,  :class_name => "EventOwnership"
  end
  
  def up
    add_column :network_managers, :event_record_id, 'integer'
    
    NetworkManager.reset_column_information
    SystemAlarmHistory.reset_column_information
    EventOwnership.reset_column_information
    AlarmRecord.reset_column_information
    
    NetworkManager.all.each do |nm|
      ah = SystemAlarmHistory.create(:event_filter => "#{nm.name}-#{nm.nm_type}", :alarm_mapping => DEFAULT_ALARM_MAPPING, :type => "SystemAlarmHistory", :retention_time => 90)
      er = AlarmRecord.create(:state => "Ok", :time_of_event => Time.now.to_f*1000, :original_state => "cleared", :details => "", :event_history_id => ah.id, :type => "AlarmRecord")
      eo = EventOwnership.create(:eventable_id => nm.id, :eventable_type => "NetworkManager", :event_history_id => ah.id)      
      ah.save(:validate => false);er.save(:validate => false);eo.save(:validate => false) 
      ar = AlarmRecord.create(:state => "Ok", :time_of_event => Time.now.to_f*1000, :original_state => "cleared", :details => "", :type => "AlarmRecord")
      ar.save(:validate => false)
      nm.update_attribute(:event_record_id, ar.id)
    end
  end
  
  def down
    
    NetworkManager.reset_column_information
    SystemAlarmHistory.reset_column_information
    EventOwnership.reset_column_information
    AlarmRecord.reset_column_information
    
    NetworkManager.all.each do |nm|
      nm.event_record.destroy if nm.event_record != nil
      EventOwnership.where(:eventable_id => nm.id, :eventable_type => "NetworkManager").each do |event_owner|
        ah = SystemAlarmHistory.find_by_id(event_owner.event_history_id)
        if ah
          # delete the created history
          ah.destroy
        end        
        event_owner.destroy
      end
    end
    
    remove_column :network_managers, :event_record_id
  end
end
