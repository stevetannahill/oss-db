class AddMissingIndexes < ActiveRecord::Migration
  def self.up
#     
#     add_index :ovc_end_point_news, [:id, :type]
#     add_index :ovc_end_point_news, :ovc_end_point_type_id
#     add_index :event_ownerships, :event_history_id
#     add_index :inventory_items, :exchange_location_id
#     add_index :sm_ownerships, :sm_history_id
#     add_index :sm_ownerships, [:smable_id, :smable_type]
#     add_index :cos_instances, [:id, :type]
#     add_index :cenx_ovc_end_point_ennis_qos_policies, [:cenx_ovc_end_point_enni_id, :qos_policy_id], :name => "cenx_ovc_end_point_enni_id_qos_policy_id"
#     add_index :order_types, :operator_network_type_id
#     add_index :oss_infos, :operator_network_type_id
#     add_index :class_of_service_types_ethernet_service_types, [:ethernet_service_type_id, :cenx_class_of_service_type_id], :name => "ethernet_service_type_id_cenx_class_of_service_type_id"
#     add_index :class_of_service_types_ethernet_service_types, [:cenx_class_of_service_type_id, :ethernet_service_type_id], :name => "cenx_class_of_service_type_id_ethernet_service_type_id"
#     add_index :ovc_end_point_news_ovc_end_point_news, [:ovc_end_point_new_id, :self_id]
#     add_index :ovc_end_point_news_ovc_end_point_news, [:self_id, :ovc_end_point_new_id]
#     add_index :ovc_news, [:id, :type]
#     add_index :ovc_news, :evc_network_id
#     add_index :ovc_news, :ethernet_service_type_id
#     add_index :ovc_news, :emergency_contact_id
#     add_index :ovc_news, :ovc_type_id
#     add_index :evcs_ovc_news, [:evc_id, :ovc_new_id]
#     add_index :demarcs, :demarc_type_id
#     add_index :demarcs, :emergency_contact_id
#     add_index :demarcs, :on_behalf_of_service_provider_id
#     add_index :demarcs, :operator_network_id
#     add_index :demarcs, [:id, :type]
#     add_index :demarcs, :exchange_id
#     add_index :demarc_types, :operator_network_type_id
#     add_index :demarc_types, [:id, :type]
#     add_index :member_attr_controls, :member_attr_id
#     add_index :ovc_types, :operator_network_type_id
#     add_index :uploaded_files, [:uploader_id, :uploader_type]
#     add_index :stateful_ownerships, :stateful_id
#     add_index :evcs, :evc_type_id
#     add_index :network_managers, :data_archive_id
#     add_index :states, [:id, :type]
#     add_index :event_histories, [:id, :type]
#     add_index :operator_networks, [:id, :type]
#     add_index :operator_networks, :operator_network_type_id
#     add_index :operator_network_types, :service_provider_id
#     add_index :ovc_end_point_types, :operator_network_type_id
#     add_index :ovc_end_point_types, :class_of_service_type_id
#     add_index :sla_exceptions, [:id, :type]
#     add_index :sla_exceptions, :cos_end_point_id
#     add_index :sla_exceptions, :enni_new_id
#     add_index :service_orders, [:id, :type]
#     add_index :service_orders, :ordered_operator_network_id
#     add_index :service_orders, :operator_network_id
#     add_index :service_orders, [:ordered_object_type_id, :ordered_object_type_type]
#     add_index :service_orders, :evc_id
#     add_index :service_orders, :ordered_entity_group_id
#     add_index :demarc_types_ethernet_service_types, [:ethernet_service_type_id, :enni_type_id]
#     add_index :demarc_types_ethernet_service_types, [:enni_type_id, :ethernet_service_type_id]
#     add_index :demarc_types_ethernet_service_types, [:ethernet_service_type_id, :uni_type_id]
#     add_index :demarc_types_ethernet_service_types, [:uni_type_id, :ethernet_service_type_id]
#     add_index :user_service_providers, :user_id
#     add_index :user_service_providers, :service_provider_id
#     add_index :cos_test_vectors, [:id, :type]
#     add_index :cos_test_vectors, :service_level_guarantee_type_id
#     add_index :cos_test_vectors, :cos_end_point_id
#     add_index :cabinets, :exchange_location_id
#     add_index :class_of_service_types, [:id, :type]
#     add_index :class_of_service_types, :operator_network_type_id
#     add_index :ethernet_service_types, :operator_network_type_id
#     add_index :exchanges, :cenx_operator_network_id
#     add_index :network_management_servers, :network_manager_id
#     add_index :ports, :demarc_id
#     add_index :ports, :node_id
#     add_index :ports, :connected_port_id
#     add_index :contacts, :service_provider_id
#     add_index :qos_policies, [:id, :type]
#     add_index :qos_policies, :exchange_id
#     add_index :exchange_locations, :exchange_id
#     add_index :nodes, :exchange_location_id
#     add_index :nodes, :cabinet_id
#     add_index :nodes, :network_manager_id
#     add_index :order_annotations, :service_order_id
#     add_index :ordered_entity_type_links, [:ordered_entity_type_id, :ordered_entity_type_type]
#     add_index :ordered_entity_type_links, :ordered_entity_group_id
#     add_index :statefuls, [:id, :type]
#     add_index :event_records, [:id, :type]
#     add_index :event_records, :event_history_id
#     add_index :cos_end_points, [:id, :type]
#     add_index :cos_end_points, :cos_instance_id
#     add_index :member_attrs, [:affected_entity_id, :affected_entity_type]
#     add_index :member_attrs, [:id, :type]
#     add_index :mtc_ownerships, :mtc_history_id
#     add_index :service_level_guarantee_types, :class_of_service_type_id
#     add_index :top_talkers, [:id, :type]
#     add_index :top_talkers, :cos_end_point_id
#     add_index :top_talkers, :enni_id
#     add_index :bulk_orders_service_orders, :service_order_id
#     add_index :bulk_orders_service_orders, :bulk_order_id
#     add_index :evc_types, :operator_network_type_id
#     add_index :member_attr_instances, [:affected_entity_id, :affected_entity_type]
#     add_index :member_attr_instances, [:id, :type]
#     add_index :member_attr_instances, [:owner_id, :owner_type]
#     add_index :ordered_entity_groups, :ethernet_service_type_id
#     add_index :ordered_entity_groups, :order_type_id
#     add_index :patch_panels, :exchange_location_id
  end
  
  def self.down
#     remove_index :ovc_end_point_news, :column => [:id, :type]
#     remove_index :ovc_end_point_news, :ovc_end_point_type_id
#     remove_index :event_ownerships, :event_history_id
#     remove_index :inventory_items, :exchange_location_id
#     remove_index :sm_ownerships, :sm_history_id
#     remove_index :sm_ownerships, :column => [:smable_id, :smable_type]
#     remove_index :cos_instances, :column => [:id, :type]
#     remove_index :cenx_ovc_end_point_ennis_qos_policies, :name => "cenx_ovc_end_point_enni_id_qos_policy_id"
#     remove_index :order_types, :operator_network_type_id
#     remove_index :oss_infos, :operator_network_type_id
#     remove_index :class_of_service_types_ethernet_service_types, :name => "ethernet_service_type_id_cenx_class_of_service_type_id"
#     remove_index :class_of_service_types_ethernet_service_types, :name => "cenx_class_of_service_type_id_ethernet_service_type_id"
#     remove_index :ovc_end_point_news_ovc_end_point_news, :column => [:ovc_end_point_new_id, :self_id]
#     remove_index :ovc_end_point_news_ovc_end_point_news, :column => [:self_id, :ovc_end_point_new_id]
#     remove_index :ovc_news, :column => [:id, :type]
#     remove_index :ovc_news, :evc_network_id
#     remove_index :ovc_news, :ethernet_service_type_id
#     remove_index :ovc_news, :emergency_contact_id
#     remove_index :ovc_news, :ovc_type_id
#     remove_index :evcs_ovc_news, :column => [:evc_id, :ovc_new_id]
#     remove_index :demarcs, :demarc_type_id
#     remove_index :demarcs, :emergency_contact_id
#     remove_index :demarcs, :on_behalf_of_service_provider_id
#     remove_index :demarcs, :operator_network_id
#     remove_index :demarcs, :column => [:id, :type]
#     remove_index :demarcs, :exchange_id
#     remove_index :demarc_types, :operator_network_type_id
#     remove_index :demarc_types, :column => [:id, :type]
#     remove_index :member_attr_controls, :member_attr_id
#     remove_index :ovc_types, :operator_network_type_id
#     remove_index :uploaded_files, :column => [:uploader_id, :uploader_type]
#     remove_index :stateful_ownerships, :stateful_id
#     remove_index :evcs, :evc_type_id
#     remove_index :network_managers, :data_archive_id
#     remove_index :states, :column => [:id, :type]
#     remove_index :event_histories, :column => [:id, :type]
#     remove_index :operator_networks, :column => [:id, :type]
#     remove_index :operator_networks, :operator_network_type_id
#     remove_index :operator_network_types, :service_provider_id
#     remove_index :ovc_end_point_types, :operator_network_type_id
#     remove_index :ovc_end_point_types, :class_of_service_type_id
#     remove_index :sla_exceptions, :column => [:id, :type]
#     remove_index :sla_exceptions, :cos_end_point_id
#     remove_index :sla_exceptions, :enni_new_id
#     remove_index :service_orders, :column => [:id, :type]
#     remove_index :service_orders, :ordered_operator_network_id
#     remove_index :service_orders, :operator_network_id
#     remove_index :service_orders, :column => [:ordered_object_type_id, :ordered_object_type_type]
#     remove_index :service_orders, :evc_id
#     remove_index :service_orders, :ordered_entity_group_id
#     remove_index :demarc_types_ethernet_service_types, :column => [:ethernet_service_type_id, :enni_type_id]
#     remove_index :demarc_types_ethernet_service_types, :column => [:enni_type_id, :ethernet_service_type_id]
#     remove_index :demarc_types_ethernet_service_types, :column => [:ethernet_service_type_id, :uni_type_id]
#     remove_index :demarc_types_ethernet_service_types, :column => [:uni_type_id, :ethernet_service_type_id]
#     remove_index :user_service_providers, :user_id
#     remove_index :user_service_providers, :service_provider_id
#     remove_index :cos_test_vectors, :column => [:id, :type]
#     remove_index :cos_test_vectors, :service_level_guarantee_type_id
#     remove_index :cos_test_vectors, :cos_end_point_id
#     remove_index :cabinets, :exchange_location_id
#     remove_index :class_of_service_types, :column => [:id, :type]
#     remove_index :class_of_service_types, :operator_network_type_id
#     remove_index :ethernet_service_types, :operator_network_type_id
#     remove_index :exchanges, :cenx_operator_network_id
#     remove_index :network_management_servers, :network_manager_id
#     remove_index :ports, :demarc_id
#     remove_index :ports, :node_id
#     remove_index :ports, :connected_port_id
#     remove_index :contacts, :service_provider_id
#     remove_index :qos_policies, :column => [:id, :type]
#     remove_index :qos_policies, :exchange_id
#     remove_index :exchange_locations, :exchange_id
#     remove_index :nodes, :exchange_location_id
#     remove_index :nodes, :cabinet_id
#     remove_index :nodes, :network_manager_id
#     remove_index :order_annotations, :service_order_id
#     remove_index :ordered_entity_type_links, :column => [:ordered_entity_type_id, :ordered_entity_type_type]
#     remove_index :ordered_entity_type_links, :ordered_entity_group_id
#     remove_index :statefuls, :column => [:id, :type]
#     remove_index :event_records, :column => [:id, :type]
#     remove_index :event_records, :event_history_id
#     remove_index :cos_end_points, :column => [:id, :type]
#     remove_index :cos_end_points, :cos_instance_id
#     remove_index :member_attrs, :column => [:affected_entity_id, :affected_entity_type]
#     remove_index :member_attrs, :column => [:id, :type]
#     remove_index :mtc_ownerships, :mtc_history_id
#     remove_index :service_level_guarantee_types, :class_of_service_type_id
#     remove_index :top_talkers, :column => [:id, :type]
#     remove_index :top_talkers, :cos_end_point_id
#     remove_index :top_talkers, :enni_id
#     remove_index :bulk_orders_service_orders, :service_order_id
#     remove_index :bulk_orders_service_orders, :bulk_order_id
#     remove_index :evc_types, :operator_network_type_id
#     remove_index :member_attr_instances, :column => [:affected_entity_id, :affected_entity_type]
#     remove_index :member_attr_instances, :column => [:id, :type]
#     remove_index :member_attr_instances, :column => [:owner_id, :owner_type]
#     remove_index :ordered_entity_groups, :ethernet_service_type_id
#     remove_index :ordered_entity_groups, :order_type_id
#     remove_index :patch_panels, :exchange_location_id
  end
end