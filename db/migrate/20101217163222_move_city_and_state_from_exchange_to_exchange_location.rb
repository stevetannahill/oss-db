class MoveCityAndStateFromExchangeToExchangeLocation < ActiveRecord::Migration

  class Exchange < ActiveRecord::Base
  end

  def self.up
    add_column :exchange_locations, :city, :string
    add_column :exchange_locations, :state_province, :string
    
    Exchange.reset_column_information
    Exchange.all.each{|ex|
      execute "UPDATE exchange_locations SET city = '#{ex.city}', state_province = '#{ex.state_province}' WHERE exchange_id = #{ex.id}"
    }
    
    remove_column :exchanges, :city
    remove_column :exchanges, :state_province
  end

  def self.down
    add_column :exchanges, :city, :string
    add_column :exchanges, :state_province, :string
    
    Exchange.reset_column_information
    Exchange.all.each{|ex|
      results = execute "SELECT city, state_province FROM exchange_locations WHERE exchange_id = #{ex.id}"
      if results.num_rows() > 0
        hash = results.fetch_hash
        execute "UPDATE exchanges SET city = '#{hash['city']}', state_province = '#{hash['state_province']}' WHERE id = #{ex.id}"
      end
      results.free()
    }
    
    remove_column :exchange_locations, :city
    remove_column :exchange_locations, :state_province
  end
end
