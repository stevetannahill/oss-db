class CreateMemberAttrControls < ActiveRecord::Migration
  def self.up
    create_table :member_attr_controls do |t|
      t.integer :member_attr_id
      t.string :description
      t.integer :execution_order
      t.string :action
      t.text :code
      t.string :info
      t.text :notes

      t.timestamps
    end
  end

  def self.down
    drop_table :member_attr_controls
  end
end
