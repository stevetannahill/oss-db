class MoreForEnniSpCos < ActiveRecord::Migration
  def self.up
    rename_column :ennis, :name, :cenx_name
    add_column :ennis, :sp_name, :string
    add_column :service_providers, :logo_url, :string
    add_column :nodes, :secondary_ip, :string
    add_column :nodes, :net_mask, :string
    add_column :nodes, :gateway_ip, :string
    add_column :nodes, :rack_number, :string
    rename_column :nodes, :ip, :primary_ip 
  end

  def self.down  
    rename_column :nodes, :primary_ip, :ip
    remove_column :nodes, :rack_number
    remove_column :nodes, :gateway_ip    
    remove_column :nodes, :net_mask
    remove_column :nodes, :secondary_ip
    remove_column :service_providers, :logo_url
    remove_column :ennis, :sp_name
    rename_column :ennis, :cenx_name, :name
  end
  
end
