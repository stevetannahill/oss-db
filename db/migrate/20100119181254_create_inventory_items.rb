class CreateInventoryItems < ActiveRecord::Migration
  def self.up
    create_table :inventory_items do |t|
      t.string :description
      t.string :cenx_naming_std
      t.string :vendor
      t.string :cabinet_position
      t.string :make_model
      t.string :serial_number
      t.string :date
      t.integer :exchange_location_id

      t.timestamps
    end
  end

  def self.down
    drop_table :inventory_items
  end
end
