class FixOssCosNotes < ActiveRecord::Migration
  def self.up
    change_column :class_of_service_types, :notes, :text
  end

  def self.down
    change_column :class_of_service_types, :notes, :string
  end
end
