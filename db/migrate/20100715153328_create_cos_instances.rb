class CreateCosInstances < ActiveRecord::Migration
  def self.up
    create_table :cos_instances do |t|
      t.integer :class_of_service_type_id
      t.integer :ovc_new_id
      t.string :notes
      t.timestamps
    end
  end

  def self.down
    drop_table :cos_instances
  end
end