class FixesForOvcsAndEndpoints < ActiveRecord::Migration
  def self.up
    add_column :exchanges, :cenx_operator_network_id, :integer
    add_column :operator_networks, :type, :string
    add_column :ovc_news, :ethernet_service_type_id, :integer
    remove_column :ovc_news, :ovc_class
  end

  def self.down
    remove_column :exchanges, :cenx_operator_network_id
    remove_column :operator_networks, :type
    remove_column :ovc_news, :ethernet_service_type_id
    add_column :ovc_news, :ovc_class, :string
  end
end
