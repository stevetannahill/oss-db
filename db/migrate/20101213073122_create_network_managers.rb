class CreateNetworkManagers < ActiveRecord::Migration
  def self.up
    create_table :network_managers do |t|
      t.string :name
      t.string :nm_type

      t.timestamps
    end
    
    execute "INSERT INTO network_managers (id, name, nm_type) VALUES (1, '5620 SAM - Standard', '5620Sam')"
    execute "INSERT INTO network_managers (id, name, nm_type) VALUES (2, 'BrixWorx - Live', 'BrixWorx')"
    execute "INSERT INTO network_managers (id, name, nm_type) VALUES (3, '5620 SAM - LightSquared', '5620Sam')"
    
    add_column :nodes, :network_manager_id, :integer
    
    execute "UPDATE nodes SET network_manager_id = 1 WHERE node_type = '7750-sr12'"
    execute "UPDATE nodes SET network_manager_id = 2 WHERE node_type = 'BV-3000'"
  end

  def self.down
    drop_table :network_managers
    
    remove_column :nodes, :network_manager_id
  end
end
