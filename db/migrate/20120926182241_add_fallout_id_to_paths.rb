class AddFalloutIdToPaths < ActiveRecord::Migration
  def change
    add_column :paths, :fallout_exception_id, :integer
  end
end
