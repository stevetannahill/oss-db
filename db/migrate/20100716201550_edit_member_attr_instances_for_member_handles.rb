class EditMemberAttrInstancesForMemberHandles < ActiveRecord::Migration
  def self.up
    add_column :member_attrs, :allow_blank, :boolean
    add_column :member_attr_instances, :type, :string
    add_column :member_attr_instances, :owner_id, :integer
    add_column :member_attr_instances, :owner_type, :string
  end

  def self.down
    remove_column :member_attrs, :allow_blank
    remove_column :member_attr_instances, :type
    remove_column :member_attr_instances, :owner_id
    remove_column :member_attr_instances, :owner_type
  end
end
