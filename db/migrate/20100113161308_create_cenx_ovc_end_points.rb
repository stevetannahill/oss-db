class CreateCenxOvcEndPoints < ActiveRecord::Migration
  def self.up
    create_table :cenx_ovc_end_points do |t|
      t.string :on_switch_name
      t.integer :stag
      t.string :status
      t.string :description
      t.integer :cenx_ovc_id
      t.integer :ovc_id
      t.integer :enni_id

      t.timestamps
    end
  end

  def self.down
    drop_table :cenx_ovc_end_points
  end
end
