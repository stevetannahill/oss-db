class OvcNew < InstanceElement
end

class RepopulateSinEvcServiceProviderSphinxes < ActiveRecord::Migration
  
  class ServiceProvider < ActiveRecord::Base
    self.inheritance_column = :_type_disabled
  end
  
  class InstanceElement < ActiveRecord::Base
    self.abstract_class = true
  end
  
  class OperatorNetwork < InstanceElement
    self.inheritance_column = :_type_disabled
    belongs_to :service_provider
  end
  
  class Exchange < ActiveRecord::Base
    self.inheritance_column = :_type_disabled
    belongs_to :cenx_operator_network, :class_name => "OperatorNetwork"
  end
        
  class OvcNew < InstanceElement
    self.inheritance_column = :_type_disabled
    has_and_belongs_to_many :evcs
    belongs_to :operator_network
    has_one :service_provider, :through => :operator_network
    belongs_to :exchange
    
    def service_provider
      if type == "CenxOvcNew"
        on = exchange_id ? exchange.cenx_operator_network : nil
      else
        on = operator_network_id ? operator_network : nil
      end
      
      (on) ? on.service_provider : nil
    end
  end
  
  class SinEvcServiceProviderSphinx < ActiveRecord::Base
  end
  
  class Evc < InstanceElement
    self.inheritance_column = :_type_disabled
    belongs_to :operator_network
    has_one :service_provider, :through => :operator_network
  end
  
  class SinEvc < Evc
  end
  
  
  
  def self.up
    
    cenx = ServiceProvider.find_by_is_system_owner(1) if ServiceProvider.respond_to?(:find_by_is_system_owner)
    cenx = ServiceProvider.find_by_is_cenx(1) if ServiceProvider.respond_to?(:find_by_is_cenx)
    
    # Populdate access table with all existing evcs
    evcs = SinEvc.find(:all)
    evcs.each do |evc|
      SinEvcServiceProviderSphinx.find_or_create_by_evc_id_and_service_provider_id(evc.id, evc.service_provider.id)
    end

    # Populdate access table with all existing ovcs
    ovcs = OvcNew.find(:all)
    ovcs.each do |ovc|
      ovc.evcs.each do |evc|
        SinEvcServiceProviderSphinx.find_or_create_by_evc_id_and_service_provider_id(evc.id, ovc.service_provider.id)
      end
    end

  end

  def self.down
  end
end
