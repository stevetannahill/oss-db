class OvcNew < InstanceElement
end

class GiveOvcsMultipleEvcs < ActiveRecord::Migration
  def self.up
    create_table :evcs_ovc_news, :id => false do |t|
      t.column :evc_id, :integer
      t.column :ovc_new_id, :integer
    end
    add_index :evcs_ovc_news, :evc_id, :name => "index_evcs_ovc_news_on_evc_id"
    add_index :evcs_ovc_news, :ovc_new_id, :name => "index_evcs_ovc_news_ovc_new_id"
    
    add_column :ovc_news, :evc_network_id, :integer
    
    OvcNew.all.each{|ovc|
      execute "INSERT INTO evcs_ovc_news (evc_id, ovc_new_id) VALUES (#{ovc.evc_id}, #{ovc.id})"
      evc = Evc.find(ovc.evc_id)
      execute "UPDATE ovc_news SET evc_network_id = #{evc.operator_network_id} WHERE id = #{ovc.id}"
    }
    
    remove_column :ovc_news, :evc_id
  end

  def self.down
    add_column :ovc_news, :evc_id, :integer
    
    OvcNew.all.each{|ovc|
      results = execute "SELECT evc_id FROM evcs_ovc_news WHERE ovc_new_id = #{ovc.id}"
      execute "UPDATE ovc_news SET evc_id = #{results.fetch_hash["evc_id"]} WHERE id = #{ovc.id}"
      results.free()
    }
    
    remove_column :ovc_news, :evc_network_id
    
    drop_table :evcs_ovc_news
  end
end
