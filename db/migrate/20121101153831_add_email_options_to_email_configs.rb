class AddEmailOptionsToEmailConfigs < ActiveRecord::Migration
  def change
    add_column :email_configs, :enable_order_email, :boolean, :default => false
    add_column :email_configs, :enable_fault_email, :boolean, :default => false
  end
end
