class ExchangeLocationEvolveForLs < ActiveRecord::Migration
  def self.up
    
    add_column :cos_test_vectors, :availability_guarantee, :string
    add_column :exchange_locations, :postal_zip_code, :string
    add_column :exchange_locations, :emergency_contact_info, :string
    add_column :exchanges, :state_province, :string

  end

  def self.down
    remove_column :cos_test_vectors, :availability_guarantee
    remove_column :exchange_locations, :postal_zip_code
    remove_column :exchange_locations, :emergency_contact_info
    remove_column :exchanges, :state_province
  end
end
