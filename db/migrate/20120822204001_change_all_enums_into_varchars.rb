class ChangeAllEnumsIntoVarchars < ActiveRecord::Migration
  ENUM_TABLES = {
    exception_sla_cos_test_vectors: [
      [:severity,"ENUM('critical', 'major', 'minor')"],
      [:sla_type,"ENUM('availability', 'flr', 'fd', 'fdv')"]
    ],
    exception_sla_network_types: [
      [:severity, "ENUM('critical', 'major', 'minor', 'clear')"]
    ],
    exception_utilization_cos_end_points: [
      [:severity, "ENUM( 'major', 'minor')"],
      [:direction, "ENUM('ingress', 'egress')"]
    ],
    exception_utilization_ennis: [
      [:severity, "ENUM( 'major', 'minor')"],
      [:direction, "ENUM('ingress', 'egress')"]
    ],
    metrics_cos_test_vectors: [
      [:metric_type, "ENUM('availability', 'flr', 'fd', 'fdv')"],
      [:measurement_type, "ENUM('end-to-end', 'delta')"],
      [:period_type,  "ENUM('weekly', 'monthly')"],
      [:severity, "ENUM('clear', 'minor', 'major', 'critical')"]
    ],
    rollup_report_demarcs: [
      [:severity, "ENUM('clear', 'minor', 'major', 'critical')"]
    ],
    rollup_report_paths: [
      [:severity, "ENUM('clear', 'minor', 'major', 'critical')"]
    ],
    rollup_report_sitegroups: [
      [:severity, "ENUM('clear', 'minor', 'major', 'critical')"]
    ],
    sla_exceptions: [
      [:sla_type, "ENUM('availability', 'flr', 'fd', 'fdv')"],
      [:direction, "ENUM('ingress', 'egress')"],
      [:severity, "ENUM('critical', 'major', 'minor')"]
    ]
  }

  def up
    ENUM_TABLES.each do |table,columns|
      model = Class.new(ActiveRecord::Base) do
        self.table_name = table
      end

      col_and_tmp = columns.map{|c,_|[c,c.to_s+"_tmp"]}

      col_and_tmp.each do |_,tmp_column|
        add_column table, tmp_column, :string
      end

      model.all.each do |m|
        col_and_tmp.each do |column_name,tmp_column|
          m[tmp_column] = m[column_name]
        end
        m.save!
      end

      col_and_tmp.each do |column_name,tmp_column|
        remove_column table, column_name
        rename_column table, tmp_column, column_name
      end
    end
  end

  def down
    ENUM_TABLES.each do |table,columns|
      model = Class.new(ActiveRecord::Base) do
        self.table_name = table
      end

      col_tmp_enum = columns.map{|c,enum|[c,c.to_s+"_tmp",enum]}

      col_tmp_enum.each do |_,tmp_column,enum|
        add_column table, tmp_column, enum
      end

      model.all.each do |m|
        col_tmp_enum.each do |column_name,tmp_column,_|
          m[tmp_column] = m[column_name]
        end
        m.save!
      end

      col_tmp_enum.each do |column_name,tmp_column,_|
        remove_column table, column_name
        rename_column table, tmp_column, column_name
      end
    end
  end
end
