class AddUrlToNetworkManager < ActiveRecord::Migration
  def self.up
    add_column :network_managers, :url, :string

    NetworkManager.all.each do |nm|
      if nm.name =~ /SAM/
        if nm.name =~ /Live/
          nm.url = "http://10.1.8.135:8085/"
        elsif nm.name =~ /Lab/
          nm.url = "http://10.1.8.138:8085/"
        end
      elsif nm.name =~ /BrixWorx/
        if nm.name =~ /Live/
          nm.url = "http://10.1.8.137:8080/"
        elsif nm.name =~ /Lab/
          nm.url = "http://172.16.24.8:8080/"
        end
      end
      nm.save(:validate => false)
    end
  end

  def self.down
    remove_column :network_managers, :url
  end
end
