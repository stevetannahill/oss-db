class RemoveIdsFromMetricsTable < ActiveRecord::Migration
  def up
    rename_column :exception_sla_cos_test_vectors, :cos_test_vector_id, :grid_circuit_id
    remove_column :cos_test_vectors, :ref_grid_circuit_id
    remove_column :metrics_cos_test_vectors, :pop_id
    remove_column :metrics_cos_test_vectors, :demarc_id
    remove_column :metrics_cos_test_vectors, :protection_path_id
    remove_column :metrics_cos_test_vectors, :cos_test_vector_id
    add_column :metrics_cos_test_vectors, :grid_circuit_id, :integer

    add_index(:metrics_cos_test_vectors, [:grid_circuit_id, :period_start_epoch], :unique => true, :name => :metrics_cos_test_vector_search)
    add_index(:cos_test_vectors, :grid_circuit_id, :name => :cos_test_vector_by_grid_circuit_id)
    add_index(:cos_test_vectors, :circuit_id, :name => :cos_test_vector_by_circuit_id)
  end

  def down
    rename_column :exception_sla_cos_test_vectors, :grid_circuit_id, :cos_test_vector_id
    add_column :cos_test_vectors, :ref_grid_circuit_id, :integer
    add_column :metrics_cos_test_vectors, :pop_id, :integer
    add_column :metrics_cos_test_vectors, :demarc_id, :integer
    add_column :metrics_cos_test_vectors, :protection_path_id, :integer
    add_column :metrics_cos_test_vectors, :cos_test_vector_id, :integer
    remove_column :metrics_cos_test_vectors, :grid_circuit_id

    remove_index(:metrics_cos_test_vectors, :name =>:metrics_cos_test_vector_search)
    remove_index(:cos_test_vectors, :name =>:cos_test_vector_by_grid_circuit_id)
    remove_index(:cos_test_vectors, :name =>:cos_test_vector_by_circuit_id)
  end
end
