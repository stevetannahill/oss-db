class OvcTypeEvolve < ActiveRecord::Migration
  def self.up
    add_column :ovc_types, :redundancy_mechanism, :string
    add_column :demarc_types, :turnup_reflection_mechanisms, :string
    add_column :demarc_types, :service_rate_reflection_supported, :boolean
    add_column :demarc_types, :max_reflection_rate, :string
  end

  def self.down
    remove_column :ovc_types, :redundancy_mechanism
    remove_column :demarc_types, :turnup_reflection_mechanisms
    remove_column :demarc_types, :service_rate_reflection_supported
    remove_column :demarc_types, :max_reflection_rate
  end
end
