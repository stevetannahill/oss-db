#This is required to expose access to the 'type' column of the database
# class Object
#   undef_method :type
# end

class AddCenxClassOfServiceType < ActiveRecord::Migration
  def self.up
    add_column :class_of_service_types, :type, :string
    add_column :class_of_service_types, :short_name, :string
    add_column :class_of_service_types, :fwding_class, :string

    #Convert all existing CENX CoS Types to the new sub class
    cenx_provider = ServiceProvider.find_by_is_system_owner(true) if ServiceProvider.respond_to?(:find_by_is_system_owner)
    cenx_provider = ServiceProvider.find_by_is_cenx(true) if ServiceProvider.respond_to?(:find_by_is_cenx)
    return if cenx_provider.nil?
    ons = cenx_provider.operator_networks

    ons.each do |on|
      return unless on.respond_to?(:class_of_service_types)
      on.class_of_service_types.each do |cos|
        cos.type = "CenxClassOfServiceType"
        cos.save false
      end
    end
    
  end

  def self.down
    remove_column :class_of_service_types, :type
    remove_column :class_of_service_types, :short_name
    remove_column :class_of_service_types, :fwding_class
  end
end
