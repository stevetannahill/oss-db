class AddClearExceptionsToScheduledReports < ActiveRecord::Migration
  def change
    add_column :reports, :clear_exceptions, :integer
  end
end
