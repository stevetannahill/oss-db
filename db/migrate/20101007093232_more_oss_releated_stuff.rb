class MoreOssReleatedStuff < ActiveRecord::Migration
  def self.up
    add_column :ovc_end_point_types, :class_of_service_type_id, :integer
  end

  def self.down
    remove_column :ovc_end_point_types, :class_of_service_type_id
  end
end
