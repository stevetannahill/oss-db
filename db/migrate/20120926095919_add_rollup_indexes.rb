class AddRollupIndexes < ActiveRecord::Migration
  def up
    add_index :rollup_ids, :cos_test_vector_id
    add_index :rollup_ids, :grid_circuit_id
    add_index :rollup_ids, :demarc_id
    add_index :rollup_ids, :protection_path_id
    add_index :rollup_ids, :site_id
    add_index :rollup_report_demarcs, :site_id
    add_index :rollup_report_demarcs, :demarc_id
    add_index :rollup_report_paths, :site_id
    add_index :rollup_report_paths, :demarc_id 
    add_index :rollup_report_paths, :protection_path_id
    add_index :rollup_report_site_groups, :site_group_id
    add_index :rollup_report_sites, :site_id
  end

  def down
    remove_index :rollup_ids, :cos_test_vector_id
    remove_index :rollup_ids, :grid_circuit_id
    remove_index :rollup_ids, :demarc_id
    remove_index :rollup_ids, :protection_path_id
    remove_index :rollup_ids, :site_id
    remove_index :rollup_report_demarcs, :site_id
    remove_index :rollup_report_demarcs, :demarc_id
    remove_index :rollup_report_paths, :site_id
    remove_index :rollup_report_paths, :demarc_id
    remove_index :rollup_report_paths, :protection_path_id
    remove_index :rollup_report_site_groups, :site_group_id
    remove_index :rollup_report_sites, :site_id
  end
end
