class AddMetricsCosTestVectors < ActiveRecord::Migration
  def up
    create_table :metrics_cos_test_vectors do |t|
      t.integer :pop_id
      t.integer :demarc_id
      t.integer :protection_path_id, :default => 1 # one or more ids to represent 1:N protection paths. Only unique per demarc.
      t.integer :cos_test_vector_id
      t.integer :period_start_epoch, :limit => 8  # 64bits epoch seconds
      t.integer :period_length_secs  # seconds, not all months are created equal, same for some weeks
      t.integer :time_declared_epoch, :limit => 8  # 64bits epoch seconds. time at which an issue was detected for the first time.
      t.column  :measurement_type, "ENUM('end-to-end', 'delta')" # for circuits with a ref circuit, we can choose between end-to-end or delta
      t.column  :period_type,  "ENUM('weekly', 'monthly')" # montly = calendar month UTC,  weekly = http://en.wikipedia.org/wiki/ISO_week_date UTC
      t.column  :severity, "ENUM('clear', 'minor', 'major', 'critical')" # critical doesn't apply for period_type<>monthly as the SLA are monthly.
      t.column  :metric_type, "ENUM('avail', 'flr', 'fd', 'fdv')" # equivalent to sla_type in exception table
      t.float   :metric
      t.integer :minor_time_over
      t.integer :major_time_over
    end
    create_table :rollup_report_paths do |t|
      t.integer :period_start_epoch
      t.integer :pop_id
      t.integer :demarc_id
      t.integer :protection_path_id # one or more ids to represent N protection paths. Only unique per demarc.
      t.column  :severity, "ENUM('clear', 'minor', 'major', 'critical')"  # high watermark of underlying CosTestVector severities.
      t.integer :metric_count
      t.integer :metric_minor_count
      t.integer :metric_major_count
    end
    create_table :rollup_report_demarcs do |t|
      t.integer :period_start_epoch
      t.integer :pop_id
      t.integer :demarc_id
      t.column  :severity, "ENUM('clear', 'minor', 'major', 'critical')" # MINOR=error on one path, MAJOR=errors on all paths.
      t.integer :metric_count
      t.integer :metric_minor_count
      t.integer :metric_major_count
    end
    create_table :rollup_report_sitegroups do |t|
      t.integer :period_start_epoch
      t.integer :pop_id
      t.column  :severity, "ENUM('clear', 'minor', 'major', 'critical')"  # high watermark of underlying Demarc severities.
      t.integer :metric_count
      t.integer :metric_minor_count
      t.integer :metric_major_count
    end

    add_column :cos_test_vectors, :grid_circuit_id, :integer
    add_column :cos_test_vectors, :ref_grid_circuit_id, :integer
    add_column :cos_test_vectors, :protection_path_id, :integer
    CosTestVector.reset_column_information
    SpirentCosTestVector.find_each do |ctv|
      ctv.update_attribute(:protection_path_id, ctv.primary ? 1 : 2) unless ctv.protection_path_id
    end
  end

  def down
    drop_table :metrics_cos_test_vectors
    drop_table :rollup_report_paths
    drop_table :rollup_report_demarcs
    drop_table :rollup_report_sitegroups
    remove_column :cos_test_vectors, :grid_circuit_id
    remove_column :cos_test_vectors, :ref_grid_circuit_id
    remove_column :cos_test_vectors, :protection_path_id

  end
end

