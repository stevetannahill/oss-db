class CreateAlarmHistory < ActiveRecord::Migration
  def self.up
    create_table "event_histories", :force => true do |t|
      t.string   "event_filter"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   :type
    end

    create_table "event_records", :force => true do |t|
      t.string   "time_of_event"
      t.string   "state"
      t.string   "details"
      t.integer  "event_history_id"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   :type
    
    end
  
    create_table "event_ownerships", :force => true do |t|
      t.references "eventable", :polymorphic => true
      t.references "event_history"       
    end

  end

  def self.down
    drop_table "event_histories"
    drop_table "event_records"
    drop_table "event_ownerships"
  end
end
