class DataArchive < ActiveRecord::Base
  has_many :network_manager
end

class UpdateAdapterInDataArchive < ActiveRecord::Migration
  def up
    DataArchive.where(:adapter => "mysql").each do |data_archive|
      data_archive.adapter = "mysql2"
      data_archive.save
    end
  end

  def down
    DataArchive.where(:adapter => "mysql2").each do |data_archive|
      data_archive.adapter = "mysql"
      data_archive.save
    end
  end
end
