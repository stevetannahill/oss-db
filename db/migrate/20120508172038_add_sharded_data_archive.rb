class AddShardedDataArchive < ActiveRecord::Migration
  def up
    add_column :data_archives, :number_of_ctvs_per_shard, :integer, :default => 0
  end

  def down
    remove_column :data_archives, :number_of_ctvs_per_shard
  end
end
