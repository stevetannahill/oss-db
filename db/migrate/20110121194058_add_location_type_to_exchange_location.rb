class AddLocationTypeToExchangeLocation < ActiveRecord::Migration

  class Exchange < ActiveRecord::Base
  end


  def self.up
    add_column :exchange_locations, :location_type, :string
    
    Exchange.reset_column_information
    Exchange.find(:all).each do |ex|
      ex.exchange_locations.each_with_index do |exl, index|
        if ex.exchange_type == "Standard Exchange"
          location_types = ["Primary", "Secondary"]
        else
          location_types = exl.exchange.location_types
        end
        exl.location_type = location_types[index]
        #Force save because we know what changed is valid
        exl.save false
      end
    end
  end

  def self.down
    remove_column :exchange_locations, :location_type
  end
end
