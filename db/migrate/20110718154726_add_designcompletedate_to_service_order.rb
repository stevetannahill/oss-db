class AddDesigncompletedateToServiceOrder < ActiveRecord::Migration
  def self.up
    add_column :service_orders, :design_complete_date, :date
  end

  def self.down
    remove_column :service_orders, :design_complete_date
  end
end
