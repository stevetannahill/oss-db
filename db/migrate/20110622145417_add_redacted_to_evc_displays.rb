class AddRedactedToEvcDisplays < ActiveRecord::Migration
  def self.up
    add_column :evc_displays, :redacted, :boolean
  end

  def self.down
    remove_column :evc_displays, :redacted
  end
end