class CreateCosEndPoints < ActiveRecord::Migration
  def self.up
    create_table :cos_end_points do |t|
      t.integer :ovc_end_point_new_id
      t.integer :cos_instance_id
      t.integer :bw_profile_type_id
      t.string :direction
      t.string :cos_id_value
      t.string :cir_kbps
      t.string :eir_kbps
      t.string :cbs_kB
      t.string :ebs_kB
      t.timestamps
    end
  end

  def self.down
    drop_table :cos_end_points
  end
end

