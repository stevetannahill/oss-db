class CleanupMonthlyMetricsClear < ActiveRecord::Migration
  def up
    sql = <<EOF
select count(*) from metrics_monthly_cos_test_vectors where severity = 'clear';
EOF
    ActiveRecord::Base.connection.execute(sql)
  end

  def down
  end
end
