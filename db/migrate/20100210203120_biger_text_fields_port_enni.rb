class BigerTextFieldsPortEnni < ActiveRecord::Migration
  def self.up
    change_column :ennis, :notes, :text
    change_column :ports, :notes, :text
  end

  def self.down
    change_column :ennis, :notes, :string
    change_column :ports, :notes, :string
  end
end
