class AddCenxOvcType < ActiveRecord::Migration
  def self.up
    add_column :cenx_ovcs, :class_of_service, :string
  end

  def self.down
    remove_column :cenx_ovcs, :class_of_service
  end
end
