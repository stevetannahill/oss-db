class CreateOrderedEntityTypeLinks < ActiveRecord::Migration
  def self.up
    create_table :ordered_entity_type_links do |t|
      t.integer :ordered_entity_group_id
      t.integer :ordered_entity_type_id
      t.integer :ordered_entity_type_type

      t.timestamps
    end
  end

  def self.down
    drop_table :ordered_entity_type_links
  end
end
