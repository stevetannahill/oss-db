class CreateUniProfiles < ActiveRecord::Migration
  def self.up
    create_table :uni_profiles do |t|
      t.string :name
      t.string :phy_type
      t.boolean :auto_negotiate
      t.integer :mtu
      t.integer :max_num_ovcs
      t.boolean :bundling
      t.boolean :ato_bundling
      t.string :nid_type
      t.boolean :cfm_supported
      t.string :lldp
      t.string :stp
      t.string :rstp
      t.string :mstp
      t.string :evst
      t.string :rpvst
      t.string :eaps
      t.string :pause
      t.string :lacp
      t.string :garp
      t.string :port_auth
      t.integer :service_type_id

      t.timestamps
    end
  end

  def self.down
    drop_table :uni_profiles
  end
end
