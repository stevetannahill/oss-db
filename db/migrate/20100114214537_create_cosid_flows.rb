class CreateCosidFlows < ActiveRecord::Migration
  def self.up
    create_table :cosid_flows do |t|
      t.integer :cir_kbps
      t.integer :eir_kbps
      t.integer :cenx_ovc_end_point_id
      t.integer :class_of_service_id

      t.timestamps
    end
  end

  def self.down
    drop_table :cosid_flows
  end
end
