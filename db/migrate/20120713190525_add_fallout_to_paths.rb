class AddFalloutToPaths < ActiveRecord::Migration
  def change
  	add_column :paths, :fallout, :boolean, :default => false
  end
end
