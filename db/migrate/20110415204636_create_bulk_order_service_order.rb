class CreateBulkOrderServiceOrder < ActiveRecord::Migration
  def self.up
    create_table :bulk_orders_service_orders, :force => true do |t|
      t.integer :service_order_id
      t.integer :bulk_order_id
    end
  end

  def self.down
    drop_table :bulk_orders_service_orders
  end
end

