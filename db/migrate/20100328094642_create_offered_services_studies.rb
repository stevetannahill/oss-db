class CreateOfferedServicesStudies < ActiveRecord::Migration
  def self.up
    create_table :offered_services_studies do |t|
      t.string :title
      t.date :completion_date
      t.string :reference_documents
      t.integer :cenx_contact_id
      t.integer :member_contact_id
      t.integer :operator_network_id

      t.timestamps
    end
  end

  def self.down
    drop_table :offered_services_studies
  end
end
