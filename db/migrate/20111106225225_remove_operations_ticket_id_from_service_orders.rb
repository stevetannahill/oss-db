class RemoveOperationsTicketIdFromServiceOrders < ActiveRecord::Migration
  def self.up
    remove_column :service_orders, :operations_ticket_id 
  end

  def self.down
    add_column :service_orders, :operations_ticket_id, :string
  end
end
