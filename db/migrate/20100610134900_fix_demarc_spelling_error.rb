class FixDemarcSpellingError < ActiveRecord::Migration
  def self.up
    rename_column :demarcs, :demark_type, :demarc_type
  end

  def self.down
    rename_column :demarcs, :demarc_type, :demark_type
  end
end
