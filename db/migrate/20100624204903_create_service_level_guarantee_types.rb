class CreateServiceLevelGuaranteeTypes < ActiveRecord::Migration
  def self.up
    create_table :service_level_guarantee_types do |t|
      t.integer :class_of_service_type_id
      t.string :min_dist_km
      t.string :max_dist_km
      t.string :delay_ms
      t.string :delay_variation_ms
      t.boolean :is_unbounded
      t.string :notes
      
      t.timestamps
    end
  end

  def self.down
    drop_table :service_level_guarantee_types
  end
end
