class AddDeltaToSinEvcServiceProviderSphinx < ActiveRecord::Migration
  def self.up
    add_column :sin_evc_service_provider_sphinxes, :delta, :boolean, :default => true, :null => false
  end

  def self.down
    remove_column :sin_evc_service_provider_sphinxes, :delta
  end
end
