class AddCircuitIdToCosTestVector < ActiveRecord::Migration
  def change
    add_column :cos_test_vectors, :circuit_id, :string
  end
end
