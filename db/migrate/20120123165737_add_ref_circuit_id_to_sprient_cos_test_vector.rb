class AddRefCircuitIdToSprientCosTestVector < ActiveRecord::Migration
  def change
    add_column :cos_test_vectors, :ref_circuit_id, :string
  end
end
