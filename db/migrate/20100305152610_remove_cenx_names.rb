class RemoveCenxNames < ActiveRecord::Migration
  def self.up
    remove_column :ennis, :cenx_name
    remove_column :evcs, :cenx_name
    remove_column :cenx_ovcs, :cenx_name
    remove_column :ovcs, :cenx_name
    add_column :evcs, :description, :string
    add_column :ovcs, :primary_contact_id, :integer
    add_column :ovcs, :testing_contact_id, :integer
    add_column :ovcs, :technical_contact_id, :integer

  end

  def self.down
    add_column :ennis, :cenx_name, :string
    add_column :evcs, :cenx_name, :string
    add_column :cenx_ovcs, :cenx_name, :string
    add_column :ovcs, :cenx_name, :string
    remove_column :evcs, :description
    remove_column :ovcs, :primary_contact_id
    remove_column :ovcs, :testing_contact_id
    remove_column :ovcs, :technical_contact_id
  end
end