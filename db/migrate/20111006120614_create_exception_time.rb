class CreateExceptionTime < ActiveRecord::Migration
  def self.up
    create_table :exception_times do |t|
      t.integer :time, :null => false
      t.timestamps
    end
  end

  def self.down
    drop_table :exception_times
  end
end
