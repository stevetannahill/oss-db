class CreateQosPolicies < ActiveRecord::Migration
  def self.up
    create_table :qos_policies do |t|
      t.string :type
      t.string :policy_name
      t.integer :policy_id

      t.integer :exchange_id

      t.timestamps
    end
    create_table :cenx_ovc_end_point_ennis_qos_policies, :id => false do |t|
      t.integer :qos_policy_id
      t.integer :cenx_ovc_end_point_enni_id
    end
    add_index "cenx_ovc_end_point_ennis_qos_policies", "qos_policy_id", :name => "index_qosps_ovces_on_qosp_id"
    add_index "cenx_ovc_end_point_ennis_qos_policies", "cenx_ovc_end_point_enni_id", :name => "index_qosps_ovces_on_ovce_id"
  end

  def self.down
    drop_table :qos_policies
    drop_table :cenx_ovc_end_point_ennis_qos_policies
  end
end
