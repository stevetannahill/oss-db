class EvolveCosEndpoint < ActiveRecord::Migration
  def self.up
    add_column :cos_end_points, :ingress_rate_limiting, :string, :default => "Policing"
    add_column :cos_end_points, :egress_rate_limiting, :string, :default => "Shaping"
    add_column :demarcs, :lag_mode , :string

    EnniNew.all.each {|enni|
      if enni.is_lag
        if enni.is_unprotected
          # Unprotected-lag
          enni.lag_mode = "N/A"
        else
          enni.lag_mode = "Active/Standby"
        end

        #force save (even if validation fails) by passing false
        #We know this particular change is valid
        enni.save false

      end
    }

  end

  def self.down
    remove_column :cos_end_points, :ingress_rate_limiting
    remove_column :cos_end_points, :egress_rate_limiting
    remove_column :demarcs, :lag_mode
  end
end
