class CreateStateful < ActiveRecord::Migration
  def self.up
    create_table "statefuls", :force => true do |t|
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   :type
    end

    create_table "states", :force => true do |t|
      t.datetime "timestamp"
      t.string   "name"
      t.text     "notes"
      t.integer  "stateful_id"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   :type
    
    end
  
    create_table "stateful_ownerships", :force => true do |t|
      t.references "stateable", :polymorphic => true
      t.references "stateful"       
    end
  end

  def self.down
    drop_table "statefuls"
    drop_table "states"
    drop_table "stateful_ownerships"
  end
end
