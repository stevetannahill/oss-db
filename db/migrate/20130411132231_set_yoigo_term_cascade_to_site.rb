class SetYoigoTermCascadeToSite < ActiveRecord::Migration

  class ServiceProvider < ActiveRecord::Base
    store :terms, accessors: [ :network_type, :aggregation_site, :site, :wdc, :gateway_site, :demarc, :site_group, :service_provider, :path, :enni, :segment, :aav, :microwatt, :cascade ]
  end

  def up
    p = ServiceProvider.where("name = 'Yoigo'").first
    unless p.nil?
      p.cascade = "Site"
      p.save
    end
  end

  def down
    # no need to undo, this is a seed migration
  end
end
