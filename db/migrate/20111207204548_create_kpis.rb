class Kpi_2011_12_26_10_10 < ActiveRecord::Base
  belongs_to :kpiable, :polymorphic => true
  
  set_table_name 'kpis'
  set_inheritance_column :_type_disabled
end

class EnniNew_2011_12_26_10_10 < Demarc
  set_table_name 'demarcs'
  set_inheritance_column :_type_disabled
  has_many :kpis, :as => :kpiable, :class_name => "Kpi_2011_12_26_10_10"
end

class BxCosTestVector_2011_12_26_10_10 < CosTestVector
  set_table_name 'cos_test_vectors'
  set_inheritance_column :_type_disabled
  has_many :kpis, :as => :kpiable, :class_name => "Kpi_2011_12_26_10_10"
end

class CreateKpis < ActiveRecord::Migration
  def up
    create_table :kpis do |t|
      t.string :name
      t.string :for_klass
      t.string :units
      t.integer :warning_threshold
      t.integer :error_threshold
      t.integer :warning_time
      t.integer :error_time
      t.integer :delta_t
      t.boolean :actual_kpi
      t.references :kpiable, :polymorphic => true
      t.timestamps
    end
    
    Kpi_2011_12_26_10_10.reset_column_information
    default_enni_kpis = [
       {:name => "egress_util", :for_klass => EnniNew.name, :warning_time => 80.hours, :error_time => 80.hours, :delta_t => 5.minutes, :warning_threshold => 50, :error_threshold => 75, :units => "%", :actual_kpi => true},
       {:name => "ingress_util", :for_klass => EnniNew.name, :warning_time => 80.hours, :error_time => 80.hours, :delta_t => 5.minutes, :warning_threshold => 50, :error_threshold => 75, :units => "%", :actual_kpi => true}
    ]
    EnniNew_2011_12_26_10_10.all.each do |enni|
      default_enni_kpis.each {|kpi| enni.kpis << Kpi_2011_12_26_10_10.create(kpi)}
    end
    
    default_costv_kpis = [    
      {:name => "delay", :for_klass => BxCosTestVector.name, :warning_time => 30.minutes, :error_time => 10.minute, :delta_t => 59.seconds, :warning_threshold => nil, :error_threshold => nil, :units => "ms", :actual_kpi => true},
      {:name => "delay_variation", :for_klass => BxCosTestVector.name, :warning_time => 30.minutes, :error_time => 10.minute, :delta_t => 59.seconds, :warning_threshold => nil, :error_threshold => nil, :units => "us", :actual_kpi => true},
      {:name => "flr", :for_klass => BxCosTestVector.name, :warning_time => 30.minutes, :error_time => 10.minute, :delta_t => 59.seconds, :warning_threshold => nil, :error_threshold => nil, :units => "%", :actual_kpi => true}
    ]
    BxCosTestVector_2011_12_26_10_10.all.each do |costv|
      default_costv_kpis.each {|kpi| costv.kpis << Kpi_2011_12_26_10_10.create(kpi)}
    end

  end
  
  def down
    drop_table :kpis
  end
end
