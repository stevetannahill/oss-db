#Needed due to removal of cenx_operator_network later
class Exchange < Pop
  belongs_to :cenx_operator_network
end

class OvcNew < InstanceElement
end

class CenxOvcNew < OvcNew
  def get_operator_network
    pop_id ? pop.cenx_operator_network : nil
  end
end

class AddPopAndOwnerToEvcDisplays < ActiveRecord::Migration
  def up
    add_column :evc_displays, :pop_id, :int
    add_column :evc_displays, :owner_service_provider_id, :int
  end
  
  def down
    remove_column :evc_displays, :pop_id
    remove_column :evc_displays, :owner_service_provider_id
  end
  
end
