class CreateNewOvcs < ActiveRecord::Migration
  def self.up
    create_table :ovc_news do |t|
      
     # common attributes
     t.string :type
     t.string :buyer_name
     t.string :seller_name
     t.string :status
     t.string :notes
     t.string :ovc_owner_role
     t.integer :evc_id
     t.integer :ovc_type_id
     t.string :cenx_id
     t.integer :operator_network_id

     # attributes for type=NewCenxOVC
     t.integer :exchange_id
     t.integer :service_id

     t.timestamps
    end

    add_column :service_orders, :evc_id, :integer

  end

  def self.down
    drop_table :ovc_news

    remove_column :service_orders, :evc_id
  end
end
