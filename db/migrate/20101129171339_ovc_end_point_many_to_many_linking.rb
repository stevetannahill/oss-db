class OvcEndPointNew < InstanceElement
end

class OvcEndPointManyToManyLinking < ActiveRecord::Migration
  def self.up
    create_table :ovc_end_point_news_ovc_end_point_news, :id => false do |t|
      t.column :ovc_end_point_new_id, :integer
      t.column :self_id, :integer
    end
    add_index :ovc_end_point_news_ovc_end_point_news, :ovc_end_point_new_id, :name => "ovc_end_point_new_id_index"
    add_index :ovc_end_point_news_ovc_end_point_news, :self_id, :name => "self_id_index"
    
    OvcEndPointNew.all.each{|ovce|
      if ovce.connected_end_point_id
        execute "INSERT INTO ovc_end_point_news_ovc_end_point_news (ovc_end_point_new_id, self_id) VALUES (#{ovce.connected_end_point_id}, #{ovce.id})"
      end
    }
    
    remove_column :ovc_end_point_news, :connected_end_point_id
  
  end

  def self.down
    add_column :ovc_end_point_news, :connected_end_point_id, :integer
    
    OvcEndPointNew.all.each{|ovce|
      results = execute "SELECT ovc_end_point_new_id FROM ovc_end_point_news_ovc_end_point_news WHERE self_id = #{ovce.id}"
      if results.num_rows() > 0
        execute "UPDATE ovc_end_point_news SET connected_end_point_id = #{results.fetch_hash["ovc_end_point_new_id"]} WHERE id = #{ovce.id}"
      end
      results.free()
    }
    
    drop_table :ovc_end_point_news_ovc_end_point_news
  end
end
