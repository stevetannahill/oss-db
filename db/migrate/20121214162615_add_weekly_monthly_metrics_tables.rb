class AddWeeklyMonthlyMetricsTables < ActiveRecord::Migration
  def up
    create_table :metrics_weekly_cos_test_vectors  do |t|
      t.integer "period_start_epoch",  :limit => 8
      t.integer "time_declared_epoch", :limit => 8
      t.float   "metric"
      t.integer "minor_time_over"
      t.integer "major_time_over"
      t.integer "grid_circuit_id"
      t.string  "metric_type"
      t.string  "severity"
    end
    add_index :metrics_weekly_cos_test_vectors, ["grid_circuit_id", "metric_type", "period_start_epoch"], :name => "metrics_cos_test_vector_weekly_search", :unique => true
    add_index :metrics_weekly_cos_test_vectors, ["grid_circuit_id", "time_declared_epoch", "severity"], :name => "index_rollup_metrics_cos_test_vectors_weekly_grid_time_sev"
    add_index :metrics_weekly_cos_test_vectors, ["grid_circuit_id", "time_declared_epoch"], :name => "index_rollup_metrics_cos_test_vectors_weekly_grid_time"

    create_table :metrics_monthly_cos_test_vectors  do |t|
      t.integer "period_start_epoch",  :limit => 8
      t.integer "time_declared_epoch", :limit => 8
      t.float   "metric"
      t.integer "minor_time_over"
      t.integer "major_time_over"
      t.integer "grid_circuit_id"
      t.string  "metric_type"
      t.string  "severity"
    end
    add_index :metrics_monthly_cos_test_vectors, ["grid_circuit_id", "metric_type", "period_start_epoch"], :name => "metrics_cos_test_vector_monthly_search", :unique => true
    add_index :metrics_monthly_cos_test_vectors, ["grid_circuit_id", "time_declared_epoch", "severity"], :name => "index_rollup_metrics_cos_test_vectors_monthly_grid_time_sev"
    add_index :metrics_monthly_cos_test_vectors, ["grid_circuit_id", "time_declared_epoch"], :name => "index_rollup_metrics_cos_test_vectors_monthly_grid_time"

    sql = <<EOF
INSERT INTO metrics_monthly_cos_test_vectors  SELECT id, period_start_epoch, time_declared_epoch, metric, minor_time_over, major_time_over, grid_circuit_id, metric_type, severity FROM metrics_cos_test_vectors WHERE period_type='monthly';
EOF
    ActiveRecord::Base.connection.execute(sql)
    
    sql = <<EOF
INSERT INTO metrics_weekly_cos_test_vectors  SELECT id, period_start_epoch, time_declared_epoch, metric, minor_time_over, major_time_over, grid_circuit_id, metric_type, severity FROM metrics_cos_test_vectors WHERE period_type='weekly';
EOF
    ActiveRecord::Base.connection.execute(sql)
    drop_table :metrics_cos_test_vectors

  end

  def down
    drop_table :metrics_monthly_cos_test_vectors
    drop_table :metrics_weekly_cos_test_vectors
  end
end
