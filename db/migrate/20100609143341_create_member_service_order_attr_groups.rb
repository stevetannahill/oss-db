class CreateMemberServiceOrderAttrGroups < ActiveRecord::Migration
  def self.up
    create_table :member_service_order_attr_groups do |t|
      t.string :name
      t.string :description
      t.integer :operartor_network_type_id

      t.timestamps
    end
    add_column :service_types, :member_service_order_attr_group_id, :integer
  end

  def self.down
    drop_table :member_service_order_attr_groups
    remove_column :service_types, :member_service_order_attr_group_id
  end

end
