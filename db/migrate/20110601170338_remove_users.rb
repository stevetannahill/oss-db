class RemoveUsers < ActiveRecord::Migration
  def self.up
    drop_table :users
  end

  def self.down
    create_table :users do |t|
      t.column :name, :string
      t.column :email, :string
      t.column :age, :integer
      t.column :ssn, :string
      t.column :phone, :string
      t.string   "email", :null => false
      t.string   "first_name"
      t.string   "last_name"
      t.string   "crypted_password",    :limit => 40
      t.string   "salt",                :limit => 40
      t.string   "activation_code",     :limit => 40
      t.datetime "last_login_at"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "service_provider_id"
      t.datetime "password_expiry"
      t.integer  "contact_id"
    end
  end
end
