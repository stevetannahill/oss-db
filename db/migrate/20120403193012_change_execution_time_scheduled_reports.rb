class ChangeExecutionTimeScheduledReports < ActiveRecord::Migration
  def up
    change_column :scheduled_reports, :execution_time, :time
  end
  
  def down
    change_column :scheduled_reports, :execution_time, :string
  end
end
