class AddTopTalkersTable < ActiveRecord::Migration
  def self.up
    create_table :top_talkers do |t|
      t.string  :type
      t.integer :enni_id
      t.integer :cos_end_point_id
      t.float   :octets_count
      t.string  :direction
      t.integer :time_period_start
      t.integer :time_period_end
    end
  end

  def self.down
    drop_table :top_talkers
  end
end
