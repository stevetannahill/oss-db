class CreateContacts < ActiveRecord::Migration
  def self.up
    create_table :contacts do |t|
      t.string :name
      t.string :position
      t.string :work_phone
      t.string :mobile_phone
      t.string :e_mail
      t.string :fax
      t.integer :service_provider_id

      t.timestamps
    end
  end

  def self.down
    drop_table :contacts
  end
end
