class AddNewDataToServiceOrder < ActiveRecord::Migration
  def self.up
    add_column :service_orders, :local_contact_id, :integer
    add_column :service_orders, :foc_date, :date
  end

  def self.down
    remove_column :service_orders, :local_contact_id
    remove_column :service_orders, :foc_date
  end
end
