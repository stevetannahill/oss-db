class ChangeStateTimestampToBigint < ActiveRecord::Migration
  def self.up
    change_column :states, :timestamp, 'bigint'
  end

  def self.down
    change_column :states, :timestamp, :datetime
  end
end
