class AddOnBehalfOfServiceProviderToDemarc < ActiveRecord::Migration
  def self.up
    add_column :demarcs, :on_behalf_of_service_provider_id, :integer
  end

  def self.down
    remove_column :demarcs, :on_behalf_of_service_provider_id
  end
end