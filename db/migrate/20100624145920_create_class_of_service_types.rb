class CreateClassOfServiceTypes < ActiveRecord::Migration
  def self.up
    create_table :class_of_service_types do |t|
      t.string :name
      t.integer :operator_network_type_id
      t.string :cos_id_type
      t.string :cos_value_enni
      t.string :cos_value_uni
      t.string :availability
      t.string :frame_loss_ratio
      t.string :mttr_hrs
      t.string :notes
      
      t.timestamps
    end
         
    # generate the join table
    create_table "class_of_service_types_ethernet_service_types", :id => false do |t|
      t.column "ethernet_service_type_id", :integer
      t.column "class_of_service_type_id", :integer
    end
    add_index "class_of_service_types_ethernet_service_types", "ethernet_service_type_id", :name => "index_costs_ests_on_est_id"
    add_index "class_of_service_types_ethernet_service_types", "class_of_service_type_id", :name => "index_costs_ests_on_cost_id"

    
  end

  def self.down
    drop_table :class_of_service_types
    drop_table :class_of_service_types_ethernet_service_types
  end
end
