class AddDisabledToMemberAttrs < ActiveRecord::Migration
  def self.up
    add_column :member_attrs, :disabled, :boolean
  end

  def self.down
    remove_column :member_attrs, :disabled
  end
end
