class ReworkDemarcTypeRemove < ActiveRecord::Migration
  def self.up
    remove_column :demarc_types, :speed
    remove_column :demarc_types, :mtu_notes
    remove_column :demarc_types, :tx_power_lo
    remove_column :demarc_types, :tx_power_hi
    remove_column :demarc_types, :rx_power_lo
    remove_column :demarc_types, :rx_power_hi
    remove_column :demarc_types, :fiber_type
    remove_column :demarc_types, :max_endpoints_per_ovc
    remove_column :demarc_types, :e_lmi_untagged
    remove_column :demarc_types, :e_lmi_tagged

  end

  def self.down
    add_column :demarc_types, :speed, :string
    add_column :demarc_types, :mtu_notes, :string
    add_column :demarc_types, :tx_power_lo, :string
    add_column :demarc_types, :tx_power_hi, :string
    add_column :demarc_types, :rx_power_lo, :string
    add_column :demarc_types, :rx_power_hi, :string
    add_column :demarc_types, :fiber_type, :string
    add_column :demarc_types, :max_endpoints_per_ovc, :string
    add_column :demarc_types, :e_lmi_untagged, :string
    add_column :demarc_types, :e_lmi_tagged, :string
  end
end
