class OssReworkBwpType < ActiveRecord::Migration
  def self.up
    remove_column :bw_profile_types, :min_cir_kbps
    remove_column :bw_profile_types, :max_cir_kbps
    remove_column :bw_profile_types, :min_cbs_kb
    remove_column :bw_profile_types, :max_cbs_kb
    remove_column :bw_profile_types, :min_eir_kbps
    remove_column :bw_profile_types, :max_eir_kbps
    remove_column :bw_profile_types, :min_ebs_kb
    remove_column :bw_profile_types, :max_ebs_kb
  end

  def self.down
    add_column :bw_profile_types, :min_cir_kbps, :string
    add_column :bw_profile_types, :max_cir_kbps, :string
    add_column :bw_profile_types, :min_cbs_kb, :string
    add_column :bw_profile_types, :max_cbs_kb, :string
    add_column :bw_profile_types, :min_eir_kbps, :string
    add_column :bw_profile_types, :max_eir_kbps, :string
    add_column :bw_profile_types, :min_ebs_kb, :string
    add_column :bw_profile_types, :max_ebs_kb, :string
  end
end
