class CreateServiceProviders < ActiveRecord::Migration
  def self.up
    create_table :service_providers do |t|
      t.string :name
      t.string :contact_email
      t.string :website
      t.boolean :is_cenx, :default => false
      t.integer :password_expiry_interval

      t.timestamps
    end
    
    add_column :ennis, :service_provider_id, :integer
    
  end

  def self.down
    remove_column :ennis, :service_provider_id
    drop_table :service_providers
  end
end
