class ModelChangesForSiteGroup < ActiveRecord::Migration
  
#  class Pop < ActiveRecord::Base
#    self.inheritance_column = :_type_disabled
#    set_table_name 'site_groups'    
#    has_many :sites    
#  end
  
  class Site < ActiveRecord::Base
    has_and_belongs_to_many :site_groups
    self.inheritance_column = :_type_disabled
  end
  
  class SiteGroup < ActiveRecord::Base
    has_and_belongs_to_many :sites
    has_many :old_sites, :class_name => "Site", :foreign_key => :pop_id
    self.inheritance_column = :_type_disabled
  end
   
  class ServiceProvider < ActiveRecord::Base
    self.inheritance_column = :_type_disabled
  end
  
  class InstanceElement < ActiveRecord::Base
    self.abstract_class = true
    self.inheritance_column = :_type_disabled
  end

  class Demarc < InstanceElement
    self.inheritance_column = :_type_disabled
  end
  
  class Segment < InstanceElement
    self.inheritance_column = :_type_disabled
  end
  
  class Kpi < ActiveRecord::Base
    self.inheritance_column = :_type_disabled
  end
  
  class KpiOwnership < ActiveRecord::Base
    self.inheritance_column = :_type_disabled
  end
  
  class QosPolicy < ActiveRecord::Base
    self.inheritance_column = :_type_disabled
  end
  
  class EvcDisplay < ActiveRecord::Base
    self.inheritance_column = :_type_disabled
  end
  
  def up
    puts "Warning: This is a non-revertable migration. Configuration will be lost and there is no way to restore it for a rollback" 
    
    create_table :site_groups_sites, :id => false do |t|
      t.integer :site_id
      t.integer :site_group_id
    end
    
    change_table :sites, :force => true do |t|
      t.string   "country"
      t.integer  "service_id_low"
      t.integer  "service_id_high"
      t.string   "site_layout" , :default => "StandardExchange7750"
    end
    
    rename_table :pops, :site_groups
    
    Site.connection.schema_cache.clear!
    Site.reset_column_information
    Site.inheritance_column = :_type_disabled
    
    # Copy the attributes across to all sites (should be only one site in current data)
    SiteGroup.where(:type => "Exchange").all.each do |sg|
      sg.old_sites.each do |site|        
        [:country, :service_id_low, :service_id_high, :pop_type].each do |attribute|
          if attribute == :pop_type
            # pop_type renamed to site_layout in site object
            site[:site_layout] = sg[attribute]
          else
            site[attribute] = sg[attribute]
          end
        end
        if site.type == "ExchangeLocation"
          site.type = "AggregationSite"
        end
        site_name = site.name
        # strip out site name if it exists in the pop name
        regex = Regexp.new(" \\[#{site.name}\\]")
        sg_name = sg.name.gsub(regex,"")
        site.name = sg_name + " [#{site.name}]"
        site.save!       
      end
    end
    
    # Update any remaining ExchangeLocation to AggregationSite
    Site.update_all("type = 'AggregationSite'", "type = 'ExchangeLocation'")
    
    
    # Change the pop_id ref to site_id and set the site_id to the correct site
    [Demarc, Segment, QosPolicy, EvcDisplay].each do |klass|
      add_column klass.table_name, :site_id, :integer            
      klass.all.each do |obj|
        if obj.pop_id
          sg = SiteGroup.find(obj.pop_id)
          raise "#{sg.class}:#{sg.id} Sites #{sg.old_sites.collect{|p| p.id}} More than 1 Site per Pop!" if sg.old_sites.size > 1
          site_id = sg.old_sites.first.id
          obj.site_id = site_id
          obj.save!
        end
      end
      remove_column klass.table_name, :pop_id
    end
    
    
    # Need to move both the ownership and the parent of the KPI from Pop to Site
    puts "Moving KPIs from Pop to Site"
    KpiOwnership.all.each do |eo|
      if eo.kpiable_type == "Pop"
        eo.kpiable_type = "Site"
        sg = eo.kpiable_id
        if sg != nil
          sg = SiteGroup.find(sg)
          raise "#{sg.class}:#{sg.id} Sites #{sg.sites.collect{|p| p.id}} More than 1 Site per Pop!" if sg.old_sites.size > 1
          site_id = sg.old_sites.first.id
          eo.kpiable_id = site_id
          eo.save!
        end
      end
      
      if eo.parent_type == "Pop"
        eo.parent_type = "Site"
        sg = eo.parent_id
        if sg != nil
          sg = SiteGroup.find(sg) 
          raise "#{sg.class}:#{sg.id} Sites #{sg.sites.collect{|p| p.id}} More than 1 Site per Pop!" if sg.old_sites.size > 1
          site_id = sg.old_sites.first.id
          eo.parent_id = site_id
          eo.save!
        end
      end
    end   
    
    SiteGroup.delete_all
        
    remove_column :site_groups, [:country, :service_id_low, :service_id_high, :pop_type]
    remove_column :sites, :pop_id
  end

  def down

    puts "Warning: This is a non-revertable migration. Configuration has been lost and cannot be restored and the model will be broken"
     
    change_table :site_groups, :force => true do |t|
      t.string   "country"
      t.integer  "service_id_low"
      t.integer  "service_id_high"
      t.string   "pop_type" , :default => "StandardExchange7750"
      t.integer  "cenx_operator_network_id"
    end
    add_column :sites, :pop_id, :integer
    
    
    # For each site create a pop!!!- Can't create the ones from before the migration as that has been lost
    Site.all.each do |site|
      sg = SiteGroup.new(:name => "Pop:" + site.name, :type => "Exchange") 
      [:country, :service_id_low, :service_id_high, :site_layout, :operator_network_id].each do |attribute|
        if attribute == :site_layout
          # site_layout renamed to pop_type in Pop object
          sg[:pop_type] = site[attribute]
        elsif attribute == :operator_network_id
          sg[:cenx_operator_network_id] = site[attribute]
        else
          sg[attribute] = site[attribute]
        end
      end
      sg.save!
      if site.type == "AggregationSite"
        site.type = "ExchangeLocation"
      end
      site.pop_id = sg.id
      site.save!
    end
    
    # Change the pop_id ref to site_id and set the site_id to the correct site
    [Demarc, Segment, QosPolicy, EvcDisplay].each do |klass|
      add_column klass.table_name, :pop_id, :integer            
      klass.all.each do |obj|
        if obj.site_id
          site = Site.find(obj.site_id)
          obj.pop_id = site.pop_id
          obj.save!
        end
      end
      remove_column klass.table_name, :site_id
    end   
    
    # Need to move both the ownership and the parent of the KPI from Pop to Site
    puts "Moving KPIs from Site to Pop"
    KpiOwnership.all.each do |eo|
      if eo.kpiable_type == "Site"
        eo.kpiable_type = "Pop"
        site = eo.kpiable_id
        if site != nil
          site = Site.find(site) 
          eo.kpiable_id = site.pop_id 
          eo.save!
        end
      end
      
      if eo.parent_type == "Site"
        eo.parent_type = "Pop"
        site = eo.parent_id
        if site != nil
          site = Site.find(site) 
          eo.parent_id = site.pop_id 
          eo.save!
        end
      end
    end   
    
    remove_column :sites, [:country, :service_id_low, :service_id_high, :site_layout]
    drop_table :site_groups_sites
    rename_table :site_groups, :pops    
  end
end
