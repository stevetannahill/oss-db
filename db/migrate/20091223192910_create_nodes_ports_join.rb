class CreateNodesPortsJoin < ActiveRecord::Migration
  def self.up
    add_column :ports, :node_id, :integer
    rename_column :ports, :porttype, :port_type
    rename_column :nodes, :nodetype, :node_type
  end

  def self.down
    rename_column :nodes, :node_type, :nodetype 
    rename_column :ports, :port_type, :porttype
    remove_column :ports, :node_id 
  end
end
