class CreateEthernetServiceTypes < ActiveRecord::Migration
  def self.up
    create_table :ethernet_service_types do |t|
      t.integer :operator_network_type_id
      t.string :name
      
      t.timestamps
    end
         
    # generate the join table
    create_table "demarc_types_ethernet_service_types", :id => false do |t|
      t.column "ethernet_service_type_id", :integer
      t.column "demarc_type_id", :integer
    end
    add_index "demarc_types_ethernet_service_types", "ethernet_service_type_id", :name => "index_dts_ests_on_est_id"
    add_index "demarc_types_ethernet_service_types", "demarc_type_id", :name => "index_dts_ests_on_dt_id"

    
  end

  def self.down
    drop_table :ethernet_service_types
    drop_table :demarc_types_ethernet_service_types
  end
end
