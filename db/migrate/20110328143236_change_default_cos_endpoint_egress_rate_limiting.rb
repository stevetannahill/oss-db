class ChangeDefaultCosEndpointEgressRateLimiting < ActiveRecord::Migration
  def self.up
    change_column_default :cos_end_points, :egress_rate_limiting, "None"
  end

  def self.down
    change_column_default :cos_end_points, :egress_rate_limiting, "Shaping"
  end
end
