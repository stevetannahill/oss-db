class AddFalloutExceptions < ActiveRecord::Migration
  def change
    create_table    :fallout_exceptions do |t|
      t.column      :timestamp, "bigint"      
      t.column      :clear_timestamp, "bigint"
      t.references  :fallout_item, :polymorphic => true
      t.string      :severity
      t.string      :exception_type
      t.string      :data_source
      t.text        :details
      t.timestamps
    end
    add_column :paths, :fallout_timestamp, :bigint, :default => 0
    add_column :paths, :fallout_severity, :string, :default => "ok"
    add_column :paths, :fallout_exception_type, :string, :default => ""
    add_column :paths, :fallout_details, :text, :default => ""
    add_column :paths, :fallout_data_source, :string    
  end
end
