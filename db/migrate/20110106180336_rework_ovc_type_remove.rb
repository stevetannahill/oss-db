class ReworkOvcTypeRemove < ActiveRecord::Migration
  def self.up
    remove_column :ovc_types, :role
    remove_column :ovc_types, :cos_marking_uni
    remove_column :ovc_types, :cos_marking_at_enni_is_pcp
    remove_column :ovc_types, :cos_marking_at_enni_notes
    remove_column :ovc_types, :service_category
    remove_column :ovc_types, :max_uni_endpoints
    remove_column :ovc_types, :max_enni_endpoints
  end

  def self.down
    add_column :ovc_types, :role, :string
    add_column :ovc_types, :cos_marking_uni, :string
    add_column :ovc_types, :cos_marking_at_enni_is_pcp, :boolean
    add_column :ovc_types, :cos_marking_at_enni_notes, :string
    add_column :ovc_types, :service_category, :string
    add_column :ovc_types, :max_uni_endpoints, :integer
    add_column :ovc_types, :max_enni_endpoints, :integer
  end
end
