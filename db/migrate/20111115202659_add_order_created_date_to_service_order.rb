class ServiceOrder_15_11_2011_11_11 < ActiveRecord::Base
  belongs_to :ordered_entity, :polymorphic => true
  
  set_table_name 'service_orders'
  set_inheritance_column :_type_disabled
end

class Exchange_15_11_2011_11_11 < ActiveRecord::Base
  
  set_table_name 'exchanges'
  set_inheritance_column :_type_disabled
end

class EnniNew_15_11_2011_11_11 < ActiveRecord::Base
  belongs_to :exchange
  set_table_name 'demarcs'
  set_inheritance_column :_type_disabled
end

class AddOrderCreatedDateToServiceOrder < ActiveRecord::Migration
  def up
    add_column :service_orders, :order_created_date, :date
    
    ServiceOrder_15_11_2011_11_11.reset_column_information
    ServiceOrder_15_11_2011_11_11.all.each do |so|
      so.order_created_date = so.order_received_date
      if so.type == "EnniOrderNew" && so.ordered_entity != nil
        ex = Exchange_15_11_2011_11_11.find(so.ordered_entity[:exchange_id])
        so.ordered_operator_network_id = ex.cenx_operator_network_id
        #so.ordered_operator_network_id = so.ordered_entity.exchange.cenx_operator_network_id
      end
      so.save
    end
    
  end
  
  def down
    remove_column :order_created_date, :service_orders
    
    ServiceOrder_15_11_2011_11_11.reset_column_information
    ServiceOrder_15_11_2011_11_11.all.each do |so|
      if so.type == "EnniOrderNew" && so.ordered_entity != nil
        so.ordered_operator_network_id = nil
      end
      so.save
    end
    
  end
end
