class CreateOrderedEntityGroups < ActiveRecord::Migration
  def self.up
    create_table :ordered_entity_groups do |t|
      t.string :name
      t.string :order_type_id

      t.timestamps
    end
    
    add_column :service_orders, :ordered_entity_group_id, :integer
  end

  def self.down
    drop_table :ordered_entity_groups
    remove_column :service_orders, :ordered_entity_group_id
  end
end
