class ChangeLightSquaredLocations < ActiveRecord::Migration

  class ExchangeLocation < ActiveRecord::Base
  end

  def self.up
    ExchangeLocation.reset_column_information
    ExchangeLocation.find_all_by_location_type("Default").each do |exl|
      exl.location_type = "Primary"
      exl.save false
    end
  end

  def self.down
  end
end
