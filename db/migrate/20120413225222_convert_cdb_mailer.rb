class ConvertCdbMailer < ActiveRecord::Migration
  class EmailConfig < ActiveRecord::Base
    serialize :override_event_email_address
    serialize :override_order_email_address
    serialize :debug_bcc_event_email_addresses
    serialize :debug_bcc_order_email_addresses
    serialize :cenx_ops_request
    serialize :cenx_ops
    serialize :cenx_eng
    serialize :cenx_cust_ops
    serialize :cenx_prov
    serialize :cenx_billing
  end

  def up
    ec = EmailConfig.first
    if ec == nil
      ec = EmailConfig.create
    end
    if defined?(CdbMailer) 
      [:debug_bcc_event_email_addresses, :debug_bcc_order_email_addresses, :override_event_email_address, :override_order_email_address,
       :cenx_ops, :cenx_ops_request, :cenx_eng, :cenx_cust_ops, :cenx_prov, :cenx_billing].each do |attribute|
         ec.send(attribute.to_s+"=", CdbMailer.send(attribute)) if CdbMailer.respond_to?(attribute)
      end
      ec.enable_delivery = ActionMailer::Base.perform_deliveries
    end
    ec.save
  end

  def down
  end
end
