class AddCenxIds < ActiveRecord::Migration
  def self.up
    add_column :service_providers, :cenx_id, :string
    add_column :ennis, :cenx_id, :string
    add_column :ovcs, :cenx_id, :string
    add_column :cenx_ovcs, :cenx_id, :string
    add_column :evcs, :cenx_id, :string
    add_column :enni_orders, :cenx_id, :string
  end

  def self.down
    remove_column :service_providers, :cenx_id
    remove_column :ennis, :cenx_id
    remove_column :ovcs, :cenx_id
    remove_column :cenx_ovcs, :cenx_id
    remove_column :evcs, :cenx_id
    remove_column :enni_orders, :cenx_id
  end
end
