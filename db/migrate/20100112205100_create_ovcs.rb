class CreateOvcs < ActiveRecord::Migration
  def self.up
    create_table :ovcs do |t|
      t.string :cenx_name
      t.string :buyer_name
      t.string :seller_name
      t.string :ovc_owner_role
      t.string :cenx_cma
      t.boolean :monitoring
      t.string :status
      t.string :description
      t.integer :evc_id
      t.integer :service_provider_id
      t.integer :service_type_id

      t.timestamps
    end
  end

  def self.down
    drop_table :ovcs
  end
end
