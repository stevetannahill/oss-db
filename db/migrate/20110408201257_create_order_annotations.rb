class CreateOrderAnnotations < ActiveRecord::Migration
  def self.up
    create_table :order_annotations do |t|
      t.datetime :date
      t.text :note
      t.string :severity
      t.references :service_order

      t.timestamps
    end
  end

  def self.down
    drop_table :order_annotations
  end
end
