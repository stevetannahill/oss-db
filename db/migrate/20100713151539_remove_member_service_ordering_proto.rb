class RemoveMemberServiceOrderingProto < ActiveRecord::Migration
  def self.up
    drop_table :member_service_orders
    drop_table :member_service_order_attr_groups
    drop_table :member_service_order_attrs
    drop_table :member_service_order_attr_instances
    remove_column :service_types, :member_service_order_attr_group_id
  end

  def self.down
    create_table :member_service_orders do |t|
      t.string :description
      t.string :order_type
      t.integer :evc_id
      t.integer :ovc_id
      t.text :notes
      t.string :cenx_id
      
      t.timestamps
    end
    create_table :member_service_order_attr_groups do |t|
      t.string :name
      t.string :description
      t.integer :operator_network_type_id
      t.string :cenx_id
      
      t.timestamps
    end
    create_table :member_service_order_attrs do |t|
      t.string :type
      t.string :name
      t.string :attr_syntax
      t.string :attr_value_range
      t.string :attr_table_name
      t.integer :member_service_order_attr_group_id
      
      t.timestamps
    end
    create_table :member_service_order_attr_instances do |t|
      t.string :attr_value
      t.integer :member_service_order_id
      t.integer :member_service_order_attr_id
      
      t.timestamps
    end
    add_column :service_types, :member_service_order_attr_group_id, :integer
  end
end
