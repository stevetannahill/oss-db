class CreateMemberAttrs < ActiveRecord::Migration
  def self.up
    create_table :member_attrs do |t|
      t.string :name
      t.integer :affected_entity_id
      t.string :affected_entity_type
      t.string :type
      t.string :syntax
      t.string :value_range

      t.timestamps
    end
  end

  def self.down
    drop_table :member_attrs
  end
end
