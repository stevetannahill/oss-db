class NetworkManagementServer_23_11_2011_11_11 < ActiveRecord::Base
  set_table_name 'network_management_servers'
end

class ConvertNetworkMgmtAddresses < ActiveRecord::Migration
  
  @@dns_to_ip = {"sam-a.cenx.localnet" =>	"10.1.8.135",
              "sam-b.cenx.localnet" => "10.1.8.136",
              "brix-app.cenx.localnet" => "10.1.8.137",
              "ls-sam-a.cenx.localnet"	=> "10.1.8.138",
              "ls-sam-b.cenx.localnet" =>	"10.1.8.139",
              "brix-db.cenx.localnet"	=> "10.1.8.140",
              "brix-lab.cenx.localnet" =>	"172.16.24.8"}
  def up
  NetworkManagementServer_23_11_2011_11_11.all.each do |nms|
      dns_name = @@dns_to_ip.index(nms.primary_ip)
      if dns_name != nil
        nms.primary_ip = dns_name
        nms.save
      end
    end
  end

  def down
    NetworkManagementServer_23_11_2011_11_11.all.each do |nms|
      ip = @@dns_to_ip[nms.primary_ip]
      if ip != nil
        nms.primary_ip = ip
        nms.save
      end
    end
  end
end
