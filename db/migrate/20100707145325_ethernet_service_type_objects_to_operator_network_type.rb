class EthernetServiceTypeObjectsToOperatorNetworkType < ActiveRecord::Migration
  def self.up
    
    remove_column :ovc_types, :ethernet_service_type_id
    remove_column :ovc_end_point_types, :ethernet_service_type_id
    add_column :ovc_types, :operator_network_type_id, :integer
    add_column :ovc_end_point_types, :operator_network_type_id, :integer
    
    # generate the join table for OVC Types
    create_table "ethernet_service_types_ovc_types", :id => false do |t|
      t.column "ethernet_service_type_id", :integer
      t.column "ovc_type_id", :integer
    end
    add_index "ethernet_service_types_ovc_types", "ethernet_service_type_id", :name => "index_ests_ovcts_on_est_id"
    add_index "ethernet_service_types_ovc_types", "ovc_type_id", :name => "index_ests_ovcts_on_ovct_id"
    
    # generate the join table for OVC End Point Types
    create_table "ethernet_service_types_ovc_end_point_types", :id => false do |t|
      t.column "ethernet_service_type_id", :integer
      t.column "ovc_end_point_type_id", :integer
    end
    add_index "ethernet_service_types_ovc_end_point_types", "ethernet_service_type_id", :name => "index_ests_ovctes_on_est_id"
    add_index "ethernet_service_types_ovc_end_point_types", "ovc_end_point_type_id", :name => "index_ests_ovctes_on_ovcet_id"
   end

  def self.down
    add_column :ovc_types, :ethernet_service_type_id, :integer
    add_column :ovc_end_point_types, :ethernet_service_type_id, :integer
    remove_column :ovc_types, :operator_network_type_id
    remove_column :ovc_end_point_types, :operator_network_type_id
    drop_table :ethernet_service_types_ovc_types
    drop_table :ethernet_service_types_ovc_end_point_types
  end
end
