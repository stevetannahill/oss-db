class RemovePopAndMajorTimeOver < ActiveRecord::Migration
  def up
    add_column :metrics_cos_test_vectors, :time_over, :integer
    rename_column :rollup_report_paths,   :pop_id, :site_id
    rename_column :rollup_report_demarcs, :pop_id, :site_id
    add_index(:metrics_cos_test_vectors, [:grid_circuit_id, :metric_type, :measurement_type, :period_type, :period_start_epoch], :unique => true, :name => :metrics_cos_test_vector_search)
    create_table :rollup_report_sites do |t|
      t.integer :period_start_epoch
      t.integer :site_id
      t.string  :severity #"ENUM('clear', 'minor', 'major', 'critical')"  # high watermark of underlying Demarc severities.
      t.integer :metric_count
      t.integer :metric_minor_count
      t.integer :metric_major_count
    end
    drop_table :rollup_report_sitegroups
    
  end

  def down
    remove_column :metrics_cos_test_vectors, :time_over
    rename_column :rollup_report_paths,   :site_id, :pop_id
    rename_column :rollup_report_demarcs, :site_id, :pop_id
    remove_index(:metrics_cos_test_vectors, :name =>:metrics_cos_test_vector_search)
    create_table :rollup_report_sitegroups do |t|
      t.integer :period_start_epoch
      t.integer :site_id
      t.string  :severity #"ENUM('clear', 'minor', 'major', 'critical')"  # high watermark of underlying Demarc severities.
      t.integer :metric_count
      t.integer :metric_minor_count
      t.integer :metric_major_count
    end
    drop_table :rollup_report_sites
  end
end

