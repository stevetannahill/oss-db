class AddComponentTypeToEvcDisplays < ActiveRecord::Migration
  def up
    add_column :evc_displays, :component_type, :string
  end
  
  def down
    remove_column :evc_displays, :component_type
  end
end
