class ChangeExchangeLocationToSite < ActiveRecord::Migration
  def up
    rename_column :exchange_locations, :location_type, :site_type
    add_column :exchange_locations, :type, :string, :default => "ExchangeLocation"
    rename_table :exchange_locations, :sites
    rename_column :cabinets, :exchange_location_id, :site_id
    rename_column :inventory_items, :exchange_location_id, :site_id
    rename_column :nodes, :exchange_location_id, :site_id
    rename_column :patch_panels, :exchange_location_id, :site_id
    change_column_default :sites, :type, nil
  end

  def down
    rename_column :exchange_locations, :site_type, :location_type 
    remove_column :exchange_locations, :type
    rename_table :sites, :exchange_locations 
    rename_column :cabinets, :site_id, :exchange_location_id
    rename_column :inventory_items, :site_id, :exchange_location_id
    rename_column :nodes, :site_id, :exchange_location_id
    rename_column :patch_panels, :site_id, :exchange_location_id
  end
end
