class MoreOssEvolution < ActiveRecord::Migration
  def self.up
    add_column :class_of_service_types, :cos_marking_enni_egress_yellow, :string, :default => "N/A"
    add_column :class_of_service_types, :cos_marking_uni_egress_yellow, :string, :default => "N/A"
    add_column :ovc_end_point_types, :notes, :string
  end

  def self.down
    remove_column :class_of_service_types, :cos_marking_enni_egress_yellow
    remove_column :class_of_service_types, :cos_marking_uni_egress_yellow
    remove_column :ovc_end_point_types, :notes
  end
end
