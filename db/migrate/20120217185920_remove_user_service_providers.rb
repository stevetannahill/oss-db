class RemoveUserServiceProviders < ActiveRecord::Migration
  def up
    drop_table :user_service_providers
  end

  def down
    create_table :user_service_providers do |t|
      t.integer :user_id, :null => false
      t.integer :service_provider_id, :null => false
      t.timestamps
    end
  end
end
