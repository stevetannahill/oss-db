class MovingCenxOvcException < ActiveRecord::Migration
  def self.up
    remove_column :sla_exceptions, :cenx_ovc_new_id
    add_column    :sla_exceptions,  :direction, "ENUM('ingress', 'egress')"

    add_column :sla_exceptions, :threshold_mbps, :float
    add_column :sla_exceptions, :time_over, :integer
    add_column :sla_exceptions, :average_rate_while_over_threshold_mbps, :float

    create_table :utilization_thresholds do |t|
      t.float  :sla_warning_threshold, :default => 0.5
      t.integer :utilization_high_time_over, :default => 2.hour.to_i
      t.integer :utilization_low_time_over, :default => 1.day.to_i
      t.float  :utilization_high_threshold, :default => 0.5
      t.float  :utilization_low_threshold, :default => 0.25
    end
    UtilizationThreshold.create
    
  end

  def self.down
    add_column :sla_exceptions, :cenx_ovc_new_id, :integer
    remove_column :sla_exceptions,  :direction
    remove_column :sla_exceptions, :threshold_mbps
    remove_column :sla_exceptions, :time_over
    remove_column :sla_exceptions, :average_rate_while_over_threshold_mbps
    drop_table :utilization_thresholds
  end
end
