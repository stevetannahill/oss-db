class AddGuaranteesToCostv < ActiveRecord::Migration
  def self.up
    add_column :cos_test_vectors, :flr_sla_guarantee, :string
    add_column :cos_test_vectors, :dv_sla_guarantee, :string
    add_column :cos_test_vectors, :delay_sla_guarantee, :string
  end

  def self.down
    remove_column :cos_test_vectors, :flr_sla_guarantee
    remove_column :cos_test_vectors, :dv_sla_guarantee
    renove_column :cos_test_vectors, :delay_sla_guarantee
  end
end
