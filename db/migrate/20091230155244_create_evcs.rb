class CreateEvcs < ActiveRecord::Migration
  def self.up
    create_table :evcs do |t|
      t.string :name
      t.string :type
      t.integer :service_provider_id

      t.timestamps
    end
  end

  def self.down
    drop_table :evcs
  end
end
