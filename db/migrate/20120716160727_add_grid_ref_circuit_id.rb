class AddGridRefCircuitId < ActiveRecord::Migration
  def up
    remove_index(:metrics_cos_test_vectors, :name =>:metrics_cos_test_vector_search)
#    add_index(:metrics_cos_test_vectors, [:grid_circuit_id, :metric_type, :measurement_type, :period_type, :period_start_epoch], :unique => true, :name => :metrics_cos_test_vector_search)
    add_column :cos_test_vectors, :ref_grid_circuit_id, :integer
    CosTestVector.reset_column_information
    SpirentCosTestVector.find_each do |ctv|
      unless ctv.ref_grid_circuit_id
        unless ctv.ref_circuit_id.nil? or ctv.ref_circuit_id.empty?
          ref_ctv = SpirentCosTestVector.find_by_circuit_id(ctv.ref_circuit_id)
          ctv.update_attribute(:ref_grid_circuit_id, ref_ctv.grid_circuit_id) unless ref_ctv.nil?
        end
      end
    end
  end

  def down
    remove_column :cos_test_vectors, :ref_grid_circuit_id
  end
end
