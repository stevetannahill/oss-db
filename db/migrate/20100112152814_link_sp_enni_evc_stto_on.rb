class LinkSpEnniEvcSttoOn < ActiveRecord::Migration
  def self.up
    rename_column :ennis, :service_provider_id, :operator_network_id
    rename_column :evcs, :service_provider_id, :operator_network_id
    rename_column :service_types, :service_provider_id, :operator_network_id
  end

  def self.down
    rename_column :service_types, :operator_network_id, :service_provider_id
    rename_column :evcs, :operator_network_id, :service_provider_id
    rename_column :ennis, :operator_network_id, :service_provider_id
  end
end
