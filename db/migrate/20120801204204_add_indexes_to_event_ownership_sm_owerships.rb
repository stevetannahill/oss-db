class AddIndexesToEventOwnershipSmOwerships < ActiveRecord::Migration
  def change
    add_index "event_ownerships", :event_history_id
    add_index "sm_ownerships", :sm_history_id    
    add_index "sm_ownerships", [:smable_id, :smable_type]
    add_index "cos_test_vectors", [:type, :circuit_id]
    
  end
end
