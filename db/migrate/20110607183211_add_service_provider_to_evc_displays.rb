class AddServiceProviderToEvcDisplays < ActiveRecord::Migration
  def self.up
    add_column :evc_displays, :service_provider_id, :integer
    remove_index :evc_displays, :name => 'row_column_constraint'
    add_index :evc_displays, [:evc_id, :row, :column, :relative_position, :service_provider_id], :name => 'row_column_constraint', :unique => true
  end

  def self.down
    remove_index :evc_displays, :name => 'row_column_constraint'
    add_index :evc_displays, [:evc_id, :row, :column, :relative_position], :name => 'row_column_constraint', :unique => true
    remove_column :evc_displays, :service_provider_id
  end
end