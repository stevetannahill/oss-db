class AddReadyToOrderDateToServiceOrder < ActiveRecord::Migration
  def change
    add_column :service_orders, :ready_to_order_date, :date
  end
end
