class ChangeScheduleExecutionType < ActiveRecord::Migration
  def change
    change_column :schedules, :execution_time, :datetime
    add_column :schedules, :timezone, :string
  end


end
