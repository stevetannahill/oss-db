class AddIndexToExceptionTables < ActiveRecord::Migration
  def change
    add_index :exception_sla_cos_test_vectors, :time_declared
    add_index :exception_sla_network_types, :time_declared
    add_index :exception_utilization_cos_end_points, :time_declared
    add_index :exception_utilization_ennis, :time_declared
  end
end
