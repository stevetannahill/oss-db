class AddSmHistory < ActiveRecord::Migration
  def self.up
    create_table "sm_ownerships", :force => true do |t|
      t.references "smable", :polymorphic => true
      t.references "sm_history"       
    end
  end

  def self.down
   drop_table "sm_ownerships"
  end
end

