class CreateDemarcs < ActiveRecord::Migration
  def self.up
    create_table :demarcs do |t|
      t.string :type
      
      # common attributes
      t.string :cenx_id
      t.string :sp_name
      t.string :demark_type
      t.string :description
      t.integer :operator_network_id
      t.integer :uni_profile_id

      # attributes for type=Uni
      t.string :address
      t.string :port_name
      t.string :uni_speed
      t.string :nid_type
      t.string :nid_sw_version
      t.string :reflection_mechanism
      t.boolean :reflection_enabled
      t.string :mon_loopback_address
      t.string :end_user_name
      t.boolean :is_new_construction
      t.string :contact_info

      t.timestamps
    end

    drop_table :unis
    drop_table :cenx_ovc_intermediate_points

    add_column :uni_profiles, :reflection_mechanisms, :string
    add_column :uni_profiles, :name, :string
    rename_column :uni_profiles, :cfm_supported, :reflection_supported

  end

  def self.down
    drop_table :demarcs

    create_table :unis do |t|
      t.timestamps
    end

    create_table :cenx_ovc_intermediate_points do |t|
      t.string :on_switch_name
      t.string :status
      t.string :description
      t.integer :cenx_ovc_id

      t.timestamps
    end

    remove_column :uni_profiles, :reflection_mechanisms
    remove_column :uni_profiles, :name
    rename_column :uni_profiles, :reflection_supported, :cfm_supported

  end
end
