class CreateUploadedFiles < ActiveRecord::Migration
  def self.up
    create_table :uploaded_files do |t|
      t.string :comment
      t.string :name
      t.string :content_type
      t.binary :data, :limit => 1.megabyte
      t.integer :uploader_id
      t.string :uploader_type

      t.timestamps
    end
  end

  def self.down
    drop_table :uploaded_files
  end
end
