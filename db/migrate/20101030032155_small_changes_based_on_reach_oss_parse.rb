class SmallChangesBasedOnReachOssParse < ActiveRecord::Migration
  def self.up
    change_column :demarc_types, :max_num_ovcs, :string
    change_column :bw_profile_types, :notes, :text
  end

  def self.down
    change_column :demarc_types, :max_num_ovcs, :integer
    change_column :bw_profile_types, :notes, :string
  end
end
