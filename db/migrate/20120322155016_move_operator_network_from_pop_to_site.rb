
class MoveOperatorNetworkFromPopToSite < ActiveRecord::Migration
  
  class Site < ActiveRecord::Base
    self.inheritance_column = :_type_disabled
    belongs_to :pop

    private
    def valid_operator_network?
    end
  end
  
  class Pop < ActiveRecord::Base
    self.inheritance_column = :_type_disabled
  end
  
  def up
    add_column :sites, :operator_network_id, :int
    Site.connection.schema_cache.clear!
    Site.reset_column_information
    Site.inheritance_column = :_type_disabled
    
    Site.all.each do |site|
        Site.update(site.id, :operator_network_id => site.pop[:operator_network_id])
    end
    remove_column :pops, :operator_network_id
  end

  def down
    add_column :pops, :operator_network_id, :int
    Pop.all.each do |pop|
      pop[:operator_network_id] = pop.sites.first ? pop.sites.first[:operator_network_id] : nil
      pop.save
    end
    remove_column :sites, :operator_network_id
  end
end
