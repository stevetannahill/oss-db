class DbCleanupFeb12010 < ActiveRecord::Migration
  def self.up
    add_column :service_providers, :address, :string
    add_column :service_providers, :cenx_cma, :string
    remove_column :ovcs, :cenx_cma
    remove_column :enni_orders, :cenx_cma
    remove_column :ennis, :primary_contact_id
    remove_column :ennis, :testing_contact_id
    add_column :ports, :connected_port_id, :integer
  end

  def self.down
    remove_column :service_providers, :address
    remove_column :service_providers, :cenx_cma
    add_column :ovcs, :cenx_cma,  :string
    add_column :enni_orders, :cenx_cma,  :string
    add_column :ennis, :primary_contact_id, :integer
    add_column :ennis, :testing_contact_id, :integer
    remove_column :ports, :connected_port_id
  end
end
