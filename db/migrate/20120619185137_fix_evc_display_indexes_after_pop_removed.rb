class FixEvcDisplayIndexesAfterPopRemoved < ActiveRecord::Migration
  def up
    # stale index
    remove_index :evc_displays, :name => :pop_search
    # new index now that pop has been removed
    add_index :evc_displays, [:service_provider_id, :site_id], :name => :site_search
  end
  
  def down
    remove_index :evc_displays, :name => :site_search
  end
end
