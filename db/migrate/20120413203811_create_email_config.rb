class CreateEmailConfig < ActiveRecord::Migration
  def change
    create_table :email_configs, :force => true do |t|
      t.string :override_event_email_address, :limit => 1024, :null => false, :default => [].to_yaml
      t.string :override_order_email_address, :limit => 1024, :null => false, :default => [].to_yaml
      t.string :debug_bcc_event_email_addresses, :limit => 1024, :null => false, :default => [].to_yaml
      t.string :debug_bcc_order_email_addresses, :limit => 1024, :null => false, :default => [].to_yaml
      t.string :cenx_ops_request, :null => false, :limit => 1024, :default => ["ops-request@support.cenx.com"].to_yaml
      t.string :cenx_ops, :limit => 1024, :null => false, :default => ["support@cenx.com"].to_yaml
      t.string :cenx_eng, :limit => 1024, :null => false, :default => ["eng@support.cenx.com"].to_yaml
      t.string :cenx_cust_ops, :limit => 1024, :null => false, :default => ["customerops@support.cenx.com"].to_yaml
      t.string :cenx_prov, :limit => 1024, :null => false, :default => ["Provisioning@CENX.com"].to_yaml
      t.string :cenx_billing, :limit => 1024, :null => false, :default => [""].to_yaml
      t.boolean :enable_delivery, :null => false, :default => false
      t.boolean :development_enable_delivery, :null => false, :default => false
    end
    
  end

end
