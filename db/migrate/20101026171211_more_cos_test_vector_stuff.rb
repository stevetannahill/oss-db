class MoreCosTestVectorStuff < ActiveRecord::Migration
  def self.up
    remove_column :cos_test_vectors, :ovc_end_point_new_id
  end

  def self.down
    add_column :cos_test_vectors, :ovc_end_point_new_id, :integer
  end
end
