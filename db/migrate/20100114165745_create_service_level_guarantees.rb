class CreateServiceLevelGuarantees < ActiveRecord::Migration
  def self.up
    create_table :service_level_guarantees do |t|
      t.integer :min_dist_km
      t.integer :max_dist_km
      t.boolean :is_unbounded
      t.decimal :availability, :precision => 9, :scale => 6
      t.decimal :frame_loss_ratio, :precision => 9, :scale => 6
      t.decimal :delay_ms, :precision => 7, :scale => 3
      t.decimal :delay_variation_ms, :precision => 7, :scale => 3
      t.decimal :mttr_days, :precision => 5, :scale => 1
      t.integer :class_of_service_id

      t.timestamps
    end
  end

  def self.down
    drop_table :service_level_guarantees
  end
end
