class ModsToSpandContactFeb82010 < ActiveRecord::Migration
  def self.up
    add_column :exchanges, :service_id_low, :integer
    add_column :exchanges, :service_id_high, :integer
    add_column :contacts, :address, :string
    remove_column :service_providers, :contact_email
  end

  def self.down
    remove_column :exchanges, :service_id_low
    remove_column :exchanges, :service_id_high
    remove_column :contacts, :address
    add_column :service_providers, :contact_email, :string
  end
end
