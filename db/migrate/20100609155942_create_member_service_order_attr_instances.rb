class CreateMemberServiceOrderAttrInstances < ActiveRecord::Migration
  def self.up
    create_table :member_service_order_attr_instances do |t|
      t.string :attr_value
      t.integer :member_service_order_id
      t.integer :member_service_order_attr_id

      t.timestamps
    end
  end

  def self.down
    drop_table :member_service_order_attr_instances
  end
end
