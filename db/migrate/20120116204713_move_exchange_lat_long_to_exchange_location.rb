class Exchange_2011_01_16_10_10 < ActiveRecord::Base
  set_table_name 'exchanges'
  has_many :exchange_locations, :order => "name", :dependent => :destroy, :foreign_key => "exchange_id", :class_name => "ExchangeLocation_2011_01_16_10_10"
end

class ExchangeLocation_2011_01_16_10_10 < ActiveRecord::Base
  set_table_name 'exchange_locations'
  belongs_to :exchange, :class_name => "Exchange_2011_01_16_10_10"
end

class MoveExchangeLatLongToExchangeLocation < ActiveRecord::Migration
  def up
    add_column :exchange_locations, :latitude, :decimal, :precision => 9, :scale => 6, :default => 0
    add_column :exchange_locations, :longitude, :decimal, :precision => 9, :scale => 6, :default => 0

    ExchangeLocation_2011_01_16_10_10.reset_column_information

    Exchange_2011_01_16_10_10.all.each do |ex|
      ex.exchange_locations.each do |exl|
        exl.latitude = ex.latitude
        exl.longitude = ex.longitude
        exl.save
      end
    end
    
    remove_column :exchanges, :latitude
    remove_column :exchanges, :longitude
    
  end

  def down
    add_column :exchanges, :latitude, :decimal, :precision => 9, :scale => 6, :default => 0
    add_column :exchanges, :longitude, :decimal, :precision => 9, :scale => 6, :default => 0
    
    Exchange_2011_01_16_10_10.reset_column_information
    
    ExchangeLocation_2011_01_16_10_10.all.each do |exl|
      ex = exl.exchange
      ex.latitude = exl.latitude
      ex.longitude = exl.longitude
      ex.save
    end
    
    remove_column :exchange_locations, :latitude
    remove_column :exchange_locations, :longitude
    
    
  end
end
