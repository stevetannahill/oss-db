class Fixevcattrs < ActiveRecord::Migration
  def self.up
    rename_column :evcs, :name, :cenx_name
    rename_column :evcs, :type, :sp_name
  end

  def self.down
    rename_column :evcs, :sp_name, :type
    rename_column :evcs, :cenx_name, :name
  end
end
