class CreateCenxOvcIntermediatePoints < ActiveRecord::Migration
  def self.up
    create_table :cenx_ovc_intermediate_points do |t|
      t.string :on_switch_name
      t.string :status
      t.string :description
      t.integer :cenx_ovc_id

      t.timestamps
    end
  end

  def self.down
    drop_table :cenx_ovc_intermediate_points
  end
end
