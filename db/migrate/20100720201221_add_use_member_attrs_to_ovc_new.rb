class AddUseMemberAttrsToOvcNew < ActiveRecord::Migration
  def self.up
    add_column :ovc_news, :use_member_attrs, :boolean
  end

  def self.down
    remove_column :ovc_news, :use_member_attrs
  end
end
