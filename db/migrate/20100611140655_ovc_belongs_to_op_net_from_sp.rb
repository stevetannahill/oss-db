class OvcBelongsToOpNetFromSp < ActiveRecord::Migration
  def self.up
    remove_column :ovcs, :service_provider_id
    add_column :ovcs, :operator_network_id, :integer
  end

  def self.down
    remove_column :ovcs, :operator_network_id
    add_column :ovcs, :service_provider_id, :integer
  end
end
