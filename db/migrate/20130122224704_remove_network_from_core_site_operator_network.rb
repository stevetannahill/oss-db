class RemoveNetworkFromCoreSiteOperatorNetwork < ActiveRecord::Migration
  def up
  	OperatorNetwork.where("name LIKE 'Coresite % Network'").each do |on| 
  		on.name = on.name.gsub(/ Network$/, "")
  		on.save
  	end
  end

  def down
  end
end
