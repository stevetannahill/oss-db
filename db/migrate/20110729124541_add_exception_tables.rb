class AddExceptionTables < ActiveRecord::Migration
  def self.up
    create_table :sla_exceptions do |t|
      # common attributes
      t.string  :type
      t.integer :time_declared
      t.boolean :failure
      t.float   :value
      
      # ExceptionCenxOvc
      t.integer :cenx_ovc_new_id
      
      #ExceptionEnniNew
      t.integer :enni_new_id
      
      #ExceptionCosEndpoint
      t.integer :cos_end_point_id
      t.column  :sla_type, "ENUM('availability', 'flr', 'fd', 'fdv')"      
    end
  end

  def self.down
    drop_table :sla_exceptions
  end
end
