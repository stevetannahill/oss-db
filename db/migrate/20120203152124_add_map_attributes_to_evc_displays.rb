class AddMapAttributesToEvcDisplays < ActiveRecord::Migration
  def change
    add_column :evc_displays, :latitude, :decimal, :precision => 9, :scale => 6
    add_column :evc_displays, :longitude, :decimal, :precision => 9, :scale => 6
    add_column :evc_displays, :site_type, :string
  end
end
