class DemarcRework < ActiveRecord::Migration
  def self.up
    rename_column :demarcs, :sp_name, :member_name
    add_column :demarcs, :demarc_type_id,  :integer
    rename_column :demarcs, :description, :notes
    rename_column :demarcs, :nid_sw_version, :connected_device_sw_version
    rename_column :demarcs, :nid_type, :connected_device_type
    rename_column :demarcs, :uni_speed, :physical_medium
    add_column :demarcs, :mtu,  :integer
    add_column :demarcs, :max_num_ovcs,  :integer
    add_column :demarcs, :exchange_id,  :integer
    add_column :demarcs, :ether_type, :string
    add_column :demarcs, :protection_type, :string
    add_column :demarcs, :lag_id, :integer
    add_column :demarcs, :fiber_handoff_type, :string
    add_column :demarcs, :stag_reservation_notes, :string
    add_column :demarcs, :auto_negotiate, :boolean
    add_column :demarcs, :ag_supported, :boolean

  end

  def self.down
    rename_column :demarcs, :member_name, :sp_name
    remove_column :demarcs, :demarc_type_id

    rename_column :demarcs, :notes, :description
    rename_column :demarcs, :connected_device_sw_version, :nid_sw_version
    rename_column :demarcs, :connected_device_type, :nid_type
    rename_column :demarcs, :physical_medium, :uni_speed
    remove_column :demarcs, :mtu
    remove_column :demarcs, :max_num_ovcs
    remove_column :demarcs, :exchange_id
    remove_column :demarcs, :ether_type
    remove_column :demarcs, :protection_type
    remove_column :demarcs, :lag_id
    remove_column :demarcs, :fiber_handoff_type
    remove_column :demarcs, :stag_reservation_notes
    remove_column :demarcs, :auto_negotiate
    remove_column :demarcs, :ag_supported
  end
end
