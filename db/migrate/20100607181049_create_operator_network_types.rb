class CreateOperatorNetworkTypes < ActiveRecord::Migration
  def self.up
    create_table :operator_network_types do |t|
      t.string :name
      t.integer :service_provider_id

      t.timestamps
    end
    remove_column :offered_services_studies, :operator_network_id
    add_column :offered_services_studies, :operator_network_type_id, :integer
  end

  def self.down
    drop_table :operator_network_types
    add_column :offered_services_studies, :operator_network_id, :integer
    remove_column :offered_services_studies, :operator_network_type_id
  end
end
