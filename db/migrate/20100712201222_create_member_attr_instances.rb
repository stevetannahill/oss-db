class CreateMemberAttrInstances < ActiveRecord::Migration
  def self.up
    create_table :member_attr_instances do |t|
      t.integer :member_attr_id
      t.integer :affected_entity_id
      t.string :affected_entity_type
      t.string :value

      t.timestamps
    end
  end

  def self.down
    drop_table :member_attr_instances
  end
end
