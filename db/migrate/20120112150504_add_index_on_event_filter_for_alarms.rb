class AddIndexOnEventFilterForAlarms < ActiveRecord::Migration
  def change
    add_index :event_histories, :event_filter
  end
end
