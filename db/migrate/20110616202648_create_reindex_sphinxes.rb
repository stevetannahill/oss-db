class CreateReindexSphinxes < ActiveRecord::Migration
  def self.up
    create_table :reindex_sphinxes do |t|
      t.datetime "started_at"
      t.datetime "finished_at"
      t.boolean "scheduled"

      t.timestamps
    end 
    
    ReindexSphinx.create(:id => 1, :started_at => Time.now, :finished_at => Time.now,:scheduled => false)
  end

  def self.down
    drop_table :reindex_sphinxes
  end
end
