class RemoveCliPromptFromNodes < ActiveRecord::Migration
  def self.up
    remove_column :nodes, :cli_prompt
  end

  def self.down
    add_column :nodes, :cli_prompt, :string
  end
end
