class AddGridIdToBuilderLog < ActiveRecord::Migration
  class CosTestVector < ActiveRecord::Base
    self.inheritance_column = :_type_disabled
  end
  
  class BuilderLog < ActiveRecord::Base
  end
  
  def up
    add_column :builder_logs, :grid_id, :integer
  	
  	BuilderLog.find_each do |bl|
  	  ctv = CosTestVector.find_by_circuit_id(bl.name)
  	  if ctv
    	  bl.grid_id = ctv.grid_circuit_id
    	  bl.save!
  	  end
	  end
  end
  
  def down
    remove_column :buidler_logs, :grid_id
  end
end
