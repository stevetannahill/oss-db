class ModelCleanupJan28 < ActiveRecord::Migration
  def self.up
    add_column :cabinets, :exchange_location_id, :integer
    add_column :patch_panels, :floor, :string
    remove_column :nodes, :mac
    remove_column :nodes, :rack_number
    add_column :nodes, :cabinet_id, :integer
    remove_column :ennis, :cenx_cma
    remove_column :exchange_locations, :cabinets
    remove_column :exchange_locations, :meet_me_room

  end

  def self.down
    remove_column :cabinets, :exchange_location_id
    remove_column :patch_panels, :floor
    add_column :nodes, :mac, :string
    add_column :nodes, :rack_number, :string
    remove_column :nodes, :cabinet_id
    add_column :ennis, :cenx_cma, :string
    add_column :exchange_locations, :cabinets, :string
    add_column :exchange_locations, :meet_me_room, :string
  end
end
