class RenameScheduledReportToSehedule < ActiveRecord::Migration
  class ScheduledTasks < ActiveRecord::Base
  end
  
  def up
    rename_table :scheduled_reports, :schedules
    rename_table :filter_templates, :scheduled_tasks
    add_column :scheduled_tasks, :type, :string
    rename_column :schedules, :filter_template_id, :scheduled_task_id
    rename_column :reports, :scheduled_report_id, :schedule_id
    
    ScheduledTask.all.each {|st| st.update_attribute(:type, "FilterTemplate")}
  end

  def down
    remove_column :scheduled_tasks, :type
    rename_column :schedules, :scheduled_task_id, :filter_template_id
    rename_column :reports, :schedule_id,  :scheduled_report_id
    rename_table :schedules, :scheduled_reports
    rename_table :scheduled_tasks, :filter_templates
  end
end
