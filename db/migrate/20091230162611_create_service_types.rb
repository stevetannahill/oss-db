class CreateServiceTypes < ActiveRecord::Migration
  def self.up
    create_table :service_types do |t|
      t.string :name
      t.string :code
      t.integer :service_provider_id
      t.integer :max_mtu
      t.boolean :c_vlan_id_preservation
      t.boolean :c_vlan_cos_preservation
      t.string :color_marking
      t.string :service_category
      t.string :unicast_frame_delivery_conditions
      t.string :multicast_frame_delivery_conditions
      t.string :broadcast_frame_delivery_conditions

      t.timestamps
    end
  end

  def self.down
    drop_table :service_types
  end
end
