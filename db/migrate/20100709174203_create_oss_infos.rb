class CreateOssInfos < ActiveRecord::Migration
  def self.up
    create_table :oss_infos do |t|
      t.string :name
      t.date :completion_date
      t.integer :operator_network_type_id
      t.integer :cenx_contact_id
      t.integer :member_contact_id
      t.string :reference_documents
      t.string :notes

      t.timestamps
    end
  end

  def self.down
    drop_table :oss_infos
  end
end
