class CreateFayeSubscribeKeys < ActiveRecord::Migration
  def self.up
    create_table :faye_subscribe_keys do |t|
      t.string :channel
      t.string :key

      t.timestamps
    end
    add_index :faye_subscribe_keys, [:channel,:key], :unique => true
  end

  def self.down
    drop_table :faye_subscribe_keys
  end
end
