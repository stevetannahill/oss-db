class AddSiteReport < ActiveRecord::Migration
  class CosTestVector < ActiveRecord::Base
    before_save :set_protection_path_id
    before_save :set_ref_grid_circuit_id
    def set_protection_path_id
      if self.protection_path_id.nil?
        self.protection_path_id = primary ? 1 : 2
      end
      return true
    end

    def set_ref_grid_circuit_id
      # If there is a ref_circuit_id and it exists then set the ref_grid_curit_id to the ref CTV grid_id
      unless self.ref_circuit_id.nil?
        ref_ctv = CosTestVector.find_by_circuit_id(self.ref_circuit_id)
        if ref_ctv
          self.ref_grid_circuit_id = ref_ctv.grid_circuit_id
        end
      end
      return true
    end
    def primary
      if ('SpirentSprint' == self.circuit_id_format)
        split_id = self.circuit_id.split('-')
        if split_id.size == 6
          return 'P' == split_id[2]
        end
      end
      return false
    end
  end
  
  def up
    add_column :rollup_report_sites, :demarc_count, :integer
    add_column :rollup_report_sites, :minor_demarc_count, :integer
    add_column :rollup_report_sites, :major_demarc_count, :integer
    create_table :rollup_ids do |t|
      t.integer :cos_test_vector_id
      t.integer :grid_circuit_id
      t.integer :demarc_id
      t.integer :protection_path_id
      t.integer :site_id
    end
    CosTestVector.find_each do |ctv|
      save_needed = false
      if ctv.protection_path_id.nil?
        ctv.protection_path_id = ctv.primary ? 1 : 2
        save_needed = true
      end
      unless ctv.ref_circuit_id.nil?
        if ctv.ref_grid_circuit_id.nil?
          ref_ctv = CosTestVector.find_by_circuit_id(ctv.ref_circuit_id)
          if ref_ctv
            ctv.ref_grid_circuit_id = ref_ctv.grid_circuit_id
            save_needed = true
          end
        end
      end
      ctv.save if save_needed
    end
  end
  def down
    remove_column :rollup_report_sites, :demarc_count
    remove_column :rollup_report_sites, :minor_demarc_count
    remove_column :rollup_report_sites, :major_demarc_count
    drop_table :rollup_ids
  end
end
