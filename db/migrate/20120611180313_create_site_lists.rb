class CreateSiteLists < ActiveRecord::Migration
  def change
    create_table :site_lists, :force => true do |t|
      t.string :name
      t.references :user

      t.timestamps
    end

    add_index :site_lists, [:user_id, :name], :unique => true
  end
end
