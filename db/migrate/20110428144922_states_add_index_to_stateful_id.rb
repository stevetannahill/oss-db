class StatesAddIndexToStatefulId < ActiveRecord::Migration
  def self.up
    add_index :states, [:stateful_id, :timestamp]
  end

  def self.down
    remove_index :states, [:stateful_id, :timestamp]
  end
end
