class CreateNetworkManagementServers < ActiveRecord::Migration
  def self.up
    create_table :network_management_servers do |t|
      t.string :name
      t.integer :network_manager_id
      t.string :primary_ip

      t.timestamps
    end
  end

  def self.down
    drop_table :network_management_servers
  end
end
