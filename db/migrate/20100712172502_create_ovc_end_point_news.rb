class CreateOvcEndPointNews < ActiveRecord::Migration
  def self.up
    create_table :ovc_end_point_news do |t|
     # common attributes
     t.string :type
     t.string :cenx_id
     t.string :member_name
     t.integer :ovc_new_id
     t.integer :demarc_id
     t.integer :ovc_end_point_type_id
     t.string :md_format
     t.string :md_level
     t.string :md_name_ieee
     t.string :ma_format
     t.string :ma_name
     t.boolean :is_monitored
     t.string :notes

     # attributes for type=OvcEndPointEnni
     t.string :stags

     # attributes for type=OvcEndPointUni
     t.string :ctags

     # attributes for type=OvcEndPointEnni
     t.string :oper_state

     t.timestamps
    end
  end

  def self.down
    drop_table :ovc_end_point_news
  end
end
