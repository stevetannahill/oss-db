class AddCenxIdToCosTestVector < ActiveRecord::Migration
  def self.up
    add_column :cos_test_vectors, :cenx_id, :string
  end

  def self.down
    remove_column :cos_test_vectors, :cenx_id
  end
end
