class MemberOvc < OvcNew
end

class AddMtcHistoryToDemarcUniMemberOvc < ActiveRecord::Migration
  def self.up
    Demarc.all.each {|d|
      if d.mtc_periods.empty? && d.class == Demarc
        d.mtc_periods = [MtcHistory.create(:event_filter => "Mtc Demarc:#{d.cenx_id}")]
        if d.get_prov_name != "Maintenance"
          d.mtc_periods.first.alarm_records << AlarmRecord.create(:time_of_event => (Time.now.to_f*1000).to_i, :state => "Ok", :details => "Out of Maintenance", :original_state => "cleared")
        else
          d.mtc_periods.first.alarm_records << AlarmRecord.create(:time_of_event => (Time.now.to_f*1000).to_i, :state => "Maintenance", :details => "In Maintenance", :original_state => "maintenance")
        end
      end
    }
    
    Uni.all.each {|d|
      if d.mtc_periods.empty?
        d.mtc_periods = [MtcHistory.create(:event_filter => "Mtc UNI:#{d.cenx_id}")]
        if d.get_prov_name != "Maintenance"
          d.mtc_periods.first.alarm_records << AlarmRecord.create(:time_of_event => (Time.now.to_f*1000).to_i, :state => "Ok", :details => "Out of Maintenance", :original_state => "cleared")
        else
          d.mtc_periods.first.alarm_records << AlarmRecord.create(:time_of_event => (Time.now.to_f*1000).to_i, :state => "Maintenance", :details => "In Maintenance", :original_state => "maintenance")
        end        
      end
    }
    
    MemberOvc.all.each {|d|
      if d.mtc_periods.empty?
        d.mtc_periods = [MtcHistory.create(:event_filter => "Mtc OVC:#{d.cenx_id}")]
        if d.get_prov_name != "Maintenance"
          d.mtc_periods.first.alarm_records << AlarmRecord.create(:time_of_event => (Time.now.to_f*1000).to_i, :state => "Ok", :details => "Out of Maintenance", :original_state => "cleared")
        else
          d.mtc_periods.first.alarm_records << AlarmRecord.create(:time_of_event => (Time.now.to_f*1000).to_i, :state => "Maintenance", :details => "In Maintenance", :original_state => "maintenance")
        end
      end
    }        
      
  end

  def self.down
    Demarc.all.each {|d|
      if d.class == Demarc
        d.mtc_periods.destroy_all
      end
    }    
    
    Uni.all.each {|d| d.mtc_periods.destroy_all if !d.mtc_periods.empty?}
    MemberOvc.all.each {|d| d.mtc_periods.destroy_all if !d.mtc_periods.empty?} 
    
  end
end
