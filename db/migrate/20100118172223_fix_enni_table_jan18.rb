class FixEnniTableJan18 < ActiveRecord::Migration
  def self.up
    remove_column :ennis, :lldp
    remove_column :ennis, :stp
    remove_column :ennis, :rstp
    remove_column :ennis, :mstp
    remove_column :ennis, :evst
    remove_column :ennis, :rpvst
    remove_column :ennis, :eaps
    remove_column :ennis, :pause
    remove_column :ennis, :lacp
    remove_column :ennis, :garp
    remove_column :ennis, :port_auth
    remove_column :ennis, :threasholds
    remove_column :ennis, :lacp_priority_support
    remove_column :ennis, :lacp_supported
    remove_column :ennis, :lag_type
    remove_column :ennis, :lag_supported
    remove_column :ennis, :consistent_ethertype
    remove_column :ennis, :max_num_ovcs
    remove_column :ennis, :ag_supported
    remove_column :ennis, :ah_supported
    remove_column :ennis, :tx_power
    remove_column :ennis, :rx_power
    remove_column :ennis, :outer_tag_ovc_mapping
    remove_column :ennis, :mtu
    remove_column :ennis, :connected_device_type
    rename_column :ennis, :if_type, :port_type
    add_column :ennis, :sp_name, :string
    add_column :ennis, :is_protected, :boolean
    add_column :ennis, :exchange_id, :integer
    add_column :ennis, :enni_profile_id, :integer
    add_column :ennis, :cenx_cma, :string
  end

  def self.down
    add_column :ennis, :lldp, :string
    add_column :ennis, :stp, :string
    add_column :ennis, :rstp, :string
    add_column :ennis, :mstp, :string
    add_column :ennis, :evst, :string
    add_column :ennis, :rpvst, :string
    add_column :ennis, :eaps, :string
    add_column :ennis, :pause, :string
    add_column :ennis, :lacp, :string
    add_column :ennis, :garp, :string
    add_column :ennis, :port_auth, :string
    add_column :ennis, :threasholds, :string
    add_column :ennis, :lag_supported, :bool
    add_column :ennis, :lag_type, :string
    add_column :ennis, :lacp_supported, :bool
    add_column :ennis, :lacp_priority_support, :bool
    add_column :ennis, :consistent_ethertype, :bool
    add_column :ennis, :max_num_ovcs, :integer
    add_column :ennis, :ah_supported, :bool
    add_column :ennis, :ag_supported, :bool
    add_column :ennis, :tx_power, :float
    add_column :ennis, :rx_power, :float
    add_column :ennis, :outer_tag_ovc_mapping, :boolean
    add_column :ennis, :mtu, :integer
    add_column :ennis, :connected_device_type, :string
    rename_column :ennis, :port_type, :if_type
    remove_column :ennis, :sp_name
    remove_column :ennis, :exchange_id
    remove_column :ennis, :is_protected
    remove_column :ennis, :enni_profile_id
    remove_column :ennis, :cenx_cma
  end
end
