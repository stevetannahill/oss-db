class CreateClassOfServices < ActiveRecord::Migration
  def self.up
    create_table :class_of_services do |t|
      t.string :name
      t.integer :cos_value
      t.integer :service_type_id

      t.timestamps
    end
  end

  def self.down
    drop_table :class_of_services
  end
end
