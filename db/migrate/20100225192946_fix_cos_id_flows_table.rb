class FixCosIdFlowsTable < ActiveRecord::Migration
  def self.up
    add_column :cosid_flows, :cbs_B, :integer
    add_column :cosid_flows, :ebs_B, :integer
    rename_column :cosid_flows, :cenx_ovc_end_point_id, :ovc_id
  end

  def self.down
    remove_column :cosid_flows, :cbs_B
    remove_column :cosid_flows, :ebs_B
    rename_column :cosid_flows, :ovc_id, :cenx_ovc_end_point_id
  end
end
