
class AlarmRecord_28_09_2011_10_10 < ActiveRecord::Base
  set_table_name 'event_records'
  self.inheritance_column = :_type_disabled
end

class SmHistory_28_09_2011_10_10 < ActiveRecord::Base  
  set_table_name 'event_histories'
  self.inheritance_column = :_type_disabled
  serialize :alarm_mapping
end

class RenameStates < ActiveRecord::Migration
  def self.up
    AlarmRecord_28_09_2011_10_10.find_all_by_type("AlarmRecord").each do |ar|
      if ar.original_state == "not_applicable"
        ar.state = "Not Live"
        ar.original_state = "not_live"
        ar.save
      elsif ar.original_state == "indeterminate"
        ar.state = "Dependency"
        ar.original_state = "dependency"
        ar.save
      end
    end
    new_mappings = {"ok" => "Ok",
            "warning" => "Warning",
            "failed" => "Failed",
            "unavailable" => "Unavailable",
            "dependency" => "Dependency",
            "maintenance" => "Maintenance",
            "not_monitored" => "Not Monitored",
            "not_live" => "Not Live",
            "No State" => "No State"
           }
    SmHistory_28_09_2011_10_10.find_all_by_type("SmHistory").each do |smh|
      smh.alarm_mapping = new_mappings
      smh.save
    end
    
  end

  def self.down
    AlarmRecord_28_09_2011_10_10.find_all_by_type("AlarmRecord").each do |ar|
      if ar.original_state == "not_live"
        ar.state = "Not Applicable"
        ar.original_state = "not_applicable"
        ar.save
      elsif ar.original_state == "dependency"
        ar.state = "Indeterminate"
        ar.original_state = "indeterminate"
        ar.save
      end
    end
    old_mappings = {"ok" => "Ok",
            "warning" => "Warning",
            "failed" => "Failed",
            "unavailable" => "Unavailable",
            "indeterminate" => "Indeterminate",
            "maintenance" => "Maintenance",
            "not_monitored" => "Not Monitored",
            "not_applicable" => "Not Applicable",
            "No State" => "No State"
           }
    SmHistory_28_09_2011_10_10.find_all_by_type("SmHistory").each do |smh|
      smh.alarm_mapping = old_mappings
      smh.save
    end
  end
end
