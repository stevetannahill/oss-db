class UpdatePortSeverityMappings < ActiveRecord::Migration
  def self.up
    # All port alarms will map minor, warning => Unavailable
    ahs = EnniNew.all.collect {|enni| enni.alarm_histories.collect {|ah| ah if ah.event_filter[/daughterCard:port-/]}}.flatten.compact
    ahs.each do |ah|
      ah.alarm_mapping["minor"] = AlarmSeverity::UNAVAILABLE
      ah.alarm_mapping["warning"] = AlarmSeverity::UNAVAILABLE
      ah.save
    end
  end

  def self.down
    ahs = EnniNew.all.collect {|enni| enni.alarm_histories.collect {|ah| ah if ah.event_filter[/daughterCard:port-/]}}.flatten.compact
    ahs.each do |ah|
      ah.alarm_mapping["minor"] = AlarmSeverity::WARNING
      ah.alarm_mapping["warning"] = AlarmSeverity::WARNING
      ah.save
    end
  end
end
