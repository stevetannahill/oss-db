class CreateRoles < ActiveRecord::Migration
  def up
    create_table :roles, :force => true do |t|
      t.string :name, {:length => 256, :null => false}

      t.timestamps
    end
    
    add_index :roles, :name, :unique => true
    
    unless Role.exists?
      Role.create(:name => 'User Management')
      Role.create(:name => 'Network')
      Role.create(:name => 'Inventory')
      Role.create(:name => 'Monitoring')
      Role.create(:name => 'Ordering')
      Role.create(:name => 'Reports')
    end
  end
  
  def down
    drop_table :roles
  end
end