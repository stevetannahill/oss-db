class SmallChangesToExlsAndEvcs < ActiveRecord::Migration
  def self.up
    change_column :exchange_locations, :notes, :text
    add_column :exchange_locations, :node_prefix, :string
    remove_column :evcs, :sp_name
  end

  def self.down
    change_column :exchange_locations, :notes, :string
    remove_column :exchange_locations, :node_prefix
    add_column :evcs, :sp_name, :string
  end
end
