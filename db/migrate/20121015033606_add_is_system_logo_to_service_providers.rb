class AddIsSystemLogoToServiceProviders < ActiveRecord::Migration
  def change
    add_column :service_providers, :is_system_logo, :boolean, :default => false
  end
end
