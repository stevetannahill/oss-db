class FixMetricsCosTestVectorKey < ActiveRecord::Migration
  def up
    remove_index(:metrics_cos_test_vectors, :name =>:metrics_cos_test_vector_search)
    add_index(:metrics_cos_test_vectors, [:grid_circuit_id, :metric_type, :measurement_type, :period_type, :period_start_epoch], :unique => true, :name => :metrics_cos_test_vector_search)
    change_column(:metrics_cos_test_vectors, :metric_type, "ENUM('availability', 'flr', 'fd', 'fdv')")
  end

  def down
    change_column(:metrics_cos_test_vectors, :metric_type, "ENUM('avail', 'flr', 'fd', 'fdv')")
    remove_index(:metrics_cos_test_vectors, :name =>:metrics_cos_test_vector_search)
    add_index(:metrics_cos_test_vectors, [:grid_circuit_id, :period_start_epoch], :unique => true, :name => :metrics_cos_test_vector_search)
  end
end
