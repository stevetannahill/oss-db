class OvcNew < InstanceElement
end

class SinEvcServiceProviderSphinx < ActiveRecord::Base
end

class Evc < InstanceElement
  self.inheritance_column = :_type_disabled
  belongs_to :operator_network
  has_one :service_provider, :through => :operator_network
end

class OvcNew < InstanceElement
  self.inheritance_column = :_type_disabled
  has_and_belongs_to_many :evcs
  belongs_to :operator_network
  has_one :service_provider, :through => :operator_network
  belongs_to :exchange
  
  def service_provider
    if type == "CenxOvcNew"
      on = exchange_id ? exchange.cenx_operator_network : nil
    else
      on = operator_network_id ? operator_network : nil
    end
    
    (on) ? on.service_provider : nil
  end
end
    
class CreateSinEvcServiceProviderSphinxes < ActiveRecord::Migration
  def self.up
    create_table :sin_evc_service_provider_sphinxes do |t|
      t.integer :evc_id
      t.integer :service_provider_id

      t.timestamps
    end

    add_index :sin_evc_service_provider_sphinxes, [:evc_id, :service_provider_id], :name => 'sin_evc_service_provider_unique', :unique => true

    # Populdate access table with all existing evcs
    evcs = Evc.find(:all)
    evcs.each do |evc|
      SinEvcServiceProviderSphinx.find_or_create_by_evc_id_and_service_provider_id(evc.id, evc.service_provider.id)
    end

    # Populdate access table with all existing ovcs
    ovcs = OvcNew.find(:all)
    ovcs.each do |ovc|
      ovc.evcs.each do |evc|
        SinEvcServiceProviderSphinx.find_or_create_by_evc_id_and_service_provider_id(evc.id, ovc.service_provider.id)
      end
    end
    
  end

  def self.down
    drop_table :sin_evc_service_provider_sphinxes
  end
end
