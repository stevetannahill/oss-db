class AddRollupReportSiteGroups < ActiveRecord::Migration
  def up
    #add some new columns to the demarcs report table column
    add_column :rollup_report_demarcs, :protection_path_count, :integer
    add_column :rollup_report_demarcs, :path_minor_count, :integer
    add_column :rollup_report_demarcs, :path_major_count, :integer

    create_table :rollup_report_site_groups do |t|
      t.integer :period_start_epoch
      t.integer :site_group_id
      t.string  :severity #"ENUM('clear', 'minor', 'major', 'critical')"  # high watermark of underlying site severities.

      #breakdown by site
      t.integer :site_count
      t.integer :site_minor_count
      t.integer :site_major_count

      #breakdown by demarc
      t.integer :demarc_count
      t.integer :demarc_minor_count
      t.integer :demarc_major_count

      #breakdown by metric
      t.integer :metric_count
      t.integer :metric_minor_count
      t.integer :metric_major_count
    end
  end

  def down
    drop_table :rollup_report_site_groups
    
    remove_column :rollup_report_demarcs, :path_major_count
    remove_column :rollup_report_demarcs, :path_minor_count
    remove_column :rollup_report_demarcs, :protection_path_count
  end
end
