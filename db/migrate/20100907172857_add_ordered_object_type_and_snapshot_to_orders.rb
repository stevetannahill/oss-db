class AddOrderedObjectTypeAndSnapshotToOrders < ActiveRecord::Migration
  def self.up
    add_column :service_orders, :ordered_object_type_id, :integer
    add_column :service_orders, :ordered_object_type_type, :string
    add_column :service_orders, :ordered_entity_snapshot, :text
  end

  def self.down
    remove_column :service_orders, :ordered_object_type_id
    remove_column :service_orders, :ordered_object_type_type
    remove_column :service_orders, :ordered_entity_snapshot
  end
end
