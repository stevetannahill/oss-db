class UpdateAlarmMappings < ActiveRecord::Migration
  def self.up
    AlarmHistory.all.each {|ah|
      if ah.event_filter["mep-"] != nil
        tmp = AlarmSeverity::default_alu_alarm_mapping
        tmp["major"] = AlarmSeverity::WARNING
        ah.alarm_mapping = tmp
        ah.save
      else
        tmp = AlarmSeverity::default_alu_alarm_mapping
        ah.alarm_mapping = tmp
        ah.save
      end
    }
    MtcHistory.all.each {|ah|
      tmp = AlarmSeverity::default_alu_alarm_mapping
      ah.alarm_mapping = tmp
      ah.save
    }
  end

  def self.down
    AlarmHistory.all.each {|ah|
      if ah.event_filter["mep-"] != nil
        tmp = AlarmSeverity::default_alu_alarm_mapping
        tmp["major"] = AlarmSeverity::WARNING
        ah.alarm_mapping = tmp
        ah.save
      else
        tmp = AlarmSeverity::default_alu_alarm_mapping
        ah.alarm_mapping = tmp
        ah.save
      end
    }
    MtcHistory.all.each {|ah|
      tmp = AlarmSeverity::default_alu_alarm_mapping
      ah.alarm_mapping = tmp
      ah.save
    }
  end
end
