class AddEnniStagResTextFieldwq < ActiveRecord::Migration
  def self.up
    add_column :ennis, :stag_reservation_notes, :string
  end

  def self.down
    remove_column :ennis, :stag_reservation_notes
  end
end
