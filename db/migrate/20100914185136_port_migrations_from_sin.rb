class PortMigrationsFromSin < ActiveRecord::Migration
  def self.up
    # indices added to database for retrieval performance
    add_index :evcs, :operator_network_id
    add_index :ovc_news, :evc_id
    add_index :ovc_news, :operator_network_id
    add_index :ovc_news, :exchange_id
    add_index :cos_instances, :class_of_service_type_id
    add_index :cos_instances, :ovc_new_id
    add_index :bw_profile_types, :class_of_service_type_id
    add_index :cos_end_points, :ovc_end_point_new_id
    add_index :operator_networks, :service_provider_id
    add_index :ovc_end_point_news, :ovc_new_id
    add_index :ovc_end_point_news, :demarc_id
    add_index :member_attrs, :affected_entity_id
    add_index :member_attr_instances, :member_attr_id
    add_index :member_attr_instances, :affected_entity_id
    add_index :member_attr_instances, :owner_id

    # adding technical contact back to service_orders
    add_column :service_orders, :technical_contact_id, :integer
  end

  def self.down
    remove_index :evcs, :operator_network_id
    remove_index :ovc_news, :evc_id
    remove_index :ovc_news, :operator_network_id
    remove_index :ovc_news, :exchange_id
    remove_index :cos_instances, :class_of_service_type_id
    remove_index :cos_instances, :ovc_new_id
    remove_index :bw_profile_types, :class_of_service_type_id
    remove_index :cos_end_points, :ovc_end_point_new_id
    remove_index :operator_networks, :service_provider_id
    remove_index :ovc_end_point_news, :ovc_new_id
    remove_index :ovc_end_point_news, :demarc_id
    remove_index :member_attrs, :affected_entity_id
    remove_index :member_attr_instances, :member_attr_id
    remove_index :member_attr_instances, :affected_entity_id
    remove_index :member_attr_instances, :owner_id
    remove_column :service_orders, :technical_contact_id
  end
end
