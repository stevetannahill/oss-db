class ChangeEnniCirCacLimitTo100 < ActiveRecord::Migration
  def self.up
    change_column_default :demarcs, :cir_limit, 100

    Demarc.all.each do |demarc|
      demarc.cir_limit = 100
      demarc.save(:validate => false)
    end
  end

  def self.down
  end
end
