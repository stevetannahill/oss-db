class CreatePrivilegesRoles < ActiveRecord::Migration
  def up
    create_table :privileges_roles, :force => true, :id => false do |t|
      t.integer :privilege_id
      t.integer :role_id
    end
    
    add_index(:privileges_roles, [:privilege_id, :role_id], :unique => true)    
  end

  def down
    drop_table :privileges_roles
  end
end
