class EvcProvStateful < ProvStateful
end
class ProvDeletedEvc < ProvDeleted
end
class ProvLiveEvc < ProvLive
end
class ProvMaintenanceEvc < ProvMaintenance
end
class ProvPendingEvc < ProvPending
end
class ProvReadyEvc < ProvReady
end
class ProvTestingEvc < ProvTesting
end


class RenameEvcToPath < ActiveRecord::Migration
  def up
    add_column :evcs, :type, :string, :default => "Evc"
    change_column_default :evcs, :type, nil

    rename_column :evc_displays, :evc_id, :path_id
    rename_column :evc_types, :evc_type, :path_type
    rename_column :evcs, :cenx_evc_type, :cenx_path_type
    rename_column :evcs, :evc_type_id, :path_type_id
    rename_column :evcs_segments, :evc_id, :path_id
    rename_column :segments, :evc_network_id, :path_network_id
    rename_column :service_orders, :evc_id, :path_id
    rename_column :sin_evc_service_provider_sphinxes, :evc_id, :path_id
    rename_column :evcs_site_lists, :evc_id, :path_id

    rename_table :evc_displays, :path_displays
    rename_table :evc_types, :path_types
    rename_table :evcs, :paths
    rename_table :evcs_segments, :paths_segments
    rename_table :sin_evc_service_provider_sphinxes, :sin_path_service_provider_sphinxes 
    rename_table :evcs_site_lists, :paths_site_lists

    PathDisplay.reset_column_information
    PathType.reset_column_information
    Path.reset_column_information
    Segment.reset_column_information
    ServiceOrder.reset_column_information
    SinPathServiceProviderSphinx.reset_column_information

    ServiceOrder.update_all({:ordered_entity_type => "Path"}, "ordered_entity_type = 'Evc'")

    StatefulOwnership.update_all({:stateable_type => "Path"}, "stateable_type = 'Evc'")
    EventOwnership.update_all({:eventable_type => "Path"}, "eventable_type = 'Evc'")
    MtcOwnership.update_all({:mtcable_type => "Path"}, "mtcable_type = 'Evc'")
    SmOwnership.update_all({:smable_type => "Path"}, "smable_type = 'Evc'")
    KpiOwnership.update_all({:kpiable_type => "Path"}, "kpiable_type = 'Evc'")
    KpiOwnership.update_all({:parent_type => "Path"}, "parent_type = 'Evc'")

    MemberAttr.update_all({:affected_entity_type => "Path"}, "affected_entity_type = 'Evc'")
    MemberAttrInstance.update_all({:affected_entity_type => "Path"}, "affected_entity_type = 'Evc'")
    
    EvcProvStateful.update_all(:type => "PathProvStateful")
    ProvDeletedEvc.update_all(:type => "ProvDeletedPath")
    ProvLiveEvc.update_all(:type => "ProvLivePath")
    ProvMaintenanceEvc.update_all(:type => "ProvMaintenancePath")
    ProvPendingEvc.update_all(:type => "ProvPendingPath")
    ProvReadyEvc.update_all(:type => "ProvReadyPath")
    ProvTestingEvc.update_all(:type => "ProvTestingPath")

  end

  def down
  end
end
