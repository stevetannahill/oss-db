class DataArchive < ActiveRecord::Base
  has_many :network_manager
end

class AddShortDsTableAndLinkToNetworkManagers < ActiveRecord::Migration
  def self.up
    create_table :data_archives do |t|
      t.string :adapter
      t.string :host
      t.string :username
      t.string :password
    end
    DataArchive.create :adapter  => "mysql", :host     => "ds1.cenx.localnet", :username => "sam_archive", :password => "toTo112358"
    DataArchive.create :adapter  => "mysql", :host     => "ds2.cenx.localnet", :username => "sam_archive", :password => "toTo112358"
    add_column :network_managers, :data_archive_id, :integer, :default => 1

    execute "UPDATE `network_managers` SET `data_archive_id`=2 WHERE `name`='5620 SAM - Lab'"
    execute "UPDATE `network_managers` SET `data_archive_id`=2 WHERE `name`='BrixWorx - Lab'"

  end

  def self.down
    drop_column :network_managers, :data_archive_id
    drop_table :data_archives
  end
end

