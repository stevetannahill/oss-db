class ChangeLs7450ExchangeTypeToLs7450Tap < ActiveRecord::Migration

  class Exchange < ActiveRecord::Base
  end
  
   def self.up
    conversion = {
      "LightSquaredExchange7450" => "LightSquaredExchange7450_TAP"
    }

    Exchange.reset_column_information
    Exchange.all.each do |ex|
      next if conversion[ex.exchange_type].nil?
      ex.exchange_type = conversion[ex.exchange_type]
      ex.save false
    end

  end

  def self.down
  end
end
