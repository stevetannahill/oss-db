class Stateful_08_02_2012_11_11 < ActiveRecord::Base
  set_table_name 'statefuls'
  set_inheritance_column :_type_disabled
end

class State_08_02_2012_11_11 < ActiveRecord::Base
  set_table_name 'states'
  set_inheritance_column :_type_disabled  
  belongs_to :stateful
end

class ServiceOrder_08_02_2012_11_11 < ActiveRecord::Base
  set_table_name 'service_orders'
  set_inheritance_column :_type_disabled
end

class DenormaliseOrderStateToOrder < ActiveRecord::Migration
  def up
    
    [:service_orders].each do |table_name|
      add_column table_name, :order_name, :string
      add_column table_name, :order_notes, :text
      add_column table_name, :order_timestamp, :bigint
    end
    
    add_index :service_orders, :title
    add_index :service_orders, :cenx_id
           
    Stateful_08_02_2012_11_11.reset_column_information
    State_08_02_2012_11_11.reset_column_information 
    ServiceOrder_08_02_2012_11_11.reset_column_information
    
    ServiceOrder_08_02_2012_11_11.all.each do |object|      
      statefuls = Stateful_08_02_2012_11_11.joins("INNER JOIN `stateful_ownerships` ON `statefuls`.`id` = `stateful_ownerships`.`stateful_id`").
               where("`stateful_ownerships`.`stateable_id` = ? AND `stateful_ownerships`.`stateable_type` = ?", object.id, object.class.to_s.sub("_08_02_2012_11_11",""))            
      statefuls.each do |s|
        if s.name == "Ordering"
          state = State_08_02_2012_11_11.find_all_by_stateful_id(s.id).last
          #s.states.last
          object.update_attributes(:order_name => state.name, :order_notes => state.notes, :order_timestamp => (state.timestamp.to_f*1000).to_i)
        end
      end
    end
  end
  
  def down
    remove_column :service_orders, :order_name
    remove_column :service_orders, :order_notes
    remove_column :service_orders, :order_timestamp
    
    remove_index :service_orders, :title
    remove_index :service_orders, :cenx_id
  end
end
