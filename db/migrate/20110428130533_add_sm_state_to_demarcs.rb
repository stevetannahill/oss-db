class AddSmStateToDemarcs < ActiveRecord::Migration
  def self.up
    Demarc.all.each {|d| d.sm_histories << SmHistory.create(:event_filter => "SM Demarc:#{d.cenx_id}") if d.sm_histories.empty? &&  d.class == Demarc}
  end

  def self.down
    Demarc.all.each {|d| d.sm_histories.destroy_all if (!d.sm_histories.empty?) &&  d.class == Demarc}
  end
end
