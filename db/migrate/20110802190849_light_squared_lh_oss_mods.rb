class LightSquaredLhOssMods < ActiveRecord::Migration
  def self.up
    add_column :ovc_types, :is_frame_aware, :boolean, :default => true
  end

  def self.down
    remove_column :ovc_types, :is_frame_aware
  end
end
