class ExceptionCleanup < ActiveRecord::Migration
  def up
    create_table :exception_sla_cos_test_vectors do |t|
      t.integer :time_declared
      t.float   :value
      t.column  :severity, "ENUM('critical', 'major', 'minor')"
      t.column  :cos_test_vector_id, :integer
      t.column  :sla_type, "ENUM('availability', 'flr', 'fd', 'fdv')"
      t.column  :time_over, :integer # used for 'major' or 'minor' exceptions
    end
    create_table :exception_utilization_ennis do |t|
      t.integer :time_declared
      t.float   :value
      t.column  :severity, "ENUM( 'major', 'minor')"
      t.integer :enni_new_id
      t.column  :direction, "ENUM('ingress', 'egress')"
      t.column  :time_over, :integer
      t.column  :average_rate_while_over_threshold_mbps, :float
    end
    create_table :exception_utilization_cos_end_points do |t|
      t.integer :time_declared
      t.float   :value
      t.column  :severity, "ENUM( 'major', 'minor')"
      t.integer :cos_end_point_id
      t.column  :direction, "ENUM('ingress', 'egress')"
      t.column  :time_over, :integer
      t.column  :average_rate_while_over_threshold_mbps, :float
    end

  end

  def down
    drop_table :exception_sla_cos_test_vectors
    drop_table :exception_utilization_ennis
    drop_table :exception_utilization_cos_end_points
  end

end