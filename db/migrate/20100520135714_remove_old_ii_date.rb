class RemoveOldIiDate < ActiveRecord::Migration
  def self.up
    remove_column :inventory_items, :date
  end

  def self.down
    add_column :inventory_items, :date, :string
  end
end
