class AddTypeToBuilderLog < ActiveRecord::Migration
  class AutomatedBuildLog <  ActiveRecord::Base
  end
  
  def up
    add_column :builder_logs, :type, :string
    rename_table :builder_logs, :automated_build_logs
    AutomatedBuildLog.update_all(:type => "BuilderLog")
  end
  
  def down
    remove_column :automated_build_logs, :type
    rename_table :automated_build_logs, :builder_logs
  end
  
end
