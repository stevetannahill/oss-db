class FixUpMemberServiceOrderSchema < ActiveRecord::Migration
  def self.up
    rename_column :member_service_order_attrs, :attr_value_type, :attr_syntax
    rename_column :member_service_order_attr_groups, :operartor_network_type_id, :operator_network_type_id
    remove_column :member_service_order_attrs, :operartor_network_type_id

    drop_table :member_service_order_attr_groups_member_service_order_attrs
  end

  def self.down
    rename_column :member_service_order_attrs, :attr_syntax, :attr_value_type
    rename_column :member_service_order_attr_groups, :operator_network_type_id, :operartor_network_type_id
    add_column :member_service_order_attrs, :operartor_network_type_id, :integer

    create_table :member_service_order_attr_groups_member_service_order_attrs, :id => false do |t|
      t.integer :member_service_order_attr_group_id
      t.integer :member_service_order_attr_id
    end

  end
end
