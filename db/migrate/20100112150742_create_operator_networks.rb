class CreateOperatorNetworks < ActiveRecord::Migration
  def self.up
    create_table :operator_networks do |t|
      t.string :name
      t.integer :service_provider_id
      t.boolean :is_processing
      t.string :job_type
      t.integer :job_id

      t.timestamps
    end
  end

  def self.down
    drop_table :operator_networks
  end
end
