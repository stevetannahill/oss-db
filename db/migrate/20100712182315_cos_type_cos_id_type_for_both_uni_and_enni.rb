class CosTypeCosIdTypeForBothUniAndEnni < ActiveRecord::Migration
  def self.up
    rename_column :class_of_service_types, :cos_id_type, :cos_id_type_enni
    add_column :class_of_service_types, :cos_id_type_uni, :string, :default => "pcp"
  end

  def self.down
    rename_column :class_of_service_types, :cos_id_type_enni, :cos_id_type
    remove_column :class_of_service_types, :cos_id_type_uni
  
  end
end
