class AddShortNameToNodes < ActiveRecord::Migration
  def self.up
    add_column :nodes, :short_name, :string
    add_column :nodes, :cli_prompt, :string
  end

  def self.down
    remove_column :nodes, :cli_prompt
    remove_column :nodes, :short_name
  end
end
