class CleanOvcEndPointForDemarc < ActiveRecord::Migration
  def self.up
    remove_column :ovc_end_points, :mon_loopback_address
    remove_column :ovc_end_points, :address
    remove_column :ovc_end_points, :end_user_name
    remove_column :ovc_end_points, :is_new_construction
    remove_column :ovc_end_points, :uni_speed
    remove_column :ovc_end_points, :contact_info
    remove_column :ovc_end_points, :nid_type
    remove_column :ovc_end_points, :nid_sw_version
    remove_column :ovc_end_points, :reflection_mechanism
    remove_column :ovc_end_points, :uni_profile_id
    add_column :ovc_end_points, :demarc_id, :integer
  end

  def self.down
    add_column :ovc_end_points, :mon_loopback_address, :string
    add_column :ovc_end_points, :address, :string
    add_column :ovc_end_points, :end_user_name, :string
    add_column :ovc_end_points, :is_new_construction, :boolean
    add_column :ovc_end_points, :uni_speed, :string
    add_column :ovc_end_points, :contact_info, :string
    add_column :ovc_end_points, :nid_type, :string
    add_column :ovc_end_points, :nid_sw_version, :string
    add_column :ovc_end_points, :reflection_mechanism, :string
    add_column :ovc_end_points, :uni_profile_id, :integer
    remove_column :ovc_end_points, :demarc_id
  end
end
