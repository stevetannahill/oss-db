class AddLatLongToExchangeAndDemarc < ActiveRecord::Migration
  def change
    add_column :demarcs, :latitude, :decimal, :precision => 9, :scale => 6, :default => 0
    add_column :demarcs, :longitude, :decimal, :precision => 9, :scale => 6, :default => 0
    add_column :exchanges, :latitude, :decimal, :precision => 9, :scale => 6, :default => 0
    add_column :exchanges, :longitude, :decimal, :precision => 9, :scale => 6, :default => 0
  end
end
