class CosTestVectorEvolve < ActiveRecord::Migration
  def self.up
    add_column :cos_test_vectors, :delay_error_threshold, :string
    add_column :cos_test_vectors, :dv_error_threshold, :string
    add_column :cos_test_vectors, :delay_warning_threshold, :string
    add_column :cos_test_vectors, :dv_warning_threshold, :string
    remove_column :nodes, :uname
    remove_column :nodes, :password
    remove_column :nodes, :short_name
    rename_column :cos_end_points, :cir_kbps, :ingress_cir_kbps
    rename_column :cos_end_points, :eir_kbps, :ingress_eir_kbps
    rename_column :cos_end_points, :cbs_kB, :ingress_cbs_kB
    rename_column :cos_end_points, :ebs_kB, :ingress_ebs_kB
    add_column :cos_end_points, :egress_cir_kbps, :string
    add_column :cos_end_points, :egress_eir_kbps, :string
    add_column :cos_end_points, :egress_cbs_kB, :string
    add_column :cos_end_points, :egress_ebs_kB, :string
    remove_column :cos_end_points, :direction
    remove_column :cos_end_points, :bw_profile_type_id
  end

  def self.down
    remove_column :cos_test_vectors, :delay_error_threshold
    remove_column :cos_test_vectors, :dv_error_threshold
    remove_column :cos_test_vectors, :delay_warning_threshold
    remove_column :cos_test_vectors, :dv_warning_threshold
    add_column :nodes, :uname, :string
    add_column :nodes, :password, :string
    add_column :nodes, :short_name, :string
    rename_column :cos_end_points, :ingress_cir_kbps, :cir_kbps
    rename_column :cos_end_points, :ingress_eir_kbps, :eir_kbps
    rename_column :cos_end_points, :ingress_cbs_kB, :cbs_kB
    rename_column :cos_end_points, :ingress_ebs_kB, :ebs_kB
    remove_column :cos_end_points, :egress_cir_kbps
    remove_column :cos_end_points, :egress_eir_kbps
    remove_column :cos_end_points, :egress_cbs_kB
    remove_column :cos_end_points, :egress_ebs_kB
    add_column :cos_end_points, :direction, :string
    add_column :cos_end_points, :bw_profile_type_id, :integer
  end
end