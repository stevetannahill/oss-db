class ChangeServiceIdToString < ActiveRecord::Migration
  def up
    change_column :segments, :service_id, :string
  end

  def down
    change_column :segments, :service_id, :integer
  end
end
