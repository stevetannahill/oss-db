
class CreateUsers < ActiveRecord::Migration

  require 'digest/sha1'
  class User < ActiveRecord::Base    

    belongs_to :service_provider
    belongs_to :privilege
    belongs_to :contact
    has_and_belongs_to_many :operator_network_types
    has_many :scheduled_tasks
    has_many :filter_templates
    has_many :site_lists
    delegate :roles, :to => :privilege

    default_scope :include => [:service_provider, :privilege]
    accepts_nested_attributes_for :operator_network_types

    before_validation :generate_salt
    validates_presence_of :email, :service_provider, :privilege
    validates_presence_of :password, :on => :create  
    validates_presence_of :operator_network_types, :unless => Proc.new { |user| user.system_admin? }
    validates_uniqueness_of :email
    validates_confirmation_of :password
    validates_length_of :password, :within => 6..20, :allow_blank => false, :on => :create
    validates :email, format: {with: /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/ }

    attr_accessor :password, :password_confirmation, :current_password
    # This is a list of the attributes a user is allowed to change through a mass update.
    # Never add something like service_provider_id or salt here
    attr_accessible :first_name, :last_name, :email, :homepage, :default_mode, :service_provider_id, :privilege_id, :operator_network_type_ids, :password, :password_confirmation, :current_password
    before_save  :encrypt_password
    before_destroy :check_system_admin

    def general_after_initialize
      return unless new_record?
      self.service_provider = @current_user.service_provider unless @current_user.nil?
    end


    def display_name
      "#{first_name} #{last_name}" rescue "User"
    end

    def self.authenticate(email, password)
      # not used at the moment but will be for password change
      u = find_by_email(email) # need to get the salt
      u = u && u.authenticated?(password) ? u : nil
    end

    def self.encrypt(password, salt)
      Digest::SHA1.hexdigest("--#{salt}--#{password}--")
    end

    def self.sodium(email)
      Digest::SHA1.hexdigest("--#{Time.now.to_s}--#{email}--")
    end

    def authenticated?(password)
      crypted_password == self.class.encrypt(password,salt)
    end

    def quick_set!(service_provider_id, email, password, *role_ids)
      self.service_provider_id = service_provider_id
      self.email = email
      self.password = password
      self.save!
    end

    def system_admin?
      service_provider && service_provider.is_system_owner? && operator_network_types.empty?
    end
    alias_method :is_admin?, :system_admin?

    def has_role?(role_sym)
      roles.any? { |r| r.name.downcase.underscore.to_sym == role_sym }
    end

    def homepage_url(request)
      "#{request.protocol}#{request.host_with_port}#{self.homepage}"
    end

    def initialize_settings
      self.homepage      ||= '/'
      self.hidden_tables ||= []
      self.default_mode  ||= 'inventory'
    end

    protected

    def generate_salt
      self.salt = self.class.sodium(email) if new_record? and salt.blank?
    end

    def encrypt_password
      self.crypted_password = self.class.encrypt(password,salt) unless password.blank?
    end

    private

    def check_system_admin
      if self.system_admin?
        errors.add(:base, "Cannot remove a system administrator")
        return false
      end
    end

  end


  
  def up
    create_table :users, :force => true do |t|
      t.string :email, :null => false
      t.string :first_name
      t.string :last_name
      t.string :crypted_password, :limit => 40
      t.string :salt, :limit => 40
      t.string :remember_token
      t.string :phone
      
      t.references :service_provider
      t.references :privilege
      t.timestamps
    end
    
    add_index :users, :email, :unique => true
    add_index :users, :service_provider_id
    add_index :users, :privilege_id
    
    unless User.exists?(:email => 'admin@cenx.com')
      user = User.create(:email => 'admin@cenx.com', :first_name => 'CENX', :last_name => 'Admin', :password => 'cozy@202B', :password_confirmation => 'cozy@202B')
      
      privilege = Privilege.find_or_create_by_name('Admin')
      user.privilege = privilege
      user.privilege.roles << Role.all
      user.service_provider = ServiceProvider.where(:is_system_owner => true).first if ServiceProvider.respond_to?(:find_by_is_system_owner)
      user.service_provider = ServiceProvider.where(:is_cenx => true).first if ServiceProvider.respond_to?(:find_by_is_cenx)
      
      user.save
    end
  end

  def down
    drop_table :users
  end
end
