class CreateBandwidthProfiles < ActiveRecord::Migration
  def self.up
    create_table :bandwidth_profiles do |t|
      t.string :bwp_type
      t.integer :min_cir_kbps
      t.integer :max_cir_kbps
      t.integer :min_cbs_kb
      t.integer :max_cbs_kb
      t.integer :min_eir_kbps
      t.integer :max_eir_kbps
      t.integer :min_ebs_kb
      t.integer :max_ebs_kb
      t.string :color_mode
      t.string :coupling_flag
      t.integer :class_of_service_id

      t.timestamps
    end
  end

  def self.down
    drop_table :bandwidth_profiles
  end
end
