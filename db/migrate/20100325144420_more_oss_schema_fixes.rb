class MoreOssSchemaFixes < ActiveRecord::Migration
  def self.up
    add_column :enni_profiles, :tx_power_lo, :string
    add_column :enni_profiles, :tx_power_hi, :string
    add_column :enni_profiles, :rx_power_lo, :string
    add_column :enni_profiles, :rx_power_hi, :string
    add_column :enni_profiles, :auto_negotiate, :boolean
    add_column :enni_profiles, :other_ether_type_notes, :string
    rename_column :enni_profiles , :optical_if_info, :additional_phy_notes
    add_column :enni_profiles , :multi_link_support, :boolean
    add_column :enni_profiles , :max_links_supported, :string
    add_column :enni_profiles , :mtu_notes, :string
    add_column :enni_profiles , :max_num_ovcs_notes, :string
    add_column :enni_profiles , :lamp, :string
    add_column :enni_profiles , :link_oam, :string
    add_column :enni_profiles , :e_lmi, :string
    add_column :enni_profiles , :mrp_b, :string
    add_column :enni_profiles , :cisco_bpdu, :string
    add_column :enni_profiles , :default_l2cp, :string
    add_column :enni_profiles , :stp_tagged, :string
    add_column :enni_profiles , :rstp_tagged, :string
    add_column :enni_profiles , :mstp_tagged, :string
    add_column :enni_profiles , :pause_tagged, :string
    add_column :enni_profiles , :lacp_tagged, :string
    add_column :enni_profiles , :lamp_tagged, :string
    add_column :enni_profiles , :link_oam_tagged, :string
    add_column :enni_profiles , :port_auth_tagged, :string
    add_column :enni_profiles , :e_lmi_tagged, :string
    add_column :enni_profiles , :lldp_tagged, :string
    add_column :enni_profiles , :garp_tagged, :string
    add_column :enni_profiles , :mrp_b_tagged, :string
    add_column :enni_profiles , :cisco_bpdu_tagged, :string
    add_column :enni_profiles , :default_l2cp_tagged, :string
    add_column :enni_profiles , :lacp_notes_tagged, :string
  end

  def self.down
    remove_column :enni_profiles, :tx_power_lo
    remove_column :enni_profiles, :tx_power_hi
    remove_column :enni_profiles, :rx_power_lo
    remove_column :enni_profiles, :rx_power_hi
    remove_column :enni_profiles, :auto_negotiate
    remove_column :enni_profiles, :other_ether_type_notes
    rename_column :enni_profiles, :additional_phy_notes, :optical_if_info
    remove_column :enni_profiles , :multi_link_support
    remove_column :enni_profiles , :max_links_supported
    remove_column :enni_profiles , :mtu_notes
    remove_column :enni_profiles , :max_num_ovcs_notes
    remove_column :enni_profiles , :lamp
    remove_column :enni_profiles , :link_oam
    remove_column :enni_profiles , :e_lmi
    remove_column :enni_profiles , :mrp_b
    remove_column :enni_profiles , :cisco_bpdu
    remove_column :enni_profiles , :default_l2cp
    remove_column :enni_profiles , :stp_tagged
    remove_column :enni_profiles , :rstp_tagged
    remove_column :enni_profiles , :mstp_tagged
    remove_column :enni_profiles , :pause_tagged
    remove_column :enni_profiles , :lacp_tagged
    remove_column :enni_profiles , :lamp_tagged
    remove_column :enni_profiles , :link_oam_tagged
    remove_column :enni_profiles , :port_auth_tagged
    remove_column :enni_profiles , :e_lmi_tagged
    remove_column :enni_profiles , :lldp_tagged
    remove_column :enni_profiles , :garp_tagged
    remove_column :enni_profiles , :mrp_b_tagged
    remove_column :enni_profiles , :cisco_bpdu_tagged
    remove_column :enni_profiles , :default_l2cp_tagged
    remove_column :enni_profiles , :lacp_notes_tagged
  end
end

#
# Table name: enni_profiles
#
#  id                    :integer(4)      not null, primary key
#  name                  :string(255)
#  fiber_type            :string(255)
#  phy_type              :string(255)
#  ether_type            :string(255)
#  consistent_ethertype  :boolean(1)
#  outer_tag_ovc_mapping :boolean(1)
#  connected_device_type :string(255)
#  lag_supported         :boolean(1)
#  lag_type              :string(255)
#  lacp_supported        :boolean(1)
#  lacp_priority_support :boolean(1)
#  mtu                   :integer(4)
#  max_num_ovcs          :integer(4)
#  ah_supported          :boolean(1)
#  ag_supported          :boolean(1)
#  lldp                  :string(255)
#  stp                   :string(255)
#  rstp                  :string(255)
#  mstp                  :string(255)
#  evst                  :string(255)
#  rpvst                 :string(255)
#  eaps                  :string(255)
#  pause                 :string(255)
#  lacp                  :string(255)
#  garp                  :string(255)
#  port_auth             :string(255)
#  operator_network_id   :integer(4)
#  notes                 :string(255)
#  created_at            :datetime
#  updated_at            :datetime
#  optical_if_info       :string(255)
#  phy_layer_notes       :string(255)
#  frame_format_notes    :string(255)
#  protection_notes      :string(255)
#  oam_notes             :string(255)
#  lacp_notes            :string(255)
#
