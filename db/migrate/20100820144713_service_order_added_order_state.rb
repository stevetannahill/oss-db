class ServiceOrderAddedOrderState < ActiveRecord::Migration
  def self.up
    add_column :service_orders, :ordered_operator_network_id, :integer
    add_column :service_orders, :order_state, :string
  end

  def self.down
    remove_column :service_orders, :ordered_operator_network_id
    remove_column :service_orders, :order_state
  end
end
