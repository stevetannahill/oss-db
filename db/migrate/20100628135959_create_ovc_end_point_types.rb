class CreateOvcEndPointTypes < ActiveRecord::Migration
  def self.up
    create_table :ovc_end_point_types do |t|
      t.string :name
      t.integer :ethernet_service_type_id

      t.timestamps
    end
  end

  def self.down
    drop_table :ovc_end_point_types
  end
end
