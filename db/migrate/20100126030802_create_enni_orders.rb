class CreateEnniOrders < ActiveRecord::Migration
  def self.up
    create_table :enni_orders do |t|
      t.string :description
      t.string :order_type
      t.integer :primary_contact_id
      t.integer :testing_contact_id
      t.date :requested_service_date
      t.date :order_received_date
      t.date :order_acceptance_date
      t.date :order_completion_date
      t.date :customer_acceptance_date
      t.integer :operator_network_id
      t.integer :enni_id
      t.string :cenx_cma
      t.string :status

      t.timestamps
    end
  end

  def self.down
    drop_table :enni_orders
  end
end
