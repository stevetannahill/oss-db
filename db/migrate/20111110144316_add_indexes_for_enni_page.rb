class AddIndexesForEnniPage < ActiveRecord::Migration
  def change
    remove_index :stateful_ownerships, [:stateable_type,:stateable_id]
    add_index :stateful_ownerships, [:stateable_id,:stateable_type]
  end
end
