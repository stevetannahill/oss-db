class CreateCosTestVectors < ActiveRecord::Migration
  def self.up
    create_table :cos_test_vectors do |t|
     t.string :type
     t.string :test_type
     t.string :vector_id
     t.string :notes
     t.integer :ovc_end_point_new_id
     t.integer :cos_end_point_id
     t.integer :service_level_guarantee_type_id
     t.timestamps
    end

    add_column :cos_end_points, :connected_cos_end_point_id, :integer
    add_column :cos_end_points, :type, :string
    add_column :demarcs, :lag_mac, :string
    add_column :cos_instances, :type, :string

  end

  def self.down
    drop_table :cos_test_vectors
    remove_column :cos_end_points, :connected_cos_end_point_id
    remove_column :cos_end_points, :type
    remove_column :demarcs, :lag_mac
    remove_column :cos_instances, :type
  end
  
end
