class OvcEndPointNew < InstanceElement
end

class AddMepIdToOvcEndPoint < ActiveRecord::Migration
  def self.up
    add_column :ovc_end_point_news, :mep_id, :string, :default => "TBD"

    OvcEndPointNew.find(:all).each do |ovcen|
      ovcen.generate_mep_id
      ovcen.save
    end

  end

  def self.down
    remove_column :ovc_end_point_news, :mep_id
  end
end
