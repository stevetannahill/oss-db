class UniOvceCleanup < ActiveRecord::Migration
  def self.up
    add_column :ovc_end_points, :uni_profile_id, :integer
    add_column :cosid_test_vectors, :ovc_end_point_id, :integer
    add_column :uni_profiles, :sp_name, :string
    remove_column :cosid_test_vectors, :ovc_monitor_id
  end

  def self.down
    remove_column :ovc_end_points, :uni_profile_id
    remove_column :cosid_test_vectors, :ovc_end_point_id
    remove_column :uni_profiles, :sp_name
    add_column :cosid_test_vectors, :ovc_monitor_id, :integer
  end
end
