class MoreCovceColumns < ActiveRecord::Migration
  def self.up
    add_column :ovc_end_points, :end_user_name, :string
    add_column :ovc_end_points, :is_new_construction, :boolean
    add_column :ovc_end_points, :uni_speed, :string
    add_column :ovc_end_points, :contact_info, :string
    add_column :ovc_end_points, :nid_type, :string
    add_column :ovc_end_points, :nid_sw_version, :string
    add_column :ovc_end_points, :sp_uni_id, :string
  end

  def self.down
    remove_column :ovc_end_points, :end_user_name
    remove_column :ovc_end_points, :is_new_construction
    remove_column :ovc_end_points, :uni_speed
    remove_column :ovc_end_points, :contact_info
    remove_column :ovc_end_points, :nid_type
    remove_column :ovc_end_points, :nid_sw_version
    remove_column :ovc_end_points, :sp_uni_id
  end
end
