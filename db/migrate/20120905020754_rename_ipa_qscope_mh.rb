class RenameIpaQscopeMh < ActiveRecord::Migration
  def up
    sql = "select id, value from member_attr_instances where value like '%IPA-%' or value like '%QSCOPE-%'"
    updates = []
    ActiveRecord::Base.connection.select_all(sql).each do |data|
      old_value = data["value"].dup
      data["value"].gsub!(/^QSCOPE-/,"QSCOPE")
      data["value"].gsub!(/^IPA-/,"IPA")
      data["value"].gsub!(/QSCOPE-/,"-QSCOPE")
      data["value"].gsub!(/IPA-/,"-IPA")
      next if data["value"] == old_value
      updates << { "id" => data["id"], "value" => data["value"]  }
    end

    updates.each do |data|
      sql = "update member_attr_instances set value = '#{data["value"]}' where id = #{data["id"]}"
      ActiveRecord::Base.connection.update(sql)
    end    
  end

  def down
    # do not undo this migration
  end
end
