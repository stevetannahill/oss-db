class FixOssTables < ActiveRecord::Migration
  def self.up
    rename_column :service_types, :code, :role
    add_column :service_types, :mef9_cert, :string
    add_column :service_types, :mef14_cert, :string
    add_column :service_types, :unicast_frame_delivery_details, :string
    add_column :service_types, :multicast_frame_delivery_details, :string
    add_column :service_types, :broadcast_frame_delivery_details, :string
    add_column :service_types, :cos_marking, :string
    
    remove_column :class_of_services, :cos_value
    add_column :class_of_services, :cos_value, :string
    add_column :class_of_services, :availability, :string
    add_column :class_of_services, :frame_loss_ratio, :string
    add_column :class_of_services, :mttr_hrs, :string

    remove_column :service_level_guarantees, :availability
    remove_column :service_level_guarantees, :frame_loss_ratio
    remove_column :service_level_guarantees, :mttr_days

    add_column :uni_profiles, :service_multiplexing, :boolean
    remove_column :uni_profiles, :name

  end

  def self.down
    rename_column :service_types, :role, :code
    remove_column :service_types, :mef9_cert
    remove_column :service_types, :mef14_cert
    remove_column :service_types, :unicast_frame_delivery_details
    remove_column :service_types, :multicast_frame_delivery_details
    remove_column :service_types, :broadcast_frame_delivery_details
    remove_column :service_types, :cos_marking

    remove_column :class_of_services, :cos_value
    add_column :class_of_services, :cos_value, :integer
    remove_column :class_of_services, :availability
    remove_column :class_of_services, :frame_loss_ratio
    remove_column :class_of_services, :mttr_hrs

    add_column :service_level_guarantees, :availability, :string
    add_column :service_level_guarantees, :frame_loss_ratio, :string
    add_column :service_level_guarantees, :mttr_days, :string

    remove_column :uni_profiles, :service_multiplexing
    add_column :uni_profiles, :name, :string
  end
end
