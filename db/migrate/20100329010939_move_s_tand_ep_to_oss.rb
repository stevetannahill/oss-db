class MoveSTandEpToOss < ActiveRecord::Migration
  def self.up
    rename_column :enni_profiles, :operator_network_id, :offered_services_study_id
    rename_column :offered_services_studies, :title, :name
    rename_column :service_types, :operator_network_id, :offered_services_study_id
    add_column :offered_services_studies, :sla_calculation_method, :string
    add_column :offered_services_studies, :metrics_details, :string
    add_column :offered_services_studies, :delay_definition, :string
  end

  def self.down
    rename_column :enni_profiles, :offered_services_study_id , :operator_network_id
    rename_column :offered_services_studies, :name , :title
    rename_column :service_types, :offered_services_study_id, :operator_network_id
    remove_column :offered_services_studies, :sla_calculation_method
    remove_column :offered_services_studies, :metrics_details
    remove_column :offered_services_studies, :delay_definition
  end
end
