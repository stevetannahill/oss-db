class RemoveMemberNameFromDemarcsAndOvcEndPoints < ActiveRecord::Migration
  def self.up
    remove_column :demarcs, :member_name
    remove_column :ovc_end_point_news, :member_name
  end

  def self.down
    add_column :demarcs, :member_name, :string
    add_column :ovc_end_point_news, :member_name, :string
  end
end
