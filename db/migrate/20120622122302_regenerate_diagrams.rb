class RegenerateDiagrams < ActiveRecord::Migration
  def up
    # Regenerate path diagrams, place in 'up' method only
    Migrate.regenerate_diagrams
  end

  def down
  end
end
