class CreateOperatorNetworkTypesUsers < ActiveRecord::Migration
  def up
    create_table :operator_network_types_users, :force => true, :id => false do |t|
      t.integer :operator_network_type_id
      t.integer :user_id
    end
    
    add_index(:operator_network_types_users, [:operator_network_type_id, :user_id], :unique => true, :name => 'operator_network_types_users_index')    
  end

  def down
    drop_table :operator_network_types_users
  end
end
