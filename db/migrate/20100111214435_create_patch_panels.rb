class CreatePatchPanels < ActiveRecord::Migration
  def self.up
    create_table :patch_panels do |t|
      t.string :name
      t.integer :exchange_location_id
      t.timestamps
    end
  end

  def self.down
    drop_table :patch_panels
  end
end
