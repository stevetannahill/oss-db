class MoreDemarcRework < ActiveRecord::Migration
  def self.up
    add_column :ports, :demarc_id,  :integer
  end

  def self.down
    remove_column :ports, :demarc_id
  end
end
