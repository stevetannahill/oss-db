class FixExchangeLocationTableJan18 < ActiveRecord::Migration
  def self.up
    add_column :exchange_locations, :contact_info, :string
    add_column :exchange_locations, :site_host, :string
    add_column :exchange_locations, :floor, :string
    add_column :exchange_locations, :cabinets, :string
    add_column :exchange_locations, :meet_me_room, :string
    add_column :exchange_locations, :clli, :string
    add_column :exchange_locations, :icsc, :string
    add_column :exchange_locations, :network_address, :string
    add_column :exchange_locations, :broadcast_address, :string
    add_column :exchange_locations, :host_range, :string
    add_column :exchange_locations, :hosts_per_subnet, :string
    add_column :exchange_locations, :notes, :string
  end

  def self.down
    remove_column :exchange_locations, :contact_info
    remove_column :exchange_locations, :host
    remove_column :exchange_locations, :floor
    remove_column :exchange_locations, :cabinets
    remove_column :exchange_locations, :meet_me_room
    remove_column :exchange_locations, :clli
    remove_column :exchange_locations, :icsc
    remove_column :exchange_locations, :network_address
    remove_column :exchange_locations, :broadcast_address
    remove_column :exchange_locations, :host_range
    remove_column :exchange_locations, :hosts_per_subnet
    remove_column :exchange_locations, :notes
  end
end
