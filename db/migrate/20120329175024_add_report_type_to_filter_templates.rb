class AddReportTypeToFilterTemplates < ActiveRecord::Migration
  def change
    remove_column :filter_templates, :type
    add_column :filter_templates, :report_type, :string
  end
end
