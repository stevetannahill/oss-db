class FixNewOvcModel < ActiveRecord::Migration
  def self.up
    add_column :ovc_news, :ovc_class, :string
  end

  def self.down
    remove_column :ovc_news, :ovc_class
  end
end
