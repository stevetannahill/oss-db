class RenameQscopeNodeNames < ActiveRecord::Migration
  def up
    sql = "select id,name from nodes"

    updates = []
    ActiveRecord::Base.connection.select_all(sql).each do |data|
      old_name = data["name"].dup
      data["name"].gsub!(/QSCOPE-/,"-QSCOPE")
      next if data["name"] == old_name
      updates << { "id" => data["id"], "name" => data["name"]  }
    end

    updates.each do |data|
      sql = "update nodes set name = '#{data["name"]}' where id = #{data["id"]}"
      ActiveRecord::Base.connection.update(sql)
    end    
  end

  def down
    # do not undo this migration
  end
end
