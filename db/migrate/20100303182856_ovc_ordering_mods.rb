class OvcOrderingMods < ActiveRecord::Migration
  def self.up
    add_column :ovc_end_points, :mac, :string
    add_column :ovc_end_points, :cenx_id, :string
    add_column :ovc_end_points, :address, :string
    add_column :enni_orders, :operations_ticket_id, :string
  end

  def self.down
    remove_column :ovc_end_points, :mac
    remove_column :ovc_end_points, :cenx_id
    remove_column :ovc_end_points, :address
    remove_column :enni_orders, :operations_ticket_id
  end
end
