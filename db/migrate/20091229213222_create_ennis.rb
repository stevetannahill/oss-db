class CreateEnnis < ActiveRecord::Migration
  def self.up
    create_table :ennis do |t|
      t.string :name
      t.string :fiber_type
      t.string :if_type
      t.float :tx_power
      t.float :rx_power
      t.string :frame_format
      t.boolean :ovc_tag_mapping
      t.integer :mtu
      t.string :l2cp_info
      t.string :lag_support_info
      t.string :connected_device_type

      t.timestamps
    end
    
    add_column :ports, :enni_id, :integer
  end

  def self.down
    remove_column :ports, :enni_id
    drop_table :ennis
  end
end
