class EnniTableModsJan252010 < ActiveRecord::Migration
  def self.up
    add_column :ennis, :protection_type, :string
    add_column :ennis, :primary_contact_id, :integer
    add_column :ennis, :testing_contact_id, :integer
    remove_column :ennis, :is_protected
  end

  def self.down
    remove_column :ennis, :protection_type
    remove_column :ennis, :primary_contact_id
    remove_column :ennis, :testing_contact_id
    add_column :ennis, :is_protected, :boolean
  end
end
