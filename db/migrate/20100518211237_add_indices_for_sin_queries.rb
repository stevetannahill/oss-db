class AddIndicesForSinQueries < ActiveRecord::Migration
  def self.up
    # indices to improve sin queries for summary filter
    add_index :cenx_ovcs, :evc_id
    add_index :cenx_ovc_end_points, :cenx_ovc_id
    add_index :ovcs, :evc_id
    add_index :ovc_end_points, :ovc_id

    # further indices to speed up sin queries to build JSON view of data
    add_index :cosid_flows, :ovc_id
    add_index :cosid_flows, :class_of_service_id
  end

  def self.down
    remove_index :cosid_flows, :class_of_service_id
    remove_index :cosid_flows, :ovc_id
    remove_index :ovc_end_points, :ovc_id
    remove_index :ovcs, :evc_id
    remove_index :cenx_ovc_end_points, :cenx_ovc_id
    remove_index :cenx_ovcs, :evc_id
  end
end
