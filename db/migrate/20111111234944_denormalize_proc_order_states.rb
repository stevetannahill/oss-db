class Evc_11_11_2011_11_11 < ActiveRecord::Base
  set_table_name 'evcs'
  set_inheritance_column :_type_disabled
end

class Ovc_11_11_2011_11_11 < ActiveRecord::Base
  set_table_name 'ovc_news'
  set_inheritance_column :_type_disabled
end
class OvcEndPointNew_11_11_2011_11_11 < ActiveRecord::Base
  set_table_name 'ovc_end_point_news'
  set_inheritance_column :_type_disabled
end
class Demarc_11_11_2011_11_11 < ActiveRecord::Base
  set_table_name 'demarcs'
  set_inheritance_column :_type_disabled
end
class Node_11_11_2011_11_11 < ActiveRecord::Base
  set_table_name 'nodes'
  set_inheritance_column :_type_disabled
end

class DenormalizeProcOrderStates < ActiveRecord::Migration
  def up
    [:demarcs, :evcs, :ovc_news, :ovc_end_point_news, :nodes].each do |table_name|
      add_column table_name, :prov_name, :string
      add_column table_name, :prov_notes, :text
      add_column table_name, :prov_timestamp, :bigint
      add_column table_name, :order_name, :string
      add_column table_name, :order_notes, :text
      add_column table_name, :order_timestamp, :bigint
    end
    
    Node_11_11_2011_11_11.reset_column_information
    Demarc_11_11_2011_11_11.reset_column_information
    OvcEndPointNew_11_11_2011_11_11.reset_column_information
    Ovc_11_11_2011_11_11.reset_column_information
    Evc_11_11_2011_11_11.reset_column_information
    
    
    (Node_11_11_2011_11_11.all + Demarc_11_11_2011_11_11.all + OvcEndPointNew_11_11_2011_11_11.all + Ovc_11_11_2011_11_11.all + Evc_11_11_2011_11_11.all).each do |object|      
      statefuls = Stateful.joins("INNER JOIN `stateful_ownerships` ON `statefuls`.`id` = `stateful_ownerships`.`stateful_id`").
               where("`stateful_ownerships`.`stateable_id` = ? AND `stateful_ownerships`.`stateable_type` = ?", object.id, object.class.to_s.sub("_11_11_2011_11_11",""))            
      statefuls.each do |s|
        if s.name == "Provisioning"
          state = s.states.last
          object.update_attributes(:prov_name => state.name, :prov_notes => state.notes, :prov_timestamp => (state.timestamp.to_f*1000).to_i)
        end
      end
      
      # get the order
      service_orders = ServiceOrder.where("`service_orders`.`ordered_entity_id` = ? AND `service_orders`.`ordered_entity_type` = ?", object.id, object.class.to_s.sub("_11_11_2011_11_11",""))
      so = service_orders.last
      if so != nil
        statefuls = Stateful.joins("INNER JOIN `stateful_ownerships` ON `statefuls`.`id` = `stateful_ownerships`.`stateful_id`").
                             where("`stateful_ownerships`.`stateable_id` = ? AND `stateful_ownerships`.`stateable_type` = ?", so.id, "ServiceOrder")               
        statefuls.each do |s|
          if s.name == "Ordering"
            state = s.states.last
            object.update_attributes(:order_name => state.name, :order_notes => state.notes, :order_timestamp => (state.timestamp.to_f*1000).to_i)
          end
        end
      end
      puts "Error Failed to save #{object.class}:#{object.id} #{object.errors.inspect}" if !object.save
    end  
  end

  def down
    [:demarcs, :evcs, :ovc_news, :ovc_end_point_news, :nodes].each do |table_name|
      remove_column table_name, :prov_state
      remove_column table_name, :prov_notes
      remove_column table_name, :prov_timestamp
      remove_column table_name, :order_state
      remove_column table_name, :order_notes
      remove_column table_name, :order_timestamp
    end
  end
end
