class ChangeExchangeTypeToIncludeNodeId < ActiveRecord::Migration

  class Exchange < ActiveRecord::Base
  end
  
  def self.up
    conversion = {
      "Standard Exchange" => "StandardExchange7750",
      "LightSquared Exchange" => "LightSquaredExchange7450_TAP",
      "LightSquaredExchange7450" => "LightSquaredExchange7450_TAP"
    }
    
    Exchange.reset_column_information
    Exchange.all.each do |ex|
      next if conversion[ex.exchange_type].nil?
      ex.exchange_type = conversion[ex.exchange_type]
      ex.save false
    end

    change_column_default :exchanges, :exchange_type, "StandardExchange7750"
  end

  def self.down
  end
end
