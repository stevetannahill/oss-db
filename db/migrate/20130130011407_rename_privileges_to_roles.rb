class RenamePrivilegesToRoles < ActiveRecord::Migration
  def up
    swap_roles_and_privileges
    rename_column :users, :privilege_id, :role_id
  end

  def down
    swap_roles_and_privileges
    rename_column :users, :role_id, :privilege_id
  end

  def swap_roles_and_privileges
    roles = Role.all
    privileges = Privilege.all

    temp_roles = roles.map {|role| role}
    temp_privileges = privileges.map {|p| p}

    Role.delete_all
    Privilege.delete_all

    rename_table :privileges, :roles_temp
    rename_table :roles, :privileges
    rename_table :roles_temp, :roles

    temp_roles.each do |role|
      Privilege.new(name: role.name) do |p|
        p.id = role.id
        p.save
      end
    end

    temp_privileges.each do |p|
      Role.new(name: p.name) do |r|
        r.id = p.id
        r.save
      end
    end

    rename_column :privileges_roles, :privilege_id, :role_id_temp
    rename_column :privileges_roles, :role_id, :privilege_id
    rename_column :privileges_roles, :role_id_temp, :role_id
  end
end
