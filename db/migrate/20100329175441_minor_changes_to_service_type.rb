class MinorChangesToServiceType < ActiveRecord::Migration
  def self.up
    remove_column :service_types, :cos_marking
    add_column :service_types, :cos_marking_at_enni_is_pcp, :boolean
    add_column :service_types, :cos_marking_at_enni_notes, :string
    add_column :service_types, :color_marking_notes, :string
    add_column :ennis, :fiber_handoff_type, :string
  end

  def self.down
    remove_column :service_types, :cos_marking_at_enni_is_pcp
    add_column :service_types, :cos_marking, :string
    remove_column :service_types, :cos_marking_at_enni_notes
    remove_column :service_types, :color_marking_notes
    remove_column :ennis, :fiber_handoff_type
  end
end
