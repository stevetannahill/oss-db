class DataFixDemarcIcon < ActiveRecord::Migration
  def self.up
    # All ENNIs must have an icon of enni
    execute "UPDATE demarcs SET demarc_icon = 'enni' WHERE type = 'EnniNew'"
  end

  def self.down
  end
end
