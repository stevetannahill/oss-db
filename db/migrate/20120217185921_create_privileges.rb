class CreatePrivileges < ActiveRecord::Migration
  def change
    create_table :privileges, :force => true do |t|
      t.string :name, {:length => 256, :null => false}

      t.timestamps
    end
  end
end
