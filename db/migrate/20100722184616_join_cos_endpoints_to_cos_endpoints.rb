class JoinCosEndpointsToCosEndpoints < ActiveRecord::Migration
  def self.up
    # generate the join table for COS End Points
    create_table "cos_end_points_cos_end_points", :id => false do |t|
      t.column "cos_end_point_id", :integer
      t.column "self_id", :integer
    end
    add_index "cos_end_points_cos_end_points", "cos_end_point_id", :name => "index_cos_end_points_cos_end_points_on_cos_end_point_id"
    add_index "cos_end_points_cos_end_points", "self_id", :name => "index_cos_end_points_cos_end_points_on_self_id"

  end

  def self.down
    drop_table :cos_end_points_cos_end_points
  end
end
