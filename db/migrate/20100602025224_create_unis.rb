class CreateUnis < ActiveRecord::Migration
  def self.up
    create_table :unis do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :unis
  end
end
