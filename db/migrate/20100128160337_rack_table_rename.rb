class RackTableRename < ActiveRecord::Migration
  def self.up
    rename_table :racks, :cabinets
  end

  def self.down
    rename_table :cabinets, :racks
  end
end
