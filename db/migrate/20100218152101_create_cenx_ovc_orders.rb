class CreateCenxOvcOrders < ActiveRecord::Migration
  def self.up
    create_table :cenx_ovc_orders do |t|
      t.string :description
      t.string :order_type
      t.integer :primary_contact_id
      t.date :requested_service_date
      t.date :order_received_date
      t.date :order_acceptance_date
      t.date :order_completion_date
      t.date :customer_acceptance_date
      t.integer :evc_id
      t.integer :cenx_ovc_id
      t.string :status
      t.string :cenx_id
      t.string :notes

      t.timestamps
    end
  end

  def self.down
    drop_table :cenx_ovc_orders
  end
end
