class CreateMemberServiceOrders < ActiveRecord::Migration
  def self.up
    create_table :member_service_orders do |t|
      t.string :description
      t.string :order_type
      t.integer :evc_id
      t.integer :ovc_id
      t.text :notes
      t.string :cenx_id

      t.timestamps
    end
  end

  def self.down
    drop_table :member_service_orders
  end
end
