class CreateOrderTypes < ActiveRecord::Migration
  def self.up
    create_table :order_types do |t|
      t.string :name
      t.integer :operator_network_type_id

      t.timestamps
    end
    
    rename_column :service_orders, :order_type, :action
  end

  def self.down
    drop_table :order_types
    rename_column :service_orders, :action, :order_type
  end
end
