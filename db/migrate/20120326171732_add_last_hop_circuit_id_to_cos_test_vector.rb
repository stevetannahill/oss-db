class AddLastHopCircuitIdToCosTestVector < ActiveRecord::Migration
  def change
    add_column :cos_test_vectors, :last_hop_circuit_id, :string
  end
end
