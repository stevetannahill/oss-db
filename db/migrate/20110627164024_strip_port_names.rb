class StripPortNames < ActiveRecord::Migration
  def self.up
    Port.all.each do |port|
      port.name = port.name.strip
      port.save false
    end
  end

  def self.down
  end
end
