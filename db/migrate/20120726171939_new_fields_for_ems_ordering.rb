class NewFieldsForEmsOrdering < ActiveRecord::Migration
  def up
    add_column :order_types, :order_submitter, :string
    add_column :order_types, :asr_number, :string
    add_column :order_types, :project_name, :string
    add_column :order_types, :response_type_requested, :string
    add_column :order_types, :customer_name, :string
    add_column :order_types, :billing_account, :string
    add_column :order_types, :bill_payer, :string
    add_column :order_types, :drc, :string
    add_column :order_types, :fusf, :string
    add_column :order_types, :promotion_number, :string

    add_column :service_orders, :order_version, :string
    add_column :service_orders, :service_center, :string
    add_column :service_orders, :order_submitted_date, :date
    add_column :service_orders, :seller_case_number, :string
    add_column :service_orders, :enhancement_code, :string
    add_column :service_orders, :ordered_entity_order_reference, :string
    add_column :service_orders, :contract_length_months, :string
    add_column :service_orders, :surcharge_status, :string

    add_column :demarcs, :nc, :string
    add_column :demarcs, :nci, :string

    add_column :segment_end_points, :provider_switch_point, :string
    add_column :segment_end_points, :related_uni_provider_name, :string

    add_column :contacts, :city, :string
    add_column :contacts, :state, :string
    add_column :contacts, :postal_code, :string
  end

  def down
    remove_column :order_types, :order_submitter
    remove_column :order_types, :asr_number
    remove_column :order_types, :project_name
    remove_column :order_types, :response_type_requested
    remove_column :order_types, :customer_name
    remove_column :order_types, :billing_account
    remove_column :order_types, :bill_payer
    remove_column :order_types, :drc
    remove_column :order_types, :fusf
    remove_column :order_types, :promotion_number

    remove_column :service_orders, :order_version
    remove_column :service_orders, :service_center
    remove_column :service_orders, :order_submitted_date
    remove_column :service_orders, :seller_case_number
    remove_column :service_orders, :enhancement_code
    remove_column :service_orders, :ordered_entity_order_reference
    remove_column :service_orders, :contract_length_months
    remove_column :service_orders, :surcharge_status

    remove_column :demarcs, :nc
    remove_column :demarcs, :nci

    remove_column :segment_end_points, :provider_switch_point
    remove_column :segment_end_points, :related_uni_provider_name

    remove_column :contacts, :city
    remove_column :contacts, :state
    remove_column :contacts, :postal_code
  end
end
