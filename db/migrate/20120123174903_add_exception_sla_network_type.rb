class AddExceptionSlaNetworkType < ActiveRecord::Migration
  def up
    create_table :exception_sla_network_types do |t|
      t.integer :time_declared
      t.column  :severity, "ENUM('critical', 'major', 'minor', 'clear')"
      t.column  :network_type, :string
      t.float   :flr
      t.float   :fd
      t.float   :fdv
    end
  end

  def down
    drop_table :exception_sla_network_types
  end
end
