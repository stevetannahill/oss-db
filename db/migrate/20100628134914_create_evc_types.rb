class CreateEvcTypes < ActiveRecord::Migration
  def self.up
    create_table :evc_types do |t|
      t.string :name
      t.integer :operator_network_type_id
      t.string :evc_type
      t.integer :max_num_unis
      t.integer :mtu

      t.timestamps
    end
  end

  def self.down
    drop_table :evc_types
  end
end
