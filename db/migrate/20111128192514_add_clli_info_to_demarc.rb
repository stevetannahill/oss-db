class AddClliInfoToDemarc < ActiveRecord::Migration
  def up
    add_column :demarcs, :near_side_clli, :string, :default => "TBD"
    add_column :demarcs, :far_side_clli, :string, :default => "TBD"
  end
  def down
    remove_column :demarcs, :near_side_clli
    remove_column :demarcs, :far_side_clli
  end
end
