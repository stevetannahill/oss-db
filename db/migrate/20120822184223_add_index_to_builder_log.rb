class AddIndexToBuilderLog < ActiveRecord::Migration
  def change
     add_index :builder_logs, :name
     add_index :builder_logs, :grid_id
  end
end
