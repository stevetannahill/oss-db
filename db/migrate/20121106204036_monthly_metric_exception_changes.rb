class MonthlyMetricExceptionChanges < ActiveRecord::Migration
  def up
    drop_table :exception_sla_cos_test_vectors

    remove_column :metrics_cos_test_vectors, :time_over
  end

  def down
    create_table "exception_sla_cos_test_vectors" do |t|
      t.integer "time_declared"
      t.float   "value"
      t.integer "grid_circuit_id"
      t.integer "time_over"
      t.string  "severity"
      t.string  "sla_type"
    end

    add_index "exception_sla_cos_test_vectors", ["grid_circuit_id", "time_declared", "severity"], :name => "index_exception_sla_cos_test_vectors_grid_time_sev"
    add_index "exception_sla_cos_test_vectors", ["grid_circuit_id", "time_declared"], :name => "index_exception_sla_cos_test_vectors_grid_time"
    add_index "exception_sla_cos_test_vectors", ["grid_circuit_id"], :name => "index_exception_sla_cos_test_vectors_on_grid_circuit_id"
    add_index "exception_sla_cos_test_vectors", ["time_declared"], :name => "index_exception_sla_cos_test_vectors_on_time_declared"
  
    add_column :metrics_cos_test_vectors, :time_over, :integer
  end
end
