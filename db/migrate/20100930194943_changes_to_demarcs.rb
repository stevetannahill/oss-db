class ChangesToDemarcs < ActiveRecord::Migration
  def self.up
    remove_column :demarcs, :mtu
    remove_column :demarcs, :stag_reservation_notes
    rename_column :demarcs, :ag_supported, :ah_supported
    rename_column :demarcs, :lag_mac, :lag_mac_primary
    add_column :demarcs, :lag_mac_secondary, :string
    remove_column :demarcs, :reflection_enabled
    add_column :ovc_end_point_news, :reflection_enabled, :boolean
  end

  def self.down
    add_column :demarcs, :mtu, :integer
    add_column :demarcs, :stag_reservation_notes, :string
    rename_column :demarcs, :ah_supported, :ag_supported
    rename_column :demarcs, :lag_mac_primary, :lag_mac
    remove_column :demarcs, :lag_mac_secondary
    add_column :demarcs, :reflection_enabled, :boolean
    remove_column :ovc_end_point_news, :reflection_enabled
  end
end
