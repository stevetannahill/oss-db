class FixEnniTable < ActiveRecord::Migration
  def self.up
    rename_column :ennis, :frame_format, :ether_type
    rename_column :ennis, :ovc_tag_mapping, :outer_tag_ovc_mapping
    add_column :ennis, :threasholds, :string
    add_column :ennis, :notes, :string
    add_column :ennis, :consistent_ethertype, :bool
    add_column :ennis, :lag_supported, :bool
    add_column :ennis, :lag_type, :string
    add_column :ennis, :lacp_supported, :bool
    add_column :ennis, :lacp_priority_support, :bool
    add_column :ennis, :max_num_ovcs, :integer
    add_column :ennis, :ah_supported, :bool
    add_column :ennis, :ag_supported, :bool   
    add_column :ennis, :lldp, :string
    add_column :ennis, :stp, :string
    add_column :ennis, :rstp, :string
    add_column :ennis, :mstp, :string
    add_column :ennis, :evst, :string
    add_column :ennis, :rpvst, :string
    add_column :ennis, :eaps, :string
    add_column :ennis, :pause, :string
    add_column :ennis, :lacp, :string
    add_column :ennis, :garp, :string
    add_column :ennis, :port_auth, :string
    remove_column :ennis, :l2cp_info
    remove_column :ennis, :lag_support_info
  end

  def self.down
    add_column :ennis, :l2cp_info, :string
    add_column :ennis, :lag_support_info, :string
    remove_column :ennis, :lldp
    remove_column :ennis, :stp
    remove_column :ennis, :rstp
    remove_column :ennis, :mstp
    remove_column :ennis, :evst
    remove_column :ennis, :rpvst
    remove_column :ennis, :eaps
    remove_column :ennis, :pause
    remove_column :ennis, :lacp
    remove_column :ennis, :garp
    remove_column :ennis, :port_auth
    remove_column :ennis, :ag_supported
    remove_column :ennis, :ah_supported
    remove_column :ennis, :max_num_ovcs
    remove_column :ennis, :lacp_priority_support
    remove_column :ennis, :lacp_supported
    remove_column :ennis, :lag_type
    remove_column :ennis, :lag_supported
    remove_column :ennis, :consistent_ethertype
    remove_column :ennis, :notes
    remove_column :ennis, :threasholds
    rename_column :ennis, :outer_tag_ovc_mapping, :ovc_tag_mapping
    rename_column :ennis, :ether_type, :frame_format
  end
end

