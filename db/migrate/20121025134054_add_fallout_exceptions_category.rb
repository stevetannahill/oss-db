class AddFalloutExceptionsCategory < ActiveRecord::Migration
  def change
    add_column :fallout_exceptions, :category, :string
    add_column :paths, :fallout_category, :string
  end
end