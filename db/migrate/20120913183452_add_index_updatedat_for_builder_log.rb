class AddIndexUpdatedatForBuilderLog < ActiveRecord::Migration
  def change
    add_index :builder_logs, :updated_at
  end
end
