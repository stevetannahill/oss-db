class LightSquaredMods2 < ActiveRecord::Migration
  def self.up
    add_column :demarc_types, :ls_access_solution_model, :string, :default => "N/A"
  end

  def self.down
    remove_column :demarc_types, :ls_access_solution_model
  end
end
