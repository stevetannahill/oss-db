class RemoveExtraEnniMemberHandles < ActiveRecord::Migration
  def self.up
    ennis = EnniNew.all
    ennis.each do |enni|
      handles = MemberHandleInstance.find(:all, :conditions => ["affected_entity_type = ? AND affected_entity_id = ? AND owner_type = ? AND owner_id != ?",'Demarc',enni.id,'ServiceProvider',enni.operator_network.service_provider])
      handles.each do |h|
        h.delete
      end
    end
  end

  def self.down
    raise IrreversibleMigration
  end
end
