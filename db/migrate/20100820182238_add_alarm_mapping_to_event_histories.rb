class AddAlarmMappingToEventHistories < ActiveRecord::Migration
  def self.up
    add_column :event_histories, :alarm_mapping, :string
  end

  def self.down
    remove_column :event_histories, :alarm_mapping
  end
end
