class CosTvEvolution < ActiveRecord::Migration
  def self.up
    add_column :cos_test_vectors, :service_instance_id, :string
    add_column :cos_test_vectors, :sla_id, :string
    add_column :cos_test_vectors, :test_instance_id, :string
    rename_column :cos_test_vectors, :vector_id, :test_instance_class_id
  end

  def self.down
    remove_column :cos_test_vectors, :service_instance_id
    remove_column :cos_test_vectors, :sla_id
    remove_column :cos_test_vectors, :test_instance_id
    rename_column :cos_test_vectors, :test_instance_class_id, :vector_id
  end
end
