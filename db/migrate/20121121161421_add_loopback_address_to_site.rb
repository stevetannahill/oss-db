class AddLoopbackAddressToSite < ActiveRecord::Migration
  def change
    add_column :sites, :loopback_address, :string
  end
end
