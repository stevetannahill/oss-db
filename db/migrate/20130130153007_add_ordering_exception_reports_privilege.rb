

class AddOrderingExceptionReportsPrivilege < ActiveRecord::Migration

  class Privilege < ActiveRecord::Base; end

  def up
    priv_name = "Ordering Exception Reports"
    p = Privilege.where(:name => priv_name).first
    if p.nil?
      p = Privilege.new(:name => priv_name)
      raise "Unable to Save: #{priv_name}" unless p.save
    end
  end

  def down
    p = Privilege.where(:name => "Ordering Exception Reports").first
    p.destroy unless p.nil?
  end
end
