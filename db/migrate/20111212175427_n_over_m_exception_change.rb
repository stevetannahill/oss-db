class SlaException   < ActiveRecord::Base
  def severity
    read_attribute(:severity).to_sym
  end
  def severity= (value)
    write_attribute(:severity, value.to_s)
  end
end
class SlaExceptionCosEndPoint < SlaException
  belongs_to :cos_test_vector
end

# alter table sla_exceptions add severity string(10), add cos_test_vector_id int, drop failure, drop threshold_mbps
# alter table sla_exceptions drop severity, drop cos_test_vector_id, add failure, add threshold_mbps
#

class NOverMExceptionChange < ActiveRecord::Migration
  def up
    
    add_column  :sla_exceptions, :severity, "ENUM('critical', 'major', 'minor')"
    add_column  :sla_exceptions, :cos_test_vector_id, :integer

      SlaException.reset_column_information
      SlaExceptionCosEndPoint.reset_column_information
      SlaExceptionCosEndPoint.all.each {|sla_ex|
      
      begin
        cose = CosEndPoint.find(sla_ex.cos_end_point_id)
        sla_ex.cos_test_vector_id = cose.cos_test_vectors.first.id.to_i if cose
      rescue
        # Cose doesnt exist give it a bogus value
        sla_ex.cos_test_vector_id = 999999999
      end
      
      if sla_ex.failure
        sla_ex.severity = :critical
      else
        sla_ex.severity = :major
      end
      
      sla_ex.save!
    }
    SlaException.find_each do |ex|
      if ex.failure
        ex.severity = :critical
      else
        ex.severity = :major
      end
      ex.save!
    end
    remove_column :sla_exceptions, :failure
    remove_column :sla_exceptions, :threshold_mbps
    
  end

  def down
    add_column :sla_exceptions, :failure, :boolean
    add_column :sla_exceptions, :threshold_mbps, :float
    SlaException.reset_column_information
    SlaExceptionCosEndPoint.reset_column_information
    SlaException.all.each do |ex|
      ex.failure = ex.severity == 'critical'
      ex.save 
    end
    SlaExceptionCosEndPoint.all.each do |sla_ex|
       begin
        costv = CosTestVector.find(sla_ex.cos_test_vector_id)
        sla_ex.cos_end_point_id = costv.cos_end_point.id if costv
      rescue
        # Costv doesnt exist give it a bogus value
        sla_ex.cos_end_point_id = 999999999
      end

      sla_ex.save 
    end
    remove_column :sla_exceptions, :severity
    remove_column :sla_exceptions, :cos_test_vector_id
  end
end
