class AddCiruitIdToRollupIds < ActiveRecord::Migration
  def change
    add_column :rollup_ids, :circuit_id, :string
  end
end
