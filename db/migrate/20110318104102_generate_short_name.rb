class GenerateShortName < ActiveRecord::Migration
  def self.up
    CenxClassOfServiceType.all.each do |ccos|
      map = {
        "Real Time" => "ef",
        "Premium Data" => "l1",
        "Standard Data" => "l2"
      }

      ccos.short_name = ccos.generate_short_name
      ccos.fwding_class = map[ccos.name]
      ccos.save false
    end
  end

  def self.down
    CenxClassOfServiceType.all.each do |ccos|
      ccos.short_name = nil
      ccos.save false
    end
  end
end
