class RemoveExclusionList < ActiveRecord::Migration
  def up
    drop_table :exclusion_lists
    drop_table :excluded_items
    remove_column :schedules, :exclusion_list_id
  end

  def down
  end
end
