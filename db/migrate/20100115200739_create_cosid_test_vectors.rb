class CreateCosidTestVectors < ActiveRecord::Migration
  def self.up
    create_table :cosid_test_vectors do |t|
      t.string :name
      t.integer :vector_id
      t.string :description
      t.integer :cosid_flow_id
      t.integer :node_id
      t.integer :service_level_guarantee_id
      t.integer :ovc_monitor_id

      t.timestamps
    end
  end

  def self.down
    drop_table :cosid_test_vectors
  end
end
