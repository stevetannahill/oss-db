class AlarmRecord_27_10_2011_10_10 < ActiveRecord::Base
  set_table_name 'event_records'
  set_inheritance_column :_type_disabled
end
class Evc_27_10_2011_10_10 < ActiveRecord::Base
  set_table_name 'evcs'
  set_inheritance_column :_type_disabled
end

class Ovc_27_10_2011_10_10 < ActiveRecord::Base
  set_table_name 'ovc_news'
  set_inheritance_column :_type_disabled
end
class OvcEndPointNew_27_10_2011_10_10 < ActiveRecord::Base
  set_table_name 'ovc_end_point_news'
  set_inheritance_column :_type_disabled
end
class Demarc_27_10_2011_10_10 < ActiveRecord::Base
  set_table_name 'demarcs'
  set_inheritance_column :_type_disabled
end
class CosEndPoint_27_10_2011_10_10 < ActiveRecord::Base
  set_table_name 'cos_end_points'
  set_inheritance_column :_type_disabled
end
class Node_27_10_2011_10_10 < ActiveRecord::Base
  set_table_name 'nodes'
  set_inheritance_column :_type_disabled
end

class AddAlarmRecord < ActiveRecord::Migration
  def self.up
    add_column :evcs, :event_record_id, 'integer'
    add_column :ovc_news, :event_record_id, 'integer'
    add_column :ovc_end_point_news, :event_record_id, 'integer'
    add_column :demarcs, :event_record_id, 'integer'
    add_column :cos_end_points, :event_record_id, 'integer'
    add_column :nodes, :event_record_id, 'integer'
    
    Demarc_27_10_2011_10_10.reset_column_information
    Evc_27_10_2011_10_10.reset_column_information
    Ovc_27_10_2011_10_10.reset_column_information
    OvcEndPointNew_27_10_2011_10_10.reset_column_information
    CosEndPoint_27_10_2011_10_10.reset_column_information
    Node_27_10_2011_10_10.reset_column_information
    
    objects = (Node_27_10_2011_10_10.all + Evc_27_10_2011_10_10.all + Ovc_27_10_2011_10_10.all + Demarc_27_10_2011_10_10.all + OvcEndPointNew_27_10_2011_10_10.all + CosEndPoint_27_10_2011_10_10.all).collect {|object| object if object.event_record_id == nil}.compact
    
    objects.each do |object|
      if object.event_record_id == nil
        er = AlarmRecord_27_10_2011_10_10.create(:state => AlarmSeverity::OK, :time_of_event => Time.now.to_f*1000, :original_state => "cleared", :details => "", :event_history_id => nil, :type => "AlarmRecord")
        er.save(:validate => false)
        object.update_attribute(:event_record_id, er.id)
      end
    end
      
  end

  def self.down
    
    objects = (Node.all + Evc.all + Ovc.all + Demarc.all + OvcEndPointNew.all + CosEndPoint.all).collect {|object| object if object.event_record_id != nil}.compact
    objects.each do |object|
      object.event_record.destroy
    end
     
    remove_column :evcs, :event_record_id
    remove_column :ovc_news, :event_record_id
    remove_column :ovc_end_point_news, :event_record_id
    remove_column :demarcs, :event_record_id
    remove_column :cos_end_points, :event_record_id
    remove_column :nodes, :event_record_id
  end
end
