class RemoveOldFalloutFromPaths < ActiveRecord::Migration
  def up
    remove_column :paths, :fallout
  end

  def down
    add_column :paths, :fallout, :boolean, :default => false
  end

end
