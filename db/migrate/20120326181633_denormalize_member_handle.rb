class SinEvcServiceProviderSphinx < ActiveRecord::Base
end

class DenormalizeMemberHandle < ActiveRecord::Migration
  def up
    add_column :sin_evc_service_provider_sphinxes, :member_handle, :string

   # Resave each sin_evc_service_provider_sphinx to add member hanle
    SinEvcServiceProviderSphinx.all.each(&:save)

    add_index :evc_displays, [:service_provider_id, :owner_service_provider_id], :name => :owner_service_provider_search
    add_index :evc_displays, [:service_provider_id, :pop_id], :name => :pop_search
    add_index :service_orders, [:type, :bulk_order_type], :name => :service_type
  end  
  
  def down
    remove_index :evc_displays, :name => :owner_service_provider_search
    remove_index :evc_displays, :name => :pop_search
    remove_index :service_orders, :name => :service_type
    
    remove_column :sin_evc_service_provider_sphinxes, :member_handle
  end
  
end
