class RenameServiceProviderIsCenx < ActiveRecord::Migration
  def change
    rename_column :service_providers, :is_cenx, :is_system_owner
  end
end
