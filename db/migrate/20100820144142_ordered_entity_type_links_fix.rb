class OrderedEntityTypeLinksFix < ActiveRecord::Migration
  def self.up
    change_column :ordered_entity_type_links, :ordered_entity_type_type, :string
  end

  def self.down
    change_column :ordered_entity_type_links, :ordered_entity_type_type, :integer
  end
end
