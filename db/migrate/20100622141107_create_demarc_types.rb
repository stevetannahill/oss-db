class CreateDemarcTypes < ActiveRecord::Migration
  def self.up
    create_table :demarc_types do |t|
      t.string :type
      
      # common attributes
      t.string :name
      t.string :demarc_type_type
      t.integer :operator_network_type_id
      t.string :physical_medium
      t.string :speed
      t.string :physical_medium_notes
      t.boolean :auto_negotiate
      t.integer :mtu
      t.string :mtu_notes
      t.integer :max_num_ovcs
      t.string :max_num_ovcs_notes
      t.string :connected_device_type
      t.string :remarks
      
      t.string :lldp_untagged
      t.string :stp_untagged
      t.string :rstp_untagged
      t.string :mstp_untagged
      t.string :evst_untagged
      t.string :rpvst_untagged
      t.string :eaps_untagged
      t.string :pause_untagged
      t.string :lacp_untagged
      t.string :garp_untagged
      t.string :port_auth_untagged
      t.string :lacp_notes_untagged
      t.string :lamp_untagged
      t.string :link_oam_untagged
      t.string :e_lmi_untagged
      t.string :mrp_b_untagged
      t.string :cisco_bpdu_untagged
      t.string :default_l2cp_untagged
      
      t.string :lldp_tagged
      t.string :stp_tagged
      t.string :rstp_tagged
      t.string :mstp_tagged
      t.string :pause_tagged
      t.string :lacp_tagged
      t.string :garp_tagged
      t.string :port_auth_tagged
      t.string :lacp_notes_tagged
      t.string :lamp_tagged
      t.string :link_oam_tagged
      t.string :e_lmi_tagged
      t.string :mrp_b_tagged
      t.string :cisco_bpdu_tagged
      t.string :default_l2cp_tagged

      # attributes for type=EnniType
      t.string :tx_power_lo
      t.string :tx_power_hi
      t.string :rx_power_lo
      t.string :rx_power_hi
      t.string :fiber_type
      t.string :frame_format
      t.string :frame_format_notes
      t.boolean :consistent_ethertype
      t.boolean :outer_tag_ovc_mapping
      t.boolean :lag_supported
      t.string :lag_type
      t.boolean :lacp_supported
      t.boolean :lacp_priority_support
      t.string :protection_notes
      t.boolean :ah_supported
      t.boolean :ag_supported
      t.string :oam_notes
      t.boolean :multi_link_support
      t.string :max_links_supported
      t.string :max_endpoints_per_ovc

      # attributes for type=UniType
      t.boolean :bundling
      t.boolean :ato_bundling
      t.boolean :reflection_supported
      t.boolean :service_multiplexing
      t.string :reflection_mechanisms

      t.timestamps
    end
  end

  def self.down
    drop_table :demarc_types
  end
end
