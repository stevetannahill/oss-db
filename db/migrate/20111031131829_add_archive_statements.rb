class AddArchiveStatements < ActiveRecord::Migration
  def self.up
    create_table :archive_statements do |t|
      t.string  :template, :limit => 4096
      t.string  :statement, :limit => 4096
      t.string  :stack_trace, :limit => 4096
    end
  end

  def self.down
    drop_table :archive_statements
  end
end
