class AddRateLimitsToEnniNew < ActiveRecord::Migration
  def self.up
    add_column :demarcs, :cir_limit, :integer, :default => 90
    add_column :demarcs, :eir_limit, :integer, :default => 300
  end

  def self.down
    remove_column :demarcs, :cir_limit
    remove_column :demarcs, :eir_limit
  end
end
