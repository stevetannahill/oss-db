class CreateOvcTypes < ActiveRecord::Migration
  def self.up
    create_table :ovc_types do |t|
      t.string :name
      t.integer :ethernet_service_type_id
      t.string :role
      t.string :ovc_type
      t.integer :max_uni_endpoints
      t.integer :max_enni_endpoints
      t.string :mef9_cert
      t.string :mef14_cert
      t.integer :max_mtu
      t.boolean :c_vlan_cos_preservation
      t.string :c_vlan_id_preservation
      t.boolean :s_vlan_cos_preservation
      t.string :s_vlan_id_preservation
      t.string :cos_marking_uni
      t.boolean :cos_marking_at_enni_is_pcp
      t.string :cos_marking_at_enni_notes
      t.string :color_marking
      t.string :color_marking_notes
      t.string :service_category
      t.string :unicast_frame_delivery_conditions
      t.string :multicast_frame_delivery_conditions
      t.string :broadcast_frame_delivery_conditions
      t.string :unicast_frame_delivery_details
      t.string :multicast_frame_delivery_details
      t.string :broadcast_frame_delivery_details
      t.string :notes

      t.timestamps
    end
  end

  def self.down
    drop_table :ovc_types
  end
end
