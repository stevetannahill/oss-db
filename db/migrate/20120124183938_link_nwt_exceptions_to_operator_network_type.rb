class ExceptionSlaNetworkType <  ActiveRecord::Base
end

class LinkNwtExceptionsToOperatorNetworkType < ActiveRecord::Migration
  def up
    add_column  :exception_sla_network_types, :operator_network_type_id, :integer
    ExceptionSlaNetworkType.reset_column_information
    ExceptionSlaNetworkType.all.each do |ex|
      network_type = OperatorNetworkType.find_by_name ex.network_type
      ex.operator_network_type_id = network_type.id
      ex.save
    end
  end

  def down
    remove_column  :exception_sla_network_types, :operator_network_type_id
  end
end
