class FixDemarcTypeDemarcRelation < ActiveRecord::Migration
  def self.up
    rename_column :demarcs, :demarc_type, :cenx_demarc_type
    remove_column :demarcs, :uni_profile_id
  end

  def self.down
    rename_column :demarcs, :cenx_demarc_type, :demarc_type
    add_column :demarcs, :uni_profile_id, :integer
  end
end
