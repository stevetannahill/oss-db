
#Needed due to cenx operator network being removed later
class CenxOperatorNetwork < OperatorNetwork
  has_one :pop, :dependent => :nullify

  def self.find_cenx_networks_w_no_pop_attached
    @ons = CenxOperatorNetwork.all.reject{|on| on.pop != nil }
  end
end


class Pop < ActiveRecord::Base
end

class ChangeExchangeToPop < ActiveRecord::Migration
  def up
    add_column :exchanges, :type, :string, :default => "Exchange"
    rename_column :exchanges, :exchange_type, :pop_type
    rename_table :exchanges, :pops

    rename_column :sites, :exchange_id, :pop_id
    rename_column :demarcs, :exchange_id, :pop_id
    rename_column :ovc_news, :exchange_id, :pop_id
    rename_column :qos_policies, :exchange_id, :pop_id
    change_column_default :pops, :type, nil

    add_column :evc_displays, :demarc_icon, :string

  end

  def down
    remove_column :exchanges, :type
    rename_column :exchanges, :pop_type, :exchange_type
    rename_table :exchanges, :pops

    rename_column :sites, :pop_id, :exchange_id
    rename_column :demarcs, :pop_id, :exchange_id
    rename_column :ovc_news, :pop_id, :exchange_id
    rename_column :qos_policies, :pop_id, :exchange_id

    remove_column :evc_displays, :demarc_icon
    
  end
end
