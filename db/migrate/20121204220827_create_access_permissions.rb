class CreateAccessPermissions < ActiveRecord::Migration
  def change
    create_table :access_permissions do |t|
      t.integer :operator_network_id
      t.integer :target_id

      t.string :permission

      t.timestamps
    end
  end
end
