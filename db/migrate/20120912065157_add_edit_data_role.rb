class AddEditDataRole < ActiveRecord::Migration
  def up
    Role.find_or_create_by_name(:name => 'Edit Data')
  end

  def down
    Role.destroy_all(:name => 'Edit Data')
  end
end
