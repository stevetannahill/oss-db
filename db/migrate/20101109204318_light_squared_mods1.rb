class LightSquaredMods1 < ActiveRecord::Migration
  def self.up
    add_column :demarcs, :port_enap_type , :string, :default => "QINQ"
  end

  def self.down
    remove_column :demarcs, :port_enap_type
  end
end
