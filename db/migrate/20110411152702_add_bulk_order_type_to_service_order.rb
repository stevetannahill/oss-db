class AddBulkOrderTypeToServiceOrder < ActiveRecord::Migration
  def self.up
    add_column :service_orders, :bulk_order_type, :string
  end

  def self.down
    remove_column :service_orders, :bulk_order_type
  end
end
