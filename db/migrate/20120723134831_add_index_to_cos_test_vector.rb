class AddIndexToCosTestVector < ActiveRecord::Migration
  def change
    add_index "cos_test_vectors", :ref_circuit_id
    add_index "cos_test_vectors", [:type, :cenx_id]
  end
end
