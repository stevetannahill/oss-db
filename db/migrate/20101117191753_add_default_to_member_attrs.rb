class AddDefaultToMemberAttrs < ActiveRecord::Migration
  def self.up
    add_column :member_attrs, :default_value, :string
  end

  def self.down
    remove_column :member_attrs, :default_value
  end
end
