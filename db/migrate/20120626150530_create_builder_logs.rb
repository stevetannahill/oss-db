class CreateBuilderLogs < ActiveRecord::Migration
  def change
    create_table :builder_logs do |t|
      t.string :name
      t.string :summary
      t.text   :summary_detail
      t.text   :details
      t.timestamps
    end    
  end
end
