class AddCustomizeSystemRole < ActiveRecord::Migration
  def up
    Role.find_or_create_by_name(:name => 'Customize System')
  end

  def down
    Role.destroy_all(:name => 'Customize System')
  end
end