class MoveMonitoringFromOvcToOvce < ActiveRecord::Migration
  def self.up
    add_column :ovc_end_points, :is_monitored, :boolean
    remove_column :ovcs, :monitoring
    rename_column :ovc_end_points, :mac, :mon_loopback_address
  end

  def self.down
    remove_column :ovc_end_points, :is_monitored
    add_column :ovcs, :monitoring, :boolean
    rename_column :ovc_end_points, :mon_loopback_address, :mac
  end
end
