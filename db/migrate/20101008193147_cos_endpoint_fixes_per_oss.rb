class CosEndpointFixesPerOss < ActiveRecord::Migration
  def self.up
    rename_column :cos_end_points, :cos_id_value, :ingress_mapping
    add_column :cos_end_points, :egress_marking, :string
    change_column :demarc_types, :outer_tag_ovc_mapping, :string
  end

  def self.down
    rename_column :cos_end_points, :ingress_mapping, :cos_id_value
    remove_column :cos_end_points, :egress_marking
    change_column :demarc_types, :outer_tag_ovc_mapping, :boolean
  end
end
