class Evc_11_11_2011_10_10 < ActiveRecord::Base
  set_table_name 'evcs'
  self.inheritance_column = :_type_disabled
end

class Ovc_11_11_2011_10_10 < ActiveRecord::Base
  set_table_name 'ovc_news'
  self.inheritance_column = :_type_disabled
end
class OvcEndPointNew_11_11_2011_10_10 < ActiveRecord::Base
  set_table_name 'ovc_end_point_news'
  self.inheritance_column = :_type_disabled
end
class Demarc_11_11_2011_10_10 < ActiveRecord::Base
  set_table_name 'demarcs'
  self.inheritance_column = :_type_disabled
end
class CosEndPoint_11_11_2011_10_10 < ActiveRecord::Base
  set_table_name 'cos_end_points'
  self.inheritance_column = :_type_disabled
end
class Node_11_11_2011_10_10 < ActiveRecord::Base
  set_table_name 'nodes'
  self.inheritance_column = :_type_disabled
end


class AddDenormalizedSmState < ActiveRecord::Migration
  def up
    [:demarcs, :evcs, :ovc_news, :ovc_end_point_news, :cos_end_points, :nodes].each do |table_name|
      add_column table_name, :sm_state, :string
      add_column table_name, :sm_details, :string
      add_column table_name, :sm_timestamp, :bigint
    end
    
    [Demarc_11_11_2011_10_10, Evc_11_11_2011_10_10, Ovc_11_11_2011_10_10, OvcEndPointNew_11_11_2011_10_10, CosEndPoint_11_11_2011_10_10, Node_11_11_2011_10_10].each do |klass|
      klass.all.each do |obj|
        smh = EventHistory.joins("INNER JOIN `sm_ownerships` ON `event_histories`.`id` = `sm_ownerships`.`sm_history_id`").
                 where("`event_histories`.`type` IN ('SmHistory') AND `sm_ownerships`.`smable_id` = ? AND `sm_ownerships`.`smable_type` = ?", obj.id, obj.class.to_s.sub("_11_11_2011_10_10",""))[0]
        if smh == nil
          sm = AlarmRecord.new(:time_of_event => 0, :state => AlarmSeverity::OK, :details => "", :original_state => "cleared")
        else
          sm = smh.alarm_records.last
        end
        klass.where("id = ?", obj.id).update_all(:sm_state => sm.state.to_s, :sm_details => sm.details, :sm_timestamp => sm.time_of_event)
      end
    end 
    
  end

  def down
    [:demarcs, :evcs, :ovc_news, :ovc_end_point_news, :cos_end_points, :nodes].each do |table_name|
      remove_column table_name, :sm_state
      remove_column table_name, :sm_details
      remove_column table_name, :sm_timestamp
    end
    
  end
end
