class RemoveOldTables < ActiveRecord::Migration
  def self.up
    remove_column :ports, :enni_id
    
    drop_table :bandwidth_profiles
    drop_table :cenx_ovcs
    drop_table :cenx_ovc_end_points
    drop_table :cenx_ovc_orders
    drop_table :class_of_services
    drop_table :cosid_flows
    drop_table :cosid_test_vectors
    drop_table :ennis
    drop_table :enni_orders
    drop_table :enni_profiles
    drop_table :offered_services_studies
    drop_table :ovcs
    drop_table :ovc_end_points
    drop_table :service_level_guarantees
    drop_table :service_types
    drop_table :uni_profiles
  end

  def self.down
    add_column :ports, :enni_id, :integer
    
    create_table :bandwidth_profiles do |t|
      t.string :bwp_type
      t.string :min_cir_kbps
      t.string :max_cir_kbps
      t.string :min_cbs_kb
      t.string :max_cbs_kb
      t.string :min_eir_kbps
      t.string :max_eir_kbps
      t.string :min_ebs_kb
      t.string :max_ebs_kb
      t.string :color_mode
      t.string :coupling_flag
      t.integer :class_of_service_id
      t.string :notes
      t.string :name
      t.string :cir_range_notes
      t.string :cbs_range_notes
      t.string :eir_range_notes
      t.string :ebs_range_notes

      t.timestamps
    end
    
    create_table :cenx_ovcs do |t|
      t.string :buyer_name
      t.string :seller_name
      t.string :status
      t.string :description
      t.integer :evc_id
      t.integer :exchange_id
      t.string :cenx_id
      t.integer :service_id
      t.string :service_type
      t.string :class_of_service

      t.timestamps
    end
    
    create_table :cenx_ovc_end_points do |t|
      t.string :on_switch_name
      t.string :stag
      t.string :status
      t.string :description
      t.integer :cenx_ovc_id
      t.integer :ovc_id
      t.integer :enni_id
      t.integer :port_id

      t.timestamps
    end
    
    create_table :cenx_ovc_orders do |t|
      t.string :description
      t.string :order_type
      t.integer :primary_contact_id
      t.date :requested_service_date
      t.date :order_received_date
      t.date :order_acceptance_date
      t.date :order_completion_date
      t.date :customer_acceptance_date
      t.integer :evc_id
      t.integer :cenx_ovc_id
      t.string :status
      t.string :cenx_id
      t.string :notes
      t.boolean :expedite
      t.string :operations_ticket_id

      t.timestamps
    end
    
    create_table :class_of_services do |t|
      t.string :name
      t.integer :service_type_id
      t.string :notes
      t.string :cos_value
      t.string :availability
      t.string :frame_loss_ratio
      t.string :mttr_hrs
      t.string :cos_value_uni

      t.timestamps
    end
    
    create_table :cosid_flows do |t|
      t.integer :cir_kbps
      t.integer :eir_kbps
      t.integer :ovc_id
      t.integer :class_of_service_id
      t.integer :cbs_B
      t.integer :ebs_B

      t.timestamps
    end
    
    create_table :cosid_test_vectors do |t|
      t.string :name
      t.integer :vector_id
      t.string :description
      t.integer :cosid_flow_id
      t.integer :node_id
      t.integer :service_level_guarantee_id
      t.integer :ovc_end_point_id

      t.timestamps
    end
    
    create_table :ennis do |t|
      t.string :ether_type
      t.integer :operator_network_id
      t.text :notes
      t.string :sp_name
      t.integer :exchange_id
      t.integer :enni_profile_id
      t.string :protection_type
      t.string :phy_type
      t.string :other_phy_type
      t.string :other_ether_type
      t.boolean :auto_negotiate
      t.boolean :ag_supported
      t.string :cenx_id
      t.integer :lag_id
      t.string :fiber_handoff_type
      t.string :stag_reservation_notes

      t.timestamps
    end
    
    create_table :enni_orders do |t|
      t.string :description
      t.string :order_type
      t.integer :primary_contact_id
      t.integer :testing_contact_id
      t.date :requested_service_date
      t.date :order_received_date
      t.date :order_acceptance_date
      t.date :order_completion_date
      t.date :customer_acceptance_date
      t.integer :operator_network_id
      t.integer :enni_id
      t.string :status
      t.string :notes
      t.string :cenx_id
      t.boolean :expedite
      t.string :operations_ticket_id

      t.timestamps
    end
    
    create_table :enni_profiles do |t|
      t.string :name
      t.string :fiber_type
      t.string :phy_type
      t.string :ether_type
      t.boolean :consistent_ethertype
      t.boolean :outer_tag_ovc_mapping
      t.string :connected_device_type
      t.boolean :lag_supported
      t.string :lag_type
      t.boolean :lacp_supported
      t.boolean :lacp_priority_support
      t.integer :mtu
      t.integer :max_num_ovcs
      t.boolean :ah_supported
      t.boolean :ag_supported
      t.string :lldp
      t.string :stp
      t.string :rstp
      t.string :mstp
      t.string :evst
      t.string :rpvst
      t.string :eaps
      t.string :pause
      t.string :lacp
      t.string :garp
      t.string :port_auth
      t.integer :offered_services_study_id
      t.string :notes
      t.string :additional_phy_notes
      t.string :phy_layer_notes
      t.string :frame_format_notes
      t.string :protection_notes
      t.string :oam_notes
      t.string :lacp_notes
      t.string :tx_power_lo
      t.string :tx_power_hi
      t.string :rx_power_lo
      t.string :rx_power_hi
      t.boolean :auto_negotiate
      t.string :other_ether_type_notes
      t.boolean :multi_link_support
      t.string :max_links_supported
      t.string :mtu_notes
      t.string :max_num_ovcs_notes
      t.string :lamp
      t.string :link_oam
      t.string :e_lmi
      t.string :mrp_b
      t.string :cisco_bpdu
      t.string :default_l2cp
      t.string :stp_tagged
      t.string :rstp_tagged
      t.string :mstp_tagged
      t.string :pause_tagged
      t.string :lacp_tagged
      t.string :lamp_tagged
      t.string :link_oam_tagged
      t.string :port_auth_tagged
      t.string :e_lmi_tagged
      t.string :lldp_tagged
      t.string :garp_tagged
      t.string :mrp_b_tagged
      t.string :cisco_bpdu_tagged
      t.string :default_l2cp_tagged
      t.string :lacp_notes_tagged

      t.timestamps
    end
    
    create_table :offered_services_studies do |t|
      t.string :name
      t.date :completion_date
      t.string :reference_documents
      t.integer :cenx_contact_id
      t.integer :member_contact_id
      t.string :sla_calculation_method
      t.string :metrics_details
      t.string :delay_definition
      t.integer :operator_network_type_id

      t.timestamps
    end
    
    create_table :ovcs do |t|
      t.string :buyer_name
      t.string :seller_name
      t.string :ovc_owner_role
      t.string :status
      t.string :description
      t.integer :evc_id
      t.integer :service_type_id
      t.string :cenx_id
      t.integer :primary_contact_id
      t.integer :testing_contact_id
      t.integer :technical_contact_id
      t.integer :operator_network_id

      t.timestamps
    end
    
    create_table :ovc_end_points do |t|
      t.string :description
      t.integer :ovc_id
      t.string :cenx_id
      t.string :sp_uni_id
      t.string :md_format
      t.string :md_level
      t.string :md_name_ieee
      t.string :ma_format
      t.string :ma_name
      t.boolean :is_monitored
      t.integer :demarc_id

      t.timestamps
    end
    
    create_table :service_level_guarantees do |t|
      t.string :min_dist_km
      t.string :max_dist_km
      t.boolean :is_unbounded
      t.string :delay_ms
      t.string :delay_variation_ms
      t.integer :class_of_service_id
      t.string :notes

      t.timestamps
    end
    
    create_table :service_types do |t|
      t.string :name
      t.string :role
      t.integer :offered_services_study_id
      t.string :max_mtu
      t.boolean :c_vlan_cos_preservation
      t.string :color_marking
      t.string :service_category
      t.string :unicast_frame_delivery_conditions
      t.string :multicast_frame_delivery_conditions
      t.string :broadcast_frame_delivery_conditions
      t.string :notes
      t.string :mef9_cert
      t.string :mef14_cert
      t.string :unicast_frame_delivery_details
      t.string :multicast_frame_delivery_details
      t.string :broadcast_frame_delivery_details
      t.string :c_vlan_id_preservation
      t.string :cos_marking_uni
      t.boolean :cos_marking_at_enni_is_pcp
      t.string :cos_marking_at_enni_notes
      t.string :color_marking_notes

      t.timestamps
    end
    
    create_table :uni_profiles do |t|
      t.string :phy_type
      t.boolean :auto_negotiate
      t.integer :mtu
      t.integer :max_num_ovcs
      t.boolean :bundling
      t.boolean :ato_bundling
      t.string :device_Type
      t.boolean :reflection_supported
      t.string :lldp
      t.string :stp
      t.string :rstp
      t.string :mstp
      t.string :evst
      t.string :rpvst
      t.string :eaps
      t.string :pause
      t.string :lacp
      t.string :garp
      t.string :port_auth
      t.integer :service_type_id
      t.string :lacp_notes
      t.boolean :service_multiplexing
      t.string :lamp
      t.string :link_oam
      t.string :e_lmi
      t.string :mrp_b
      t.string :cisco_bpdu
      t.string :default_l2cp
      t.string :sp_name
      t.string :reflection_mechanisms
      t.string :name

      t.timestamps
    end
  end
end
