-- Temporary seed file to populate at least one user in new db
-- run: script/dbconsole < db/seed_user.sql

INSERT INTO `users` (`id`,`email`,`first_name`,`last_name`,`crypted_password`,`salt`,`activation_code`,`last_login_at`,`created_at`,`updated_at`,`service_provider_id`,`password_expiry`,`contact_id`)
VALUES
	(1, 'admin@wirelesswholesaler.com', 'John', 'Doe', '0f65753a0ace0dc602968f793f0e3837b7b1e7f4', '66d75f539d134a033836430441c61cdf68a15222', NULL, NULL, '2011-04-19 00:00:00', '2011-04-19 00:00:00', 9, NULL, NULL);


INSERT INTO `roles` (`id`,`name`,`service_provider_id`,`created_at`,`updated_at`)
VALUES
	(1, 'Administrator', 9, '2011-04-19 00:00:00', '2011-04-19 00:00:00');

INSERT INTO `roles_users` (`role_id`,`user_id`)
VALUES
	(1, 1);
