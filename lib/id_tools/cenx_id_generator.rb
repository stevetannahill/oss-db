module IdTools

  # TODO use logger


class LagIdGenerator
  def self.generate_enni_lag_id site
    # First ID (1) reserved for interchassis link
    id = 2
    while EnniNew.find(:first, :conditions => "lag_id = #{id} and site_id = #{site.id}") != nil
      id += 1
       #logger.info("Try ID: #{id}")
    end
    #TODO deal w upper limit
    return id
  end

end

class ServiceIdGenerator

  def self.generate_on_net_ovc_service_id site
    id = nil
    
    if site.service_id_high < 10000
      SW_ERR "ERROR: #{site.name} has service_id_high < 10000 which are reserved for tests"
      return -1
    end
    taken_service_ids = OnNetOvc.where("service_id <> 'N/A' and service_id >= #{site.service_id_low} and service_id <= #{site.service_id_high}").pluck(:service_id).map{|s| s.to_i}
    max_taken_id = taken_service_ids.empty? ? site.service_id_low : taken_service_ids.max
    max_service_id = [site.service_id_high, max_taken_id+1].min
    available_ids = ((site.service_id_low..max_service_id).to_a) - taken_service_ids
    id = available_ids.first unless available_ids.empty?
#    (site.service_id_low .. (site.service_id_high)).each do |i|
#      if OnNetOvc.find(:all, :conditions => "service_id = #{i}").empty?
#        id = i
#        break
#      end
#    end

    if id.nil?
      SW_ERR "ERROR: No On Net OVC Service ID for #{site.name} available"
      return -1
    end
    
    return id.to_s
  end
end

class CenxIdGenerator
  
#Type ID -> {:class => Class, :string => type_string} lookup table
CLASS_LOOKUP_TABLE = {
  "01" => "ServiceProvider",
  "02" => "EnniOrderNew",
  "03" => "EnniNew",
  "04" => "Evc",
  "05" => "OnNetOvc",
  "06" => "OnNetOvcEndPointEnni",
  "07" => "OvcEndPointEnni",
  "08" => "Uni",
  "09" => "OnNetOvcOrder",
  "10" => "OffNetOvcOrder",
  "11" => "BxCosTestVector",
  "12" => "OffNetOvc",
  "13" => "DemarcOrder",
  "14" => "OvcEndPointUni",
  "15" => "Demarc",
  "16" => "BulkOrder",
  "17" => "SpirentCosTestVector",
  "18" => "IpFlow",
  "19" => "OnNetRouter",
  "20" => "OnNetRouterSubIf"
}
#Type ID -> Type String lookup
STRING_LOOKUP_TABLE = {
  "01" => "sp",
  "02" => "so",
  "03" => "demarc",
  "04" => "path",
  "05" => "segment",
  "06" => "segmentep",
  "07" => "segmentep",
  "08" => "demarc",
  "09" => "so",
  "10" => "so",
  "11" => "costv",
  "12" => "segment",
  "13" => "so",
  "14" => "segmentep",
  "15" => "demarc",
  "16" => "bo",
  "17" => "costv",
  "18" => "path",
  "19" => "segment",
  "20" => "segmentep"
}

#Set appropriate defaults
CLASS_LOOKUP_TABLE.default = nil
STRING_LOOKUP_TABLE.default = "invalid_type"

@@tailmax = 100000000

def self.generate_cenx_id table
  id_head = IdTools::CenxIdGenerator.generate_id_head(table)
  if table.count < @@tailmax
    id = id_head + sprintf("%08d", rand(@@tailmax))
    while table.base_class.send(:find, :first, :conditions => "cenx_id = '#{id}'") != nil
      id = id_head + sprintf("%08d", rand(@@tailmax))
    end
  else
    id = id_head + "#{@@tailmax}"
  end
  return id
end

def self.generate_id_head( table )
  current_version = "10"
  type_id, tab = nil, table
  
  #Force valid class, this will NOT find a parent class
  type_id = CLASS_LOOKUP_TABLE.invert[table.name]

### The method below allows it to be non-strict on children
#  #Search Class and it's ancestors for a valid type
#  until !type_id.nil? or tab.nil?
#    type_id = CLASS_LOOKUP_TABLE.invert[tab.name]
#    tab = tab.superclass
#  end

  type_id = "99" if type_id.nil?
  SW_ERR "CENX ID Gen: ERROR - unable to find a valid type ID for table #{table}" if type_id == "99"
  return "#{current_version}#{type_id}"
end

def self.decode_cenx_id cenx_id
  cenx_id = "" if cenx_id ==nil
  current_version = "10"
  result = true
  result_string = ""
  version_num = (cenx_id.length >= 2) ? cenx_id[0..1] : ""
  type_id = (cenx_id.length >= 4) ? cenx_id[2..3] : ""
  
  
  unless cenx_id.match(/\d*/) && cenx_id.match(/\d*/)[0] == cenx_id
    result = false
    result_string += "* invalid format - must contain only numbers;\n"
  end
  
  if version_num != current_version && version_num != ""
    result = false
    result_string += "* invalid version - first 2 numbers must be #{current_version};\n"
  end

  if type_id.empty?
      type_string = "invalid_type"
      result_string += "* missing type ID: #{type_id} - 3rd and 4th numbers;\n"
      result = false
  else
    table = CLASS_LOOKUP_TABLE[type_id]
    type_string = STRING_LOOKUP_TABLE[type_id]
    result = table.nil? ? false : true
  end
  
  if ( result && cenx_id[4..-1] == "#{@@tailmax}")
    result_string += "* no more valid CENX ID numbers for type: #{type_string};\n"
    result = false
  elsif cenx_id.length != 12
    result = false
    result_string += "* invalid length - not 12 numbers long;\n"
  end
  
  return result, result_string, type_string, table.nil? ? nil : Kernel.const_get(table)
end
 
end

class CenxCmaGenerator

  @@cnamax = 10000

  def self.generate_cenx_cma table
    if table.count() < @@cnamax
      id = sprintf "%04d", rand(@@cnamax)
      while table.base_class.send(:find, :first, :conditions => "cenx_cma LIKE '#{id}-%'") != nil
        id = sprintf "%04d", rand(@@cnamax)
      end
      return "#{id}-1";
    else
      id = "@@cnamax"
    end
    return id
  end

  def self.decode_cenx_cma cenx_cma
    cenx_cma = "" if cenx_cma ==nil
    result = true
    result_string = ""
    version_num = ""
    cma_num = ""
    cma_match = cenx_cma.match(/\A\d{4}-\d\d*\z/)
    
    if ( cenx_cma == "@@cnamax" )
      result_string += "* no more valid CENX CMA numbers;\n"
      result = false
    elsif !cma_match || cma_match[0] != cenx_cma
      result_string += "* malformed cenx_cma;\n"
      result = false
    else
      version_num = cenx_cma[5..-1]
      cma_num = cenx_cma[0..3]
    end
    
    return result, result_string, cma_num, version_num
  end
 
end

end
