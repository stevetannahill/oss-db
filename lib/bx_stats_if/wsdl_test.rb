require 'rubygems'
require "soap/wsdlDriver"
require "savon"

a_wsdl ="http://localhost:3000/admin/render_bx_auth"
#a_wsdl ="file:///Users/Dave/work/code/cenx-oss-db/lib/bx_stats_if/wsdl/Authentication.wsdl"
de_wsdl ="http://localhost:3000/admin/render_bx_de"
#de_wsdl ="file:///Users/Dave/work/code/cenx-oss-db/lib/bx_stats_if/wsdl/DataExtraction.wsdl"
u_wsdl ="http://localhost:3000/admin/render_bx_util"
#u_wsdl ="file:///Users/Dave/work/code/cenx-oss-db/lib/bx_stats_if/wsdl/Utils.wsdl"

#a_driver = SOAP::WSDLDriverFactory.new(a_wsdl).create_rpc_driver
#puts a_driver.display
#puts a_driver.endpoint_url
##puts a_driver.instance_variables
#puts a_driver.singleton_methods
##a_driver.authenticate "dave" "xxxx"
#puts "--------------------------"
#
#de_driver = SOAP::WSDLDriverFactory.new(de_wsdl).create_rpc_driver
#puts de_driver.display
#puts de_driver.endpoint_url
##puts de_driver.instance_variables
#puts de_driver.singleton_methods
#
#puts "--------------------------"
#
#u_driver = SOAP::WSDLDriverFactory.new(u_wsdl).create_rpc_driver
#puts u_driver.display
#puts u_driver.endpoint_url
##puts u_driver.instance_variables
#puts u_driver.singleton_methods
#
#puts "****************************"
#puts "****************************"

a_client = Savon::Client.new a_wsdl
puts a_client.wsdl.soap_actions
puts a_client.wsdl.namespace_uri

response = a_client.authenticate do |soap|
  soap.body = { :username => 'administrator',  :password => 'admin'}
end

#a_client.authenticate

#puts "--------------------------"
#
#de_client = Savon::Client.new de_wsdl
#puts de_client.wsdl.soap_actions
#puts de_client.wsdl.namespace_uri
#
#puts "--------------------------"
#
#u_client = Savon::Client.new u_wsdl
#puts u_client.wsdl.soap_actions
#puts u_client.wsdl.namespace_uri
#
#puts "--------------------------"



#x_driver  = SOAP::WSDLDriverFactory.new("http://www.jasongaylord.com/webservices/zipcodes.asmx?wsdl").create_rpc_driver
#puts x_driver.display
#puts x_driver.endpoint_url
##puts x_driver.instance_variables
#puts x_driver.singleton_methods
#
#
#
#class SomeDotNetWrapper
#  attr_accesssor :endpoint, :service
#
#  def initialize(endpoint=nil, service=nil)
#    @endpoint = endpoint
#    @service  = service
#  end
#
#  def get_user_id_from_credentials(username, password)
#    soap = wsdl.create_rpc_driver
#    response = soap.GetUserID(:username => username, :password => password)
#    soap.reset_stream
#    response.getUserIDResult
#  end
#
#  private
#    def wsdl
#      SOAP::WSDLDriverFactory.new("http://#{@endpoint}/services/#{@service}.asmx?WSDL")
#    end
#end
#
#class SomeDotNetWrapperController < ApplicationController
#
#  def store_other_id
#    raise InvalidCredentials if params[:username].blank? || params[:password].blank?
#    service = SomeDotNetWrapper.new('http://example.com', 'authentication')
#    other_id = service.get_user_id_from_credentials(params[:username], params[:password])
#    current_user.update_attribute(:other_id, other_id) unless other_id.to_i == 0
#  rescue InvalidCredentials
#    # ...
#  end
#end


