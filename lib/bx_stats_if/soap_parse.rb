require File.dirname(__FILE__) + "/bx_if_common"
require File.dirname(__FILE__) + "/bx_soap_interface"

file_to_parse = ARGV[0]
unless file_to_parse
  puts"No file specified"
  exit(1)
end

file = File.new(file_to_parse, 'r')

resp_string = ''
file.each_line do |line|
  resp_string += line
end

doc = REXML::Document.new resp_string

result = doc.elements["soap:Envelope/soap:Body/getTestDataResponse/result"].get_text.value
#TODO handle result
puts "Result: #{result}"

query_token = doc.elements["soap:Envelope/soap:Body/getTestDataResponse/query_token"].get_text.value
puts "query_token #{query_token}"

more_data = doc.elements["soap:Envelope/soap:Body/getTestDataResponse/more_data"].get_text.value
puts "more_data #{more_data}"

data = []
h = {}
test_data_path = "soap:Envelope/soap:Body/getTestDataResponse/response_data/TestDataObject"

time_stamp = doc.elements["#{test_data_path}/brix_time_stamp"].get_text.value
h["time_stamp"] = time_stamp

test_instance_class_id = doc.elements["#{test_data_path}/test_instance_class_id"].get_text.value
h["test_instance_class_id"] = test_instance_class_id

test_result_path = "#{test_data_path}/data"
doc.elements.each(test_result_path) {
    |tr|
    tr.elements.each{ |result|
      h[result.elements[1].get_text.value] = result.elements[2].get_text.value
      }
  }

data << h
p data
