require 'socket'      # Sockets are in standard library
require File.dirname(__FILE__) + "/bx_if_common"
module BxIf

class StatsApi

  def initialize(nms)
    @server = nil
    @nms_id = nil   

    if nms != nil
      @nms_id = nms.id
      puts "Connecting to server at: #{BxIfCommon::BX_IF_SERVER_IP}:#{BxIfCommon::BX_IF_SERVER_PORT}"
      begin
        @server = TCPSocket.open(BxIfCommon::BX_IF_SERVER_IP, BxIfCommon::BX_IF_SERVER_PORT)
      rescue Errno::EHOSTUNREACH, Errno::ENETUNREACH, Errno::ENOTCONN, Errno::EBADF, Errno::ECONNREFUSED, Errno::ECONNRESET
        SW_ERR "Exception trying to open socket #{BxIfCommon::BX_IF_SERVER_PORT} on Address #{BxIfCommon::BX_IF_SERVER_IP}. Exception: #{$!}"
        @server = nil
      end
    else
      SW_ERR "NMS is nil"
    end      
  end


  def get_parameter_values test_name, sla_id

    filters = {
      'sla_id' => sla_id,
      'test_name' => test_name
    }

    msg = BxIfCommon::BxIfMsg.new(@nms_id, 'get_parameter_values', filters)

    return send_request(msg)

  end

  def get_test_instances test_name, sla_id, service_instance_id

    filters = {
      'sla_id' => sla_id,
      'service_instance_id' => service_instance_id,
      'test_name' => test_name
    }

    msg = BxIfCommon::BxIfMsg.new(@nms_id, 'get_test_instances', filters)

    return send_request(msg)
  end

  def get_test_data test_name, sla_id, start_time, end_time, test_instance_class_id, result_list

    filters = {
      'sla_id' => sla_id,
      'test_name' => test_name,
      'start_time' => start_time,
      'end_time' => end_time,
      'test_instance_class_id' => test_instance_class_id,
      'result_list' => result_list + ["Time Stamp"]      
    }

    msg = BxIfCommon::BxIfMsg.new(@nms_id, 'get_test_data', filters)

    return send_request(msg)
  end

  def get_sla_id sla_name

    filters = {
      'sla_name' => sla_name
    }

    msg = BxIfCommon::BxIfMsg.new(@nms_id, 'get_sla_id', filters)

    return send_request(msg)
  end

  def get_service_instances sla_id

    filters = {
      'sla_id' => sla_id
    }
    msg = BxIfCommon::BxIfMsg.new(@nms_id, 'get_service_instances', filters)

    return send_request(msg)
  end

  def send_request(msg)
    # If the server is not connected return a failure
    if @server == nil
      return false, []
    end
    
    begin
      raw_data = Marshal.dump(msg)
      puts "Sending msg: #{raw_data.size} #{Time.now}"
      serialized_size = sprintf "%08d", raw_data.size
      @server.write serialized_size
      @server.write raw_data

      msg_size =  @server.recv( 8 )
      puts "Reply Msg Size: #{msg_size}"

      result = false
      reply = ""
      while reply.size < msg_size.to_i
        reply += @server.recv( 200000 )
      end

      puts "Got Reply w size: #{reply.size} #{Time.now}"
      data = Marshal.load(reply)
    rescue
      SW_ERR "Exception in sending request to Brix #{$!}"
      data = []
    end

    puts "Number of Records: #{data.size}"

    if(data.size >0)
      result = true
    end

    return result, data
  end

  def close
    @server.close if @server != nil
  end

end

end


