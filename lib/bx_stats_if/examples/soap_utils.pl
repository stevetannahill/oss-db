#!/bin/env brixperl
# $Id: soap_utils.pl,v 1.4 2010/05/17 19:54:34 harrison Exp $

# This example script calls the Authenticate
# function of the SOAP API.

use strict;
use Data::Dumper;
$Data::Dumper::Indent   = 1;
$Data::Dumper::Deepcopy = 1;
use SOAP::Lite;

#SOAP::Lite->import(+trace => 'all');
use HTTP::Cookies;
use Timer;

# Modify these to match your system settings
my $SERVER = 'localhost:8080';
my $UNAME  = 'administrator';
my $PWORD  = 'admin';

# Establish the request. Authentication is
# managed through API/SOAP/Auth, but other
# functions are available through:
#
# - API/SOAP/Utils           (Top-level object functions)
# - API/SOAP/DataExtraction  (Data extraction functions)
#
my $request = SOAP::Lite->new(
    uri   => 'API/SOAP/Auth',
    proxy => "http://${SERVER}/API/SOAP/"
);

# Call the desired function as a method. Note that input
# parameters must be packaged as SOAP data.
my $result = eval {
    $request->Authenticate(
        SOAP::Data->name( uname => $UNAME ),
        SOAP::Data->name( pword => $PWORD ),
    );
};
if ($@) {
    print STDERR "Request error: $@\n";
    exit;
}
print "\n-------------------------------------\n";
print "         Authenticate Output         \n";
print "-------------------------------------\n";

# Print out the response.
print Dumper( $result->body() );

## Pull out the auth token
my $resp = $result->body();

# Someone writing an actual client app would need to put a fair amount of
# error handling in here. This test script just grabs the token without
# checking for success first.

my $token = $resp->{AuthenticateResponse}{token};

# Get slas
my $utils_request = SOAP::Lite->new(
    uri   => 'API/SOAP/Utils',
    proxy => "http://${SERVER}/API/SOAP/",
);

# Get Slas
get_slas( token => $token, utils_request => $utils_request );

# Get Services
get_services( token => $token, utils_request => $utils_request );

# Get Service Instances
get_service_instances( token => $token, utils_request => $utils_request );

# Get Verifiers
get_verifiers( token => $token, utils_request => $utils_request );

# Get Tests
get_tests( token => $token, utils_request => $utils_request );

# HELPER SUBS
sub get_slas {
    my %args          = @_;
    my $token         = $args{token};
    my $utils_request = $args{utils_request};

    # As per the design doc, we can pass exactly one of the sla_names, sla_ids, and sla_match.
    # We are passing sla_names here.
    my @sla_names = qw (API-TEST SOAP_SLA);
    my @sla_ids   = ();
    my $sla_match;

    my $result = eval {
        $utils_request->getSLAS(
            SOAP::Data->name( authority      => $token ),
            SOAP::Data->name( sla_names      => \@sla_names ),
            SOAP::Data->name( sla_ids        => undef ),
            SOAP::Data->name( sla_match      => undef ),
            SOAP::Data->name( case_sensitive => 1 )
        );
    };
    if ($@) {
        print STDERR "Request error for SLAS: $@\n";
    }
    print "\n-------------------------------------\n";
    print "           getSLAs' Output           \n";
    print "-------------------------------------\n";
    print "Successful Output: \n" . Dumper( $result->body() );

    $result = eval {
        $utils_request->getSLAS(
            SOAP::Data->name( authority      => $token ),
            SOAP::Data->name( sla_names      => \@sla_names ),
            SOAP::Data->name( sla_ids        => undef ),
            SOAP::Data->name( sla_match      => 'API' ),
            SOAP::Data->name( case_sensitive => 1 )
        );
    };
    if ($@) {
        print STDERR "Request error for SLAS: $@\n";
    }
    print "Output for too many arguments: \n" . Dumper( $result->body() );

    @sla_names = qw('FR_64byte_National' 'Test_API');
    $result    = eval {
        $utils_request->getSLAS(
            SOAP::Data->name( authority      => $token ),
            SOAP::Data->name( sla_names      => \@sla_names ),
            SOAP::Data->name( sla_ids        => undef ),
            SOAP::Data->name( sla_match      => undef ),
            SOAP::Data->name( case_sensitive => 1 )
        );
    };
    if ($@) {
        print STDERR "Request error for SLAS: $@\n";
    }
    print "Output for wrong input: \n" . Dumper( $result->body() );
}

sub get_services {
    my %args          = @_;
    my $token         = $args{token};
    my $utils_request = $args{utils_request};
    my @service_names = ("One-Way Latency");
    my @service_ids   = qw(1001 1002);
    my $service_match = undef;

    my $result = eval {
        $utils_request->getServices(
            SOAP::Data->name( authority      => $token ),
            SOAP::Data->name( service_names  => \@service_names ),
            SOAP::Data->name( service_ids    => undef ),
            SOAP::Data->name( service_match  => $service_match ),
            SOAP::Data->name( case_sensitive => 1 )
        );
    };

    if ($@) {
        print STDERR "Request error for Service: $@\n";
    }
    print "\n-------------------------------------\n";
    print "           getServices' Output           \n";
    print "-------------------------------------\n";
    print "Successful Output :\n" . Dumper( $result->body() );

    # Request for more than one argument
    $result = eval {
        $utils_request->getServices(
            SOAP::Data->name( authority      => $token ),
            SOAP::Data->name( service_names  => \@service_names ),
            SOAP::Data->name( service_ids    => \@service_ids ),
            SOAP::Data->name( service_match  => $service_match ),
            SOAP::Data->name( case_sensitive => 1 )
        );
    };
    if ($@) {
        print STDERR "Request error for Service: $@\n";
    }
    print "Output for too many arguemnts :\n" . Dumper( $result->body() );

    # Request for wrong values
    @service_ids = qw (1050 1051);
    $result      = eval {
        $utils_request->getServices(
            SOAP::Data->name( authority      => $token ),
            SOAP::Data->name( service_names  => undef ),
            SOAP::Data->name( service_ids    => \@service_ids ),
            SOAP::Data->name( service_match  => undef ),
            SOAP::Data->name( case_sensitive => 1 )
        );
    };
    if ($@) {
        print STDERR "Request error for Service: $@\n";
    }
    print "Output for wrong input :\n" . Dumper( $result->body() );
}

sub get_service_instances {
    my %args          = @_;
    my $token         = $args{token};
    my $utils_request = $args{utils_request};

    my $sla_id     = qw (1025);
    my $service_id = undef;

    my $result = eval {
        $utils_request->getServiceInstances(
            SOAP::Data->name( authority      => $token ),
            SOAP::Data->name( sla_id         => $sla_id ),
            SOAP::Data->name( service_id     => $service_id ),
            SOAP::Data->name( case_sensitive => 1 )
        );

    };
    if ($@) {
        print STDERR "Request error for Service: $@\n";
    }
    print "\n-------------------------------------\n";
    print "     getServiceInstances' Output     \n";
    print "-------------------------------------\n";

    print "Successful Output: \n" . Dumper( $result->body() );

    $result = eval {
        $utils_request->getServiceInstances(
            SOAP::Data->name( authority      => $token ),
            SOAP::Data->name( sla_id         => 1002 ),
            SOAP::Data->name( service_id     => $service_id ),
            SOAP::Data->name( case_sensitive => 1 )
        );

    };
    if ($@) {
        print STDERR "Request error for Service Instances: $@\n";
    }
    print "Output for wrong input: \n" . Dumper( $result->body() );
}

sub get_verifiers {
    my %args          = @_;
    my $token         = $args{token};
    my $utils_request = $args{utils_request};

    my @verifier_names = ( 'API - 1', 'API - 3' );
    my @verifier_ids = qw (1004 1005);
    my $verifier_match;

    # Correct output
    my $result = eval {
        $utils_request->getVerifiers(
            SOAP::Data->name( authority      => $token ),
            SOAP::Data->name( verifier_names => \@verifier_names ),
            SOAP::Data->name( verifier_ids   => undef ),
            SOAP::Data->name( verifier_match => undef ),
            SOAP::Data->name( case_sensitive => undef )
        );
    };
    if ($@) {
        print STDERR "Request error for Verfier: $@\n";
    }
    print "\n-------------------------------------\n";
    print "         getVerifiers' Output         \n";
    print "-------------------------------------\n";
    print "Successful Output\n" . Dumper( $result->body() );

    $result = eval {
        $utils_request->getVerifiers(
            SOAP::Data->name( authority      => $token ),
            SOAP::Data->name( verifier_names => \@verifier_names ),
            SOAP::Data->name( verifier_ids   => undef ),
            SOAP::Data->name( verifier_match => 'API' ),
            SOAP::Data->name( case_sensitive => 1 )
        );
    };
    if ($@) {
        print STDERR "Request error for Verfier: $@\n";
    }
    print "Output for too many arguments\n" . Dumper( $result->body() );

    # Output with cardinality match failure
    $result = eval {
        $utils_request->getVerifiers(
            SOAP::Data->name( authority      => $token ),
            SOAP::Data->name( verifier_names => undef ),
            SOAP::Data->name( verifier_ids   => \@verifier_ids ),
            SOAP::Data->name( verifier_match => undef ),
            SOAP::Data->name( case_sensitive => 1 )
        );
    };
    if ($@) {
        print STDERR "Request error for Verfier: $@\n";
    }
    print "Output for wrong input \n" . Dumper( $result->body() );
}

sub get_tests {
    my %args          = @_;
    my $token         = $args{token};
    my $utils_request = $args{utils_request};

    my @test_names     = qw ();
    my $test_match     = undef;
    my $verifier_match = 'API';
    my $sla_id         = '';
    my $module_name    = 'com.brixnet.abbronewayactivetest.17.2581';

    # Correct output
    my $result = eval {
        $utils_request->getTests(
            SOAP::Data->name( authority      => $token ),
            SOAP::Data->name( module_name    => $module_name ),
            SOAP::Data->name( test_names     => undef ),
            SOAP::Data->name( test_match     => $test_match ),
            SOAP::Data->name( verifier_match => undef ),
            SOAP::Data->name( sla_id         => $sla_id ),
            SOAP::Data->name( case_sensitive => 1 )
        );
    };
    if ($@) {
        print STDERR "Request error for Tests: $@\n";
    }
    print "\n-------------------------------------\n";
    print "           getTests' Output           \n";
    print "-------------------------------------\n";
    print "Successful Output :\n" . Dumper( $result->body() );

    my $result = eval {
        $utils_request->getTests(
            SOAP::Data->name( authority      => $token ),
            SOAP::Data->name( module_name    => $module_name ),
            SOAP::Data->name( test_names     => undef ),
            SOAP::Data->name( test_match     => undef ),
            SOAP::Data->name( verifier_match => $verifier_match ),
            SOAP::Data->name( sla_id         => $sla_id ),
            SOAP::Data->name( case_sensitive => 1 )
        );
    };
    if ($@) {
        print STDERR "Request error for Tests: $@\n";
    }
    print "Output for too many arguments: \n" . Dumper( $result->body() );

    # Output for wrong values
    $result = eval {
        $utils_request->getTests(
            SOAP::Data->name( authority      => $token ),
            SOAP::Data->name( module_name    => $module_name ),
            SOAP::Data->name( test_names     => undef ),
            SOAP::Data->name( test_match     => undef ),
            SOAP::Data->name( verifier_match => undef ),
            SOAP::Data->name( sla_id         => 1002 ),
            SOAP::Data->name( case_sensitive => 1 )
        );
    };
    if ($@) {
        print STDERR "Request error for Tests: $@\n";
    }
    print "Output for wrong input: \n" . Dumper( $result->body() );

}
1;
