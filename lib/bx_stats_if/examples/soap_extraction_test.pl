#!/bin/env brixperl
# $Id: soap_extraction_test.pl,v 1.3 2010/05/17 19:54:34 harrison Exp $

# This example script calls the Authenticate
# function of the SOAP API.

use strict;
use Data::Dumper;
$Data::Dumper::Indent=1;
$Data::Dumper::Deepcopy=1;
use SOAP::Lite;
use MIME::Base64;
use Storable qw(nfreeze);

#SOAP::Lite->import(+trace => 'all');
use HTTP::Cookies;
use Timer;

# Modify these to match your system settings
my $SERVER = 'localhost:8080';
my $UNAME  = 'administrator';
my $PWORD  = 'admin';

# Establish the request. Authentication is
# managed through API/SOAP/Auth, but other
# functions are available through:
#
# - API/SOAP/Utils           (Top-level object functions)
# - API/SOAP/DataExtraction  (Data extraction functions)
#
my $request = SOAP::Lite->new
    ( uri   => 'API/SOAP/Auth',
      proxy => "http://${SERVER}/API/SOAP/" );

# Call the desired function as a method. Note that input
# parameters must be packaged as SOAP data.
my $result = 
eval {
    $request->Authenticate
	( SOAP::Data->name(uname => $UNAME),
	  SOAP::Data->name(pword => $PWORD),
	);
};
if ($@) {
    print STDERR "Request error: $@\n";
    exit;
}

# Print out the response.
print Dumper($result->body());    

## Pull out the auth token
my $resp  = $result->body();

# Someone writing an actual client app would need to put a fair amount of
# error handling in here. This test script just grabs the token without
# checking for success first.

my $token = $resp->{AuthenticateResponse}{token};

my $extraction_request = SOAP::Lite->new
( uri   => "API/SOAP/DataExtraction",
  proxy => "http://${SERVER}/API/SOAP/", 
);

get_test_parameters(  token => $token, extraction_request => $extraction_request);
get_test_instances(   token => $token, extraction_request => $extraction_request);
get_test_result_info( token => $token, extraction_request => $extraction_request);
get_test_data(        token => $token, extraction_request => $extraction_request);

# HELPER SUBS
sub get_test_parameters {
    my %args        = @_;
    my $token       = $args{token};
    my $extraction_request = $args{extraction_request};
    my $result    =
    eval {
        $extraction_request->getParameterValues
            ( SOAP::Data->name(authority    => $token),
              SOAP::Data->name(soap_test_instance  => \SOAP::Data->value(
                                                      SOAP::Data->name( test_instance_id => 1002),
                                                      SOAP::Data->name( test_instance_class_id => 1002),
            )));
    };
    if ($@) {
      print STDERR "Request error: $@\n";
    }

    print Dumper $result->body;
}

sub get_test_instances {
    my %args        = @_;
    my $token       = $args{token};
    my $extraction_request = $args{extraction_request};

    my $result    =
    eval {
        $extraction_request->getTestInstances
            ( SOAP::Data->name(authority    => $token),
              SOAP::Data->name(soap_filter_object  => \SOAP::Data->value(
                                                      SOAP::Data->name( sla_id    => '1000'),
                                                      SOAP::Data->name( test => \SOAP::Data->value( 
                                                                                        SOAP::Data->name( test_name => 'com.brixnet.onewayactivetest.17.2581')))
                                                  )));
    };
    if ($@) {
      print STDERR "Request error: $@\n";
    }
    print Dumper $result->body;
}

sub get_test_result_info {
    my %args        = @_;
    my $token       = $args{token};
    my $extraction_request = $args{extraction_request};

    my $result    =
    eval {
        $extraction_request->getTestResultInfo
            ( SOAP::Data->name(authority    => $token),
              SOAP::Data->name(soap_test  => \SOAP::Data->value(
                                                      SOAP::Data->name( test_name => 'com.brixnet.onewayactivetest.17.2581')))
            );
    };
    if ($@) {
      print STDERR "Request error: $@\n";
    }

    print Dumper $result->body;
}

sub get_test_data {
    my %args        = @_;
    my $token       = $args{token};
    my $extraction_request = $args{extraction_request};

    my $result    =
    eval {
        $extraction_request->getTestData
            ( SOAP::Data->name(authority     => $token),
              SOAP::Data->name(filter_object => \SOAP::Data->value(
                                                      SOAP::Data->name( test => \SOAP::Data->value(
                                                                                        SOAP::Data->name( test_name => 'com.brixnet.abbronewayactivetest.17.2581'))),
                                                      SOAP::Data->name(start_time  => '04/28/10 11:00:00'),
                                                      SOAP::Data->name(end_time    => '04/28/10 12:10:00'),
                                                      SOAP::Data->name(sla_ids  => (1025)))),
              SOAP::Data->name(test_result_list => \SOAP::Data->value(
                                                      SOAP::Data->name(result_name  => 'Number of Packets')),
                                                    \SOAP::Data->value(
                                                      SOAP::Data->name(result_name  => 'Minimum UDP Latency to Responder')),
                                                    \SOAP::Data->value(
                                                      SOAP::Data->name(result_name  => 'Maximum UDP Latency to Responder')),
                                                    \SOAP::Data->value(
                                                      SOAP::Data->name(result_name  => 'Average UDP Latency to Responder')),
                                                    )
            );
    };
    if ($@) {
      print STDERR "Request error: $@\n";
    }
    print Dumper $result->body;

    while ($result->body->{getTestDataResponse}{more_data}) {
        $result    =
        eval {
            $extraction_request->getTestData
                ( SOAP::Data->name(authority     => $token),
                  SOAP::Data->name(query_token   => $result->body->{getTestDataResponse}{query_token})  
                );
        };
        if ($@) {
            print STDERR "Request error: $@\n";
        }

        print Dumper $result->body;
    }
}
1;
