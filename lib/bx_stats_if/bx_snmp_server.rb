require 'rubygems'
require 'snmp'
require 'logger'
require 'cenx_name_generator'

BRIX_MIB_DIR = "#{Rails.root}/lib/bx_stats_if/brixmib"
BRIX_MIB_TMP_DIR = "#{Rails.root}/tmp/brix_auto_gen_mibs"
MAX_LOG_SIZE = 10*1024*1024

# There are no defintions from the MIB files that can be used to map overall result value to the text so it's coded here
OVERALL_RESULT_VALUE = ["SUCCESSFUL", "NO_RESPONSE", "INVALID_RESPONSE_DOMAIN", "INVALID_RESPONSE_VLAN_ID", "INVALID_RESPONSE_VLAN_PRI", "INVALID_RESPONSE_SEQ_NUM"]

class FormatLogger < Logger
  def format_message(severity, timestamp, progname, msg)
    "#{timestamp.to_s} #{severity} #{Thread.current} #{msg}\n"
  end
end

def setup
  # set up logging
  logger = FormatLogger.new("#{Rails.root}/log/#{File.basename(__FILE__, '.*')}.log", 1, MAX_LOG_SIZE)
  Rails.logger = logger # Give logger access to all ruby objects

  # Don't log SQL queries
  ActiveRecord::Base.logger = nil  
  ActiveRecord::Base.clear_active_connections!

  # read and return the brix configuration
  brix_cfg = YAML::load(IO.read("#{Rails.root}/config/brix.yml"))[Rails.env] 

  logger.level = Logger::Severity.const_get(brix_cfg["loglevel"].upcase)
  # Log the ActionMailer
  ActionMailer::Base.logger = Rails.logger
  
  return brix_cfg
end

def import_brix_mibs
  # Generate the file needed by the SNMP lib from the MIB files
  #sudo apt-get install libsmi2ldbl
  SNMP::MIB.import_module("-f python #{BRIX_MIB_DIR}/mibs/common/common/brix-global-reg.mib", BRIX_MIB_TMP_DIR)
  SNMP::MIB.import_module("-p #{BRIX_MIB_DIR}/mibs/common/common/brix-global-reg.mib -f python #{BRIX_MIB_DIR}/mibs/common/common/brix-tc-mib.mib", BRIX_MIB_TMP_DIR)
  SNMP::MIB.import_module("-p #{BRIX_MIB_DIR}/mibs/common/common/brix-global-reg.mib -p #{BRIX_MIB_DIR}/mibs/common/common/brix-tc-mib.mib -f python #{BRIX_MIB_DIR}/mibs/verifier/verifier/brixtest.mib", BRIX_MIB_TMP_DIR)
  # Notice the -k otherwise we get an error about Gauge32 - not sure what this may affect!!
  SNMP::MIB.import_module("-k -p #{BRIX_MIB_DIR}/mibs/common/common/brix-global-reg.mib -p #{BRIX_MIB_DIR}/mibs/common/common/brix-tc-mib.mib -p #{BRIX_MIB_DIR}/mibs/verifier/verifier/brixtest.mib -f python #{BRIX_MIB_DIR}/mibs/verifier/verifier/brixverifier.mib", BRIX_MIB_TMP_DIR)
  SNMP::MIB.import_module(" -p #{BRIX_MIB_DIR}/mibs/common/common/brix-global-reg.mib -p #{BRIX_MIB_DIR}/mibs/common/common/brix-tc-mib.mib -p #{BRIX_MIB_DIR}/mibs/verifier/verifier/brixtest.mib -f python #{BRIX_MIB_DIR}/mibs/bxwxsrv/bxwxsrv/brixworx-server-mib.mib", BRIX_MIB_TMP_DIR)
end

def trap_handler(trap, mib, mibs, mib_names)
  Rails.logger.info "Got SNMP trap from #{trap.source_ip}"
  data = {}
  trap.each_varbind {|var| 
    data[mibs.index(var.name.to_s)] = var.value.to_s
    Rails.logger.debug "#{var.name.to_s} #{mibs.index(var.name.to_s)} value = #{var.value.to_s}"
  }
#  if $brix_servers[trap.source_ip] == nil
#    SW_ERR "Received a trap from a non BrixWorx server ip = #{trap.source_ip} Known Brix Servers = #{$brix_servers.keys.inspect}"
#    return
#  end
  
  test_type = data["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapTestName"]
  
  state = ""
  case mibs.index(trap.varbind_list.find {|v| v.name.to_s == mib.oid("SNMPv2-MIB::snmpTrapOID.0").to_s}.value.to_s)
  when "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapThresholdPassingTrap"
    state = "cleared"
  when "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapThresholdWarningTrap"
    state = "warning"
  when "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapThresholdFailingTrap"
    state = "failed"
  when "SNMPv2-MIB::coldStart"
    Rails.logger.info "SNMP coldStart received"
    resync(trap.source_ip)
  when "SNMPv2-MIB::warmStart"
    Rails.logger.info "SNMP warmStart received"
  else
    SW_ERR "Unknown Trap received #{trap.inspect}"
  end
  
  if !state.empty?
    Rails.logger.info "SNMP trap from verifier #{data["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapVerifierName"]}"
    # Get the time of the trap
    name = data["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapTestTarget"] + " " + data["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapSLAName"]
    time_values = data["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapTime"].unpack("nccccccacc")
    if time_values[7..9] == ["", nil, nil]
      # The time is localtime of the Brix server best we can do is to use local
      time = Time.local(time_values[0], time_values[1], time_values[2], time_values[3], time_values[4], time_values[5]).to_i*1000
    else
      # There is a +/- offset from UTC yyyy-mm-dd hh:mm:ss +/-hhmm"
      time = Time.parse("#{time_values[0]}-#{time_values[1]}-#{time_values[2]} #{time_values[3]}:#{time_values[4]}:#{time_values[5]} #{time_values[7]}#{"%02d" % time_values[8]}#{"%02d" % time_values[9]}").to_i*1000
    end
    # Extract the results data
    expected_results = ["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultName", "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultValue", "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultStatus"]
    results = trap.varbind_list.find_all {|r| !([mibs.index(r.name.to_s)] & expected_results).empty?}.in_groups_of(3).collect {|d| Hash[*d.collect {|r| [mibs.index(r.name.to_s), r.value.to_s]}.flatten]}
    
    # Remove all results that are Ok and sort from error down to warning
    results.delete_if {|r| r["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultStatus"].to_i <= 2}
    results.sort {|a,b| b["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultStatus"].to_i <=> a["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultStatus"].to_i}
    
    # Convert the numeric status codes to strings and rename the test names for display
    status_msg = {2 => "Ok", 3 => "Warning", 4 => "Failed"}
    test_name = case 
    when test_type[/^com.brixnet.swv.ethloopbacktest/]
      {"result" => "result", "percentFramesLost" => "flr", "delayVariationAvg" => "dv", "delayAvg" => "delay"}
    when test_type[/^com.brixnet.swv.ethframedelaytest/]
      {"result" => "result", "percentFramesLost" => "flr", "delayVariationAvg" => "dv", "delayAvg" => "delay"}
    when test_type[/^com.brixnet.pingactivetest/]
      {"result" => "result", "percentLostPackets" => "flr", "avgJitter" => "dv", "endToEndDelayAvg" => "delay"}
    else
      SW_ERR "Unknown Test Type received #{test_type}"
    end
    
    # Replace the ResultName with the ones from the hash - Note any ResaultNames which don't match will be converted to nil and ignored
    results.each {|r| 
      r["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultStatus"] = status_msg[r["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultStatus"].to_i]
      r["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultName"] = test_name[r["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultName"]]
    }
    
    # Build the details
    details = "Internal Error - No Alarm History found "
    ah = BrixAlarmHistory.find_by_event_filter(name)
    if ah != nil
      # For BrixWorx assumption is one AlarmHistory per CosEndPoint
      target_obj = ah.event_ownerships.first.eventable_type.constantize.find_by_id(ah.event_ownerships.first.eventable_id)
      details = ""
      results.each do |r|
        test = r["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultName"]
        # Ignore any Results which we are not interested in
        next if test.blank?
        
        if ["flr", "dv", "delay"].include?(test)
          error_threshold = target_obj.send("#{test}_error_threshold")
          warning_threshold = target_obj.send("#{test}_warning_threshold")
        end
        case test
          when 'flr'
            actual_value = r["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultValue"].to_f
          when 'dv'
            actual_value = r["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultValue"].to_f/2.0
          when 'delay'
            actual_value = (r["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultValue"].to_f/1000)/2.0
          when 'result'
            # No SLAs
            error_threshold = nil
            warning_threshold = nil
            
            actual_value = OVERALL_RESULT_VALUE[r["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultValue"].to_i] 
            if actual_value.nil?
              # Could not find the text to log the number & SWERR
              actual_value = "Brix result value is #{r["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultValue"]}"
              SW_ERR "Brix unknown Result value #{r["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultValue"]}"
            end            
        end
        details << BrixAlarmHistory.details_formatter(test, r["BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultStatus"], actual_value, warning_threshold, error_threshold) + "\n"       
      end   
    end          
    BrixAlarmHistory.process_event({:timestamp => time, :event_filter => name, :state => state, :details => details})
  end
end  

def resync(server_ip)
  # If there are a lot of alarms active (100's of them) this could take some time
  # Start the resync in a thread.
  if $brix_servers[server_ip] == nil 
    SW_ERR "Failed to find Brix server with IP address #{server} in CDB"
    return nil
  end
  thread = Thread.new(server_ip) do |server|
    begin
      Rails.logger.info "BrixAlarm resync thread starting for #{server}"
      $brix_servers[server].synchronize do
        nms = NetworkManagementServer.all.find {|nmserver| nmserver.primary_ip == server && nmserver.network_manager.nm_type == "BrixWorx"}.network_manager 
        max_attempts = 2
        resync_attempts = 0
        success = false
        while !success && (resync_attempts < max_attempts) do   
          Rails.logger.info "BrixAlarm resync starting for #{server}"
          begin
            success = BrixAlarmHistory.alarm_sync_all(nms)
          rescue
            Rails.logger.error "Exception in resync thread for #{server} #{$!} #{$!.backtrace.join("\n")}"
          end
          Rails.logger.info "BrixAlarm resync ending for #{server} success = #{success} Attempt #{resync_attempts}"
          sleep(30) if !success
          resync_attempts += 1
        end
      end
      Rails.logger.info "BrixAlarm resync thread exiting for #{server}"
    rescue
      Rails.logger.info "Exception in resync for #{server} #{$!} #{$!.backtrace.join("\n")}"
    end
  end
  return thread
end  
  
def check_nms_servers
  $brix_servers_mutex.synchronize do    
    # remove/add any old/new NMS
    # Remove any network managers that no longer exist 
    all_ips = NetworkManagementServer.all.collect {|server| server.primary_ip if server.network_manager.nm_type == 'BrixWorx'}.compact
  
    $brix_servers.delete_if {|ip, value| !all_ips.include?(ip)}
 
    # Add any new BrixWorx servers
    all_ips.each do |ip|
      if $brix_servers[ip] == nil
        # add a Brixworx server
        $brix_servers[ip] = Mutex.new
        resync(ip) 
      end
    end
  end
end  

  # Main line begins    
  brix_cfg = setup
  Rails.logger.info "Server starting"
  
  import_brix_mibs

  mib_names = ["SNMPv2-MIB::snmpTrapOID.0", "SNMPv2-MIB::coldStart", "SNMPv2-MIB::warmStart",
    "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapThresholdPassingTrap", "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapThresholdWarningTrap", "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapThresholdFailingTrap",
    "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapTime", "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapVerifierName", "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapSLAName", 
    "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapServiceName", "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapTestName", "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapTestTarget", 
    "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapVerifierId", "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapSLAId", "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapServiceId", 
    "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapServiceInstId", "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapTestId", "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapTestInstId",
    "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapTestStatus", "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultName", "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultValue",
    "BRIXWORX-SERVER-MIB::bxWxSrvTestLevelTrapResultStatus"]

  mib = SNMP::MIB.new
  mib.load_module("SNMPv2-MIB")
  ["BRIX-GLOBAL-REG", "BRIX-TC-MIB", "BRIX-TEST-MIB", "BRIX-VERIFIER-MIB", "BRIXWORX-SERVER-MIB"].each {|mib_file| mib.load_module(mib_file, BRIX_MIB_TMP_DIR)}

  mibs = Hash[*mib_names.collect {|name| [name, mib.oid("#{name}").to_s]}.flatten]
  
  # Start the resync thread then the trap thread. After the trap thread has started do an initial resync
  $brix_servers_mutex = Mutex.new
  $brix_servers={}
  NetworkManagementServer.all.each {|server| $brix_servers[server.primary_ip] = Mutex.new if server.network_manager.nm_type == "BrixWorx"}
  
  # Do initial resync
  $brix_servers.each_key {|server| resync(server)}
  
  m = SNMP::TrapListener.new(:Host => brix_cfg["host"], :Port => brix_cfg["port"], :Community => brix_cfg["community"]) do |manager|   
    manager.on_trap_v2c do |trap|          
      begin       
        # Ensure database is still connected
        ActiveRecord::Base.verify_active_connections!
        check_nms_servers
        trap_handler(trap, mib, mibs, mib_names)
      rescue
        Rails.logger.error "Exception while processing an SNMP trap #{$!} #{$!.backtrace.join("\n")}"
      end
    end
  end  
  
  m.join
  Rails.logger.info "Server Shutting down"

