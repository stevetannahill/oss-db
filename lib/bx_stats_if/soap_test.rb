require File.dirname(__FILE__) + "/bx_if_common"
require File.dirname(__FILE__) + "/bx_soap_interface"

file_to_send = ARGV[0]
unless file_to_send
  puts"No file specified"
  exit(1)
end

file = File.new(file_to_send, 'r')

request_string = ''
file.each_line do |line|
  request_string += line
end

@@bx_soap_url = BxIfCommon::BX_SOAP_AUTH_URL
resp_data = BxSoapInterface::send_request request_string,@@bx_soap_url
puts "Got response:"
puts resp_data
puts "Size: #{resp_data.size}"

