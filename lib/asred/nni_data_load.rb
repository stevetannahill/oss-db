require 'rubygems'
require 'redis'
require 'json'
require 'csv'

if 1 > ARGV.size
  puts "Usage: nni_data_load <csv_file> [start_record] [num_of_records]"
  exit 1
end

csv_fname = ARGV[0]
first_record = ARGV[1].nil? ? 0 : ARGV[1]
num_of_records = ARGV[2].nil? ? -1 : ARGV[2]

#config = JSON.load(File.open(File.expand_path("/Users/paterson/deploy/monitor_configs/config/redis.conf",__FILE__))).symbolize_keys
#redis = Redis.new(config)
redis = Redis.new

row_ctr = 1
rows_processed = 0
CSV.foreach( csv_fname, :headers => true,
                        :header_converters => :symbol,
                        :converters => :numeric ) do |row|
  # I'm sure there's a better way to do this in ruby; but 5 minutes of
  # googling didn't reveal it, so I've moved on...
  if row_ctr >= first_record and ( rows_processed <= num_of_records or
                                   -1 == num_of_records )

    rowhash = row.to_hash

    nni_fqn = "#{rowhash[:msc]}/#{rowhash[:node]}/#{rowhash[:demarc]} [#{rowhash[:aav]}]" if rowhash[:msc] and rowhash[:node] and rowhash[:demarc]

    unless nni_fqn.nil?
      keys = rowhash.keys.compact

      keys.each do |key|
        redis.set("#{nni_fqn}:#{key.upcase}", rowhash[key])
      end

      puts "Processed #{nni_fqn}:* (#{keys.size} fields) ... "  
    end

    rows_processed += 1
  end

  row_ctr += 1
end
