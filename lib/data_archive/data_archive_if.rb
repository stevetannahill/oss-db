
#  require 'data_archive/data_archive_if'
#  DataArchiveIf.test_no_aggregation

class DataArchiveRecord < ActiveRecord::Base
  self.abstract_class = true
  # If data_archive_connection isn't configured in database.yml this will raise an exception.
  establish_connection "data_archive_#{Rails.env}"
end

# If an exception is raised the API returns a nil object and logs the error.

#  3 use cases:
#    1) no aggregation. i.e. if data is stored as 5 minutes periods, each 5 minutes period is returned.
#      time_increment = 1
#      attribute aggregation function is ignored (it can be set to "")
#    2) aggregation into larger time periods.
#      time_increment = length of returned time periods
#    3) aggregation into a single row (summary)
#      time_increment = 0
#
#  For cases #2 and #3, 
#R: Output times are be aligned with time increment. e.g. time_increment of
#   15 minutes:  00:45, 00:00, 00:15, 00:30, 00:45, ...
#R: for all specified time periods: (max - min) > time_increment
#
# Min and max boundary ARE aligned with 5 minutes time increments.
# Results are aligned with time_increment
# time_increment > 5 minutes

class DataArchiveIf < DataArchiveRecord
  MONITOR_STATEMENTS = false
  @@number_of_ctvs_per_shard = nil
  def initialize
    raise "DataArchiveIf is an abstract class and cannot be instantiated."
  end

  def self.connect
    puts "Connecting to data_archive..."
    if @@number_of_ctvs_per_shard.nil?
      results = self.connection.select_all("SELECT number_of_ctvs_per_shard FROM sharding_info;")
      results.each do |record|
        @@number_of_ctvs_per_shard= record["number_of_ctvs_per_shard"]
      end
    end
    return self.connection, @@number_of_ctvs_per_shard
  end

  def self.log_completion_time str, start_time
    end_time = Time.now
    Rails.logger.info "DataArchiveIf completion time: #{'%.3f' % (end_time-start_time).to_f} secs for: #{str} "
  end
  
  def self.execute_sql_query conn, sql_query
    Rails.logger.debug "DataArchiveIf #{sql_query}"
    return conn.select_all(DataArchiveRecord.send(:sanitize_sql,[sql_query, 10]))
  end

  #--------------------------------------------------------------------
  # aggregation
  #
  # Example:
  #  v-----------------w-----------------x-----------------y-----------------z   <-- 15 mins boundaries
  #  0-----1-----2-----3-----4-----5-----6-----7-----8-----9----10----11----12   <-- 5 mins boundaries
  #                      s-----------------------e                               <-- alarm start/end
  #        a-----------------b                 c-----------------------------d   <-- queried periods
  #
  #
  # Request:
  #    time_periods = [a,b] [c,d]
  #    time_increment = 900000 # 15 mins
  #    operation = 'sum'
  #
  # starts each new period at 1 second after time boundary thus including
  #  records with timeCaptured of 999 milliseconds or less inside the boundary.
  #
  # the 'attributes' parameter contains a list of names for the data returned as well as the
  #  the SQL aggregation function to use. Possibilities: SUM, MIN, MAX, AVG, STDDEV
  #  It is also possible to include multiple columns.
  #  Example: "result" => "SUM(X + Y)"  , where 'result' is the output name and 'X' and 'Y' are two columns of the queried table.
  #
  # The 'where' parameter contains an array of SQL conditions in the form:
  #     "`x`>'y'"
  #     The use of the backquotes and quotes is strongly recommended.
  #     The content of the innermost arrays are ANDed together while the
  #     entity
  #     Example:
  #     [ ["`x`>'y'", "`z`='1'" ], [ "`u`='3'" ] ]
  #     Maps to:
  #     ((`x`>'y' AND `z`='1') OR (`u`='3'))
  #     
  #  The time_period and time_increment must be specified in the units of the time column as stored in the DB.
  #
  #  The following example shows which time slot records would be aggregated into:
  # timeCaptured, aggregated time(human readable)
  #1303702200000, 1303702200000(23:30:00)
  #1303702200611, 1303702200000(23:30:00)
  #1303702200999, 1303702200000(23:30:00)
  #
  #1303702201000, 1303704000000(00:00:00)
  #1303704000000, 1303704000000(00:00:00)
  #1303704000460, 1303704000000(00:00:00)
  #
  #1303704300772, 1303705800000(00:30:00)
  #1303704600659, 1303705800000
  #1303704900544, 1303705800000
  #1303705200851, 1303705800000
  #1303705500733, 1303705800000
  #1303705799999, 1303705800000
  #1303705800000, 1303705800000(00:30:00)
  #1303705800637, 1303705800000(00:30:00)
  #1303705800999, 1303705800000(00:30:00)
  #
  #1303705801000, 1303707600000(01:00:00)
  #1303706100550, 1303707600000(01:00:00)
  #1303706400834, 1303707600000
  #1303706700712, 1303707600000
  #1303707000602, 1303707600000
  #1303707300471, 1303707600000
  #1303707600000, 1303707600000(01:00:00)
  #1303707600856, 1303707600000(01:00:00)
  #
  #1303707900765, 1303709400000(01:30:00)
  #1303708200666, 1303709400000(
  #1303708500590, 1303709400000(
  #1303708800511, 1303709400000(
  #1303709100826, 1303709400000(
  #1303709400000, 1303709400000(
  #1303709400703, 1303709400000(01:30:00)
  #
  #
  # Output:
  #  [ ta: no data
  #    tw: sum( delta[1], delta[2], delta[3] )  <-- delta[1] included.
  #    tx: sum( delta[4] )                      <-- other deltas excluded because of alarm.
  #    ty: sum( delta[8], delta[9] )            <-- delta[7] excluded because of alarm.
  #    tz: sum( delta[10], delta[11], delta[12] )
  #  ]
  #

  def self.query_and_aggregate class_name, time_name, where, group_by, order_by, attributes, time_periods, time_increment
    conn = nil
    begin
      # do some parameter validation:
      (attributes.map { |name, operation| Rails.logger.warn "empty operation in query_and_aggregate" if operation.empty? }) unless attributes.nil?
      raise "time_increment must be greater than zero" if (time_increment < 1)
      raise "time period in time_periods must contain two time entries: start and end" if (time_periods[0].size != 2)
      time_start = nil
      time_periods.each do | time_period |
        raise " start time(#{time_period[0]}) > end time(#{time_period[1]})" if time_period[0]>time_period[1]
        #        now = (Time.now.to_f * 1000).to_i
        #        if time_period[1]>now then
        #          Rails.logger.warn " end time(#{time_period[1]}) > now(#{now}), substituting with now(#{now})"
        #          time_period[1] = now
        #        end
        time_start = time_period[0] if time_start.nil?
        time_start = time_period[0] if time_period[0] < time_start
      end
      conn, number_of_ctvs_per_shard = self.connect
      table_name_suffix = '_archive'
      sql_str = String.new
      sql_str << "SELECT (#{time_start} + TRUNCATE((#{time_name} - #{time_start})/#{time_increment}, 0) * #{time_increment} + #{time_increment}) as time, "
      sql_str << (group_by.map{|name| "`#{name}`" }.join(", ") + ", ") unless group_by.nil?
      sql_str << (attributes.map { |name, operation| "#{operation} AS `#{name}`"}.join(", ")) unless attributes.nil?
      sql_str << " FROM `#{class_name}#{table_name_suffix}` "
      sql_str << " WHERE ("
      sql_str << (time_periods.map { | time_period | "(`#{time_name}` BETWEEN #{time_period[0]} AND #{time_period[1]})" }.join( " OR "))
      sql_str << (") AND (" + (where.map{|array| ( "("+ array.map { |value| "#{value}" }.join(" AND ") +")")}).join(" OR ") + ")") unless where.nil?
      sql_str << " GROUP BY time "
      sql_str << ("," + group_by.map{|name| "`#{name}`" }.join(", ") ) unless group_by.nil?
      sql_str << (" ORDER BY " + order_by.map{|name| "`#{if (name==time_name) then 'time' else name end }`" }.join(", ") ) unless order_by.nil?

      return nil if conn.nil?
      results = execute_sql_query conn, sql_str
      output_array = Array.new
      results.each do |record|
        output_array << record
      end
      # replace 'time' by time_name in output
      output_array.each {|result|
        result[time_name] = result["time"]
        result.delete("time")}
      return output_array
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      SW_ERR "DataArchiveIf: #{e.message} "
      return nil
    end
  end

  

  def self.simplified_query_and_aggregate_test sharded
    class_name = 'ethframedelaytest'
    time_name = 'sample_end_epoch'
    normalized_time_name = 'time'
    where = nil
    ref_where =nil
    if sharded
      circuit_id          = 10
      ref_circuit_id      = 7
      last_hop_circuit_id = 9
      where = {'grid_circuit_id' => circuit_id }
      ref_where = [{'grid_circuit_id' => ref_circuit_id }, {'grid_circuit_id' => last_hop_circuit_id }  ]
    else
      circuit_id = 'AU54XC356-RS-P-V2527-CIR00055-P6'
      ref_where = [ {'CircuitId' => "#{ref_circuit_id}"} ]
      circuit_id          = 'AU54XC224-RN-P-V2503-CIR00055-P6'
      ref_circuit_id      = 'AU13XC092-DNR-P-V2501-CIR00250-P6'
      last_hop_circuit_id = 'AU54XC226-RN-P-V2507-CIR00055-P6'
      where = {'CircuitId' => "#{circuit_id}"}
      ref_where = [{'CircuitId' => "#{ref_circuit_id}"}, {'CircuitId' => "#{last_hop_circuit_id}"}  ]
    end
    time_periods = [[ 1325368860,1325368860+1294476]]
    time_increment = 28800
    column_info = SpirentStatsArchive::SPIRENT_METRIC_COLUMNS
    metric_types = [ :frame_loss, :frame_loss_ratio, :delay, :delay_variation, :min_delay, :min_delay_variation, :max_delay, :max_delay_variation, :frames_sent, :frames_received, ]
    group_by = nil
    order_by = ['time']
    x= DataArchiveIf.simplified_query_and_aggregate metric_types, class_name, time_name, normalized_time_name, where, column_info, time_periods, time_increment
  end

=begin

SELECT
(1325368860 + TRUNCATE((sample_end_epoch - 1325368860)/28800, 0) * 28800 + 28800) as time ,
SUM(FramesSent) - SUM(FramesReceived) AS `framesLost`, (SUM(FramesSent) - SUM(FramesReceived))/SUM(FramesSent)*100 AS `frameLossRatio`, AVG(AverageDelay/2) AS `averageLatency`, AVG(AverageDelayVariation/2) AS `averageDelayVariation`, MIN(MinimumDelay/2) AS `minimumLatency`, MIN(MinimumDelayVariation/2) AS `minimumDelayVariation`, MAX(MaximumDelay/2) AS `maximumLatency`, MAX(MaximumDelayVariation/2) AS `maximumDelayVariation`
FROM `spirent_archive`.`ethframedelaytest_archive`
WHERE ((`sample_end_epoch` BETWEEN 1325368860 AND 1326663336)) AND (( `CircuitId`='AU54XC356-RS-P-V2527-CIR00055-P6' ))
GROUP BY time   ORDER BY `time`;

+------------+------------+----------------+----------------+-----------------------+----------------+-----------------------+----------------+-----------------------+
| time       | framesLost | frameLossRatio | averageLatency | averageDelayVariation | minimumLatency | minimumDelayVariation | maximumLatency | maximumDelayVariation |
+------------+------------+----------------+----------------+-----------------------+----------------+-----------------------+----------------+-----------------------+
| 1325397660 |     106210 |        73.7569 |  3076.42187500 |          269.35937500 |      2080.5000 |                0.0000 |      3725.0000 |             1624.5000 |
...
+------------+------------+----------------+----------------+-----------------------+----------------+-----------------------+----------------+-----------------------+
45 rows in set (0.15 sec)


=end
  # computes the data required to graph avg/min/max for circuit
  def self.simplified_query_and_aggregate metric_types,  class_name, time_name, normalized_time_name, where, column_info, time_periods, time_increment
    conn = nil
    begin
      raise "time_increment must be greater than zero" if (time_increment < 1)
      raise "time period in time_periods must contain two time entries: start and end: #{time_periods.inspect}" if (time_periods[0].size != 2)
      time_start = nil
      time_periods.each do | time_period |
        raise " start time(#{time_period[0]}) > end time(#{time_period[1]})" if time_period[0]>time_period[1]
        time_start = time_period[0] if time_start.nil?
        time_start = time_period[0] if time_period[0] < time_start
      end
      raise "too many keys specified" if where.size != 1
      conn, number_of_ctvs_per_shard = self.connect
      table_name_suffix = '_archive'
      if 0 != number_of_ctvs_per_shard
        id = where.first[1]
        shard_id = which_shard_id id, number_of_ctvs_per_shard
        table_name_suffix = "_shard#{shard_id}"
      end
      where_time_str = time_periods.map { | time_period | "(`#{time_name}` BETWEEN #{time_period[0] } AND #{time_period[1]})" }.join( " OR ")
      where_str = ("("+ where.map { |column_name, value| " `#{column_name}`='#{value}' " }.join(" AND ") +")")
      keys = []
      where.each do|column_name, value|
        keys << column_name
      end
      select_time_str= "(#{time_start} + TRUNCATE((#{time_name} - #{time_start})/#{time_increment}, 0) * #{time_increment} + #{time_increment}) as #{normalized_time_name} "
      metrics_strs =[]
      metric_types.each do |metric_type|
        case metric_type
        when :frames_sent
          metrics_strs << "SUM(#{column_info[metric_type]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        when :frames_received
          metrics_strs << "SUM(#{column_info[metric_type]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        when :frame_loss
          metrics_strs << "SUM(#{column_info[:frames_sent]}) - SUM(#{column_info[:frames_received]}) AS `#{Stats::METRIC_NAMES[:frame_loss]}`"
        when :frame_loss_ratio
          metrics_strs << "(SUM(#{column_info[:frames_sent]}) - SUM(FramesReceived))/SUM(#{column_info[:frames_sent]})*100 AS `#{Stats::METRIC_NAMES[:frame_loss_ratio]}`"
        when :delay
          metrics_strs << "AVG(#{column_info[metric_type]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        when :delay_variation
          metrics_strs << "AVG(#{column_info[metric_type]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        when :min_delay
          metrics_strs << "MIN(#{column_info[metric_type]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        when :min_delay_variation
          metrics_strs << "MIN(#{column_info[metric_type]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        when :max_delay
          metrics_strs << "MAX(#{column_info[metric_type]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        when :max_delay_variation
          metrics_strs << "MAX(#{column_info[metric_type]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        when :octets_count
          SW_ERR "ERROR: not supported  #{metric_type}\n"
        when :sample_count
          metrics_strs << "COUNT(*) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        else
          SW_ERR "ERROR: unknown  #{metric_type}\n"
        end
      end
      metrics_str = metrics_strs.join(", ")
      sql_query = ''
      sql_query = <<EOF
SELECT
#{select_time_str},
#{metrics_str}
FROM `#{class_name}#{table_name_suffix}`
WHERE (#{where_time_str}) AND (#{where_str})
GROUP BY #{normalized_time_name}
EOF
      
      return nil if conn.nil?
      results = execute_sql_query conn, sql_query
      output_array = Array.new
      results.each do |record|
        # replace 'time' by time_name in output
        record[time_name] = record["time"]
        record.delete("time")
        output_array << record
      end
      if (output_array[0].nil?)
        SW_ERR "ERROR:  Are we connected to the archive? Query returned no data for #{metric_types} query: #{sql_query}\n"
      end
      return output_array
    end
=begin
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      SW_ERR "DataArchiveIf: #{e.message} "
      return nil
    end
=end
  end


  def self.differential_query_and_aggregate_test sharded
    class_name = 'ethframedelaytest'
    time_name = 'sample_end_epoch'
    normalized_time_name = 'time'
    where = nil
    ref_where =nil
    if sharded
      circuit_id          = 10
      ref_circuit_id      = 7
      last_hop_circuit_id = 9
      where = {'grid_circuit_id' => circuit_id }
      ref_where = [{'grid_circuit_id' => ref_circuit_id }, {'grid_circuit_id' => last_hop_circuit_id }  ]
    else
      circuit_id = 'AU54XC356-RS-P-V2527-CIR00055-P6'
      ref_where = [ {'CircuitId' => "#{ref_circuit_id}"} ]
      circuit_id          = 'AU54XC224-RN-P-V2503-CIR00055-P6'
      ref_circuit_id      = 'AU13XC092-DNR-P-V2501-CIR00250-P6'
      last_hop_circuit_id = 'AU54XC226-RN-P-V2507-CIR00055-P6'
      where = {'CircuitId' => "#{circuit_id}"}
      ref_where = [{'CircuitId' => "#{ref_circuit_id}"}, {'CircuitId' => "#{last_hop_circuit_id}"}  ]
    end
    time_periods = [[ 1325368860,1325368860+1294476]]
    time_increment = 28800
    column_info = SpirentStatsArchive::SPIRENT_METRIC_COLUMNS
    metric_types = [ :frame_loss, :frame_loss_ratio, :delay, :delay_variation, :min_delay, :min_delay_variation, :max_delay, :max_delay_variation, :frames_sent, :frames_received, ]
    group_by = nil
    order_by = ['time']
    x= DataArchiveIf.differential_query_and_aggregate metric_types, class_name, time_name, normalized_time_name, where, ref_where, column_info, time_periods, time_increment
  end

=begin
SELECT
t.time,
t.`framesLost` AS `framesLost`, t.`frameLossRatio` AS `frameLossRatio`, t.`averageLatency` AS `averageLatency`, t.`averageDelayVariation` AS `averageDelayVariation`, t.`minimumLatency` AS `minimumLatency`, t.`minimumDelayVariation` AS `minimumDelayVariation`, t.`maximumLatency` AS `maximumLatency`, t.`maximumDelayVariation` AS `maximumDelayVariation`,
t1.`framesLost` AS `ref1_framesLost`, t1.`frameLossRatio` AS `ref1_frameLossRatio`, t1.`averageLatency` AS `ref1_averageLatency`, t1.`averageDelayVariation` AS `ref1_averageDelayVariation`, t1.`minimumLatency` AS `ref1_minimumLatency`, t1.`minimumDelayVariation` AS `ref1_minimumDelayVariation`, t1.`maximumLatency` AS `ref1_maximumLatency`, t1.`maximumDelayVariation` AS `ref1_maximumDelayVariation`, t2.`framesLost` AS `ref2_framesLost`, t2.`frameLossRatio` AS `ref2_frameLossRatio`, t2.`averageLatency` AS `ref2_averageLatency`, t2.`averageDelayVariation` AS `ref2_averageDelayVariation`, t2.`minimumLatency` AS `ref2_minimumLatency`, t2.`minimumDelayVariation` AS `ref2_minimumDelayVariation`, t2.`maximumLatency` AS `ref2_maximumLatency`, t2.`maximumDelayVariation` AS `ref2_maximumDelayVariation`,
t.`framesLost` - t1.`framesLost` AS `delta1_framesLost`, t.`frameLossRatio` - t1.`frameLossRatio` AS `delta1_frameLossRatio`, t.`averageLatency` - t1.`averageLatency` AS `delta1_averageLatency`, t.`averageDelayVariation` - t1.`averageDelayVariation` AS `delta1_averageDelayVariation`, t.`minimumLatency` - t1.`minimumLatency` AS `delta1_minimumLatency`, t.`minimumDelayVariation` - t1.`minimumDelayVariation` AS `delta1_minimumDelayVariation`, t.`maximumLatency` - t1.`maximumLatency` AS `delta1_maximumLatency`, t.`maximumDelayVariation` - t1.`maximumDelayVariation` AS `delta1_maximumDelayVariation`, t.`framesLost` - t2.`framesLost` AS `delta2_framesLost`, t.`frameLossRatio` - t2.`frameLossRatio` AS `delta2_frameLossRatio`, t.`averageLatency` - t2.`averageLatency` AS `delta2_averageLatency`, t.`averageDelayVariation` - t2.`averageDelayVariation` AS `delta2_averageDelayVariation`, t.`minimumLatency` - t2.`minimumLatency` AS `delta2_minimumLatency`, t.`minimumDelayVariation` - t2.`minimumDelayVariation` AS `delta2_minimumDelayVariation`, t.`maximumLatency` - t2.`maximumLatency` AS `delta2_maximumLatency`, t.`maximumDelayVariation` - t2.`maximumDelayVariation` AS `delta2_maximumDelayVariation`
FROM
(SELECT
(1325368860 + TRUNCATE((sample_end_epoch - 1325368860)/28800, 0) * 28800 + 28800) as time ,
SUM(framesSent) - SUM(framesReceived) AS `framesLost`, (SUM(framesSent) - SUM(framesReceived))/SUM(framesSent)*100 AS `frameLossRatio`, AVG(AverageDelay/2) AS `averageLatency`, AVG(AverageDelayVariation/2) AS `averageDelayVariation`, MIN(MinimumDelay/2) AS `minimumLatency`, MIN(MinimumDelayVariation/2) AS `minimumDelayVariation`, MAX(MaximumDelay/2) AS `maximumLatency`, MAX(MaximumDelayVariation/2) AS `maximumDelayVariation`
FROM `spirent_archive`.`ethframedelaytest_archive`
WHERE ((`sample_end_epoch` BETWEEN 1325368860 AND 1326663336)) AND (( `CircuitId`='AU54XC224-RN-P-V2503-CIR00055-P6' ))
GROUP BY time
)t
LEFT JOIN
(SELECT
(1325368860 + TRUNCATE((sample_end_epoch - 1325368860)/28800, 0) * 28800 + 28800) as time ,
SUM(framesSent) - SUM(framesReceived) AS `framesLost`, (SUM(framesSent) - SUM(framesReceived))/SUM(framesSent)*100 AS `frameLossRatio`, AVG(AverageDelay/2) AS `averageLatency`, AVG(AverageDelayVariation/2) AS `averageDelayVariation`, MIN(MinimumDelay/2) AS `minimumLatency`, MIN(MinimumDelayVariation/2) AS `minimumDelayVariation`, MAX(MaximumDelay/2) AS `maximumLatency`, MAX(MaximumDelayVariation/2) AS `maximumDelayVariation`
FROM `spirent_archive`.`ethframedelaytest_archive`
WHERE ((`sample_end_epoch` BETWEEN 1325368860 AND 1326663336)) AND ( `CircuitId`='AU13XC092-DNR-P-V2501-CIR00250-P6' )
GROUP BY time
)t1
ON t.time = t1.time
LEFT JOIN
(SELECT
(1325368860 + TRUNCATE((sample_end_epoch - 1325368860)/28800, 0) * 28800 + 28800) as time ,
SUM(framesSent) - SUM(framesReceived) AS `framesLost`, (SUM(framesSent) - SUM(framesReceived))/SUM(framesSent)*100 AS `frameLossRatio`, AVG(AverageDelay/2) AS `averageLatency`, AVG(AverageDelayVariation/2) AS `averageDelayVariation`, MIN(MinimumDelay/2) AS `minimumLatency`, MIN(MinimumDelayVariation/2) AS `minimumDelayVariation`, MAX(MaximumDelay/2) AS `maximumLatency`, MAX(MaximumDelayVariation/2) AS `maximumDelayVariation`
FROM `spirent_archive`.`ethframedelaytest_archive`
WHERE ((`sample_end_epoch` BETWEEN 1325368860 AND 1326663336)) AND ( `CircuitId`='AU54XC226-RN-P-V2507-CIR00055-P6' )
GROUP BY time
)t2
ON t.time = t2.time

+------------+------------+----------------+----------------+-----------------------+----------------+-----------------------+----------------+-----------------------+-----------------+---------------------+---------------------+----------------------------+---------------------+----------------------------+---------------------+----------------------------+-----------------+---------------------+---------------------+----------------------------+---------------------+----------------------------+---------------------+----------------------------+-------------------+-----------------------+-----------------------+------------------------------+-----------------------+------------------------------+-----------------------+------------------------------+-------------------+-----------------------+-----------------------+------------------------------+-----------------------+------------------------------+-----------------------+------------------------------+
| time       | framesLost | frameLossRatio | averageLatency | averageDelayVariation | minimumLatency | minimumDelayVariation | maximumLatency | maximumDelayVariation | ref1_framesLost | ref1_frameLossRatio | ref1_averageLatency | ref1_averageDelayVariation | ref1_minimumLatency | ref1_minimumDelayVariation | ref1_maximumLatency | ref1_maximumDelayVariation | ref2_framesLost | ref2_frameLossRatio | ref2_averageLatency | ref2_averageDelayVariation | ref2_minimumLatency | ref2_minimumDelayVariation | ref2_maximumLatency | ref2_maximumDelayVariation | delta1_framesLost | delta1_frameLossRatio | delta1_averageLatency | delta1_averageDelayVariation | delta1_minimumLatency | delta1_minimumDelayVariation | delta1_maximumLatency | delta1_maximumDelayVariation | delta2_framesLost | delta2_frameLossRatio | delta2_averageLatency | delta2_averageDelayVariation | delta2_minimumLatency | delta2_minimumDelayVariation | delta2_maximumLatency | delta2_maximumDelayVariation |
+------------+------------+----------------+----------------+-----------------------+----------------+-----------------------+----------------+-----------------------+-----------------+---------------------+---------------------+----------------------------+---------------------+----------------------------+---------------------+----------------------------+-----------------+---------------------+---------------------+----------------------------+---------------------+----------------------------+---------------------+----------------------------+-------------------+-----------------------+-----------------------+------------------------------+-----------------------+------------------------------+-----------------------+------------------------------+-------------------+-----------------------+-----------------------+------------------------------+-----------------------+------------------------------+-----------------------+------------------------------+
| 1325397660 |         81 |         0.0563 |  3822.91145833 |         1252.08854167 |      2318.0000 |                0.0000 |      8648.0000 |             6227.5000 |          144000 |            100.0000 |                NULL |                       NULL |                NULL |                       NULL |                NULL |                       NULL |              32 |              0.0222 |       2918.76562500 |               705.38020833 |           2118.5000 |                     0.0000 |           5327.5000 |                  3192.0000 |           -143919 |              -99.9437 |                  NULL |                         NULL |                  NULL |                         NULL |                  NULL |                         NULL |                49 |                0.0341 |          904.14583333 |                 546.70833333 |              199.5000 |                       0.0000 |             3320.5000 |                    3035.5000 |
| 1325426460 |         93 |         0.0646 |  3805.70833333 |         1267.42708333 |      2320.5000 |                0.0000 |      8695.0000 |             6209.0000 |          144000 |            100.0000 |                NULL |                       NULL |                NULL |                       NULL |                NULL |                       NULL |              35 |              0.0243 |       2884.53125000 |               708.11458333 |           2120.5000 |                     0.0000 |           5303.0000 |                  3172.0000 |           -143907 |              -99.9354 |                  NULL |                         NULL |                  NULL |                         NULL |                  NULL |                         NULL |                58 |                0.0403 |          921.17708333 |                 559.31250000 |              200.0000 |                       0.0000 |             3392.0000 |                    3037.0000 |
...
+------------+------------+----------------+----------------+-----------------------+----------------+-----------------------+----------------+-----------------------+-----------------+---------------------+---------------------+----------------------------+---------------------+----------------------------+---------------------+----------------------------+-----------------+---------------------+---------------------+----------------------------+---------------------+----------------------------+---------------------+----------------------------+-------------------+-----------------------+-----------------------+------------------------------+-----------------------+------------------------------+-----------------------+------------------------------+-------------------+-----------------------+-----------------------+------------------------------+-----------------------+------------------------------+-----------------------+------------------------------+
41 rows in set (0.35 sec)


=end
  def self.differential_query_and_aggregate metric_types, class_name, time_name, normalized_time_name, where, ref_where, column_info, time_periods, time_increment
    if (where.nil? or where.empty?)
      SW_ERR "ERROR: no where clause specified   #{metric_types}  \n"
      return nil
    end
    return simplified_query_and_aggregate  metric_types,  class_name, time_name, normalized_time_name, where, column_info, time_periods, time_increment  if ref_where.nil? or ref_where.empty?
    return [{"COUNT" => 0}] if time_periods.empty?
    begin
      raise "time_increment must be greater than zero" if (time_increment < 1)
      raise "time period in time_periods must contain two time entries: start and end: #{time_periods.inspect}" if (time_periods[0].size != 2)
      time_start = nil
      time_periods.each do | time_period |
        raise " start time(#{time_period[0]}) > end time(#{time_period[1]})" if time_period[0]>time_period[1]
        time_start = time_period[0] if time_start.nil?
        time_start = time_period[0] if time_period[0] < time_start
      end
      raise "too many keys specified" if where.size != 1
      conn, number_of_ctvs_per_shard = self.connect
      table_name_suffix = '_archive'
      ref_table_name_suffix = '_archive'
      if 0 != number_of_ctvs_per_shard
        id = where.first[1]
        shard_id = which_shard_id id, number_of_ctvs_per_shard
        table_name_suffix = "_shard#{shard_id}"
      end
      where_time_str = time_periods.map { | time_period | "(`#{time_name}` BETWEEN #{time_period[0] } AND #{time_period[1]})" }.join( " OR ")
      where_str = ("("+ where.map { |column_name, value| " `#{column_name}`='#{value}' " }.join(" AND ") +")")
      select_time_str= "(#{time_start} + TRUNCATE((#{time_name} - #{time_start})/#{time_increment}, 0) * #{time_increment} + #{time_increment}) AS #{normalized_time_name} "
      total_strs = []
      aggr_total_strs = []
      metric_types.each do |metric_type|
        total_strs << "t.`#{Stats::METRIC_NAMES[metric_type]}` AS `#{Stats::METRIC_NAMES[metric_type]}`"
        aggr_operation_str =''
        case metric_type
        when :frames_sent, :frames_received
          aggr_operation_str = 'SUM'
        when :delay, :delay_variation
          aggr_operation_str = 'AVG'
        when :min_delay, :min_delay_variation
          aggr_operation_str = 'MIN'
        when :max_delay, :max_delay_variation
          aggr_operation_str = 'MAX'
        when :frame_loss, :frame_loss_ratio, :sample_count
        when :octets_count
          SW_ERR "ERROR: not supported  #{metric_type}\n"
        else
          SW_ERR "ERROR: unknown  #{metric_type}\n"
        end
        case metric_type
        when :frame_loss
          aggr_total_strs << "SUM(#{Stats::METRIC_NAMES[:frames_sent]}) - SUM(#{Stats::METRIC_NAMES[:frames_received]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        when :frame_loss_ratio
          aggr_total_strs << "(SUM(#{Stats::METRIC_NAMES[:frames_sent]}) - SUM(#{Stats::METRIC_NAMES[:frames_received]}))/SUM(#{Stats::METRIC_NAMES[:frames_sent]})*100 AS `#{Stats::METRIC_NAMES[metric_type]}`"
        when :sample_count
          aggr_total_strs << "COUNT(*) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        else
          aggr_total_strs << "#{aggr_operation_str}(#{column_info[metric_type]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        end
      end
      total_str   = total_strs.join(", ")
      aggr_total_str = aggr_total_strs.join(", ")
      sql_inner_join = ''
      delta_strs = []
      ref_strs = []
      ref_index = 1
      ref_where.each do |hash|
        aggr_ref_strs = []
        ref_where_str =  hash.map { |column_name, value| " `#{column_name}`='#{value}' " }.join(" AND ")
        metric_types.each do |metric_type|
          ref_strs << "t#{ref_index}.`#{Stats::METRIC_NAMES[metric_type]}` AS `ref#{ref_index}_#{Stats::METRIC_NAMES[metric_type]}`"
          delta_strs << "t.`#{Stats::METRIC_NAMES[metric_type]}` - t#{ref_index}.`#{Stats::METRIC_NAMES[metric_type]}` AS `delta#{ref_index}_#{Stats::METRIC_NAMES[metric_type]}`"
          aggr_operation_str =''
          case metric_type
          when :frames_sent, :frames_received
            aggr_operation_str = 'SUM'
          when :delay, :delay_variation
            aggr_operation_str = 'AVG'
          when :min_delay, :min_delay_variation
            aggr_operation_str = 'MIN'
          when :max_delay, :max_delay_variation
            aggr_operation_str = 'MAX'
          when :frame_loss, :frame_loss_ratio, :sample_count
          when :octets_count
            SW_ERR "ERROR: not supported  #{metric_type}\n"
          else
            SW_ERR "ERROR: unknown  #{metric_type}\n"
          end
          case metric_type
          when :frame_loss
            aggr_ref_strs   << "SUM(#{Stats::METRIC_NAMES[:frames_sent]}) - SUM(#{Stats::METRIC_NAMES[:frames_received]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
          when :frame_loss_ratio
            aggr_ref_strs   << "(SUM(#{Stats::METRIC_NAMES[:frames_sent]}) - SUM(#{Stats::METRIC_NAMES[:frames_received]}))/SUM(#{Stats::METRIC_NAMES[:frames_sent]})*100 AS `#{Stats::METRIC_NAMES[metric_type]}`"
          when :sample_count
            aggr_ref_strs << "COUNT(*) AS `#{Stats::METRIC_NAMES[metric_type]}`"
          else
            aggr_ref_strs   << "#{aggr_operation_str}(#{column_info[metric_type]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
          end
        end
        if 0 != number_of_ctvs_per_shard
          id = hash.first[1]
          shard_id = which_shard_id id, number_of_ctvs_per_shard
          ref_table_name_suffix = "_shard#{shard_id}"
        end
        aggr_ref_str = aggr_ref_strs.join(", ")
        sql_inner_join << <<EOF
LEFT JOIN
(SELECT
#{select_time_str},
#{aggr_ref_str}
FROM `#{class_name}#{ref_table_name_suffix}`
WHERE (#{where_time_str}) AND (#{ref_where_str})
GROUP BY #{normalized_time_name}
)t#{ref_index}
ON t.#{normalized_time_name} = t#{ref_index}.#{normalized_time_name}
EOF
        ref_index += 1
      end
      ref_str     = ref_strs.join(", ")
      delta_str   = delta_strs.join(", ")
      sql_query = <<EOF
SELECT
t.#{normalized_time_name},
#{total_str},
#{ref_str},
#{delta_str}
FROM
(SELECT
#{select_time_str},
#{aggr_total_str}
FROM `#{class_name}#{table_name_suffix}`
WHERE (#{where_time_str}) AND (#{where_str})
GROUP BY #{normalized_time_name}
)t
#{sql_inner_join}
EOF

      return nil if conn.nil?
      results = execute_sql_query conn, sql_query
      output_array = Array.new
      results.each do |record|
        output_array << record
      end
      if (output_array[0].nil?)
        SW_ERR "ERROR:  Are we connected to the archive? Query returned no data for query: #{sql_query}\n"
      end
      return output_array

    end
=begin
  rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      SW_ERR "DataArchiveIf: #{e.message} "
      return nil
    end
=end
  end

  #------------------------------------------------------------------------
  # No aggregation:
  # Example:
  #  v-----------------w-----------------x-----------------y-----------------z   <-- 15 mins boundaries
  #  0-----1-----2-----3-----4-----5-----6-----7-----8-----9----10----11----12   <-- 5 mins boundaries
  #                      s-----------------------e                               <-- alarm start/end
  #        a-----------------b                 c-----------------------------d   <-- queried periods
  #

  # no aggregation :
  # Request:
  #    time_periods = [a,b] [c,d]
  # Output:
  #    [delta[1], delta[2], delta[3], delta[4], delta[8],
  #    delta[9], delta[10], delta[11], delta[12] ]
  #
  # 'attributes'  is an array of columns to be returned.
  def self.query_dont_aggregate class_name, time_name, where, group_by, order_by, attributes, time_periods
    conn = nil
    begin
      conn, number_of_ctvs_per_shard = self.connect
      table_name_suffix = '_archive'
      sql_str = String.new
      sql_str << "SELECT #{time_name} as time"
      sql_str << ", " unless group_by.nil? and attributes.nil?
      sql_str << (group_by.map{|name| "`#{name}`" }.join(", ") + ", ") unless group_by.nil?
      sql_str << (attributes.map { |name| "`#{name}`"}.join(", ")) unless attributes.nil?
      sql_str << " FROM `#{class_name}#{table_name_suffix}` "
      sql_str << " WHERE ("
      sql_str << (time_periods.map { | time_period | "(`#{time_name}` BETWEEN #{time_period[0]} AND #{time_period[1]})" }.join( " OR "))
      sql_str << (") AND (" + (where.map{|array| ( "("+ array.map { |value| " #{value} " }.join(" AND ") +")")}).join(" OR ") + ")") unless where.nil?
      sql_str << (" ORDER BY " + order_by.map{|name| "`#{if (name==time_name) then 'time' else name end }`" }.join(", ") ) unless order_by.nil?

      if MONITOR_STATEMENTS
        sql_str2 = String.new
        sql_str2 << "SELECT #{time_name} as time"
        sql_str2 << ", " unless group_by.nil? and attributes.nil?
        sql_str2 << (group_by.map{|name| "`#{name}`" }.join(", ") + ", ") unless group_by.nil?
        sql_str2 << (attributes.map { |name| "`#{name}`"}.join(", ")) unless attributes.nil?
        sql_str2 << " FROM `#{class_name}#{table_name_suffix}` "
        sql_str2 << " WHERE (`#{time_name}` BETWEEN #time_start# AND #time_end#) "
        sql_str2 << " AND ()" unless where.nil?
        sql_str2 << (" ORDER BY " + order_by.map{|name| "`#{if (name==time_name) then 'time' else name end }`" }.join(", ") ) unless order_by.nil?
        stmt = ArchiveStatement.find( :first, :conditions => [ "template=?", sql_str2 ])
        if stmt.nil?
          stmt = ArchiveStatement.new
          stmt.template = sql_str2;
          stmt.statement = sql_str;
          stmt.stack_trace = Kernel.caller(0)
          stmt.save
        end
      end
      return nil if conn.nil?
      results = execute_sql_query conn, sql_str
      output_array = Array.new
      results.each do |record|
        output_array << record
      end
      # replace 'time' by time_name in output
      output_array.each {|result|
        result[time_name] = result["time"]
        result.delete("time")}
      return output_array
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      SW_ERR "DataArchiveIf: #{e.message} "
      return nil
    end
  end

  #------------------------------------------------------------------------
  # Example:
  #  0-----1-----2-----3-----4-----5-----6-----7-----8-----9----10----11----12   <-- 5 mins boundaries
  #                      s-----------------------e                               <-- alarm start/end
  #        a-----------------b                 c-----------------------------d   <-- queried periods
  # 3) summary (overall SLA report )
  # Request:
  #    time_periods = [a,b] [c,d]
  #    operation = 'sum'
  # Output:
  #  [
  #    sum( delta[1], delta[2], delta[3], delta[4], delta[8],
  #         delta[9], delta[10], delta[11], delta[12] )
  #  ]
  #
  # the 'attributes' parameter contains a list of names for the data returned as well as the
  #  the SQL aggregation function to use. Possibilities: SUM, MIN, MAX, AVG, STDDEV
  #  It is also possible to include multiple columns.
  #  Example: "result" => "SUM(X + Y)"  , where 'result' is the output name and 'X' and 'Y' are two columns of the queried table.
  #
  # The 'where' parameter contains an array of SQL conditions in the form:
  #     "`x`>'y'"
  #     The use of the backquotes and quotes is strongly recommended.
  #     The content of the innermost arrays are ANDed together while the
  #     entity
  #     Example:
  #     [ ["`x`>'y'", "`z`='1'" ], [ "`u`='3'" ] ]
  #     Maps to:
  #     ((`x`>'y' AND `z`='1') OR (`u`='3'))
  #
  #  The time_period and time_increment must be specified in the units of the time column as stored in the DB.



  #Example output (partial)
  # => [
  #{"egressQChipDroppedOutProfPackets_delta"=>"0", "ingressQChipDroppedLoPrioPackets_delta"=>"0", "ingressQChipForwardedInProfPackets_delta"=>"0", "displayedName"=>"Lag 11:1030.0", "monitoredObjectSiteId"=>"10.10.100.5", "time"=>"1299283200000", "egressQChipDroppedInProfPackets_delta"=>"0", "ingressQChipDroppedHiPrioPackets_delta"=>"0", "ingressQChipForwardedOutProfPackets_delta"=>"145546"},
  #{"egressQChipDroppedOutProfPackets_delta"=>"0", "ingressQChipDroppedLoPrioPackets_delta"=>"0", "ingressQChipForwardedInProfPackets_delta"=>"0", "displayedName"=>"Lag 11:1030.0", "monitoredObjectSiteId"=>"10.10.100.5", "time"=>"1299369600000", "egressQChipDroppedInProfPackets_delta"=>"0", "ingressQChipDroppedHiPrioPackets_delta"=>"0", "ingressQChipForwardedOutProfPackets_delta"=>"154673"},
  #{"egressQChipDroppedOutProfPackets_delta"=>"0", "ingressQChipDroppedLoPrioPackets_delta"=>"0", "ingressQChipForwardedInProfPackets_delta"=>"0", "displayedName"=>"Lag 11:1030.0", "monitoredObjectSiteId"=>"10.10.100.5", "time"=>"1299456000000", "egressQChipDroppedInProfPackets_delta"=>"0", "ingressQChipDroppedHiPrioPackets_delta"=>"0", "ingressQChipForwardedOutProfPackets_delta"=>"155162"},
  #{"egressQChipDroppedOutProfPackets_delta"=>"0", "ingressQChipDroppedLoPrioPackets_delta"=>"0", "ingressQChipForwardedInProfPackets_delta"=>"0", "displayedName"=>"Lag 11:1030.0", "monitoredObjectSiteId"=>"10.10.100.5", "time"=>"1299542400000", "egressQChipDroppedInProfPackets_delta"=>"0", "ingressQChipDroppedHiPrioPackets_delta"=>"0", "ingressQChipForwardedOutProfPackets_delta"=>"155031"},
  #]

  # Obsolete, replaced by simplified_query_and_summarize. Still used by some SAM code.
  def self.query_and_summarize class_name, time_name, where, group_by, attributes, time_periods, order_by = nil, order_asc = true, limit=0
    conn = nil
    begin
      if time_periods.nil? or time_periods.empty?
        raise " no time period specified"
        return nil
      end
      time_periods.each do | time_period |
        raise " start time(#{time_period[0]}) > end time(#{time_period[1]})" if time_period[0]>time_period[1]
        #        now = (Time.now.to_f * 1000).to_i
        #        if time_period[1]>now then
        #          Rails.logger.warn " end time(#{time_period[1]}) > now(#{now}), substituting with now(#{now})"
        #          time_period[1] = now
        #        end
      end
      conn, number_of_ctvs_per_shard = self.connect
      table_name_suffix = '_archive'
      (attributes.map { |name, operation| Rails.logger.warn "empty operation in query_and_summarize" if operation.empty? }) unless attributes.nil?
      sql_str = String.new
      sql_str << 'SELECT '
      sql_str << (group_by.map{|name| "`#{name}`" }.join(", ") + ", ") unless group_by.nil? or group_by.empty?
      sql_str << (attributes.map { |name, operation| "#{operation} AS `#{name}`"}.join(", ")) unless attributes.nil?
      sql_str << " FROM `#{class_name}#{table_name_suffix}` "
      sql_str << ' WHERE ('
      sql_str << (time_periods.map { | time_period | "(`#{time_name}` BETWEEN #{time_period[0] } AND #{time_period[1]})" }.join( " OR "))
      sql_str << ")"
      sql_str << (" AND (" + (where.map{|array| ( "("+ array.map { |value| " #{value} " }.join(" AND ") +")")}).join(" OR ") + ") ") unless where.nil? or where.empty?
      sql_str << " GROUP BY " + (group_by.map{|name| "`#{name}`" }.join(", ")) unless group_by.nil? or group_by.empty?
      sql_str << " ORDER BY " + (order_by.map{|name| "`#{name}` #{" DESC " unless order_by.nil? or order_asc}" }.join(", ")) unless order_by.nil?
      sql_str << " LIMIT #{limit} " unless  0 == limit

      if MONITOR_STATEMENTS
        sql_str2 = String.new
        sql_str2 << 'SELECT '
        sql_str2 << (group_by.map{|name| "`#{name}`" }.join(", ") + ", ") unless group_by.nil? or group_by.empty?
        sql_str2 << (attributes.map { |name, operation| "#{operation} AS `#{name}`"}.join(", ")) unless attributes.nil?
        sql_str2 << " FROM `#{class_name}#{table_name_suffix}` "
        sql_str2 << " WHERE (`#{time_name}` BETWEEN #time_start# AND #time_end#) "
        sql_str2 << " AND ()" unless where.nil?
        sql_str2 << " GROUP BY " + (group_by.map{|name| "`#{name}`" }.join(", ")) unless group_by.nil? or group_by.empty?
        sql_str2 << " ORDER BY " + (order_by.map{|name| "`#{name}` #{" DESC " unless order_by.nil? or order_asc}" }.join(", ")) unless order_by.nil?
        sql_str2 << " LIMIT #{limit} " unless  0 == limit
        stmt = ArchiveStatement.find( :first, :conditions => [ "template=?", sql_str2 ])
        if stmt.nil?
          stmt = ArchiveStatement.new
          stmt.template = sql_str2;
          stmt.statement = sql_str;
          stmt.stack_trace = Kernel.caller(0)
          stmt.save
        end
      end
      return nil if conn.nil?
      results = execute_sql_query conn, sql_str
      output_array = Array.new
      results.each do |record|
        output_array << record
      end
      return output_array
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      SW_ERR "DataArchiveIf: #{e.message} "
      return nil
    end
  end
  #top_columns = { 'name1' => 'SUM', 'name2' => 'AVG'}
  def self.query_aggregate_and_count class_name, time_name, where, group_by, order_by, selected_columns, time_periods, time_increment, count_condition, top_columns
    return [{"COUNT" => 0}] if time_periods.empty?
    begin
      (selected_columns.map { |name, operation| Rails.logger.warn "empty operation in query_and_aggregate" if operation.empty? }) unless selected_columns.nil?
      raise "time_increment must be greater than zero" if (time_increment < 1)
      raise "time period in time_periods must contain two time entries: start and end: #{time_periods.inspect}" if (time_periods[0].size != 2)
      time_start = nil
      time_periods.each do | time_period |
        raise " start time(#{time_period[0]}) > end time(#{time_period[1]})" if time_period[0]>time_period[1]
        time_start = time_period[0] if time_start.nil?
        time_start = time_period[0] if time_period[0] < time_start
      end
      conn, number_of_ctvs_per_shard = self.connect
      table_name_suffix = '_archive'
      sql_str = String.new
      sql_str << 'SELECT count(*) AS COUNT '
      sql_str << (',' + (top_columns.map { |name, operation| "#{operation}(`#{name}`) AS `#{name}`"}.join(", "))) unless top_columns.nil?
      sql_str << " FROM (SELECT (#{time_start} + TRUNCATE((#{time_name} - #{time_start})/#{time_increment}, 0) * #{time_increment} + #{time_increment}) as time, "
      sql_str << (group_by.map{|name| "`#{name}`" }.join(", ") + ", ") unless group_by.nil?
      sql_str << (selected_columns.map { |name, operation| "#{operation} AS `#{name}`"}.join(", ")) unless selected_columns.nil?
      sql_str << " FROM `#{class_name}#{table_name_suffix}` "
      sql_str << ' WHERE ('
      sql_str << (time_periods.map { | time_period | "(`#{time_name}` BETWEEN #{time_period[0]} AND #{time_period[1]})" }.join( " OR "))
      sql_str << (") AND (" + (where.map{|array| ( "("+ array.map { |value| "#{value}" }.join(" AND ") +")")}).join(" OR ") + ")") unless where.nil?
      sql_str << ' GROUP BY time '
      sql_str << ("," + group_by.map{|name| "`#{name}`" }.join(", ") ) unless group_by.nil?
      sql_str << (" ORDER BY " + order_by.map{|name| "`#{if (name==time_name) then 'time' else name end }`" }.join(", ") ) unless order_by.nil?
      sql_str << (") t WHERE (" + (count_condition.map{|value| ( "(#{value})")}).join(" AND ") + ")")

      if MONITOR_STATEMENTS
        sql_str2 = String.new
        sql_str2 << 'SELECT count(*) AS COUNT '
        sql_str2 << (',' + (top_columns.map { |name, operation| "#{operation}(`#{name}`) AS `#{name}`"}.join(", "))) unless top_columns.nil?
        sql_str2 << (group_by.map{|name| "`#{name}`" }.join(", ") + ", ") unless group_by.nil?
        sql_str2 << (selected_columns.map { |name, operation| "#{operation} AS `#{name}`"}.join(", ")) unless selected_columns.nil?
        sql_str2 << " FROM `#{class_name}#{table_name_suffix}` "
        sql_str2 << " WHERE (`#{time_name}` BETWEEN #time_start# AND #time_end#) "
        sql_str2 << " AND ()" unless where.nil?
        sql_str2 << " GROUP BY time "
        sql_str2 << ("," + group_by.map{|name| "`#{name}`" }.join(", ") ) unless group_by.nil?
        sql_str2 << (" ORDER BY " + order_by.map{|name| "`#{if (name==time_name) then 'time' else name end }`" }.join(", ") ) unless order_by.nil?
        sql_str2 << (") t WHERE (" + (count_condition.map{|value| ( "(#{value})")}).join(" AND ") + ")")
        stmt = ArchiveStatement.find( :first, :conditions => [ "template=?", sql_str2 ])
        if stmt.nil?
          stmt = ArchiveStatement.new
          stmt.template = sql_str2;
          stmt.statement = sql_str;
          stmt.stack_trace = Kernel.caller(0)
          stmt.save
        end
      end
      return nil if conn.nil?
      results = execute_sql_query conn, sql_str

      output_array = Array.new
      results.each do |record|
        output_array << record
      end

      return output_array
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      SW_ERR "DataArchiveIf: #{e.message} "
      return nil
    end
  end


  def self.simplified_query_and_summarize_test sharded
    class_name = 'ethframedelaytest'
    time_name = 'sample_end_epoch'
    where = nil
    ref_where =nil
    if sharded
      circuit_id          = 10
      ref_circuit_id      = 7
      last_hop_circuit_id = 9
      where = {'grid_circuit_id' => circuit_id }
      ref_where = [{'grid_circuit_id' => ref_circuit_id }, {'grid_circuit_id' => last_hop_circuit_id }  ]
    else
      circuit_id = 'AU54XC356-RS-P-V2527-CIR00055-P6'
      ref_where = [ {'CircuitId' => "#{ref_circuit_id}"} ]
      circuit_id          = 'AU54XC224-RN-P-V2503-CIR00055-P6'
      ref_circuit_id      = 'AU13XC092-DNR-P-V2501-CIR00250-P6'
      last_hop_circuit_id = 'AU54XC226-RN-P-V2507-CIR00055-P6'
      where = {'CircuitId' => "#{circuit_id}"}
      ref_where = [{'CircuitId' => "#{ref_circuit_id}"}, {'CircuitId' => "#{last_hop_circuit_id}"}  ]
    end
    time_periods = [[ 1325368860,1325368860+1294476]]
    column_info = SpirentStatsArchive::SPIRENT_METRIC_COLUMNS
    metric_types = [
      :frame_loss,
      :frame_loss_ratio,
      :delay,
      :delay_variation,
      :min_delay,
      :min_delay_variation,
      :max_delay,
      :max_delay_variation,
    ]
    x= DataArchiveIf.simplified_query_and_summarize metric_types, class_name, time_name, where, column_info, time_periods
    metric_types.each do |metric_type|
      puts "#{Stats::METRIC_NAMES[metric_type]} = #{x[0][Stats::METRIC_NAMES[metric_type]]}"
    end
  end
=begin
SELECT
SUM(FramesSent) - SUM(FramesReceived) AS `framesLost`, 100*(SUM(FramesSent) - SUM(FramesReceived))/SUM(FramesSent) AS `frameLossRatio`, AVG(AverageDelay/2) AS `averageLatency`, AVG(AverageDelayVariation/2) AS `averageDelayVariation`, MIN(MinimumDelay/2) AS `minimumLatency`, MIN(MinimumDelayVariation/2) AS `minimumDelayVariation`, MAX(MaximumDelay/2) AS `maximumLatency`, MAX(MaximumDelayVariation/2) AS `maximumDelayVariation`
FROM `spirent_archive`.`ethframedelaytest_archive`
WHERE ((`sample_end_epoch` BETWEEN 1325368860 AND 1326663336)) AND (( `CircuitId`='AU54XC356-RS-P-V2527-CIR00055-P6' )) ;


+------------+----------------+----------------+-----------------------+----------------+-----------------------+----------------+-----------------------+
| framesLost | frameLossRatio | averageLatency | averageDelayVariation | minimumLatency | minimumDelayVariation | maximumLatency | maximumDelayVariation |
+------------+----------------+----------------+-----------------------+----------------+-----------------------+----------------+-----------------------+
|    4725686 |        73.0965 |  3470.74953596 |          720.93364269 |      2070.5000 |                0.0000 |      5283.0000 |             3195.5000 |
+------------+----------------+----------------+-----------------------+----------------+-----------------------+----------------+-----------------------+

=end
  def self.simplified_query_and_summarize   metric_types, class_name, time_name, where, column_info, time_periods
    return nil if where.nil? or where.empty?
    conn = nil
    begin
      if time_periods.nil? or time_periods.empty?
        raise " no time period specified"
        return nil
      end
      time_periods.each do | time_period |
        raise " start time(#{time_period[0]}) > end time(#{time_period[1]})" if time_period[0]>time_period[1]
      end
      raise "too many keys specified" if where.size != 1
      conn, number_of_ctvs_per_shard = self.connect
      table_name_suffix = '_archive'
      if 0 != number_of_ctvs_per_shard
        id = where.first[1]
        shard_id = which_shard_id id, number_of_ctvs_per_shard
        table_name_suffix = "_shard#{shard_id}"
      end
      where_time_str = time_periods.map { | time_period | "(`#{time_name}` BETWEEN #{time_period[0] } AND #{time_period[1]})" }.join( " OR ")
      where_str = ("("+ where.map { |column_name, value| " `#{column_name}`='#{value}' " }.join(" AND ") +")")
      keys = []
      where.each do|column_name, value|
        keys << column_name
      end
      sql_query = ''
      selected_metrics =[]
      metric_types.each do |metric_type|
        case metric_type
        when :frames_sent, :frames_received
          selected_metrics <<  "SUM(#{column_info[metric_type]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        when :frame_loss
          selected_metrics << "SUM(#{column_info[:frames_sent]}) - SUM(#{column_info[:frames_received]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        when :frame_loss_ratio
          selected_metrics << "100*(SUM(#{column_info[:frames_sent]}) - SUM(#{column_info[:frames_received]}))/SUM(#{column_info[:frames_sent]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        when :delay,:delay_variation
          selected_metrics << "AVG(#{column_info[metric_type]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        when :min_delay, :min_delay_variation
          selected_metrics << "MIN(#{column_info[metric_type]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        when :max_delay, :max_delay_variation
          selected_metrics << "MAX(#{column_info[metric_type]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        when :sample_count
          selected_metrics << "COUNT(*) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        when :octets_count
          SW_ERR "ERROR: not supported  #{metric_type}\n"
        else
          SW_ERR "ERROR: unknown  #{metric_type}\n"
        end
      end
      selected_metrics_str = selected_metrics.join(", ")
      sql_query = <<EOF
SELECT
#{selected_metrics_str}
FROM `#{class_name}#{table_name_suffix}`
WHERE (#{where_time_str}) AND (#{where_str}) ;
EOF
      return nil if conn.nil?
      results = execute_sql_query conn, sql_query
      output_array = Array.new
      results.each do |record|
        output_array << record
      end
      if (output_array[0].nil?)
        SW_ERR "ERROR:  Are we connected to the archive? Query returned no data for #{metric_types} query: #{sql_query}\n"
      end
      return output_array
    end
=begin
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      SW_ERR "DataArchiveIf: #{e.message} "
      return nil
    end
=end
  end

  def self.differential_query_and_summarize_test sharded
    class_name = 'ethframedelaytest'
    time_name = 'sample_end_epoch'
    where = nil
    ref_where =nil
    if sharded
      circuit_id          = 10
      ref_circuit_id      = 7
      last_hop_circuit_id = 9
      where = {'grid_circuit_id' => circuit_id }
      ref_where = [{'grid_circuit_id' => ref_circuit_id }, {'grid_circuit_id' => last_hop_circuit_id }  ]
    else
      circuit_id = 'AU54XC356-RS-P-V2527-CIR00055-P6'
      ref_where = [ {'CircuitId' => "#{ref_circuit_id}"} ]
      circuit_id          = 'AU54XC224-RN-P-V2503-CIR00055-P6'
      ref_circuit_id      = 'AU13XC092-DNR-P-V2501-CIR00250-P6'
      last_hop_circuit_id = 'AU54XC226-RN-P-V2507-CIR00055-P6'
      where = {'CircuitId' => "#{circuit_id}"}
      ref_where = [{'CircuitId' => "#{ref_circuit_id}"}, {'CircuitId' => "#{last_hop_circuit_id}"}  ]
    end
    time_periods = [[ 1325368860,1325368860+1294476]]
    column_info = SpirentStatsArchive::SPIRENT_METRIC_COLUMNS
    metric_types =  [
      :frame_loss,
      :frame_loss_ratio,
      :delay,
      :delay_variation,
      :min_delay,
      :min_delay_variation,
      :max_delay,
      :max_delay_variation,
    ]
    x = DataArchiveIf.differential_query_and_summarize  metric_types, class_name, time_name, where, ref_where, column_info, time_periods
    metric_types.each do |metric_type|
      key = "#{Stats::METRIC_NAMES[metric_type]}"
      puts "#{key} = #{x[0][key]}"
      key = "ref1_#{Stats::METRIC_NAMES[metric_type]}"
      puts "#{key} = #{x[0][key]}"
      key = "delta1_#{Stats::METRIC_NAMES[metric_type]}"
      puts "#{key} = #{x[0][key]}"
      key = "ref2_#{Stats::METRIC_NAMES[metric_type]}"
      puts "#{key} = #{x[0][key]}"
      key = "delta2_#{Stats::METRIC_NAMES[metric_type]}"
      puts "#{key} = #{x[0][key]}"
    end
  end
=begin
SELECT
t.`framesLost` AS `framesLost`, t.`frameLossRatio` AS `frameLossRatio`, t.`averageLatency` AS `averageLatency`, t.`averageDelayVariation` AS `averageDelayVariation`, t.`minimumLatency` AS `minimumLatency`, t.`minimumDelayVariation` AS `minimumDelayVariation`, t.`maximumLatency` AS `maximumLatency`, t.`maximumDelayVariation` AS `maximumDelayVariation`,
t1.`framesLost` AS `ref1_framesLost`, t1.`frameLossRatio` AS `ref1_frameLossRatio`, t1.`averageLatency` AS `ref1_averageLatency`, t1.`averageDelayVariation` AS `ref1_averageDelayVariation`, t1.`minimumLatency` AS `ref1_minimumLatency`, t1.`minimumDelayVariation` AS `ref1_minimumDelayVariation`, t1.`maximumLatency` AS `ref1_maximumLatency`, t1.`maximumDelayVariation` AS `ref1_maximumDelayVariation`, t2.`framesLost` AS `ref2_framesLost`, t2.`frameLossRatio` AS `ref2_frameLossRatio`, t2.`averageLatency` AS `ref2_averageLatency`, t2.`averageDelayVariation` AS `ref2_averageDelayVariation`, t2.`minimumLatency` AS `ref2_minimumLatency`, t2.`minimumDelayVariation` AS `ref2_minimumDelayVariation`, t2.`maximumLatency` AS `ref2_maximumLatency`, t2.`maximumDelayVariation` AS `ref2_maximumDelayVariation`,
t.`framesLost` - t1.`framesLost` AS `delta1_framesLost`, t.`frameLossRatio` - t1.`frameLossRatio` AS `delta1_frameLossRatio`, t.`averageLatency` - t1.`averageLatency` AS `delta1_averageLatency`, t.`averageDelayVariation` - t1.`averageDelayVariation` AS `delta1_averageDelayVariation`, t.`minimumLatency` - t1.`minimumLatency` AS `delta1_minimumLatency`, t.`minimumDelayVariation` - t1.`minimumDelayVariation` AS `delta1_minimumDelayVariation`, t.`maximumLatency` - t1.`maximumLatency` AS `delta1_maximumLatency`, t.`maximumDelayVariation` - t1.`maximumDelayVariation` AS `delta1_maximumDelayVariation`, t.`framesLost` - t2.`framesLost` AS `delta2_framesLost`, t.`frameLossRatio` - t2.`frameLossRatio` AS `delta2_frameLossRatio`, t.`averageLatency` - t2.`averageLatency` AS `delta2_averageLatency`, t.`averageDelayVariation` - t2.`averageDelayVariation` AS `delta2_averageDelayVariation`, t.`minimumLatency` - t2.`minimumLatency` AS `delta2_minimumLatency`, t.`minimumDelayVariation` - t2.`minimumDelayVariation` AS `delta2_minimumDelayVariation`, t.`maximumLatency` - t2.`maximumLatency` AS `delta2_maximumLatency`, t.`maximumDelayVariation` - t2.`maximumDelayVariation` AS `delta2_maximumDelayVariation`
FROM
(SELECT
SUM(framesSent) - SUM(framesReceived) AS `framesLost`, (SUM(framesSent) - SUM(framesReceived))/SUM(framesSent)*100 AS `frameLossRatio`, AVG(AverageDelay/2) AS `averageLatency`, AVG(AverageDelayVariation/2) AS `averageDelayVariation`, MIN(MinimumDelay/2) AS `minimumLatency`, MIN(MinimumDelayVariation/2) AS `minimumDelayVariation`, MAX(MaximumDelay/2) AS `maximumLatency`, MAX(MaximumDelayVariation/2) AS `maximumDelayVariation`
FROM `spirent_archive`.`ethframedelaytest_archive`
WHERE ((`sample_end_epoch` BETWEEN 1325368860 AND 1326663336)) AND (( `CircuitId`='AU54XC224-RN-P-V2503-CIR00055-P6' ))
)t,
(SELECT
SUM(framesSent) - SUM(framesReceived) AS `framesLost`, (SUM(framesSent) - SUM(framesReceived))/SUM(framesSent)*100 AS `frameLossRatio`, AVG(AverageDelay/2) AS `averageLatency`, AVG(AverageDelayVariation/2) AS `averageDelayVariation`, MIN(MinimumDelay/2) AS `minimumLatency`, MIN(MinimumDelayVariation/2) AS `minimumDelayVariation`, MAX(MaximumDelay/2) AS `maximumLatency`, MAX(MaximumDelayVariation/2) AS `maximumDelayVariation`
FROM `spirent_archive`.`ethframedelaytest_archive`
WHERE ((`sample_end_epoch` BETWEEN 1325368860 AND 1326663336)) AND ( `CircuitId`='AU13XC092-DNR-P-V2501-CIR00250-P6' )
)t1
, (SELECT
SUM(framesSent) - SUM(framesReceived) AS `framesLost`, (SUM(framesSent) - SUM(framesReceived))/SUM(framesSent)*100 AS `frameLossRatio`, AVG(AverageDelay/2) AS `averageLatency`, AVG(AverageDelayVariation/2) AS `averageDelayVariation`, MIN(MinimumDelay/2) AS `minimumLatency`, MIN(MinimumDelayVariation/2) AS `minimumDelayVariation`, MAX(MaximumDelay/2) AS `maximumLatency`, MAX(MaximumDelayVariation/2) AS `maximumDelayVariation`
FROM `spirent_archive`.`ethframedelaytest_archive`
WHERE ((`sample_end_epoch` BETWEEN 1325368860 AND 1326663336)) AND ( `CircuitId`='AU54XC226-RN-P-V2507-CIR00055-P6' )
)t2

+------------+----------------+----------------+-----------------------+----------------+-----------------------+----------------+-----------------------+-----------------+---------------------+---------------------+----------------------------+---------------------+----------------------------+---------------------+----------------------------+-----------------+---------------------+---------------------+----------------------------+---------------------+----------------------------+---------------------+----------------------------+-------------------+-----------------------+-----------------------+------------------------------+-----------------------+------------------------------+-----------------------+------------------------------+-------------------+-----------------------+-----------------------+------------------------------+-----------------------+------------------------------+-----------------------+------------------------------+
| framesLost | frameLossRatio | averageLatency | averageDelayVariation | minimumLatency | minimumDelayVariation | maximumLatency | maximumDelayVariation | ref1_framesLost | ref1_frameLossRatio | ref1_averageLatency | ref1_averageDelayVariation | ref1_minimumLatency | ref1_minimumDelayVariation | ref1_maximumLatency | ref1_maximumDelayVariation | ref2_framesLost | ref2_frameLossRatio | ref2_averageLatency | ref2_averageDelayVariation | ref2_minimumLatency | ref2_minimumDelayVariation | ref2_maximumLatency | ref2_maximumDelayVariation | delta1_framesLost | delta1_frameLossRatio | delta1_averageLatency | delta1_averageDelayVariation | delta1_minimumLatency | delta1_minimumDelayVariation | delta1_maximumLatency | delta1_maximumDelayVariation | delta2_framesLost | delta2_frameLossRatio | delta2_averageLatency | delta2_averageDelayVariation | delta2_minimumLatency | delta2_minimumDelayVariation | delta2_maximumLatency | delta2_maximumDelayVariation |
+------------+----------------+----------------+-----------------------+----------------+-----------------------+----------------+-----------------------+-----------------+---------------------+---------------------+----------------------------+---------------------+----------------------------+---------------------+----------------------------+-----------------+---------------------+---------------------+----------------------------+---------------------+----------------------------+---------------------+----------------------------+-------------------+-----------------------+-----------------------+------------------------------+-----------------------+------------------------------+-----------------------+------------------------------+-------------------+-----------------------+-----------------------+------------------------------+-----------------------+------------------------------+-----------------------+------------------------------+
|       4497 |         0.0696 |  3744.49965189 |         1250.88315154 |      2317.0000 |                0.0000 |      8719.0000 |             6386.5000 |         1332000 |             23.1009 |       1934.41069012 |                 1.87009472 |           1929.5000 |                     0.0000 |           2343.5000 |                   411.5000 |            2256 |              0.0349 |       2966.35251799 |               741.97888141 |           2117.5000 |                     0.0000 |           5435.0000 |                  3193.0000 |          -1327503 |              -23.0313 |         1810.08896177 |                1249.01305682 |              387.5000 |                       0.0000 |             6375.5000 |                    5975.0000 |              2241 |                0.0347 |          778.14713391 |                 508.90427013 |              199.5000 |                       0.0000 |             3284.0000 |                    3193.5000 |
+------------+----------------+----------------+-----------------------+----------------+-----------------------+----------------+-----------------------+-----------------+---------------------+---------------------+----------------------------+---------------------+----------------------------+---------------------+----------------------------+-----------------+---------------------+---------------------+----------------------------+---------------------+----------------------------+---------------------+----------------------------+-------------------+-----------------------+-----------------------+------------------------------+-----------------------+------------------------------+-----------------------+------------------------------+-------------------+-----------------------+-----------------------+------------------------------+-----------------------+------------------------------+-----------------------+------------------------------+
1 row in set (0.40 sec)

=end

  def self.differential_query_and_summarize  metric_types, class_name, time_name, where, ref_where, column_info, time_periods
    return nil if where.nil? or where.empty?
    return simplified_query_and_summarize    metric_types,class_name, time_name, where, column_info, time_periods if ref_where.nil? or ref_where.empty?
    conn = nil
    begin
      if time_periods.nil? or time_periods.empty?
        raise " no time period specified"
        return nil
      end
      time_periods.each do | time_period |
        raise " start time(#{time_period[0]}) > end time(#{time_period[1]})" if time_period[0]>time_period[1]
      end
      raise "too many keys specified" if where.size != 1
      conn, number_of_ctvs_per_shard  = self.connect
      table_name_suffix = '_archive'
      ref_table_name_suffix = '_archive'
      if 0 != number_of_ctvs_per_shard
        id = where.first[1]
        shard_id = which_shard_id id, number_of_ctvs_per_shard
        table_name_suffix = "_shard#{shard_id}"
      end
      where_time_str = time_periods.map { | time_period | "(`#{time_name}` BETWEEN #{time_period[0] } AND #{time_period[1]})" }.join( " OR ")
      where_str = ("("+ where.map { |column_name, value| " `#{column_name}`='#{value}' " }.join(" AND ") +")")
      total_strs = []
      aggr_total_strs = []
      metric_types.each do |metric_type|
        total_strs << "t.`#{Stats::METRIC_NAMES[metric_type]}` AS `#{Stats::METRIC_NAMES[metric_type]}`"
        aggr_operation_str =''
        case metric_type
        when :frames_sent, :frames_received
          aggr_operation_str = 'SUM'
        when :delay, :delay_variation
          aggr_operation_str = 'AVG'
        when :min_delay, :min_delay_variation
          aggr_operation_str = 'MIN'
        when :max_delay, :max_delay_variation
          aggr_operation_str = 'MAX'
        when :frame_loss, :frame_loss_ratio, :sample_count
        when :octets_count
          SW_ERR "ERROR: not supported  #{metric_type}\n"
        else
          SW_ERR "ERROR: unknown  #{metric_type}\n"
        end
        case metric_type
        when :frame_loss
          aggr_total_strs << "SUM(#{Stats::METRIC_NAMES[:frames_sent]}) - SUM(#{Stats::METRIC_NAMES[:frames_received]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        when :frame_loss_ratio
          aggr_total_strs << "(SUM(#{Stats::METRIC_NAMES[:frames_sent]}) - SUM(#{Stats::METRIC_NAMES[:frames_received]}))/SUM(#{Stats::METRIC_NAMES[:frames_sent]})*100 AS `#{Stats::METRIC_NAMES[metric_type]}`"
        when :sample_count
          aggr_total_strs << "COUNT(*) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        else
          aggr_total_strs << "#{aggr_operation_str}(#{column_info[metric_type]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
        end
      end
      total_str   = total_strs.join(", ")
      aggr_total_str = aggr_total_strs.join(", ")
      sql_subqueries = ''
      delta_strs = []
      ref_strs = []
      ref_index = 1
      ref_where.each do |hash|
        aggr_ref_strs = []
        ref_where_str =  hash.map { |column_name, value| " `#{column_name}`='#{value}' " }.join(" AND ")
        metric_types.each do |metric_type|
          ref_strs << "t#{ref_index}.`#{Stats::METRIC_NAMES[metric_type]}` AS `ref#{ref_index}_#{Stats::METRIC_NAMES[metric_type]}`"
          delta_strs << "t.`#{Stats::METRIC_NAMES[metric_type]}` - t#{ref_index}.`#{Stats::METRIC_NAMES[metric_type]}` AS `delta#{ref_index}_#{Stats::METRIC_NAMES[metric_type]}`"
          aggr_operation_str =''
          case metric_type
          when :frames_sent, :frames_received
            aggr_operation_str = 'SUM'
          when :delay, :delay_variation
            aggr_operation_str = 'AVG'
          when :min_delay, :min_delay_variation
            aggr_operation_str = 'MIN'
          when :max_delay, :max_delay_variation
            aggr_operation_str = 'MAX'
          when :frame_loss, :frame_loss_ratio, :sample_count
          when :octets_count
            SW_ERR "ERROR: not supported  #{metric_type}\n"
          else
            SW_ERR "ERROR: unknown  #{metric_type}\n"
          end
          case metric_type
          when :frame_loss
            aggr_ref_strs   << "SUM(#{Stats::METRIC_NAMES[:frames_sent]}) - SUM(#{Stats::METRIC_NAMES[:frames_received]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
          when :frame_loss_ratio
            aggr_ref_strs   << "(SUM(#{Stats::METRIC_NAMES[:frames_sent]}) - SUM(#{Stats::METRIC_NAMES[:frames_received]}))/SUM(#{Stats::METRIC_NAMES[:frames_sent]})*100 AS `#{Stats::METRIC_NAMES[metric_type]}`"
          when :sample_count
            aggr_ref_strs << "COUNT(*) AS `#{Stats::METRIC_NAMES[metric_type]}`"
          else
            aggr_ref_strs   << "#{aggr_operation_str}(#{column_info[metric_type]}) AS `#{Stats::METRIC_NAMES[metric_type]}`"
          end
        end
        if 0 != number_of_ctvs_per_shard
          id = hash.first[1]
          shard_id = which_shard_id id, number_of_ctvs_per_shard
          ref_table_name_suffix = "_shard#{shard_id}"
        end
        aggr_ref_str = aggr_ref_strs.join(", ")
        sql_subqueries << ', ' if sql_subqueries != ''
        sql_subqueries << <<EOF
(SELECT
#{aggr_ref_str}
FROM `#{class_name}#{ref_table_name_suffix}`
WHERE (#{where_time_str}) AND (#{ref_where_str})
)t#{ref_index}
EOF
        ref_index += 1
      end
      ref_str     = ref_strs.join(", ")
      delta_str   = delta_strs.join(", ")

      sql_query = <<EOF
SELECT
#{total_str},
#{ref_str},
#{delta_str}
FROM
(SELECT
#{aggr_total_str}
FROM `#{class_name}#{table_name_suffix}`
WHERE (#{where_time_str}) AND (#{where_str})
)t,
#{sql_subqueries}
EOF
      return nil if conn.nil?
      results = execute_sql_query conn, sql_query
      output_array = Array.new
      results.each do |record|
        output_array << record
      end
      if (output_array[0].nil?)
        SW_ERR "ERROR:  Are we connected to the archive? Query returned no data for query: #{sql_query}\n"
      end
      return output_array
    end
=begin
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      SW_ERR "DataArchiveIf: #{e.message} "
      return nil
    end
=end
  end

  def self.simplified_query_aggregate_and_count_test sharded
    class_name = 'ethframedelaytest'
    time_name = 'sample_end_epoch'
    where = nil
    ref_where =nil
    if sharded
      circuit_id          = 10
      ref_circuit_id      = 7
      last_hop_circuit_id = 9
      where = {'grid_circuit_id' => circuit_id }
      ref_where = [{'grid_circuit_id' => ref_circuit_id }, {'grid_circuit_id' => last_hop_circuit_id }  ]
    else
      circuit_id = 'AU54XC356-RS-P-V2527-CIR00055-P6'
      ref_where = [ {'CircuitId' => "#{ref_circuit_id}"} ]
      circuit_id          = 'AU54XC224-RN-P-V2503-CIR00055-P6'
      ref_circuit_id      = 'AU13XC092-DNR-P-V2501-CIR00250-P6'
      last_hop_circuit_id = 'AU54XC226-RN-P-V2507-CIR00055-P6'
      where = {'CircuitId' => "#{circuit_id}"}
      ref_where = [{'CircuitId' => "#{ref_circuit_id}"}, {'CircuitId' => "#{last_hop_circuit_id}"}  ]
    end
    time_periods = [[ 1325368860,1325368860+1294476]]
    time_increment = 300
    column_info = SpirentStatsArchive::SPIRENT_METRIC_COLUMNS
    thresholds ={    :frame_loss_ratio => 70,
      :delay=> 3000,
      :delay_variation => 800,
    }
    [
      :frame_loss_ratio,
      :delay,
      :delay_variation,
    ].each do |metric_type|
      x= DataArchiveIf.simplified_query_aggregate_and_count metric_type, class_name, time_name, where, column_info, time_periods, time_increment, thresholds[metric_type]
      puts "COUNT = #{x[0]['COUNT']}"
    end
  end
=begin

SELECT count(*) AS COUNT FROM
(SELECT (1325368860 + TRUNCATE((sample_end_epoch - 1325368860)/300, 0) * 300 + 300) as time ,
AVG(AverageDelay/2) AS `averageLatency`
FROM `spirent_archive`.`ethframedelaytest_archive`
WHERE ((`sample_end_epoch` BETWEEN 1325368860 AND 1326663336)) AND (( `CircuitId`='AU54XC356-RS-P-V2527-CIR00055-P6' ))
GROUP BY time ) t
WHERE ((`averageLatency` > 3000));

+-------+
| COUNT |
+-------+
|  3850 |
+-------+

=end

  def self.simplified_query_aggregate_and_count metric_type, class_name, time_name, where, column_info, time_periods, time_increment, threshold_value
    return [{"COUNT" => 0}] if time_periods.empty?
    conn = nil
    begin
      raise "time_increment must be greater than zero" if (time_increment < 1)
      raise "time period in time_periods must contain two time entries: start and end: #{time_periods.inspect}" if (time_periods[0].size != 2)
      time_start = nil
      time_periods.each do | time_period |
        raise " start time(#{time_period[0]}) > end time(#{time_period[1]})" if time_period[0]>time_period[1]
        time_start = time_period[0] if time_start.nil?
        time_start = time_period[0] if time_period[0] < time_start
      end
      raise "too many keys specified" if where.size != 1
      conn, number_of_ctvs_per_shard  = self.connect
      table_name_suffix = '_archive'
      if 0 != number_of_ctvs_per_shard
        id = where.first[1]
        shard_id = which_shard_id id, number_of_ctvs_per_shard
        table_name_suffix = "_shard#{shard_id}"
      end
      where_time_str = time_periods.map { | time_period | "(`#{time_name}` BETWEEN #{time_period[0] } AND #{time_period[1]})" }.join( " OR ")
      where_str = ("("+ where.map { |column_name, value| " `#{column_name}`='#{value}' " }.join(" AND ") +")")
      select_time_str= "(#{time_start} + TRUNCATE((#{time_name} - #{time_start})/#{time_increment}, 0) * #{time_increment} + #{time_increment}) as time "
      sql_query = ''
      case metric_type
      when :frame_loss
        sql_query = <<EOF
SELECT count(*) AS COUNT FROM
(SELECT #{select_time_str},
SUM(#{column_info[:frames_sent]}) - SUM(#{column_info[:frames_received]}) AS `#{Stats::METRIC_NAMES[metric_type]}`
FROM `#{class_name}#{table_name_suffix}`
WHERE (#{where_time_str}) AND (#{where_str})
GROUP BY time ) t
WHERE ((`#{Stats::METRIC_NAMES[metric_type]}` > #{threshold_value}));
EOF

      when :frame_loss_ratio
        sql_query = <<EOF
SELECT count(*) AS COUNT FROM
(SELECT #{select_time_str},
100*(SUM(#{column_info[:frames_sent]}) - SUM(#{column_info[:frames_received]}))/SUM(#{column_info[:frames_sent]}) AS `#{Stats::METRIC_NAMES[metric_type]}`
FROM `#{class_name}#{table_name_suffix}`
WHERE (#{where_time_str}) AND (#{where_str})
GROUP BY time ) t
WHERE ((`#{Stats::METRIC_NAMES[metric_type]}` > #{threshold_value}));
EOF
      when :delay, :delay_variation
        sql_query = <<EOF
SELECT count(*) AS COUNT FROM
(SELECT #{select_time_str},
AVG(#{column_info[metric_type]}) AS `#{Stats::METRIC_NAMES[metric_type]}`
FROM `spirent_archive`.`ethframedelaytest_archive`
WHERE (#{where_time_str}) AND (#{where_str})
GROUP BY time ) t
WHERE ((`#{Stats::METRIC_NAMES[metric_type]}` > #{threshold_value}));
EOF
      when :min_delay, :min_delay_variation, :max_delay, :max_delay_variation, :octets_count, :sample_count
        SW_ERR "ERROR: not supported  #{metric_type}\n"
      else
        SW_ERR "ERROR: unknown  #{metric_type}\n"
      end
      return nil if conn.nil?
      results = execute_sql_query conn, sql_query
      output_array = Array.new
      results.each do |record|
        output_array << record
      end
      if (output_array[0].nil?)
        SW_ERR "ERROR:  Are we connected to the archive? Query returned no data for #{metric_type} query: #{sql_query}\n"
      end
      return output_array
    end
=begin
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      SW_ERR "DataArchiveIf: #{e.message} "
      return nil
    end
=end
  end

  def self.differential_query_aggregate_and_count_test sharded
    class_name = 'ethframedelaytest'
    time_name = 'sample_end_epoch'
    where = nil
    ref_where =nil
    if sharded
      circuit_id          = 10
      ref_circuit_id      = 7
      last_hop_circuit_id = 9
      where = {'grid_circuit_id' => circuit_id }
      ref_where = [{'grid_circuit_id' => ref_circuit_id }, {'grid_circuit_id' => last_hop_circuit_id }  ]
    else
      circuit_id = 'AU54XC356-RS-P-V2527-CIR00055-P6'
      ref_where = [ {'CircuitId' => "#{ref_circuit_id}"} ]
      circuit_id          = 'AU54XC224-RN-P-V2503-CIR00055-P6'
      ref_circuit_id      = 'AU13XC092-DNR-P-V2501-CIR00250-P6'
      last_hop_circuit_id = 'AU54XC226-RN-P-V2507-CIR00055-P6'
      where = {'CircuitId' => "#{circuit_id}"}
      ref_where = [{'CircuitId' => "#{ref_circuit_id}"}, {'CircuitId' => "#{last_hop_circuit_id}"}  ]
    end
    time_periods = [[ 1325368860,1325368860+1294476]]
    time_increment = 300
    column_info = SpirentStatsArchive::SPIRENT_METRIC_COLUMNS
    thresholds ={    :frame_loss_ratio => 0.7,
      :delay=> 1700,
      :delay_variation => 800,
    }
    [
      :frame_loss_ratio,
      :delay,
      :delay_variation,
    ].each do |metric_type|
      x= DataArchiveIf.differential_query_aggregate_and_count metric_type, class_name, time_name, where, ref_where, column_info, time_periods, time_increment, thresholds[metric_type]
      puts "COUNT = #{x[0]['COUNT']}"
    end
  end

=begin

SELECT count(*) AS COUNT FROM
(SELECT
 t.time, t.TotalMetric - t1.RefMetric AS `averageLatency`  FROM
(SELECT (1325368860 + TRUNCATE((sample_end_epoch - 1325368860)/300, 0) * 300 + 300) as time ,
AverageDelay/2 AS TotalMetric
FROM `spirent_archive`.`ethframedelaytest_archive`
WHERE ((`sample_end_epoch` BETWEEN 1325368860 AND 1325368860+1294476)) AND (`CircuitId`='AU54XC356-RS-P-V2527-CIR00055-P6')
) t
LEFT JOIN
(SELECT (1325368860 + TRUNCATE((sample_end_epoch - 1325368860)/300, 0) * 300 + 300) as time ,
AverageDelay/2 AS RefMetric
FROM `spirent_archive`.`ethframedelaytest_archive`
WHERE ((`sample_end_epoch` BETWEEN 1325368860 AND 1325368860+1294476)) AND (`CircuitId`='AU60XC653-DNR-P-V2525-CIR00250-P6')
) t1
ON t.time = t1.time
)t
WHERE ((`averageLatency` > 1700));

+-------+
| COUNT |
+-------+
|  2649 |
+-------+


=end

  def self.differential_query_aggregate_and_count metric_type, class_name, time_name, where, ref_where, column_info, time_periods, time_increment, threshold_value
    return nil if where.nil? or where.empty?
    return simplified_query_aggregate_and_count  metric_type, class_name, time_name, where, column_info, time_periods, time_increment, threshold_value  if ref_where.nil? or ref_where.empty?
    return [{"COUNT" => 0}] if time_periods.empty?
    conn = nil
    begin
      raise "time_increment must be greater than zero" if (time_increment < 1)
      raise "time period in time_periods must contain two time entries: start and end: #{time_periods.inspect}" if (time_periods[0].size != 2)
      time_start = nil
      time_periods.each do | time_period |
        raise " start time(#{time_period[0]}) > end time(#{time_period[1]})" if time_period[0]>time_period[1]
        time_start = time_period[0] if time_start.nil?
        time_start = time_period[0] if time_period[0] < time_start
      end
      raise "too many keys specified" if where.size != 1
      # raise "too many keys specified" if ref_where.size != 1
      conn, number_of_ctvs_per_shard  = self.connect
      table_name_suffix = '_archive'
      ref_table_name_suffix = '_archive'
      if 0 != number_of_ctvs_per_shard
        id = where.first[1]
        shard_id = which_shard_id id, number_of_ctvs_per_shard
        table_name_suffix = "_shard#{shard_id}"
        id = ref_where.first.first[1]
        shard_id = which_shard_id id, number_of_ctvs_per_shard
        ref_table_name_suffix = "_shard#{shard_id}"
      end
      where_time_str = time_periods.map { | time_period | "(`#{time_name}` BETWEEN #{time_period[0] } AND #{time_period[1]})" }.join( " OR ")
      where_str = ("("+ where.map { |column_name, value| " `#{column_name}`='#{value}' " }.join(" AND ") +")")
      ref_where_str = ("("+ ref_where[0].map { |column_name, value| " `#{column_name}`='#{value}' " }.join(" AND ") +")")
      select_time_str= "(#{time_start} + TRUNCATE((#{time_name} - #{time_start})/#{time_increment}, 0) * #{time_increment} + #{time_increment}) as time "
      sql_query = ''
      case metric_type
      when :frame_loss
        sql_query = <<EOF
SELECT count(*) AS COUNT FROM
(SELECT
t.TotalTx - ( t.TotalRx + (t1.RefTx - t1.RefRx)) AS `#{Stats::METRIC_NAMES[metric_type]}`
FROM
(SELECT #{select_time_str},
#{column_info[:frames_sent]} AS TotalTx, #{column_info[:frames_received]} AS TotalRx
FROM `#{class_name}#{table_name_suffix}`
WHERE (#{where_time_str}) AND (#{where_str})
) t
LEFT JOIN
(SELECT #{select_time_str},
#{column_info[:frames_sent]} AS RefTx, #{column_info[:frames_received]} AS RefRx
FROM `#{class_name}#{ref_table_name_suffix}`
WHERE (#{where_time_str}) AND (#{ref_where_str})
) t1
ON t.time = t1.time
)t
WHERE ((`#{Stats::METRIC_NAMES[metric_type]}` > #{threshold_value}));
EOF
      when :frame_loss_ratio
        sql_query = <<EOF
SELECT count(*) AS COUNT FROM
(SELECT
 100* (t.TotalTx - ( t.TotalRx + (t1.RefTx - t1.RefRx))) / t.TotalRx AS `#{Stats::METRIC_NAMES[metric_type]}`
FROM
(SELECT #{select_time_str},
#{column_info[:frames_sent]} AS TotalTx, #{column_info[:frames_received]} AS TotalRx
FROM `#{class_name}#{table_name_suffix}`
WHERE (#{where_time_str}) AND (#{where_str})
) t
LEFT JOIN
(SELECT #{select_time_str},
#{column_info[:frames_sent]} AS RefTx, #{column_info[:frames_received]} AS RefRx
FROM `#{class_name}#{ref_table_name_suffix}`
WHERE (#{where_time_str}) AND (#{ref_where_str})
) t1
ON t.time = t1.time
)t
WHERE ((`#{Stats::METRIC_NAMES[metric_type]}` > #{threshold_value}));
EOF
      when :delay, :delay_variation
        sql_query = <<EOF
SELECT count(*) AS COUNT FROM
(SELECT
 t.time, t.TotalMetric - t1.RefMetric AS `#{Stats::METRIC_NAMES[metric_type]}`  FROM
(SELECT #{select_time_str},
#{column_info[metric_type]} AS TotalMetric
FROM `#{class_name}#{table_name_suffix}`
WHERE (#{where_time_str}) AND (#{where_str})
) t
LEFT JOIN
(SELECT #{select_time_str},
#{column_info[metric_type]} AS RefMetric
FROM `#{class_name}#{ref_table_name_suffix}`
WHERE (#{where_time_str}) AND (#{ref_where_str})
) t1
ON t.time = t1.time
)t
WHERE ((`#{Stats::METRIC_NAMES[metric_type]}` > #{threshold_value}));
EOF
      when :min_delay, :min_delay_variation, :max_delay, :max_delay_variation, :octets_count, :sample_count
        SW_ERR "ERROR: not supported  #{metric_type}\n"
      else
        SW_ERR "ERROR: unknown  #{metric_type}\n"
      end
      return nil if conn.nil?
      results = execute_sql_query conn, sql_query
      output_array = Array.new
      results.each do |record|
        output_array << record
      end
      if (output_array[0].nil?)
        SW_ERR "ERROR:  Are we connected to the archive? Query returned no data for #{metric_type} query: #{sql_query}\n"
      end
      return output_array
    end
=begin
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      SW_ERR "DataArchiveIf: #{e.message} "
      return nil
    end
=end
  end

  #------------------------------------------------------------------------
  #ruby-1.8.7-p248 :001 > DataArchiveIf.query_delay_tests '100050', [[1295441073999, 1295441073999] ]
  # => [{"neTestIndex"=>"VPLS 100050 mep 1 to mep 2"}, {"neTestIndex"=>"VPLS 100050 mep 2  to mep 11"}]
  #SELECT `neTestIndex` FROM `ethernetoam.CfmTwoWayDelayTest_archive` WHERE `maintenanceAssociationId`='100050'  AND ((`timeFirstObserved` < 1295441073999 < `timeLastObserved`) OR (`timeFirstObserved` < 1295441073999 < `timeLastObserved`));
  def self.query_delay_tests maintenanceAssociationId, time_periods_msecs
    conn = nil
    begin
      conn, number_of_ctvs_per_shard  = self.connect
      table_name_suffix = '_archive'
      sql_str = String.new
      sql_str << "SELECT `neTestIndex` FROM `sam_archive`.`ethernetoam.CfmTwoWayDelayTest_archive` WHERE `maintenanceAssociationId`='#{maintenanceAssociationId}'  "
      sql_str << "AND ("
      sql_str << (time_periods_msecs.map { | time_period | "(`timeFirstObserved` < #{time_period[0]} AND #{time_period[0]} < `timeLastObserved`) OR (`timeFirstObserved` < #{time_period[1]} AND #{time_period[1]} < `timeLastObserved`)"}.join( " OR "))
      sql_str << ")"
      if MONITOR_STATEMENTS
        sql_str2 = String.new
        sql_str2 << "SELECT `neTestIndex` FROM `sam_archive`.`ethernetoam.CfmTwoWayDelayTest_archive` WHERE `maintenanceAssociationId`='#maintenanceAssociationId#'  "
        sql_str2 << "AND (`timeFirstObserved` < #time_start#) AND (#time_end# < `timeLastObserved`) OR (`timeFirstObserved` < #time_start# AND #time_end# < `timeLastObserved`)"
        stmt = ArchiveStatement.find( :first, :conditions => [ "template=?", sql_str2 ])
        if stmt.nil?
          stmt = ArchiveStatement.new
          stmt.template = sql_str2;
          stmt.statement = sql_str;
          stmt.stack_trace  = Kernel.caller(0)
          stmt.save
        end
      end
      return nil if conn.nil?
      results = execute_sql_query conn, sql_str
      output_array = Array.new
      results.each do |record|
        output_array << record
      end
      return output_array
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      return nil
    end
  end

  #  class_name = 'ethframedelaytest'
  #  time_name = 'time_start'
  #  columns = ['CircuitId', 'time_start', 'available']
  #  where = [ [   "`CircuitId`='blablabla'" ],  ]
  #  limit = number or nil. If nil return all records
  def self.last_availability_for(class_name, time_name, columns, where, limit=1)
    conn = nil
    begin
      limit_str = "DESC LIMIT #{limit}"
      limit_str = "" if !limit
      sql_str = String.new
      sql_str << 'SELECT '
      sql_str << (columns.map { |name| "`#{name}`"}.join(", ")) unless columns.nil?
      sql_str << " FROM `#{class_name}_availability` "
      sql_str << (' WHERE ' + (where.map{|array| ( "("+ array.map { |value| "#{value}" }.join(" AND ") +")")}).join(" OR ") ) unless where.nil?
      sql_str << " ORDER BY #{time_name} #{limit_str}"
      conn, number_of_ctvs_per_shard  = self.connect
      return nil if conn.nil?
      results = execute_sql_query conn, sql_str
      output_array = Array.new
      results.each do |record|
        output_array << record
      end
      return output_array
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      return nil
    end
  end

  #  class_name = 'ethframedelaytest'
  #  time_name = 'time_start'
  #  columns = ['CircuitId', 'time_start', 'available']
  def self.for_each_availability_change(class_name, time_name, columns, where, start_time)
    conn = nil
    begin
      sql_str = String.new
      sql_str << 'SELECT '
      sql_str << (columns.map { |name| "`#{name}`"}.join(", ")) unless columns.nil?
      sql_str << " FROM `#{class_name}_availability` "
      sql_str << " WHERE (`#{time_name}`>#{start_time})"
      sql_str << (" AND (" + (where.map{|array| ( "("+ array.map { |value| "#{value}" }.join(" AND ") +")")}).join(" OR ") + ")") unless where.nil?
      sql_str << " ORDER BY #{time_name} "
      
      #      if MONITOR_STATEMENTS
      #        sql_str2 = String.new
      #        sql_str2 << "SELECT `neTestIndex` FROM `sam_archive`.`ethernetoam.CfmTwoWayDelayTest_archive` WHERE `maintenanceAssociationId`='#maintenanceAssociationId#'  "
      #        sql_str2 << "AND (`timeFirstObserved` < #time_start#) AND (#time_end# < `timeLastObserved`) OR (`timeFirstObserved` < #time_start# AND #time_end# < `timeLastObserved`)"
      #        stmt = ArchiveStatement.find( :first, :conditions => [ "template=?", sql_str2 ])
      #        if stmt.nil?
      #          stmt = ArchiveStatement.new
      #          stmt.template = sql_str2;
      #          stmt.statement = sql_str;
      #          stmt.stack_trace  = Kernel.caller(0)
      #          stmt.save
      #        end
      #      end
      conn, number_of_ctvs_per_shard  = self.connect
      return nil if conn.nil?
      results = execute_sql_query conn, sql_str
      results.each do |record|
        yield record
      end
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      return nil
    end

  end

  def self.which_shard_id grid_circuit_id, number_of_ids_per_shard
    return  grid_circuit_id/number_of_ids_per_shard + 1
  end


  def self.get_database_size(db_name)
    conn = nil
    sizes = []
    begin
      conn, number_of_ctvs_per_shard  = self.connect
      if 0 != number_of_ctvs_per_shard
        sql_query = <<EOF
SELECT table_schema "Database Name", sum( data_length + index_length ) / 1024 / 1024 "Database Size in MB" 
FROM information_schema.TABLES GROUP BY table_schema;
EOF
        sizes = execute_sql_query(conn, sql_query)
        sizes.each do |size|
          if size["Database Name"] == db_name
            return size["Database Size in MB"].round
          end
        end
      end
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      return nil
    end
    return nil
  end

  # querying all the shards is slow, so get all the needed values from one round of queries
  def self.analyze_shards(db_name, class_name)
    conn = nil
    output = {:shard_count => 0, :first_empty => 'no empty shards', :count_empty => 0, :partial_shards => {}, :gid_list => []}
    begin
      conn, number_of_ctvs_per_shard  = self.connect
      if 0 != number_of_ctvs_per_shard
        table_name_suffix = 0
        begin
          while true
            table_name_suffix += 1
            sql_query = <<EOF
SELECT DISTINCT grid_circuit_id FROM `#{db_name}`.`#{class_name}_shard#{table_name_suffix}`;
EOF
            results = execute_sql_query(conn, sql_query)
            output[:shard_count] += 1
            if results.size == 0
              output[:first_empty] = "#{class_name}_shard#{table_name_suffix}" if output[:first_empty] == 'no empty shards'
              output[:count_empty] += 1
            else
              output[:partial_shards]["#{class_name}_shard#{table_name_suffix}"] = number_of_ctvs_per_shard - results.size if results.size < number_of_ctvs_per_shard
            end
            results.each{|result| output[:gid_list].push((result["grid_circuit_id"]).to_i)}
          end
        rescue ActiveRecord::StatementInvalid => e
          output[:space_empty] = output[:count_empty] * number_of_ctvs_per_shard
          return output
        end
      end
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      return nil
    end
  end

  def self.newest(db_name, class_name)
    conn = nil
    output = 0
    begin
      conn, number_of_ctvs_per_shard  = self.connect
      if 0 != number_of_ctvs_per_shard
        table_name_suffix = 0
        begin
          while true
            table_name_suffix += 1
            sql_query = <<EOF
SELECT MAX(sample_end_epoch) FROM `#{db_name}`.`#{class_name}_shard#{table_name_suffix}`;
EOF
            result = (execute_sql_query(conn, sql_query))[0]["MAX(sample_end_epoch)"]
            output = result if result and result > output
          end
        rescue ActiveRecord::StatementInvalid => e
          return output
        end
      end
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      return nil
    end
  end



  def self.count_connections
    conn = nil
    begin
      conn, number_of_ctvs_per_shard  = self.connect
      sql_query = "show status like '%connect%';"
      conn.execute(sql_query).each{|result| return result[1] if result[0] == "Connections"}
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      return nil
    end
  end

  def self.list_processes
    conn = nil
    begin
      conn, number_of_ctvs_per_shard  = self.connect
      sql_query = "show processlist;"
      execute_sql_query(conn, sql_query)
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      return nil
    end
  end

  def self.slow_query_status
    conn = nil
    results = {}
    begin
      conn, number_of_ctvs_per_shard  = self.connect
      sql_query = "show global status like '%slow%';"
      conn.execute(sql_query).each{|result| results[:count] = result[1] if result[0] == 'Slow_queries'}
      sql_query = "show global variables like '%lo%que%';"
      conn.execute(sql_query).each do |result|
        results[:status] = result[1] if result[0] == 'slow_query_log'
        results[:file] = result[1] if result[0] == 'slow_query_log_file'
        results[:time] = result[1] if result[0] == 'long_query_time'
      end
      return results
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      return nil
    end
  end

  def self.get_curcuit (db_name, class_name, gid)
    conn = nil
    output = {}
    begin
      conn, number_of_ctvs_per_shard  = self.connect
      if 0 != number_of_ctvs_per_shard
        table_name_suffix = which_shard_id(gid, number_of_ctvs_per_shard)
        sql_query = <<SQL
SELECT * FROM `#{db_name}`.`#{class_name}_shard#{table_name_suffix}`
WHERE grid_circuit_id = #{gid} ORDER BY sample_end_epoch ASC;
SQL
        curcuit_entries = execute_sql_query(conn, sql_query)
        if curcuit_entries.size != 0
          output[:record_start_epoch] = Time.at(curcuit_entries[0]["sample_end_epoch"]).to_datetime
          output[:record_end_epoch] =  Time.at(curcuit_entries[curcuit_entries.size - 1]["sample_end_epoch"]).to_datetime

          #logic for adding other key=>value pairs here.
        else
          nil # curcuit not found
        end
      end
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      return nil
    end
    return output
  end
  #------------------------------------------------------------------------
  #------------------------------------------------------------------------
  #------------------------------------------------------------------------

  def self.get_all_grid_ids(class_name)
    conn = nil
    output_array = []
    begin
      conn, number_of_ctvs_per_shard  = self.connect
      table_name_suffixes = ['_archive']
      if 0 != number_of_ctvs_per_shard
        table_name_suffixes = []
        (1..1000).each {|shard_id| table_name_suffixes << "_shard#{shard_id}"}
      end

      table_name_suffixes.each do |table_name_suffix, grid_ids|
        sql_query = <<EOF
SELECT DISTINCT grid_circuit_id FROM `#{class_name}#{table_name_suffix}`;
EOF
       output_array.concat(execute_sql_query(conn, sql_query)) rescue nil
      end
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      return nil
    end
    return output_array.collect {|row| row.values}.flatten
  end
  
  def self.delete_spirint_grid(class_name, grid_circuit_id)
    conn = nil
    begin
      # Availability
      sql_str = String.new
      sql_str << 'DELETE '
      sql_str << " FROM `#{class_name}_availability` "
      sql_str << " WHERE (`grid_circuit_id` = #{grid_circuit_id})"

      conn, number_of_ctvs_per_shard  = self.connect
      return nil if conn.nil?
      results = execute_sql_query conn, sql_str
      
      table_name_suffix = '_archive'
      if 0 != number_of_ctvs_per_shard
        shard_id = which_shard_id(grid_circuit_id, number_of_ctvs_per_shard)
        table_name_suffix = "_shard#{shard_id}"
      end
            
      sql_str = String.new
      sql_str << 'DELETE '
      sql_str << " FROM `#{class_name}#{table_name_suffix}` "
      sql_str << " WHERE (`grid_circuit_id` = #{grid_circuit_id})"
      
      results = execute_sql_query conn, sql_str
          
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      return nil
    end
  end  
    
  def self.get_last_record(class_name, time_name, where)
    conn = nil
    begin      
      conn, number_of_ctvs_per_shard = self.connect
      table_name_suffix = '_archive'
      if 0 != number_of_ctvs_per_shard
        id = where.first[1]
        shard_id = which_shard_id id, number_of_ctvs_per_shard
        table_name_suffix = "_shard#{shard_id}"
      end
      sql_str = String.new
      sql_str << "SELECT #{time_name} as time"
      sql_str << " FROM `#{class_name}#{table_name_suffix}` "
      sql_str << " WHERE "
      sql_str << ("("+ where.map { |column_name, value| " `#{column_name}`='#{value}' " }.join(" AND ") +")")  unless where.nil?      
      sql_str << (" ORDER BY `time` DESC LIMIT 1" )

      return nil if conn.nil?
      results = execute_sql_query conn, sql_str
      output_array = Array.new
      results.each do |record|
        output_array << record
      end
      # replace `time` by time_name in output
      output_array.each {|result|
        result[time_name] = result["time"]
        result.delete("time")}
      return output_array.first
    rescue Exception => e
      Rails.logger.error "DataArchiveIf: #{e.message}   #{e.backtrace.join(';')}"
      SW_ERR "DataArchiveIf: #{e.message} "
      return nil
    end
  end

  def self.test_aggregation 
    class_name = "service.SapBaseStatsLogRecord"
    time_name = "timeCaptured"
    where1 = [ #OR
      [   "`monitoredObjectSiteId`='10.10.100.6'" , "`displayedName`='Lag 11:1030.0'",  ],
      [   "`monitoredObjectSiteId`='10.10.100.5'" , "`displayedName`='Lag 11:1030.0'",  ],
      [   "`monitoredObjectSiteId`='10.10.100.5'" , "`displayedName`='Lag 12:1030.0'",  ],
      [   "`monitoredObjectSiteId`='10.10.100.5'" , "`displayedName`='Lag 199:1030.0'", ],
    ]
    group_by = ["monitoredObjectSiteId" , "displayedName"]
    attributes2 = {
      "egressQChipDroppedInProfPackets_delta"    => "SUM(egressQChipDroppedInProfPackets_delta)",
      "egressQChipDroppedOutProfPackets_delta"   => "SUM(egressQChipDroppedOutProfPackets_delta)",
      "ingressQChipDroppedHiPrioPackets_delta"   => "SUM(ingressQChipDroppedHiPrioPackets_delta)",
      "ingressQChipDroppedLoPrioPackets_delta"   => "SUM(ingressQChipDroppedLoPrioPackets_delta)",
      "ingressQChipForwardedInProfPackets_delta" => "SUM(ingressQChipForwardedInProfPackets_delta)",
      "ingressQChipForwardedOutProfPackets_delta"=> "SUM(ingressQChipForwardedOutProfPackets_delta)",
    }
    time_periods2 = [
      [1298815200000 , 1300629600000]
    ]
    time_increment_msecs = 86400000 # 1 day
    timestart = Time.now
    result= self.query_and_aggregate "sam_archive", class_name, time_name, where1, nil, nil, attributes2, time_periods2, time_increment_msecs
    log_completion_time " typical FLR query, aggregation group by time only", timestart
    timestart = Time.now
    result= self.query_and_aggregate "sam_archive", class_name,time_name, where1, group_by, nil, attributes2, time_periods2, time_increment_msecs
    log_completion_time " typical FLR query, aggregation group by all", timestart
    order_by = ["timeCaptured"]
    timestart = Time.now
    results= self.query_and_aggregate "sam_archive", class_name,time_name, where1, group_by, order_by, attributes2, time_periods2, time_increment_msecs
    log_completion_time " typical FLR query, aggregation sorted by time", timestart
    puts "test_aggregation"
    pp results

    return results
  end
  #------------------------------------------------------------------------

  def self.test_aggregation_fdv 
    class_name = "ethernetoam.CfmTwoWayDelayTestResult"
    time_name = "timeCaptured"
    where =   [
      [ "`neTestIndex`='100573676013 mep 1 to mep 2'" ,
        "`execTrigger`='neTriggered'",
        "`maintenanceAssociationId`='50000'" ],
    ]
    group_by = ["neTestIndex", "execTrigger", "maintenanceAssociationId"]

    attributes2 = {
      :delay_variation    => "AVG(roundTripJitter)",
      "averageRoundTripTime"   => "AVG(averageRoundTripTime)",
    }
    time_periods2 = [
      [1298815200000 , 1300629600000]
    ]
    order_by = ["timeCaptured"]
    time_increment_msecs = 86400000 # 1 day
    timestart = Time.now
    results= self.query_and_aggregate "sam_archive", class_name, time_name, where, group_by, order_by, attributes2, time_periods2, time_increment_msecs
    log_completion_time " typical FLR query, aggregation", timestart
    puts "test_aggregation_fdv"
    pp results
    return results
  end
  #------------------------------------------------------------------------

  def self.test_summary 
    class_name = "service.SapBaseStatsLogRecord"
    time_name = "timeCaptured"
    where1 = [ #OR
      [   "`monitoredObjectSiteId`='10.10.100.6'" , "`displayedName`='Lag 11:1030.0'",  ],
      [   "`monitoredObjectSiteId`='10.10.100.5'" , "`displayedName`='Lag 11:1030.0'",  ],
      [   "`monitoredObjectSiteId`='10.10.100.5'" , "`displayedName`='Lag 12:1030.0'",  ],
      [   "`monitoredObjectSiteId`='10.10.100.5'" , "`displayedName`='Lag 199:1030.0'", ],
    ]
    group_by = ["monitoredObjectSiteId" , "displayedName"]
    attributes2 = {
      "egressQChipDroppedInProfPackets_delta"    => "SUM(egressQChipDroppedInProfPackets_delta)",
      "egressQChipDroppedOutProfPackets_delta"   => "SUM(egressQChipDroppedOutProfPackets_delta)",
      "ingressQChipDroppedHiPrioPackets_delta"   => "SUM(ingressQChipDroppedHiPrioPackets_delta)",
      "ingressQChipDroppedLoPrioPackets_delta"   => "SUM(ingressQChipDroppedLoPrioPackets_delta)",
      "ingressQChipForwardedInProfPackets_delta" => "SUM(ingressQChipForwardedInProfPackets_delta)",
      "ingressQChipForwardedOutProfPackets_delta"=> "SUM(ingressQChipForwardedOutProfPackets_delta)",
    }
    time_periods2 = [
      [1298815200000 , 1300629600000]
    ]
    timestart = Time.now
    results= self.query_and_summarize "sam_archive", class_name,time_name, where1, group_by, attributes2, time_periods2
    log_completion_time " typical FLR query, summary", timestart
    puts "test_summary"
    pp results
    return results
  end
  #------------------------------------------------------------------------
  def self.generate_times time_start, time_end
    time = time_start
    increment = (time_end - time_start)/80.0
    output = Array.new
    while time <= 1300629600000
      time_period = Array.new
      time_period << time.to_i
      time_period << time.to_i + 300000
      output << time_period
      time = time + increment
    end
    return output
  end

  def self.test_no_aggregation 
    class_name = "service.SapBaseStatsLogRecord"
    time_name = "timeCaptured"
    where1 = [
      [   "`monitoredObjectSiteId`='10.10.100.6'" , "`displayedName`='Lag 11:1030.0'",  ],
      [   "`monitoredObjectSiteId`='10.10.100.5'" , "`displayedName`='Lag 11:1030.0'",  ],
      [   "`monitoredObjectSiteId`='10.10.100.5'" , "`displayedName`='Lag 12:1030.0'",  ],
      [   "`monitoredObjectSiteId`='10.10.100.5'" , "`displayedName`='Lag 199:1030.0'", ],
    ]
    group_by = ["monitoredObjectSiteId" , "displayedName"]
    attributes2 = [
      "egressQChipDroppedInProfPackets"   ,
      "egressQChipDroppedOutProfPackets"  ,
      "ingressQChipDroppedHiPrioPackets"  ,
      "ingressQChipDroppedLoPrioPackets"  ,
      "ingressQChipForwardedInProfPackets",
      "ingressQChipForwardedOutProfPackets",
    ]
    time_periods =  generate_times 1298815200000 , 1300629600000
    timestart = Time.now
    results= self.query_dont_aggregate "sam_archive", class_name,time_name, where1, group_by, nil, attributes2, time_periods
    log_completion_time " typical FLR query, NO aggregation", timestart
    return results
  end
  #------------------------------------------------------------------------
  def self.test_bruce 
    # but always within 1 second of arrival.
    time_periods_msecs = [[1303243207000, 1303329607000], [1303416007000, 1303502407000]]
    class_name = "service.SapBaseStatsLogRecord"
    where=[
      ["`displayedName`='Lag 11:1152.0'", "`monitoredObjectSiteId`='10.10.100.5'"],
      ["`displayedName`='Lag 11:1152.0'", "`monitoredObjectSiteId`='10.10.100.6'"],
      ["`displayedName`='Lag 12:1252.0'", "`monitoredObjectSiteId`='10.10.100.5'"],
      ["`displayedName`='Lag 199:52.0'",  "`monitoredObjectSiteId`='10.10.100.5'"]]
    group_by=["monitoredObjectSiteId", "displayedName"]
    attributes={
      "egressQChipDroppedOutProfPackets_delta"=>"SUM(egressQChipDroppedOutProfPackets_delta)",
      "ingressQChipForwardedInProfPackets_delta"=>"SUM(ingressQChipForwardedInProfPackets_delta)",
      "egressQChipForwardedInProfPackets_delta"=>"SUM(egressQChipForwardedInProfPackets_delta)",
      "egressQChipDroppedInProfPackets_delta"=>"SUM(egressQChipDroppedInProfPackets_delta)",
      "egressQChipForwardedOutProfPackets_delta"=>"SUM(egressQChipForwardedOutProfPackets_delta)",
      "ingressQChipForwardedOutProfPackets_delta"=>"SUM(ingressQChipForwardedOutProfPackets_delta)"}
    time_name = "timeCaptured"
    timestart = Time.now
    results= query_and_summarize "sam_archive", class_name, time_name, where, group_by, attributes, time_periods_msecs
    log_completion_time " test_bruce", timestart
    puts "test_bruce"
    pp results
    return results
  end

  #------------------------------------------------------------------------
  def self.test_bruce2 
    class_name = "service.SapBaseStatsLogRecord"
    where=[
      ["`displayedName`='Lag 5:632.*'",  "`monitoredObjectSiteId`='10.10.100.3'"],
      ["`displayedName`='Lag 5:632.*'",  "`monitoredObjectSiteId`='10.10.100.3'"],
      ["`displayedName`='Lag 2:1078.*'", "`monitoredObjectSiteId`='10.10.100.3'"],
      ["`displayedName`='Lag 2:1078.*'", "`monitoredObjectSiteId`='10.10.100.3'"],
      ["`displayedName`='Lag 199:1.*'",  "`monitoredObjectSiteId`='10.10.100.3'"],
      ["`displayedName`='Lag 199:1.*'",  "`monitoredObjectSiteId`='10.10.100.3'"]]
    group_by=["monitoredObjectSiteId","displayedName" ]
    attributes={
      "egressQChipDroppedOutProfPackets_delta"=>"SUM(egressQChipDroppedOutProfPackets_delta)",
      "ingressQChipForwardedInProfPackets_delta"=>"SUM(ingressQChipForwardedInProfPackets_delta)",
      "egressQChipForwardedInProfPackets_delta"=>"SUM(egressQChipForwardedInProfPackets_delta)",
      "egressQChipDroppedInProfPackets_delta"=>"SUM(egressQChipDroppedInProfPackets_delta)",
      "egressQChipForwardedOutProfPackets_delta"=>"SUM(egressQChipForwardedOutProfPackets_delta)",
      "ingressQChipForwardedOutProfPackets_delta"=>"SUM(ingressQChipForwardedOutProfPackets_delta)"}
    time_name="timeCaptured"
    # 999 msecs offsets are used since these records are never captured on time
    time_periods_msecs = [[1303704000999,1303965000999 ]]
    time_increment_msecs = 1800000
    order_by = ["timeCaptured"]
    timestart = Time.now
    results = query_and_aggregate "sam_archive", class_name, time_name, where, group_by, order_by, attributes, time_periods_msecs, time_increment_msecs
    log_completion_time " test_bruce2", timestart
    puts "test_bruce2"
    pp results
    return results

    #SELECT (1303702200999 + TRUNCATE((timeCaptured)/1800000, 0) * 1800000 + 1800000) as time, `monitoredObjectSiteId`, `displayedName`, SUM(`ingressQChipForwardedInProfPackets_delta`) AS `ingressQChipForwardedInProfPackets_delta`, SUM(`egressQChipDroppedOutProfPackets_delta`) AS `egressQChipDroppedOutProfPackets_delta`, SUM(`egressQChipForwardedInProfPackets_delta`) AS `egressQChipForwardedInProfPackets_delta`, SUM(`egressQChipDroppedInProfPackets_delta`) AS `egressQChipDroppedInProfPackets_delta`, SUM(`ingressQChipForwardedOutProfPackets_delta`) AS `ingressQChipForwardedOutProfPackets_delta`, SUM(`egressQChipForwardedOutProfPackets_delta`) AS `egressQChipForwardedOutProfPackets_delta` FROM `sam_archive`.`service.SapBaseStatsLogRecord_archive`  WHERE ((`timeCaptured` BETWEEN 1303702200999 AND 1303965000999)) AND ((`displayedName`='Lag 5:632.*' AND `monitoredObjectSiteId`='10.10.100.3') OR (`displayedName`='Lag 5:632.*' AND `monitoredObjectSiteId`='10.10.100.3') OR (`displayedName`='Lag 2:1078.*' AND `monitoredObjectSiteId`='10.10.100.3') OR (`displayedName`='Lag 2:1078.*' AND `monitoredObjectSiteId`='10.10.100.3') OR (`displayedName`='Lag 199:1.*' AND `monitoredObjectSiteId`='10.10.100.3') OR (`displayedName`='Lag 199:1.*' AND `monitoredObjectSiteId`='10.10.100.3')) GROUP BY time ,`monitoredObjectSiteId`, `displayedName` ORDER BY `time`;

  end
  #------------------------------------------------------------------------
  def self.test_bruce3 
    class_name = "service.SapBaseStatsLogRecord"
    where=[
      ["`displayedName`='Lag 5:632.*'",  "`monitoredObjectSiteId`='10.10.100.3'"],
      ["`displayedName`='Lag 5:632.*'",  "`monitoredObjectSiteId`='10.10.100.3'"],
      ["`displayedName`='Lag 2:1078.*'", "`monitoredObjectSiteId`='10.10.100.3'"],
      ["`displayedName`='Lag 2:1078.*'", "`monitoredObjectSiteId`='10.10.100.3'"],
      ["`displayedName`='Lag 199:1.*'",  "`monitoredObjectSiteId`='10.10.100.3'"],
      ["`displayedName`='Lag 199:1.*'",  "`monitoredObjectSiteId`='10.10.100.3'"]]
    group_by=[ "monitoredObjectSiteId", "displayedName"]
    attributes={
      "egressQChipDroppedOutProfPackets_delta"=>"SUM(egressQChipDroppedOutProfPackets_delta)",
      "ingressQChipForwardedInProfPackets_delta"=>"SUM(ingressQChipForwardedInProfPackets_delta)",
      "egressQChipForwardedInProfPackets_delta"=>"SUM(egressQChipForwardedInProfPackets_delta)",
      "egressQChipDroppedInProfPackets_delta"=>"SUM(egressQChipDroppedInProfPackets_delta)",
      "egressQChipForwardedOutProfPackets_delta"=>"SUM(egressQChipForwardedOutProfPackets_delta)",
      "ingressQChipForwardedOutProfPackets_delta"=>"SUM(ingressQChipForwardedOutProfPackets_delta)"}
    time_name="timeCaptured"
    # 999 msecs offsets are used since these records are never captured on time
    time_periods_msecs = [[1303704000999, 1303963200999]]
    time_increment_msecs = 1800000
    order_by = ["timeCaptured"]
    timestart = Time.now
    results = query_and_aggregate "sam_archive", class_name, time_name, where, group_by, order_by, attributes, time_periods_msecs, time_increment_msecs
    log_completion_time " test_bruce3", timestart
    puts "test_bruce3"
    pp results
    return results
  end
  #-----------------------------------
  def self.test_brix_ethframedelaytest_aggregate 
    class_name = "com.brixnet.swv.ethframedelaytest"
    where=[
      ["`test_instance_class_id`='3394'",
        "`Result`='SUCCESSFUL'"]  # include only SUCCESSFUL tests
    ]
    group_by=nil #[ "test_instance_class_id"]
    attributes={
      "AverageNetworkRoundTripLatency" => "MAX(AverageNetworkRoundTripLatency)",
      "ValidFramesReceived" => "SUM(ValidFramesReceived)",
      #  "Result" => "???",
      "AverageNetworkRoundTripLatencyVariation" => "MAX(AverageNetworkRoundTripLatencyVariation)",
      "FramesTransmitted" => "SUM(FramesTransmitted)",
    }
    time_name="TimeStamp"
    time_periods_nanosecs = [[1307332801849728000, 1308766036858585000]]
    time_increment_nanosecs = 1800 * 1000000000 # 30 minutes
    order_by = ["TimeStamp"]
    timestart = Time.now
    results = query_and_aggregate "brix_archive", class_name, time_name, where, group_by, order_by, attributes, time_periods_nanosecs, time_increment_nanosecs
    log_completion_time "test_brix_ethframedelaytest_aggregate", timestart
    puts "test_brix_ethframedelaytest_aggregate"
    pp results
    return results
  end
  #-----------------------------------
  def self.test_brix_ethframedelaytest_summary 
    class_name = "com.brixnet.swv.ethframedelaytest"
    where=[
      ["`test_instance_class_id`='3394'",
        "`Result`='SUCCESSFUL'"]  # include only SUCCESSFUL tests
    ]
    group_by=nil #[ "test_instance_class_id"]
    attributes={
      "AverageNetworkRoundTripLatency" => "MAX(AverageNetworkRoundTripLatency)",
      "ValidFramesReceived" => "SUM(ValidFramesReceived)",
      #  "Result" => "???",
      "AverageNetworkRoundTripLatencyVariation" => "MAX(AverageNetworkRoundTripLatencyVariation)",
      "FramesTransmitted" => "SUM(FramesTransmitted)",
    }
    time_name="TimeStamp"
    time_periods_nanosecs = [[1307332801849728000, 1308766036858585000]]
    timestart = Time.now
    results = query_and_summarize "brix_archive", class_name, time_name, where, group_by, attributes, time_periods_nanosecs
    log_completion_time "test_brix_ethframedelaytest_summary", timestart
    puts "test_brix_ethframedelaytest_summary"
    pp results
    return results
  end
  #------------------------------------------------------------------------
  def self.test_brix_pingactivetest_aggregate 
    class_name = "com.brixnet.pingactivetest"
    where=[
      ["`test_instance_class_id`='3030'",
        "`Result`='SUCCESSFUL'"]  # include only SUCCESSFUL tests
    ]
    group_by=nil #[ "test_instance_class_id"]
    attributes={
      "NumberofNoResponses" => "SUM(59 - NumberofNoResponses)",
      "AverageRoundTripLatency" => "MAX(AverageRoundTripLatency)",
      "PercentLostPackets" => "SUM(PercentLostPackets)",
      "AverageJitter" => "MAX(AverageJitter)",
    }
    time_name="TimeStamp"
    time_periods_nanosecs = [[1307332801849728000, 1308766036858585000]]
    time_increment_nanosecs = 1800 * 1000000000 # 30 minutes
    order_by = ["TimeStamp"]
    timestart = Time.now
    results = query_and_aggregate "brix_archive", class_name, time_name, where, group_by, order_by, attributes, time_periods_nanosecs, time_increment_nanosecs
    log_completion_time "test_brix_pingactivetest_aggregate", timestart
    puts "test_brix_pingactivetest_aggregate"
    pp results
    return results
  end
  #------------------------------------------------------------------------
  def self.test_brix_pingactivetest_summary 
    class_name = "com.brixnet.pingactivetest"
    where=[
      ["`test_instance_class_id`='3030'",
        "`Result`='SUCCESSFUL'"]  # include only SUCCESSFUL tests
    ]
    group_by=nil #[ "test_instance_class_id"]
    attributes={
      "NumberofNoResponses" => "SUM(59 - NumberofNoResponses)",
      "AverageRoundTripLatency" => "MAX(AverageRoundTripLatency)",
      "PercentLostPackets" => "SUM(PercentLostPackets)",
      "AverageJitter" => "MAX(AverageJitter)",
    }
    time_name="TimeStamp"
    time_periods_nanosecs = [[1307332801849728000, 1308766036858585000]]
    timestart = Time.now
    results = query_and_summarize "brix_archive", class_name, time_name, where, group_by, attributes, time_periods_nanosecs
    log_completion_time "test_brix_pingactivetest_summary", timestart
    puts "test_brix_pingactivetest_summary"
    pp results
    return results
  end
  #------------------------------------------------------------------------
  def self.test_brix_pingactivetest_availability 
    class_name = "com.brixnet.pingactivetest"
    where=[
      ["`test_instance_class_id`='3030'",
        "`Result`='SUCCESSFUL'"]  # include only SUCCESSFUL tests
    ]
    group_by=nil
    attributes= nil
    time_name="TimeStamp"
    time_periods_nanosecs = [[1307332801849728000, 1308766036858585000]]
    order_by = ["TimeStamp"]
    timestart = Time.now
    results = query_dont_aggregate "brix_archive", class_name, time_name, where, group_by, order_by, attributes, time_periods_nanosecs
    log_completion_time "test_brix_pingactivetest_availability", timestart
    puts "test_brix_pingactivetest_availability"
    pp results
    return results
  end
  #------------------------------------------------------------------------

  def self.test_summary2
    class_name = "service.SapBaseStatsLogRecord"
    time_name = "timeCaptured"
    where1 = [ #OR
      [   "`monitoredObjectSiteId`='10.10.100.15'" , "`displayedName`='Port 1/2/14:100.0'", ],
      [   "`monitoredObjectSiteId`='10.10.100.15'" , "`displayedName`='Port 1/2/13:1.0'", ],
      [   "`monitoredObjectSiteId`='10.10.100.15'" , "`displayedName`='Lag 199:2.0'", ],
    ]
    group_by = ["monitoredObjectSiteId" , "displayedName"]
    attributes2 = {
      "egressQChipDroppedInProfPackets_delta"    => "SUM(egressQChipDroppedInProfPackets_delta)",
      "egressQChipDroppedOutProfPackets_delta"   => "SUM(egressQChipDroppedOutProfPackets_delta)",
      "ingressQChipDroppedHiPrioPackets_delta"   => "SUM(ingressQChipDroppedHiPrioPackets_delta)",
      "ingressQChipDroppedLoPrioPackets_delta"   => "SUM(ingressQChipDroppedLoPrioPackets_delta)",
      "ingressQChipForwardedInProfPackets_delta" => "SUM(ingressQChipForwardedInProfPackets_delta)",
      "ingressQChipForwardedOutProfPackets_delta"=> "SUM(ingressQChipForwardedOutProfPackets_delta)",
    }
    time_periods2 = [
      [1315281300000 , 1315324444000],
      [1315323844000 , 1315324556000]
    ]
    timestart = Time.now
    results= DataArchiveIf.query_and_summarize "sam_archive", class_name,time_name, where1, group_by, attributes2, time_periods2
    puts "test_summary2"
    pp results
    return results
  end
  #------------------------------------------------------------------------
  def self.test 
    test_aggregation
    test_aggregation_fdv 
    test_summary 
    test_no_aggregation 
    test_bruce 
    test_bruce2 
    test_bruce3 
    test_brix_ethframedelaytest_aggregate 
    test_brix_ethframedelaytest_summary 
    test_brix_pingactivetest_aggregate 
    test_brix_pingactivetest_summary 
    test_brix_pingactivetest_availability 
  end

  def self.test_diff_apis
   sharded=false
   DataArchiveIf.simplified_query_and_aggregate_test sharded
   DataArchiveIf.differential_query_and_aggregate_test sharded
   DataArchiveIf.simplified_query_and_summarize_test sharded
   DataArchiveIf.differential_query_and_summarize_test sharded
   DataArchiveIf.simplified_query_aggregate_and_count_test sharded
   DataArchiveIf.differential_query_aggregate_and_count_test sharded
  end
end


