require 'socket'      # Sockets are in standard library
require File.dirname(__FILE__) + "/sam_if_common"
include SamIfCommon

  if ARGV.size < 2
    puts "Usage ruby server_debug.rb <host> <port> [<command>]"
    return
  end

  server = ARGV[0]
  port = ARGV[1]
  command = ""
  ARGV[2..-1].each {|cmd| command += cmd + " "}
  socket = TCPSocket.new(server, port)
  request = Marshal.dump(SamIfMsg.new(3,command.chomp(" "), {}, {}))
  serialized_size = sprintf "%08d", request.size
  socket.write serialized_size
  socket.write(request)
  reply = socket.recv(5000)
  puts reply
  socket.close
