require "net/telnet"
require "rubygems"
require "active_record"
#require "mysql"
#require "jdbc-mysql"
#require "jdbc/mysql"

if RUBY_PLATFORM =~ /java/ #manual change req'd in database.yml:   adapter: <jdbc>mysql
  puts"...Java..."
  require "jdbc/mysql" 
  
  # Pulling java in....
  #include Java
  require 'java'
  Dir["/Users/Dave/work/Projects/ServiceMonitoring/Qosera/OSS/trunk/lib/*.jar"].each { |jar| require jar }
  Dir["/Users/Dave/work/Projects/ServiceMonitoring/Qosera/OSS/trunk/dist/*.jar"].each { |jar| require jar }
else
  puts"...No Java..."
  #require "mysql"  
end

#Establish connection to database
require File.dirname(__FILE__) + "/../../app/models/node"
if RUBY_PLATFORM =~ /java/ 
  dbconfig = YAML::load(File.open(File.dirname(__FILE__) + '/../../config/databasej.yml'))
else
  dbconfig = YAML::load(File.open(File.dirname(__FILE__) + '/../../config/database.yml'))
end
ActiveRecord::Base.establish_connection(dbconfig['development'])

target = ARGV[0]
unless target
  puts"No target specified"
  exit(1)
end

target.chomp
puts"Lookup: #{target}"
res = Node.find_by_short_name("#{target}")
unless res
  puts"Failed to find target with ID = #{target}."
  exit(1)
end

puts "Connecting to #{target} at #{res[:primary_ip]}..."
puts "enter something:"
command = "show debug"

puts "Testing telnet"
t = Net::Telnet::new(
  "Host" => "#{res[:primary_ip]}",
  "Timeout" => 10,
  "Prompt" => /[$%#>] \z/n
  )
  
t.login("#{res[:uname]}", "#{res[:password]}") { |c| print c }
t.cmd("#{command}") { |c| print c }
t.cmd("exit") { |c| print c }
t.close

=begin
puts"Connect to cNode Database..."

java_import('com.qosera.CPortAPI.CPortJAPI')
#or
#include_class Java::ComQoseraCPortAPI::CPortJAPI

java_import('com.qosera.CPortAPI.CUserID')

api = CPortJAPI.new
errMsg = api.open(
  "172.16.24.104",
  "orcl", 
  "qosera", 
  "Q05C3nX")

if errMsg
  puts"api.open: #{errMsg}"
  exit(1)
end  

puts"...OK"
puts"Authenticate user su..."
userId = api.authenticate_user( "su", "su" )
unless userId
  puts"User authenticate failed"
  exit(1)
end

puts"...OK"
puts"Close connection..."
errMsg = api.CloseUser( userId );
if errMsg
  puts"api.CloseUser: #{errMsg}"
  exit(1)
end

puts"...OK"
puts"Close connection again..."

errMsg = api.close_user( userId );
if errMsg
  puts"api.close_user: #{errMsg}"
  exit(1)
end

puts"...OK"
=end

#java_import('java.lang.String') { |pkg, name| "Java" + name }
#s = JavaString.new("Dave")
#puts"String: #{s}"
s1= java.lang.String.new("Dave1")
puts"String1: #{s1}"

h = java.util.HashMap.new
h["Hello"]="World"
puts"#{h}"

# Java GUI test
#java_import('javax.swing.*')
puts"Swing lib GUI Test ...."
f = javax.swing.JFrame.new("Test")
#f.set_size 400, 400
f.setLayout(java.awt.FlowLayout.new);
f.add(javax.swing.JLabel.new("Hello world!"))
b = javax.swing.JButton.new("Press me!")
f.add b
b.add_action_listener do |a|
  puts"Button 1 Pressed - Hello"
end
f.add(javax.swing.JLabel.new("Goodbye World!"))
b = javax.swing.JButton.new("Press me!")
f.add b
b.add_action_listener do |a|
  puts"Button 2 Pressed - Goodbye"
  f.setVisible(false)
end
f.pack
#f.show
f.setVisible(true)

# Exec shell copmmands
puts"Try some shell commands..."
directorylist = %x[find . -name '*test.rb' | sort]
directorylist.each do |filename|
  puts"#{filename}"
  filename.chomp!
  puts"#{filename}"
  # work with file
end

exec "ls"












