require 'rubygems'
require 'soap/element'
require 'soap/rpc/driver'
require 'soap/processor'
require 'soap/streamHandler'
require 'soap/property'
gem 'soap'
require 'soap/hash'

#command_string = 'fm.FaultManager.findFaults'
#
SAM_URL = "http://10.1.8.135:8080/xmlapi/invoke"
#xml_header = SOAP::Hash.new(
#  :header => {
#  :security => {
#    "password" => "3a29165910db9b1041cbf58aa47beb61",
#    "user" => "dschubert"
#  },
#  :requestID => "client1:0"},
#  :attributes! => { :header => { :xmlns => "xmlapi_1.0" } }
#)
#
#xml_body = SOAP::Hash.new(
#  "fm.FaultManager.findFaults" => {
#    :faultFilter => nil
#  },
#  :attributes! => { "fm.FaultManager.findFaults" => { :xmlns => "xmlapi_1.0" } }
#)
#
##xml_body = SOAP::Hash.new(
##  :faultFilter => nil
##)
#
#
##puts xml_header.to_xml
##puts xml_body.to_xml
#
#
stream = SOAP::HTTPStreamHandler.new(SOAP::Property.new)
#
#header_item = SOAP::SOAPElement.new( xml_header.to_xml, nil)
#header = SOAP::SOAPHeader.new()
#header.add nil, header_item
#
#body_item = SOAP::SOAPElement.new(xml_body.to_xml, nil)
#body = SOAP::SOAPBody.new(body_item)
#
#envelope = SOAP::SOAPEnvelope.new(header, body)
#request_string = SOAP::Processor.marshal(envelope)

request_string = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<SOAP:Envelope
  xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">

    <SOAP:Header>
        <header xmlns=\"xmlapi_1.0\">
            <security>
                <user>dschubert</user>
                <password>3a29165910db9b1041cbf58aa47beb61</password>
            </security>
            <requestID>client1:0</requestID>
        </header>
    </SOAP:Header>

    <SOAP:Body>
        <fm.FaultManager.findFaults xmlns=\"xmlapi_1.0\">
            <faultFilter/>
        </fm.FaultManager.findFaults>
    </SOAP:Body>

</SOAP:Envelope>"
puts "SENDING:"
puts request_string
puts "\n"
request = SOAP::StreamHandler::ConnectionData.new(request_string)

resp_data = stream.send(SAM_URL, request)#, 'getResponse')

puts "Got response:"
puts resp_data.receive_string

