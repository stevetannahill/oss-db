require 'socket'      # Sockets are in standard library
require File.dirname(__FILE__) + "/sam_if_common"

module SamIf

class StatsApi

  def initialize
    puts "Connecting to server at: #{SamIfCommon::SAM_IF_SERVER_IP}:#{SamIfCommon::SAM_IF_SERVER_PORT}"
    @server = TCPSocket.open(SamIfCommon::SAM_IF_SERVER_IP, SamIfCommon::SAM_IF_SERVER_PORT)
  end

  def get_service_stats service_id, class_name, start_time, end_time, attribute_name

    filters = {
      'service_id' => service_id,
      'class_name' => class_name,
      'start_time' => start_time,
      'end_time' => end_time
      }

    result_filters = [
      attribute_name,
      'timeRecorded'
    ]

    msg = SamIfCommon::SamIfMsg.new('get_service_stats', filters, result_filters)

    raw_data = Marshal.dump(msg)
    puts "Sending msg: #{raw_data.size}"
    @server.write raw_data

    s =  @server.recvfrom( 8 )[0].chomp
    puts "Reply Msg Size: #{s}"

    result_str = ""
    reply = ""
    puts "------------------------------------"
    while reply.size < s.to_i
      puts "Before Receive data: #{reply.size}"
      reply += @server.recvfrom( 200000 )[0].chomp
      puts "After Receive data: #{reply.size}"
    end
    puts "------------------------------------"

    #reply = @server.gets
    puts "Got Reply w size: #{reply.size}"
    data = Marshal.load(reply)

    puts "Number of Records: #{data.size}"
    data.sort! { |a,b| a['timeRecorded'] <=> b['timeRecorded'] }
#
#    data.each do |record|
#      result_str += "------------------------------------\n"
#        result_str += "#{record['timeRecorded']} = #{record['outOfProfileOctetsForwarded']}\n"
##      record.each { |key, value|
##        result_str += "#{key} = #{value}\n"
##      }
#      result_str += "------------------------------------\n"
#    end

    result_str += "Encoded size: #{s}\n"
    result_str += "Reply size: #{reply.size}\n"
    result_str += "Records found: #{data.size}\n"

    return result_str, data
  end

  def send_command command
    msg_data = {}
    msg = SamIfCommon::SamIfMsg.new(command, msg_data)
    @server.write Marshal.dump(msg)
    reply = @server.recvfrom( 200000 )[0].chomp
    return reply
  end


end

end

