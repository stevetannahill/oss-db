require 'socket'      # Sockets are in standard library
require "sam_if_common"

module SamIf

class StatsApi

  def initialize(nms)
    @server = nil
    @nms_id = nil
    
    if nms != nil
      @nms_id = nms == nil ? nil : nms.id   
      Rails.logger.info "Connecting to server at: #{SamIfCommon::SAM_IF_SERVER_IP}:#{SamIfCommon::SAM_IF_SERVER_PORT}"
      begin
        @server = TCPSocket.open(SamIfCommon::SAM_IF_SERVER_IP, SamIfCommon::SAM_IF_SERVER_PORT)
      rescue Errno::EHOSTUNREACH, Errno::ENETUNREACH, Errno::ENOTCONN, Errno::EBADF, Errno::ECONNREFUSED, Errno::ECONNRESET
        SW_ERR "Exception trying to open socket #{SamIfCommon::SAM_IF_SERVER_PORT} on Address #{SamIfCommon::SAM_IF_SERVER_IP}. Exception: #{$!}"
        @server = nil
      end
    else
      SW_ERR "NMS is nil"
    end
  end
  
  def get_mib_stats service_id, class_name, collect_times, attribute_names, sap

    filters = {
      'service_id' => service_id,
      'class_name' => class_name,
      'collect_times' => collect_times,
      'sap' => sap
    }
    result_filters = attribute_names + ['timeCaptured']

    msg = SamIfCommon::SamIfMsg.new(@nms_id, 'get_mib_stats', filters, result_filters)
    
    result, data = send_request(msg, 'timeCaptured')
    
    return result, data
  end


  def get_delay_stats query_filters, class_name, collect_times, attribute_names, limit_attribute, limit_value

    filters = {
      'query_filters' => query_filters,
      'class_name' => class_name,
      'collect_times' => collect_times,
      'limit_attribute' => limit_attribute,
      'limit_value' => limit_value
    }
    result_filters = attribute_names + ['timeCaptured']

    msg = SamIfCommon::SamIfMsg.new(@nms_id, 'get_delay_stats', filters, result_filters)
    
    result, data = send_request(msg, 'timeCaptured')

    return result, data
  end

  def get_service_stats query_filters, collect_times, attribute_names

    filters = {
      'query_filters' => query_filters,
      'collect_times' => collect_times
    }    

    result_filters = attribute_names + ['timeRecorded']

    msg = SamIfCommon::SamIfMsg.new(@nms_id, 'get_service_stats', filters, result_filters)

    result, data = send_request(msg, 'timeRecorded')
    
    return result, data
  end
  
  def get_alarm alarm_object, attribute_names

    filters = {
      'alarm_object' => alarm_object
    }
    
    result_filters = attribute_names

    msg = SamIfCommon::SamIfMsg.new(@nms_id, 'get_alarm', filters, result_filters)
    
    result, data = send_request(msg, nil)
    
    return result, data
  end
  
  def get_inventory class_name, query_filters, attribute_names

    filters = {
      'class_name' => class_name,
      'query_filters' => query_filters
    }
    
    result_filters = attribute_names

    msg = SamIfCommon::SamIfMsg.new(@nms_id, 'get_inventory', filters, result_filters)
    
    result, data = send_request(msg, nil)
    
    return result, data
  end
 
  def send_request(msg, sort_tag)
    if @server == nil
      return false, []
    end
    begin
      result = true
      data = []
      raw_data = Marshal.dump(msg)
      Rails.logger.info "Sending msg: #{raw_data.size} #{Time.now}"
      
      serialized_size = sprintf "%08d", raw_data.size
      @server.write serialized_size
      @server.write raw_data

      msg_size =  @server.recv( 8 )
      Rails.logger.info "Reply Msg Size: #{msg_size}"

      reply = ""
      while reply.size < msg_size.to_i
        chunk = @server.recv( 200000 )
        break if chunk.size == 0 # The peer has closed it's socket
        reply += chunk
      end
    
      Rails.logger.info "Got Reply w size: #{reply.size} #{Time.now}"
      data = Marshal.load(reply)

      Rails.logger.info "Number of Records: #{data.size}"
      data.sort! { |a,b| a[sort_tag] <=> b[sort_tag] } if sort_tag != nil

      puts "Encoded size: #{msg_size}"
      puts "Reply size: #{reply.size}"
      puts "Records found: #{data.size}"   
    rescue
      SW_ERR "Exception in sending request to SAM #{$!}"
      result = false
      data = []
    end     
    
    return result, data
  end
        

  def send_command command
    filters = {}
    result_filters = []

    msg = SamIfCommon::SamIfMsg.new(command, filters, result_filters)
    @server.write Marshal.dump(msg)
    reply = @server.recv( 200000 )
    return reply
  end

  def close
    @server.close if @server != nil
  end

end

end

