require 'java'
require 'samOss.jar'
require 'sam_if_common'

class SAMJmsListener
  include javax.jms.MessageListener
  include javax.jms.ExceptionListener
  attr_reader :client_id
  attr_reader :nms_name
  attr_reader :nms
  
  def initialize network_manager, name, user, passwd, client_id, filter, topic_name, no_rx_msg_timeout = (2 * 60)
    @name = name
    @user = user
    @passwd = passwd
    @client_id = client_id
    @filter = filter
    @topic_name = topic_name
    @callbacks = []
    @no_msg_rx_timeout = no_rx_msg_timeout
    @nms = network_manager
    @nms_name = @nms.name.gsub(/\s/,'_')
    @active_nms = nms.network_management_servers.first
    @is_connected = false
    @reconnecting = false
    @mutex = Mutex.new
    reload_model
    reset_variables
  end
  
  # Need to reload the nms data from the database everytime because of caching.
  # The nms data may have changed so need to check if it still exists and
  # the active IP is still associated with the nms
  # if the active_nms no longer exists in the model reconnect with the new IP
  # if requested
  def reload_model(reconnect_on_ip_change = false)
    result = true
    begin
      @nms.reload
      if @nms.network_management_servers.find_by_primary_ip(@active_nms) == nil
        reason = "IP address (#{@nms.name}-#{@active_nms}) changed reconnecting"
         @active_nms = nil
         if @nms.network_management_servers.first != nil
          @active_nms = @nms.network_management_servers.first.primary_ip
        end
        begin
          reconnect(reason) if reconnect_on_ip_change
        rescue
          err = "Exception #{$!} #{$!.backtrace.join("\n")}"
          Rails.logger.error err; SW_ERR err
        end
      end
    rescue ActiveRecord::RecordNotFound
      # The nms object no longer exists! 
      result = false
    end
    return result
  end
  

  def add_callback callback
    @callbacks << callback
  end
  
  def remove_callback callback
    @callbacks.delete(callback)
  end
  
  def connection_sane
    # Used to verify if the connection is still receiving messages provided or it's reconnecting
    return (@last_msg_received + @no_msg_rx_timeout > Time.now) || @reconnecting
  end
  
  
  def open_connection
    begin
      return if !reload_model
      return if @active_nms == nil
      attempts = 1
      # nms_name to use in logs if it's not already set in the Thread variable
      alt_nms_name = Thread.current["nms_name"] ? "" : @nms_name
       
      begin
      @env = java.util.Hashtable.new
      @env.put javax.naming.Context.INITIAL_CONTEXT_FACTORY,"org.jnp.interfaces.NamingContextFactory"
      @env.put javax.naming.Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces"
      @env.put "jnp.disableDiscovery", "true"
      @env.put "jnp.timeout", "60000"
      jms_server = "jnp://#{@active_nms}:1099/"
      @env.put javax.naming.Context.PROVIDER_URL, jms_server
      @jndiContext = javax.naming.InitialContext.new @env
    
      @initial_context = @jndiContext.lookup "external/5620SamJMSServer"
      @topic_connection_factory = @initial_context.lookup "SAMConnectionFactory"
      rescue javax.naming.NamingException => e
        if attempts < @nms.network_management_servers.size
          attempts += 1
          err = "#{alt_nms_name} JNDI API lookup failed: #{e} for #{jms_server} #{e.backtrace.join("\n")}" 
          Rails.logger.error err; SW_ERR err         
          server = @nms.network_management_servers.find_by_primary_ip(@active_nms)
          server = @nms.network_management_servers[@nms.network_management_servers.index(server)+1]
          server = @nms.network_management_servers.first if server == nil
          if server == nil
            @active_nms = nil
            raise # No servers
          else
            @active_nms = server.primary_ip
          end
          java.lang.Thread.sleep 1000
          retry
        else
          raise
        end
      end

      Rails.logger.info "#{alt_nms_name} Connection created for user: #{@user}:#{@passwd}"
      @topic_connection = @topic_connection_factory.createTopicConnection @user, @passwd

      @topic_connection.setClientID @client_id
      @topic_session = @topic_connection.createTopicSession false, javax.jms.TopicSession::AUTO_ACKNOWLEDGE
      Rails.logger.info "#{alt_nms_name} Topic session created...#{@client_id}"

      begin
        topic = @initial_context.lookup @topic_name
      rescue javax.naming.NamingException => e
        err = "#{alt_nms_name} JNDI Topic lookup failed: #{e} #{e.backtrace.join("\n")}"
        Rails.logger.error err; SW_ERR err
      end

      Rails.logger.info "#{alt_nms_name} Finished initializing topic: #{@topic_name} #{@filter}"
      
      @topic_subscriber = @topic_session.createDurableSubscriber topic, @client_id, @filter, false

      @accepted_client_id = @topic_connection.getClientID
      Rails.logger.info "#{alt_nms_name} Client id: #{@accepted_client_id}"

      @topic_connection.setExceptionListener self

      begin
        @topic_subscriber.setMessageListener self
      rescue java.lang.Exception
        err = "#{alt_nms_name} Exception setting message listener: #{$!} #{$!.backtrace.join("\n")}"
        Rails.logger.error err; SW_ERR err
      end

      @topic_connection.start
      Rails.logger.info "#{alt_nms_name} Connected and listening..."

      @is_connected = true
      @last_msg_received = Time.now
      SystemAlarmHistory.process_event({:event_filter => "#{@nms.name}-#{@nms.nm_type}", :state => "cleared", :timestamp => Time.now.to_f*1000, :details => ""})
    rescue java.lang.Throwable
      err = "#{alt_nms_name} initialize Exception: #{$!} #{$!.backtrace.join("\n")}"
      Rails.logger.error err; SW_ERR err
      @is_connected = false
      SystemAlarmHistory.process_event({:event_filter => "#{@nms.name}-#{@nms.nm_type}", :state => "failed", :timestamp => Time.now.to_f*1000, :details => "#{$!}"})
      raise(RuntimeError, $!)
    end
  end

  def close_connection(delete_durable_topic = false)
    # nms_name to use in logs if it's not already set in the Thread variable
    alt_nms_name = Thread.current["nms_name"] ? "" : @nms_name
    
    @is_connected = false

    @topic_connection.setExceptionListener(nil) if @topic_connection != nil
    @topic_subscriber.setMessageListener(nil) if @topic_subscriber != nil

    Rails.logger.info "#{alt_nms_name} Stop topic Connection..."
    begin
      @topic_connection.stop if @topic_connection != nil
    rescue
      err = "#{alt_nms_name} Exception stopping topic #{$!} #{$!.backtrace.join("\n")}"
      Rails.logger.error err; SW_ERR err
    end

    Rails.logger.info "#{alt_nms_name} Close topic Subscriber..."
    begin
      @topic_subscriber.close if @topic_subscriber != nil
    rescue
      err = "#{alt_nms_name} Exception closing topic: #{$!} #{$!.backtrace.join("\n")}"
      Rails.logger.error err; SW_ERR err
    end
  
    Rails.logger.info "#{alt_nms_name} Close topic Session..."
    begin
      if @topic_session != nil
        @topic_session.unsubscribe(@client_id) if delete_durable_topic
        @topic_session.close()
      end
    rescue
      err = "#{alt_nms_name} Exception closing session #{$!} #{$!.backtrace.join("\n")}"
      Rails.logger.error err; SW_ERR err
    end

    Rails.logger.info "#{alt_nms_name} Close topic Connection..."
    begin
      @topic_connection.close() if @topic_connection != nil
    rescue
      err = "#{alt_nms_name} Exception closing connection #{$!} #{$!.backtrace.join("\n")}"
      Rails.logger.error err; SW_ERR err
    end
    
    reset_variables
  end

  def onMessage message    
    # Set a Thread variable to the NMS name so it can be included in the logs
    # This is done here as it's not possible to get the Thread when openning the connection
    Thread.current["nms_name"] = @nms_name
    
    @message_count += 1
    @last_msg_received = Time.now
    message_text =  message.getObject.getText  
    #Rails.logger.info "Message...\n#{message_text}"
    # call any registered clients  
    begin
      @callbacks.each {|cb| cb.on_message(message_text, self)}
    rescue
      err = "Execption #{$!} #{$!.backtrace.join("\n")}"
      Rails.logger.error err; SW_ERR err
    end
    Rails.logger.info "#{@name} Got message: #{@message_count} Processing time: #{Time.now.to_f - @last_msg_received.to_f}"
      
  end

  def onException exception
    # Set a Thread variable to the NMS name so it can be included in the logs
    # This is done here as it's not possible to get the Thread when openning the connection
  
    Thread.current["nms_name"] = @nms_name
    
    @exception_count += 1
    Rails.logger.info "#{@name} Got exception #{@exception_count}"
    Rails.logger.info "Message..."
    Rails.logger.info "#{exception.getMessage}"

    begin
      @topic_connection.setExceptionListener nil
    rescue
      #Ignore this exception, the TCP connection may already be closed.
    end

    reconnect(exception.getMessage)
  end

  def resync
    @callbacks.each {|cb| cb.resync(self)}
  end
  
  def test_reconnect
      reconnect("Testing reconnect")
  end

  def reconnect(reason = "reconnect unknown reason")
    SystemAlarmHistory.process_event({:event_filter => "#{@nms.name}-#{@nms.nm_type}", :state => "failed", :timestamp => Time.now.to_f*1000, :details => reason})
    
    @mutex.synchronize {
      @reconnecting = true
      close_connection
      while !@is_connected
        Rails.logger.info "Reconnecting"
        begin
          @reconnection_count += 1
          open_connection
          break
        rescue 
          Rails.logger.info "Reconnection Attempt #: #{@reconnection_count} Failed - Exception: #{$!} #{$!.backtrace.join("\n")}"
          close_connection
        end

        begin
          java.lang.Thread.sleep 5000
        rescue java.lang.Exception => e
          # This exception should not happen unless the process
          # is killed at this point, in which case it is ignored.
        end
      end
      @reconnecting = false
    }
  end
  
  def debug_dump
    reply = "SAMJmsListener Debug Stats\n"
    reply += "--------------------------\n"
    reply += "Name: #{@name}\n"
    reply += "Is Connected: #{@is_connected}\n"
    reply += "Reconnecting: #{@reconnecting}\n"
    reply += "Message Count: #{@message_count}\n"
    reply += "Exception Count: #{@exception_count}\n"
    reply += "Reconnection Count: #{@reconnection_count}\n"
    reply += "Client ID: #{@accepted_client_id}\n"
    reply += "Topic #{@topic_name}\n"
    reply += "Filter: #{@filter}\n"
    reply += "Last message received #{@last_msg_received}\n"
    reply += "JMS server #{@active_nms}\n"
    reply += "Callbacks: #{@callbacks.inspect}\n"
    return reply
  end

  private

  def reset_variables
    @is_connected = false
    @message_count = 0
    @exception_count = 0
    @reconnection_count = 0
    @env = nil
    @jndiContext = nil
    @initial_context = nil
    @topic_connection_factory = nil
    @topic_connection = nil
    @topic_session = nil
    @topic_subscriber = nil
    @accepted_client_id = nil
    @last_msg_received = Time.now
  end

end
