
require "sam_soap_interface"
require 'logger'

logger = Logger.new(STDOUT)
Rails.logger = logger # Give logger access to all ruby objects
Rails.logger.level = Logger::Severity::DEBUG

def send_request data
  Rails.logger.debug "SENDING:"
  Rails.logger.debug data
  attempts = 1
  current_nms = "10.1.8.136"

  # No server can be found exit
  raise if current_nms == nil
  
  begin
    stream = SOAP::HTTPStreamHandler.new(SOAP::Property.new)
    stream.client.connect_timeout=10
    stream.client.send_timeout=20
    stream.client.receive_timeout=20 #10 # For each 16K block

    request = SOAP::StreamHandler::ConnectionData.new(data)
    url = "http://#{current_nms}:8080/xmlapi/invoke"
    time_at_send = Time.now
    resp_data = stream.send(url, request)
  rescue Exception => e
    if attempts >= @nms.network_management_servers.size * 2
      raise # Tried all servers twice - fail
    else 
      # Try the other server 
      Rails.logger.info "Failed to connect #{current_nms}"
    end
  end
  Rails.logger.info "Got response: Duration = #{Time.now - time_at_send}"
  #Rails.logger.debug resp_data.receive_string
  Rails.logger.info "Response size: #{resp_data.receive_string.size}"
  
  return resp_data.receive_string
end


file_to_send = ARGV[0]
unless file_to_send
  puts"No file specified"
  exit(1)
end

file = File.new(file_to_send, 'r')

request_string = ''
file.each_line do |line|
  request_string += line
end


resp_data = send_request request_string
puts "Got response:"
puts resp_data
puts "Size: #{resp_data.size}"



