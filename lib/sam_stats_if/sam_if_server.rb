#gem 'libxml-ruby', '>= 0.8.3'
#require 'xml'

# set up logging
MAX_LOG_SIZE = 10*1024*1024
class FormatLogger < Logger
  def format_message(severity, timestamp, progname, msg)
    "#{timestamp.to_s} #{severity} #{Thread.current} #{Thread.current["nms_name"]} #{msg}\n"
  end
end


def print_menu
  str = "Debug menu for #{File.basename(__FILE__, '.*')}\n"
  str += "b) Close client session\n"
  str += "d) Thread information\n"
  str += "l [level]) Dump Logger level or set it to level\n" 
  str += "q) Quit Server\n"
  return str
end

def debug_handler(command, client, sam_soap_interfaces)
  stop_server = false
  stop_debug = false
  reply = ""
  
  split_cmd = command.split(" ")
  case split_cmd[0]
  when "q"
    Rails.logger.info "Shutting down server. Bye!"
    client.puts "Closing the connection. Bye!"
    stop_debug = true
    stop_server = true
  when "b"
    Rails.logger.info "Closing client session. Bye!"
    reply = "Closing client session. Bye!"
    stop_debug = true
  when "d"
    Rails.logger.info "Thread Info"
    reply = "Threads = #{Thread.list.inspect} Count #{Thread.list.size}\nSam If:#{sam_soap_interfaces.inspect}"
  when "l"
    Rails.logger.info "Logging level command"
    if split_cmd.size == 1
      reply = ["DEBUG", "INFO", "WARN", "ERROR", "FATAL", "UNKNOWN"][Rails.logger.level]
    else
      if Logger::Severity.constants.include?(split_cmd[1].upcase)
        Rails.logger.level = Logger::Severity.const_get(split_cmd[1].upcase)
        reply = "Logger level set to #{split_cmd[1].upcase}"
      else
        reply = "#{split_cmd[1]} is an invalid log level"
      end
    end
  else
    reply = print_menu
  end
  
  if !reply.empty?
    begin
      client.puts reply
    rescue Exception => e #Errno::EIO, Errno::ECONNRESET, Errno::ENOTCONN
      SW_ERR "Socket write failure...#{e.message}"
    end   
  end  
  client.close if stop_debug

  return stop_server, stop_debug
end

def respond_to_client(client, data)
  serialized_data = ""
  serialized_data = Marshal.dump(data)
  serialized_size = sprintf "%08d", serialized_data.size
  Rails.logger.info "Reply size: #{serialized_size}"
  client.write serialized_size
  client.write serialized_data
end

# Main begins
logger = FormatLogger.new("#{Rails.root}/log/#{File.basename(__FILE__, '.*')}.log", 1, MAX_LOG_SIZE)
Rails.logger = logger
Rails.logger.level = (Rails.env == "development") ? Logger::Severity::DEBUG : Logger::Severity::INFO

# Don't log SQL queries
ActiveRecord::Base.logger = nil  
ActiveRecord::Base.clear_active_connections!

@thread_count = 0
server = TCPServer.open(SamIfCommon::SAM_IF_SERVER_PORT)
Rails.logger.info "Listening for TCP clients on port #{SamIfCommon::SAM_IF_SERVER_PORT}"

# The sam_soap_interfaces is added to when a new request comes in with an unknown nms id
# This is done in different threads so make sure the creation is protected
sam_soap_mutex = Mutex.new
sam_soap_interfaces = Hash.new {|hash,key|
  sam_soap_mutex.synchronize {
    begin
      if NetworkManager.find(key).nm_type == "5620Sam"
        hash[key] = SamSoapInterface.new(key)
      else
        SW_ERR "NetworkManager with id #{key} id is not a SAM NMS it is a #{NetworkManager.find(key).nm_type}"
      end
    rescue ActiveRecord::RecordNotFound
      SW_ERR "Cannot find NMS with id #{key}"
    end
  }
}

keep_going = true
while (keep_going)
  Rails.logger.info "Create new thread..."
  @thread_count += 1
  client_socket = server.accept
  
  # For each message ensure the connection to the database is up
  ActiveRecord::Base.verify_active_connections!  
  
  # Remove any network managers that no longer exist 
  sam_soap_mutex.synchronize {
    all_nms = NetworkManager.find_all_by_nm_type("5620Sam").collect{|nms| nms.id} 
    sam_soap_interfaces.delete_if {|key, value| !all_nms.include?(key)}
  }
  
  t = Thread.start(client_socket) do |client|    
  loop {
    begin
      Rails.logger.info "Thread: Waiting for client msg..."
  
      s = client.recvfrom(8)[0]
      break if s.empty?
      
      result_str = ""
      raw_data = ""
      while raw_data.size < s.to_i
        raw_data += client.recvfrom( 8000 )[0]
      end
    
      msg = Marshal.load(raw_data)
      
      # Find (and possiblly create - if it's not been seen before) the sam soap interface based on
      # the nms id. If it's not found exit the thread
      soap_if = sam_soap_interfaces[msg.get_nms_id]
      break if soap_if == nil
      
      if !soap_if.reload_model
        SW_ERR "Failed to reload the model for #{soap_if.nms} it's been deleted"
         sam_soap_mutex.synchronize { sam_soap_interfaces.delete(soap_if) }
        break
      end
      
      # Set a Thread variable to the NMS name so it can be included in the logs
      Thread.current["nms_name"] = soap_if.nms.name.gsub(/\s/,'_')
      
      command = msg.get_command
      Rails.logger.info "Got command: #{command}"
      
      case command.split(" ")[0]
      when "q", "b", "d", "l"        
        stop_server, stop_debug = debug_handler(command, client, sam_soap_interfaces)
        if stop_server
          server.close
          keep_going = false
        end
        break if stop_debug
      when "get_service_stats"
        result, data = soap_if.get_service_stats msg
        respond_to_client(client, data)
      when "get_delay_stats"
        result, data = soap_if.get_delay_stats msg
        respond_to_client(client, data)
      when "get_mib_stats"
        result, data = soap_if.get_mib_stats msg
        respond_to_client(client, data)
      when "get_alarm"
        result, data = soap_if.get_alarm msg
        respond_to_client(client, data)
      when "get_inventory"
        result, data = soap_if.get_inventory msg
        respond_to_client(client, data)
      else
        client.puts print_menu
      end
    rescue Exception => e
      Rails.logger.info "Exception in thread #{Thread.current} #{e.message} #{e.backtrace.join("\n")}"
      client.close
      raise # re-raise which will exit the thread
    end
  }
  # At the end of the thread always close the socket
  client.close
  end
end
server.close
RAILS_DEAFULT_LOGGER.info "Server Shutting down port #{SamIfCommon::SAM_IF_SERVER_PORT}"

