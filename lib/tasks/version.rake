require "pathname"

dry_run = false

desc "Display The Current Version"
task :version => "version:current"


namespace :version do

  desc "Update to the next version"
  task :next => "version:next:revision"

  desc "Update to the previous version"
  task :previous => "version:previous:revision"
  
  desc "Deploying the next version"
  task :deploy => "version:deploy:revision"
  
  task :current  do
    print "#{version_to_s}\n"
  end

  namespace :show do

    desc "Display the next version"
    task :next => "version:show:next:revision"

    desc "Display the previous version"
    task :previous => "version:show:previous:revision"

    [:next,:previous].each do |increment|
      namespace increment do
        [:major,:minor,:revision].each do |which|
          desc "Display the #{increment} #{which} version"
          task which do
            print "#{version_to_s(which,increment)}\n"
          end
        end
      end
    end
  end
  
  [:next,:previous].each do |operation|
    namespace operation do
      [:major,:minor,:revision].each do |which|
        desc "Update to the #{operation} #{which} version"
        task which do
          update_version(which,operation)
        end
      end
    end
  end
  
  desc "Commit the version changes to the repo"
  task :commit do
    print "... Undoing any staged files\n"
    system "git reset HEAD" unless dry_run

    print "... Staging lib/version.rb\n"
    system "git add lib/version.rb" unless dry_run

    print "... Committing staged files\n"
    system "git commit -m 'updating to version #{version_to_s}'" unless dry_run

    print "... Pushing commit files"
    system "git push" unless dry_run
  end
  
  namespace :deploy do
    [:major,:minor,:revision].each do |which|
      desc "Deploy the next #{which} version"
      task which => ["version:next:#{which}","version:commit"]
    end
  end
  
  
  def update_version(increment = :none, operation = :next)
    current_parts = lookup_version_parts
    next_parts = update_version_parts(current_parts,increment,operation)
    current = version_parts_to_s(current_parts)
    nextt = version_parts_to_s(next_parts)
    
    print "... Performing a #{increment} upgrade from #{current} to #{nextt}\n"
    old_input = File.open(version_filename).read
    File.open(version_filename, 'w') {|f| f.write(old_input.sub(current,nextt)) }
    print "... done.\n"
  end

  def version_to_s(increment = :none, operation = :next)
    parts = lookup_version_parts
    version_parts_to_s(parts,increment, operation)
  end

  def version_parts_to_s(parts, increment = :none, operation = :next)
    parts = update_version_parts(parts,increment,operation)
    return "#{parts[0]}.#{parts[1]}.#{parts[2]}"
  end

  def update_version_parts(parts, increment = :none, operation = :next)
    parts = parts.dup
    inc = operation == :next ? 1 : -1
    if increment == :major
      parts[0] += inc 
      parts[1] = 0  
      parts[2] = 0
    elsif increment == :minor
      parts[1] = inc 
      parts[2] = 0
    elsif increment == :revision
      parts[2] += inc  
    end
    return parts
  end

  def lookup_version_parts
    file = File.open(version_filename)
    file.each_line do |l|
      l.strip!
      next unless l.start_with?("VERSION")
      m = l.match(/ = \"(.+)\.(.+)\.(.+)\" unless/)
      return [ m[1].to_i, m[2].to_i, m[3].to_i ]
    end
  end
  
  def version_filename
    Pathname.new(File.join(File.dirname(__FILE__), '..', 'version.rb')).realpath
  end

end
