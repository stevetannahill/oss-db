require 'csv'
namespace :dbfix do
  desc "load new users from a spreadsheet"
  task :load_users => :environment do
    if !ENV["FILE"]
      puts "Please give location of csv file to read new users from. (e.x. ...load_users FILE=\"path/to/file/name.csv\""
    elsif File.exist?(ENV["FILE"])

      newUsers = []
      header = true
      userIndex = nil
      emailIndex = nil
      passwordIndex = nil
      privilegeIndex = nil
      isSysAdminIndex = nil
      providerIndex = nil
      networksIndex = nil
      defaultModeIndex = nil
      CSV.foreach(ENV["FILE"]) do |row|
        #Don't parse header
        if header
          row.each_with_index do |header, index| 
            if header =~ /User/
              userIndex = index
            elsif header =~ /email/
              emailIndex = index
            elsif header =~ /Password/
              passwordIndex = index
            elsif header =~ /Privilege/
              privilegeIndex = index
            elsif header =~ /is_sys_admin/
              isSysAdminIndex = index
            elsif header =~ /Provider/
              providerIndex = index
            elsif header =~ /Networks/
              networksIndex = index
            elsif header =~ /Default_Mode/
              defaultModeIndex = index
            end
          end
          header = false
          raise Exception.new("Could not find all required columns") if (userIndex.nil? or emailIndex.nil? or passwordIndex.nil? or privilegeIndex.nil? or
                                                                                isSysAdminIndex.nil? or providerIndex.nil? or networksIndex.nil?)
        else
          fullName = row[userIndex].split(',')
          fullName[1] = fullName[1].split('[')[0].strip

          networks = row[networksIndex].split(';')
          if networks[0] == "All" or networks[0] == "all" or networks[0] == ""
            networks = []
          end

          networkIDs = networks.collect{ |network| OperatorNetworkType.find_by_name(network)["id"] }

          if !User.find_by_email(row[emailIndex])
            user = User.new({:email => row[emailIndex], :first_name => fullName[1], :last_name => fullName[0], :password => row[passwordIndex],
                            :service_provider_id => ServiceProvider.find_by_name(row[providerIndex])["id"], :privilege_id => Privilege.find_by_name(row[privilegeIndex])["id"],
                            :operator_network_type_ids => networkIDs})
            user.default_mode = row[defaultModeIndex]
            user.save
            puts "#{user.display_name} added to the users table"
          else
            puts "#{row[emailIndex]} already exists"
          end
        end
      end
    else
      if ENV["FILE"].ends_with?(".csv")
        puts "file does not exist"
      else
        puts "#{ENV["FILE"]} is not a csv file"
      end
    end
  end
end
