namespace :dbfix do
  desc "Finds all Unis with a reflection_mechanism and converts it"
  task :uni_vlans => :environment do 
    cr = "\r"           
    clear = "\e[0K"     
    reset = cr + clear
    result = {:modified => 0, :errors => 0, :correct => 0}
    results = {:uni_uni => result.dup, :uni_enni => result.dup}
    utus = EnniNew.all(:include => :demarc_type).select{|d| d.demarc_type and d.demarc_type.ls_access_solution_model == "UNI-UNI"}
    utes = EnniNew.all(:include => :demarc_type).select{|d| d.demarc_type and d.demarc_type.ls_access_solution_model == "UNI-ENNI"}
    begin
      utus.each_with_index do |enni, index|
        print "#{reset}Checking Uni-Uni VLANs #{index+1}/#{utus.size}"
        STDOUT.flush

        clear = true
        error = false
        segment_endpoints = enni.segment_end_points.select{|enni_endpoint| not enni_endpoint.segment.service_provider.is_system_owner}
        segment_endpoints.each do |enni_endpoint|
          uni_endpoint = enni_endpoint.segment.segment_end_points.select{|enni_endpoint| enni_endpoint.demarc.is_a? Uni}.first
          next if uni_endpoint.nil?
          vlan = enni_endpoint.stags
          vlan = vlan.nil? ? "TBD" : vlan
          ctags = uni_endpoint.ctags.nil? ? "TBD" : uni_endpoint.ctags
          if ctags != vlan
            clear = false
            uni_endpoint.ctags = vlan
            uni_endpoint.save
            if uni_endpoint.errors.any?
              error = true
              puts "#{reset}Errors for UNI-UNI Uni(#{uni_endpoint.demarc.cenx_name})'s endpoint #{uni_endpoint.cenx_name}"
              puts "  #{uni_endpoint.errors.full_messages.join("\n  ")}" 
            end
          end
        end

        if not clear
          if error
            results[:uni_uni][:errors] += 1
          else
            results[:uni_uni][:modified] += 1
          end
        else
          results[:uni_uni][:correct] += 1
        end
      end

      utes.each_with_index do |enni, index|
        print "#{reset}Checking Uni-Enni VLANs #{index+1}/#{utes.size}"
        STDOUT.flush

        clear = true
        error = false
        segment_endpoints = enni.segment_end_points.select{|enni_endpoint| not enni_endpoint.segment.service_provider.is_system_owner}
        segment_endpoints.each do |enni_endpoint|
          uni_endpoint = enni_endpoint.segment.segment_end_points.select{|enni_endpoint| enni_endpoint.demarc.is_a? Uni}.first
          next if uni_endpoint.nil?
          vlan = "*"
          ctags = uni_endpoint.ctags
          if ctags != vlan
            clear = false
            uni_endpoint.ctags = vlan
            uni_endpoint.save
            if uni_endpoint.errors.any?
              error = true
              puts "#{reset}Errors for UNI-ENNI Uni(#{uni_endpoint.demarc.cenx_name})'s endpoint #{uni_endpoint.cenx_name}"
              puts "  #{uni_endpoint.errors.full_messages.join("\n  ")}" 
            end
          end
        end

        if not clear
          if error
            results[:uni_enni][:errors] += 1
          else
            results[:uni_enni][:modified] += 1
          end
        else
          results[:uni_enni][:correct] += 1
        end
      end
    ensure
      print reset
      puts "\n\nSummary:"
      puts "UNI-UNI: Total:#{utus.size}, Fixed:#{results[:uni_uni][:modified]}, Correct:#{results[:uni_uni][:correct]}, Failed:#{results[:uni_uni][:errors]}"
      puts "UNI-ENNI: Total:#{utes.size}, Fixed:#{results[:uni_enni][:modified]}, Correct:#{results[:uni_enni][:correct]}, Failed:#{results[:uni_enni][:errors]}"
      missing = EnniNew.all(:include => :demarc_type).select{|d| d.demarc_type and (d.demarc_type.ls_access_solution_model != "UNI-ENNI" and d.demarc_type.ls_access_solution_model != "UNI-UNI")}
      puts "MISSING INFO: #{missing.size} [#{missing.map{|d| d.cenx_id}.join(", ")}]"
    end
  end
end