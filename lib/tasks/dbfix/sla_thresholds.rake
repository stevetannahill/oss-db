namespace :dbfix do
  desc "Goes through all Cos Test Vectors and confirms or fixes their thresholds to the SLA specified values"
  task :sla_thresholds => :environment do 
    cr = "\r"           
    clear = "\e[0K"     
    reset = cr + clear
    results = Hash.new
    costs = CosTestVector.all#(:limit => 10)
    total = costs.size
    begin
      costs.each_with_index do |cost, index|
        print "#{reset}Converting Cost #{index+1}/#{total}"
        STDOUT.flush
        #puts "Converting Cost #{index+1}"
        service_provider = cost.cos_end_point.segment_end_point.segment.service_provider.name.to_sym
        #puts service_provider
        next unless SLA_THRESHOLDS.keys.include? service_provider
        results[service_provider] = Hash.new(0) if results[service_provider].nil?
        desired_thresholds = SLA_THRESHOLDS[service_provider]
        thresholds = {}
        desired_thresholds.keys.each do |method|
          thresholds[method] = cost.send(method).to_f
        end

        #puts thresholds.inspect
        #puts desired_thresholds.inspect

        if thresholds != desired_thresholds
          cost.assign_attributes(desired_thresholds)
          cost.save
          if cost.errors.any?
            puts "#{reset}Errors for CosTV(#{cost.cenx_id}) for Service Provider #{service_provider}"
            puts "  #{cost.errors.full_messages.join("\n  ")}" 
            cost.save(:validate => false)
            results[service_provider][:errors] += 1
          else
            results[service_provider][:modified] += 1
          end
          #puts "Converted"
        else
          results[service_provider][:correct] += 1
          #puts "Correct"
          next
        end
      end
    ensure
      print reset
      puts "\n\nSummary:"
      results.each do |aav, stats|
        puts "#{aav}: Count: #{stats[:correct]+stats[:modified]} Fixed: #{stats[:modified]} Already Correct: #{stats[:correct]} Error: #{stats[:errors]}"
      end
    end
  end


  SLA_THRESHOLDS = {
    :ATT => {
      :delay_sla_guarantee => 8000,
      :delay_error_threshold => 8000,
      :delay_warning_threshold => 6400,
      :dv_sla_guarantee => 2000,
      :dv_error_threshold => 2000,
      :dv_warning_threshold => 1600,
      :flr_sla_guarantee => 5E-03,
      :flr_error_threshold => 5E-03,
      :flr_warning_threshold => 4E-03,
      :availability_guarantee => 99.99,
    },
    :Comcast => {
      :delay_sla_guarantee => 8000,
      :delay_error_threshold => 8000,
      :delay_warning_threshold => 6400,
      :dv_sla_guarantee => 2000,
      :dv_error_threshold => 2000,
      :dv_warning_threshold => 1600,
      :flr_sla_guarantee => 5E-03,
      :flr_error_threshold => 5E-03,
      :flr_warning_threshold => 4E-03,
      :availability_guarantee => 99.99,
    },
    :TWC => {
      :delay_sla_guarantee => 8000,
      :delay_error_threshold => 8000,
      :delay_warning_threshold => 6400,
      :dv_sla_guarantee => 2000,
      :dv_error_threshold => 2000,
      :dv_warning_threshold => 1600,
      :flr_sla_guarantee => 6.25E-05,
      :flr_error_threshold => 6.25E-05,
      :flr_warning_threshold => 5E-05,
      :availability_guarantee => 99.99,
    },
    :Towercloud => {
      :delay_sla_guarantee => 8000,
      :delay_error_threshold => 8000,
      :delay_warning_threshold => 6400,
      :dv_sla_guarantee => 2000,
      :dv_error_threshold => 2000,
      :dv_warning_threshold => 1600,
      :flr_sla_guarantee => 6.25E-05,
      :flr_error_threshold => 6.25E-05,
      :flr_warning_threshold => 5E-05,
      :availability_guarantee => 99.99,
    },
    :Charter => {
      :delay_sla_guarantee => 8000,
      :delay_error_threshold => 8000,
      :delay_warning_threshold => 6400,
      :dv_sla_guarantee => 2000,
      :dv_error_threshold => 2000,
      :dv_warning_threshold => 1600,
      :flr_sla_guarantee => 1E-03,
      :flr_error_threshold => 1E-03,
      :flr_warning_threshold => 8E-02,
      :availability_guarantee => 99.99,
    },
    :Dukenet => {
      :delay_sla_guarantee => 8000,
      :delay_error_threshold => 8000,
      :delay_warning_threshold => 6400,
      :dv_sla_guarantee => 2000,
      :dv_error_threshold => 2000,
      :dv_warning_threshold => 1600,
      :flr_sla_guarantee => 6.25E-05,
      :flr_error_threshold => 6.25E-05,
      :flr_warning_threshold => 5E-05,
      :availability_guarantee => 99.99,
    },
    :Fiberlight => {
      :delay_sla_guarantee => 8000,
      :delay_error_threshold => 8000,
      :delay_warning_threshold => 6400,
      :dv_sla_guarantee => 2000,
      :dv_error_threshold => 2000,
      :dv_warning_threshold => 1600,
      :flr_sla_guarantee => 6.25E-05,
      :flr_error_threshold => 6.25E-05,
      :flr_warning_threshold => 5E-05,
      :availability_guarantee => 99.99,
    },
    :LightTower => {
      :delay_sla_guarantee => 8000,
      :delay_error_threshold => 8000,
      :delay_warning_threshold => 6400,
      :dv_sla_guarantee => 2000,
      :dv_error_threshold => 2000,
      :dv_warning_threshold => 1600,
      :flr_sla_guarantee => 6.25E-05,
      :flr_error_threshold => 6.25E-05,
      :flr_warning_threshold => 5E-05,
      :availability_guarantee => 99.99,
    },
    :Phonoscope => {
      :delay_sla_guarantee => 8000,
      :delay_error_threshold => 8000,
      :delay_warning_threshold => 6400,
      :dv_sla_guarantee => 2000,
      :dv_error_threshold => 2000,
      :dv_warning_threshold => 1600,
      :flr_sla_guarantee => 6.25E-05,
      :flr_error_threshold => 6.25E-05,
      :flr_warning_threshold => 5E-05,
      :availability_guarantee => 99.99,
    },
    :Zayo => {
      :delay_sla_guarantee => 8000,
      :delay_error_threshold => 8000,
      :delay_warning_threshold => 6400,
      :dv_sla_guarantee => 2000,
      :dv_error_threshold => 2000,
      :dv_warning_threshold => 1600,
      :flr_sla_guarantee => 6.25E-05,
      :flr_error_threshold => 6.25E-05,
      :flr_warning_threshold => 5E-05,
      :availability_guarantee => 99.99,
    }
  }
end