namespace :dbfix do

  def get_member_handle(object)
    system_owner = ServiceProvider.find_by_is_system_owner(1)
    object.member_handle_instance_by_owner(system_owner)
  end

  def get_cascade(path)
    return get_member_handle(path).value
  end

  def get_aav(path)
    system_owner = ServiceProvider.find_by_is_system_owner(1)
    return path.segments.select{|s| s.service_provider != system_owner}.first.service_provider.name
  end

  def missing_costv?(path)
    system_owner = ServiceProvider.find_by_is_system_owner(1)
    middle_segment = path.segments.select{|s| s.service_provider != system_owner}.first
    uni_endpoint = middle_segment.segment_end_points.select{|ep| ep.is_a? OvcEndPointUni}.first
    primary_cost = nil
    backup_cost = nil
    uni_endpoint.cos_test_vectors.each do |costv|
      if costv.circuit_id =~ /-P-/
        primary_cost = costv
      elsif costv.circuit_id =~ /-B-/
        backup_cost = costv
      end
      break if (primary_cost and backup_cost)
    end

    return !(primary_cost and backup_cost)
  end

  desc 'Finds and converts point to point circuits that should be dual evc'
  task :all_ptp_to_dual => :environment do
    aavs = ENV["AAVS"] || "TWC,Towercloud,Comcast"
    aavs = aavs.split(",")

    paths = Path.all.select{|p| aavs.include? get_aav(p)}
    paths.reject!{|p| cascade = get_cascade(p); cascade =~ /-P$/ or cascade =~ /-B$/}
    paths.reject!{|p| missing_costv?(p)}
    paths.map!{|p| get_cascade(p)}
    puts "Found #{paths.size} candidates"
    ENV["CASCADES"] = paths.join(",")
    Rake::Task['dbfix:ptp_to_dual'].invoke
  end

  desc 'Convert previously created point to point circuits into proper dual evc circuits'
  task :ptp_to_dual => :environment do
    $fake_output = false
    require './db/fake_go_in_service.rb'

    if ENV["CASCADES"]
      cascades = ENV["CASCADES"].split(",")
    else
      STDOUT.puts "Please enter your cascades"
      cascades = STDIN.gets.chomp.split(",")
    end

    verbosity = case ENV["VERBOSE"]
      when "quiet"
        0
      when "normal"
        1
      when "loud"
        2
      when "blaring"
        3
      else
        0
    end

    # move cursor to beginning of line
    cr = "\r"           
    clear = "\e[0K"     
    reset = cr + clear

    info = {:success => [], :failures => {}}
    begin
      cascades.each_with_index do |cascade, index|
        print "#{reset}Converting Cascade(#{index+1}/#{cascades.size}): #{cascade}"
        STDOUT.flush
        begin
          ActiveRecord::Base.transaction do
            #mais = MemberAttrInstance.search(cascade).select{|m| m.affected_entity_type == 'Path'}
            mais = MemberAttrInstance.find_by_value_and_affected_entity_type(cascade, 'Path')
            path = mais.affected_entity
            Ptp2DualConverter.convert(path, verbosity)
            info[:success] << cascade
          end
        rescue Exception => e
          info[:failures][cascade] = e
        end
      end
    ensure
      print "#{reset}Reindexing Sphinx"
      STDOUT.flush
      Rake::Task['ts:rebuild'].invoke

      info[:failures].each do |cascade, error|
        puts "-Failures for cascade #{cascade}"
        puts error.message  
        puts error.backtrace.inspect if verbosity > 0
      end
      puts "\n\nSummary"
      puts " Success: #{info[:success].size == cascades.size ? "All (#{info[:success].size})" : info[:success].size}"
      puts " Failures: #{info[:failures].keys.size}" if info[:failures].keys.any?
    end
  end

  class Ptp2DualConverter

    def self.convert(path, verbosity)
      @verbosity = verbosity
      @system_owner = ServiceProvider.find_by_is_system_owner(1)
      @endpoint_connections = Hash.new
      @cos_connections = Hash.new

      copy_and_dual_path(path)
      @new_path.reload
      set_monitoring_endpoint_vlan
      log = ""
      success = StateHelper.evc_live(@new_path, log)
      vprint log, 3
      raise Exception.new(log) unless success

      # @new_path.layout_diagram
      
      return @new_path
    end
    
    def self.copy_and_dual_path(path)
      vprint" Cloning Path: #{path.cenx_name}", 1
      clone_path(path)

      path.segments.each do |segment|
        if segment.is_a?(OffNetOvc) && segment.service_provider.name == 'Sprint'
          reuse_segment(segment)
          next
        end

        vprint"  Cloning Segment: #{segment.cenx_name}", 1
        clone_segment(segment)
        segment.segment_end_points.each do |segment_endpoint|
          vprint"   Cloning Segment Endpoint: #{segment_endpoint.cenx_name}", 1
          clone_segment_endpoint(segment_endpoint)
          @monitoring_endpoint = @new_segment_endpoint if (segment_endpoint.respond_to?(:is_connected_to_monitoring_port) and segment_endpoint.is_connected_to_monitoring_port)
          segment_endpoint.cos_end_points.each do |cos_endpoint|
            vprint"    Cloning Cos Endpoint: #{cos_endpoint.cos_name}", 2
            cos_endpoint = clone_cos_endpoint(cos_endpoint)
            @new_segment_endpoint.cos_end_points << @new_cos_endpoint
          end
          @new_segment.segment_end_points << @new_segment_endpoint
        end
      end

      connect_segment_endpoints
      connect_cos_endpoints

      return @new_path
    end

    def self.clone_path(path)
      @new_path = path.dup
      @new_path.operator_network = path.operator_network
      @new_path.path_type = path.path_type
      save @new_path

      mh = get_member_handle(path).value.dup
      convert_member_handle(path, mh, 'P')
      convert_member_handle(@new_path, mh, 'B')
    end

    def self.reuse_segment(segment)
      @new_path.segments << segment
      segment.segment_end_points.each do |ep|
        demarc = ep.demarc
        add_endpoint_connection(demarc, ep)

        ep.cos_end_points.each do |c|
          add_cos_connection(demarc, c, c)
        end

      end
    end

    def self.clone_segment(segment)
      @new_segment = segment.dup
      @new_segment.site = segment.site
      @new_segment.segment_type = segment.segment_type
      @new_segment.operator_network_id = segment.operator_network_id
      @new_segment.path_network = segment.path_network
      @new_segment.ethernet_service_type = segment.ethernet_service_type
      @new_segment.emergency_contact = segment.emergency_contact
      @new_segment.service_id = IdTools::ServiceIdGenerator.generate_on_net_ovc_service_id @new_segment.site if @new_segment.is_a? OnNetOvc
      @new_segment.paths << @new_path
      save @new_segment

      mh = get_member_handle(segment).value.dup
      convert_member_handle(segment, mh, 'P')
      convert_member_handle(@new_segment, mh, 'B')
    end

    def self.clone_segment_endpoint(segment_endpoint)
      @new_segment_endpoint = segment_endpoint.dup
      @new_segment_endpoint.segment_end_point_type = segment_endpoint.segment_end_point_type
      @demarc = segment_endpoint.demarc
      new_demarc = find_demarc(@demarc)
      raise Exception.new("Demarc #{@demarc.inspect} could not find matching demarc") if !@demarc.nil? && new_demarc.nil?
      @new_segment_endpoint.demarc = new_demarc
      save @new_segment_endpoint
      add_endpoint_connection(@demarc, @new_segment_endpoint)      
    end

    def self.clone_cos_endpoint(cos_endpoint)
      find_cos_instance(cos_endpoint)
      @new_cos_endpoint = cos_endpoint.dup
      @new_cos_endpoint.cos_instance = @new_cos_instance
      save @new_cos_endpoint
      cos_endpoint.cos_test_vectors.select{|c| c.circuit_id =~ /-B-/}.each do |cost|
        vprint"      Moving cos test vector #{cost.circuit_id} to new cos endpoint", 2
        cost.cos_end_point = @new_cos_endpoint
        save cost
        @vlan = cost.vlan_id
      end
      add_cos_connection(@demarc, @new_cos_endpoint, cos_endpoint)
    end

    def self.find_cos_instance(cos_endpoint)
      @new_cos_instance = @new_segment.cos_instances.select{|c| c.cos_name == cos_endpoint.cos_name}.first
      if @new_cos_instance.nil?
        @new_cos_instance = cos_endpoint.cos_instance.dup
        @new_cos_instance.class_of_service_type = cos_endpoint.cos_instance.class_of_service_type
        @new_cos_instance.segment = @new_segment
        save @new_cos_instance  
        @new_segment.reload
      end
    end

    def self.set_monitoring_endpoint_vlan
      @monitoring_endpoint.stags = @vlan
      save @monitoring_endpoint
    end


    def self.find_demarc(demarc)
      return nil if demarc.nil?
      new_demarc = nil
      if demarc.is_a? Uni
        vprint"    Reusing demarc for Uni: #{demarc.id}", 1
        new_demarc = demarc
      else
        site = demarc.site
        if demarc.demarc_icon == 'router'
          new_demarc = lookup_demarc(demarc, site)
        elsif demarc.is_connected_to_monitoring_port
          new_demarc = lookup_demarc(demarc, site)
        else
          new_demarc = lookup_demarc(demarc, site)
        end
      end
      return new_demarc
    end

    def self.lookup_demarc(demarc, site)
      return nil if site.nil? || demarc.nil?
      member_handle = demarc.member_handle_instance_by_owner(@system_owner).value.dup
      if member_handle =~ /IPA01/
        member_handle.gsub!('IPA01', 'IPA02')
      elsif member_handle =~ /IPA02/
        member_handle.gsub!('IPA02', 'IPA01')
      elsif member_handle =~ /Primary/
        member_handle.gsub!('Primary', 'Alternate')
      elsif member_handle =~ /Alternate/
        member_handle.gsub!('Alternate', 'Primary')
      end

      vprint"    Looking up demarc #{member_handle} on site #{site.name}", 1
      # demarcs = MemberAttrInstance.search(member_handle).select{|m| m.affected_entity_type == 'Demarc'}.map{|m| m.affected_entity}
      demarcs = MemberAttrInstance.find_all_by_value_and_affected_entity_type(member_handle, 'Demarc').map{|m| m.affected_entity}
      demarc = demarcs.select{|d| d.ports.any? and d.ports.first.node.site == site}.first
      raise Exception.new("Could not find demarc #{member_handle} on site #{site.name}") if demarc.nil?
      vprint"    From demarc #{@demarc.id} found demarc #{demarc.id}", 3
      return demarc
    end

    def self.add_endpoint_connection(demarc, segment_endpoint)
       unless demarc.nil?
        @endpoint_connections[demarc] = [] if @endpoint_connections[demarc].nil?
        @endpoint_connections[demarc] << segment_endpoint
      end
    end

    def self.add_cos_connection(demarc, cos, other_cos)
      unless demarc.nil?
        @cos_connections[demarc] = {} if @cos_connections[demarc].nil?
        name = cos.cos_instance.class_of_service_type.operator_network_type.name + ":" + cos.cos_name
        @cos_connections[demarc][name] = {
          :cos_endpoint => cos, 
          :connected => other_cos.cos_end_points.map{|c| c.cos_instance.class_of_service_type.operator_network_type.name + ":" + c.cos_name}
        }
      end
    end

    def self.connect_segment_endpoints
      print_segment_connections
      @endpoint_connections.each do |demarc, endpoints|
        next if (endpoints.size < 2 or demarc.nil?)
        endpoints.each do |ep|
          endpoints.each {|ep2| ep.segment_end_points << ep2 unless ep == ep2}
        end
      end
    end

    def self.connect_cos_endpoints
      print_cos_connections
      @cos_connections.each do |demarc, cos_connection|
        next if (cos_connection.keys.size < 2 or demarc.nil?)
        cos_connection.each do |name, info|
          cos = info[:cos_endpoint]
          info[:connected].each do |other|
            cos.cos_end_points << cos_connection[other][:cos_endpoint]
          end
        end
      end
    end


    def self.save(object)
      object.save
      if object.errors.any?
        raise Exception.new "Building #{object.class} failed with error #{object.errors.full_messages}"
      else
        vprint"Saved #{object.class}: #{object.id} #{object.respond_to?("cenx_id") ? object.cenx_id : (object.respond_to?("cos_name") ? object.cos_name : object.name)}", 3
      end
      object.reload
      return object
    end

    def self.convert_member_handle(object, member_handle, role)
      re = member_handle.match(/([\w\d\s:]*)(-?\d*)?/)
      member_handle = "#{re[1]}-#{role}" 
      member_handle_instance = get_member_handle(object)
      member_handle_instance.value = member_handle
      save member_handle_instance
    end

    def self.vprint(string, level)
      if level <= @verbosity
         puts string
      end
    end

    def self.print_segment_connections
      vprint"Connecting Segment Endpoints:", 3
      vprint"{", 3
      @endpoint_connections.each do |demarc, endpoints|
        vprint"  Demarc: #{get_member_handle(demarc).value}:#{demarc.id} => #{endpoints.map{|e| e.id}.inspect},", 3
      end
      vprint"}", 3
    end

    def self.print_cos_connections
      vprint"Connecting Cos Endpoints:", 3
      vprint"{", 3
      @cos_connections.each do |demarc, connections|
        vprint"  Demarc: #{get_member_handle(demarc).value}:#{demarc.id} => {", 3
        connections.each do |name, info|
          vprint"    #{name} => {:cos_endpoint => #{info[:cos_endpoint].id}, :connected => #{info[:connected].inspect}", 3
        end
      end
      vprint"}", 3
    end
  end

end
