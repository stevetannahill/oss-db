namespace :lkp do
  desc "Generate/Update the Redis CosTestVectors, used when updating the database manually"
  task :recount => :environment do    
    CosTestVector.all.each(&:update_lkp)
  end
end
