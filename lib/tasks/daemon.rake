namespace :daemon do
  script_locations = {
	  :event_server => 'event'
  }
  
  nice_cmd = "nice -n 15"

  script_locations.keys.each do |daemon|
     namespace daemon do
      [:start,:stop,:restart,:status].each do |action|
        desc "#{action.to_s.capitalize} the #{daemon.to_s.capitalize} Daemon"
        task action do
          system("#{nice_cmd} #{Rails.root}/lib/#{script_locations[daemon]}/#{daemon}_ctl #{action} -- #{Rails.env}")
        end
      end

      desc "Start or Restart the #{daemon.to_s.capitalize} Daemon"
      task :start_or_restart do
        system("#{nice_cmd} #{Rails.root}/lib/#{script_locations[daemon]}/#{daemon}_ctl stop -- #{Rails.env}")
        system("#{nice_cmd} #{Rails.root}/lib/#{script_locations[daemon]}/#{daemon}_ctl start -- #{Rails.env}")
      end
    end
  end

  script_runner_locations = {
	  :sam_if_server => 'sam_stats_if',
	  :bx_if_server => 'bx_stats_if',
	  :bx_snmp_server => 'bx_stats_if',
	  :exception_daemon => 'stats',
	  :availability_daemon => 'stats',
  }

  script_runner_locations.keys.each do |daemon|
     namespace daemon do
      [:start,:stop,:restart,:status].each do |action|
        desc "#{action.to_s.capitalize} the #{daemon.to_s.capitalize} Daemon"
        task action do
          system("#{nice_cmd} #{Rails.root}/lib/sam_stats_if/script_runner_ctl #{action} -- #{Rails.root}/lib/#{script_runner_locations[daemon]}/#{daemon}.rb")
        end
      end

      desc "Start or Restart the #{daemon.to_s.capitalize} Daemon"
      task :start_or_restart do
        system("#{nice_cmd}  #{Rails.root}/lib/sam_stats_if/script_runner_ctl stop -- #{Rails.root}/lib/#{script_runner_locations[daemon]}/#{daemon}.rb")
        system("#{nice_cmd}  #{Rails.root}/lib/sam_stats_if/script_runner_ctl start -- #{Rails.root}/lib/#{script_runner_locations[daemon]}/#{daemon}.rb")
      end
    end
  end
  
  bin_locations = {
	  :automated_build_center => 'automated_build_center'
  }

  bin_locations.keys.each do |daemon|
     namespace daemon do
      [:start,:stop,:restart,:status].each do |action|
        desc "#{action.to_s.capitalize} the #{daemon.to_s.capitalize} Daemon"
        task action do
          system("#{nice_cmd} #{Rails.root}/lib/#{bin_locations[daemon]}/#{daemon}_ctl #{action}")
        end
      end

      desc "Start or Restart the #{daemon.to_s.capitalize} Daemon"
      task :start_or_restart do
        system("#{nice_cmd} #{Rails.root}/lib/#{script_locations[daemon]}/#{daemon}_ctl stop")
        system("#{nice_cmd} #{Rails.root}/lib/#{script_locations[daemon]}/#{daemon}_ctl start")
      end
    end
  end  
  
end
