begin  
  require 'metric_fu'  
    
  MetricFu::Configuration.run do |config|  
    config.metrics = [:rcov,:rails_best_practices]  
    # config.flay ={:dirs_to_flay => ['app', 'lib'], :filetypes => ['rb'] }

    config.rcov[:test_files] = ['spec/**/*_spec.rb']  
    config.rcov[:rcov_opts] << "-Ispec" # Needed to find spec_helper
    # config.flog = { :dirs_to_flog => ['app'] }  
  end  
    
rescue LoadError  
end