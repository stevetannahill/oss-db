namespace :monit do
  
  desc "Stop monit process from managing processes"
  task :stop do
    system "monit -c #{monitrc_file} quit"
  end
  
  desc "Start monit process to manage processes"
  task :start do
    system "monit -c #{monitrc_file}"
  end
  
  desc "Unmonitor a particular process (specify 'all' to unmonitor all)"
  task :unmonitor, :process_name do |t,args|
    system "monit -c #{monitrc_file} unmonitor #{args.process_name}"
  end
  
  desc "Monitor a particular process (specify 'all' to monitor all)"
  task :monitor, :process_name do |t, args|
    system "monit -c #{monitrc_file} monitor #{args.process_name}"
  end
  
  desc "Restart monit process"
  task :restart do
    monit.stop
    system  "sleep 5"
    monit.start
  end
  
  def monitrc_file
    "/home/deployer/.monitrc"
  end
  
end
