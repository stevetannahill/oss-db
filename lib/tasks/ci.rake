unless ARGV.any? {|a| a =~ /^gems/} # Don't load anything when running the gems:* tasks
  namespace :ci do
    desc "Perform a build on the CI server"
    task :build  do
      begin
        Rake::Task['ci:db:config'].invoke
        Rake::Task['ci:local'].invoke
        Rake::Task['ci:success'].invoke
      rescue Exception => e
        Rake::Task['ci:failure'].invoke
        raise e
      end
    end
    
    task :local do
      Rake::Task['ci:db:reset'].invoke
      Rake::Task['ci:qa'].invoke
      # Rake::Task['ci:deploy:chuck:headless'].invoke
    end
    
    desc "Update your code base (will erase local changes!!!)"
    task :rebase do
      system('git checkout .')
      system('git pull --rebase')
    end

    desc "Run QA"
    task :qa do
      Rake::Task['ci:rspec'].invoke
      # no longer required, can be run directly over top of rspec
      # Rake::Task['ci:rcov'].invoke
    end

    desc "Run Rspec"
    task :rspec do
      system "mkdir -p ../public" unless File.exists?("../public")
      Rake::Task['ci:rspec_run'].invoke
      Rake::Task['ci:rspec_coresite_run'].invoke
    end

    RSpec::Core::RakeTask.new(:rspec_run) do |t|
      t.pattern = "./spec/**/*spec.rb"
      t.rspec_opts = ["--tag ~coresite_app", "--format", "html", "--out", "../public/rspec.html"]
      t.fail_on_error = true
    end
    
    RSpec::Core::RakeTask.new(:rspec_coresite_run) do |t|
      t.pattern = "./spec/**/*spec.rb"
      t.rspec_opts = ["--tag coresite_app", "--format", "html", "--out", "../public/rspec.html"]
      t.fail_on_error = true
    end

    # desc "Run Rcov"
    # task :rcov do
    #   system "mkdir -p ../public/coverage" unless File.exists?("../public/coverage")
    #   system "ruby -S bundle exec rcov -Ispec:lib --exclude osx\/objc,gems\/,spec\/,features\/ --rails --output ../public/coverage ./spec/**/*_spec.rb"
    # end
    
    [:chuck,:inventorysprint].each do |host|
      namespace :deploy do
        namespace host do
          [:headless,:full].each do |mode|

            desc "Deploy the application (#{mode}) on #{host}"
            task mode do
              Rake::Task["ci:deploy:#{host}:#{mode}:go"].invoke
            end

            namespace mode do

              task :go do
                Rake::Task["ci:deploy:#{host}:#{mode}:get_backup_db"].invoke
                Rake::Task["ci:deploy:#{host}:#{mode}:load_backup_db"].invoke
                Rake::Task["ci:deploy:#{host}:#{mode}:update_code"].invoke
                system "curl http://#{host}.cenx.localnet:2001/version > ../public/version.txt"
              end

              task :update_code do
                puts "About to update the application on #{host}"
                deploy_dir = File.expand_path(File.dirname(__FILE__) + '/../..')
                headless_cmd = mode == :headless ? "-S headless=true" : ""
                all_scripts = [ "cd #{deploy_dir} && cap -f Capfile.new #{host} deploy:setup", "cd #{deploy_dir} && cap -f Capfile.new #{host} deploy:cleanup #{headless_cmd}", "cd #{deploy_dir} && cap -f Capfile.new #{host} deploy:migrations #{headless_cmd}" ]
                all_scripts.each do |script|
                  sh script do |ok, status|
                    ok or fail "Unable to deploy application (#{status.exitstatus}): [#{script}]"
                  end
                end
              end

              task :get_backup_db do
                puts "Moving the production data to #{host}"
                system "scp deployer@gandalf.cenx.localnet:/home/deployer/mysql_backups/oss_db_production_latest.sql.tar.gz deployer@#{host}.cenx.localnet:/tmp"
                system "ssh deployer@#{host}.cenx.localnet 'cd /tmp && tar zxfv oss_db_production_latest.sql.tar.gz'"
              end

              task :load_backup_db do
                puts "Loading the production db into #{host}"
                system "ssh deployer@#{host}.cenx.localnet 'echo \"DROP DATABASE IF EXISTS oss_db_production; CREATE DATABASE oss_db_production;\" > /tmp/oss_db_production_create.sql'"
                system "ssh deployer@#{host}.cenx.localnet 'mysql -u root -h 127.0.0.1 -pdev1234 < /tmp/oss_db_production_create.sql'"
                system "ssh deployer@#{host}.cenx.localnet 'mysql -u root -h 127.0.0.1 -pdev1234 -D oss_db_production  < /tmp/oss_db_production.sql'"
              end

            end
          end
        end
      end
    end

    desc "The Build Succeeded, so tell our monitoring service"
    task :success do
      if File.exists?("/home/deployer/monitor/log")
        system 'echo "Cdb succeeded, http://cc.cenx.localnet" > /home/deployer/monitor/log/Cdb.cc'
      else
        print "BUILD SUCCEEDED, but log directory (/home/deployer/monitor/log) does not exist"
      end
    end

    desc "The Build failed, so tell our monitoring service"
    task :failure do
      if File.exists?("/home/deployer/monitor/log")
        system "curl http://cc.cenx.localnet/oss-db > /home/deployer/monitor/log/Cdb.cc"
      else
        raise "BUILD FAILED, but log directory (/home/deployer/monitor/log) does not exist"
      end
    end
        
    namespace :db do

      desc "Setup the correct database configuration files"
      task :config do
        source_db_file = '/cenx/oss-db/sensitive/database.yml'
        dest_db_file = "#{Rails.root}/config/database.yml"
        abort "No database file [#{source_db_file}], unable to continue CI build" unless File.exists? source_db_file
        FileUtils.cp source_db_file, dest_db_file, :preserve => false
      end

      desc "Setup the database"
      task :reset do
        Rake::Task['db:drop'].invoke
        Rake::Task['db:create'].invoke
        Rake::Task['db:migrate'].invoke
      end
    end
  end
end
