=begin
ex. call
bundle exec rake scripts:abc_results 'path/to/automated_build_center_results_failed.csv' 'path/to/automated_build_center_results_NCS.csv' 'path/to/automated_build_center_results_ok.csv' 'build_results_analysis.csv'
=end


namespace :scripts do

  @fallout_types = {}
  @mw_fallout_types = {}
  @fallout_cascades = {}

  desc 'Analyze the build logs from the automated build center.'
  task :abc_results => :environment do

    #create an array that will sotre all the build result files
    #use the 'ARGV[i...j]' to get the arguments from the environment (3 dots means the last value is not included). Note: the first argument will always be the name of the task, therefore start from 1 (ARGV[1])
    infiles = ARGV[1...-1]
    
    #at this point, 'infiles' array will have the necessary files required to be processed 
    #get the last entry from the ARGV array into the 'outfile' variable
    outfile = ARGV[-1]

    total_successful = 0
    total_mw_successful = 0
    total_fallout = 0
    total_mw_fallout = 0
    mw_fallout_cascades = []

    oems_successful = {}
    oems_mw_successful = {}
    oems_fallout = {}
    oems_mw_fallout = {}

    aavs_successful = {}
    aavs_fallout = {}

    infiles.each do |infile|
      id_index = nil
      name_index = nil
      summary_index = nil
      summary_detail_index = nil
      details_index = nil
      created_index = nil
      updated_index = nil
      grid_id_index = nil

      puts "Reading in file #{infile}..."
    
      CSV.foreach(infile) do |row|
        new_row = []
        # get the column indexs
        if !id_index
          row.each_with_index do |title, index|
            if title.downcase =~ /^id/
              id_index = index
            elsif title.downcase =~ /name/
              name_index = index
            elsif title.downcase =~ /summary$/
              summary_index = index
            elsif title.downcase =~ /summary_detail/
              summary_detail_index = index
            elsif title.downcase =~ /details/
              details_index = index
            elsif title.downcase =~ /created/
              created_index = index
            elsif title.downcase =~ /updated/
              updated_index = index
            elsif title.downcase =~ /grid/
              grid_id_index = index
            else
              puts "CSV file contains unknown column #{title} at index #{index} -- ignoring"
            end
          end
        else
          mw_fallout = false
          case row[summary_index]
          when "Ok"
            total_successful += 1
            if (decoded_cid = CircuitIdFormat::SpirentSprint.decode(row[name_index])) && (pb = PlayBook.data(decoded_cid[:site_name]))
              # if this oem/aav already has a count then increment, otherwise start the count
              oems_successful[pb[:oem]] ? oems_successful[pb[:oem]] += 1 : oems_successful[pb[:oem]] = 1
              aavs_successful[pb[:access_provider]] ? aavs_successful[pb[:access_provider]] += 1 : aavs_successful[pb[:access_provider]] = 1
              if pb[:access_provider] == "Sprint MW"
                total_mw_successful += 1 
                oems_mw_successful[pb[:oem]] ? oems_mw_successful[pb[:oem]] += 1 : oems_mw_successful[pb[:oem]] = 1
              end
            else
              puts "** Error decoding cid or finding play book entry for a successfully built CTV **"
              puts "oem, aav, and mw count not incremented"
            end
          when "Not Currently Supported"
            total_fallout += 1

            if (decoded_cid = CircuitIdFormat::SpirentSprint.decode(row[name_index])) && (pb = PlayBook.data(decoded_cid[:site_name]))
              # if this oem/aav already has a count then increment, otherwise start the count
              oems_fallout[pb[:oem]] ? oems_fallout[pb[:oem]] += 1 : oems_fallout[pb[:oem]] = 1
              aavs_fallout[pb[:access_provider]] ? aavs_fallout[pb[:access_provider]] += 1 : aavs_fallout[pb[:access_provider]] = 1
              if pb[:access_provider] =~ /Sprint MW/ || AutomatedBuildCenter::MICROWAVE_SITE_TYPES.include?(decoded_cid[:site_type])
                mw_fallout = true
                total_mw_fallout += 1
                mw_fallout_cascades << decoded_cid[:site_name]
                oems_mw_fallout[pb[:oem]] ? oems_mw_fallout[pb[:oem]] += 1 : oems_mw_fallout[pb[:oem]] = 1
              end
            else
              puts "** Error decoding cid or finding play book entry for not currently supported CTV **"
              puts "oem, aav, and mw count not incremented"
            end

            add_fallout("Vendor not supported", row[name_index].split("-")[0], mw_fallout)

          when "Failed"
            total_fallout += 1

            if row[summary_detail_index] =~ /CIR must be 5 characters/ || row[summary_detail_index] =~ /Circuit Id conists of 6 parts seperated by -./
              add_fallout("Bad circuit ID format", row[name_index].split("-")[0])
            elsif row[summary_detail_index] =~ /Could not find Cascade .* in playbook/
              if AutomatedBuildCenter::MICROWAVE_SITE_TYPES.include?(row[name_index].split("-")[1])
                mw_fallout = true
                total_mw_fallout += 1
                mw_fallout_cascades << row[name_index].split("-")[0]
              end
              add_fallout("Cascade not in Playbook", row[name_index].split("-")[0], mw_fallout)
            else
              if (decoded_cid = CircuitIdFormat::SpirentSprint.decode(row[name_index])) && (pb = PlayBook.data(decoded_cid[:site_name]))
                # if this oem/aav already has a count then increment, otherwise start the count
                oems_fallout[pb[:oem]] ? oems_fallout[pb[:oem]] += 1 : oems_fallout[pb[:oem]] = 1
                aavs_fallout[pb[:access_provider]] ? aavs_fallout[pb[:access_provider]] += 1 : aavs_fallout[pb[:access_provider]] = 1
                if pb[:access_provider] =~ /Sprint MW/ || AutomatedBuildCenter::MICROWAVE_SITE_TYPES.include?(decoded_cid[:site_type])
                  mw_fallout = true
                  total_mw_fallout += 1
                  mw_fallout_cascades << decoded_cid[:site_name]
                  oems_mw_fallout[pb[:oem]] ? oems_mw_fallout[pb[:oem]] += 1 : oems_mw_fallout[pb[:oem]] = 1
                end
              else
                puts "** Error decoding cid or finding play book entry for Failed (not bad cid and not missing pb entry) CTV **"
                puts "oem, aav, and mw count not incremented"
              end

              if row[summary_detail_index] =~ /The corresponding Circuit Id for Grid id/
                add_fallout("Grid ID in use", decoded_cid[:site_name], mw_fallout)

              elsif row[summary_detail_index] =~ /Circuit does not match existing formats/ || row[summary_detail_index] =~ /Circuit VLAN is .* does not match existing VLAN/
                add_fallout("Data changed from Initial", decoded_cid[:site_name], mw_fallout)

              elsif row[summary_detail_index] =~ /could not be found in the Sat Sites file/
                add_fallout("Cascade not in Sat Sites file", decoded_cid[:site_name], mw_fallout)

              elsif row[summary_detail_index] =~ /AAV is Sprint MW but site type .* is not a MW site type/
                add_fallout("MW AAV with non-MW site type", decoded_cid[:site_name], mw_fallout)

              elsif row[summary_detail_index] =~ /missing its reference circuit ID in the spirent data/ || row[summary_detail_index] =~  /reference circuit (\"\") is not built in CDB/ ||
                    row[summary_detail_index] =~ /reference circuit does not exist (\"\")/
                add_fallout("Donor site not populated in data feed", decoded_cid[:site_name], mw_fallout)

              elsif row[summary_detail_index] =~ /reference circuit (.*) is not built in CDB/ || row[summary_detail_index] =~ /reference circuit does not exist (.*)/
                add_fallout("Donor site not in CDB", decoded_cid[:site_name], mw_fallout)

              else
                add_fallout("Other", decoded_cid[:site_name], mw_fallout)
              end
            end
          else
            puts "build result not recognised (#{row[summary_index]}) -- skipping"
          end
        end
      end      
    end

    output = []

    total = total_successful + total_fallout
    total_mw = total_mw_successful + total_mw_fallout

    output << ["Status", "Count", "Percent of total"]
    output << ["Successful", total_successful, "#{(((1.0 * total_successful) / total) * 100).round}%"]
    output << ["Fallout", total_fallout, "#{(((1.0 * total_fallout) / total) * 100).round}%"]
    output << []

    output << ["MW successful", total_mw_successful, "#{(((1.0 * total_mw_successful) / total_mw) * 100).round}%"]
    output << ["Fallout", total_mw_fallout, "#{(((1.0 * total_mw_fallout) / total_mw) * 100).round}%"]
    output << []
    output << []


    output << ["OEM", "Success count", "Percent of total successes", "Fallout count", "Percent of total fallout", "MW success count", "Percent of total MW successes",
              "MW fallout count", "Percent of total MW fallout"]
    oems = (oems_successful.keys + oems_fallout.keys).uniq
    oems.each do |oem|
      oems_successful[oem] ? successes = oems_successful[oem] : successes = 0
      oems_fallout[oem] ? fallouts = oems_fallout[oem] : fallouts = 0

      oems_mw_successful[oem] ? mw_successes = oems_mw_successful[oem] : mw_successes = 0
      oems_mw_fallout[oem] ? mw_fallouts = oems_mw_fallout[oem] : mw_fallouts = 0


      output << [oem, successes, "#{(((1.0 * successes) / total_successful) * 100).round(1)}%", fallouts, "#{(((1.0 * fallouts) / total_fallout) * 100).round(1)}%",
                mw_successes, "#{(((1.0 * mw_successes) / total_mw_successful) * 100).round(1)}%", mw_fallouts, "#{(((1.0 * mw_fallouts) / total_mw_fallout) * 100).round(1)}%"]
    end
    output << []

    output << ["AAV", "Success count", "Percent of total successes", "Fallout count", "Percent of total fallout"]
    aavs = (aavs_successful.keys + aavs_fallout.keys).uniq
    aavs.each do |aav|
      aavs_successful[aav] ? successes = aavs_successful[aav] : successes = 0
      aavs_fallout[aav] ? fallouts = aavs_fallout[aav] : fallouts = 0

      output << [aav, successes, "#{(((1.0 * successes) / total_successful) * 100).round(2)}%", fallouts, "#{(((1.0 * fallouts) / total_fallout) * 100).round(2)}%"]
    end
    output << []

    output << ["Fallout type", "Count", "Percent of total fallout", "MW count", "Percent of total MW fallout"]
    @fallout_types.each do |type, count|
      @mw_fallout_types[type] ? mw_fallout = @mw_fallout_types[type] : mw_fallout = 0
      output << [type, count, "#{(((1.0 * count) / total_fallout) * 100).round(1)}%", mw_fallout, "#{(((1.0 * mw_fallout) / total_mw_fallout) * 100).round(1)}%"]
    end
    output << []

    output << ["Fallout Cascades"]
    size = 0
    @fallout_cascades.each do |type, cascades|
      @fallout_cascades[type] = cascades.uniq!
      size = cascades.size unless size >= cascades.size
    end

    mw_fallout_cascades.uniq!

    size = mw_fallout_cascades.size if mw_fallout_cascades.size > size

    types = @fallout_cascades.keys

    output << types + ["", "MW fallout cascades"]
    output << types.collect{ |type| @fallout_cascades[type].size} + ["", mw_fallout_cascades.size]
    (0...size).each do |index|
      output << types.collect{ |type| @fallout_cascades[type][index] ? @fallout_cascades[type][index] : "" } + ["", mw_fallout_cascades[index]]
    end


    if outfile
      puts "Writing to #{outfile}..."

      CSV.open(outfile, "w") do |csv|
        output.each do |row|
          csv << row
        end

        csv << []
      end
    else
      puts "writing to screen..."
      
      output.each do |row|
        puts row.join(", ")
      end
    end

    puts 'Complete'
  end

  def add_fallout(type, cascade, mw=false)
    @fallout_types[type] ? @fallout_types[type] += 1 : @fallout_types[type] = 1
    @mw_fallout_types[type] ? @mw_fallout_types[type] += 1 : @mw_fallout_types[type] = 1 if mw
    @fallout_cascades[type] ? @fallout_cascades[type] << cascade : @fallout_cascades[type] = [cascade]
  end
end