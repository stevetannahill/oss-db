module CoreSite_55SouthMarket


  #The following is default site seeding info

  #Nodes Specific ONLY to this Site
  # Node Structure: (NOTE: Key may not be the same as node type
  # so don't use it as a shortcut, just most of the time it will be the same
  # the node's :type)
  # "" => {
  #    :type => "",
  #    :vendor => "",
  #    :make_model => "",
  #    :description => "",
  #    :nm_connection => {:name => "", nm_type => ""},
  #    :ports => [
  #      {:name => "", :phy_type => ""},
  #    ]
  # },
  NODE = {
    "N_MLXe8" => {
      :type => "Brocade MLXe8",
      :vendor => "Brocade",
      :make_model => "Brocade MLXe8",
      :description => "Brocade MLXe8",
      :nm_connection => {:name => "CoreSite SolarWinds", :nm_type => "SolarWinds"},
      :ports => [
        {:name => "1/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/8", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/9", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/10", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "7/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "7/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "7/3", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "8/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "8/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "8/3", :phy_type => "10GigE LR; 1310nm; SMF"},
      ]
    },
  }

  #This submodule is a specific site for this site
  # EACH Site MUST have a NODE_INSTANCE hash of nodes and
  # a PORT_CONNECTIONS array of internal connections
  module Primary
    NODE_INSTANCE = {
      "N_MLXe8_1" => {
        :node => CoreSite_55SouthMarket::NODE["N_MLXe8"], :id => 1,
        :identifier => ".IDC14.IDF.MLX8.", :id_length => 2,
        :ip_suffix => { :primary => "41", :secondary => nil, :gateway => "1", :loopback => "83" },
        :enni_connections => []
       },
       "N_MLXe8_2" => {
        :node => CoreSite_55SouthMarket::NODE["N_MLXe8"], :id => 2,
        :identifier => ".IDC14.IDF.MLX8.", :id_length => 2,
        :ip_suffix => { :primary => "42", :secondary => nil, :gateway => "1", :loopback => "84" },
        :enni_connections => []
       },
       #IDC02.MDF
       "N_CES_2048_IDC02_MDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".IDC02.MDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "90", :secondary => nil, :gateway => "1", :loopback => "87" },
        :enni_connections => []
       },
       "N_CES_2048_IDC02_MDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".IDC02.MDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "91", :secondary => nil, :gateway => "1", :loopback => "88" },
        :enni_connections => []
       },
       #IDC10.IDF
       "N_CES_2048_IDC10_IDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".IDC10.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "92", :secondary => nil, :gateway => "1", :loopback => "89" },
        :enni_connections => []
       },
       "N_CES_2048_IDC10_IDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".IDC10.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "93", :secondary => nil, :gateway => "1", :loopback => "90" },
        :enni_connections => []
       },
       #IDC14.IDF
       "N_CES_2048_IDC14_IDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".IDC14.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "94", :secondary => nil, :gateway => "1", :loopback => "91" },
        :enni_connections => []
       },
       "N_CES_2048_IDC14_IDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".IDC14.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "95", :secondary => nil, :gateway => "1", :loopback => "92" },
        :enni_connections => []
       },
       #IDC02.IDF
       "N_CES_2024_IDC02_IDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 1,
        :identifier => ".IDC02.IDF.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "110", :secondary => nil, :gateway => "1", :loopback => "93" },
        :enni_connections => []
       },
       "N_CES_2024_IDC02_IDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 2,
        :identifier => ".IDC02.IDF.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "111", :secondary => nil, :gateway => "1", :loopback => "94" },
        :enni_connections => []
       },
       #IDC16.IDF
       "N_CES_2024_IDC16_IDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 1,
        :identifier => ".IDC16.IDF.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "112", :secondary => nil, :gateway => "1", :loopback => "85" },
        :enni_connections => []
       },
       "N_CES_2024_IDC16_IDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 2,
        :identifier => ".IDC16.IDF.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "113", :secondary => nil, :gateway => "1", :loopback => "86" },
        :enni_connections => []
       },
    }

    # [ [ [node_type, port], [node_type, port] ], [ [node_type, port], [node_type, port] ], ... ]
    # | | | Single Port   |  | Conected Port | |                                                |
    # | |Single connection (only need one way) |                                                |
    # | List of all connections                                                                 |

    PORT_CONNECTIONS = [
      #IDC02_MDF
      [[NODE_INSTANCE["N_MLXe8_1"], "1/5"], [NODE_INSTANCE["N_CES_2048_IDC02_MDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_1"], "1/6"], [NODE_INSTANCE["N_CES_2048_IDC02_MDF_2"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "1/5"], [NODE_INSTANCE["N_CES_2048_IDC02_MDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "1/6"], [NODE_INSTANCE["N_CES_2048_IDC02_MDF_2"], "1/2"]],
      #IDC10_IDF
      [[NODE_INSTANCE["N_MLXe8_1"], "1/7"], [NODE_INSTANCE["N_CES_2048_IDC10_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_1"], "1/8"], [NODE_INSTANCE["N_CES_2048_IDC10_IDF_2"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "1/7"], [NODE_INSTANCE["N_CES_2048_IDC10_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "1/8"], [NODE_INSTANCE["N_CES_2048_IDC10_IDF_2"], "1/2"]],
      #IDC14_IDF
      [[NODE_INSTANCE["N_MLXe8_1"], "1/9"], [NODE_INSTANCE["N_CES_2048_IDC14_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_1"], "1/10"], [NODE_INSTANCE["N_CES_2048_IDC14_IDF_2"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "1/9"], [NODE_INSTANCE["N_CES_2048_IDC14_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "1/10"], [NODE_INSTANCE["N_CES_2048_IDC14_IDF_2"], "1/2"]],
      #IDC02_IDF
      [[NODE_INSTANCE["N_MLXe8_1"], "1/1"], [NODE_INSTANCE["N_CES_2024_IDC02_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_1"], "1/2"], [NODE_INSTANCE["N_CES_2024_IDC02_IDF_2"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "1/1"], [NODE_INSTANCE["N_CES_2024_IDC02_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "1/2"], [NODE_INSTANCE["N_CES_2024_IDC02_IDF_2"], "1/2"]],
      #IDC16_IDF
      [[NODE_INSTANCE["N_MLXe8_1"], "1/3"], [NODE_INSTANCE["N_CES_2024_IDC16_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_1"], "1/4"], [NODE_INSTANCE["N_CES_2024_IDC16_IDF_2"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "1/3"], [NODE_INSTANCE["N_CES_2024_IDC16_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "1/4"], [NODE_INSTANCE["N_CES_2024_IDC16_IDF_2"], "1/2"]],
      #Interconnect
      [[NODE_INSTANCE["N_MLXe8_1"], "7/3"], [NODE_INSTANCE["N_MLXe8_2"], "7/3"]],
      [[NODE_INSTANCE["N_MLXe8_1"], "8/3"], [NODE_INSTANCE["N_MLXe8_2"], "8/3"]],
    ]
  end

  #Hash of all sites (a.k.a. submodules) for this Site type.
  SITES = {
    "Primary" => Primary
  }

  #Inter Site Connections
  # [ [ [ Site String, Node Instance Name, Port ], [ Site String, Node Instance Name, Port ], ... ]
  # | | |    Single Port                            |  |         Connected Port                    |      |
  # | |         Single Connection (Only need one way)                                              |      |
  # |    List of all connection between all sites                                                     |
  #
  CONNECTIONS = [
  ]

end
