module CoreSite_1656McCarthy


  #The following is default site seeding info

  #Nodes Specific ONLY to this Site
  # Node Structure: (NOTE: Key may not be the same as node type
  # so don't use it as a shortcut, just most of the time it will be the same
  # the node's :type)
  # "" => {
  #    :type => "",
  #    :vendor => "",
  #    :make_model => "",
  #    :description => "",
  #    :nm_connection => {:name => "", nm_type => ""},
  #    :ports => [
  #      {:name => "", :phy_type => ""},
  #    ]
  # },
  NODE = {
    "N_MLXe8" => {
      :type => "Brocade MLXe8",
      :vendor => "Brocade",
      :make_model => "Brocade MLXe8",
      :description => "Brocade MLXe8",
      :nm_connection => {:name => "CoreSite SolarWinds", :nm_type => "SolarWinds"},
      :ports => [
        {:name => "1/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "7/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "7/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "8/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "8/2", :phy_type => "10GigE LR; 1310nm; SMF"},
      ]
    },
  }

  #This submodule is a specific site for this site
  # EACH Site MUST have a NODE_INSTANCE hash of nodes and
  # a PORT_CONNECTIONS array of internal connections
  module Primary
    NODE_INSTANCE = {
      "N_MLXe8_1" => {
        :node => CoreSite_1656McCarthy::NODE["N_MLXe8"], :id => 1,
        :identifier => ".IDC01.MDF.MLX8.", :id_length => 2,
        :ip_suffix => { :primary => "21", :secondary => nil, :gateway => "1", :loopback => "103" },
        :enni_connections => []
       },
       "N_MLXe8_2" => {
        :node => CoreSite_1656McCarthy::NODE["N_MLXe8"], :id => 2,
        :identifier => ".IDC01.MDF.MLX8.", :id_length => 2,
        :ip_suffix => { :primary => "22", :secondary => nil, :gateway => "1", :loopback => "104" },
        :enni_connections => []
       },
       #IDC01.MDF
       "N_CES_2048_IDC01_MDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".IDC01.MDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "40", :secondary => nil, :gateway => "1", :loopback => "105" },
        :enni_connections => []
       },
       "N_CES_2048_IDC01_MDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".IDC01.MDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "41", :secondary => nil, :gateway => "1", :loopback => "106" },
        :enni_connections => []
       },
    }

    # [ [ [node_type, port], [node_type, port] ], [ [node_type, port], [node_type, port] ], ... ]
    # | | | Single Port   |  | Conected Port | |                                                |
    # | |Single connection (only need one way) |                                                |
    # | List of all connections                                                                 |

    PORT_CONNECTIONS = [
      #IDC01_MDF
      [[NODE_INSTANCE["N_MLXe8_1"], "1/1"], [NODE_INSTANCE["N_CES_2048_IDC01_MDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_1"], "1/2"], [NODE_INSTANCE["N_CES_2048_IDC01_MDF_2"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "1/1"], [NODE_INSTANCE["N_CES_2048_IDC01_MDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "1/2"], [NODE_INSTANCE["N_CES_2048_IDC01_MDF_2"], "1/2"]],
      #Interconnect
      [[NODE_INSTANCE["N_MLXe8_1"], "7/2"], [NODE_INSTANCE["N_MLXe8_2"], "7/2"]],
      [[NODE_INSTANCE["N_MLXe8_1"], "8/2"], [NODE_INSTANCE["N_MLXe8_2"], "8/2"]],
    ]
  end

  #Hash of all sites (a.k.a. submodules) for this Site type.
  SITES = {
    "Primary" => Primary
  }

  #Inter Site Connections
  # [ [ [ Site String, Node Instance Name, Port ], [ Site String, Node Instance Name, Port ], ... ]
  # | | |    Single Port                            |  |         Connected Port                    |      |
  # | |         Single Connection (Only need one way)                                              |      |
  # |    List of all connection between all sites                                                     |
  #
  CONNECTIONS = [
  ]

end
