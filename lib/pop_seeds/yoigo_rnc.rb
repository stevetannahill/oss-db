module Yoigo_RNC

  #The following is default site seeding info

  #Nodes Specific ONLY to this Site
  # Node Structure: (NOTE: Key may not be the same as node type
  # so don't use it as a shortcut, just most of the time it will be the same
  # the node's :type)
  # "" => {
  #    :type => "",
  #    :vendor => "",
  #    :make_model => "",
  #    :description => "",
  #    :nm_connection => {:name => "", nm_type => ""},
  #    :ports => [
  #      {:name => "", :phy_type => ""},
  #    ]
  # },
  NODE = {
    "N_Tellabs_8660" => {
      :type => "Tellabs 8660",
      :vendor => "Tellabs",
      :make_model => "8660",
      :description => "Tellabs 8660",
      :nm_connection => {:name => "Tellabs_EMS_1", :nm_type => "Tellabs EMS"},
      :ports => [
        {:name => "ge2/0/0", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge2/0/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge2/0/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge2/0/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge2/0/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge2/0/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge2/0/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge2/0/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge2/0/8", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge10/0/0", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge10/0/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge10/0/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge10/0/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge10/0/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge10/0/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge10/0/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge10/0/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge11/0/0", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge11/0/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge11/0/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge11/0/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge11/0/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge11/0/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge11/0/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge11/0/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge12/0/0", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge12/0/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge12/0/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge12/0/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge12/0/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge12/0/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge12/0/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge12/0/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge12/1/0", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge12/1/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge12/1/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge12/1/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge12/1/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge12/1/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge12/1/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge12/1/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge13/0/0", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge13/0/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge13/0/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge13/0/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge13/0/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge13/0/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge13/0/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge13/0/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge13/1/0", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge13/1/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge13/1/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge13/1/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge13/1/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge13/1/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge13/1/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge13/1/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        
        {:name => "A console", :phy_type => "Other"},
        {:name => "B console", :phy_type => "Other"},
        {:name => "A MGMT", :phy_type => "10/100/1000 Base T"},
        {:name => "B MGMT", :phy_type => "10/100/1000 Base T"}
      ]
    },
    "N_RNC" => {
      :type => "RNC",
      :vendor => "TBD",
      :make_model => "TBD",
      :description => "RNC Node",
      :nm_connection => {:name => "OSS-RC_1", :nm_type => "OSS-RC"},
      :ports => [
        {:name => "3/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "3/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "26/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "26/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        
        {:name => "A console", :phy_type => "Other"},
        {:name => "B console", :phy_type => "Other"},
        {:name => "A MGMT", :phy_type => "10/100/1000 Base T"},
        {:name => "B MGMT", :phy_type => "10/100/1000 Base T"}
      ]
    },
    
    "N_BSC" => {
      :type => "BSC",
      :vendor => "TBD",
      :make_model => "TBD",
      :description => "BSC Node",
      :nm_connection => {:name => "OSS-RC_1", :nm_type => "OSS-RC"},
      :ports => [
        {:name => "1/11", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/12", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/13", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/11", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/12", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/13", :phy_type => "1000Base-LX; 1310nm; SMF"},

        {:name => "A console", :phy_type => "Other"},
        {:name => "B console", :phy_type => "Other"},
        {:name => "A MGMT", :phy_type => "10/100/1000 Base T"},
        {:name => "B MGMT", :phy_type => "10/100/1000 Base T"}
      ]
    }
  }

  #This submodule is a specific site for this site
  # EACH Site MUST have a NODE_INSTANCE hash of nodes and
  # a PORT_CONNECTIONS array of internal connections
  module Primary
    NODE_INSTANCE = {
      "N_Tellabs_8660_1" => {
        :node => Yoigo_RNC::NODE["N_Tellabs_8660"], :id => 1,
        :identifier => "_", :id_length => 3,
        :ip_suffix => { :primary => "130", :secondary => "131", :gateway => "161" },
        :enni_connections => [
          { :protection_type => "unprotected", :physical_medium => "10/100/1000 Base T",
            :ether_type => "0x8100", :auto_negotiate => true, :port_enap_type => "NULL",
            :port_connections => ["ge2/0/7"] }
        ]
       },
       "N_Tellabs_8660_2" => {
        :node => Yoigo_RNC::NODE["N_Tellabs_8660"], :id => 2,
        :identifier => "_", :id_length => 3,
        :ip_suffix => { :primary => "132", :secondary => "133", :gateway => "161" },
        :enni_connections => [
          { :protection_type => "unprotected", :physical_medium => "10/100/1000 Base T",
            :ether_type => "0x8100", :auto_negotiate => true, :port_enap_type => "NULL",
            :port_connections => ["ge2/0/7"] }
        ]
       },
       "N_RNC_1" => {
        :node => Yoigo_RNC::NODE["N_RNC"], :id => 1,
        :identifier => "_RNC_", :id_length => 3,
        :ip_suffix => { :primary => "170", :secondary => "171", :gateway => "161" },
        :enni_connections => nil
       },
       "N_MINI-LINK_1" => {
        :node => CommonSite::NODE["N_MINI-LINK"], :id => 1,
        :identifier => "_ML_", :id_length => 3,
        :ip_suffix => { :primary => nil, :secondary => nil, :gateway => "161" },
        :enni_connections => nil
       },
       "N_BSC_1" => {
        :node => Yoigo_RNC::NODE["N_BSC"], :id => 1,
        :identifier => "_BSC_", :id_length => 3,
        :ip_suffix => { :primary => "174", :secondary => "175", :gateway => "161" },
        :enni_connections => nil
       },
       "N_BV-3000_1" => {
        :node => CommonSite::NODE["N_BV-3000"], :id => 1,
        :identifier => "_BV3_", :id_length => 3,
        :ip_suffix => { :primary => "182", :secondary => nil, :gateway => "161" },
        :enni_connections => nil
       }
    }

    # [ [ [node_type, port], [node_type, port] ], [ [node_type, port], [node_type, port] ], ... ]
    # | | | Single Port   |  | Conected Port | |                                                |
    # | |Single connection (only need one way) |                                                |
    # | List of all connections                                                                 |

    PORT_CONNECTIONS = [
      [[NODE_INSTANCE["N_Tellabs_8660_1"], "ge2/0/1"], [NODE_INSTANCE["N_Tellabs_8660_2"], "ge2/0/1"]],
      [[NODE_INSTANCE["N_Tellabs_8660_1"], "ge13/0/1"], [NODE_INSTANCE["N_Tellabs_8660_2"], "ge13/0/1"]],


      [[NODE_INSTANCE["N_Tellabs_8660_1"], "ge10/0/6"], [NODE_INSTANCE["N_RNC_1"], "3/1"]],
      [[NODE_INSTANCE["N_Tellabs_8660_2"], "ge12/0/7"], [NODE_INSTANCE["N_RNC_1"], "26/1"]],

      [[NODE_INSTANCE["N_Tellabs_8660_1"], "ge12/0/6"], [NODE_INSTANCE["N_MINI-LINK_1"], "11/1"]],
      [[NODE_INSTANCE["N_Tellabs_8660_2"], "ge2/0/4"], [NODE_INSTANCE["N_MINI-LINK_1"], "11/2"]],

      [[NODE_INSTANCE["N_Tellabs_8660_1"], "ge2/0/3"], [NODE_INSTANCE["N_BSC_1"], "1/12"]],
      [[NODE_INSTANCE["N_Tellabs_8660_1"], "ge10/0/4"], [NODE_INSTANCE["N_BSC_1"], "1/11"]],
      [[NODE_INSTANCE["N_Tellabs_8660_1"], "ge11/0/4"], [NODE_INSTANCE["N_BSC_1"], "1/13"]],

      [[NODE_INSTANCE["N_Tellabs_8660_2"], "ge11/0/3"], [NODE_INSTANCE["N_BSC_1"], "2/13"]],
      [[NODE_INSTANCE["N_Tellabs_8660_2"], "ge11/0/4"], [NODE_INSTANCE["N_BSC_1"], "2/12"]],
      [[NODE_INSTANCE["N_Tellabs_8660_2"], "ge12/0/4"], [NODE_INSTANCE["N_BSC_1"], "2/11"]],

      [[NODE_INSTANCE["N_Tellabs_8660_1"], "ge2/0/8"], [NODE_INSTANCE["N_BV-3000_1"], "TEST1"]],
      [[NODE_INSTANCE["N_Tellabs_8660_2"], "ge2/0/8"], [NODE_INSTANCE["N_BV-3000_1"], "TEST2"]]
    ]
  end

  #Hash of all sites (a.k.a. submodules) for this Site type.
  SITES = {
    "Primary" => Primary
  }

  #Inter Site Connections
  # [ [ [ Site String, Node Instance Name, Port ], [ Site String, Node Instance Name, Port ], ... ]
  # | | |    Single Port                            |  |         Connected Port                    |      |
  # | |         Single Connection (Only need one way)                                              |      |
  # |    List of all connection between all sites                                                     |
  #
  CONNECTIONS = [
  ]

end
