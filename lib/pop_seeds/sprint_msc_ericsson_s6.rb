module SprintMSC_Ericsson_S6

  #The following is default site seeding info

  #Nodes Specific ONLY to this Site
  # Node Structure: (NOTE: Key may not be the same as node type
  # so don't use it as a shortcut, just most of the time it will be the same
  # the node's :type)
  # "" => {
  #    :type => "",
  #    :vendor => "",
  #    :make_model => "",
  #    :description => "",
  #    :nm_connection => {:name => "", nm_type => ""},
  #    :ports => [
  #      {:name => "", :phy_type => ""},
  #    ]
  # },
  NODE = {
    "N_SE1200" => {
      :type => "SE1200",
      :vendor => "Ericsson",
      :make_model => "SE1200 - 3HE05867AA",
      :description => "Ericsson SE1200",
      :nm_connection => {:name => "Sprint NEM #1", :nm_type => "Sprint NEM"},
      :ports => [
        { :name => "1/1" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/10" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/11" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/12" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/13" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/14" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/15" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/16" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/17" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/18" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/19" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/2" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/20" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/21" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/22" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/23" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/24" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/25" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/26" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/27" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/28" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/29" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/3" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/30" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/31" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/32" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/33" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/34" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/35" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/36" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/37" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/38" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/39" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/4" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/40" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/41" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/42" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/43" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/44" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/45" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/46" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/47" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/48" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/49" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/5" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/50" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/51" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/52" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/53" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/54" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/55" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/56" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/57" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/58" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/59" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/6" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/60" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/62" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/7" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/8" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/8" , :phy_type => "10/100/1000 Base T" },
        { :name => "1/9" , :phy_type => "10/100/1000 Base T" },
        { :name => "10/1" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "10/10" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "10/11" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "10/12" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "10/13" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "10/14" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "10/15" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "10/16" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "10/17" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "10/18" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "10/19" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "10/2" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "10/20" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "10/3" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "10/4" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "10/5" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "10/6" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "10/7" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "10/8" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "10/9" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "11/1" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "11/10" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "11/11" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "11/12" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "11/13" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "11/14" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "11/15" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "11/16" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "11/17" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "11/18" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "11/19" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "11/2" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "11/20" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "11/3" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "11/4" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "11/5" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "11/6" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "11/7" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "11/8" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "11/9" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "12/1" , :phy_type => "10GigE SR; 1310nm; SMF" },
        { :name => "12/1" , :phy_type => "10GigE SR; 1310nm; SMF" },
        { :name => "12/1" , :phy_type => "10GigE SR; 850nm; MMF" },
        { :name => "12/2" , :phy_type => "10GigE SR; 850nm; MMF" },
        { :name => "12/3" , :phy_type => "10GigE SR; 850nm; MMF" },
        { :name => "12/4" , :phy_type => "10GigE SR; 850nm; MMF" },
        { :name => "13/1" , :phy_type => "10GigE SR; 850nm; MMF" },
        { :name => "13/2" , :phy_type => "10GigE SR; 850nm; MMF" },
        { :name => "13/3" , :phy_type => "10GigE SR; 1310nm; SMF" },
        { :name => "13/3" , :phy_type => "10GigE SR; 850nm; MMF" },
        { :name => "13/4" , :phy_type => "10GigE SR; 850nm; MMF" },
        { :name => "14/1" , :phy_type => "10GigE SR; 1310nm; SMF" },
        { :name => "14/1" , :phy_type => "10GigE SR; 850nm; MMF" },
        { :name => "14/2" , :phy_type => "10GigE SR; 850nm; MMF" },
        { :name => "14/3" , :phy_type => "10GigE SR; 850nm; MMF" },
        { :name => "14/4" , :phy_type => "10GigE SR; 850nm; MMF" },
        { :name => "2/1" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "2/10" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "2/11" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "2/12" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "2/13" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "2/14" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "2/15" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "2/16" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "2/17" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "2/18" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "2/19" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "2/2" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "2/20" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "2/3" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "2/4" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "2/5" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "2/6" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "2/7" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "2/8" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "2/9" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "3/1" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "3/10" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "3/11" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "3/12" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "3/13" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "3/14" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "3/15" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "3/16" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "3/17" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "3/18" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "3/19" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "3/2" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "3/20" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "3/3" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "3/4" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "3/5" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "3/6" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "3/7" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "3/8" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "3/9" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "4/1" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "4/10" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "4/11" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "4/12" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "4/13" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "4/14" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "4/15" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "4/16" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "4/17" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "4/18" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "4/19" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "4/2" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "4/20" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "4/3" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "4/4" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "4/5" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "4/6" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "4/7" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "4/8" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "4/9" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "5/1" , :phy_type => "10GigE SR; 1310nm; SMF" },
        { :name => "5/1" , :phy_type => "10GigE SR; 1310nm; SMF" },
        { :name => "5/2" , :phy_type => "10GigE SR; 850nm; MMF" },
        { :name => "5/3" , :phy_type => "10GigE SR; 850nm; MMF" },
        { :name => "5/4" , :phy_type => "10GigE SR; 850nm; MMF" },
        { :name => "6/1" , :phy_type => "10GigE SR; 1310nm; SMF" },
        { :name => "6/1" , :phy_type => "10GigE SR; 850nm; MMF" },
        { :name => "6/2" , :phy_type => "10GigE SR; 850nm; MMF" },
        { :name => "6/3" , :phy_type => "10GigE SR; 850nm; MMF" },
        { :name => "6/4" , :phy_type => "10GigE SR; 850nm; MMF" },
        { :name => "9/1" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "9/10" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "9/11" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "9/12" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "9/13" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "9/14" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "9/15" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "9/16" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "9/17" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "9/18" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "9/19" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "9/2" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "9/20" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "9/3" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "9/4" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "9/5" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "9/6" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "9/7" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "9/8" , :phy_type => "1000Base-SX; 850nm; MMF" },
        { :name => "9/9" , :phy_type => "1000Base-SX; 850nm; MMF" }, 

        { :name => "Routing Instance #1", :phy_type => "L3 Handoff"},
        { :name => "A console", :phy_type => "Other"},
        { :name => "B console", :phy_type => "Other"},
        { :name => "CPM/SF A MGMT", :phy_type => "10/100/1000 Base T"},
        { :name => "CPM/SF B MGMT", :phy_type => "10/100/1000 Base T"} ]

    }
  }

  #This submodule is a specific site for this site
  # EACH Site MUST have a NODE_INSTANCE hash of nodes and
  # a PORT_CONNECTIONS array of internal connections
  module Primary
    NODE_INSTANCE = {
      "N_SE1200_1" => {
        :node => SprintMSC_Ericsson_S6::NODE["N_SE1200"], :id => 1,
        :identifier => "-IPA", :id_length => 2,
        :ip_suffix => { :primary => "170", :secondary => "171", :gateway => "161" },
        :enni_connections => [
          { :protection_type => "unprotected", :physical_medium => "10/100/1000 Base T",
            :ether_type => "0x8100", :auto_negotiate => true, :port_enap_type => "QINQ",
            :port_connections => ["6/2"], :member_handle => "IPA01-TEST1" },
          { :protection_type => "unprotected", :physical_medium => "L3 Handoff", :demarc_icon => "router",
            :ether_type => "Other", :auto_negotiate => false, :port_enap_type => "NULL",
            :port_connections => ["Routing Instance #1"], :member_handle => "IPA01-VRF-01" }
        ]
       },
       "N_SE1200_2" => {
        :node => SprintMSC_Ericsson_S6::NODE["N_SE1200"], :id => 2,
        :identifier => "-IPA", :id_length => 2,
        :ip_suffix => { :primary => "172", :secondary => "173", :gateway => "161" },
        :enni_connections => [
          { :protection_type => "unprotected", :physical_medium => "10/100/1000 Base T",
            :ether_type => "0x8100", :auto_negotiate => true, :port_enap_type => "QINQ",
            :port_connections => ["6/2"], :member_handle => "IPA02-TEST1" },
          { :protection_type => "unprotected", :physical_medium => "L3 Handoff", :demarc_icon => "router",
            :ether_type => "Other", :auto_negotiate => false, :port_enap_type => "NULL",
            :port_connections => ["Routing Instance #1"], :member_handle => "IPA02-VRF-01" }
        ]
       },
       "N_QoS-Scope-1G_1" => {
        :node => CommonSite::NODE["N_QoS-Scope-1G"], :id => 1,
        :identifier => "QSCOPE", :id_length => 2,
        :ip_suffix => { :primary => "182", :secondary => nil, :gateway => "161" },
        :enni_connections => nil
       }
    }

    # [ [ [node_type, port], [node_type, port] ], [ [node_type, port], [node_type, port] ], ... ]
    # | | | Single Port   |  | Conected Port | |                                                |
    # | |Single connection (only need one way) |                                                |
    # | List of all connections                                                                 |

    PORT_CONNECTIONS = [
#      [[NODE_INSTANCE["N_SE1200_1"], "1/1/1"], [NODE_INSTANCE["N_SE1200_2"], "1/1/1"]],
#      [[NODE_INSTANCE["N_SE1200_1"], "2/1/1"], [NODE_INSTANCE["N_SE1200_2"], "2/1/1"]],
#      [[NODE_INSTANCE["N_SE1200_1"], "3/1/1"], [NODE_INSTANCE["N_SE1200_2"], "3/1/1"]],
#      [[NODE_INSTANCE["N_SE1200_1"], "4/1/1"], [NODE_INSTANCE["N_SE1200_2"], "4/1/1"]],
#      [[NODE_INSTANCE["N_SE1200_1"], "1/2/1"], [NODE_INSTANCE["N_QoS-Scope-1G_1"], "TEST1"]],
#      [[NODE_INSTANCE["N_SE1200_2"], "1/2/1"], [NODE_INSTANCE["N_QoS-Scope-1G_1"], "TEST2"]],
#      [[NODE_INSTANCE["N_SE1200_1"], "1/2/2"], [NODE_INSTANCE["N_QoS-Scope-1G_1"], "TEST3"]],
#      [[NODE_INSTANCE["N_SE1200_2"], "1/2/2"], [NODE_INSTANCE["N_QoS-Scope-1G_1"], "TEST4"]]

      [[NODE_INSTANCE["N_SE1200_1"], "12/3"], [NODE_INSTANCE["N_SE1200_2"], "12/3"]],
      [[NODE_INSTANCE["N_SE1200_1"], "5/3"],  [NODE_INSTANCE["N_SE1200_2"], "5/3"]],
      [[NODE_INSTANCE["N_SE1200_1"], "6/2"],  [NODE_INSTANCE["N_QoS-Scope-1G_1"], "TEST1"]],
      [[NODE_INSTANCE["N_SE1200_2"], "6/2"],  [NODE_INSTANCE["N_QoS-Scope-1G_1"], "TEST2"]],
      [[NODE_INSTANCE["N_SE1200_1"], "13/4"], [NODE_INSTANCE["N_QoS-Scope-1G_1"], "TEST3"]],
      [[NODE_INSTANCE["N_SE1200_2"], "13/4"], [NODE_INSTANCE["N_QoS-Scope-1G_1"], "TEST4"]]

    ]
  end

  #Hash of all sites (a.k.a. submodules) for this Site type.
  SITES = {
    "Primary" => Primary
  }

  #Inter Site Connections
  # [ [ [ Site String, Node Instance Name, Port ], [ Site String, Node Instance Name, Port ], ... ]
  # | | |    Single Port                            |  |         Connected Port                    |      |
  # | |         Single Connection (Only need one way)                                              |      |
  # |    List of all connection between all sites                                                     |
  #
  CONNECTIONS = [
  ]

end
