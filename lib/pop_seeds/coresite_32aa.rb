module CoreSite_32AA


  #The following is default site seeding info

  #Nodes Specific ONLY to this Site
  # Node Structure: (NOTE: Key may not be the same as node type
  # so don't use it as a shortcut, just most of the time it will be the same
  # the node's :type)
  # "" => {
  #    :type => "",
  #    :vendor => "",
  #    :make_model => "",
  #    :description => "",
  #    :nm_connection => {:name => "", nm_type => ""},
  #    :ports => [
  #      {:name => "", :phy_type => ""},
  #    ]
  # },
  NODE = {
    "N_MLXe16" => {
      :type => "Brocade MLXe16",
      :vendor => "Brocade",
      :make_model => "Brocade MLXe16",
      :description => "Brocade MLXe16",
      :nm_connection => {:name => "CoreSite SolarWinds", :nm_type => "SolarWinds"},
      :ports => [
        {:name => "1/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "2/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "15/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "15/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "16/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "16/5", :phy_type => "10GigE LR; 1310nm; SMF"},
      ]
    },
  }

  #This submodule is a specific site for this site
  # EACH Site MUST have a NODE_INSTANCE hash of nodes and
  # a PORT_CONNECTIONS array of internal connections
  module Primary
    NODE_INSTANCE = {
      "N_MLXe16_1" => {
        :node => CoreSite_32AA::NODE["N_MLXe16"], :id => 1,
        :identifier => ".ICD07.MDF.MLX16.", :id_length => 2,
        :ip_suffix => { :primary => "80", :secondary => nil, :gateway => "1", :loopback => "1" },
        :enni_connections => []
       },
       "N_MLXe16_2" => {
        :node => CoreSite_32AA::NODE["N_MLXe16"], :id => 2,
        :identifier => ".IDC07.MDF.MLX16.", :id_length => 2,
        :ip_suffix => { :primary => "81", :secondary => nil, :gateway => "1", :loopback => "2" },
        :enni_connections => []
       },
       #HUB24.H2F04
       "N_CES_2048_HUB24_H2F04_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".HUB24.H2F04.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "84", :secondary => nil, :gateway => "1", :loopback => "5" },
        :enni_connections => []
       },
       "N_CES_2048_HUB24_H2F04_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".HUB24.H2F04.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "85", :secondary => nil, :gateway => "1", :loopback => "6" },
        :enni_connections => []
       },
       #IDC07.MDF
       "N_CES_2048_IDC07_MDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".IDC07.MDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "82", :secondary => nil, :gateway => "1", :loopback => "3" },
        :enni_connections => []
       },
       "N_CES_2048_IDC07_MDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".IDC07.MDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "83", :secondary => nil, :gateway => "1", :loopback => "4" },
        :enni_connections => []
       },
    }

    # [ [ [node_type, port], [node_type, port] ], [ [node_type, port], [node_type, port] ], ... ]
    # | | | Single Port   |  | Conected Port | |                                                |
    # | |Single connection (only need one way) |                                                |
    # | List of all connections                                                                 |

    PORT_CONNECTIONS = [
      #HUB24_H2F04
      [[NODE_INSTANCE["N_MLXe16_1"], "1/2"], [NODE_INSTANCE["N_CES_2048_HUB24_H2F04_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "15/2"], [NODE_INSTANCE["N_CES_2048_HUB24_H2F04_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "1/2"], [NODE_INSTANCE["N_CES_2048_HUB24_H2F04_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "15/2"], [NODE_INSTANCE["N_CES_2048_HUB24_H2F04_2"], "1/2"]],
      #IDC07_MDF
      [[NODE_INSTANCE["N_MLXe16_1"], "1/1"], [NODE_INSTANCE["N_CES_2048_IDC07_MDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "15/1"], [NODE_INSTANCE["N_CES_2048_IDC07_MDF_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "1/1"], [NODE_INSTANCE["N_CES_2048_IDC07_MDF_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe16_2"], "15/1"], [NODE_INSTANCE["N_CES_2048_IDC07_MDF_2"], "1/2"]],
      #Interconnect
      [[NODE_INSTANCE["N_MLXe16_1"], "2/1"], [NODE_INSTANCE["N_MLXe16_2"], "2/1"]],
      [[NODE_INSTANCE["N_MLXe16_1"], "16/1"], [NODE_INSTANCE["N_MLXe16_2"], "16/1"]],
    ]
  end

  #Hash of all sites (a.k.a. submodules) for this Site type.
  SITES = {
    "Primary" => Primary
  }

  #Inter Site Connections
  # [ [ [ Site String, Node Instance Name, Port ], [ Site String, Node Instance Name, Port ], ... ]
  # | | |    Single Port                            |  |         Connected Port                    |      |
  # | |         Single Connection (Only need one way)                                              |      |
  # |    List of all connection between all sites                                                     |
  #
  CONNECTIONS = [
  ]

end
