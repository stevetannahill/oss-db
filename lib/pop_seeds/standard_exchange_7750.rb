module StandardExchange7750
  #The following is default site seeding info
  
  #Nodes Specific ONLY to this Site
  # Node Structure: (NOTE: Key may not be the same as node type
  # so don't use it as a shortcut, just most of the time it will be the same
  # the node's :type)
  # "" => {
  #    :type => "",
  #    :vendor => "",
  #    :make_model => "",
  #    :description => "",
  #    :nm_connection => {:name => "", nm_type => ""},
  #    :ports => [
  #      {:name => "", :phy_type => ""},
  #    ]
  # },
  NODE = {
    "N_7750-sr12" => {
      :type => "7750-sr12",
      :vendor => "Alcatel",
      :make_model => "7750-SR12 - 3HE00104AAAC02",
      :description => "Alcatel 7750-SR12 Service Router",
      :nm_connection => {:name => "5620 SAM - Live", :nm_type => "5620Sam"},
      :ports => [
        {:name => "1/1/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/8", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/9", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/10", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/11", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/12", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/13", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/14", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/15", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/16", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/2/1", :phy_type => "1000Base-SX; 850nm; MMF"},
        {:name => "1/2/2", :phy_type => "1000Base-SX; 850nm; MMF"},
        {:name => "1/2/3", :phy_type => "1000Base-SX; 850nm; MMF"},
        {:name => "1/2/13", :phy_type => "10/100/1000 Base T"},
        {:name => "1/2/18", :phy_type => "10/100/1000 Base T"},
        {:name => "1/2/19", :phy_type => "10/100/1000 Base T"},
        {:name => "1/2/20", :phy_type => "10/100/1000 Base T"},
        {:name => "1/2/21", :phy_type => "10/100/1000 Base T"},
        {:name => "1/2/22", :phy_type => "10/100/1000 Base T"},
        {:name => "1/2/23", :phy_type => "10/100/1000 Base T"},
        {:name => "1/2/24", :phy_type => "10/100/1000 Base T"},
        {:name => "3/1/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "3/1/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "3/1/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "3/1/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "3/1/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "3/1/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "3/1/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "3/1/8", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "3/1/9", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "3/1/10", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "3/1/11", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "3/1/12", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "3/1/13", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "3/1/14", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "3/1/15", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "3/1/16", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "3/2/1", :phy_type => "1000Base-SX; 850nm; MMF"},
        {:name => "3/2/2", :phy_type => "1000Base-SX; 850nm; MMF"},
        {:name => "3/2/3", :phy_type => "1000Base-SX; 850nm; MMF"},
        {:name => "3/2/18", :phy_type => "10/100/1000 Base T"},
        {:name => "3/2/19", :phy_type => "10/100/1000 Base T"},
        {:name => "3/2/20", :phy_type => "10/100/1000 Base T"},
        {:name => "3/2/21", :phy_type => "10/100/1000 Base T"},
        {:name => "3/2/22", :phy_type => "10/100/1000 Base T"},
        {:name => "3/2/23", :phy_type => "10/100/1000 Base T"},
        {:name => "3/2/24", :phy_type => "10/100/1000 Base T"},
        {:name => "A console", :phy_type => "Other"},
        {:name => "B console", :phy_type => "Other"},
        {:name => "CPM/SF A MGMT", :phy_type => "10/100/1000 Base T"},
        {:name => "CPM/SF B MGMT", :phy_type => "10/100/1000 Base T"}
      ]
    }
  }

  #This submodule is a specific site for this site
  # EACH Site MUST have a NODE_INSTANCE hash of nodes and
  # a PORT_CONNECTIONS array of internal connections
  module Primary
    #Node Instance Structure
    #{
    # :name => {
    #   :node => ::NODE[""], :id => 1,
    #   :ip_suffix => { :primary => "", :secondary => "", :gateway => "" },
    #   :identifier => "",
    #   :enni_connections => [{
    #     :protection_type => "", :lag_mode => "", :lag_id => 199, :physical_medium => "",
    #     :ether_type => "", :auto_negotiate => true, :port_enap_type => "",
    #     :port_connections => ["", ""] },]
    # },
    #]
    NODE_INSTANCE = {
      "N_7750-sr12_1" => {
        :node => StandardExchange7750::NODE["N_7750-sr12"], :id => 1,
        :identifier => "ALU",
        :ip_suffix => { :primary => "170", :secondary => "171", :gateway => "161" },
        :enni_connections => [{
          :protection_type => "MCard-LAG", :lag_mode => "Active/Standby", :lag_id => 199, :physical_medium => "10/100/1000 Base T",
          :ether_type => "0x8100", :auto_negotiate => true, :port_enap_type => "QINQ",
          :port_connections => ["1/2/23", "3/2/23"]}]
      },
      "N_BV-3000_1" => {
        :node => CommonSite::NODE["N_BV-3000"], :id => 1,
        :identifier => "BV3",
        :ip_suffix => { :primary => "182", :secondary => nil, :gateway => "161" },
        :enni_connections => nil
      },
      "N_cisco-3750_1" => {
        :node => CommonSite::NODE["N_cisco-3750"], :id => 1,
        :identifier => "RTR",
        :ip_suffix => { :primary => "161", :secondary => nil, :gateway => "161" },
        :enni_connections => nil
      },
      "N_smb-6000_1" => {
        :node => CommonSite::NODE["N_smb-6000"], :id => 1,
        :identifier => "SMB",
        :ip_suffix => { :primary => "176", :secondary => nil, :gateway => "161" },
        :enni_connections => nil
      },
      "N_Dell-PowerEdge-R410_1" => {
        :node => CommonSite::NODE["N_Dell-PowerEdge-R410"], :id => 1,
        :identifier => "SRV",
        :ip_suffix => { :primary => "165", :secondary => "166", :gateway => "161" },
        :enni_connections => nil
      },
      "N_Terminal-Server-2511_1" => {
        :node => CommonSite::NODE["N_Terminal-Server-2511"], :id => 1,
        :identifier => "TS",
        :ip_suffix => { :primary => "177", :secondary => nil, :gateway => "161" },
        :enni_connections => nil
      },
      "N_Juniper-SSG-5_1" => {
        :node => CommonSite::NODE["N_Juniper-SSG-5"], :id => 1,
        :identifier => "VPN",
        :ip_suffix => { :primary => "25", :secondary => nil, :gateway => "26" },
        :enni_connections => nil
      }
    }

    # [ [ [node_type, port], [node_type, port] ], [ [node_type, port], [node_type, port] ], ... ]
    # | | | Single Port   |  | Conected Port | |                                                |
    # | |Single connection (only need one way) |                                                |
    # | List of all connections                                                                 |

    PORT_CONNECTIONS = [
      [[NODE_INSTANCE["N_7750-sr12_1"], "1/2/18"], [NODE_INSTANCE["N_smb-6000_1"], "A1-02"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "1/2/19"], [NODE_INSTANCE["N_smb-6000_1"], "A1-03"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "1/2/20"], [NODE_INSTANCE["N_smb-6000_1"], "A1-04"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "1/2/21"], [NODE_INSTANCE["N_smb-6000_1"], "A1-05"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "1/2/22"], [NODE_INSTANCE["N_smb-6000_1"], "A1-06"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "1/2/23"], [NODE_INSTANCE["N_BV-3000_1"], "TEST1"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "1/2/24"], [NODE_INSTANCE["N_Dell-PowerEdge-R410_1"], "Gi - 3"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "3/2/18"], [NODE_INSTANCE["N_smb-6000_1"], "A2-02"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "3/2/19"], [NODE_INSTANCE["N_smb-6000_1"], "A2-03"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "3/2/20"], [NODE_INSTANCE["N_smb-6000_1"], "A2-04"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "3/2/21"], [NODE_INSTANCE["N_smb-6000_1"], "A2-05"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "3/2/22"], [NODE_INSTANCE["N_smb-6000_1"], "A2-06"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "3/2/23"], [NODE_INSTANCE["N_BV-3000_1"], "TEST2"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "3/2/24"], [NODE_INSTANCE["N_Dell-PowerEdge-R410_1"], "Gi - 4"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "A console"], [NODE_INSTANCE["N_Terminal-Server-2511_1"], "2013"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "B console"], [NODE_INSTANCE["N_Terminal-Server-2511_1"], "2014"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "CPM/SF A MGMT"], [NODE_INSTANCE["N_cisco-3750_1"], "Fa1/0/19"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "CPM/SF B MGMT"], [NODE_INSTANCE["N_cisco-3750_1"], "Fa2/0/20"]],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa1/0/15"], [NODE_INSTANCE["N_Dell-PowerEdge-R410_1"], "Gi - 1"]],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa1/0/21"], [NODE_INSTANCE["N_smb-6000_1"], "MGMT1"]],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa1/0/22"], [NODE_INSTANCE["N_Terminal-Server-2511_1"], "E0"]],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa1/0/24"], [NODE_INSTANCE["N_BV-3000_1"], "MGMT1"]],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa2/0/2"], [NODE_INSTANCE["N_Juniper-SSG-5_1"], "bgroup0-ethernet0/2"]],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa2/0/14"], [NODE_INSTANCE["N_BV-3000_1"], "MGMT2"]],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa2/0/15"], [NODE_INSTANCE["N_Dell-PowerEdge-R410_1"], "Gi - 2"]],
      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2001"], [NODE_INSTANCE["N_cisco-3750_1"], "console to Sw #1 - Master"]],
      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2002"], [NODE_INSTANCE["N_cisco-3750_1"], "console to Sw #2 - Non Master"]],
      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2004"], [NODE_INSTANCE["N_BV-3000_1"], "console"]],
      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2005"], [NODE_INSTANCE["N_Juniper-SSG-5_1"], "console"]],
      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2009"], [NODE_INSTANCE["N_smb-6000_1"], "console"]]
    ]
  end

  #This submodule is a specific site for this site
  # EACH Site MUST have a NODE_INSTANCE hash of nodes and
  # a PORT_CONNECTIONS array of internal connections
  module Secondary
    #Node Instance Structure
    #{
    # :name => {
    #   :node => ::NODE[""], :id => 1,
    #   :ip_suffix => { :primary => "", :secondary => "", :gateway => "" },
    #   :enni_connections => [{
    #     :protection_type => "", :lag_mode => "", :lag_id => 199, :physical_medium => "",
    #     :ether_type => "", :auto_negotiate => true, :port_enap_type => "",
    #     :port_connections => ["", ""] },]
    # },
    #]
    NODE_INSTANCE = {
      "N_7750-sr12_1" => {
        :node => StandardExchange7750::NODE["N_7750-sr12"], :id => 1,
        :identifier => "ALU",
        :ip_suffix => { :primary => "170", :secondary => "171", :gateway => "161" },
        :enni_connections => nil
      },
      "N_cisco-3750_1" => {
        :node => CommonSite::NODE["N_cisco-3750"], :id => 1,
        :identifier => "RTR",
        :ip_suffix => { :primary => "161", :secondary => nil, :gateway => "161" },
        :enni_connections => nil
      },
      "N_Dell-PowerEdge-R410_1" => {
        :node => CommonSite::NODE["N_Dell-PowerEdge-R410"], :id => 1,
        :identifier => "SRV",
        :ip_suffix => { :primary => "165", :secondary => "166", :gateway => "161" },
        :enni_connections => nil
      },
      "N_Terminal-Server-2511_1" => {
        :node => CommonSite::NODE["N_Terminal-Server-2511"], :id => 1,
        :identifier => "TS",
        :ip_suffix => { :primary => "177", :secondary => nil, :gateway => "161" },
        :enni_connections => nil
      }
    }

    # [ [ [node_type, port], [node_type, port] ], [ [node_type, port], [node_type, port] ], ... ]
    # | | | Single Port   |  | Conected Port | |                                                |
    # | |Single connection (only need one way) |                                                |
    # | List of all connections                                                                 |

    PORT_CONNECTIONS = [
      [[NODE_INSTANCE["N_7750-sr12_1"], "1/2/24"], [NODE_INSTANCE["N_Dell-PowerEdge-R410_1"], "Gi - 3"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "3/2/24"], [NODE_INSTANCE["N_Dell-PowerEdge-R410_1"], "Gi - 4"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "A console"], [NODE_INSTANCE["N_Terminal-Server-2511_1"], "2013"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "B console"], [NODE_INSTANCE["N_Terminal-Server-2511_1"], "2014"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "CPM/SF A MGMT"], [NODE_INSTANCE["N_cisco-3750_1"], "Fa1/0/19"]],
      [[NODE_INSTANCE["N_7750-sr12_1"], "CPM/SF B MGMT"], [NODE_INSTANCE["N_cisco-3750_1"], "Fa2/0/20"]],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa1/0/15"], [NODE_INSTANCE["N_Dell-PowerEdge-R410_1"], "Gi - 1"]],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa1/0/22"], [NODE_INSTANCE["N_Terminal-Server-2511_1"], "E0"]],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa2/0/15"], [NODE_INSTANCE["N_Dell-PowerEdge-R410_1"], "Gi - 2"]],
      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2001"], [NODE_INSTANCE["N_cisco-3750_1"], "console to Sw #1 - Master"]],
      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2002"], [NODE_INSTANCE["N_cisco-3750_1"], "console to Sw #2 - Non Master"]]
    ]
  end

    #This submodule is a specific site for this site
  # EACH Site MUST have a NODE_INSTANCE hash of nodes and
  # a PORT_CONNECTIONS array of internal connections
  module Teather
    #Node Instance Structure
    #{
    # :name => {
    #   :node => ::NODE[""], :id => 1,
    #   :ip_suffix => { :primary => "", :secondary => "", :gateway => "" },
    #   :enni_connections => [{
    #     :protection_type => "", :lag_mode => "", :lag_id => 199, :physical_medium => "",
    #     :ether_type => "", :auto_negotiate => true, :port_enap_type => "",
    #     :port_connections => ["", ""] },]
    # },
    #]
    NODE_INSTANCE = {

    }

    # [ [ [node_type, port], [node_type, port] ], [ [node_type, port], [node_type, port] ], ... ]
    # | | | Single Port   |  | Conected Port | |                                                |
    # | |Single connection (only need one way) |                                                |
    # | List of all connections                                                                 |

    PORT_CONNECTIONS = [

    ]
  end

  #Hash of all sites (a.k.a. submodules) for this Site type.
  SITES = {
    "Primary" => Primary,
    "Secondary" => Secondary,
    "Teather" => Teather
  }

  #Inter Site Connections
  # [ [ [ Site String, Node Instance Name, Port ], [ Site String, Node Instance Name, Port ], ... ]
  # | | |    Single Port                            |  |         Connected Port                    |      |
  # | |         Single Connection (Only need one way)                                              |      |
  # |    List of all connection between all sites                                                     |
  #
  CONNECTIONS = [
    [["Primary", "N_7750-sr12_1", '2/2/1'], ["Secondary", "N_7750-sr12_1", '2/2/1']],
    [["Primary", "N_7750-sr12_1", '4/2/1'], ["Secondary", "N_7750-sr12_1", '4/2/1']]
  ]

  #This is used to seed additional information to nodes when two or more
  # sites are connected
  # {
  #  "Site String" => { NODE_INSTANCE => {<information>}, ... },
  #  "Site String" => { NODE_INSTANCE => {<information>}, ... }
  # }
  NODE_SEED = {
    "Primary" => {
      "N_7750-sr12_1" => {
        :ports => [
          {:name => "2/2/1", :phy_type => "10GigE LR; 1310nm; SMF"},
          {:name => "4/2/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        ]
      }
    },
    "Secondary" => {
      "N_7750-sr12_1" => {
      :ports => [
        {:name => "2/2/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "4/2/1", :phy_type => "10GigE LR; 1310nm; SMF"},
      ]
      }     
    }
  }

end
