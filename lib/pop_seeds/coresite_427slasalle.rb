module CoreSite_427SLasalle

   #The following is default site seeding info

  #Nodes Specific ONLY to this Site
  # Node Structure: (NOTE: Key may not be the same as node type
  # so don't use it as a shortcut, just most of the time it will be the same
  # the node's :type)
  # "" => {
  #    :type => "",
  #    :vendor => "",
  #    :make_model => "",
  #    :description => "",
  #    :nm_connection => {:name => "", nm_type => ""},
  #    :ports => [
  #      {:name => "", :phy_type => ""},
  #    ]
  # },
  NODE = {
  }

  #This submodule is a specific site for this site
  # EACH Site MUST have a NODE_INSTANCE hash of nodes and
  # a PORT_CONNECTIONS array of internal connections
  module Primary
    NODE_INSTANCE = {
      "N_MLXe8_1" => {
        :node => CommonCoreSitePop::NODE["N_MLXe8"], :id => 1,
        :identifier => ".IDC05.MDF.MLX8.", :id_length => 2,
        :ip_suffix => { :primary => "60", :secondary => nil, :gateway => "1", :loopback => "1" },
        :enni_connections => []
       },
       "N_MLXe8_2" => {
        :node => CommonCoreSitePop::NODE["N_MLXe8"], :id => 2,
        :identifier => ".IDC05.MDF.MLX8.", :id_length => 2,
        :ip_suffix => { :primary => "61", :secondary => nil, :gateway => "1", :loopback => "2" },
        :enni_connections => []
       },
       #IDC03.IDF
       "N_CES_2024_IDC03_IDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 1,
        :identifier => ".IDC03.IDF.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "66", :secondary => nil, :gateway => "1", :loopback => "7" },
        :enni_connections => []
       },
       "N_CES_2024_IDC03_IDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 2,
        :identifier => ".IDC03.IDF.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "67", :secondary => nil, :gateway => "1", :loopback => "8" },
        :enni_connections => []
       },
       #IDC01.100
       "N_CES_2024_IDC01_100_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 1,
        :identifier => ".IDC01.100.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "68", :secondary => nil, :gateway => "1", :loopback => "9" },
        :enni_connections => []
       },
       "N_CES_2024_IDC01_100_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 2,
        :identifier => ".IDC01.100.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "69", :secondary => nil, :gateway => "1", :loopback => "10" },
        :enni_connections => []
       },
       #IDC01.150
       "N_CES_2024_IDC01_150_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 1,
        :identifier => ".IDC01.150.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "70", :secondary => nil, :gateway => "1", :loopback => "11" },
        :enni_connections => []
       },
       "N_CES_2024_IDC01_150_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2024"], :id => 2,
        :identifier => ".IDC01.150.S2024.", :id_length => 2,
        :ip_suffix => { :primary => "71", :secondary => nil, :gateway => "1", :loopback => "12" },
        :enni_connections => []
       },
       #IDC05.MDF
       "N_CES_2048_IDC05_MDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".IDC05.MDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "62", :secondary => nil, :gateway => "1", :loopback => "3" },
        :enni_connections => []
       },
       "N_CES_2048_IDC05_MDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".IDC05.MDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "63", :secondary => nil, :gateway => "1", :loopback => "4" },
        :enni_connections => []
       },
       #LL.IDF
       "N_CES_2048_LL_IDF_1" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 1,
        :identifier => ".LL.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "64", :secondary => nil, :gateway => "1", :loopback => "5" },
        :enni_connections => []
       },
       "N_CES_2048_LL_IDF_2" => {
        :node => CommonCoreSitePop::NODE["N_CES_2048"], :id => 2,
        :identifier => ".LL.IDF.S2048.", :id_length => 2,
        :ip_suffix => { :primary => "65", :secondary => nil, :gateway => "1", :loopback => "6" },
        :enni_connections => []
       },
    }

    # [ [ [node_type, port], [node_type, port] ], [ [node_type, port], [node_type, port] ], ... ]
    # | | | Single Port   |  | Conected Port | |                                                |
    # | |Single connection (only need one way) |                                                |
    # | List of all connections                                                                 |

    PORT_CONNECTIONS = [
      #IDC03.IDF
      [[NODE_INSTANCE["N_MLXe8_1"], "1/1"], [NODE_INSTANCE["N_CES_2024_IDC03_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_1"], "1/2"], [NODE_INSTANCE["N_CES_2024_IDC03_IDF_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "1/1"], [NODE_INSTANCE["N_CES_2024_IDC03_IDF_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "1/2"], [NODE_INSTANCE["N_CES_2024_IDC03_IDF_2"], "1/2"]],
      #IDC01.100
      [[NODE_INSTANCE["N_MLXe8_1"], "1/3"], [NODE_INSTANCE["N_CES_2024_IDC01_100_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_1"], "1/4"], [NODE_INSTANCE["N_CES_2024_IDC01_100_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "1/3"], [NODE_INSTANCE["N_CES_2024_IDC01_100_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "1/4"], [NODE_INSTANCE["N_CES_2024_IDC01_100_2"], "1/2"]],
      #IDC01.150
      [[NODE_INSTANCE["N_MLXe8_1"], "1/5"], [NODE_INSTANCE["N_CES_2024_IDC01_150_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_1"], "2/1"], [NODE_INSTANCE["N_CES_2024_IDC01_150_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "1/5"], [NODE_INSTANCE["N_CES_2024_IDC01_150_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "2/1"], [NODE_INSTANCE["N_CES_2024_IDC01_150_2"], "1/2"]],
      #IDC05.MDF
      [[NODE_INSTANCE["N_MLXe8_1"], "2/2"], [NODE_INSTANCE["N_CES_2048_IDC05_MDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_1"], "2/3"], [NODE_INSTANCE["N_CES_2048_IDC05_MDF_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "2/2"], [NODE_INSTANCE["N_CES_2048_IDC05_MDF_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "2/3"], [NODE_INSTANCE["N_CES_2048_IDC05_MDF_2"], "1/2"]],
      #LL.IDF
      [[NODE_INSTANCE["N_MLXe8_1"], "2/4"], [NODE_INSTANCE["N_CES_2048_LL_IDF_1"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_1"], "2/5"], [NODE_INSTANCE["N_CES_2048_LL_IDF_2"], "1/1"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "2/4"], [NODE_INSTANCE["N_CES_2048_LL_IDF_1"], "1/2"]],
      [[NODE_INSTANCE["N_MLXe8_2"], "2/5"], [NODE_INSTANCE["N_CES_2048_LL_IDF_2"], "1/2"]],
      #Interconnect
      [[NODE_INSTANCE["N_MLXe8_1"], "3/1"], [NODE_INSTANCE["N_MLXe8_2"], "3/1"]],
      [[NODE_INSTANCE["N_MLXe8_1"], "4/1"], [NODE_INSTANCE["N_MLXe8_2"], "4/1"]],
    ]
  end

  #Hash of all sites (a.k.a. submodules) for this Site type.
  SITES = {
    "Primary" => Primary
  }

  #Inter Site Connections
  # [ [ [ Site String, Node Instance Name, Port ], [ Site String, Node Instance Name, Port ], ... ]
  # | | |    Single Port                            |  |         Connected Port                    |      |
  # | |         Single Connection (Only need one way)                                              |      |
  # |    List of all connection between all sites                                                     |
  #
  CONNECTIONS = [
  ]

end