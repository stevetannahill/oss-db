module SprintMSC_Samsung

  #The following is default site seeding info

  #Nodes Specific ONLY to this Site
  # Node Structure: (NOTE: Key may not be the same as node type
  # so don't use it as a shortcut, just most of the time it will be the same
  # the node's :type)
  # "" => {
  #    :type => "",
  #    :vendor => "",
  #    :make_model => "",
  #    :description => "",
  #    :nm_connection => {:name => "", nm_type => ""},
  #    :ports => [
  #      {:name => "", :phy_type => ""},
  #    ]
  # },
  NODE = {
    "N_ASR9010" => {
      :type => "ASR9010",
      :vendor => "Cisco",
      :make_model => "ASR9010",
      :description => "Cisco ASR9010",
      :nm_connection => {:name => "Sprint NEM #1", :nm_type => "Sprint NEM"},
      :ports => [
                {:name => "1/1/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "1/1/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "1/1/3", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "1/1/4", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "1/1/5", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "1/1/6", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "1/1/7", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "1/1/8", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "1/1/9", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "1/1/10", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "1/2/1", :phy_type => "10/100/1000 Base T"},
        {:name => "1/2/2", :phy_type => "10/100/1000 Base T"},
        {:name => "2/1/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/1/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/1/3", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/1/4", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/1/5", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/1/6", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/1/7", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/1/8", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/1/9", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/1/10", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/2/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/2/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/2/3", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/2/4", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/2/5", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/2/6", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/2/7", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/2/8", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/2/9", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/2/10", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/1/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/1/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/1/3", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/1/4", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/1/5", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/1/6", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/1/7", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/1/8", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/1/9", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/1/10", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/2/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/2/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/2/3", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/2/4", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/2/5", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/2/6", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/2/7", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/2/8", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/2/9", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/2/10", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "4/1/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "4/1/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "4/1/3", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "4/1/4", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "4/1/5", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "4/1/6", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "4/1/7", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "4/1/8", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "4/1/9", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "4/1/10", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "Routing Instance #1", :phy_type => "L3 Handoff"},
        {:name => "A console", :phy_type => "Other"},
        {:name => "B console", :phy_type => "Other"},
        {:name => "CPM/SF A MGMT", :phy_type => "10/100/1000 Base T"},
        {:name => "CPM/SF B MGMT", :phy_type => "10/100/1000 Base T"}
      ]
    }
  }

  #This submodule is a specific site for this site
  # EACH Site MUST have a NODE_INSTANCE hash of nodes and
  # a PORT_CONNECTIONS array of internal connections
  module Primary
    NODE_INSTANCE = {
      "N_ASR9010_1" => {
        :node => SprintMSC_Samsung::NODE["N_ASR9010"], :id => 1,
        :identifier => "-IPA", :id_length => 2,
        :ip_suffix => { :primary => "170", :secondary => "171", :gateway => "161" },
        :enni_connections => [
          { :protection_type => "unprotected", :physical_medium => "10/100/1000 Base T",
            :ether_type => "0x8100", :auto_negotiate => true, :port_enap_type => "QINQ",
            :port_connections => ["1/2/1"], :member_handle => "IPA01-TEST1" },
          { :protection_type => "unprotected", :physical_medium => "L3 Handoff", :demarc_icon => "router",
            :ether_type => "Other", :auto_negotiate => false, :port_enap_type => "NULL",
            :port_connections => ["Routing Instance #1"], :member_handle => "IPA01-VRF-01" }
        ]
       },
       "N_ASR9010_2" => {
        :node => SprintMSC_Samsung::NODE["N_ASR9010"], :id => 2,
        :identifier => "-IPA", :id_length => 2,
        :ip_suffix => { :primary => "172", :secondary => "173", :gateway => "161" },
        :enni_connections => [
          { :protection_type => "unprotected", :physical_medium => "10/100/1000 Base T",
            :ether_type => "0x8100", :auto_negotiate => true, :port_enap_type => "QINQ",
            :port_connections => ["1/2/1"], :member_handle => "IPA02-TEST1" },
          { :protection_type => "unprotected", :physical_medium => "L3 Handoff", :demarc_icon => "router",
            :ether_type => "Other", :auto_negotiate => false, :port_enap_type => "NULL",
            :port_connections => ["Routing Instance #1"], :member_handle => "IPA02-VRF-01" }
        ]
       },
       "N_QoS-Scope-1G_1" => {
        :node => CommonSite::NODE["N_QoS-Scope-1G"], :id => 1,
        :identifier => "-QSCOPE", :id_length => 2,
        :ip_suffix => { :primary => "182", :secondary => nil, :gateway => "161" },
        :enni_connections => nil
       }
    }

    # [ [ [node_type, port], [node_type, port] ], [ [node_type, port], [node_type, port] ], ... ]
    # | | | Single Port   |  | Conected Port | |                                                |
    # | |Single connection (only need one way) |                                                |
    # | List of all connections                                                                 |

    PORT_CONNECTIONS = [
      [[NODE_INSTANCE["N_ASR9010_1"], "1/1/1"], [NODE_INSTANCE["N_ASR9010_2"], "1/1/1"]],
      [[NODE_INSTANCE["N_ASR9010_1"], "2/1/1"], [NODE_INSTANCE["N_ASR9010_2"], "2/1/1"]],
      [[NODE_INSTANCE["N_ASR9010_1"], "3/1/1"], [NODE_INSTANCE["N_ASR9010_2"], "3/1/1"]],
      [[NODE_INSTANCE["N_ASR9010_1"], "4/1/1"], [NODE_INSTANCE["N_ASR9010_2"], "4/1/1"]],
      [[NODE_INSTANCE["N_ASR9010_1"], "1/2/1"], [NODE_INSTANCE["N_QoS-Scope-1G_1"], "TEST1"]],
      [[NODE_INSTANCE["N_ASR9010_2"], "1/2/1"], [NODE_INSTANCE["N_QoS-Scope-1G_1"], "TEST2"]]
    ]
  end

  #Hash of all sites (a.k.a. submodules) for this Site type.
  SITES = {
    "Primary" => Primary
  }

  #Inter Site Connections
  # [ [ [ Site String, Node Instance Name, Port ], [ Site String, Node Instance Name, Port ], ... ]
  # | | |    Single Port                            |  |         Connected Port                    |      |
  # | |         Single Connection (Only need one way)                                              |      |
  # |    List of all connection between all sites                                                     |
  #
  CONNECTIONS = [
  ]

end