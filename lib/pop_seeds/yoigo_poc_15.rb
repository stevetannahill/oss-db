module Yoigo_POC_15

  #The following is default site seeding info

  #Nodes Specific ONLY to this Site
  # Node Structure: (NOTE: Key may not be the same as node type
  # so don't use it as a shortcut, just most of the time it will be the same
  # the node's :type)
  # "" => {
  #    :type => "",
  #    :vendor => "",
  #    :make_model => "",
  #    :description => "",
  #    :nm_connection => {:name => "", nm_type => ""},
  #    :ports => [
  #      {:name => "", :phy_type => ""},
  #    ]
  # },
  NODE = {
    "N_Tellabs_8630" => {
      :type => "Tellabs 8630",
      :vendor => "Tellabs",
      :make_model => "8630",
      :description => "Tellabs 8630",
      :nm_connection => {:name => "Tellabs_EMS_1", :nm_type => "Tellabs EMS"},
      :ports => [
        {:name => "ge9/0/0", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge9/0/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge9/0/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge9/0/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge9/0/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge9/0/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge9/0/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge9/0/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge9/1/0", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge9/1/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge9/1/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge9/1/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge9/1/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge9/1/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge9/1/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge9/1/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        
        {:name => "A console", :phy_type => "Other"},
        {:name => "B console", :phy_type => "Other"},
        {:name => "A MGMT", :phy_type => "10/100/1000 Base T"},
        {:name => "B MGMT", :phy_type => "10/100/1000 Base T"}
      ]
    }
  }

  #This submodule is a specific site for this site
  # EACH Site MUST have a NODE_INSTANCE hash of nodes and
  # a PORT_CONNECTIONS array of internal connections
  module Primary
    NODE_INSTANCE = {
      "N_Tellabs_8630_6" => {
        :node => Yoigo_POC_15::NODE["N_Tellabs_8630"], :id => 6,
        :identifier => "_", :id_length => 3,
        :ip_suffix => { :primary => "TBD", :secondary => "TBD", :gateway => "TBD" },
        :enni_connections => nil
       },
       "N_MINI-LINK_1" => {
        :node => CommonSite::NODE["N_MINI-LINK"], :id => 1,
        :identifier => "_ML_", :id_length => 3,
        :ip_suffix => { :primary => nil, :secondary => nil, :gateway => "161" },
        :enni_connections => [
          { :protection_type => "unprotected", :physical_medium => "Other",
            :ether_type => "Other", :auto_negotiate => true, :port_enap_type => "DOT1Q", :demarc_icon => "MW site",
            :port_connections => ["MW-1"] },
          { :protection_type => "unprotected", :physical_medium => "Other",
            :ether_type => "Other", :auto_negotiate => true, :port_enap_type => "DOT1Q", :demarc_icon => "MW site",
            :port_connections => ["MW-2"] },
          { :protection_type => "unprotected", :physical_medium => "Other",
            :ether_type => "Other", :auto_negotiate => true, :port_enap_type => "DOT1Q", :demarc_icon => "MW site",
            :port_connections => ["MW-3"] },
          { :protection_type => "unprotected", :physical_medium => "Other",
            :ether_type => "Other", :auto_negotiate => true, :port_enap_type => "DOT1Q", :demarc_icon => "MW site",
            :port_connections => ["MW-4"] }

        ]
       },
       "N_SIU_1" => {
        :node => CommonSite::NODE["N_SIU"], :id => 1,
        :identifier => "_SIU_", :id_length => 3,
        :ip_suffix => { :primary => "TBD", :secondary => "TBD", :gateway => "TBD" },
        :enni_connections => nil
       },
      "N_RBS_1" => {
        :node => CommonSite::NODE["N_RBS"], :id => 1,
        :identifier => "_RBS_", :id_length => 3,
        :ip_suffix => { :primary => "TBD", :secondary => "TBD", :gateway => "TBD" },
        :enni_connections => [
          { :protection_type => "unprotected", :physical_medium => "Other",
            :ether_type => "Other", :auto_negotiate => true, :port_enap_type => "NULL", :demarc_icon => "cell site",
            :port_connections => ["radio-if"] }
        ]
       }
    }

    # [ [ [node_type, port], [node_type, port] ], [ [node_type, port], [node_type, port] ], ... ]
    # | | | Single Port   |  | Conected Port | |                                                |
    # | |Single connection (only need one way) |                                                |
    # | List of all connections                                                                 |

    PORT_CONNECTIONS = [ 
      [[NODE_INSTANCE["N_Tellabs_8630_6"], "ge9/1/1"], [NODE_INSTANCE["N_MINI-LINK_1"], "11/1"]],
      [[NODE_INSTANCE["N_Tellabs_8630_6"], "ge9/0/6"], [NODE_INSTANCE["N_MINI-LINK_1"], "11/2"]],

      [[NODE_INSTANCE["N_SIU_1"], "ge1"], [NODE_INSTANCE["N_RBS_1"], "ge1"]],
      [[NODE_INSTANCE["N_SIU_1"], "so0"], [NODE_INSTANCE["N_RBS_1"], "so0"]],
      [[NODE_INSTANCE["N_SIU_1"], "so1"], [NODE_INSTANCE["N_RBS_1"], "so1"]],
      [[NODE_INSTANCE["N_SIU_1"], "so2"], [NODE_INSTANCE["N_RBS_1"], "so2"]],
    ]
  end

  #Hash of all sites (a.k.a. submodules) for this Site type.
  SITES = {
    "Primary" => Primary
  }

  #Inter Site Connections
  # [ [ [ Site String, Node Instance Name, Port ], [ Site String, Node Instance Name, Port ], ... ]
  # | | |    Single Port                            |  |         Connected Port                    |      |
  # | |         Single Connection (Only need one way)                                              |      |
  # |    List of all connection between all sites                                                     |
  #
  CONNECTIONS = [
  ]

end
