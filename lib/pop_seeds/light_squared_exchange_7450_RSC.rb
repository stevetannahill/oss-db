module LightSquaredExchange7450_RSC

  #The following is default site seeding info

  #Nodes Specific ONLY to this Site
  # Node Structure: (NOTE: Key may not be the same as node type
  # so don't use it as a shortcut, just most of the time it will be the same
  # the node's :type)
  # "" => {
  #    :type => "",
  #    :vendor => "",
  #    :make_model => "",
  #    :description => "",
  #    :nm_connection => {:name => "", nm_type => ""},
  #    :ports => [
  #      {:name => "", :phy_type => ""},
  #    ]
  # },
  NODE = {
    "N_7450-ESS7" => {
      :type => "7450-ESS7",
      :vendor => "Alcatel",
      :make_model => "7450-ESS7 - 3HE05867AA",
      :description => "Alcatel 7450-ESS12 Ethernet Service Switch",
      :nm_connection => {:name => "5620 SAM - Live", :nm_type => "5620Sam"},
      :ports => [
        {:name => "1/1/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "1/1/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "1/2/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/2/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/2/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/2/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/2/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/2/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/2/7", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/2/8", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/2/9", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/2/10", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/2/11", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/2/12", :phy_type => "10/100/1000 Base T"},
        {:name => "1/2/13", :phy_type => "10/100/1000 Base T"},
        {:name => "1/2/14", :phy_type => "10/100/1000 Base T"},
        {:name => "1/2/15", :phy_type => "10/100/1000 Base T"},
        {:name => "1/2/16", :phy_type => "10/100/1000 Base T"},
        {:name => "1/2/17", :phy_type => "10/100/1000 Base T"},
        {:name => "1/2/18", :phy_type => "10/100/1000 Base T"},
        {:name => "1/2/19", :phy_type => "10/100/1000 Base T"},
        {:name => "1/2/20", :phy_type => "10/100/1000 Base T"},
        {:name => "2/1/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/1/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/2/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "2/2/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/1/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/1/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/2/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "3/2/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "4/1/1", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "4/1/2", :phy_type => "10GigE LR; 1310nm; SMF"},
        {:name => "A console", :phy_type => "Other"},
        {:name => "B console", :phy_type => "Other"},
        {:name => "CPM/SF A MGMT", :phy_type => "10/100/1000 Base T"},
        {:name => "CPM/SF B MGMT", :phy_type => "10/100/1000 Base T"}
      ]
    }
  }

  #This submodule is a specific site for this site
  # EACH Site MUST have a NODE_INSTANCE hash of nodes and
  # a PORT_CONNECTIONS array of internal connections
  module Primary
    NODE_INSTANCE = {
      "N_7450-ESS7_1" => {
        :node => LightSquaredExchange7450_RSC::NODE["N_7450-ESS7"], :id => 1,
        :ip_suffix => { :primary => "170", :secondary => "171", :gateway => "161" },
        :identifier => "SWSxxx", :id_length => 3,
        :enni_connections => [
          { :protection_type => "unprotected-LAG", :lag_mode => "N/A", :lag_id => 199, :physical_medium => "10/100/1000 Base T",
            :ether_type => "0x8100", :auto_negotiate => true, :port_enap_type => "QINQ",
            :port_connections => ["1/2/19"] },
          { :protection_type => "unprotected-LAG", :lag_mode => "N/A", :lag_id => 197, :physical_medium => "10/100/1000 Base T",
            :ether_type => "0x8100", :auto_negotiate => true, :port_enap_type => "QINQ",
            :port_connections => ["1/2/12"] }
        ]
       },
       "N_7450-ESS7_2" => {
        :node => LightSquaredExchange7450_RSC::NODE["N_7450-ESS7"], :id => 2,
        :ip_suffix => { :primary => "172", :secondary => "173", :gateway => "161" },
        :identifier => "SWSxxx", :id_length => 3,
        :enni_connections => [
          { :protection_type => "unprotected-LAG", :lag_mode => "N/A", :lag_id => 198, :physical_medium => "10/100/1000 Base T",
            :ether_type => "0x8100", :auto_negotiate => true, :port_enap_type => "QINQ",
            :port_connections => ["1/2/19"] },
          { :protection_type => "unprotected-LAG", :lag_mode => "N/A", :lag_id => 196, :physical_medium => "10/100/1000 Base T",
            :ether_type => "0x8100", :auto_negotiate => true, :port_enap_type => "QINQ",
            :port_connections => ["1/2/12"] }
        ]
       },
       "N_BV-3000_1" => {
        :node => CommonSite::NODE["N_BV-3000"], :id => 1,
        :identifier => "SMAxxx", :id_length => 3,
        :ip_suffix => { :primary => "182", :secondary => nil, :gateway => "161" },
        :enni_connections => nil
       },
       "N_BV-3000_2" => {
        :node => CommonSite::NODE["N_BV-3000"], :id => 2,
        :identifier => "SMAxxx", :id_length => 3,
        :ip_suffix => { :primary => "183", :secondary => nil, :gateway => "161" },
        :enni_connections => nil
       },
       "N_RTU-310_1" => {
        :node => CommonSite::NODE["N_RTU-310"], :id => 1,
        :identifier => "TESxxx", :id_length => 3,
        :ip_suffix => { :primary => "184", :secondary => nil, :gateway => "161" },
        :enni_connections => nil
       },
       "N_RTU-310_2" => {
        :node => CommonSite::NODE["N_RTU-310"], :id => 2,
        :identifier => "TESxxx", :id_length => 3,
        :ip_suffix => { :primary => "185", :secondary => nil, :gateway => "161" },
        :enni_connections => nil
       },
       "N_cisco-3750_1" => {
        :node => CommonSite::NODE["N_cisco-3750"], :id => 1,
        :identifier => "RTMxxx", :id_length => 3,
        :ip_suffix => { :primary => "161", :secondary => nil, :gateway => "161" },
        :enni_connections => nil
       },
       "N_smb-6000_1" => {
        :node => CommonSite::NODE["N_smb-6000"], :id => 1,
        :identifier => "TGExxx", :id_length => 3,
        :ip_suffix => { :primary => "176", :secondary => nil, :gateway => "161" },
        :enni_connections => nil
       },
       "N_Dell-PowerEdge-R410_1" => {
        :node => CommonSite::NODE["N_Dell-PowerEdge-R410"], :id => 1,
        :identifier => "TRPxxx", :id_length => 3,
        :ip_suffix => { :primary => "165", :secondary => "166", :gateway => "161" },
        :enni_connections => nil
       },
       "N_Terminal-Server-2511_1" => {
        :node => CommonSite::NODE["N_Terminal-Server-2511"], :id => 1,
        :identifier => "TRMxxx", :id_length => 3,
        :ip_suffix => { :primary => "177", :secondary => nil, :gateway => "161" },
        :enni_connections => nil
       },
       "N_Juniper-SSG-5_1" => {
        :node => CommonSite::NODE["N_Juniper-SSG-5"], :id => 1,
        :identifier => "RTXxxx", :id_length => 3,
        :ip_suffix => { :primary => "25", :secondary => nil, :gateway => "26" },
        :enni_connections => nil
       },
    }

    # [ [ [node_type, port], [node_type, port] ], [ [node_type, port], [node_type, port] ], ... ]
    # | | | Single Port   |  | Conected Port | |                                                |
    # | |Single connection (only need one way) |                                                |
    # | List of all connections                                                                 |

    PORT_CONNECTIONS = [
      [[NODE_INSTANCE["N_7450-ESS7_1"], "1/1/1"], [NODE_INSTANCE["N_7450-ESS7_2"], "1/1/1"]],
      [[NODE_INSTANCE["N_7450-ESS7_1"], "2/1/1"], [NODE_INSTANCE["N_7450-ESS7_2"], "2/1/1"]],
      [[NODE_INSTANCE["N_7450-ESS7_1"], "3/1/1"], [NODE_INSTANCE["N_7450-ESS7_2"], "3/1/1"]],
      [[NODE_INSTANCE["N_7450-ESS7_1"], "4/1/1"], [NODE_INSTANCE["N_7450-ESS7_2"], "4/1/1"]],

      [[NODE_INSTANCE["N_7450-ESS7_1"], "1/2/12"], [NODE_INSTANCE["N_RTU-310_1"], "TEST1"]],
      [[NODE_INSTANCE["N_7450-ESS7_1"], "1/2/13"], [NODE_INSTANCE["N_smb-6000_1"], "A1-01"]],
      [[NODE_INSTANCE["N_7450-ESS7_1"], "1/2/14"], [NODE_INSTANCE["N_smb-6000_1"], "A1-02"]],
      [[NODE_INSTANCE["N_7450-ESS7_1"], "1/2/15"], [NODE_INSTANCE["N_smb-6000_1"], "A1-03"]],
      [[NODE_INSTANCE["N_7450-ESS7_1"], "1/2/16"], [NODE_INSTANCE["N_smb-6000_1"], "A2-01"]],
      [[NODE_INSTANCE["N_7450-ESS7_1"], "1/2/17"], [NODE_INSTANCE["N_smb-6000_1"], "A2-02"]],
      [[NODE_INSTANCE["N_7450-ESS7_1"], "1/2/18"], [NODE_INSTANCE["N_smb-6000_1"], "A2-03"]],
      [[NODE_INSTANCE["N_7450-ESS7_1"], "1/2/19"], [NODE_INSTANCE["N_BV-3000_1"], "TEST1"]],
      [[NODE_INSTANCE["N_7450-ESS7_1"], "1/2/20"], [NODE_INSTANCE["N_Dell-PowerEdge-R410_1"], "Gi - 3"]],


      [[NODE_INSTANCE["N_7450-ESS7_2"], "1/2/12"], [NODE_INSTANCE["N_RTU-310_2"], "TEST1"]],
      [[NODE_INSTANCE["N_7450-ESS7_2"], "1/2/13"], [NODE_INSTANCE["N_smb-6000_1"], "A1-04"]],
      [[NODE_INSTANCE["N_7450-ESS7_2"], "1/2/14"], [NODE_INSTANCE["N_smb-6000_1"], "A1-05"]],
      [[NODE_INSTANCE["N_7450-ESS7_2"], "1/2/15"], [NODE_INSTANCE["N_smb-6000_1"], "A1-06"]],
      [[NODE_INSTANCE["N_7450-ESS7_2"], "1/2/16"], [NODE_INSTANCE["N_smb-6000_1"], "A2-04"]],
      [[NODE_INSTANCE["N_7450-ESS7_2"], "1/2/17"], [NODE_INSTANCE["N_smb-6000_1"], "A2-05"]],
      [[NODE_INSTANCE["N_7450-ESS7_2"], "1/2/18"], [NODE_INSTANCE["N_smb-6000_1"], "A2-06"]],
      [[NODE_INSTANCE["N_7450-ESS7_2"], "1/2/19"], [NODE_INSTANCE["N_BV-3000_2"], "TEST1"]],
      [[NODE_INSTANCE["N_7450-ESS7_2"], "1/2/20"], [NODE_INSTANCE["N_Dell-PowerEdge-R410_1"], "Gi - 4"]],


      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa1/0/15"], [NODE_INSTANCE["N_Dell-PowerEdge-R410_1"], "Gi - 1"]],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa1/0/21"], [NODE_INSTANCE["N_smb-6000_1"], "MGMT1"]],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa1/0/22"], [NODE_INSTANCE["N_Terminal-Server-2511_1"], "E0"]],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa1/0/24"], [NODE_INSTANCE["N_BV-3000_1"], "MGMT1"]],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa2/0/2"], [NODE_INSTANCE["N_Juniper-SSG-5_1"], "bgroup0-ethernet0/2"]],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa2/0/14"], [NODE_INSTANCE["N_BV-3000_1"], "MGMT2"]],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa2/0/15"], [NODE_INSTANCE["N_Dell-PowerEdge-R410_1"], "Gi - 2"]],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa1/0/19"], [NODE_INSTANCE["N_7450-ESS7_1"], "CPM/SF A MGMT"] ],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa2/0/20"], [NODE_INSTANCE["N_7450-ESS7_1"], "CPM/SF B MGMT"]],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa1/0/21"], [NODE_INSTANCE["N_7450-ESS7_2"], "CPM/SF A MGMT"] ],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa1/0/22"], [NODE_INSTANCE["N_7450-ESS7_2"], "CPM/SF B MGMT"]],

      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2001"], [NODE_INSTANCE["N_cisco-3750_1"], "console to Sw #1 - Master"]],
      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2002"], [NODE_INSTANCE["N_cisco-3750_1"], "console to Sw #2 - Non Master"]],
      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2004"], [NODE_INSTANCE["N_BV-3000_1"], "console"]],
      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2005"], [NODE_INSTANCE["N_BV-3000_2"], "console"]],
      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2006"], [NODE_INSTANCE["N_Juniper-SSG-5_1"], "console"]],
      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2009"], [NODE_INSTANCE["N_smb-6000_1"], "console"]],
      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2011"], [NODE_INSTANCE["N_7450-ESS7_1"], "A console"]],
      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2012"], [NODE_INSTANCE["N_7450-ESS7_1"], "B console"]],
      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2013"], [NODE_INSTANCE["N_7450-ESS7_2"], "A console"]],
      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2014"], [NODE_INSTANCE["N_7450-ESS7_2"], "B console"]]
    ]
  end

  #Hash of all sites (a.k.a. submodules) for this Site type.
  SITES = {
    "Primary" => Primary
  }

  #Inter Site Connections
  # [ [ [ Site String, Node Instance Name, Port ], [ Site String, Node Instance Name, Port ], ... ]
  # | | |    Single Port                            |  |         Connected Port                    |      |
  # | |         Single Connection (Only need one way)                                              |      |
  # |    List of all connection between all sites                                                     |
  #
  CONNECTIONS = [
  ]

end
