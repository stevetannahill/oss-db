module LightSquaredExchange7210_SGW

  #The following is default site seeding info

  #Nodes Specific ONLY to this Site
  # Node Structure: (NOTE: Key may not be the same as node type
  # so don't use it as a shortcut, just most of the time it will be the same
  # the node's :type)
  # "" => {
  #    :type => "",
  #    :vendor => "",
  #    :make_model => "",
  #    :description => "",
  #    :nm_connection => {:name => "", nm_type => ""},
  #    :ports => [
  #      {:name => "", :phy_type => ""},
  #    ]
  # },
  NODE = {
    "N_7210 SAS-M" => {
      :type => "7210 SAS-M",
      :vendor => "Alcatel",
      :make_model => "7210 SAS-M - <Model>",
      :description => "<Description>",
      :nm_connection => {:name => "5620 SAM - Live", :nm_type => "5620Sam"},
      :ports => [
        {:name => "1/1/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/2", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/3", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/4", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/5", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/6", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/7", :phy_type => "1000Base-SX; 850nm; MMF"},
        {:name => "1/1/8", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/9", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/10", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/11", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/12", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/13", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/14", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/15", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/16", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/17", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/18", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/19", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/20", :phy_type => "10/100/1000 Base T"},
        {:name => "1/1/21", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/22", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/23", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "1/1/24", :phy_type => "10/100/1000 Base T"},

        {:name => "Console", :phy_type => "Other"},
        {:name => "MGMT A", :phy_type => "10/100/1000 Base T"},
      ]
    }
  }

  #This submodule is a specific site for this site
  # EACH Site MUST have a NODE_INSTANCE hash of nodes and
  # a PORT_CONNECTIONS array of internal connections
  module Primary
    NODE_INSTANCE = {
      "N_7210 SAS-M_1" => {
        :node => LightSquaredExchange7210_SGW::NODE["N_7210 SAS-M"], :id => 1,
        :identifier => "SWSxxx", :id_length => 3,
        :ip_suffix => { :primary => "170", :secondary => "171", :gateway => "161" },
        :enni_connections => [
          { :protection_type => "unprotected-LAG", :lag_mode => "N/A", :lag_id => 199, :physical_medium => "10/100/1000 Base T",
            :ether_type => "0x8100", :auto_negotiate => true, :port_enap_type => "QINQ",
            :port_connections => ["1/1/20"] }
        ]
       },
       "N_7210 SAS-M_2" => {
        :node => LightSquaredExchange7210_SGW::NODE["N_7210 SAS-M"], :id => 2,
        :identifier => "SWSxxx", :id_length => 3,
        :ip_suffix => { :primary => "172", :secondary => "173", :gateway => "161" },
        :enni_connections => [
          { :protection_type => "unprotected-LAG", :lag_mode => "N/A", :lag_id => 198, :physical_medium => "10/100/1000 Base T",
            :ether_type => "0x8100", :auto_negotiate => true, :port_enap_type => "QINQ",
            :port_connections => ["1/1/20"] }
        ]
       },
       "N_RTU-310_1" => {
        :node => CommonSite::NODE["N_RTU-310"], :id => 1,
        :identifier => "TESxxx", :id_length => 3,
        :ip_suffix => { :primary => "186", :secondary => "187", :gateway => "161" },
        :enni_connections => nil
       },
       "N_cisco-3750_1" => {
        :node => CommonSite::NODE["N_cisco-3750"], :id => 1,
        :identifier => "RTMxxx", :id_length => 3,
        :ip_suffix => { :primary => "161", :secondary => nil, :gateway => "161" },
        :enni_connections => nil
       },
       "N_cisco-3750_2" => {
        :node => CommonSite::NODE["N_cisco-3750"], :id => 2,
        :identifier => "RTMxxx", :id_length => 3,
        :ip_suffix => { :primary => "26", :secondary => nil, :gateway => "161" },
        :enni_connections => nil
       },
       "N_Dell-PowerEdge-R410_1" => {
        :node => CommonSite::NODE["N_Dell-PowerEdge-R410"], :id => 1,
        :identifier => "TRPxxx", :id_length => 3,
        :ip_suffix => { :primary => "165", :secondary => "166", :gateway => "161" },
        :enni_connections => nil
       },
       "N_Terminal-Server-2511_1" => {
        :node => CommonSite::NODE["N_Terminal-Server-2511"], :id => 1,
        :identifier => "TRMxxx", :id_length => 3,
        :ip_suffix => { :primary => "177", :secondary => nil, :gateway => "161" },
        :enni_connections => nil
       },
       "N_Juniper-SSG-5_1" => {
        :node => CommonSite::NODE["N_Juniper-SSG-5"], :id => 1,
        :identifier => "RTXxxx", :id_length => 3,
        :ip_suffix => { :primary => "25", :secondary => nil, :gateway => "26" },
        :enni_connections => nil
       },
    }

    # [ [ [node_type, port], [node_type, port] ], [ [node_type, port], [node_type, port] ], ... ]
    # | | | Single Port   |  | Conected Port | |                                                |
    # | |Single connection (only need one way) |                                                |
    # | List of all connections                                                                 |

    PORT_CONNECTIONS = [
      [[NODE_INSTANCE["N_7210 SAS-M_1"], "1/1/20"], [NODE_INSTANCE["N_RTU-310_1"], "TEST1"]],
      [[NODE_INSTANCE["N_7210 SAS-M_1"], "1/1/24"], [NODE_INSTANCE["N_Dell-PowerEdge-R410_1"], "Gi - 3"]],


      [[NODE_INSTANCE["N_7210 SAS-M_2"], "1/1/20"], [NODE_INSTANCE["N_RTU-310_1"], "TEST2"]],
      [[NODE_INSTANCE["N_7210 SAS-M_2"], "1/1/24"], [NODE_INSTANCE["N_Dell-PowerEdge-R410_1"], "Gi - 4"]],


      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa1/0/15"], [NODE_INSTANCE["N_Dell-PowerEdge-R410_1"], "Gi - 1"]],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa1/0/19"], [NODE_INSTANCE["N_7210 SAS-M_1"], "MGMT A"] ],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa1/0/22"], [NODE_INSTANCE["N_Terminal-Server-2511_1"], "E0"]],
      [[NODE_INSTANCE["N_cisco-3750_1"], "Fa1/0/23"], [NODE_INSTANCE["N_RTU-310_1"], "MGMT1"]],

      [[NODE_INSTANCE["N_cisco-3750_2"], "Fa1/0/2"], [NODE_INSTANCE["N_Juniper-SSG-5_1"], "ethernet0/0"]],
      [[NODE_INSTANCE["N_cisco-3750_2"], "Fa1/0/15"], [NODE_INSTANCE["N_Dell-PowerEdge-R410_1"], "Gi - 2"]],
      [[NODE_INSTANCE["N_cisco-3750_2"], "Fa1/0/19"], [NODE_INSTANCE["N_7210 SAS-M_2"], "MGMT A"] ],
      [[NODE_INSTANCE["N_cisco-3750_2"], "Fa1/0/23"], [NODE_INSTANCE["N_RTU-310_1"], "MGMT2"]],


      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2001"], [NODE_INSTANCE["N_cisco-3750_1"], "console to Sw #1 - Master"]],
      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2002"], [NODE_INSTANCE["N_cisco-3750_2"], "console to Sw #1 - Master"]],
      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2005"], [NODE_INSTANCE["N_Juniper-SSG-5_1"], "console"]],
      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2013"], [NODE_INSTANCE["N_7210 SAS-M_1"], "Console"]],
      [[NODE_INSTANCE["N_Terminal-Server-2511_1"], "2015"], [NODE_INSTANCE["N_7210 SAS-M_2"], "Console"]]

    ]
  end

  #Hash of all sites (a.k.a. submodules) for this Site type.
  SITES = {
    "Primary" => Primary
  }

  #Inter Site Connections
  # [ [ [ Site String, Node Instance Name, Port ], [ Site String, Node Instance Name, Port ], ... ]
  # | | |    Single Port                            |  |         Connected Port                    |      |
  # | |         Single Connection (Only need one way)                                              |      |
  # |    List of all connection between all sites                                                     |
  #
  CONNECTIONS = [
  ]

end
