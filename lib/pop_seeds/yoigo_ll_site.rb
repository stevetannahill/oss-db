module Yoigo_LL

  #The following is default site seeding info

  #Nodes Specific ONLY to this Site
  # Node Structure: (NOTE: Key may not be the same as node type
  # so don't use it as a shortcut, just most of the time it will be the same
  # the node's :type)
  # "" => {
  #    :type => "",
  #    :vendor => "",
  #    :make_model => "",
  #    :description => "",
  #    :nm_connection => {:name => "", nm_type => ""},
  #    :ports => [
  #      {:name => "", :phy_type => ""},
  #    ]
  # },
  NODE = {}

  #This submodule is a specific site for this site
  # EACH Site MUST have a NODE_INSTANCE hash of nodes and
  # a PORT_CONNECTIONS array of internal connections
  module Primary
    NODE_INSTANCE = {
      "N_SIU_1" => {
        :node => CommonSite::NODE["N_SIU"], :id => 1,
        :identifier => "_SIU_", :id_length => 3,
        :ip_suffix => { :primary => "TBD", :secondary => "TBD", :gateway => "TBD" },
        :enni_connections => nil
       },
      "N_RBS_1" => {
        :node => CommonSite::NODE["N_RBS"], :id => 1,
        :identifier => "_RBS_", :id_length => 3,
        :ip_suffix => { :primary => "TBD", :secondary => "TBD", :gateway => "TBD" },
        :enni_connections => [
          { :protection_type => "unprotected", :physical_medium => "Other",
            :ether_type => "Other", :auto_negotiate => true, :port_enap_type => "NULL", :demarc_icon => "cell site",
            :port_connections => ["radio-if"] }
        ]
       }
    }

    # [ [ [node_type, port], [node_type, port] ], [ [node_type, port], [node_type, port] ], ... ]
    # | | | Single Port   |  | Conected Port | |                                                |
    # | |Single connection (only need one way) |                                                |
    # | List of all connections                                                                 |

    PORT_CONNECTIONS = [
      [[NODE_INSTANCE["N_SIU_1"], "ge1"], [NODE_INSTANCE["N_RBS_1"], "ge1"]],
      [[NODE_INSTANCE["N_SIU_1"], "so0"], [NODE_INSTANCE["N_RBS_1"], "so0"]],
      [[NODE_INSTANCE["N_SIU_1"], "so1"], [NODE_INSTANCE["N_RBS_1"], "so1"]],
      [[NODE_INSTANCE["N_SIU_1"], "so2"], [NODE_INSTANCE["N_RBS_1"], "so2"]]
    ]
  end

  #Hash of all sites (a.k.a. submodules) for this Site type.
  SITES = {
    "Primary" => Primary
  }

  #Inter Site Connections
  # [ [ [ Site String, Node Instance Name, Port ], [ Site String, Node Instance Name, Port ], ... ]
  # | | |    Single Port                            |  |         Connected Port                    |      |
  # | |         Single Connection (Only need one way)                                              |      |
  # |    List of all connection between all sites                                                     |
  #
  CONNECTIONS = [
  ]

end
