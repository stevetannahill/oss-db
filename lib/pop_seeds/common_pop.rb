module CommonSite

  #Node Structure: (NOTE: Key may not be the same as node type
  # so don't use it as a shortcut, just most of the time it will)
  #"" => {
  #   :type => "",
  #   :vendor => "",
  #   :make_model => "",
  #   :description => "",
  #   :nm_connection => {:name => "", nm_type => ""},
  #   :ports => [
  #     {:name => "", :phy_type => ""},
  #   ]
  #},

  NODE = {
    "N_BV-3000" => {
      :type => "BV-3000",
      :vendor => "EXFO",
      :make_model => "BV-3000",
      :description => "Brix Worx Verifier - BV-3000",
      :nm_connection => {:name => "BrixWorx - Live", :nm_type => "BrixWorx"},
      :ports => [
        {:name => "console", :phy_type => "Other"},
        {:name => "MGMT1", :phy_type => "10/100/1000 Base T"},
        {:name => "MGMT2", :phy_type => "10/100/1000 Base T"},
        {:name => "TEST1", :phy_type => "10/100/1000 Base T"},
        {:name => "TEST2", :phy_type => "10/100/1000 Base T"}
      ]
    },

    "N_QoS-Scope-1G" => {
      :type => "QoS-Scope-1G",
      :vendor => "Spirent",
      :make_model => "QoS-Scope-1G",
      :description => "Spirent Monitoring Probe - QoS-Scope-1G",
      :nm_connection => {:name => "Sprint Spirent Server", :nm_type => "eScout"},
      :ports => [
        {:name => "console", :phy_type => "Other"},
        {:name => "MGMT1", :phy_type => "10/100/1000 Base T"},
        {:name => "MGMT2", :phy_type => "10/100/1000 Base T"},
        {:name => "TEST1", :phy_type => "10/100/1000 Base T"},
        {:name => "TEST2", :phy_type => "10/100/1000 Base T"},
        {:name => "TEST3", :phy_type => "10/100/1000 Base T"},
        {:name => "TEST4", :phy_type => "10/100/1000 Base T"}
      ]
    },

    "N_RTU-310" => {
      :type => "RTU-310",
      :vendor => "EXFO",
      :make_model => "RTU-310",
      :description => "EXFO - Test Set",
      :nm_connection => {:name => "BrixWorx - Live", :nm_type => "BrixWorx"},
      :ports => [
        {:name => "console", :phy_type => "Other"},
        {:name => "MGMT1", :phy_type => "10/100/1000 Base T"},
        {:name => "MGMT2", :phy_type => "10/100/1000 Base T"},
        {:name => "TEST1", :phy_type => "10/100/1000 Base T"},
        {:name => "TEST2", :phy_type => "10/100/1000 Base T"}
      ]
    },

    "N_smb-6000" => {
      :type => "smb-6000",
      :vendor => "SPIRENT",
      :make_model => "SMB 6000",
      :description => "SmartBits 6000",
      :nm_connection => nil,
      :ports => [
        {:name => "A1-01", :phy_type => "10/100/1000 Base T"},
        {:name => "A1-02", :phy_type => "10/100/1000 Base T"},
        {:name => "A1-03", :phy_type => "10/100/1000 Base T"},
        {:name => "A1-04", :phy_type => "10/100/1000 Base T"},
        {:name => "A1-05", :phy_type => "10/100/1000 Base T"},
        {:name => "A1-06", :phy_type => "10/100/1000 Base T"},
        {:name => "A2-01", :phy_type => "10/100/1000 Base T"},
        {:name => "A2-02", :phy_type => "10/100/1000 Base T"},
        {:name => "A2-03", :phy_type => "10/100/1000 Base T"},
        {:name => "A2-04", :phy_type => "10/100/1000 Base T"},
        {:name => "A2-05", :phy_type => "10/100/1000 Base T"},
        {:name => "A2-06", :phy_type => "10/100/1000 Base T"},
        {:name => "console", :phy_type => "Other"},
        {:name => "MGMT1", :phy_type => "10/100/1000 Base T"}
      ]
    },

    "N_cisco-3750" => {
      :type => "cisco-3750",
      :vendor => "Cisco",
      :make_model => "WS-C3750-24TS-E",
      :description => "Cisco 3750 24TS-E",
      :nm_connection => nil,
      :ports => [
        {:name => "console to Sw #1 - Master", :phy_type => "Other"},
        {:name => "console to Sw #2 - Non Master", :phy_type => "Other"},
        {:name => "Fa1/0/1", :phy_type => "10/100/1000 Base T"},
        {:name => "Fa1/0/2", :phy_type => "10/100/1000 Base T"},
        {:name => "Fa1/0/15", :phy_type => "10/100/1000 Base T"},
        {:name => "Fa1/0/19", :phy_type => "10/100/1000 Base T"},
        {:name => "Fa1/0/21", :phy_type => "10/100/1000 Base T"},
        {:name => "Fa1/0/22", :phy_type => "10/100/1000 Base T"},
        {:name => "Fa1/0/23", :phy_type => "10/100/1000 Base T"},
        {:name => "Fa1/0/24", :phy_type => "10/100/1000 Base T"},
        {:name => "Fa2/0/1", :phy_type => "10/100/1000 Base T"},
        {:name => "Fa2/0/2", :phy_type => "10/100/1000 Base T"},
        {:name => "Fa2/0/14", :phy_type => "10/100/1000 Base T"},
        {:name => "Fa2/0/15", :phy_type => "10/100/1000 Base T"},
        {:name => "Fa2/0/20", :phy_type => "10/100/1000 Base T"}
      ]
    },

    "N_Dell-PowerEdge-R410" => {
      :type => "Dell-PowerEdge-R410",
      :vendor => "Dell",
      :make_model => "PowerEdge-R410",
      :description => "Dell - PowerEdge R410-server",
      :nm_connection => nil,
      :ports => [
        {:name => "Gi - 1", :phy_type => "10/100/1000 Base T"},
        {:name => "Gi - 2", :phy_type => "10/100/1000 Base T"},
        {:name => "Gi - 3", :phy_type => "10/100/1000 Base T"},
        {:name => "Gi - 4", :phy_type => "10/100/1000 Base T"}
      ]
    },

    "N_Terminal-Server-2511" => {
      :type => "Terminal-Server-2511",
      :vendor => "Cisco",
      :make_model => "Cisco AS2511-RJ",
      :description => "Cisco Terminal Server",
      :nm_connection => nil,
      :ports => [
        {:name => "2001", :phy_type => "Other"},
        {:name => "2002", :phy_type => "Other"},
        {:name => "2003", :phy_type => "Other"},
        {:name => "2004", :phy_type => "Other"},
        {:name => "2005", :phy_type => "Other"},
        {:name => "2006", :phy_type => "Other"},
        {:name => "2007", :phy_type => "Other"},
        {:name => "2008", :phy_type => "Other"},
        {:name => "2009", :phy_type => "Other"},
        {:name => "2010", :phy_type => "Other"},
        {:name => "2011", :phy_type => "Other"},
        {:name => "2012", :phy_type => "Other"},
        {:name => "2013", :phy_type => "Other"},
        {:name => "2014", :phy_type => "Other"},
        {:name => "2015", :phy_type => "Other"},
        {:name => "2016", :phy_type => "Other"},
        {:name => "E0", :phy_type => "10/100/1000 Base T"}
      ]
    },

    "N_Juniper-SSG-5" => {
      :type => "Juniper-SSG-5",
      :vendor => "Juniper",
      :make_model => "SSG 5",
      :description => "Juniper Netscreen SSG 5",
      :nm_connection => nil,
      :ports => [
        {:name => "bgroup0-ethernet0/2", :phy_type => "10/100/1000 Base T"},
        {:name => "console", :phy_type => "Other"},
        {:name => "ethernet0/0", :phy_type => "10/100/1000 Base T"}
      ]
    },

    "N_smb-600" => {
      :type => "smb-600",
      :vendor => "SPIRENT",
      :make_model => "SMB 600",
      :description => "SmartBits 600",
      :nm_connection => nil,
      :ports => []
    },

    "N_Juniper-SSG-140" => {
      :type => "Juniper-SSG-140",
      :vendor => "Juniper",
      :make_model => "SSG 140",
      :description => "Juniper Netscreen SSG 140",
      :nm_connection => nil,
      :ports => []
    },

    # Yoigo
    "N_SIU" => {
      :type => "SIU",
      :vendor => "Ericsson",
      :make_model => "SIU",
      :description => "SIU",
      :nm_connection => {:name => "OSS-RC_1", :nm_type => "OSS-RC"},
      :ports => [
        {:name => "ge0 WAN", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "ge1", :phy_type => "10/100/1000 Base T"},
        {:name => "so0", :phy_type => "Other"},
        {:name => "so1", :phy_type => "Other"},
        {:name => "so2", :phy_type => "Other"},
      ]
    },
    "N_RBS" => {
      :type => "RBS",
      :vendor => "Ericsson",
      :make_model => "RBS",
      :description => "RBS",
      :nm_connection => {:name => "OSS-RC_1", :nm_type => "OSS-RC"},
      :ports => [
        {:name => "ge1", :phy_type => "10/100/1000 Base T"},
        {:name => "so0", :phy_type => "Other"},
        {:name => "so1", :phy_type => "Other"},
        {:name => "so2", :phy_type => "Other"},
        {:name => "radio-if", :phy_type => "Other"}
      ],
      },
      "N_MINI-LINK" => {
      :type => "MINI-LINK TN",
      :vendor => "Ericsson",
      :make_model => "TBD",
      :description => "MINI-LINK TN",
      :nm_connection => {:name => "IPT-NMS_1", :nm_type => "IPT-NMS"},
      :ports => [
        {:name => "MW-1", :phy_type => "Other"},
        {:name => "MW-2", :phy_type => "Other"},
        {:name => "MW-3", :phy_type => "Other"},
        {:name => "MW-4", :phy_type => "Other"},
        {:name => "11/1", :phy_type => "1000Base-LX; 1310nm; SMF"},
        {:name => "11/2", :phy_type => "1000Base-LX; 1310nm; SMF"}
      ]
    },
  }

end
