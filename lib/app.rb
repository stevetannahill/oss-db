# Is this a custom app? ie :coresite
module App

  def self.set(app)
    @app = app
  end

  def self.is?(app)
    @app == app
  end

  def self.app_name
    @app
  end
  
end
