# Widgets
dir = File.dirname(__FILE__)
require "#{dir}/widgets/core"
require "#{dir}/widgets/css_template"
require "#{dir}/widgets/highlightable"
require "#{dir}/widgets/disableable"

##### Navigation #####
require "#{dir}/widgets/navigation_item"
require "#{dir}/widgets/navigation"
require "#{dir}/widgets/navigation_helper"
ActionController::Base.helper Widgets::NavigationHelper

##### Tabnav #####
require "#{dir}/widgets/tab"
require "#{dir}/widgets/tabnav"
require "#{dir}/widgets/tabnav_helper"
ActionController::Base.helper Widgets::TabnavHelper

##### Table #####
require "#{dir}/widgets/table_helper"
ActionController::Base.helper Widgets::TableHelper

##### Code #####
# not enabled by default because it depends on the Syntax gem
# require "#{dir}/widgets/code_helper"
# ActionController::Base.helper Widgets::CodeHelper

##### ShowHide #####
require "#{dir}/widgets/showhide_helper"
ActionController::Base.helper Widgets::ShowhideHelper

##### Tooltip #####
require "#{dir}/widgets/tooltip_helper"
ActionController::Base.helper Widgets::TooltipHelper

##### Progressbar #####
require "#{dir}/widgets/progressbar_helper"
ActionController::Base.helper Widgets::ProgressbarHelper

##### Spiffy Corners #####
require "#{dir}/widgets/spiffy_corners/spiffy_corners_helper"
ActionController::Base.helper Widgets::SpiffyCorners::SpiffyCornersHelper

##### UtilsHelper #####
require "#{dir}/widgets/utils_helper"
ActionController::Base.helper Widgets::UtilsHelper

