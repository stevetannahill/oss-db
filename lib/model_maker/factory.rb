module ModelMaker

  # To create an object using the Factory you must go through 4 steps
  # new > create > save > finalize
  #      |--  build   --|
  # |-- Factory#make  --|
  # |--    Factory#produce     --|

  #Generic Factory class that handles all the generic data and creation of ONLY the associated ActiveRecord model
  # To use this class, create a subclass that inherits from this class
  # fill the attributes and relations with the data for that object
  class Factory
    
    #------- Class Level Data --------#

    #attributes is a list of symbols of attributes (columns in the object's database
    # which are not part of a relationship) for the object.
    def self.attributes(attributes=nil)
      #Set the attributes
      @attributes ||= [:id, :created_at, :updated_at, :cenx_id, :cenx_name_prefix, :type]
      @attributes = (@attributes | attributes).uniq unless attributes.nil?

      #Return the attributes
      parent_attributes = (superclass.respond_to?(:attributes) ? superclass.attributes : [])
      attrs = (@attributes + parent_attributes).flatten.uniq
      return attrs.flatten
    end
    
    #relations is a list of symbols of relationships with other objects. This should
    # be without the _id suffix. NOTE: These do NOT include many-to-many relationships
    # (those are stored in a separate table)
    def self.relations(relations=nil)
      @relations ||= []
      @relations = (@relations | relations).uniq unless relations.nil?

      parent_relations = (superclass.respond_to?(:relations) ? superclass.relations : [])
      rels = (@relations + parent_relations).flatten.uniq
      return rels.flatten
    end

    #metadata is a list of symbols of symbols that are used in the order hash
    # that does not correspond with an attribute or a relation in the model
    def self.metadata(metadata=nil, should_inherit=true)
      parent_metadata = (superclass.respond_to?(:metadata) ? superclass.metadata : [])
      @metadata ||= []
      @metadata = @metadata | parent_metadata if should_inherit
      @metadata = @metadata | metadata unless metadata.nil?
      @metadata = @metadata.flatten.uniq
      return @metadata
    end

    #Set's the Factory's ActiveRecord model (if provided) and returns the model
    # (can be used to just retrieve the model if passed nil)
    def self.model(model=nil)
      @model = model unless model.nil?
      return @model
    end

    #Returns a list of all attributes and relations
    def self.params
      return attributes + relations
    end

    #Returns a list of all attributes and relations with their _id suffix
    def self.column_names
      columns = params.collect {|param| get_column_name(param)}
      return columns
    end

    #Returns the column name of the provided param name.
    def self.get_column_name(param)
      if relations.include? param
        return "#{param}_id".to_sym
      else
        return param
      end
    end

    #Determine if the Factory doesn't know about a column in the models
    def self.missing_columns
      covered_columns = column_names
      covered_columns = covered_columns.collect {|col| col.to_s}

      missing_columns = model.column_names.reject {|col| covered_columns.include? col}
      return missing_columns
    end

    #Wrapper around the Factory that covers Steps 1,2,3 and returns the factory
    def self.make(params={})
      factory = self.new(params)
      factory.build(true)
      return factory
    end

    #Wrapper around the Factory that covers all Steps in the production and returns
    # the final ActiveRecord:Base object.
    def self.produce(params)
      factory = self.make(params)
      return factory.finalize
    end

    #------- End Class Level Data ------#


    attr_reader :attributes, :relations

    #Step 1 in Factory Production
    #hash: should be a hash with keys corresponding to the values in the @attributes
    # and @relations functions.
    # * The @attribute values should simply contain their values
    # * The @relations values can be either another ModelMaker::Factory object
    #   or an ActiveRecord object representing the object, or even an integer
    #   representing the ID of the related object
    def initialize(params={})
      
      if self.class.model and self.class.missing_columns.any?
        raise BuilderError.new("Factory: #{self.class} is missing columns #{self.class.missing_columns.join(", ")} please update the Factory class to handle them.")
      end
      
      params.each do |attr, value|

        #If this param isn't an available option create an error
        if not (self.class.relations.include? attr or self.class.attributes.include? attr or self.class.metadata.include? attr)
          error_msg = "Factory: #{self.class} does not know about the attribute \"#{attr}. Please review and either drop from the request, or add it to the factory.\n"
          error_msg += "  -- Relations (#{self.class.relations.size}): #{self.class.relations.join(",")}\n"
          error_msg += "  -- Attribtues (#{self.class.attributes.size}): (#{self.class.attributes.join(",")}\n"
          error_msg += "  -- Metadata (#{self.class.metadata.size}): #{self.class.metadata.join(",")}"
          raise BuilderError.new(error_msg)
        end
      end
      
      @attributes = Hash.new
      self.class.attributes.each do |attr|
        @attributes[attr] = params[attr] unless params[attr].nil?
      end
      @relations = Hash.new
      self.class.relations.each do |rel|
        @relations[rel] = params[rel] unless params[rel].nil?
      end
    end

    #Step 2 in Factory Production
    # Creates and saves the ActiveRecord object
    def create(hook=true)
      pre_create if hook #Hook
      raise BuilderError.new("Factory: Undefined ActiveRecord::Base model for #{self.class}") if self.class.model.nil?
      unless created?
        @object = self.class.model.new #(params)
        params.each { |k,v| @object.send("#{k}=",v) }
      end
      post_create if hook #Hook
    end

    #Step 3 in Factory Production
    #Wrapper to save the ActiveRecord object, providing pre_save and post_save hooks
    #Will raise BuilderError on all validation errors.
    def save(hook=true)
      pre_save if hook #Hook
      @object.save
      raise BuilderError.new("Factory: Error saving #{@object.class}. Errors are '#{@object.errors.full_messages.split("\n").join(", ")}'") unless @object.errors.empty?
      @object.reload if built? #Reload object if it was actually saved (no validations etc)
      post_save if hook #Hook
    end

    #Step 4 and Final Step in Factory Production
    #Final function call. Should return completed object
    def finalize(hook=true)
      pre_finalize if hook #Hook
      save
      return @object
    end

    #Wrapper for Step 2 and Step 3
    #Create and save the object
    def build(hook=true)
      create(hook)
      save(hook)
    end

    #Return a hash that can be passed to an ActiveRecord::Base#new call
    def params
      params = @attributes.dup

      #Build the proper <name>_id relation for the database
      @relations.each do |name, value|
        if value.is_a? Factory
          value.build unless value.built?
          params["#{name}_id".to_sym] = value.get_id
        elsif value.is_a? ActiveRecord::Base
          params["#{name}_id".to_sym] = value.id
        else
          params["#{name}_id".to_sym] = value
        end
      end
      return params
    end

    #Populates the many-to-many relationship "relation" with values from "values"
    # values: an array filled with any of the following
    #  * An ModelMaker::Factory to be built and stored
    #  * An ActiveRecord::Base object to be added to the relation
    #  * An integer representing the ID of a row corresponding to the other object
    #    to be stored in the collection
    def populate(relation, values)

      values = [values] unless values.respond_to?(:each)

      #Add each value to the relation
      values.each do |value|
        #Retrieve the ActiveRecord object
        if value.is_a? Factory
          value.build unless value.built?
          value = value.get_id
        elsif value.is_a? ActiveRecord::Base
          value = value.id
        end

        if value.is_a? Integer
          #Construct the "other_ids" method for method others
          method = relation.to_s[0..-2] #Remove trailing 's'
          method = "#{method}_ids"
          @object.send("#{method}=", @object.send(method).push(value)) # other_ids = other_ids + value
          next
        else
          raise BuilderError.new("Factory: #{self.class}: Can not handle #{value.inspect} for a many-to-many relationship")
        end

        raise BuilderError.new("Factory: Error when attempting to populate #{@object.class}.#{relation}. Errors are '#{@object.errors.full_messages.split("\n").join(", ")}'") if not @object.valid?
      end
    end

    #Provides the Active Record object for manipulation
    # NOT RECOMMENDED
    def operate
      yield @object
    end

    protected
    #Returns whether or not the object has already been created?
    def created?
      return !@object.nil?
    end

    #Returns whether or not the object has already been built (saved to database)
    def built?
      return !@object.id.nil? if created?
    end

    #Return the id of the object
    def get_id
      return @object.id if built?
    end

    private
    def pre_create
    end
    def post_create
    end

    def pre_save
    end
    def post_save
    end

    def pre_finalize
    end
  end

  module Stateful
    def self.extended(base)
      base.attributes [:event_record_id, :sm_state, :sm_details, :sm_timestamp, :prov_name, :prov_notes, :prov_timestamp, :order_name, :order_notes, :order_timestamp]
    end
  end
end
