module ModelMaker

  class SegmentEndPointBuilder < Builder

    def initialize(name, order, segment_factory, connection_manager, handle_manager)
      @name = name
      @connection_manager = connection_manager
      @handle_manager = handle_manager
      @segment_factory = segment_factory
      super(order)
    end
    
    def handle_demarc
      demarc_order = @order[:demarc_order]

      demarc = nil
      if demarc_order[:name]
        demarc = @connection_manager.get_demarc(demarc_order[:name])
      elsif demarc_order[:id]
        demarc = Demarc.find(demarc_order[:id])
        raise BuilderError.new("Invalid demarc id #{demarc_order[:id]} on segment endpoint #{@name}") if demarc.nil?
      else
        raise BuilderError.new("Segment Endpoint #{@name}: Missing either a demarc name or a valid demarc id for order #{demarc_order.inspect}")
      end
      
      @connection_manager.assign_demarc(@name, demarc)
    end
    
    def build_segmentep
      #Create link back to Segment
      @params[:segment] = @segment_factory
      @factory = factory.make(@params)
      @factory.save(true)
    end

    def build_cos_endpoints
      return if @order[:cos_endpoints].nil?
      @order[:cos_endpoints].each do |name, data|
        cos_instance = @segment_factory.cos_instances.select{|c| c.cos_name == name}.first
        data[:cos_instance] = cos_instance
        data[:segment_end_point] = @factory
        cose = build_cos_endpoint(data)
        @connection_manager.add_cose(@name, cose)
      end

    end

    def build_cos_endpoint(params)
      CosEndPointFactory.produce(params)
    end

    def build
      handle_demarc
      build_segmentep
      build_cos_endpoints
      segmentep = @factory.finalize
      
      
      @connection_manager.add_segmentep(@name, segmentep)
      @handle_manager.add_handle(segmentep, @order[:member_handles])
      return segmentep
    end

    #Overwrite Class#new to provide proper subclass
    def self.new(name, segment_type, order, segment_factory, connection_manager, handle_manager)
      demarc_type = order[:demarc_order][:builder_type]
      builder_class = get_builder_by_type(segment_type, demarc_type)
      raise BuilderError.new("Segment Endpoint Builder: Can not find valid SegmentE Builder for #{segment_type} @ #{demarc_type}") if builder_class.nil?
      obj = builder_class.allocate
      obj.send(:initialize, name, order, segment_factory, connection_manager, handle_manager)
      obj
    end
    private
    def self.get_builder_by_type(segment_type, demarc_type)
      builders = {
        "Off Net OVC" => {
          "Uni" => OvcEndPointUniBuilder,
          "Enni" => OvcEndPointEnniBuilder,
        },
        "On Net OVC" => {
          "Enni" => OnNetOvcEndPointEnniBuilder
        },
        "On Net Router" => {
          "Enni" => OnNetRouterSubIfBuilder
        }
      }

      return builders[segment_type][demarc_type]
    end
  end

  class OvcEndPointBuilder < SegmentEndPointBuilder
    factory OvcEndPointBuilder
  end


  class OvcEndPointUniBuilder < OvcEndPointBuilder
    factory OvcEndPointUniFactory

    def build_cos_endpoint(params)
      cose = super(params)

      #Add Cos Test Vectors if applicable
      if params[:cos_test_vectors]
        params[:cos_test_vectors].each do |costv_params|
          costv_params[:cos_end_point] = cose
          case costv_params[:builder_type]
          when "Brix CosTV"
            BxCosTestVectorFactory.produce(costv_params)
          when "Spirent CosTV"
            SpirentCosTestVectorFactory.produce(costv_params)
          else
            raise BuilderError.new("Invalid CosTV builder #{costv_params[:builder_type]}")
          end
        end
      end

      return cose
    end
  end

  class OvcEndPointEnniBuilder < OvcEndPointBuilder
    factory OvcEndPointEnniFactory
  end
  
  class OnNetOvcEndPointEnniBuilder < OvcEndPointEnniBuilder
    factory OnNetOvcEndPointEnniFactory

    def build_cos_endpoints
      @enni = EnniNew.find(@order[:demarc_order][:id])
      raise BuilderError.new("Can not find ENNI with ID #{@order[:demarc_order][:id]}") if @enni.nil?

      if @enni.is_connected_to_monitoring_port
        @segment_factory.cos_instances.each do |cosi|
          params = {
            :cos_instance => cosi,
            :segment_end_point => @factory
          }
          cose = MonitorCosEndPointFactory.produce(params)
          @connection_manager.add_cose(@name, cose)
        end
      else
        super
      end
    end

    def build_cos_endpoint(params)
      CenxCosEndPointFactory.produce(params)
    end
  end

  class RouterSubIfBuilder < OnNetOvcEndPointEnniBuilder
    factory RouterSubIfFactory
  end

  class OnNetRouterSubIfBuilder < RouterSubIfBuilder
    factory OnNetRouterSubIfFactory
  end
  
  
end
