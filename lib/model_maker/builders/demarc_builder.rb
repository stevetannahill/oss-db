module ModelMaker

  class DemarcBuilder < Builder

    def initialize(order, handle_manager, bulk_order_manager)
      super(order)
      @handle_manager = handle_manager
      @bulk_order_manager = bulk_order_manager
    end

    def build
      if @order[:id].nil?
        if @order[:operator_network].is_a? String
          @params[:operator_network] = OperatorNetwork.find_by_name(@order[:operator_network])
        end

        @demarc = build_demarc
        @order[:id] = @demarc.id
        build_demarc_order
      else
        @demarc = Demarc.find(@order[:id])
      end

      @handle_manager.add_handle(@demarc, @order[:member_handles])

      return @demarc
    end
    
    def build_demarc_order
      if @order[:service_order]
        params = @order[:service_order].dup
        params[:ordered_entity] = @demarc
        params[:operator_network] = @order[:operator_network] unless params[:operator_network]
        order = order_factory.produce(params)
        params[:bulk_orders].each do |order_name|
          #puts "*** Adding Demarc Order #{order.id} #{order.title} to Bulk order on #{order_name}"
          @bulk_order_manager.add_order(order, order_name)
        end if params[:bulk_orders]
      end
    end

    def order_factory
      return DemarcOrderFactory
    end

    #Override Class#new to provide proper subclass
    def self.new(order, handle_manager, bulk_order_manager)
      demarc_type = order[:builder_type]
      builder_class = get_builder_by_type(demarc_type)
      raise BuilderError.new("DemarcBuilder: Can not determine proper DemarcBuilder for Demarc type #{demarc_type}") if builder_class.nil?
      obj = builder_class.allocate
      obj.send(:initialize, order, handle_manager, bulk_order_manager)
      obj
    end
    private
    def self.get_builder_by_type(type)
      builders = {
        "Enni" => EnniBuilder,
        "Uni" => UniBuilder
      }

      return builders[type]
    end
  end

  class UniBuilder < DemarcBuilder
    def build_demarc
      UniFactory.produce(@params)
    end

    def order_factory
      return DemarcOrderFactory
    end
  end

  class EnniBuilder < DemarcBuilder
    def build_demarc
      
      #If Exchange was build by ModelMaker
      if @order[:site].is_a? String
        @params[:site] = Site.find_by_name(@order[:site])
      end
      
      @factory = EnniFactory.make(@params)
      
      if @order[:ports]
        ports = []
        @order[:ports].each do |port_connection|
          if port_connection.is_a? Array
            node_order, port_order = port_connection
            node = Node.find_by_name(node_order)
            raise BuilderError.new("EnniBuilder: Could not find a node by the name of #{node_order}") if node.nil?

            port = nil
            if port_order.is_a? Hash
              port_order[:node] = node
              port = PortFactory.make(port_order)
            elsif port_order.is_a? String
              port = node.ports.find_by_name(port_order)
            end
            raise BuilderError.new("EnniBuilder: Could not find a port by the name of #{port_order} on node #{node_order}") if port.nil?
          end
          ports << port
        end
        @factory.populate(:ports, ports)
      end

      return @factory.finalize
      
    end

    def order_factory
      return EnniOrderFactory
    end
  end

end
