module ModelMaker

  class SegmentBuilder < Builder

    def initialize(name, order, path_builder)
      super(order)
      @name = name
      @path_factory = path_builder.path_factory
      @connection_manager = path_builder.connection_manager
      @handle_manager = path_builder.handle_manager
      @bulk_order_manager = path_builder.bulk_order_manager
    end

    def default_params
      return {:segment_owner_role => "Seller"}
    end

    def build_segment
      @factory = factory.new(@params)
      @factory.create
      @factory.populate(:paths, @path_factory)
    end

    def build_order
      return if @order[:service_order].nil? #No order to be made
      params = @order[:service_order].dup
      params[:path] = @path_factory
      params[:ordered_entity] = @factory
      params[:operator_network] = @order[:path_network]
      order = build_segment_order(params)
      params[:bulk_orders].each do |path_name|
        #puts "*** Adding Segment Order #{order.id} #{order.title} to Bulk order on #{path_name}"
        @bulk_order_manager.add_order(order, path_name)
      end if params[:bulk_orders]
      
    end

    def build_cosis
      est_id = @order[:ethernet_service_type]
      ethernet_service_type = EthernetServiceType.find(est_id)
      raise BuilderError.new("Segment Builder: Can not find Ethernet Service Type with ID #{est_id}") if ethernet_service_type.nil?
      
      cos_names = @order[:segment_endpoints].collect do |segmentep_name, segmentep|
        segmentep[:cos_endpoints].collect{|cos_name, cose| cos_name} unless segmentep[:cos_endpoints].nil?
      end.flatten.uniq.compact

      cos_names.each do |cos_name|
        cost = ethernet_service_type.class_of_service_types.select{|cost| cost.name == cos_name}.first
        raise BuilderError.new("Cos Type #{cos_name} not found for Ethernet Service Type #{ethernet_service_type.inspect}") if cost.nil?
        build_cosi(cost)
      end
    end

    def build_endpoints
      @order[:segment_endpoints].each do |segmentep_name, segmentep_order|
        segmentep = SegmentEndPointBuilder.build(segmentep_name, @order[:builder_type], segmentep_order, @factory, @connection_manager, @handle_manager)
      end
    end

    def build
      build_segment
      build_order
      build_cosis
      build_endpoints
      
      segment = @factory.finalize
      @handle_manager.add_handle(segment, @order[:member_handles])
      return segment
    end

    #Override Class#new to provide proper subclass
    def self.new(name, order, path_builder)
      segment_type = order[:builder_type]
      builder_class = get_builder_by_type(segment_type)
      raise BuilderError.new("Segment Builder: Can not determine proper SegmentBuilder for Segment type #{segment_type}") if builder_class.nil?
      obj = builder_class.allocate
      obj.send(:initialize, name, order, path_builder)
      obj
    end
    private
    def self.get_builder_by_type(type)
      builders = {
        "Off Net OVC" => OffNetOvcBuilder,
        "On Net OVC" => OnNetOvcBuilder,
        "On Net Router" => OnNetRouterBuilder,
      }

      return builders[type]
    end
    
  end

  class OvcBuilder < SegmentBuilder
  end

  class OffNetOvcBuilder < OvcBuilder
    factory OffNetOvcFactory

    def build_segment_order(params)
      OffNetOvcOrderFactory.produce(params)
    end

    def build_cosi(cost)
      CosInstanceFactory.produce({:class_of_service_type => cost, :segment => @factory})
    end
  end

  class OnNetSegmentBuilder < SegmentBuilder
  end

  class OnNetOvcBuilder < OnNetSegmentBuilder
    factory OnNetOvcFactory

    def build_segment_order(params)
      OnNetOvcOrderFactory.produce(params)
    end

    def build_cosi(cost)
      CenxCosInstanceFactory.produce({:class_of_service_type => cost, :segment => @factory})
    end
  end

  class OnNetRouterBuilder < OnNetSegmentBuilder
    factory OnNetRouterFactory

    def build_segment_order(params)
      OnNetOvcOrderFactory.produce(params)
    end

    def build_cosi(cost)
      CenxCosInstanceFactory.produce({:class_of_service_type => cost, :segment => @factory})
    end
  end
end
