module ModelMaker

  class Circuit
    attr_accessor :paths, :sites, :demarcs, :error, :successful
  end

  class CircuitBuilder < Builder

    def build
      circuit = Circuit.new
      @handle_manager = MemberHandleManager.new
      @bulk_order_manager = BulkOrderManager.new
      build_bulk_orders

      circuit.sites = build_sites
      circuit.demarcs = build_demarcs
      circuit.paths = build_paths

      @bulk_order_manager.connect_orders
      @handle_manager.build_handles
      return circuit
    end

    def build_bulk_orders
      return if @order[:bulk_orders].nil?
      
      @order[:bulk_orders].each do |name, order|
        params = order.dup
        bulk_order = BulkOrderFactory.produce(params)
        #puts "Build Bulk Order with name #{name}"
        @bulk_order_manager.add_bulk_order(name, bulk_order)
      end
    end

    def build_sites
      return [] if @order[:sites].nil?

      sites = Array.new
      @order[:sites].each do |site_order|
        site = SiteBuilder.build(site_order)
        sites << site
      end

      return sites
    end

    def build_demarcs
      return [] if @order[:demarcs].nil?

      demarcs = Array.new
      @order[:demarcs].each do |name, demarc_order|
        demarc = DemarcBuilder.build(demarc_order, @handle_manager, @bulk_order_manager)
        demarcs << demarc
      end

      return demarcs
    end

    def build_paths
      return [] if @order[:paths].nil?

      paths = Array.new
      @order[:paths].each do |path_name, path_order|
        path = PathBuilder.build(path_name, path_order, @order[:demarcs], @handle_manager, @bulk_order_manager)
        paths << path
      end

      return paths
    end

  end
end
