module ModelMaker
  class SiteBuilder < Builder
    factory SiteFactory

    def build
      build_operator_network
      build_site_factory
      build_site_groups
      @site = @sitefactory.finalize
      seed_site
      return @site
    end

    def build_operator_network
      if @order[:operator_network].is_a? Hash
        operator_network = OperatorNetworkFactory.produce(@order[:operator_network])
        @params[:operator_network] = operator_network
      end
    end

    def build_site_factory
      @sitefactory = factory.make(@params)
    end

    def build_site_groups
      return if @order[:site_groups].nil?
      site_groups = []
      @order[:site_groups].each do |site_group|
        if site_group.is_a? String
          name = site_group
          site_group = SiteGroup.find_by_name(name)
          site_group = SiteGroupFactory.make({:name => name}) if site_group.nil?
        end

        site_groups << site_group
      end

      @sitefactory.populate(:site_groups, site_groups)
    end

    def seed_site
      if @site.can_seed
        @site.seed
      else
        raise BuilderError.new("SiteBuilder: Error can not seed site #{@site.name}")
      end
    end
    
    #Override Class#new to provide proper subclass
    def self.new(order)
      pop_type = order[:builder_type]
      builder_class = get_builder_by_type(pop_type)
      raise BuilderError.new("Pop Builder: Can not determine proper PopBuilder for Pop type #{pop_type}") if builder_class.nil?
      obj = builder_class.allocate
      obj.send(:initialize, order)
      obj
    end
    private
    def self.get_builder_by_type(type)
      builders = {
        "Site" => SiteBuilder,
        "AggregationSite" => AggregationSiteBuilder
      }

      return builders[type]
    end
  end

  class AggregationSiteBuilder < SiteBuilder
    factory AggregationSiteFactory
  end
end
