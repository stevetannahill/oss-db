module ModelMaker

  def self.quick?
    !ActsAsValidationsOnDemand.run_validations? && ReindexSphinx.status == :ignore
  end

  def self.quick=(val)
    ActsAsValidationsOnDemand.run_validations = !val
    ReindexSphinx.status = val ? :ignore : :live
  end

  class Builder

    #Generic higher level Builder class. This class is responsible for the use
    # of a Factory object to create the Active Record model but also to handle the
    # associations and creations of other objects and to pass work to other
    # Builder classes.
    #

    #Allows the builder to store the factory for the object to be built
    def self.factory(factory=nil)
      @factory = factory unless factory.nil?
      return @factory
    end
    
    def factory
      return self.class.factory
    end

    #Wrapper which allows building an object straight from the class
    def self.build(*args)
      builder = self.new(*args)
      builder.build
    end

    #Default Initializer
    def initialize(order)
      @order = order
      @params = default_params.merge(order)
    end

    #Must be overwritten by child class
    def build
    end

    #Override function to allow children to provide default parameters
    def default_params
      return {}
    end
    
  end
end