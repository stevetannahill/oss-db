module ModelMaker

  #Circuit Building
  def self.build(order)
    circuit = Circuit.new

    begin
      ActiveRecord::Base.transaction do
        circuit = CircuitBuilder.build(order)
      end
      circuit.successful = true
    rescue BuilderError => e
      circuit.error = e
      circuit.successful = false
      SW_ERR e.message
    end

    return circuit
  end

  def self.execute(recipes)
    results = []

    
    recipes.each do |r|
      recipe = r.dup
      result = {
        :data => {
          :__type => nil, 
          :public_id => nil,
        }, 
        :error => {
          :error_id => nil,
          :message => nil,
          :object => nil,
        }
      }
      begin
        version = recipe.delete(:__version)
        if version.nil? or version != "1"
          raise BuilderError.new "Could not execute recipe due to invalid version (#{version}) in recipe #{recipe}"
        end

        strategy_type = recipe.delete(:__type)
        if strategy_type.nil?
          raise BuilderError.new  "Could not find a valid strategy type (#{strategy_type}) in recipe #{recipe}"
        end

        strategy = ModelMaker::Strategies.const_get(strategy_type)
        if strategy.nil?
          raise BuilderError.new "Could not find valid strategy with name #{strategy_type}"
        else
          result[:data][:__type] = strategy_type
        end

        object = strategy.execute(recipe)
        result[:data][:public_id] = object.public_id

      rescue BuilderError => e
        SW_ERR e.message
        result[:error][:error_id] = 501
        result[:error][:message] = e.message
        result[:error][:object] = e
      ensure
        results << result
      end
    end
    return results
  end

  #Error Handling
  class BuilderError < StandardError
  end
  
end
