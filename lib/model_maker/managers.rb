module ModelMaker
  class ConnectionManager

    #The ConnectionManager handles the interconnections between Segment endpoints
    # and their corresponding COS endpoints
    #
    #The connections between these objects are to be done after all objects are
    # created and finalized and have been provided to the ConnectionManager
    #
    #A simple way to think of this ConnectionManager would be a filing cabinet
    # During initialization the ConnectionManager creates all the necessary files and
    # throughout the building of the Path when certain objects are finished building
    # you provide them to the ConnectionManager to be filed in the proper location
    #
    # After everything has been built the ConnectionManager will use these objects
    # and create the interconnections between them.
    #
    #The @connections hash has the following structure
    # {
    #   <Segment Endpoint Name> => {
    #       :connected => [<Other Segment Endpoint Name 1>, <Other Segment Endpoint Name 2>, ...],
    #       :object => <SegmentEndPoint>,
    #       :demarc => <Demarc>,
    #       :cos_endpoints => {
    #           <COS Endpoint Name> => {
    #               :connected => [[<Other Segment Endpoint Name 1>, <Other COS Endpoint Name 1>],
    #                              [<Other Segment Endpoint Name 2>, <Other COS Endpoint Name 2>] ... ],
    #               :object => <CosEndPoint>
    #           },
    #           .
    #           .
    #           .
    #
    #       }
    #   },
    #   .
    #   .
    #   .
    # }
    #
    #

    
    attr_reader :connections

    #Builds the basic structure of the @connections hash and fills in all
    # connection information
    def initialize(path_order, data = {})
      @connections = data[:connections] || Hash.new
      @demarcs = data[:demarcs] || Hash.new

      #Build Connections
      path_order[:segments].each do |segment_name, segment_order|
        segment_order[:segment_endpoints].each do |name, segmentep_order|

          @connections[name] = Hash.new
          @connections[name][:connected] = (segmentep_order[:connected].nil? ? [] : segmentep_order[:connected])
          @connections[name][:cos_endpoints] = Hash.new

          if segmentep_order[:id]
            segmentep = SegmentEndPoint.find(segmentep_order[:id])
            @connections[name][:object] = segmentep
            @connections[name][:demarc] = segmentep.demarc
          end
          
          if not segmentep_order[:cos_endpoints].nil?
            segmentep_order[:cos_endpoints].each do |cos_name, cose_order|
              @connections[name][:cos_endpoints][cos_name] = Hash.new
              @connections[name][:cos_endpoints][cos_name][:connected] = (cose_order[:connected].nil? ? [] : cose_order[:connected])

              if cose_order[:id]
                @connections[name][:cos_endpoints][cos_name][:object] = CosEndPoint.find(cose_order[:id])
              end
            end
          end
        end if segment_order[:segment_endpoints]
      end
    end

    def add_demarcs(demarcs)      
      demarcs.each do |name, demarc_data|
        raise BuilderError.new("ConnectionManager: Missing ID of demarc #{name} : #{demarc_data.inspect}") if demarc_data[:id].nil?
        demarc = Demarc.find(demarc_data[:id])
        raise BuilderError.new("ConnectionManager: Can not find demarc by ID #{demarc_data[:id]} for #{demarc_data.inspect}") if demarc.nil?
        
        @demarcs[name] = demarc
      end
    end
    
    def get_demarc(name)
      demarc = @demarcs[name]
      raise BuilderError.new("ConnectionManager: Can not find demarc labeled #{name}") if demarc.nil?
      return demarc
    end

    def assign_demarc(segmentep_name, demarc)
      @connections[segmentep_name][:demarc] = demarc
    end

    #Add an actual SegmentEndPoint to it's place in the @connections hash
    def add_segmentep(endpoint, segmentep)
      if not @connections[endpoint].nil?
        @connections[endpoint][:object] = segmentep
      end
    end

    #Add an actual CosEndPoint to it's place in the @connections hash
    def add_cose(endpoint, cose)
      if not @connections[endpoint].nil?
        if not @connections[endpoint][:cos_endpoints][cose.cos_name].nil?
          @connections[endpoint][:cos_endpoints][cose.cos_name][:object] = cose
        end
      end
    end

    #Final function call. Will go through all objects and all connections and
    # connect everything needed.
    def build_connections
      check_sanity
      build_demarc_connections
      build_endpoint_connections
    end
    
    private

    #Before attempting to make connections we must make sure everything has been
    # instantiated and that every requested connection has a corresponding endpoint
    def check_sanity
      #Do all connections have an object?
      @connections.each do |segmentep_name, segmentep_data|
        if segmentep_data[:object].nil? or not segmentep_data[:object].is_a? SegmentEndPoint
          raise BuilderError.new("ConnectionManager: Segment endpoint instance for #{segmentep_name} not provided")
        end

        if segmentep_data[:demarc].nil? or not segmentep_data[:demarc].is_a? Demarc
          raise BuilderError.new("ConnectionManager: Segment endpoint #{segmentep_name} does not have a valid demarc provided")
        end

        segmentep_data[:cos_endpoints].each do |cos_name, cos_data|
          if cos_data[:object].nil? or not cos_data[:object].is_a? CosEndPoint
            raise BuilderError.new("ConnectionManager: COS endpoint instance for #{cos_name} not provided for Segment endpoint #{segmentep_name}")
          end
        end
      end


     #Do all connections have their ends present?
     segmentep_names = @connections.keys
     @connections.each do |segmentep_name, segmentep_data|
       segmentep_data[:connected].each do |other_segmentep|
         if not segmentep_names.include? other_segmentep
           raise BuilderError.new("ConnectionManager: Can not find requested Segment endpoint #{other_segmentep} to connect to #{segmentep_name}")
         end
       end

       segmentep_data[:cos_endpoints].each do |cos_name, cos_data|
         cos_data[:connected].each do |other_endpoint, other_name|
           if @connections[other_endpoint].nil?
             raise BuilderError.new("ConnectionManager: Can not find requested Segment endpoint #{other_endpoint} that COS endpoint #{cos_name} on Segment #{segmentep_name} is supposed to connect to")
           end
           if not @connections[other_endpoint][:cos_endpoints].keys.include? other_name
            raise BuilderError.new("ConnectionManager: Can not find requested COS endpoint #{other_name} on Segment endpoint #{other_endpoint} to connect to #{cos_name} on Segment endpoint #{segmentep_name}")
           end
         end
       end
     end
    end

    def build_demarc_connections
      @connections.each do |segmentep_name, segmentep_data|
        segmentep = segmentep_data[:object]
        demarc = segmentep_data[:demarc]
        segmentep.demarc = demarc
        raise BuilderError.new("ConnectionManager: Errors encountered while connecting Segment endpoint #{segmentep_name} to demarc. Errors for #{segmentep.class} are '#{segmentep.errors.full_messages.join(", ")}'") unless segmentep.save
        segmentep_data[:object].reload
      end
    end

    def build_endpoint_connections
      @connections.each do |segmentep_name, segmentep_data|
        connect_endpoints(segmentep_name, segmentep_data[:connected])
        segmentep_data[:cos_endpoints].each do |cos_name, cos_data|
          connect_cos_endpoints(cos_data[:object], cos_data[:connected], segmentep_name)
        end
      end
    end

    def connect_endpoints(endpoint, connections)
      segmentep = @connections[endpoint][:object]
      connections.each do |other|
        other_segmentep = @connections[other][:object]
        if not segmentep.segment_end_points.include? other_segmentep
          segmentep.segment_end_points << other_segmentep
          raise BuilderError.new("ConnectionManager: Error connecting SegmentEs #{endpoint} and #{other}. The errors occured on #{segmentep.class} #{endpoint} and are '#{segmentep.errors.full_messages.split("\n").join(", ")}'") if not segmentep.valid?
          raise BuilderError.new("ConnectionManager: Error connecting SegmentEs #{endpoint} and #{other}. The errors occured on #{other_segmentep.class} #{other} and are '#{other_segmentep.errors.full_messages.split("\n").join(", ")}'") if not other_segmentep.valid?
        end
      end
    end
    
    def connect_cos_endpoints(cose, connections, segmentep_name)
      connections.each do |other_endpoint, other_name|
        other_cose = @connections[other_endpoint][:cos_endpoints][other_name][:object]
        if not cose.cos_end_points.include? other_cose
          cose.cos_end_points << other_cose
          raise BuilderError.new("ConnectionManager: Error connecting COSEs #{cose.cos_name} on #{segmentep_name} and #{other_name} on #{other_endpoint}. The errors occured on #{cose.cos_name} and are '#{cose.errors.full_messages.split("\n").join(", ")}'") if not cose.valid?
          raise BuilderError.new("ConnectionManager: Error connecting COSEs #{cose.cos_name} on #{segmentep_name} and #{other_name} on #{other_endpoint}. The errors occured on #{other_name} and are '#{other_cose.errors.full_messages.split("\n").join(", ")}'") if not other_cose.valid?
        end
      end
    end

    public
    def to_s
      str = ""
      @connections.each do |segmentep_name, segmentep_data|
        id = segmentep_data[:object] ? segmentep_data[:object].id : "nil"
        str += "#{segmentep_name}(#{id})@#{segmentep_data[:demarc]} -> #{segmentep_data[:connected].inspect} => {\n"
        segmentep_data[:cos_endpoints].each do |cos_name, cos_data|
          id = cos_data[:object] ? cos_data[:object].id : "nil"
          str += "  #{cos_name}(#{id}) => #{cos_data[:connected].inspect}\n"
        end
        str += "}\n"
      end
      return str
    end
    
  end


  class MemberHandleManager
    #The MemberHandleManager is responsible for storing each object that will require
    # member handles and the respective values.
    #
    #The MemberHandleManager will then at the end when everything has been created
    # assign the member handle values to the corresponding pre-created (by CDB)
    # member handle
    #
    #The @member_handles hash has the following structure
    # {
    #   <InstanceElement> => [
    #       [<Owner ServiceProvider ID>, "Member Handle Value"],
    #       [<Owner ServiceProvider ID>, "Member Handle Value"], ...
    #   ],
    #   .
    #   .
    #   .
    # }
    #
    
    def initialize(data = {})
      @member_handles = data[:member_handles] || Hash.new
    end
    
    def add_handle(object, handles)
      return if handles.nil? or handles.empty? #No handles provided

      if @member_handles[object].nil?
        @member_handles[object] = handles
      else
        @member_handles[object] << handles
        @member_handles[object] = @member_handles[object].flatten.uniq
      end
    end

    def build_handles
      @member_handles.each do |entity, handles|
        #Force generation of all member_handles
        entity.save
        entity.reload

        handles.each do |owner_id, handle|
          owner = ServiceProvider.find(owner_id)
          raise BuilderError.new("Could not find Service Provider with ID #{owner_id} for member handles on #{entity}") if owner.nil?
          member_handle = entity.member_handle_instance_by_owner(owner)
          name = entity.respond_to?(:cenx_name) ? entity.cenx_name : entity.name
          if member_handle.nil?
            raise BuilderError.new("Could not find member handle for #{owner.name} on #{name}/n HANDLES: #{to_s}")
          end
          member_handle.value = handle
          raise BuilderError.new("Could not save member handle #{handle} for #{owner.name} on #{name} because #{member_handle.errors.full_messages.join(", ")}") unless member_handle.save
        end
        
        # need a save to update cenx_ids in certain cases
        entity.save
      end
    end

    def to_s
      str = ""
      @member_handles.each do |entity, handles|
        name = entity.respond_to?(:cenx_name) ? entity.cenx_name : entity.name
        str += "#{name}\n  => #{handles.inspect}\n"
      end
      return str
    end
  end

  class BulkOrderManager
    def initialize
      @bulk_orders = Hash.new
    end

    def add_bulk_order(name, order=nil)
      if @bulk_orders[name].nil?
        #puts "*** Creating Bulk Order #{name}"
        @bulk_orders[name] = Hash.new
        @bulk_orders[name][:connected] = Array.new
      end
      @bulk_orders[name][:order] = order unless order.nil?
    end

    def add_order(order, bulk_order_name)
      add_bulk_order(bulk_order_name) if @bulk_orders[bulk_order_name].nil?
      #puts "*** Adding order #{order.title} to #{bulk_order_name} - #{@bulk_orders[bulk_order_name][:connected].size}"
      @bulk_orders[bulk_order_name][:connected] << order
      #puts "*** Added order #{order.title} to #{bulk_order_name} - #{@bulk_orders[bulk_order_name][:connected].size}"
    end

    def connect_orders
      check_sanity
      
      #puts "*** Connecting orders: num BO: #{@bulk_orders.size}"

      @bulk_orders.each do |name, info|
        bulk_order = info[:order]
        #puts "*** Connecting : #{info[:connected].size} to #{bulk_order.title}"
        info[:connected].each do |order|
          #puts "*** Connecting #{order.title}"
          bulk_order.service_orders << order
        end
      end
    end

    private
    def check_sanity
      @bulk_orders.each do |name, info|
        if info[:order].nil?
          raise BuilderError.new "BulkOrderManager: Was never provided a valid bulk_order for Path named '#{name}'"
        end
      end
    end
  end
end
