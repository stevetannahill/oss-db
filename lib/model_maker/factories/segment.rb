module ModelMaker
  class SegmentFactory < Factory
    model Segment
    extend Stateful
    attributes [
      :segment_owner_role,
      :notes,
      :service_id,
      :use_member_attrs,
      :status,
      :as_id,
      :router_type,
      :router_id,
      :routing_domain
    ]
    relations [
      :site,
      :segment_type,
      :operator_network,
      :site_group,
      :ethernet_service_type,
      :path_network,
      :emergency_contact
    ]
    metadata [:segment_endpoints, :builder_type, :service_order, :member_handles]

    def cos_instances
      return @object.cos_instances if created?
    end
    
  end
  
  class OnNetSegmentFactory < SegmentFactory
    model OnNetSegment
    extend Stateful

    def pre_create
      #Generate a service_id if it isn't provided
      if @attributes[:service_id].nil?
        site = Site.find(@relations[:site])
        @attributes[:service_id] = IdTools::ServiceIdGenerator.generate_on_net_ovc_service_id site
      end
    end
  end

  class OnNetOvcFactory < OnNetSegmentFactory
    model OnNetOvc
    extend Stateful
  end

  class OnNetRouterFactory < OnNetSegmentFactory
    model OnNetRouter
    extend Stateful
  end

  class OvcFactory < SegmentFactory
    model Ovc
    extend Stateful
  end

  class OffNetOvcFactory < OvcFactory
    model OffNetOvc
    extend Stateful
  end
end
