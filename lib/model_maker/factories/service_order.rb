
module ModelMaker
  class ServiceOrderFactory < Factory
    model ServiceOrder
    attributes [
      :ordered_entity_type,
      :ordered_entity_subtype,
      :action,
      :title,
      :expedite,
      :order_state,
      :order_created_date,
      :ready_to_order_date,
      :order_received_date,
      :order_completion_date,
      :requested_service_date,
      :order_acceptance_date,
      :customer_acceptance_date,
      :billing_start_date,
      :foc_date,
      :notes,
      :ordered_object_type_type,
      :bulk_order_type,
      :status,
      :design_complete_date,
      :order_created_date, 
      :order_name,
      :order_notes,
      :order_timestamp,
      :ordered_entity_snapshot
   ]
   relations [
     :ordered_entity,
     :ordered_entity_group,
     :operator_network,
     :ordered_object_type,
     :ordered_operator_network,
     :path,
     :primary_contact,
     :testing_contact,
     :technical_contact,
     :local_contact
   ]

   metadata [:bulk_orders]
  end

  class OnNetOvcOrderFactory < ServiceOrderFactory
    model OnNetOvcOrder

    def pre_create
      #Set the :ordered_entity_type and :ordered_entity_subtype
      @attributes[:ordered_entity_type] = "Segment"
      @attributes[:ordered_entity_subtype] = "OnNetOvc"
      @attributes[:action] = "New"
    end
  end

  class OffNetOvcOrderFactory < ServiceOrderFactory
    model OffNetOvcOrder

    def pre_create
      #Set the :ordered_entity_type and :ordered_entity_subtype
      @attributes[:ordered_entity_type] = "Segment"
      @attributes[:ordered_entity_subtype] = "OffNetOvc"
      @attributes[:action] = "New"
    end
  end

  class BulkOrderFactory < ServiceOrderFactory
    model BulkOrder
    metadata [], false #Prevent BulkOrderFactory from inheriting :bulk_orders metadata

    def pre_create
      #Set the :ordered_entity_type
      @attributes[:ordered_entity_type] = "Path"
      @attributes[:ordered_entity_subtype] = "" #Honestly required! Check ServiceProviderTypes::BULK_ORDERED_ENTITY_SUBTYPES
      @attributes[:action] = "New"
    end
  end

  class DemarcOrderFactory < ServiceOrderFactory
    model DemarcOrder

    def pre_create
      #Set the :ordered_entity_type and :ordered_entity_subtype
      @attributes[:ordered_entity_type] = "Demarc"
      @attributes[:ordered_entity_subtype] = "UNI"
      @attributes[:action] = "New"
    end
  end

  class EnniOrderFactory < ServiceOrderFactory
    model EnniOrderNew

    def pre_create
      #Set the :ordered_entity_type and :ordered_entity_subtype
      @attributes[:ordered_entity_type] = "Demarc"
      @attributes[:ordered_entity_subtype] = "ENNI"
      @attributes[:action] = "New"
    end
  end
end
