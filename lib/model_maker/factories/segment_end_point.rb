module ModelMaker
  class SegmentEndPointFactory < Factory
    model SegmentEndPoint
    extend Stateful
    relations [
      :segment,
      :demarc,
      :segment_end_point_type
    ]

    attributes [
      :md_format,
      :md_level,
      :md_name_ieee,
      :ma_format,
      :ma_name,
      :is_monitored,
      :notes,
      :stags,
      :ctags,
      :oper_state,
      :eth_cfm_configured,
      :reflection_enabled,
      :mep_id,
      :ip_address,
      :routing_cost
    ]
    metadata [:cos_endpoints, :demarc_order, :connected, :member_handles]

    def post_create
      segmentep_types = @object.get_candidate_segment_end_point_types
      raise BuilderError.new("Found more than one Segment Endpoint Type: #{segmentep_types.inspect}") unless segmentep_types.one?
      segmentep_type = segmentep_types.first
      @object.segment_end_point_type = segmentep_type
    end

    def pre_save
      @object.layout_diagram_on_save = false
    end

  end

  class OvcEndPointFactory < SegmentEndPointFactory
    model OvcEndPoint
    extend Stateful
  end

  class OvcEndPointEnniFactory < OvcEndPointFactory
    model OvcEndPointEnni
    extend Stateful
  end

  class OvcEndPointUniFactory < OvcEndPointFactory
    model OvcEndPointUni
    extend Stateful
  end

  class OnNetOvcEndPointEnniFactory < OvcEndPointEnniFactory
    model OnNetOvcEndPointEnni
    extend Stateful
  end

  class RouterSubIfFactory < OnNetOvcEndPointEnniFactory
    model RouterSubIf
    extend Stateful
  end

  class OnNetRouterSubIfFactory < RouterSubIfFactory
    model OnNetRouterSubIf
    extend Stateful
  end
end
