module ModelMaker
  class SiteGroupFactory < Factory
    model SiteGroup
    attributes [
      :name
    ]
  end

end
