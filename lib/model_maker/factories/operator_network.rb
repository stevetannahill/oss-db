module ModelMaker
  class OperatorNetworkFactory < Factory
    model OperatorNetwork
    attributes [
      :name,
      :is_processing, 
      :job_type, 
      :job_id,
    ]

    relations [
      :service_provider,
      :operator_network_type,
    ]
  end
end
