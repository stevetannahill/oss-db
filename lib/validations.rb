module ActiveModel
  module Validations
  	class InclusionOfEachValidator < EachValidator
			def initialize(options)
				super(options.reverse_merge(:message => I18n.translate('errors.messages.inclusion')))
			end
			
			def validate_each(record, attribute, value)
				enum = options[:in] || options[:within]
				list = enum.is_a?(Symbol) ? record.send(enum) : enum
				value.each{|val|
					record.errors.add(attribute, options[:message]) unless list.include?(val)
				}
			end
  	end
  	
  	class AsCenxIDValidator < EachValidator
      def validate_each(record, attribute, value)
      	valid, error_string, type_string, table = IdTools::CenxIdGenerator.decode_cenx_id(value)
      	
      	# Check if the cenx_id is not valid
      	if not valid
      		SW_ERR "Invalid Cenx ID detected: #{value}"
          record.send("#{attribute}=","Error Generating ID!")
					if options[:message]
						record.errors.add(attribute, options[:message])
					else
						record.errors.add(attribute, error_string)
					end
				# Check if the cenx_id's table doesn't line up with the record's class
				elsif not record.is_a?(table)
					SW_ERR "Invalid Cenx ID to Type Mapping deteted: #{value}"
					record.attributes = { attribute => "Error Generating ID!" }
					if options[:message]
						record.errors.add(attribute, options[:message])
					else
						record.errors.add(attribute, "ID invalid type identifer")
					end
				# Check if an existing record exists that is not the record in questions with the same cenx_id
				elsif existing_records = record.class.find(:all, :conditions => ["cenx_id = ?", value]) and
							existing_records.length > 0 and existing_records.select{|r| r == record}.empty?
				  SW_ERR "Duplicate Cenx ID deteted: #{value}"
				  if options[:message]
				  	record.errors.add(attribute, options[:message])
				  else
				  	record.errors.add(attribute, "ID has already been taken")
				  end
				end
      end
  	end
  	
  	class AsListValidator < EachValidator
			def initialize(options)
        super(options.reverse_merge(:delimiter => /\s*;\s*/, :in => nil, :allow_duplicate => false))
      end

      def validate_each(record, attribute, value)
				if value != nil
					items = value.split(options[:delimiter])
					if items.empty?
						if options[:message]
							record.errors.add(attribute, options[:message])
						else
							record.errors.add(attribute, "can't be blank.")
						end
					elsif options[:in] && items.reject{|i| options[:in].include? i}.length != 0
						if options[:message]
							record.errors.add(attribute, options[:message])
						else
							record.errors.add(attribute, "must be a list of #{options[:in].join(", ")}.")
						end
					elsif !options[:allow_duplicate] && items.uniq.length != items.length
						if options[:message]
							record.errors.add(attribute, options[:message])
						else
							record.errors.add(attribute, "may not list the same item twice.")
						end
					end
				end
      end
  	end
  	
  	class RangeOfValidator < Validator
  		attr_reader :lower_bounds, :upper_bounds, :unbounds
  	
  		def initialize(options)
  			options.reverse_merge!(:unbounded => nil, :overlapping => true, :scope => nil, :unique_ends => false, :min_range => 0, :max_range => nil)
  			
        if options[:unbounded].is_a? Array
          @unbounds = " IN ('#{options[:unbounded].join("','")}')"
        elsif !options[:unbounded].nil?
          @unbounds = " = #{options[:unbounded]}"
        end
        
        attributes = options[:attributes]
        
        @lower_bounds = attributes.select{|a| attributes.index(a) % 2 == 0}
        @upper_bounds = {}
        @lower_bounds.each { |a|
          @upper_bounds[a] = attributes[attributes.index(a) + 1]
        }
  		
  			super(options)
      end

      def validate(record)
				lower_bounds.each do |l_attr_name|
					l_value = record.read_attribute_for_validation(l_attr_name)
					u_attr_name = upper_bounds[l_attr_name]
          u_value = record.read_attribute_for_validation(u_attr_name)
					#Check if there are already any errors on the attributes, because validates numericality is done first
          if record.errors[u_attr_name].empty? && record.errors[l_attr_name].empty?
            if !options[:overlapping]
							connection = record.connection
							finder_class = record.class
              
              l_sql_attribute = "#{record.class.quoted_table_name}.#{connection.quote_column_name(l_attr_name)}"
              u_sql_attribute = "#{record.class.quoted_table_name}.#{connection.quote_column_name(u_attr_name)}"
                            s_condition_sql = options[:unique_ends] ?
                "(#{(unbounds ? (l_sql_attribute + unbounds + " OR " + "NOT (?" + unbounds + ") AND ") : "") + l_sql_attribute} <= ?) AND (#{(unbounds ? u_sql_attribute + unbounds + " OR " + "NOT (?" + unbounds + ") AND "  : "") + u_sql_attribute} >= ?)" :
                "(#{(unbounds ? (l_sql_attribute + unbounds + " OR " + "NOT (?" + unbounds + ") AND ")  : "") + l_sql_attribute} < ?) AND (#{(unbounds ? u_sql_attribute + unbounds + " OR " + "NOT (?" + unbounds + ") AND "  : "") + u_sql_attribute} > ?)"
                b_condition_sql = "(#{(unbounds ? "?" + unbounds + " OR " : "") + l_sql_attribute} >= ?) AND (#{(unbounds ? "?" + unbounds + " OR " : "") + u_sql_attribute} <= ?)"
              l_condition_params = unbounds ? [l_value, l_value.to_f, l_value, l_value.to_f] : [l_value.to_f, l_value.to_f]
              u_condition_params = unbounds ? [u_value, u_value.to_f, u_value, u_value.to_f] : [u_value.to_f, u_value.to_f]
              b_condition_params = unbounds ? [l_value, l_value.to_f, u_value, u_value.to_f] : [l_value.to_f, u_value.to_f]
              condition_params = []
              condition_sql = ""
              
              if scope = options[:scope]
                Array(scope).map do |scope_item|
                  scope_value = record.send(scope_item)
                  condition_sql << " AND " << record.class.send(:sanitize_sql_hash,{scope_item => scope_value},record.class.table_name) #attribute_condition("#{record.class.quoted_table_name}.#{scope_item}", scope_value)
                end
              end

              unless record.new_record?
                condition_sql << " AND #{record.class.quoted_table_name}.#{record.class.primary_key} <> ?"
                condition_params << record.send(:id)
              end

              l_condition_params = l_condition_params + condition_params
              u_condition_params = u_condition_params + condition_params
              b_condition_params = b_condition_params + condition_params

              l_condition_sql = s_condition_sql + condition_sql
              u_condition_sql = s_condition_sql + condition_sql
              b_condition_sql = b_condition_sql + condition_sql
                            
							if !l_value.nil? && l_value != "" && finder_class.exists?([l_condition_sql, *l_condition_params])
								record.errors.add(l_attr_name, "is within another range.", :default => options[:message], :value => l_value)
							end
							if !u_value.nil? && u_value != "" && finder_class.exists?([u_condition_sql, *u_condition_params])
								record.errors.add(u_attr_name, "is within another range.", :default => options[:message], :value => u_value)
							end
							if !u_value.nil? && u_value != "" && !l_value.nil? && l_value != "" && finder_class.exists?([b_condition_sql, *b_condition_params])
								record.errors.add(l_attr_name, "encapsulates another range.", :default => options[:message], :value => l_value)
								record.errors.add(u_attr_name, "encapsulates another range.", :default => options[:message], :value => u_value)
							end
            end
            
            if (!options[:unbounded] || !((options[:unbounded].is_a? Array) ? ((options[:unbounded].include? l_value) || (options[:unbounded].include? u_value)) : (options[:unbounded] == l_value || options[:unbounded] == u_value))) && !l_value.nil? && l_value != "" && !u_value.nil? && u_value != ""
              if l_value.to_f > u_value.to_f
                if options[:message]
                  record.errors.add(l_attr_name, options[:message])
                  record.errors.add(u_attr_name, options[:message])
                else
                  record.errors.add(l_attr_name, "must be smaller then #{u_attr_name} which is #{u_value}.")
                  record.errors.add(u_attr_name, "must be larger then #{l_attr_name} which is #{l_value}.")
                end
              elsif options[:min_range] && u_value.to_f - l_value.to_f < options[:min_range]
                if options[:message]
                  record.errors.add(l_attr_name, options[:message])
                  record.errors.add(u_attr_name, options[:message])
                else
                  record.errors.add(l_attr_name, "must be at least #{options[:min_range]} smaller then #{u_attr_name} which is #{u_value}.")
                  record.errors.add(u_attr_name, "must be at least #{options[:min_range]} larger then #{l_attr_name} which is #{l_value}.")
                end
              elsif options[:max_range] && u_value.to_f - l_value.to_f > options[:max_range]
                if options[:message]
                  record.errors.add(l_attr_name, options[:message])
                  record.errors.add(u_attr_name, options[:message])
                else
                  record.errors.add(l_attr_name, "must be at most #{options[:max_range]} smaller then #{u_attr_name} which is #{u_value}.")
                  record.errors.add(u_attr_name, "must be at most #{options[:max_range]} larger then #{l_attr_name} which is #{l_value}.")
                end
              end
            end
          end
				end
      end
  	end
  	
  	class ClassOfValidator < EachValidator
  		def initialize(options)
  			super(options.reverse_merge(:not_a_kind_of => nil, :kind_of => nil, :not_in => nil, :not_a => nil, :is_a => nil, :is_not => nil, :without => nil))
      end

      def constantize(name)
        return name unless name.kind_of?(String)
        Kernel.const_get name
      end

      def validate_each(record, attribute, value)
      	#compare exact class (sub classes fail)
				if options[:is] && value.class != constantize(options[:is])
					record.errors.add(attribute, "must be a #{options[:is]}")
				end
				if options[:is_not] && value.class == constantize(options[:is_not])
					record.errors.add(attribute, "must not be a #{options[:is_not]}")
				end
				if options[:in] && options[:in].select{|kind| value.class == constantize(kind)}.empty?
					record.errors.add(attribute, "must be a one of #{options[:in].join(", ")}")
				end
				if options[:not_in] && options[:not_in].select{|kind| value.class != constantize(kind)}.empty?
					record.errors.add(attribute, "must be one of #{options[:not_in].join(", ")}")
				end
				#is_a comparissons aka accepts subclasses
				if options[:is_a] && !(value.is_a? constantize(options[:is_a]))
					record.errors.add(attribute, "must be a kind of #{options[:is_a]} (is_a)")
				end
				if options[:kind_of] && !(value.is_a? constantize(options[:kind_of]))
					record.errors.add(attribute, "must be a kind of #{options[:kind_of]} (kind_of)")
				end
				if options[:not_a] && (value.is_a? constantize(options[:not_a]))
					record.errors.add(attribute, "must not be a kind of #{options[:not_a]}")
				end
				if options[:not_a_kind_of] && (value.is_a? constantize(options[:not_a_kind_of]))
					record.errors.add(attribute, "must not be a kind of #{options[:not_a_kind_of]}")
				end
				if options[:within] && options[:within].select{|kind| !value.is_a? constantize(kind)}.empty?
					record.errors.add(attribute, "must be a kind of #{options[:within].join(", ")} (within)")
				end
				if options[:without] && options[:without].select{|kind| value.is_a? constantize(kind)}.empty?
					record.errors.add(attribute, "must not be a kind of #{options[:without].join(", ")}")
				end
      end
  	end
  
    module HelperMethods
    	def validate_inclusion_of_each(*attr_names)
    		validates_with InclusionOfEachValidator, _merge_attributes(attr_names)
    	end
    	
    	def validates_as_cenx_id(*attr_names)
    		validates_with AsCenxIDValidator, _merge_attributes(attr_names)
    	end
    
    	def validates_as_list(*attr_names)
    		validates_with AsListValidator, _merge_attributes(attr_names)
    	end
    	
    	def validates_range_of(*attr_names)
    		options = _merge_attributes(attr_names)
    	
    		options[:attributes].each do |attribute|
					numericality_options = options.merge(:unless => Proc.new { |n|
						options[:unbounded] && ((options[:unbounded].is_a? Array) ? (options[:unbounded].include? n.send(attribute)) : (options[:unbounded] == n.send(attribute)))
					})
					numericality_options[:attributes] = Array.wrap(attribute)
				
					validates_with NumericalityValidator, numericality_options
    		end
    		
    		validates_with RangeOfValidator, options
    	end
    	
    	def validates_class_of(*attr_names)
    		validates_with ClassOfValidator, _merge_attributes(attr_names)
    	end
    end
  end
end
