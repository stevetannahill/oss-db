module CoreSite
  module Authentication
    class Filter            
      @@config = nil
      class << self
        def configure(config)
          @@config = config
        end
        
        def filter(controller)
          
          if controller.session['cas_user'].blank? || controller.params[:x]
            #tenant=1698&email=Michael.Byrne@coresite.com&site=9&order=True&view=True&admin=True&time=16102013-15:40:15
            if controller.params[:x]              
              decoded_params = Rack::Utils.parse_nested_query(CsCrypt.coresite_decode(controller.params[:x])) rescue {} # resuce incase decode returns non UTF-8 chars
              if ["tenant", "email", "site", "order", "view", "admin", "time"].all? {|attr| decoded_params[attr]}                
                user = User.find_by_email(decoded_params["email"])
                sp = nil
                on = OperatorNetwork.find_by_id(decoded_params["tenant"])
                sp = on.service_provider if on
                coresite_user = (decoded_params["tenant"].to_i == -1) && (decoded_params["site"].to_i == -1)
                sp = ServiceProvider.system_owner if coresite_user
                expire_time = Time.strptime(decoded_params["time"] + " UTC", "%d%m%Y-%H:%M:%S %Z")
                expire_delta = expire_time - Time.now
                
                if Time.now.utc < expire_time                  
                  if !user && (sp || coresite_user)                 
                    user = create_user(decoded_params, sp, on, coresite_user)
                  end
                  if user && user.service_provider == sp
                    remove_session_data(controller)
                    update_permissions(user, coresite_user, decoded_params)
                    controller.session['cas_user'] = user.email
                    controller.session['site'] = decoded_params["site"]
                    controller.session['expiry_date'] = expire_time
                    controller.session['expiry_delta'] = expire_delta
                    controller.session['tenant'] = on.id if on
                  else
                    SW_ERR "Failed to find user or service_provider does not match #{decoded_params.inspect} sp = #{sp.inspect} user #{user.inspect}"
                    remove_session_data(controller)
                  end
                  return true
                else
                  SW_ERR "Session has expired #{decoded_params.inspect}"
                  remove_session_data(controller)
                  controller.redirect_to('/session_expired.html')
                end
              else
                SW_ERR "Invalid params #{decoded_params.inspect}"
                remove_session_data(controller)
              end
            else
              SW_ERR "No encrypted authentication string passsed in (x)"
              remove_session_data(controller)
              controller.redirect_to('/session_expired.html')
            end
          elsif controller.session['expiry_date'] && Time.now.utc > controller.session['expiry_date']
            SW_ERR "Session has expired"
            remove_session_data(controller)
            controller.redirect_to('/session_expired.html')
          end
          if controller.session['expiry_date'].present? && controller.session['expiry_delta'].present?
            # Update the expiry date
            controller.session['expiry_date'] = Time.now + controller.session['expiry_delta']
          end
          return true
        end
      
        def create_user(decoded_params, sp, on, coresite_user)
          # TODO Need to select correct mapping once we know 
          # privilege_mapping = {"Admin" => "CoreSite Privilege", "Order" => "CoreSite Privilege", "View" => "CoreSite Privilege"}
          sp = ServiceProvider.system_owner if coresite_user
          user = User.create(:email => decoded_params["email"])
          user.service_provider = sp
          user.password = "No password for now"
         
          if not coresite_user
            user.operator_network_types = [on.operator_network_type]
            user.homepage = "/dashboard/"
          end
          update_permissions(user, coresite_user, decoded_params)
          
          if user.save
            site = Site.find_by_id(decoded_params["site"])
            if site
              contact = Contact.create(:name => "#{decoded_params["email"]} [#{site.name}]", :e_mail => decoded_params["email"], :service_provider => sp)
            elsif not coresite_user # It's a coresite user - don't fail
              SW_ERR "Could not find site #{decoded_params}"
              user.destroy
              user = nil
            end
          else
            SW_ERR "Failed to save user #{user.inspect}\n#{user.errors.full_messages}"
            user = nil
          end
          return user
        end
        
        def update_permissions(user, coresite_user, decoded_params)
          if coresite_user
            user.role = Role.find_by_name("CoreSite Privilege")
          else
            if decoded_params["order"] == "True"
              user.role = Role.find_by_name("Tenant Privilege")
            else
              user.role = Role.find_by_name("Read Only Privilege")
            end
          end
          user.save
        end
      
        def remove_session_data(controller)
          controller.session.delete('cas_user')
          controller.session.delete('site')
          controller.session.delete('expiry_date')
          controller.session.delete('expiry_delta')
          controller.session.delete('tenant')
        end
      end
    end
  

    class CsCrypt
      CODE = "AES-256-CBC"
      
      def CsCrypt.coresite_encode(text)
        crypt = Crypt.encrypt(text, CoresiteConfig.instance.config["auth_password"], CoresiteConfig.instance.config["auth_iv"], CODE)
        if crypt
          URI.escape(Base64.encode64(crypt), Regexp.new("[^#{URI::PATTERN::UNRESERVED}]"))
        end
      end
      
      def CsCrypt.coresite_decode(text)
        begin
          if text
            Crypt.decrypt(Base64.decode64(text), CoresiteConfig.instance.config["auth_password"], CoresiteConfig.instance.config["auth_iv"], CODE)            
          end
        rescue
          nil
        end
      end
    end
  end
end
