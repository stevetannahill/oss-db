require 'singleton'
class CoresiteConfig
  include Singleton

  def initialize
    @coresite_config = YAML::load(IO.read("#{Rails.root}/config/coresite.yml"))[Rails.env]
    @coresite_config["auth_iv"] = Base64.decode64(@coresite_config["auth_iv"])
    @coresite_config["auth_password"] = Base64.decode64(@coresite_config["auth_password"])
  end

  def config
    @coresite_config
  end

end

