class CoresiteConfigureNode
  class << self
    attr_accessor :stub_options
  end
  
  SCRIPT_EXIT_STR = "CENX Configure Script Exit Status::"
  CONFIGURE_FAIL = 6
  CONNECT_FAIL = 7
  CONFIGURE_OK = 0
  EXECUTABLE_FAIL = 65536 # Linux exit_status is a 16 bit integer
  
  # exit_status == 0 means ok otherwise it's the exit_status
  def self.configure(node, cmds, ignore_errors = false)    
    return [false, "Invalid Node or no commands"] if node.blank? || cmds.blank? 
    result = false
        
    hostname = node.primary_ip
    username = node.network_manager.username
    password =  node.network_manager.get_password
    exp_prompt = node.name + '*[>#]' # The CLI prompt is the node and ends in a "#" or ">"
    error_re = ignore_errors ? "" : 'Invalid input*|Incomplete command*|Unrecognized command*|Error: *|Error - *'
    if hostname.blank? || username.blank? || password.blank? || exp_prompt.blank?
      output = "One of the hostname (#{hostname}), username (#{username}), password or expected prompt (#{exp_prompt}) are empty"
    else
      log_file = Tempfile.new(["#{hostname}_", ".exp"], Rails.root.to_s + "/tmp")
      log_file.close 
      if stub_options
        exit_status = case stub_options
          when :ok then CONFIGURE_OK
          when :connect_fail then CONNECT_FAIL
          when :failure then CONFIGURE_FAIL
          else CONFIGURE_OK
        end
        File.open(log_file, "w") {|f| f.write("Stubbed router configuration")}
      else
        result = system("#{Rails.root}/bin/cdb/coresite_ssh.exp #{hostname} #{username} #{password} #{log_file.path} '#{exp_prompt}' '#{error_re}' '#{cmds}'")
        exit_status = $?.exitstatus
        exit_status = EXECUTABLE_FAIL if result.nil?
      end  
      log_file.open
      output = log_file.read.gsub(/\r\n/, "\n").gsub(/\r/, "\n") # Convert any /r/n to /n and any single /r to /n
      output << "\n#{SCRIPT_EXIT_STR}#{exit_status}\n" << "pid: #{$?.try(:pid)}"
      output << "\nConfiguration #{exit_status == 0 ? "Passed" : "Failed"}"
      log_file.close
      log_file.unlink
    end
    
    return [exit_status, output]
  end
  
  def self.failure_reason(exit_status)
    case exit_status.to_i
      when CONFIGURE_FAIL then "Failed to configure the router"
      when CONNECT_FAIL then "Failed to connect to the router"
      when CONFIGURE_OK then "Configured router"
      when EXECUTABLE_FAIL then "Executable failed"
      else "Failed to configure router: exit status:#{exit_status}"
    end
  end
  
end