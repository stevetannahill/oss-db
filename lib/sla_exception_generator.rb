# SlaExceptionGenerator.compute_sla_exceptions Time.utc(2011, 8, 1), Time.utc(2011, 8, 2)
# SlaExceptionGenerator.compute_enni_utilization_exceptions Time.utc(2011, 8, 1), Time.utc(2011, 8, 2)
# SlaExceptionGenerator.compute_cos_end_point_utilization_exceptions Time.utc(2011, 8, 1), Time.utc(2011, 8, 2)

class Time
  def duration_string
    difference = to_i
    days = (difference/(3600*24)).to_i
    hours = ((difference%(3600*24))/3600).to_i
    mins = ((difference%(3600))/60).to_i
    secs = (difference%60).to_i
    "#{days} days #{ '%02d' % hours}:#{'%02d' % mins}:#{'%02d' % secs} hours"
  end
end

class SlaExceptionGenerator
  FIRST_TIME = Time.utc(2011, 1, 1)

  def self.log_completion_time str, start_time
    end_time = Time.now
    Rails.logger.debug "Completion time: #{'%.3f' % (end_time-start_time).to_f} secs for: #{str} "
  end

  def self.config
    YAML::load(IO.read("#{Rails.root}/config/exception.yml"))
  end

  def self.compute_exceptions start_time, end_time=Time.now
    return nil if start_time > end_time
    compute_time_start = Time.now
    compute_enni_utilization_exceptions start_time, end_time
    # Serge : compute_cos_end_point_utilization_exceptions start_time, end_time
    # no longer calling 'compute_sla_exceptions' as this is now handled by LKP.
    
    puts "BJW Waiting";t = Time.now
    Process.waitall
    puts "BJW Done #{Time.now-t}"
    
    log_completion_time "Total exception computation", compute_time_start
  end

  def self.generate_test_exceptions
    ExceptionUtilizationEnni.delete_all
    ExceptionUtilizationCosEndPoint.delete_all
    ExceptionSlaCosTestVector.delete_all
    ExceptionTime.delete_all
    now = Time.now.to_i
    exception_count = 0
    util_severity = [:major, :minor]
    direction = [:ingress, :egress]
    ennis = EnniNew.find_all_by_prov_name(ProvStateful::LIVE)
    ennis.each do |enni|
      ex = ExceptionUtilizationEnni.new
      ex.time_declared  = now
      ex.enni_new_id = enni.id
      ex.severity = util_severity[exception_count%3]
      ex.direction = direction[exception_count%2]
      ex.value = 50
      ex.time_over = ex.value
      ex.average_rate_while_over_threshold_mbps = 500
      ex.save
      exception_count += 1
    end
    segment_end_points = OnNetOvcEndPointEnni.find_all_by_prov_name(ProvStateful::LIVE)
    segment_end_points.each do |segment_end_point|
      segment_end_point.cos_end_points.each do|cep|
        ex = ExceptionUtilizationCosEndPoint.new 
        ex.time_declared  = now
        ex.cos_end_point_id = cep.id
        ex.severity = util_severity[exception_count%3]
        ex.value = 900
        ex.value = 50
        ex.time_over = ex.value
        ex.direction = direction[exception_count%2]
        ex.time_over = 1200
        ex.average_rate_while_over_threshold_mbps = 300
        ex.save
        exception_count += 1
      end
    end
    severity = [:critical, :major, :minor]
    sla_type = [:availability, :frame_loss_ratio, :delay, :delay_variation ]
    OvcEndPointUni.find_each do |segment_end_point|
      segment_end_point.cos_test_vectors.each do |cos_test_vector|
        sla_type.each do |sla|
          ex = ExceptionSlaCosTestVector.new
          ex.time_declared  = now
          ex.value = 50
          ex.sla_type = ExceptionSlaCosTestVector.to_exception_sla_type(sla)
          ex.grid_circuit_id = cos_test_vector.grid_circuit_id
          if sla == :availability
            ex.severity = :critical
          else
            ex.severity = severity[exception_count%3]
          end
          ex.time_over = 1000 unless ex.severity == :critical
          ex.save
          exception_count += 1
        end
      end
    end


  end

  def self.dump_all_exceptions
    exception_count = 0
    # Utilization Enni
    ExceptionUtilizationEnni.find_each do |ex|
      exception_count += 1
      puts "---------"
      pp ex
      $stdout.flush
      puts ex.get_errored_period_threshold
      puts ex.get_errored_period_delta_time_secs
      puts ex.get_errored_period_max_secs
    end
    # Utilization OnNetOvcEnne
    ExceptionUtilizationCosEndPoint.find_each do |ex|
      exception_count += 1
      puts "---------"
      pp ex
      $stdout.flush
      puts ex.get_errored_period_threshold
      puts ex.get_errored_period_delta_time_secs
      puts ex.get_errored_period_max_secs
    end
    # SLAs
    ExceptionSlaCosTestVector.find_each do |ex|
      exception_count += 1
      puts "---------"
      pp ex
      $stdout.flush
      puts ex.get_errored_period_threshold
      puts ex.get_errored_period_delta_time_secs
      puts ex.get_errored_period_max_secs
      puts ex.get_value_units_s
      puts ex.get_threshold_units_s
    end
    puts "exception_count =#{exception_count}"
  end

  def self.compute_top_talkers start_time, end_time
    return nil if start_time > end_time
    time_start = Time.now
    compute_top_talkers_enni start_time, end_time
    compute_top_talkers_cos_end_point start_time, end_time
    log_completion_time "Total top-talker computation", time_start
  end

  def self.compute_enni_utilization_exceptions start_time, end_time=Time.now
    now = Time.now
    month_count = 0
    start_year = start_time.year
    end_year = end_time.year
    cfg = self.config
    ennis = EnniNew.where("prov_name = ?", ProvStateful::LIVE).pluck(:id)   
    if ennis.size/cfg["enni_utilization"]["max_processes"] == 0
      enni_chunks = [ennis]
    else
      enni_chunks = ennis.each_slice(ennis.size/cfg["enni_utilization"]["max_processes"]).to_a
    end
    config = ActiveRecord::Base.remove_connection      
    enni_chunks.collect do |ennis|
      bt = Time.now
      pid = fork do
        ActiveRecord::Base.establish_connection(config)
        Rails.logger.debug "Starting to Process Ennis #{ennis} in process #{Process.pid}"        
        (start_year..end_year).each do |y|
          if y == start_time.year
            start_month = start_time.month
          else 
            start_month = 1
          end
          if y == end_time.year
            end_month = end_time.month
          else 
            end_month = 12
          end
      
          (start_month..end_month).each do |m|
            puts "y=#{y}, m=#{m}"
            enni_count = 0
            month_compute_start_time = Time.now
            month_count += 1
            time_month_start = Time.utc(y, m, 1)
            time_month_end = time_month_start + 1.month - 1.second
            time_end = time_month_end
            EnniNew.where(:id => ennis).find_each do |enni|
              exit if Process.ppid == 1 # The parent has gone away - exit - Works even for kill -9 of parent
              enni_count += 1
              compute_start_time = Time.now
              puts "month(#{m}) enni.id: #{enni.id}"
              stats= enni.stats_data(time_month_start, time_end, Time.now, false)
              stats.compute_enni_utilization_exceptions
              log_completion_time "enni exception(#{enni.id}) ", compute_start_time
            end
          end
        end
        Rails.logger.debug "Ending Processing of Ennis #{ennis} in process #{Process.pid}"
      end
      SW_ERR "Failed to fork for #{ennis}" if pid == -1
    end
    ActiveRecord::Base.establish_connection(config)
  end

  def self.compute_cos_end_point_utilization_exceptions start_time, end_time
    now = Time.now
    month_count = 0
    start_year = start_time.year
    end_year = end_time.year
    
    cfg = self.config
    segment_end_points = OnNetOvcEndPointEnni.where("prov_name = ?", ProvStateful::LIVE).pluck(:id)
    if segment_end_points.size/cfg["cos_end_point_utilization"]["max_processes"] == 0
      segmentepp_chunks = [segment_end_points]
    else
      segmentepp_chunks = segment_end_points.each_slice(segment_end_points.size/cfg["cos_end_point_utilization"]["max_processes"]).to_a
    end
    config = ActiveRecord::Base.remove_connection
    
    segmentepp_chunks.collect do |segment_end_points|
      bt = Time.now
      pid = fork do
        ActiveRecord::Base.establish_connection(config)
        Rails.logger.debug "Starting to Process SegmentEndPoints #{segment_end_points} in process #{Process.pid}"               
        (start_year..end_year).each do |y|
          if y == start_time.year
            start_month = start_time.month
          else
            start_month = 1
          end
          if y == end_time.year
            end_month = end_time.month
          else
            end_month = 12
          end
          (start_month..end_month).each do |m|
            end_point_count = 0
            month_compute_start_time = Time.now
            month_count +=1
            time_month_start = Time.utc(y, m, 1)
            time_month_end = time_month_start + 1.month - 1.second
            time_end = time_month_end
            OnNetOvcEndPointEnni.where(:id => segment_end_points).find_each do |segment_end_point|
              exit if Process.ppid == 1 # The parent has gone away - exit - Works even for kill -9 of parent
              compute_start_time = Time.now
              end_point_count += 1
              stats= SamStatsArchive.new(segment_end_point, time_month_start, time_end)
              stats.compute_cos_end_point_utilization_exceptions
              log_completion_time "Segment exception computing for #{end_point_count} cos_end_points in month(#{m})", compute_start_time
            end
          end
        end
        Rails.logger.debug "Ending Processing of SegmentEndPoints #{segment_end_points} in process #{Process.pid}"
      end
      SW_ERR "Failed to fork for #{segment_end_points}" if pid == -1         
    end
    ActiveRecord::Base.establish_connection(config)
  end
  
  

  #Every X hours, for each Segment:
  #Time_start = Get first day of current month
  #Time_end =  Get time.now
  #If a failure exception has already been declared for a given SLA metric, the exception computation is skipped for that SLA parameter (for that Segment only).
  #If a warning exception has already been declared for a given SLA metric no new warning exception shall be generated from this computation.
  # For each SLA metric
  #   For Availability:
  #   available_time_periods = get from oss-db the list of time periods for which the Segment was unavailable.
  #   unavailable_time = Compute the sum of all unavailable time we have measured so far.
  #   If unavailable_time > SLA Then generate Failure exception
  #   Else If unavailable_time > X% SLA Then generate Warning exception.  Where X is read from cdb.
  #   For each of the following: FLR, FD, FDV, Segment Utilization, ENNI Utilization
  #      Averaged_metric = Get averaged metric computed over available_time_periods by querying the data archive.
  #      Adjusted_average = adjust average assuming it the metric remains optimal for the remaining of the calendar month
  #      If Adjusted_average > SLA Then generate Failure exception
  #      Else
  #         Excess_count = count how many intervals during available time had a metric > SLA
  #         If (Excess_count * duration of an interval) > max_amount_of_time_allowed Then generate Warning exception. Where max_amount_of_time_allowed is read from CDB.

  def self.compute_sla_exceptions start_time, end_time
    now = Time.now
    start_year = start_time.year
    end_year = end_time.year
    
    cfg = self.config
    ctvs = CosTestVector.joins(:cos_end_point => :segment_end_point).where(:segment_end_points => {:prov_name => "Live", :is_monitored => true}).pluck("cos_test_vectors.id")
    if ctvs.size/cfg["sla_exceptions"]["max_processes"] == 0
      ctv_chunks = [ctvs]
    else
      ctv_chunks = ctvs.each_slice(ctvs.size/cfg["sla_exceptions"]["max_processes"]).to_a
    end
    config = ActiveRecord::Base.remove_connection
    ctv_chunks.collect do |ctvs|
      bt = Time.now
      pid = fork do
        Rails.logger.debug "Starting to Process CosTestVectors #{ctvs} in process #{Process.pid}"
        puts "#{Process.pid} BJW Chunks #{ctvs} #{Time.now-bt}"; t=Time.now
        ActiveRecord::Base.establish_connection(config)
    
        (start_year..end_year).each do |y|          
          month_count = 0
          if y == start_time.year
            start_month = start_time.month
          else
            start_month = 1
          end
          if y == end_time.year
            end_month = end_time.month
          else
            end_month = 12
          end
          (start_month..end_month).each do |m|
            cos_test_vector_count = 0
            month_count +=1
            time_month_start = Time.utc(y, m, 1)
            time_month_end = time_month_start + 1.month - 1.second
            #TODO support all other types of SegmentEndPoints (including OnNetOvcEndpointEnni
            CosTestVector.where(:id => ctvs).find_each do |ctv|
              exit if Process.ppid == 1 # The parent has gone away - exit - Works even for kill -9 of parent
              cos_test_vector_count += 1
              t2=Time.now
              stats=ctv.stats_data(time_month_start, time_month_end, Time.now, false)
              Rails.logger.debug "#{Process.pid} BJW stats_data #{ctv.id} #{Time.now-t2}"
          
              stats.bx_availability_kludge
              [:availability, :frame_loss_ratio, :delay, :delay_variation].each do |metric_type|
                t1 = Time.now
                stats.compute_endpoint_sla_exceptions metric_type
                Rails.logger.debug "#{Process.pid} BJW compute_endpoint_sla_exceptions #{ctv.id} #{metric_type} #{Time.now-t1}"
              end
            end
          end
        end
        Rails.logger.debug "Ending Processing of CosTestVectors #{ctvs} in process #{Process.pid}"
        puts "#{Process.pid} BJW End #{Time.now-t}"
      end
      SW_ERR "Failed to fork for #{ctvs}" if pid == -1
    end
    ActiveRecord::Base.establish_connection(config)
  end



  def self.compute_top_talkers_enni start_time, end_time
    now = Time.now
    month_count = 0
    start_year = start_time.year
    end_year = end_time.year
    (start_year..end_year).each do |y|
      if y == start_time.year
        start_month = start_time.month
      else
        start_month = 1
      end
      if y == end_time.year
        end_month = end_time.month
      else
        end_month = 12
      end
      (start_month..end_month).each do |m|
        month_compute_start_time = Time.now
        month_count += 1
        time_month_start = Time.utc(y, m, 1)
        time_month_end = time_month_start + 1.month - 1.second
        time_month_end = Time.now if time_month_end > Time.now
        talkers = EhealthEnniStatsArchive.compute_top_talkers_enni [ [time_month_start.to_i, time_month_end.to_i ] ]
        log_completion_time "Total ENNI top talker computing for month(#{m})", month_compute_start_time
        talkers.each do |talker|
          ex = TopTalkerEnni.find( :first, :conditions => [ "enni_id=? AND direction=? AND time_period_start=? ",talker[:enni_id], talker[:direction], time_month_start.to_i])
          ex = TopTalkerEnni.new  if ex.nil?
          ex.enni_id = talker[:enni_id]
          ex.octets_count = talker[:octets_count].to_f
          ex.direction = talker[:direction]
          ex.time_period_start = time_month_start.to_i
          ex.time_period_end = time_month_end.to_i
          ex.save
        end
      end
    end
    log_completion_time "Total ENNI top talker computing over #{month_count} months", now
  end

  def self.compute_top_talkers_cos_end_point start_time, end_time
    now = Time.now
    start_year = start_time.year
    end_year = end_time.year
    (start_year..end_year).each do |y|
      month_count = 0
      if y == start_time.year
        start_month = start_time.month
      else
        start_month = 1
      end
      if y == end_time.year
        end_month = end_time.month
      else
        end_month = 12
      end
      (start_month..end_month).each do |m|
        month_compute_start_time = Time.now
        month_count += 1
        time_month_start = Time.utc(y, m, 1)
        time_month_end = time_month_start + 1.month - 1.second
        time_month_end = Time.now if time_month_end > Time.now
        talkers = SamStatsArchive.compute_top_talkers_cos_end_point [ [time_month_start.to_i, time_month_end.to_i ] ]
        log_completion_time "Total CosEndPoint top talker computing for month(#{m})", month_compute_start_time
        talkers.each do |talker|
          ex = TopTalkerCosEndPoint.find( :first, :conditions => [ "cos_end_point_id=? AND direction=? AND time_period_start=? ",talker[:cos_end_point_id], talker[:direction], time_month_start.to_i])
          ex = TopTalkerCosEndPoint.new  if ex.nil?
          ex.cos_end_point_id = talker[:cos_end_point_id]
          ex.octets_count = talker[:octets_count].to_f
          ex.direction = talker[:direction]
          ex.time_period_start = time_month_start.to_i
          ex.time_period_end = time_month_end.to_i
          ex.save
        end
      end
    end
    log_completion_time "Total CosEndPoint top talker computing over #{month_count} months", now
  end

  def self.test_identity start_time, end_time
    now = Time.now
    start_year = start_time.year
    end_year = end_time.year
    (start_year..end_year).each do |y|
      month_count = 0
      if y == start_time.year
        start_month = start_time.month
      else
        start_month = 1
      end
      if y == end_time.year
        end_month = end_time.month
      else
        end_month = 12
      end
      (start_month..end_month).each do |m|
        cos_test_vector_count = 0
        month_count +=1
        time_month_start = Time.utc(y, m, 1)
        time_month_end = time_month_start + 1.month - 1.second
        #TODO support all other types of SegmentEndPoints (including OnNetOvcEndpointEnni
        segment_end_points = OvcEndPointUni.find_all_by_is_monitored_and_prov_name(true, ProvStateful::LIVE)
        segment_end_points.each do |segment_end_point|
          segment_end_point.cos_end_points.collect {|cep| cep.cos_test_vectors}.flatten.each do|ctv|
            cos_test_vector_count += 1
            stats=ctv.stats_data(time_month_start, time_month_end, Time.now, false)              
            stats.bx_availability_kludge
            stats.test_metric_identity
          end
        end
      end
    end
  end


end

