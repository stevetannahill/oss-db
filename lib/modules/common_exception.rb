module CommonException #:nodoc:
  # = Common Exception
  #
  # Common exceptions are shared by Rolled Up Reports, CDB Utilization and SLA exceptions
  include ApplicationHelper
  def self.included(base)
    base.class_eval do
      scope :s_clear, where('severity = ?', :clear)
      scope :s_minor, where('severity = ?', :minor)
      scope :s_major, where('severity = ?', :major)
      scope :s_critical, where('severity = ?', :critical)

      def self.s_count
        select("COUNT(DISTINCT #{self.table_name}.id) as s_count").uniq(false).first.try(:[], :s_count) || 0
      end

      def self.s_severity_count
        select("COUNT(DISTINCT #{self.table_name}.id) as count, severity").
        group('severity').
        order('').
        uniq(false)
      end

      def self.order_with_severity(sort_column, sort_direction)
        if sort_column == 'severity'
          order("FIELD(#{sort_column}, 'clear', 'minor', 'major', 'critical') #{sort_direction}")
        else
          order("#{sort_column} #{sort_direction}")
        end
      end

      def weekly_start_period
        Time.at(self.period_start_epoch).in_time_zone.beginning_of_week.strftime('%Y-%m-%d')
      end

      def weekly_end_period
        Time.at(self.period_start_epoch).in_time_zone.end_of_week.strftime('%Y-%m-%d')
      end

      def weekly_period
        "#{self.weekly_start_period} to #{self.weekly_end_period}"
      end
    end
  end
end