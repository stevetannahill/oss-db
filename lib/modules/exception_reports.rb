module ExceptionReports

  def sla_exceptions(paths, params, date_range_start, date_range_end, user)
    metrics_cos_test_vectors('monthly', paths, params, date_range_start, date_range_end, user)
  end


  def oem_sla_exceptions(paths, params, date_range_start, date_range_end, user)
    conditions = 'time_declared >= :date_range_start AND time_declared <= :date_range_end AND paths.id IN (:paths)'

    unless params[:severity].blank?
      conditions << ' AND severity IN (:severity)'
    end

    sort_columns = ['severity', 'time_declared', nil, 'operator_network_types.name']
    sort_column = sort_columns[params[:iSortCol_0] ? params[:iSortCol_0].to_i : 0]
    sort_direction = params[:sSortDir_0] || "DESC"

    return ExceptionSlaNetworkType.includes(:operator_network_type => {:operator_networks => :paths}).
                                   where(conditions, {:date_range_start => date_range_start.to_i, :date_range_end => date_range_end.to_i, :severity => params[:severity], :paths => paths.pluck('paths.id')}).
                                   order_with_severity(sort_column, sort_direction)
  end


  def fallout_exceptions(paths, params, date_range_start, date_range_end, user)
    conditions = 'timestamp >= :date_range_start AND timestamp <= :date_range_end AND fallout_item_type = "Path" AND fallout_item_id IN (:paths)'

    conditions << ' AND exception_type = :fallout_exception_type' unless params[:fallout_exception_type].blank?
    conditions << ' AND category IN (:fallout_category)' unless params[:fallout_category].blank?
    conditions << ' AND operator_networks.operator_network_type_id IN (:oem)' if params[:oem].present?

    sort_columns = [nil, 'timestamp', nil, 'service_providers.name', 'exception_type', nil]
    sort_column = sort_columns[params[:iSortCol_0] ? params[:iSortCol_0].to_i : 1]
    sort_direction =  params[:sSortDir_0] || "DESC"

    # Find current exceptions
    finder = FalloutException.current.includes({:path => [{:segments => :operator_network}, :service_provider]}).
                                      where(conditions, {:oem => params[:oem], :fallout_exception_type => params[:fallout_exception_type], :fallout_category => params[:fallout_category], :date_range_start => date_range_start.to_i, :date_range_end => date_range_end.to_i, :paths => paths.pluck('paths.id')}).
                                      order("#{sort_column} #{sort_direction}")

    finder
  end

  
  def circuit_utilization_exceptions(paths, params, date_range_start, date_range_end, user)
     conditions = 'time_declared >= :date_range_start AND time_declared <= :date_range_end AND paths.id IN (:paths)'

      unless params[:severity].blank?
        conditions << ' AND severity IN (:severity)'
      end

      unless params[:service_provider].blank?
        conditions << ' AND service_providers.id IN (:service_provider_id)'
      end

      conditions << ' AND operator_networks.operator_network_type_id IN (:oem)' if params[:oem].present?

      sort_columns = ['severity', nil, 'time_declared', nil, 'service_providers.name', 'direction']
      sort_column = sort_columns[params[:iSortCol_0] ? params[:iSortCol_0].to_i : 0]
      sort_direction = params[:sSortDir_0] || "DESC"

      return ExceptionUtilizationCosEndPoint.includes(:cos_end_point => [{:cos_instance => [:class_of_service_type, {:segment => :ethernet_service_type}]}, {:segment_end_point => {:segment => [:operator_network, :service_provider, :paths]}}]).
                                             where(conditions, {:oem => params[:oem], :service_provider_id => params[:service_provider], :date_range_start => date_range_start.to_i, :date_range_end => date_range_end.to_i, :severity => params[:severity], :paths => paths.pluck('paths.id')}).
                                             order_with_severity(sort_column, sort_direction)
  end
  

  def enni_utilization_exceptions(ennis, params, date_range_start, date_range_end, user)
    sort_columns = ['severity', nil, 'time_declared', 'sites.name', 'service_providers.name', 'direction']
    sort_column = sort_columns[params[:iSortCol_0] ? params[:iSortCol_0].to_i : 0]
    sort_direction = params[:sSortDir_0] || "DESC"
    conditions = 'time_declared >= :date_range_start AND time_declared <= :date_range_end AND enni_new_id IN (:ennis)'

    unless params[:severity].blank?
      conditions << ' AND severity IN (:severity)'
    end

    unless params[:service_provider].blank?
      conditions << ' AND service_providers.id IN (:service_provider_id)'
    end

    conditions << ' AND operator_networks.operator_network_type_id IN (:oem)' if params[:oem].present?

    return ExceptionUtilizationEnni.includes({:enni_new => [:site, {:operator_network => :service_provider}]}).
                                    where(conditions, {:oem => params[:oem], :date_range_start => date_range_start.to_i, :date_range_end => date_range_end.to_i, :severity => params[:severity], :ennis => ennis.pluck('demarcs.id').uniq}).
                                    order_with_severity(sort_column, sort_direction)
  end



 def site_group_exceptions(paths, params, date_range_start, date_range_end, user)
    conditions = 'period_start_epoch >= :date_range_start AND period_start_epoch <= :date_range_end '
    conditions << ' AND path_displays.service_provider_id = :user_service_provider_id'
    conditions << ' AND rollup_report_site_groups.site_group_id IN (:site_group)' if params[:site_group].present?
    conditions << ' AND site_groups_sites.site_id IN (:site)' if params[:site].present?
    conditions << ' AND operator_networks.operator_network_type_id IN (:oem)' if params[:oem].present?
    conditions << ' AND operator_networks.service_provider_id IN (:service_provider_id)' if params[:service_provider].present?

    if params[:severity].present?
      params[:severity].each do |sev|
        if ['clear', 'minor', 'major'].include?(sev)
          conditions << " AND demarc_#{sev}_count > 0"
        end
      end
    end

    exclude_sites = []
    include_sites = []

    if params['limit_sites'].present? && params['limit_site_list_id'].present?
      if params['limit_sites'] == 'exclude_sites'
        exclude_sites = PathsSiteList.where('site_list_id IN (?)', params[:limit_site_list_id]).pluck(:path_id)
        conditions << ' AND path_displays.path_id NOT IN (:exclude_sites)'
      else
        include_sites = PathsSiteList.where('site_list_id IN (?)', params[:limit_site_list_id]).pluck(:path_id)
        if include_sites.any?
          conditions << ' AND path_displays.path_id IN (:include_sites)'
        end
      end
    end

    sort_columns = ['period_start_epoch', nil, 'site_count', 'demarc_count', nil, nil, nil]
    sort_column = sort_columns[params[:iSortCol_0].to_i || 0]
    sort_direction =  params[:sSortDir_0] || "DESC"

    finder = RollupReportSiteGroup.joins("INNER JOIN site_groups_sites ON rollup_report_site_groups.site_group_id = site_groups_sites.site_group_id
                                          INNER JOIN path_displays ON path_displays.site_id = site_groups_sites.site_id AND path_displays.entity_type = 'Demarc'").
                                   where(conditions, {:exclude_sites => exclude_sites, :include_sites => include_sites, :site => params[:site], :user_service_provider_id => user.service_provider.id, :service_provider_id => params[:service_provider], :oem => params[:oem], :date_range_start => date_range_start.to_i, :date_range_end => date_range_end.to_i, :severity => params[:severity], :site_group => params[:site_group], :search => "%#{params[:sSearch]}%"}).
                                   order("#{sort_column} #{sort_direction}").
                                   uniq

    if params[:oem].present? || params[:service_provider].present? || params[:component].present? || params[:site_type].present?
      finder = finder.joins("INNER JOIN paths ON path_displays.path_id = paths.id
                             INNER JOIN paths_segments ON paths_segments.path_id = paths.id
                             INNER JOIN segments ON segments.id = paths_segments.segment_id
                             INNER JOIN operator_networks ON operator_networks.id = segments.operator_network_id
                             INNER JOIN segment_end_points ON segment_end_points.segment_id = segments.id
                             INNER JOIN cos_end_points ON segment_end_points.id = cos_end_points.segment_end_point_id
                             INNER JOIN cos_test_vectors ON cos_end_points.id = cos_test_vectors.cos_end_point_id")
    end

    finder = component_search(finder, params)

    finder
  end

  
  def satellite_site_exceptions(paths, params, date_range_start, date_range_end, user)
    conditions = 'period_start_epoch >= :date_range_start AND period_start_epoch <= :date_range_end '
    conditions << ' AND path_displays.service_provider_id = :user_service_provider_id'
    conditions << ' AND severity IN (:severity)' if params[:severity].present?
    conditions << ' AND site_groups_sites.site_group_id IN (:site_group)' if params[:site_group].present?
    conditions << ' AND rollup_report_sites.site_id IN (:site)' if params[:site].present?
    conditions << ' AND operator_networks.operator_network_type_id IN (:oem)' if params[:oem].present?
    conditions << ' AND operator_networks.service_provider_id IN (:service_provider_id)' if params[:service_provider].present?

    if params[:severity].present?
      params[:severity].each do |sev|
        if ['clear', 'minor', 'major'].include?(sev)
          conditions << " AND #{sev}_demarc_count > 0"
        end
      end
    end

    if params['limit_sites'].present? && params['limit_site_list_id'].present?
      if params['limit_sites'] == 'exclude_sites'
        exclude_sites = PathsSiteList.where('site_list_id IN (?)', params[:limit_site_list_id]).pluck(:path_id)
        conditions << ' AND path_displays.path_id NOT IN (:exclude_sites)'
      else
        include_sites = PathsSiteList.where('site_list_id IN (?)', params[:limit_site_list_id]).pluck(:path_id)
        if include_sites.any?
          conditions << ' AND path_displays.path_id IN (:include_sites)'
        end
      end
    end

    sort_columns = ['period_start_epoch', nil, 'demarc_count', nil, nil, nil]
    sort_column = sort_columns[params[:iSortCol_0].to_i || 0]
    sort_direction =  params[:sSortDir_0] || "DESC"

    finder = RollupReportSite.joins("INNER JOIN path_displays ON rollup_report_sites.site_id = path_displays.site_id AND path_displays.entity_type = 'Demarc'").
                                   where(conditions, {:exclude_sites => exclude_sites, :include_sites => include_sites, :site => params[:site], :user_service_provider_id => user.service_provider.id, :service_provider_id => params[:service_provider], :oem => params[:oem], :date_range_start => date_range_start.to_i, :date_range_end => date_range_end.to_i, :severity => params[:severity], :site_group => params[:site_group], :search => "%#{params[:sSearch]}%"}).
                                   order("#{sort_column} #{sort_direction}").
                                   uniq

    if params[:site_group].present?
      finder = finder.joins("INNER JOIN site_groups_sites ON rollup_report_sites.site_id = site_groups_sites.site_id")
    end

    if params[:oem].present? || params[:service_provider].present? || params[:component].present? || params[:site_type].present?
      finder = finder.joins("INNER JOIN paths ON path_displays.path_id = paths.id
                             INNER JOIN paths_segments ON paths_segments.path_id = paths.id
                             INNER JOIN segments ON segments.id = paths_segments.segment_id
                             INNER JOIN operator_networks ON operator_networks.id = segments.operator_network_id
                             INNER JOIN segment_end_points ON segment_end_points.segment_id = segments.id
                             INNER JOIN cos_end_points ON segment_end_points.id = cos_end_points.segment_end_point_id
                             INNER JOIN cos_test_vectors ON cos_end_points.id = cos_test_vectors.cos_end_point_id")

    end

    finder = component_search(finder, params)

    finder                                   
  end


  def cell_site_exceptions(paths, params, date_range_start, date_range_end, user)
    conditions = 'period_start_epoch >= :date_range_start AND period_start_epoch <= :date_range_end '
    conditions << ' AND severity IN (:severity)' if params[:severity].present?
    conditions << ' AND path_displays.service_provider_id = :user_service_provider_id'
    conditions << ' AND site_groups_sites.site_group_id IN (:site_group)' if params[:site_group].present?
    conditions << ' AND rollup_report_demarcs.site_id IN (:site)' if params[:site].present?
    conditions << ' AND operator_networks.operator_network_type_id IN (:oem)' if params[:oem].present?
    conditions << ' AND operator_networks.service_provider_id IN (:service_provider_id)' if params[:service_provider].present?

    sort_columns = ['severity', 'period_start_epoch', nil, nil, nil, nil, nil, 'metric_count']
    sort_column = sort_columns[params[:iSortCol_0].to_i || 0]
    sort_direction =  params[:sSortDir_0] || "DESC"

    if params['limit_sites'].present? && params['limit_site_list_id'].present?
      if params['limit_sites'] == 'exclude_sites'
        exclude_sites = PathsSiteList.where('site_list_id IN (?)', params[:limit_site_list_id]).pluck(:path_id)
        conditions << ' AND path_displays.path_id NOT IN (:exclude_sites)'
      else
        include_sites = PathsSiteList.where('site_list_id IN (?)', params[:limit_site_list_id]).pluck(:path_id)
        if include_sites.any?
          conditions << ' AND path_displays.path_id IN (:include_sites)'
        end
      end
    end

    finder = RollupReportDemarc.joins("INNER JOIN path_displays ON rollup_report_demarcs.demarc_id = path_displays.entity_id AND path_displays.entity_type = 'Demarc'").
                                where(conditions, {:exclude_sites => exclude_sites, :include_sites => include_sites, :site => params[:site], :user_service_provider_id => user.service_provider.id, :service_provider_id => params[:service_provider], :oem => params[:oem], :date_range_start => date_range_start.to_i, :date_range_end => date_range_end.to_i, :severity => params[:severity], :site_group => params[:site_group], :site => params[:site], :search => "%#{params[:sSearch]}%"}).
                                order_with_severity(sort_column, sort_direction).
                                uniq
    
    if params[:site_group].present?
      finder = finder.joins("INNER JOIN site_groups_sites ON rollup_report_demarcs.site_id = site_groups_sites.site_id")
    end

    if params[:oem].present? || params[:service_provider].present? || params[:cell_site_name].present? || params[:component].present? || params[:site_type].present?
      finder = finder.joins("INNER JOIN paths ON path_displays.path_id = paths.id
                             INNER JOIN paths_segments ON paths_segments.path_id = paths.id
                             INNER JOIN segments ON segments.id = paths_segments.segment_id
                             INNER JOIN operator_networks ON operator_networks.id = segments.operator_network_id
                             INNER JOIN segment_end_points ON segment_end_points.segment_id = segments.id
                             INNER JOIN cos_end_points ON segment_end_points.id = cos_end_points.segment_end_point_id
                             INNER JOIN cos_test_vectors ON cos_end_points.id = cos_test_vectors.cos_end_point_id")

    end    

    unless params[:cell_site_name].blank?
      finder = finder.joins('INNER JOIN member_attr_instances ON rollup_report_demarcs.demarc_id = member_attr_instances.affected_entity_id AND member_attr_instances.affected_entity_type = "Demarc"')
      params[:cell_site_name].to_s.gsub(',', ' ').squish.split(' ').each do |cell_site_name|
        finder = finder.where('member_attr_instances.value LIKE ?', "%#{cell_site_name}%").
                        where('member_attr_instances.owner_id = ?', user.service_provider.id)
      end
    end

    finder = component_search(finder, params)

    finder
  end


  def cell_site_path_exceptions(paths, params, date_range_start, date_range_end, user)
    conditions = 'period_start_epoch >= :date_range_start AND period_start_epoch <= :date_range_end '
    conditions << ' AND severity IN (:severity)' if params[:severity].present?
    conditions << ' AND path_displays.service_provider_id = :user_service_provider_id'
    conditions << ' AND site_groups_sites.site_group_id IN (:site_group)' if params[:site_group].present?
    conditions << ' AND rollup_report_paths.site_id IN (:site)' if params[:site].present?
    conditions << ' AND operator_networks.operator_network_type_id IN (:oem)' if params[:oem].present?
    conditions << ' AND operator_networks.service_provider_id IN (:service_provider_id)' if params[:service_provider].present?

    sort_columns = ['severity', 'period_start_epoch', nil, nil, nil, nil, nil, 'metric_count']
    sort_column = sort_columns[params[:iSortCol_0].to_i || 0]
    sort_direction =  params[:sSortDir_0] || "DESC"

    if params['limit_sites'].present? && params['limit_site_list_id'].present?
      if params['limit_sites'] == 'exclude_sites'
        exclude_sites = PathsSiteList.where('site_list_id IN (?)', params[:limit_site_list_id]).pluck(:path_id)
        conditions << ' AND path_displays.path_id NOT IN (:exclude_sites)'
      else
        include_sites = PathsSiteList.where('site_list_id IN (?)', params[:limit_site_list_id]).pluck(:path_id)
        if include_sites.any?
          conditions << ' AND path_displays.path_id IN (:include_sites)'
        end
      end
    end

    finder = RollupReportPath.joins("INNER JOIN path_displays ON rollup_report_paths.demarc_id = path_displays.entity_id AND path_displays.entity_type = 'Demarc'").
                                   where(conditions, {:exclude_sites => exclude_sites, :include_sites => include_sites, :site => params[:site], :user_service_provider_id => user.service_provider.id, :service_provider_id => params[:service_provider], :oem => params[:oem], :date_range_start => date_range_start.to_i, :date_range_end => date_range_end.to_i, :severity => params[:severity], :site_group => params[:site_group], :search => "%#{params[:sSearch]}%"}).
                                   order("#{sort_column} #{sort_direction}").
                                   uniq
                                   
    if params[:site_group].present?
      finder = finder.joins("INNER JOIN site_groups_sites ON rollup_report_paths.site_id = site_groups_sites.site_id")
    end

    if params[:oem].present? || params[:service_provider].present? || params[:component].present? || params[:site_type].present?
      finder = finder.joins("INNER JOIN paths ON path_displays.path_id = paths.id
                             INNER JOIN paths_segments ON paths_segments.path_id = paths.id
                             INNER JOIN segments ON segments.id = paths_segments.segment_id
                             INNER JOIN operator_networks ON operator_networks.id = segments.operator_network_id
                             INNER JOIN segment_end_points ON segment_end_points.segment_id = segments.id
                             INNER JOIN cos_end_points ON segment_end_points.id = cos_end_points.segment_end_point_id
                             INNER JOIN cos_test_vectors ON cos_end_points.id = cos_test_vectors.cos_end_point_id")                             
    end

    unless params[:cell_site_name].blank?
      finder = finder.joins('INNER JOIN member_attr_instances ON rollup_report_paths.demarc_id = member_attr_instances.affected_entity_id AND member_attr_instances.affected_entity_type = "Demarc"')
      params[:cell_site_name].to_s.gsub(',', ' ').squish.split(' ').each do |cell_site_name|
        finder = finder.where('member_attr_instances.value LIKE ?', "%#{cell_site_name}%").
                        where('member_attr_instances.owner_id = ?', user.service_provider.id)
      end
    end

    finder = component_search(finder, params)

    finder
  end


  def metrics_exceptions(paths, params, date_range_start, date_range_end, user)
    metrics_cos_test_vectors('weekly', paths, params, date_range_start, date_range_end, user)
  end

  def metrics_cos_test_vectors(period_type='weekly', paths, params, date_range_start, date_range_end, user)
    date_field = period_type == 'monthly' ? 'time_declared_epoch' : 'period_start_epoch'
    conditions = "#{date_field} >= :date_range_start AND #{date_field} <= :date_range_end "

    exclude_sites = []
    include_sites = []

    if params['limit_sites'].present? && params['limit_site_list_id'].present?
      if params['limit_sites'] == 'exclude_sites'
        exclude_sites = PathsSiteList.where('site_list_id IN (?)', params[:limit_site_list_id]).pluck(:path_id)
        conditions << ' AND path_displays.path_id NOT IN (:exclude_sites)'
      else
        include_sites = PathsSiteList.where('site_list_id IN (?)', params[:limit_site_list_id]).pluck(:path_id)
        if include_sites.any?
          conditions << ' AND path_displays.path_id IN (:include_sites)'
        end
      end
    end
    
    conditions << ' AND severity <> "clear" ' if period_type == 'monthly'
    conditions << ' AND path_displays.service_provider_id = :user_service_provider_id'
    conditions << ' AND severity IN (:severity)' if params[:severity].present?
    conditions << ' AND operator_networks.service_provider_id IN (:service_provider_id)' if params[:service_provider].present?
    conditions << ' AND metric_type IN (:sla_type)' if params[:sla_type].present?
    conditions << ' AND demarc_id = :demarc' if params[:demarc].present?
    conditions << ' AND circuit_id like :search' if params[:sSearch].present?
    # conditions << ' AND site_groups.id IN (:site_group)' if params[:site_group].present?
    conditions << ' AND segment_sites.site_id IN (:site)' if params[:site].present?
    conditions << ' AND operator_networks.operator_network_type_id IN (:oem)' if params[:oem].present?

    sort_columns = ['severity', nil, date_field, nil, nil, nil, nil, 'metric_type']
    sort_column = sort_columns[params[:iSortCol_0].to_i || 0]
    sort_direction =  params[:sSortDir_0] || "DESC"

    if period_type == 'monthly'
      finder = MetricsMonthlyCosTestVector
      table_name = 'metrics_monthly_cos_test_vectors'
    else
      finder = MetricsWeeklyCosTestVector
      table_name = 'metrics_weekly_cos_test_vectors'
    end

    finder = finder.joins("INNER JOIN cos_test_vectors ON #{table_name}.grid_circuit_id = cos_test_vectors.grid_circuit_id
                           INNER JOIN cos_end_points ON cos_end_points.id = cos_test_vectors.cos_end_point_id
                           INNER JOIN segment_end_points ON segment_end_points.id = cos_end_points.segment_end_point_id
                           INNER JOIN segments ON segments.id = segment_end_points.segment_id
                           INNER JOIN path_displays ON path_displays.entity_id = segments.id AND path_displays.entity_type = 'Segment'").
                    where(conditions, {:site => params[:site], :oem => params[:oem], :exclude_sites => exclude_sites, :include_sites => include_sites, :sla_type => params[:sla_type], :service_provider_id => params[:service_provider], :site_group => params[:site_group], :date_range_start => date_range_start.to_i, :date_range_end => date_range_end.to_i, :severity => params[:severity], :demarc => params[:demarc], :user_service_provider_id => user.service_provider.id, :search => "%#{params[:sSearch]}%"}).
                    order_with_severity(sort_column, sort_direction).
                    uniq


    # if params[:site_group].present?
    #   finder = finder.joins("INNER JOIN site_groups_sites ON path_displays.site_id = site_groups_sites.site_id")
    # end

    if params[:site].present?
      finder = finder.joins("INNER JOIN paths_segments ON paths_segments.path_id = path_displays.path_id
                             INNER JOIN segments AS segment_sites ON paths_segments.segment_id = segment_sites.id")
    end

    if params[:oem].present? || params[:service_provider].present?
      finder = finder.joins("INNER JOIN operator_networks ON operator_networks.id = segments.operator_network_id")
    end


    if exclude_sites.any?
      member_handles = SinPathServiceProviderSphinx.where('path_id IN (?)', exclude_sites).
                                                   where('service_provider_id = ?', user.service_provider.id).
                                                   pluck(:member_handle)
      member_handles.each do |mh|
        finder = finder.where('circuit_id NOT LIKE ?', "%#{mh.gsub(/-.*/, '')}%") 
      end
    end

    finder = component_search(finder, params)

    finder
  end
  
  
  def component_search(finder, params)
    unless params[:component].blank?
      component_terms = params[:component].to_s.gsub(',', ' ').squish.split(' ')
      component_condition = (params[:component_condition].present? && ['and', 'or'].include?(params[:component_condition].downcase)) ? params[:component_condition] : 'AND'
      finder = finder.where(component_terms.map{|component| " circuit_id LIKE '%#{component}%' "}.join(component_condition))
    end

    unless params[:site_type].blank?
      component_terms = []
      if params[:site_type].include?('fiber')
        component_terms += ['ETH', 'DNR']
      end

      if params[:site_type].include?('microwave')
        component_terms += ['RS', 'RN', 'MH', 'FAN']
      end

      unless component_terms.empty?
        finder = finder.where(component_terms.map{|component| " circuit_id LIKE '%-#{component}-%' "}.join('OR'))
      end
    end

    finder
  end


  def get_date_range(date_range_selector, date_range_start=nil, date_range_end=nil, default='month_to_date')
    # last hour is sort of hack, since we don't handle time yet, just show the day
    @last_hour = {:value => 'last_hour', 'data-date-range-start' => RangeDateStart.new(nil, :today), 'data-date-range-end' => RangeDateEnd.new(nil, :today)}
    @today = {:value => 'today', 'data-date-range-start' => RangeDateStart.new(nil, :today), 'data-date-range-end' => RangeDateEnd.new(nil, :today)}
    @week_to_date = {:value => 'week_to_date', 'data-date-range-start' => RangeDateStart.new(nil, :week), 'data-date-range-end' => RangeDateEnd.new(nil, :today)}
    @week_previous = {:value => 'week_previous', 'data-date-range-start' => RangeDateStart.new(nil, :week_previous), 'data-date-range-end' => RangeDateEnd.new(nil, :week_previous_end)}
    @seven_days_previous = {:value => 'seven_days_previous', 'data-date-range-start' => RangeDateStart.new(7.days.ago), 'data-date-range-end' => RangeDateEnd.new(nil, :today)}
    @month_to_date = {:value => 'month_to_date', 'data-date-range-start' => RangeDateStart.new(nil, :month), 'data-date-range-end' => RangeDateEnd.new(nil, :today)}
    @month_previous = {:value => 'month_previous', 'data-date-range-start' => RangeDateStart.new(nil, :month_previous), 'data-date-range-end' => RangeDateEnd.new(nil, :today)}
    @three_months_previous = {:value => 'three_months_previous', 'data-date-range-start' => RangeDateStart.new(nil, :three_months_previous), 'data-date-range-end' => RangeDateEnd.new(nil, :today)}
    @six_months_previous = {:value => 'six_months_previous', 'data-date-range-start' => RangeDateStart.new(nil, :six_months_previous), 'data-date-range-end' => RangeDateEnd.new(nil, :today)}
    @nine_months_previous = {:value => 'nine_months_previous', 'data-date-range-start' => RangeDateStart.new(nil, :nine_months_previous), 'data-date-range-end' => RangeDateEnd.new(nil, :today)}
    @twelve_months_previous = {:value => 'twelve_months_previous', 'data-date-range-start' => RangeDateStart.new(nil, :twelve_months_previous), 'data-date-range-end' => RangeDateEnd.new(nil, :today)}
    @year_to_date = {:value => 'year_to_date', 'data-date-range-start' => RangeDateStart.new(nil, :year), 'data-date-range-end' => RangeDateEnd.new(nil, :today)}  
    @date_range = {:value => 'date_range', :id => 'date_range_option'}

    unless ['last_hour', 'today', 'week_to_date', 'week_previous', 'seven_days_previous', 'month_to_date', 'month_previous', 'three_months_previous', 'six_months_previous', 'nine_months_previous', 'twelve_months_previous', 'year_to_date', 'date_range'].include?(date_range_selector)
      date_range_selector = default
    end
    instance_variable_get("@#{date_range_selector}")[:selected] = 'selected'

    if date_range_selector.blank? || date_range_selector == 'date_range'
      @date_range_start = RangeDateStart.new(date_range_start)
      @date_range_end = RangeDateEnd.new(date_range_end)
    else
      @date_range_start = instance_variable_get("@#{date_range_selector}")['data-date-range-start']
      @date_range_end = instance_variable_get("@#{date_range_selector}")['data-date-range-end']
    end
    
    return @date_range_start, @date_range_end
  end
  
end
