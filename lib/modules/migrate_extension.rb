# Used by db:migrate to signal that diagrams have to be regenerated at the end of the migrations
module Migrate
  
  @@regenerate_diagrams = false

  # Tell migration code to regenerate diagrams
  def self.regenerate_diagrams(bool=true)
    @@regenerate_diagrams = bool
  end

  # Check if diagrams should be regenerated
  def self.regenerate_diagrams?
    @@regenerate_diagrams
  end

end