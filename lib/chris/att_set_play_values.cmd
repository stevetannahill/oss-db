# NEW ASR
set asr.admin_section.CCNA XEN
set ethervc.admin_section.PON AU54XC355-EVC
set asr.admin_section.ICSC SP42
set asr.admin_section.REQTYP value:SD
set reqTyp2 value:D
set asr.admin_section.ACT value:C

#ICASR
set asr.admin_section.VER 1
set icsc_ind B
set asr.admin_section.CC C
set asr.admin_section.UNE D
set asr.admin_section.QA E
set cbd F
set asr.admin_section.DDD 2010-06-15
set fdt H
set asr.admin_section.PROJECT MJCASECJRSTW
set asr.admin_section.CCI J
set casenum K
set asr.admin_section.PPTD L
set asr.admin_section.NOR M
set nor_t N
set asr.admin_section.LUP O
set asr.admin_section.BSA P
set asr.admin_section.ACTI Q
set asr.admin_section.QSA R
set asr.admin_section.WST S
set asr.admin_section.LATA T
set asr.admin_section.EVCI A
set asr.admin_section.RTR F
set asr.admin_section.SUP 1
set asr.admin_section.AFO W
set asr.admin_section.QNAI X
set asr.admin_section.TQ Y
set asr.admin_section.EXP Z
set eda AA
set asr.admin_section.AENG BB
set asr.admin_section.ALBR CC
set asr.admin_section.AGAUTH DD
set asr.admin_section.DATED 2010-06-16
set asr.admin_section.CUST SPRINT SPECTRUM L.P.
set la GG
set asr.admin_section.LADATED 2010-06-17
set asr.admin_section.LANM II
set jpr JJ
set asr.admin_section.FBA KK
set asr.admin_section.FNI LL
set asr.admin_section.FNT MM
set asr.admin_section.RFNI NN
set asr.admin_section.CFNI OO
set asr.admin_section.PSL PP
set asr.admin_section.PSLI QQ
set unit RR
set piu SS
set asr.admin_section.PLU TT
set asr.admin_section.WSI UU
set asr.admin_section.LTP VV
set asr.admin_section.SPEC OEMAR1
set asr.admin_section.CKR XX
set asr.admin_section.ECCKT YY
set asr.admin_section.QTY 1
set asr.admin_section.BAN A1
set asr.admin_section.ASG A2
set asr.admin_section.BIC a
set "asr.admin_section.BIC TEL" 613 222 1234
set srn A6
set "asr.admin_section.BIC ID" A7
set asr.admin_section.TSC A8
set istn_area A9
set istn_exch A10
set istn_num A11
set asr.admin_section.ACTL A12
set asr.admin_section.APOT A13
set asr.admin_section.RORD A14
set ethervc.uni_mapping_details.0.RPON A15
set asr.admin_section.LAG b
set asr.admin_section.CCVN A16
set asr.admin_section.ASC-EF A17
set asr.admin_section.TSP A18
set tsp_prior A19
set asr.admin_section.SAN A20
set asr.admin_section.AFG A21
set asr.admin_section.REMARKS NEW POINT TO POINT EVC

# ICEUA
set muxloc EA1
set hvp a
set sr2 EA3
set secloc CATHNGAMA0AW
set sei Y
set cfau b
set scfa EA5
set sdir c
set sfni EA7
set smuxloc EA8
set sccea EA9
set s25 EA10
set exr d
set otc EA12
set secloc_geto e
set gbtn 613-555-1234
set gcon EA14
set gtel 614-556-1235 ext 67
set secnci 02CXF.1GE
set qpr f
set sectlv_t "   . T"
set sectlv_r "   . R"
set sec_adm EA16
set es EA17
set profe EA18a
set profi EA18b
set serv_remarks EA19


# ICADM
set asr.bill_section.BILLNUM SPRINT SPECTRUM L.P.
set asr.bill_section.SBILLNUM B2
set asr.bill_section.ACNA MJC
set asr.bill_section.TE a
set asr.bill_section.STREET 12150 MONUMENT DRIVE
set asr.bill_section.FLOOR B5
set asr.bill_section.ROOM ST 700
set asr.bill_section.VTA B7
set fusf E
set asr.bill_section.CITY FAIRFAX
set asr.bill_section.STATE VA
set "asr.bill_section.ZIP CODE" 22033
set asr.bill_section.VCVTA B11
set asr.bill_section.BILLCON KARI DIDONATO
set "asr.bill_section.TEL NO" 913 762 6474
set asr.bill_section.EBP B17
set asr.bill_section.IWBAN B18
set "asr.bill_section.BILLCON EMAIL" B19
set asr.bill_section.PNUM B20
set asr.bill_section.PSD B21
set asr.contact_section.INIT.INIT 
set "asr.contact_section.INIT.TEL NO" 773 404 2827
set "asr.contact_section.INIT.INIT FAX NO" 6134441234
set "asr.contact_section.INIT.INIT EMAIL" 
set asr.contact_section.DSGCON.DSGCON CHARLIE BROWN
set "asr.contact_section.DSGCON.TEL NO" 773 404 2827
set "asr.contact_section.DSGCON.DSG FAX NO" 613 555 1234
set asr.contact_section.DRC B30
set asr.contact_section.FDRC B31
set asr.contact_section.STREET 1060 WEST ADDISON ST
set asr.contact_section.FLOOR B33
set asr.contact_section.ROOM B34
set asr.contact_section.CITY CHICAGO
set asr.contact_section.STATE IL
set "asr.contact_section.ZIP CODE" 60613
set "asr.contact_section.DSGCON.DSG EMAIL" CHARLIE.BROWN@CENX.COM
set asr.contact_section.IMPCON.IMPCON TECH ON DUTY
set "asr.contact_section.IMPCON.TEL NO" 877 379 8378
set asr.contact_section.MTCE B44
set "asr.contact_section.TEL NO" 613 232 1234
set mtce_exch B46
set mtce_num B47
set mtce_email B48
set "asr.contact_section.CB TEL NO" 613 333 1234
set asr.contact_section.CBPC B52

#ICEUS
set eusa.circuit_detail_section.NC KRBJ
set eusa.circuit_detail_section.NCI 02LNF.A04
set tlv_t "   . T"
set tlv_r "   . R"
set pqpr a
set sr2 EU2
set sss b
set trf c
set hvp d
set mst e
set cklt EU7
set nsl f
set muxloc EU9
set pri_adm EU10
set nvc g
set pspeed EU12
set lmp h
set nu i
set bsc j
set etet k
set ctx 613-555-1234
set ipai l
set ip_address 192.168.0.1
set subnet_mask EU18
set wacd1 EU19
set wacd2 EU20
set lagid EU21
set divckt EU22
set divpon EU23
set priloc EU24
set priloc_cfau m
set cfa EU26
set dir n
set cpt1 EU28a
set cpt2 EU28b
set priloc_s25 EU29
set priloc_exr o
set priloc_otc EU31
set geto p
set prigbtn 613-555-1235
set priloc_gcon EU33
set prigtel 613-555-1236 ext 78
set ccea EU34
set ctx_lstd_nm  EU35
set serv_remarks  EU36

#ICEVC
set uref 01
set evcnum EV1
set eusa.circuit_detail_section.NC KRBJ
set evcid EV2
set nut a
set evcckr EV4
set aunt EV5
set uact N
set rpon_uni EV6
set nci_uni 02LNF.A04
set l2cp EV7
set root_leaf b
set evcsp GSVLGAMA1XW
set ruid "38/KSGN/600079/   /SB"
set cevlan EV9
set cevlan_2 EV10
set cevlan2_1 EV11
set cevlan2_2 EV12
set cevlan3_1 EV13
set cevlan3_2 EV14
set cevlan4_1 EV15
set cevlan4_2 EV16
set cevlan5_1 EV17
set cevlan5_2 EV18
set cevlan6_1 EV19
set cevlan6_2 EV20
set cevlan7_1 EV21
set cevlan7_2 EV22
set cevlan8_1 EV23
set cevlan8_2 EV24
set cevlan9_1 EV25
set cevlan9_2 EV26
set cevlan10_1 EV27
set cevlan10_2 EV28
set cevlan11_1 EV29
set cevlan11_2 EV30
set cevlan12_1 EV31
set cevlan12_2 EV32
set cevlan13_1 EV33
set cevlan13_2 EV34
set cevlan14_1 EV35
set cevlan14_2 EV36
set pacevlan 0234
set pacevlan_2 EV37
set pacevlan2_1 EV38
set pacevlan2_2 EV39
set pacevlan3_1 EV40
set pacevlan3_2 EV41
set pacevlan4_1 EV42
set pacevlan4_2 EV43
set pacevlan5_1 EV44
set pacevlan5_2 EV45
set pacevlan6_1 EV46
set pacevlan6_2 EV47
set pacevlan7_1 EV48
set pacevlan7_2 EV49
set pacevlan8_1 EV50
set pacevlan8_2 EV51
set pacevlan9_1 EV52
set pacevlan9_2 EV53
set pacevlan10_1 EV54
set pacevlan10_2 EV55
set pacevlan11_1 EV56
set pacevlan11_2 EV57
set pacevlan12_1 EV58
set pacevlan12_2 EV59
set pacevlan13_1 EV60
set pacevlan13_2 EV61
set pacevlan14_1 EV62
set pacevlan14_2 EV63
set svlan_ind c
set svlan EV65
set pa_svlan EV66
set evcmpid EV67
set lref1 1
set losact1 N
set los1 EV68
set spec1 OEMAR1
set pbit1 EV69
set bdw1 50M
set dscp1 EV70
set dscp1_2 EV71
set tos1 EV72
set lref2 2
set losact2 d
set los2 EV75
set spec2 EV76
set pbit2 EV77
set bdw2 EV78
set dscp2 EV79
set dscp2_2 EV80
set tos2 EV81
set lref3 3
set losact3 e
set los3 EV84
set spec3 EV85
set pbit3 EV86
set bdw3 EV87
set dscp3 EV88
set dscp3_2 EV89
set tos3 EV90
set lref4 4
set losact4 f
set los4 EV93
set spec4 EV94
set pbit4 EV95
set bdw4 EV96
set dscp4 EV97
set dscp4_2 EV98
set tos4 EV99
set remarks_evc1 EV100

#ICSAP
set action_code I
set refnum 0001
set priloc_pi a
set name:pri_euname "SPRINT AT33XC198-UNI"
set priloc_aft b
set priloc_ncon c
set priloc_sapr SA1
set priloc_sano SA2
set priloc_sasf SA3
set priloc_sasd d
set priloc_sasn SA5
set priloc_sath SA6
set priloc_sass e
set priloc_ld1 SA8
set priloc_lv1 SA9
set priloc_ld2 SA10
set priloc_lv2 SA11
set priloc_ld3 SA12
set priloc_lv3 SA13
set priloc_city SA14
set priloc_state SA15
set priloc_zip SA16
set priloc_icol SA17
set priloc_aai SA18
set priloc_jkcde SA19
set priloc_jknum f
set priloc_jkpos g
set priloc_js h
set priloc_smjk i
set priloc_pca SA24
set priloc_si j
set priloc_spot SA26
set priloc_lcon LAVONNE NORRILS
set actel 630-607-4160
set paactel 614-556-1235 ext 79
set priloc_lcon_email SA28
set pri_alcon SA29
set palcon 615-557-1236 ext 80
set paalcon 616-558-1237 ext 81
set priloc_alcon_email SA30
set pri_acpgn SA31
set pri_acppn SA32
set priloc_acc SA33
set pri_wktel 617-559-1238 ext 82

#ICSPE
set ethervc.ethervc_detail.NC VLP-
set ethervc.uni_mapping_details.0.NCI C2 
set tlv_t C3
set tlv_r C4
set ethervc.uni_mapping_details.1.NCI C5
set pqpr C6
set sectlv_t C7
set sectlv_r C8
set sr2 C9
set s25 C10
set exr C11
set sss a
set atn C13
set trf b
set mst c
set hvp d
set cklt C17
set nsl C18
set etet e
set asr.admin_section.MUX C20
set cfau f
set cfa C22
set dir g
set cpt1 C24
set cpt2 C25
set scfau h
set scfa C27
set sdir i
set hban C29
set sfni C30
set pri_adm C31
set smuxloc C32
set nvc C33
set pspeed C34
set lmp j
set nu k
set bsc l
set geto m
set gbtn_area C39
set gbtn_exch C40
set gbtn_num C41
set ccea C42
set wacd1 C43
set lagid C44
set wacd2 C45
set ipai n
set ip_address C47
set subnet_mask C48
set divckt C49
set divpon C50
set serv_remarks C51

# ICSP2
set secloc D1
set qpr D2
set sei a
set sccea D3
set otc D4
set gcon D5
set gtel_area D6
set gtel_exch D7
set gtel_num D8
set gtel_ext D9
set ctx_area D10
set ctx_exch D11
set ctx_num D12
set ctx_lstd_nm D13
set sec_adm D14
set es b
set profe D16
set profi D17
set serv_remarks D18