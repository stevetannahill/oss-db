screenshot
type name:billnm {asr.bill_section.BILLNUM}
type name:sbillnm {asr.bill_section.SBILLNUM}
type name:icadmForm,name:acna {asr.bill_section.ACNA}
type name:te {asr.bill_section.TE}
type name:bill_str {asr.bill_section.STREET}
type name:bill_fl {asr.bill_section.FLOOR}
type name:bill_rm {asr.bill_section.ROOM}
type name:vta {asr.bill_section.VTA}
type name:fusf {asr.bill_section.FUSF}
type name:bill_city {asr.bill_section.CITY}
type name:bill_state {asr.bill_section.STATE}
type name:bill_zip {asr.bill_section.ZIP CODE}
type name:vcvta_adm {asr.bill_section.VCVTA}
type name:billcon {asr.bill_section.BILLCON}
type name:billcon_area {area({asr.bill_section.TEL NO})}
type name:billcon_exch {exch({asr.bill_section.TEL NO})}
type name:billcon_num {num({asr.bill_section.TEL NO})}
type name:billcon_ext {ext({asr.bill_section.TEL NO})}
type name:ebp {asr.bill_section.EBP}
type name:iwban {asr.bill_section.IWBAN}
type name:email {asr.bill_section.BILLCON EMAIL}
type name:pnum {asr.bill_section.PNUM}
type name:psd {asr.bill_section.PSD}
type name:init {asr.contact_section.INIT.INIT}
type name:init_area_cd {area({asr.contact_section.INIT.TEL NO})}
type name:init_exch {exch({asr.contact_section.INIT.TEL NO})}
type name:init_num {num({asr.contact_section.INIT.TEL NO})}
type name:init_ext {ext({asr.contact_section.INIT.TEL NO})}
type name:ifax_area {area({asr.contact_section.INIT.INIT FAX NO})}
type name:ifax_exch {exch({asr.contact_section.INIT.INIT FAX NO})}
type name:ifax_num {num({asr.contact_section.INIT.INIT FAX NO})}
type name:init_email {asr.contact_section.INIT.INIT EMAIL}
type name:dsgcon {asr.contact_section.DSGCON.DSGCON}
type name:dsgcon_area {area({asr.contact_section.DSGCON.TEL NO})}
type name:dsgcon_exch {exch({asr.contact_section.DSGCON.TEL NO})}
type name:dsgcon_num {num({asr.contact_section.DSGCON.TEL NO})}
type name:dsgcon_ext {ext({asr.contact_section.DSGCON.TEL NO})}
type name:dfax_area {area({asr.contact_section.DSGCON.DSG FAX NO})}
type name:dfax_exch {exch({asr.contact_section.DSGCON.DSG FAX NO})}
type name:dfax_num {num({asr.contact_section.DSGCON.DSG FAX NO})}
type name:drc {asr.contact_section.DRC}
type name:fdrc {asr.contact_section.FDRC}
type name:dsgcon_str {asr.contact_section.STREET}
type name:dsgcon_fl {asr.contact_section.FLOOR}
type name:dsgcon_rm {asr.contact_section.ROOM}
type name:dsgcon_city {asr.contact_section.CITY}
type name:dsgcon_state {asr.contact_section.STATE}
type name:dsgcon_zip {asr.contact_section.ZIP CODE}
type name:dsgcon_email {asr.contact_section.DSGCON.DSG EMAIL}
type name:impcon {asr.contact_section.IMPCON.IMPCON}
type name:impcon_area {area({asr.contact_section.IMPCON.TEL NO})}
type name:impcon_exch {exch({asr.contact_section.IMPCON.TEL NO})}
type name:impcon_num {num({asr.contact_section.IMPCON.TEL NO})}
type name:impcon_ext {ext({asr.contact_section.IMPCON.TEL NO})}
#type name:mtce {asr.contact_section.MTCE}
#type name:mtce_area {area({asr.contact_section.TEL NO})}
#type name:mtce_exch {exch({asr.contact_section.TEL NO})}
#type name:mtce_num {num({asr.contact_section.TEL NO})}
#type name:mtce_email {mtce_email}
#type name:cb_tel_area {area({asr.contact_section.CB TEL NO})}
#type name:cb_tel_exch {exch({asr.contact_section.CB TEL NO})}
#type name:cb_tel_num {num({asr.contact_section.CB TEL NO})}
#type name:cbpc {asr.contact_section.CBPC}
screenshot