class CsvReader
  class << self; attr_accessor :csv_file; end
  class << self; attr_accessor :csv_data; end
  
  
  @csv_file = ""
  @csv_data = nil
  @csv_file_timestamp = nil
  @key_col = nil
  
  def self.data(what)
    begin
      if @csv_data.blank? || File.mtime(@csv_file) > @csv_file_timestamp
        Rails.logger.info "Reading new file #{@csv_file}"
        @csv_data = CSV.open(@csv_file,"rb", {:headers => :first_row, :header_converters => :symbol})
        if @csv_data
          @csv_data = @csv_data.readlines
          @csv_file_timestamp = File.mtime(@csv_file)
          # Convert it to hash based on the key for fast lookup
          @csv_data = Hash[@csv_data.map {|x| [x[@key_col], x]}]
        end
      end
      @csv_data[what]
    rescue
      SW_ERR "Failed to process #{@csv_file} - Exception is #{$!} Traceback: #{$!.backtrace.join("\n")}"
      nil
    end
  end 
  
  def self.csv_file=(new_file)
    @csv_file = new_file
    @csv_data = nil
    @csv_file_timestamp = nil
  end
  
end