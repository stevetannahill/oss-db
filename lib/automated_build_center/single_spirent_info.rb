class SingleSpirentInfo < CsvReader
  @csv_file = AutomatedBuildCenter.abc_root +  "spirent_info/unique_circuit_data.csv"
  @csv_data = nil
  @csv_file_timestamp = nil
  @key_col = :circuit_id
end
