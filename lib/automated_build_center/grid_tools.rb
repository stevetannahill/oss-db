module GridTools
  E_TO_I_KEY = "Eve.grid.gci.e_to_i"
  I_TO_E_KEY = "Eve.grid.gci.i_to_e"
  LKP_KEY = "lkp:*:circuit"
  OSL_GRID_STOPPED = "GridTools:grid_stopped"
  OSL_GRID_STOPPING = "GridTools:grid_stopping"
  OSL_GRID_STOP_PROGRESS = "GridTools:grid_stop_progress"
  
  @@grid_app = nil
  @@osl_app = nil
  @@lkp_redis = nil
  @@grid_thread = nil
  
  def self.pause_sleep_time
    13
  end

  def self.circuit_cid_gid(id)
    get_redis_and_apps

    is_grid_id = (true if Float(id) rescue false)
    
    if is_grid_id
      grid_id = id
      if @@grid_redis.hget(I_TO_E_KEY, grid_id).nil?
        grid_id = nil
      end
      circuit_id = @@grid_redis.hget(I_TO_E_KEY, grid_id)
    else
      circuit_id = id
      if @@grid_redis.hget(E_TO_I_KEY, circuit_id).nil?
        circuit_id = nil
      end
      grid_id = @@grid_redis.hget(E_TO_I_KEY, circuit_id)
    end
    return [circuit_id, grid_id]
  end
  
  def self.circuit_info(id)
    get_redis_and_apps
    details = ""
    seperator = "\n" + ("+"*80) + "\n" 
    details << "Circuit Information about #{id}\n"

    circuit_id, grid_id = circuit_cid_gid(id)

    details <<  "Grid Id is #{grid_id.nil? ? "Unknown" : grid_id} Circuit id is #{circuit_id.blank? ? "Unknown" : circuit_id}"
    
    if circuit_id || grid_id
      grid_id = grid_id.to_i
      keys = @@lkp_redis.keys("lkp:#{grid_id}:*")
      details << "#{seperator}LKP redis keys\n"
      details << keys.join("\n")
    
      details << seperator
      if grid_id
        last_record = DataArchiveIf.get_last_record("ethframedelaytest", "sample_end_epoch", {'grid_circuit_id' => grid_id})
        details << "last sample time in Data Archive is #{last_record ? Time.at(last_record["sample_end_epoch"]) : "No records"}"
      else
        details << "Grid id is not valid so cannot retrive data from DataArchive"
      end
    
      if circuit_id
        details << seperator
        ctv = SpirentCosTestVector.find_by_circuit_id(circuit_id)
        if ctv
          details << "CosTestVector exists on cascade #{ctv.site_name}\n"
          ctvs = SpirentCosTestVector.find(:all, :conditions => ["circuit_id LIKE ?", "#{ctv.site_name}%"])
          details << "CosTestVecotrs on Cascade #{ctv.site_name}\n#{ctvs.collect{|c| c.circuit_id}.join("\n")}"
        else
          details << "Cannot find Circuit on CDB"
        end
        
        details << seperator
        bl = BuilderLog.find_by_name(circuit_id)
        if bl
          details << "Builder log: Grid id #{bl.grid_id} Summary: #{bl.summary} Summary detail: #{bl.summary_detail}\n"
          details << "Details:\n#{bl.details}"
        else
          details << "Builder Log does not exist\n"
        end
      end
    end
    return details
  end
    
  def self.remove_circuit(id)
    get_redis_and_apps
    if !grid_stopped?
      return "Grid is not stopped"
    end
    
    is_grid_id = (true if Float(id) rescue false)
    
    # get the other id
    if is_grid_id
      grid_id = id
      circuit_id = @@grid_redis.hget(I_TO_E_KEY, grid_id)
    else
      circuit_id = id
      grid_id = @@grid_redis.hget(E_TO_I_KEY, circuit_id).to_i
    end
    
    return "Cannot find grid id (#{grid_id}) and/or circuit id (#{circuit_id})" unless grid_id && circuit_id
    
    # check if removing the circuit would effect other circuits.
    valid, reason = valid_to_remove(circuit_id)

    if valid
      # check to make sure it isn't an active circuit
      last_record = DataArchiveIf.get_last_record("ethframedelaytest", "sample_end_epoch", {'grid_circuit_id' => grid_id})
      if last_record && last_record["sample_end_epoch"] > (4.hours.ago).to_i
        return "The circuit looks like it is live - last sample time was #{last_record ? Time.at(last_record["sample_end_epoch"]) : "No records"}"
      else 
        # delete circuit from cdb, lkp, and grid
        return "Failed to delete from CDB! All changes rolled back." unless cdb_remove_circuit(circuit_id, grid_id)
        return "Failed to delete from LKP" unless delete_lkp(grid_id)
        return "Failed to delete from Grid" unless delete_grid(grid_id)
        # we are no longer deleting data from the data archive. This will be handled by the archive's cleaner program. 
        #DataArchiveIf.delete_spirint_grid("ethframedelaytest", grid_id)
        nil
      end
    else
      return reason
    end
  end
  
  def self.get_grid_info
    exec = GridTools.currently_executing
    latest = GridTools.grid_latest
    stopped = GridTools.grid_stopped?
    stopping = GridTools.grid_stopping?
    progress = GridTools.grid_stop_progress.split("\n").reverse
    return {exec: exec, latest: latest, stopped: stopped, stopping: stopping, progress: progress}
  end
  
  def self.currently_executing
    get_redis_and_apps
    processes = @@grid_app.cmd("eve exec?",true,false)[:data]
    if processes and processes[:exec]
      newest_pid, newest_execs = processes[:process_info].first
      processes[:process_info].each do |pid, execs|
        if newest_execs.last["started"] < execs.last["started"]
          newest_pid = pid
          newest_execs = execs
        end
      end
      { newest_pid => newest_execs.last }
    else
      newest = "Nothing executing"
    end
  end

  def self.grid_latest
    get_redis_and_apps
    latest = []
    sources = @@grid_app.cmd("source",true,false)[:data]
    sources.each_with_index do |data,index|
      path,storage = data
      storage_path = storage.storage_path(false)
      # HACK - should be a message
      last_accessed = @@grid_redis.get(@@grid_app.lookup_id("sources.#{storage.storage_path}.last_accessed"))
      last_accessed = "Never" if last_accessed.blank?
      latest << "The last poll for #{storage_path} was completed at #{last_accessed} UTC"
    end
    return latest
  end
  
  def self.grid_stopped?(osl_output = false)
    get_redis_and_apps(false)
    @@osl_redis.get(OSL_GRID_STOPPED) == "true"
  end
  
  def self.grid_stopping?
    get_redis_and_apps(false)
    @@osl_redis.get(OSL_GRID_STOPPING)
  end
  
  def self.grid_stop_progress(last_progress = "")
    get_redis_and_apps(false)
    if @@osl_redis.get(OSL_GRID_STOP_PROGRESS)
      regex = Regexp.new("^#{last_progress}")
      @@osl_redis.get(OSL_GRID_STOP_PROGRESS).sub(regex, "")
    else
      ""
    end
  end
  
  def self.stop_grid(block = false, osl_output = false)
    return if grid_stopped?# || grid_stopping?
    get_redis_and_apps
    if @@grid_thread.nil? && !grid_stopping?
      @@osl_redis.del(OSL_GRID_STOP_PROGRESS)
      @@grid_thread = Thread.new do      
        begin
          setup_rabbitmq        
          num_consecutive_tries = 5

          # handle the case where grid is already pausing
          pausing = false 
          executing = currently_executing         
          # results is a hash {pid => {"started" => hh, "cmd" => "sleep 60"}}
          unless executing.is_a?(String)
            executing.each do |pid, data|
              pausing = data["cmd"] == "sleep #{pause_sleep_time}"
            end
          end

          unless pausing
            @@osl_redis.del(OSL_GRID_STOP_PROGRESS)
            @@osl_redis.set(OSL_GRID_STOPPING, "Stopping")
            @@osl_redis.set(OSL_GRID_STOP_PROGRESS, "Waiting for Grid to be idle\n")
            wait_for_grid_status("sleep 300", osl_output)
          end
          @@grid_app.cmd("pause #{pause_sleep_time} grid")
          @@osl_redis.set(OSL_GRID_STOPPING, "Stopping-Grid paused")
          @@osl_redis.append(OSL_GRID_STOP_PROGRESS, "Grid paused\n")
          @@osl_redis.append(OSL_GRID_STOP_PROGRESS, "Waiting for Grid to become paused\n")
          wait_for_grid_status("sleep #{pause_sleep_time}", osl_output)

          # Now check queues cid.task_queue, lkp.prism.task_queue, da.task_queue
          all_queues_empty = 0
          begin
            raise "Grid restarted aborting the stop" unless grid_stopping?
            q_sizes = {}
            [AutomatedBuildCenter::QUEUE, "lkp.prism.task_queue", "da.task_queue"].each do |q|
              result = @@grid_app.cmd("sub ls #{q}", osl_output)
              if result && result[:data]
                q_sizes.merge!({q => result[:data].size})
              else
                @@osl_redis.append(OSL_GRID_STOP_PROGRESS, "Error can't find the queue size for '#{q}'\n")
                q_sizes.merge!({q => 0})
              end
            end
            if q_sizes.values.all? {|q| q == 0}
              all_queues_empty += 1
            else
              all_queues_empty = 0
            end
            @@osl_redis.append(OSL_GRID_STOP_PROGRESS, "Waiting for queue sizes to be zero #{all_queues_empty} #{q_sizes.inspect}\n")
            sleep 10
          end until all_queues_empty >= num_consecutive_tries
    
          # Now check ABC is not processing anything
          number_not_processing = 0
          begin
            raise "Grid restarted aborting the stop" unless grid_stopping?
            result = @@osl_app.cmd("cid current", osl_output)
            if result && result[:data] && result[:data].size == 0
              number_not_processing += 1
            else
              number_not_processing = 0
            end
            sleep 10
            @@osl_redis.append(OSL_GRID_STOP_PROGRESS, "Waiting for ABC to finish processing #{number_not_processing}\n")        
          end until number_not_processing >= num_consecutive_tries

          # now check lkp redis queues
          keys = @@lkp_redis.keys("lkp:*:to_be_processed")
          number_of_zero_q_lengths = 0
          found_zero_q_lengths = false
          begin
            raise "Grid restarted aborting the stop" unless grid_stopping?
            zero_qs = {}
            keys.each {|key| zero_qs.merge!({key => @@lkp_redis.llen(key)})}
            if zero_qs.values.all? {|size| size == 0}
              number_of_zero_q_lengths += 1
            else
              number_of_zero_q_lengths = 0
            end
            @@osl_redis.append(OSL_GRID_STOP_PROGRESS, "Waiting for LKP redis queues to be zero #{number_of_zero_q_lengths} #{zero_qs.inspect}\n")        
            sleep 10
          end until number_of_zero_q_lengths >= num_consecutive_tries
    
          @@osl_redis.set(OSL_GRID_STOPPED, "true") if grid_stopping?
        rescue Exception => e
          @@osl_redis.append(OSL_GRID_STOP_PROGRESS, "Exception #{e.message}\n")    
        ensure
          @@grid_thread = nil    
          @@osl_redis.del(OSL_GRID_STOPPING)
        end        
      end
    end
    
    if block
      progress = ""
      begin        
        partial = grid_stop_progress(progress)
        progress = progress + partial
        print partial
        sleep 10
      end until grid_stopped? || !grid_stopping?
    end
    return @@grid_thread
  end
    
  def self.start_grid(osl_output = false)
    get_redis_and_apps(false)
    # Note don't use grid as the id - grid has been paused and a call to Eve::Application will block
    # So resueme by using another id ie osl.poe
    @@osl_app.cmd("resume grid", osl_output)
    @@osl_redis.del(OSL_GRID_STOPPED)   
    @@osl_redis.del(OSL_GRID_STOPPING) 
    @@grid_thread = nil
  end
  
  private
  
  def self.cdb_remove_circuit(circuit_id, grid_id)
    ActiveRecord::Base.transaction(:requires_new => true) do
      bl = BuilderLog.find_by_name(circuit_id)
      bl.destroy if bl
      ctv = SpirentCosTestVector.find_by_circuit_id(circuit_id)
      if ctv
        paths = ctv.cos_end_point.segment_end_point.segment.paths
        ctv.destroy
        if CircuitIdFormat::SpirentSprint.valid(circuit_id)      
          decoded_cid = CircuitIdFormat::SpirentSprint.decode(circuit_id)
      
          # Delete the path if there are no circuits left on the cascade
          if ctv = CosTestVector.find(:first, :conditions => ["circuit_id LIKE ?", "#{decoded_cid[:site_name]}-%"]).nil?
            # Delete the path
            puts "Deleting path #{paths.first.cenx_name}"
            paths.first.destroy
          end
        end    
      end
      MetricsMonthlyCosTestVector.where(:grid_circuit_id => grid_id).destroy_all if grid_id
      MetricsWeeklyCosTestVector.where(:grid_circuit_id => grid_id).destroy_all if grid_id
      return true
    end
    nil
  end
  
  def self.delete_lkp(grid_id)    
    keys = @@lkp_redis.keys("lkp:#{grid_id}:*")
    keys.each {|key| @@lkp_redis.del(key)}
    #redis 127.0.0.1:6379[3]> keys lkp:14272*
    #1) "lkp:14272:1346025461:availability"
    #2) "lkp:14272:M:1343779200:tracking"
    #3) "lkp:14272:M:1346457600:tracking"
    #4) "lkp:14272:circuit"
    #5) "lkp:14272:cos_test_vector"
    #6) "lkp:14272:availability_reset"
    true
  end
  
  def self.delete_grid(grid_id)
    if grid_id
      @@grid_app.cmd("gci rm #{grid_id}")
      true
    end
  end
  
  def self.valid_to_remove(circuit_id)
    if (ctv = SpirentCosTestVector.find_by_circuit_id(circuit_id)).blank?
      return true, "Can't find the circuit #{circuit_id}"
    elsif !(ref_circuits = SpirentCosTestVector.find_all_by_ref_circuit_id(circuit_id)).blank?
      return false, "Can't delete the circuit as other circuits reference this circuit as a reference circuit #{ref_circuits.collect{|ctv| ctv.circuit_id}}"
    elsif !(ref_circuits = SpirentCosTestVector.find_all_by_last_hop_circuit_id(circuit_id)).blank?
      return false, "Can't delete the circuit as other circuits reference this circuit as a last hop #{ref_circuits.collect{|ctv| ctv.circuit_id}}"
    else
      return true, nil
    end
  end
  
  def self.wait_for_grid_status(cmd, osl_output = false)
    grid_entered_state = false
    begin
      # When grid exits it does not always clean up the redis database. Can't check the process id to see if it exists
      # because Grid can be running on a different server. Until this is fixed take the one with the most recent time
      raise "Grid restarted aborting the stop" unless grid_stopping?
      executing = currently_executing        
      # results is a hash {pid => {"started" => hh, "cmd" => "sleep 60"}}
      if executing and !executing.is_a?(String)
        executing.each do |pid, data|
          grid_entered_state = data["cmd"] == cmd
        end
      end
      @@osl_redis.append(OSL_GRID_STOP_PROGRESS, "Waiting for Grid to be #{cmd}\n")
      sleep 10 unless grid_entered_state
    end until grid_entered_state
  end
  
  def self.get_redis_and_apps(get_grid_app = true)
    if !@@grid_app && get_grid_app
      @@grid_app = Eve::Application.new(:config => "#{Rails.root}/config/grid.yml", :observer => true)
      GridApp.eve = @@grid_app
      GridApp.eve.run([])
      @@grid_redis = @@grid_app.redis
    end

    if !@@osl_app
      @@osl_app = Eve::Application.new(:config => "#{Rails.root}/config/osl.poe.yml")
      OslApp.eve = @@osl_app
      OslApp.eve.run([])
      @@osl_redis = @@osl_app.redis
    end
    
    if !@@lkp_redis
      @@lkp_redis = LKP.redis
    end
  end
  
  def self.setup_rabbitmq    
    #Ensure the rabbitmq server is configured
    osl_app = Eve::Application.new(:config => "#{Rails.root}/config/osl.poe.yml")
    OslApp.eve = osl_app
    OslApp.eve.run([])
    osl_app.cmd("run #{Rails.root}/config/abc_config.cmd")
  end
     
end