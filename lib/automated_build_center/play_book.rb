class PlayBook < CsvReader
  @csv_file = AutomatedBuildCenter.abc_root +  "playbook/playbook.csv"
  @csv_data = nil
  @csv_file_timestamp = nil
  @key_col = :cascade  
end
