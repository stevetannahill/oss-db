
class MonitoredEndpointStats < Stats

  def get_supported_stats
    if (!@obj.cos_end_point.segment_end_point.is_monitored)
      return []
    else
      return [:availability, :flr, :delay]
    end
  end
  
  def stats_get_delay_tests(ignore_cos = false)   
    # If there are no COS then don't return the tests
    return_tests = {}
    {"dv" => "delay_variation", "delay" => "delay"}.each { |sla_attribute, attribute|
      return_tests[attribute] = [] 
      return_tests[attribute] = [""] if stats_get_slas(sla_attribute).first.size != 0 || ignore_cos
    }
    return return_tests
  end
  
  def get_delay_attributes(attribute_types)
    attribute_types = [attribute_types] if !attribute_types.is_a?(Array)
    attributes = attribute_types.uniq.collect do |attribute|
      case attribute
      when :sla
        [:delay, :delay_variation]
      when :troubleshooting
        case @obj.test_type
        when 'Ethernet Frame Delay Test', 'Ethernet Loopback Test'
          [:min_delay, :max_delay, :min_delay_variation, :max_delay_variation]
        when 'Ping Active Test'
          [:min_delay, :max_delay]
        else
          SW_ERR "ERROR: Unknown Test Type: #{@obj.test_type}\n"
          []
        end
      else
        SW_ERR "Invalid attribute type #{attribute}"
        []
      end
    end.flatten
    
    return attributes
  end
  
  def stats_get_flr_slas(ignore_sla = false)
    sla, sla_id = stats_get_slas("flr")
    sla_info = {}
    cep = @obj.cos_end_point
    sla_info[cep.cos_name] = {:cos_id => sla_id.keys.first, :sla_threshold => sla.values.first[:sla_threshold].to_s}
    sla_info[cep.cos_name][:sla_warning_threshold] = (sla.values.first[:sla_warning_threshold].to_f).to_s
    sla_info[cep.cos_name][:sla_error_threshold] = (sla.values.first[:sla_error_threshold].to_f).to_s

    return sla_info
  end
  

  def stats_get_availability_slas
    sla = "N/A"
    if !@obj.cos_end_point.segment_end_point.is_monitored
      SW_ERR "The endpoint is not monitored"
    else
      sla = @obj.availability_guarantee
    end   
    return sla 
  end
  
  def stats_get_slas(sla_attribute, ignore_cos = false)
    slas = {}        
    sla_ids = {}
    cep = @obj.cos_end_point
    if !cep.segment_end_point.is_monitored
      SW_ERR "The endpoint is not monitored(#{cep.segment_end_point.is_monitored})"
    elsif !sla_attribute.nil?
      mapping = {"flr" => :frame_loss, "dv" => :delay_variation, "delay" => :delay}
    
      cep = @obj.cos_end_point
      slas[cep.cos_name] = {}
      slas[cep.cos_name][:sla_threshold] = @obj.get_errored_period_threshold(:critical, mapping[sla_attribute]) 
      slas[cep.cos_name][:sla_error_threshold] = @obj.get_errored_period_threshold(:major, mapping[sla_attribute])
      slas[cep.cos_name][:sla_warning_threshold] = @obj.get_errored_period_threshold(:minor, mapping[sla_attribute])
      sla_ids[cep.id] = {}      
      sla_ids[cep.id][:sla_threshold] = slas[cep.cos_name][:sla_threshold]
      sla_ids[cep.id][:sla_error_threshold] = slas[cep.cos_name][:sla_error_threshold]
      sla_ids[cep.id][:sla_warning_threshold] = slas[cep.cos_name][:sla_warning_threshold]
    end
    return slas, sla_ids
  end
  
  # Get the Reference or "Turn-up" test results 
  def stats_get_reference_data(attribute)
    mapping = {:delay => :delay_reference, :min_delay => :delay_min_reference, :max_delay => :delay_max_reference,
               :delay_variation => :dv_reference, :min_delay_variation => :dv_min_reference, :max_delay_variation => :dv_max_reference,
               :frame_loss_ratio => :flr_reference}
    ref_attribute = mapping[attribute]
    cep = @obj.cos_end_point
    ref = {cep.cos_name => {}}
    ref_ids = {cep.id => {}}
          
    if !cep.segment_end_point.is_monitored
      SW_ERR "The endpoint is not monitored(#{cep.segment_end_point.is_monitored})"
    elsif ref_attribute && !@obj.send(ref_attribute).blank?
      ref[cep.cos_name] = {}
      ref[cep.cos_name][:reference] = @obj.send(ref_attribute).to_f
      ref_ids[cep.id] = {}      
      ref_ids[cep.id][:reference] = ref[cep.cos_name][:reference]
    end
    return ref, ref_ids
  end
  

  def stats_get_delay_sla_method(delay_type)
    case delay_type
    when :delay then 'delay'
    when :delay_variation then 'dv'
    else
      nil
    end
  end

  def convert_keys_to_where keys
    where = []
    keys.each do |key|
      where << key.map { |k, v| "`#{k}`='#{v}'" }
    end
    where
  end

  def compute_all_endpoint_sla_exceptions
    [:availability, :frame_loss_ratio, :delay, :delay_variation ].each { |metric_type| compute_endpoint_sla_exceptions metric_type}
  end

  # computes all severity of exceptions and verify:
  #  a minor is detected when a major or a critical is detected
  #  a major is detected when a critical is detected
  def test_exception_validity cos_test_vector, metric_type
    minor = false
    major = false
    critical = false
    ###1) is there a minor exception?
    metric_type = :frame_loss if :frame_loss_ratio == metric_type
    minor_errored_count = count_errored_periods( :minor, metric_type)
    min_count_threshold = cos_test_vector.get_errored_period_max_errored_count( :minor, metric_type)
    if minor_errored_count > min_count_threshold
      #min_time_over = minor_errored_count * cos_test_vector.get_errored_period_delta_time_secs
      minor = true
    end

    ###2) is there a major exception?
    metric_type = :frame_loss if :frame_loss_ratio == metric_type
    major_errored_count = count_errored_periods( :major, metric_type)
    max_count_threshold = cos_test_vector.get_errored_period_max_errored_count( :major, metric_type)
    if major_errored_count > max_count_threshold
      #max_time_over = minor_errored_count * cos_test_vector.get_errored_period_delta_time_secs
      major = true
    end

    ###3) is there a critical exception?
    metric_type = :frame_loss_ratio if :frame_loss == metric_type
    metric = compute_metric metric_type
    unless metric.nil?
    value = metric
    sla = cos_test_vector.get_errored_period_threshold(:critical, metric_type)
    if (metric > sla)
      critical = true
    end
    else
puts "%%%%%%%%%%% compute metric ERROR id(#{cos_test_vector.id}) metric(#{metric_type}) critical(#{critical}), major(#{major}), minor(#{minor}) minor_errored_count(#{minor_errored_count}), min_count_threshold(#{min_count_threshold}), max_count_threshold(#{max_count_threshold}), major_errored_count(#{major_errored_count})"
    end
    if ((critical and ( not major or not minor)) or (major and not minor))
      
    sla_crit  = cos_test_vector.get_errored_period_threshold(:critical, metric_type)
    sla_major = cos_test_vector.get_errored_period_threshold(:major, metric_type)
    sla_minor  = cos_test_vector.get_errored_period_threshold(:minor, metric_type)

      puts "%%%%%%%%%%% ERROR id(#{cos_test_vector.id}) metric(#{metric_type}) critical(#{critical}), major(#{major}), minor(#{minor}) minor_errored_count(#{minor_errored_count}), min_count_threshold(#{min_count_threshold}), max_count_threshold(#{max_count_threshold}), major_errored_count(#{major_errored_count}), value(#{value}), sla(#{sla_crit}), sla_major(#{sla_major}), sla_minor(#{sla_minor})"
    end

  end

  def compute_the_exception cos_test_vector, ex, metric_type
    new_exception = ex.nil?
    ex = ExceptionSlaCosTestVector.new  if new_exception
    now = Time.now
    if (@end_time> now)
      ex.time_declared  = now.to_i
    else
      ex.time_declared  = @end_time.to_i
    end
    ex.sla_type = ExceptionSlaCosTestVector.to_exception_sla_type(metric_type)
    ex.grid_circuit_id = cos_test_vector.grid_circuit_id

    # If there are NO periods above the minor threshold then there is noting to do
    if (new_exception || (:major == ex.severity) || (:minor == ex.severity))
      minor_errored_count = count_errored_periods( :minor, metric_type)
      # If there are NO periods above the minor threshold then there is nothing to do
      if minor_errored_count > 0         
        metric = compute_metric metric_type
        unless metric.nan?
          ex.value = metric
          sla = cos_test_vector.get_errored_period_threshold(:critical, metric_type)
          if (metric > sla)
            ex.severity = :critical
            puts "!!!exception id(#{cos_test_vector.id}) metric(%-16s ) (%-8s) (#{ex.time_declared}) valid(#{ex.valid?}) #{metric} > #{sla}" %  [metric_type, ex.severity]
            ex.save!
          elsif new_exception || (:minor == ex.severity)
            metric_type = :frame_loss if :frame_loss_ratio == metric_type
            major_errored_count = count_errored_periods( :major, metric_type)
            if major_errored_count.nil?
              SW_ERR "Could not compute #{metric_type}. Are we connected to the archive?"
            else
              max_major_errored_count = cos_test_vector.get_errored_period_max_errored_count( :major, metric_type)
              if major_errored_count > max_major_errored_count
                ex.time_over = major_errored_count * cos_test_vector.get_errored_period_delta_time_secs
                ex.severity = :major
                puts "!!!exception id(#{cos_test_vector.id}) metric(%-16s ) (%-8s) (#{ex.time_declared}) valid(#{ex.valid?}) #{major_errored_count} > #{max_major_errored_count}" % [metric_type, ex.severity]
                ex.save!
              elsif new_exception
                max_minor_errored_count = cos_test_vector.get_errored_period_max_errored_count( :minor, metric_type)
                if minor_errored_count > max_minor_errored_count
                  ex.time_over = minor_errored_count * cos_test_vector.get_errored_period_delta_time_secs
                  ex.severity = :minor
                  puts "!!!exception id(#{cos_test_vector.id}) metric(%-16s ) (%-8s) (#{ex.time_declared}) valid(#{ex.valid?}) #{minor_errored_count} > #{max_minor_errored_count}" %  [metric_type, ex.severity]
                  ex.save!
                end
              end
            end
          end
        end
      end
    end
  end

  def compute_endpoint_sla_exceptions metric_type
    duration = @end_time.to_f - @start_time.to_f
    if !@obj.cos_end_point.segment_end_point.is_monitored
      SW_ERR "The endpoint is not monitored(#{@obj.segment_end_point.is_monitored})"
    else
      cos_test_vector = @obj
      exs = ExceptionSlaCosTestVector.find( :all, :conditions => [ "grid_circuit_id=? AND time_declared BETWEEN ? AND ?",cos_test_vector.grid_circuit_id,  @start_time.to_i, @end_time.to_i ])
      ex = exs.find{|ex| ex.sla_type == ExceptionSlaCosTestVector.to_exception_sla_type( metric_type) }
      if  :availability == metric_type
        avail= stats_availability_results
        unavailable_time_msecs= avail[:unavailable_time_msecs]
        unavailable_time = unavailable_time_msecs.to_f/1000
        sla_availability_monthly = avail[:sla_monthly]
        allowed_unavailable_time = (1.0 - (sla_availability_monthly.to_f/100)) * duration
        if (unavailable_time > allowed_unavailable_time)
          ex = ExceptionSlaCosTestVector.new  if ex.nil?
          now = Time.now
          if (@end_time> now)
            ex.time_declared  = now.to_i
          else
            ex.time_declared  = @end_time.to_i
          end
          ex.value = (duration.to_i-unavailable_time)/duration.to_i * 100
          ex.sla_type = :availability
          ex.grid_circuit_id = cos_test_vector.grid_circuit_id
          ex.severity = :critical
          puts "!!!exception! Availability (#{ex.severity}) (#{ex.time_declared}) valid(#{ex.valid?}) #{unavailable_time} > #{allowed_unavailable_time} (#{'%2.3f' % (unavailable_time/duration.to_i)}%)"
          ex.save!
        end
      else
        compute_the_exception cos_test_vector, ex, metric_type
      end
    end
  end
  
  def ref_circuit_flr
    return nil
  end

  #--------
  def compute_metric metric_type
    ref_where = get_ref_where
    results = compute_metrics [metric_type]
    if (ref_where.nil?) # not a differential
      return results[METRIC_NAMES[metric_type]]
    else
      return results["delta1_#{METRIC_NAMES[metric_type]}"]
    end
  end

  def compute_metrics metric_types
    results = {}
    time_name, table_name, column_info =  get_table_and_selected_columns
    time_periods_native = get_available_time_periods_native
    unless (metric_types.find_index(:availability).nil?)
      # availability is computed differently and is not yet computed by this API
      SW_ERR(":availability passed to compute_metrics")
    end
    where = get_where
    ref_where = get_ref_where
    # If we are completely unavailable there is no point in contacting the archive
    unless time_periods_native.nil? or time_periods_native.empty?
      query_results = DataArchiveIf.differential_query_and_summarize  metric_types, table_name, time_name, where, ref_where, column_info, time_periods_native
    end
    data = {}
    unless query_results.nil? or query_results[0].nil?
      data = query_results[0]
    end
    if (ref_where.nil?) # not a differential
      metric_types.each do |metric_type |
        key =METRIC_NAMES[metric_type]
        value = data[key]
        if value.nil?
          # This means there either was no data, only nil data or
          # that we were in unavailable time for the entire period.
          results[key] = 0.0
        else
          results[key] = value.to_f * @extrapolation_factor
        end
      end
    else
      metric_types.each do |metric_type |
        [METRIC_NAMES[metric_type],
          "ref1_#{METRIC_NAMES[metric_type]}",
          "delta1_#{METRIC_NAMES[metric_type]}"
        ].each do |key|
          value = data[key]
          if value.nil?
            # This means there either was no data, only nil data or
            # that we were in unavailable time for the entire period.
            results[key] = 0.0
          else
            results[key] = value.to_f * @extrapolation_factor
          end
        end
      end
    end
    return results
  end

=begin
  cos_test_vector = CosTestVector.find(24)
  metric_types = [ :frame_loss, :frame_loss_ratio, :delay, :delay_variation, :min_delay, :min_delay_variation, :max_delay, :max_delay_variation ]
  metric_types = [ :frame_loss, :delay, :delay_variation, :min_delay, :min_delay_variation, :max_delay, :max_delay_variation ]
  time_month_start = Time.at(1325368860)
  time_month_end   = Time.at(1325368860+1209600+1209600)
  stats= cos_test_vector.stats_data(time_month_start, time_month_end, Time.now, false)
  x= stats.compute_metric_data_points metric_types
=end

  def compute_metric_data_points metric_types
    normalized_time_name = Stats::NORMALIZED_TIME_NAME
    time_name, table_name, column_info =  get_table_and_selected_columns
    # don't use available time for graph data. Return all data points regardless.
    time_periods_native = [ [@start_time.to_i, @end_time.to_i] ]
    return []  if time_periods_native.nil? or time_periods_native.empty?
    where = get_where
    ref_where = get_all_ref_where
    time_increment_native = to_native @collection_period_secs
    adjusted_time_periods_native  = time_periods_native.clone
    adjusted_time_periods_native[0][0]= to_native adjust_start_time
    adjusted_time_periods_native = adjusted_time_periods_native.map {|time| [time[0], time[1]]  }
    result_data = DataArchiveIf.differential_query_and_aggregate metric_types, table_name, time_name, normalized_time_name, where, ref_where, column_info, time_periods_native, time_increment_native
    return result_data
  end


  # Count how many times the metric aggregated over the time increment was above the specified threshold.
  def count_errored_periods  severity_level, metric_type
    cos_test_vector = @obj
    time_name, table_name, column_info =  get_table_and_selected_columns
    time_periods_native = get_available_time_periods_native
    return 0  if time_periods_native.nil? or time_periods_native.empty?
    where = get_where
    ref_where = get_ref_where
    time_increment_secs = cos_test_vector.get_errored_period_delta_time_secs
    time_increment_secs = collection_period  if(time_increment_secs <= collection_period)
    time_increment_native = to_native time_increment_secs
    threshold = cos_test_vector.get_errored_period_threshold  severity_level, metric_type
    results = DataArchiveIf.differential_query_aggregate_and_count metric_type, table_name, time_name, where, ref_where, column_info, time_periods_native, time_increment_native, threshold
    count = results[0]['COUNT']
    return count
  end

end
