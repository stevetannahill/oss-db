class BxStatsArchive  < MonitoredEndpointStats


  ETH_FRAME_DELAY_TEST_TABLE = "com.brixnet.swv.ethframedelaytest"

  ETH_FRAME_DELAY_TEST_COLUMNS = {
    #    :availability => 'n/a',
    #    :frame_loss => 'framesLost',
    #    :frame_loss_ratio => 'frameLossRatio',
    :delay => 'AverageNetworkRoundTripLatency/2',
    :delay_variation => 'AverageNetworkRoundTripLatencyVariation/2' ,
    :min_delay => 'MinimumNetworkRoundTripLatency/2',
    :min_delay_variation => 'MinimumNetworkRoundTripLatencyVariation/2',
    :max_delay => 'MaximumNetworkRoundTripLatency/2',
    :max_delay_variation => 'MaximumNetworkRoundTripLatencyVariation/2' ,
    #    :octets_count => 'octets_count',
    :frames_sent => 'FramesTransmitted',
    :frames_received => 'ValidFramesReceived',
  }
  
  PING_TEST_TABLE = "com.brixnet.pingactivetest"

  PING_TEST_COLUMNS = {
    #    :availability => 'n/a',
    #    :frame_loss => 'framesLost',
    #    :frame_loss_ratio => 'frameLossRatio',
    :delay => 'AverageRoundTripLatency/2',
    :delay_variation => 'AverageJitter/2' ,
    :min_delay => 'MinimumRoundTripLatency/2',
    :min_delay_variation => nil,
    :max_delay => 'MaximumRoundTripLatency/2',
    :max_delay_variation => nil ,
    #    :octets_count => 'octets_count',
    :frames_sent => '59',
    :frames_received => '59 - NumberofNoResponses',
  }

  ETH_LOOPBACK_TEST_TABLE = "com.brixnet.swv.ethloopbacktest"
  
  ETH_LOOPBACK_TEST_COLUMNS = {
    #    :availability => 'n/a',
    #    :frame_loss => 'framesLost',
    #    :frame_loss_ratio => 'frameLossRatio',
    :delay => 'AverageRoundTripLatency/2',
    :delay_variation => 'AverageRoundTripLatencyVariation/2' ,
    :min_delay => 'MinimumRoundTripLatency/2',
    :min_delay_variation => 'MinimumRoundTripLatencyVariation/2',
    :max_delay => 'MaximumRoundTripLatency/2',
    :max_delay_variation => 'MaximumRoundTripLatencyVariation/2' ,
    #    :octets_count => 'octets_count',
    :frames_sent => 'FramesTransmitted',
    :frames_received => 'ValidFramesReceived',
  }

  
  NSEC_IN_SEC = 1000000000
  NSEC_IN_MILLISEC = 1000000

  def initialize(obj, start_time, end_time, current_time=Time.now, adjust_time=true)
    super(obj, start_time, end_time, current_time, adjust_time)
    # For Brix the time unit is nanoseconds so convert from seconds to nanosec
    @time_periods_nsecs = @time_periods_secs.map {|time| [time[0]*NSEC_IN_SEC, time[1]*NSEC_IN_SEC]  }
  end
  
  def timestamp_name
    "TimeStamp"
  end
  
  def stats_get_normalized_flr_data(ignore_sla = false)
    result_data = []
    cep = @obj.cos_end_point
    if !cep.segment_end_point.is_monitored
      SW_ERR "The endpoint is not monitored"
    else  
      result_data = []    
      sla_id = @obj.sla_id
      test_class_id = @obj.test_instance_class_id
      test_type = @obj.test_type

      if sla_id.empty? || test_class_id.empty?
        SW_ERR "sla_id(#{sla_id}) and/or test_instance_class_id(#{test_class_id}) are empty"
      else
        if (test_type == 'Ethernet Frame Delay Test')
          class_name = "com.brixnet.swv.ethframedelaytest"
          # Egress, Ingress
          attributes = {"ValidFramesReceived" => "SUM(ValidFramesReceived)", "FramesTransmitted" => "SUM(FramesTransmitted)"}
          mapping = {:egress => "ValidFramesReceived", :ingress => "FramesTransmitted"}
        elsif (test_type == 'Ping Active Test')
          class_name = "com.brixnet.pingactivetest"
          #TODO this 59 is hardcoded because its not available in test results
          # we should call get_test_parameters ("Number of Pings")
          # Egress, Ingress
          attributes = {"NumberofNoResponses" => "SUM(59 - NumberofNoResponses)", "NumberOfRecords" => "59 * COUNT(*)"}
          mapping = {:egress => "NumberofNoResponses", :ingress => "NumberOfRecords"}
        elsif (test_type == 'Ethernet Loopback Test')
          class_name = "com.brixnet.swv.ethloopbacktest"
          # Egress, Ingress
          attributes = {"ValidFramesReceived" => "SUM(ValidFramesReceived)", "FramesTransmitted" => "SUM(FramesTransmitted)"}
          mapping = {:egress => "ValidFramesReceived", :ingress => "FramesTransmitted"}            
        else
          SW_ERR "ERROR: Unknown Test Type: #{test_type}\n"
          class_name = ""
          attributes = {}
          mapping = {}
        end
        time_name = timestamp_name
        where=[["`test_instance_class_id`='#{test_class_id}'"] ]
        group_by = nil
        order_by = [timestamp_name]
        time_periods_nanosecs =  [[(adjust_start_time*NSEC_IN_SEC) + 999, (@end_time.to_i*NSEC_IN_SEC) + 999]]  # period [begin + 999ns, end + 999ns] time  the bounds are inclusive.
        time_increment_nanosecs = @collection_period_secs*NSEC_IN_SEC
        nms_id = @obj.get_monitoring_manager.id
        bx_data = DataArchiveIf.query_and_aggregate(class_name, time_name, where, group_by, order_by, attributes, time_periods_nanosecs, time_increment_nanosecs)
        bx_data = [] if bx_data.nil?
        bx_data.each do |bxd|
          total_ingress = bxd[mapping[:ingress]].to_i
          total_egress = bxd[mapping[:egress]].to_i
          result_data << {:timeCaptured => (bxd[time_name].to_i/NSEC_IN_SEC), :deltaIngress => total_ingress, :deltaEgress => total_egress}
        end
      end
    end
    slas = stats_get_flr_slas(ignore_sla)
    refs, ref_ids = stats_get_reference_data(:frame_loss_ratio)
    return {cep.cos_name => {:cos_id => slas[cep.cos_name][:cos_id], :sla => slas[cep.cos_name], :ref => refs[cep.cos_name], :data => {"Total" => result_data}}}
  end
  
  
  def get_test_class_attributes(delay_type, test_type)
    class_name = nil
    attribute_mappings = nil
    attributes   = nil
    if (test_type == 'Ethernet Frame Delay Test')
      class_name = "com.brixnet.swv.ethframedelaytest"
    elsif (test_type == 'Ping Active Test')
      class_name = "com.brixnet.pingactivetest"
    elsif (test_type == 'Ethernet Loopback Test')
      class_name = "com.brixnet.swv.ethloopbacktest"
    else
      SW_ERR "ERROR: Unknown Test Type: #{test_type}\n"
      class_name = ""
    end

    if !delay_type.is_a?(Array)
      delay_type = [delay_type]
    end

    attribute_mappings = {}
    attributes = {}
    delay_type.each do |dt|
      case dt
      when :delay
        if (test_type == 'Ethernet Frame Delay Test')
          attribute_mappings["AverageNetworkRoundTripLatency"] = :delay
          attributes["AverageNetworkRoundTripLatency"] = "AVG(AverageNetworkRoundTripLatency/2)"
        elsif (test_type == 'Ping Active Test')
          attribute_mappings["AverageRoundTripLatency"] = :delay
          attributes["AverageRoundTripLatency"] = "AVG(AverageRoundTripLatency/2)"
        elsif (test_type == 'Ethernet Loopback Test')
          attribute_mappings["AverageRoundTripLatency"] = :delay
          attributes["AverageRoundTripLatency"] = "AVG(AverageRoundTripLatency/2)"
        else
          SW_ERR "ERROR: Unknown Test Type: #{test_type}\n"
          [{}, {}]
        end
      when :delay_variation
        if (test_type == 'Ethernet Frame Delay Test')
          attribute_mappings["AverageNetworkRoundTripLatencyVariation"] = :delay_variation
          attributes["AverageNetworkRoundTripLatencyVariation"] = "AVG(AverageNetworkRoundTripLatencyVariation/2)"
        elsif (test_type == 'Ping Active Test')
          attribute_mappings["AverageJitter"] = :delay_variation
          attributes["AverageJitter"] = "AVG(AverageJitter/2)"
        elsif (test_type == 'Ethernet Loopback Test')
          attribute_mappings["AverageRoundTripLatencyVariation"] = :delay_variation
          attributes["AverageRoundTripLatencyVariation"] = "AVG(AverageRoundTripLatencyVariation/2)"
        else
          SW_ERR "ERROR: Unknown Test Type: #{test_type}\n"
          [{}, {}]
        end
      when :max_delay
        if (test_type == 'Ethernet Frame Delay Test')
          attribute_mappings["MaximumNetworkRoundTripLatency"] = :max_delay
          attributes["MaximumNetworkRoundTripLatency"] = "MAX(MaximumNetworkRoundTripLatency/2)"
        elsif (test_type == 'Ping Active Test')
          attribute_mappings["MaximumRoundTripLatency"] = :max_delay
          attributes["MaximumRoundTripLatency"] = "MAX(MaximumRoundTripLatency/2)"
        elsif (test_type == 'Ethernet Loopback Test')
          attribute_mappings["MaximumRoundTripLatency"] = :max_delay
          attributes["MaximumRoundTripLatency"] = "MAX(MaximumRoundTripLatency/2)"
        else
          SW_ERR "ERROR: Unknown Test Type: #{test_type}\n"
          [{}, {}]
        end
      when :max_delay_variation
        if (test_type == 'Ethernet Frame Delay Test')
          attribute_mappings["MaximumNetworkRoundTripLatencyVariation"] = :max_delay_variation
          attributes["MaximumNetworkRoundTripLatencyVariation"] = "MAX(MaximumNetworkRoundTripLatencyVariation/2)"
        elsif (test_type == 'Ping Active Test')
          # Not supported
        elsif (test_type == 'Ethernet Loopback Test')
          attribute_mappings["MaximumRoundTripLatencyVariation"] = :max_delay_variation
          attributes["MaximumRoundTripLatencyVariation"] = "MAX(MaximumRoundTripLatencyVariation/2)"
        else
          SW_ERR "ERROR: Unknown Test Type: #{test_type}\n"
          [{}, {}]
        end
      when :min_delay
        if (test_type == 'Ethernet Frame Delay Test')
          attribute_mappings["MinimumNetworkRoundTripLatency"] = :min_delay
          attributes["MinimumNetworkRoundTripLatency"] = "MIN(MinimumNetworkRoundTripLatency/2)"
        elsif (test_type == 'Ping Active Test')
          attribute_mappings["MinimumRoundTripLatency"] = :min_delay
          attributes["MinimumRoundTripLatency"] = "MIN(MinimumRoundTripLatency/2)"
        elsif (test_type == 'Ethernet Loopback Test')
          attribute_mappings["MinimumRoundTripLatency"] = :min_delay
          attributes["MinimumRoundTripLatency"] = "MIN(MinimumRoundTripLatency/2)"
        else
          SW_ERR "ERROR: Unknown Test Type: #{test_type}\n"
          [{}, {}]
        end
      when :min_delay_variation
        if (test_type == 'Ethernet Frame Delay Test')
          attribute_mappings["MinimumNetworkRoundTripLatencyVariation"] = :min_delay_variation
          attributes["MinimumNetworkRoundTripLatencyVariation"] = "MIN(MinimumNetworkRoundTripLatencyVariation/2)"
        elsif (test_type == 'Ping Active Test')
          # Not supported
        elsif (test_type == 'Ethernet Loopback Test')
          attribute_mappings["MinimumRoundTripLatencyVariation"] = :min_delay_variation
          attributes["MinimumRoundTripLatencyVariation"] = "MIN(MinimumRoundTripLatencyVariation/2)"
        else
          SW_ERR "ERROR: Unknown Test Type: #{test_type}\n"
          [{}, {}]
        end
      else
        SW_ERR "Invalid delay type #{dt}"
      end
    end
    return     class_name, attribute_mappings, attributes
  end


  def compute_delay(delay_type)
    result = 0
    cep = @obj.cos_end_point
    if !cep.segment_end_point.is_monitored
      SW_ERR "The endpoint is not monitored"
    else
      test_type = @obj.test_type
      class_name, attribute_mappings, attributes =  get_test_class_attributes(delay_type, test_type)
      sla_id = @obj.sla_id
      test_class_id = @obj.test_instance_class_id
      if sla_id.empty? || test_class_id.empty?
        SW_ERR "sla_id(#{sla_id}) and/or test_instance_class_id(#{test_class_id}) are empty"
      else
        time_name = timestamp_name
        where=[["`test_instance_class_id`='#{test_class_id}'", "`Result`='SUCCESSFUL'"]]
        group_by = nil
        nms_id = @obj.get_monitoring_manager.id
        bx_data = DataArchiveIf.query_and_summarize(class_name, time_name, where, group_by, attributes, @time_periods_nsecs)
        bx_data = [] if bx_data.nil?
        puts "Error NO data from archive" if bx_data.nil?
        puts "Error more than one row returned from DataArchive" if bx_data.size > 1
        # convert names from Brix tests to "normalized" names
        # convert the two way test result to a one way result by dividing by 2
        bx_data.each do |bxd|
          attribute_mappings.each do |attribute, hash_key|
            result = (bxd[attribute].to_f * @extrapolation_factor) if bxd[attribute]
          end
        end
      end
    end
    return result
  end
  
  def stats_get_delay_data(delay_type, test_name = nil)
    data = {}
    result_data = []    
    cep = @obj.cos_end_point
    if !cep.segment_end_point.is_monitored
      SW_ERR "The endpoint is not monitored"
    else
      test_type = @obj.test_type
     
      class_name, attribute_mappings, attributes =  get_test_class_attributes(delay_type, test_type)
      result_data = []
      sla_id = @obj.sla_id
      test_class_id = @obj.test_instance_class_id
      
      if sla_id.empty? || test_class_id.empty?
        SW_ERR "sla_id(#{sla_id}) and/or test_instance_class_id(#{test_class_id}) are empty"
      else
        time_name = timestamp_name
        where=[["`test_instance_class_id`='#{test_class_id}'", "`Result`='SUCCESSFUL'"]]
        group_by = nil
        order_by = [timestamp_name]
        time_periods_nanosecs =  [[(adjust_start_time*NSEC_IN_SEC) + 999, (@end_time.to_i*NSEC_IN_SEC) + 999]]  # period [begin + 999ns, end + 999ns] time  the bounds are inclusive.
        time_increment_nanosecs = @collection_period_secs*NSEC_IN_SEC
        nms_id = @obj.get_monitoring_manager.id
        bx_data = DataArchiveIf.query_and_aggregate(class_name, time_name, where, group_by, order_by, attributes, time_periods_nanosecs, time_increment_nanosecs)
        bx_data = [] if bx_data.nil?

        bx_data.each do |bxd|
          attrs = {}
          attribute_mappings.each do |attribute, hash_key|
            attrs[hash_key] = bxd[attribute].to_f if bxd[attribute]
          end
          result_data << ({"neTestIndex" => "", "timeCaptured" => to_sec(bxd[time_name])}.merge(attrs))
        end
      end
      data[cep.cos_name] = result_data      
    end
    return data
  end

  def compute_overall_flr_value
    overall_flr = (0.0/0.0) # This is NaN
    # If there are no time periods then it's been available all the time
    if @time_periods_nsecs.empty?
      overall_flr = 0.0
    else
      sla_id = @obj.sla_id
      test_class_id = @obj.test_instance_class_id
      test_type = @obj.test_type
      if sla_id.empty? || test_class_id.empty?
        SW_ERR "sla_id(#{sla_id}) and/or test_instance_class_id(#{test_class_id}) are empty"
      else
        if (test_type == 'Ethernet Frame Delay Test') 
          class_name = "com.brixnet.swv.ethframedelaytest"
          # Egress, Ingress
          attributes = {"ValidFramesReceived" => "SUM(ValidFramesReceived)", "FramesTransmitted" => "SUM(FramesTransmitted)"}
          mapping = {:egress => "ValidFramesReceived", :ingress => "FramesTransmitted"}
        elsif (test_type == 'Ping Active Test')
          class_name = "com.brixnet.pingactivetest"
          #TODO this 59 is hardcoded because its not available in test results
          # we should call get_test_parameters ("Number of Pings")
          # Egress, Ingress
          attributes = {"NumberofNoResponses" => "SUM(59 - NumberofNoResponses)", "NumberOfRecords" => "59 * COUNT(*)"} 
          mapping = {:egress => "NumberofNoResponses", :ingress => "NumberOfRecords"}      
        elsif (test_type == 'Ethernet Loopback Test')
          class_name = "com.brixnet.swv.ethloopbacktest"
          # Egress, Ingress
          attributes = {"ValidFramesReceived" => "SUM(ValidFramesReceived)", "FramesTransmitted" => "SUM(FramesTransmitted)"}
          mapping = {:egress => "ValidFramesReceived", :ingress => "FramesTransmitted"}
        else
          SW_ERR "ERROR: Unknown Test Type: #{test_type}\n"
          class_name = ""
          attributes = {}
          mapping = {}
        end
  
        time_name = timestamp_name
        where=[["`test_instance_class_id`='#{test_class_id}'"]]
        group_by = nil
    
        nms_id = @obj.get_monitoring_manager.id
        data = DataArchiveIf.query_and_summarize( class_name, time_name, where, group_by, attributes, @time_periods_nsecs)
        data = [] if data.nil?
    
        ti = 0
        te = 0
        data.each do |idata|
          Rails.logger.debug "Overall flr raw #{idata[mapping[:ingress]]}  #{idata[mapping[:egress]]}"
          ti = ti + idata[mapping[:ingress]].to_i
          te = te + idata[mapping[:egress]].to_i
        end

        if !data.empty?
          # if there was data and there was no ingress return 0 rather than NaN
          overall_flr = ti == 0 ? 0.0 : ((ti - te)/ti.to_f) * 100 *@extrapolation_factor
        end
      end
    end
    return overall_flr
  end    
  
  
  def compute_overall_flr
    overall_flr = compute_overall_flr_value
    cep = @obj.cos_end_point
    return {cep.cos_name => {:cos_id =>cep.id, :aggregate_flr => overall_flr}}
  end

  def bx_availability_kludge
    # This is temporary until we can get notified about NO_RESPONSE via Brix
    cep = @obj.cos_end_point
    if !cep.segment_end_point.is_monitored || cep.alarm_histories.empty?
      SW_ERR "The endpoint is not monitored #{cep.segment_end_point.is_monitored} or there are no alarm history #{cep.alarm_histories.empty?}"
    else
      sla_id = @obj.sla_id
      test_class_id = @obj.test_instance_class_id
      if sla_id.empty? || test_class_id.empty?
        SW_ERR "sla_id(#{sla_id}) and/or test_instance_class_id(#{test_class_id}) are empty"
      else         
        test_type = @obj.test_type
        if (test_type == 'Ethernet Frame Delay Test')
          class_name = "com.brixnet.swv.ethframedelaytest"
        elsif (test_type == 'Ping Active Test')
          class_name = "com.brixnet.pingactivetest"
        elsif (test_type == 'Ethernet Loopback Test')
          class_name = "com.brixnet.swv.ethloopbacktest"
        else
          SW_ERR "ERROR: Unknown Test Type: #{test_type}\n"
          class_name = ""
        end
        
        attributes = ["Result"]
        time_name = timestamp_name
        where=[["`test_instance_class_id`='#{test_class_id}'"]]
        group_by = nil
        order_by = [timestamp_name]
        time_periods_nanosecs =  [[(@start_time.to_i*NSEC_IN_SEC) + 999, (@end_time.to_i*NSEC_IN_SEC) + 999]]  # period [begin + 999ns, end + 999ns] time  the bounds are inclusive.
        time_increment_nanosecs = 0
        nms_id = @obj.get_monitoring_manager.id
        bx_data = DataArchiveIf.query_dont_aggregate(class_name, time_name, where, group_by, order_by, attributes, time_periods_nanosecs)
        bx_data = [] if bx_data.nil?                
        if !bx_data.empty?
          # Set the initial availability to the last recorded alarm before the first record
          timestamp = bx_data.first[time_name].to_i - BrixAlarmHistory.alarm_time_correction
          last_state_index = cep.alarm_history.event_records.bsearch_lower_boundary {|x| x.time_of_event.to_i <=> timestamp} - 1
          if last_state_index >= 0
            current_availability = cep.alarm_history.event_records[last_state_index].state ==  AlarmSeverity::OK ? 'SUCCESSFUL' : 'NO_RESPONSE'
          else 
            # Inserting at the start so set the current availability to the opposite of the first one so the first one
            # will always get added
            current_availability = bx_data.first["Result"] != 'SUCCESSFUL' ? 'SUCCESSFUL' : 'NO_RESPONSE'
          end
           
          bx_data.each do |bxd|
            timestamp = (bxd[time_name].to_i/(1000*1000) - BrixAlarmHistory.alarm_time_correction).to_s
            state = bxd["Result"]
            if state == 'SUCCESSFUL' && current_availability != 'SUCCESSFUL'
              if cep.alarm_histories.first.alarm_records.find_by_time_of_event(timestamp) == nil
                cep.alarm_histories.first.alarm_records << AlarmRecord.create(:time_of_event => timestamp, :state => AlarmSeverity::OK, :details => "cleared", :original_state => "cleared")
              end
            elsif state != 'SUCCESSFUL' && current_availability == 'SUCCESSFUL'
              if cep.alarm_histories.first.alarm_records.find_by_time_of_event(timestamp) == nil
                cep.alarm_histories.first.alarm_records << AlarmRecord.create(:time_of_event => timestamp, :state => AlarmSeverity::UNAVAILABLE, :details => "unavailable", :original_state => "unavailable")
              end
            end 
            current_availability = state              
          end
        end
      end
    end
  end
    
  def collection_period
    return 59.seconds
  end
  
  
  def to_native the_time
    # returns time in nsecs
    the_time.to_i * NSEC_IN_SEC
  end

  def to_sec the_time
    # returns time in nsecs
    (the_time.to_i / NSEC_IN_SEC).to_i
  end

  def get_time_periods_native
    # returns time in nsecs
    return @time_periods_nsecs
  end

  def get_table_and_selected_columns
    time_name = nil
    table_name = nil
    column_info = nil
    test_type = @obj.test_type
    if (test_type == 'Ethernet Frame Delay Test')
      table_name= ETH_FRAME_DELAY_TEST_TABLE
      column_info= ETH_FRAME_DELAY_TEST_COLUMNS
    elsif (test_type == 'Ping Active Test')
      table_name= PING_TEST_TABLE
      column_info= PING_TEST_COLUMNS
    elsif (test_type == 'Ethernet Loopback Test')
      table_name= ETH_LOOPBACK_TEST_TABLE
      column_info= ETH_LOOPBACK_TEST_COLUMNS
    else
      SW_ERR "ERROR: Unknown Test Type: #{test_type}\n"
    end
    time_name = timestamp_name
    return time_name, table_name, column_info
  end

  def get_where
    where = [ [ "`test_instance_class_id`='#{@obj.test_instance_class_id}'" ] ]
    return where
  end
  def get_ref_where
    return nil
  end
  def get_all_ref_where
    return nil
  end

end    
