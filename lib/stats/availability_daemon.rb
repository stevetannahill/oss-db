
# set up logging
MAX_LOG_SIZE = 10*1024*1024
class FormatLogger < Logger
  def format_message(severity, timestamp, progname, msg)
    "#{timestamp.to_s} #{severity} #{msg}\n"
  end
end

# Main begins
logger = FormatLogger.new("#{Rails.root}/log/#{File.basename(__FILE__, '.*')}.log", 1, MAX_LOG_SIZE)
Rails.logger = logger # Give logger access to all ruby objects
Rails.logger.level = (Rails.env == "development") ? Logger::Severity::DEBUG : Logger::Severity::INFO

Rails.logger.info "#{File.basename(__FILE__, '.*')} starting..."

keep_going = true
while (keep_going)
  Rails.logger.info "starting availability poll"
  
  # Ensure the connection to the database is up
  ActiveRecord::Base.verify_active_connections!  
  # Find latest time used to compute exceptions
  compute_start_time = Time.now
  last_time = AvailabilityTime.find(:first)
  if last_time.nil?
    start_time=SlaExceptionGenerator::FIRST_TIME
    last_time = AvailabilityTime.new
  else
    start_time=Time.at(last_time.time)
  end
  
  max_timestamp = 0
  if SpirentCosTestVector.count > 0
    # TODO remove - will need to go through all CTV and get the archive id's and then
    # loop over each data archive to get the date
    class_name = 'ethframedelaytest'
    time_name  = 'time_start'
    columns = ['grid_circuit_id', 'time_start', 'available']
    where = nil
    DataArchiveIf.for_each_availability_change(class_name, time_name, columns, where, start_time.to_i) do |record| 
      state = record["available"] == 1 ? "cleared" : "unavailable"
      timestamp = record["time_start"]*1000
      obj = CosTestVector.find_by_grid_circuit_id(record['grid_circuit_id'])
      if obj
        max_timestamp = timestamp if timestamp > max_timestamp
        event_filter = "#{obj.circuit_id}-Availability"
        details = ""
        tmp = {:event_filter => event_filter, :timestamp => timestamp, :state => state, :details => ""}
        AvailabilityHistory.process_event(tmp)
      end
    end
  end
  if max_timestamp > 0
    # On the next poll start from the latest alarm time. This works because the availability for now is always at the same 5 minute
    # interval for all CosTestVectors so if only part of the CTVs have been processed on the next poll we will get the remainder.
    # The down side is that for each poll (if there are no changes) it will get the same results hence un-needed processing is done.
    last_time.time = (max_timestamp/1000) - 1.hour
    last_time.save
  end
  Rails.logger.info "Finished availability poll - took #{Time.now-compute_start_time} seconds" 
  
  sleep(5.minutes) 
end

