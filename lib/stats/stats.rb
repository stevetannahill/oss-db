
# Class that holds stats data for SAM stats for On Net OVCs and also BrixWorx stats for monitored endpoints  
class Stats       
  attr_accessor :obj
  attr_accessor :pad_results
  
  UNAVAILABLE_THRESHOLD = AlarmSeverity::UNAVAILABLE

  METRIC_SYMBOLS = [
    :availability,
    :frame_loss,
    :frame_loss_ratio,
    :delay,
    :delay_variation,
    :min_delay,
    :min_delay_variation,
    :max_delay,
    :max_delay_variation,
    :octets_count,
    :frames_sent,
    :frames_received,
    :sample_count
  ]

  METRIC_NAMES = {
    :availability => 'available',
    :frame_loss => 'framesLost',
    :frame_loss_ratio => 'frameLossRatio',
    :delay => 'averageLatency',
    :delay_variation => 'averageDelayVariation' ,
    :min_delay => 'minimumLatency',
    :min_delay_variation => 'minimumDelayVariation',
    :max_delay => 'maximumLatency',
    :max_delay_variation => 'maximumDelayVariation' ,
    :octets_count => 'octets_count',
    :frames_sent => 'framesSent',
    :frames_received => 'framesReceived',
    :sample_count => 'sampleCount',
  }

  METRIC_UNITS = {
    :availability => '%',
    :frame_loss => 'frames',
    :frame_loss_ratio => '%',
    :delay => 'usecs',
    :delay_variation => 'usecs' ,
    :min_delay => 'usecs',
    :min_delay_variation => 'usecs',
    :max_delay => 'usecs',
    :max_delay_variation => 'usecs' ,
    :octets_count => 'octets',
    :frames_sent => 'frames',
    :frames_received => 'frames',
    :sample_count => 'samples',
  }

  NORMALIZED_TIME_NAME = 'timeCaptured'

  def initialize(obj, start_time, end_time, current_time, adjust_end_time)
    @pad_results = false
    @obj = obj
    @start_time = Time.at(start_time)
    @current_time = Time.at(current_time)
    # If the end time is in the future then adjust the end time to Time.now - 1 collection period so we will have data.
    # however for availability always use the real end time - See stats_availability_results for more details
    @avail_end_time = Time.at(end_time)
    if adjust_end_time and end_time.to_i > @current_time.to_i
      # Adjust the end time to current_time - 1 collection period so we will have data.
      @end_time = Time.at(current_time.to_i - (current_time.to_i % collection_period.to_i))
    else
      @end_time = Time.at(end_time)      
    end
    # Exceptions for current month are computed until end of current month.
    # Therefore compute extrapolation factor if dealing with an end time in the future.
    if @end_time > @current_time
      total_time = @end_time - @start_time
      future_time = @end_time - @current_time
      @extrapolation_factor = (total_time.to_f - future_time.to_f) / total_time.to_f
    else
      @extrapolation_factor = 1.0
    end
    # default collection_period is based on 80 data points with a min of collection_period
    stats_set_collection_interval_based_on_points
    avail_data = stats_availability_results(UNAVAILABLE_THRESHOLD)[:data]
    @time_periods_secs = get_available_time_periods(avail_data)
  end
  
  
  def stats_flr_summary(cos_names = nil)
    overall_flrs = stats_calculate_overall_flr
    slas = stats_get_flr_slas
    
    if cos_names == nil
      cos_names = overall_flrs.keys
    else
      # if a single value is passed make it an array and then convert any Fixnums from the id to the cos_name
      cos_names = [cos_names] if !cos_names.is_a?(Array)
      cos_names = cos_names.collect {|cos_name| cos_name.is_a?(Fixnum) ? slas.find {|cos, sla_value| sla_value[:cos_id] == cos_name}.first : cos_name}.compact  
    end
    
    return_data = {}
    cos_names.each do |cos_name|
    
      overall_flr = overall_flrs[cos_name][:aggregate_flr]
      if overall_flr.nan?
        overall_flr = "No data available"
      else
        overall_flr = "#{'%.5f' % overall_flr}"
      end      
      return_data[cos_name] = {:cos_id => slas[cos_name][:cos_id],
                               :sla => slas[cos_name],
                               :aggregate_flr => overall_flr}
    end
    
    return return_data
  end
    
  
  # returns {"cos_name" => {:cos_id => xx, :sla => {:sla_threshold => xx, :sla_warning_threshold => yy}, :aggregate_flr => FFF, :data => {"name" => [{:x => XX, :y => YY}...]}}}
  def stats_flr_results(normalized_data, cos_names = nil)  
    if cos_names == nil
      cos_names = normalized_data.keys
    else
      # if a single value is passed make it an array and then convert any Fixnums from the id to the cos_name
      cos_names = [cos_names] if !cos_names.is_a?(Array)
      cos_names = cos_names.collect {|cos_name| cos_name.is_a?(Fixnum) ? slas.find {|cos, sla_value| sla_value[:cos_id] == cos_name}.first : cos_name}.compact  
    end
    
    # only return data for the slas which have data
    cos_names = cos_names.collect {|cos_name| cos_name if normalized_data[cos_name] != nil}.compact
    
    # Get the overall FLR for each COS
    overall_flrs = stats_calculate_overall_flr
    
    return_data = {}
    
    cos_names.each do |cos_name|
      y_data = {}
      normalized_data[cos_name][:data].each do |name, data|
        # Do we have frame data ie :deltaIngress/Egress or actual frame loss ratio
        frame_data = data.any? {|record| record[:deltaIngress]} 
        
        y_data[name] = []
        data.each do |record|
          if frame_data
            ti = record[:deltaIngress]
            te = record[:deltaEgress]
            delta = ti-te
            #Rails.logger.debug "#{cos_name} TOP flr points ti: #{ti} te: #{te} delta: #{delta} #{Time.at(record[:timeCaptured])} "                 
            y_value = 0.0

            if ti > 0 # If there is no ingress there is no frame loss
              y_value = ((ti - te).to_f/ti.to_f) * 100
              if y_value.abs < 5e-4
                # if Y is very small set it to zero
                y_value = 0.0
              end
            end
          else
            y_value = record[:frame_loss_ratio].to_f
          end
          timeCaptured = record[:timeCaptured].to_i
          y_data[name] << {:x => timeCaptured, :y => y_value}
          #Rails.logger.debug "#{cos_name}-#{name} flr points y:#{y_value} ActualFLR:#{((ti - te).to_f/ti.to_f) * 100} ActualFlr before adj:#{(delta.to_f/ti.to_f) * 100} #{Time.at(timeCaptured)} Delta:#{delta} ti:#{ti} te:#{te}"
        end
      end

      overall_flr = overall_flrs[cos_name][:aggregate_flr]
      if overall_flr.nan?
        overall_flr = "No data available"
      else
        overall_flr = "#{'%.5f' % overall_flr}"
      end      
      return_data[cos_name] = {:cos_id => normalized_data[cos_name][:cos_id],
                               :sla => normalized_data[cos_name][:sla],
                               :ref => normalized_data[cos_name][:ref],
                               :aggregate_flr => overall_flr,
                               :data => y_data}
    end
    if @pad_results
      z = []
      return_data.each {|cos, cos_v| cos_v[:data].each {|test, test_v| z << test_v}}
      Stats.pad_results(z)
    end
    return return_data
  end
  
  # Gets Frame loss data in a normalized format - The returned data can be passed to
  # stats_flr_results
  def stats_get_normalized_flr_data(ignore_sla = false)
    SW_ERR "Not Implemented"
  end
  
  def unavailable_time_msecs(threshold = UNAVAILABLE_THRESHOLD)
    start_time = @start_time.to_i
    end_time = @end_time.to_i
    avail_start_ms = start_time * 1000
    avail_end_ms = end_time * 1000
    history = @obj.alarm_history.subset(avail_start_ms, avail_end_ms)
    history += @obj.mtc_history.subset(avail_start_ms, avail_end_ms)
    avail_start_ms = history.alarm_records.first.time_of_event.to_i if avail_start_ms < history.alarm_records.first.time_of_event.to_i
    avail_end_ms = (Time.now.to_f*1000).to_i if avail_end_ms > (Time.now.to_f*1000).to_i #history.alarm_records.last.time_of_event.to_i
    return history.unavailable_time_msecs(avail_start_ms, avail_end_ms, threshold)
  end


  def stats_availability_summary(threshold = UNAVAILABLE_THRESHOLD, history = nil)
    start_time = @start_time.to_i
    end_time = @avail_end_time.to_i
    avail_start_ms = start_time * 1000
    avail_end_ms = end_time * 1000
    
    if history == nil
      history = @obj.alarm_history.subset(avail_start_ms, avail_end_ms)
      history += @obj.mtc_history.subset(avail_start_ms, avail_end_ms)
    end
    
    availability = history.availability_percent(avail_start_ms, avail_end_ms, threshold)
    unavailable_time_msecs = history.unavailable_time_msecs(avail_start_ms, avail_end_ms, threshold)
    sla_monthly = stats_get_availability_slas
    
    return {:aggregate_avail => availability, :from_time => Time.at(avail_start_ms/1000), :to_time => Time.at(avail_end_ms/1000) ,:sla_monthly => sla_monthly, :unavailable_time_msecs => unavailable_time_msecs }
  end
    

  # returns
  # {:from_time=>Time,
  #  :to_time=>Time,
  #  :data=>[{:x=>XX, :y=>1|0}, ....], # 1=Available, 0=Unavailable
  #  :sla_monthly=>XXXX,
  #  :aggregate_avail=>ZZZ} 
  def stats_availability_results(threshold = UNAVAILABLE_THRESHOLD, history = nil)
    start_time = @start_time.to_i
    end_time = @avail_end_time.to_i
    avail_start_ms = start_time * 1000
    avail_end_ms = end_time * 1000
    
    if history == nil
      history = @obj.alarm_history.subset(avail_start_ms, avail_end_ms)
      history += @obj.mtc_history.subset(avail_start_ms, avail_end_ms)
    end
        
    points = []
    availability = "No Availability available"
    unavailable_time_msecs = 0
    if history.alarm_records.size != 0
      avail_start_ms = history.alarm_records.first.time_of_event if avail_start_ms < history.alarm_records.first.time_of_event
      if avail_end_ms > (Time.now.to_f*1000).to_i
        # The given end time is in the future so ideally we make it Time.now however SAM and the CDB server may have different
        # times (we have seen the SAM server being 9 minutes ahead of CDB server). So if the last alarm record is > Time.now then
        # set the end time to the alarm record otherwise set it to Time.now
        if (history.alarm_records.last.time_of_event > (Time.now.to_f*1000).to_i) && (history.alarm_records.last.time_of_event <= avail_end_ms)          
          avail_end_ms = history.alarm_records.last.time_of_event + 1000 # Make the end 1 second after the event
        else
          avail_end_ms = (Time.now.to_f*1000).to_i 
        end
      end
            
      availability = history.availability_percent(avail_start_ms, avail_end_ms, threshold)
      unavailable_time_msecs = history.unavailable_time_msecs(avail_start_ms, avail_end_ms, threshold)
      previous_event_time = history.alarm_records.first.time_of_event.to_i/1000
      previous_availability = history.alarm_records.first.state >= threshold ? 0 : 1
      points << {:x => previous_event_time, :y => previous_availability} if previous_event_time > start_time && previous_event_time < end_time
      
      history.alarm_records[1..-1].each do |ar|
        event_time = ar.time_of_event.to_i/1000
        current_availability = ar.state >= threshold ? 0 : 1
        # If the event is after the start time and the previous is before the start time
        # then add a point at start time with the state of the previous point
        # Do a similar thing but for the end time               
        if event_time >= start_time && previous_event_time < start_time
          points << {:x => start_time, :y => previous_availability}
          points << {:x => event_time, :y => current_availability} if previous_availability != current_availability
        elsif event_time >= end_time && previous_event_time < end_time                    
          points << {:x => end_time, :y => previous_availability}          
        elsif event_time >= start_time && event_time <= end_time                    
          points << {:x => event_time, :y => current_availability} if previous_availability != current_availability          
        end
        previous_event_time = event_time
        previous_availability = current_availability
      end

      # If there are no points then use the last known alarm
      points << {:x => start_time, :y => history.alarm_records.last.state >= threshold ? 0 : 1} if points.size == 0
      
      # If the last point is before the end time then set the point to now or to end_time      
      if points.last[:x] < end_time
        points << {:x => (avail_end_ms.to_f/1000).to_i, :y => points.last[:y]}        
      end
    end
    sla_monthly = stats_get_availability_slas
    return {:data => points, :aggregate_avail => availability, :from_time => Time.at(avail_start_ms/1000), :to_time => Time.at(avail_end_ms/1000) ,:sla_monthly => sla_monthly, :unavailable_time_msecs => unavailable_time_msecs }
  end
 
 
 
  def stats_delay_summary(delay_type, ignore_sla = false)
    sla_attribute = stats_get_delay_sla_method(delay_type)
    attribute = get_sla_name(delay_type)
    slas, sla_ids = stats_get_slas(sla_attribute)
    
    agg = case attribute
    when :delay
      {:aggregate => (compute_delay(:delay)).round(4)}
    when :delay_variation
      {:aggregate => compute_delay(:delay_variation).round(4)}
    else
      {}
    end     
    result = {:sla => slas, :sla_id => sla_ids} 
    result.merge!(agg)
    
    return result
  end
    
 
  # data is the returned data from stats_get_delay_data
  # delay_type is :delay or :delay_variation
  # The return value is 
  # {:data => {
  #             "test_one"  => {"real-time" => {:x,:y}, {:x,:y}}
  #                            {"premium"   => {:x,:y}, {:x,:y}}
  #             "test_two"  => {"real-time" => {:x,:y}, {:x,:y}}
  #                            {"premium"   => {:x,:y}, {:x,:y}}
  #            }
  #  :sla => {"real_time" => "xxx", "premium" => "yyy"}
  #  :sla_id => {id => "xxx", id => "yyy"}
  # }
  # ie xxx[:data]["test_name"]["real-time"]
  #    xxx[:sla]["real-time"][:sla][:sla_threhold]
  #                                [:sla_warning_threshold]
  #    xxx[:sla][cod_id][:sla_id][:sla_threhold]
  #                              [:sla_warning_threshold]
  def stats_delay_results(data, delay_type, ignore_sla = false)
    sla_attribute = stats_get_delay_sla_method(delay_type)
    attribute = get_sla_name(delay_type)
    data = stats_process_delay_data(data, attribute, sla_attribute, ignore_sla)
    if @pad_results
      z = []
      data[:data].each {|test,test_v| test_v.each {|cos,cos_v| z << cos_v}}
      Stats.pad_results(z)
    end
    return data
  end
  
  # delay_type is :delay or :delay_variation  
  # returns
  # {"cos_name" => [{"neTestIndex"=>"test name", "timeCaptured"=>"TTTT", "averageRoundTripTime | roundTripJitter"=>"0"}....]....}
  def stats_get_delay_data(delay_type, test_name = nil)
    SW_ERR "Not implemented"
  end  

  # delay_type is :delay or :delay_variation
  # returns
  # {"delay"=> ["test1:, "test2"...."],
  #  "delay_variation"=> ["test1:, "test2"...."]} 
  def stats_get_delay_tests   
    SW_ERR "Not implemented"
  end

  # counter_types is an array of the following :dropped_frames, :frame_counts, :octet_counts, :dropped_octets 
  # returns raw data to be passed to stats_frame_results
  # return data is [[DisplayedName][:klass]
  #                                [:id]
  #                                [:data][cos_name][counter_type => [{:timeCaptured => xx, :totalIngress => ccc, :totalEgress => kk}.]..]
  #               ]
  def stats_get_frame_count_data(counter_types)   
    SW_ERR "Not implemented"
  end

  # The following 2 procs are used when processing the frame counts to give either a delta
  # of the counts or to get the MBytes/Sec
  # Once we have fully switched to the archive this should move to stats.rb
  # This is here currently so it's easy to switch between SAM and the archive
  @@frame_deltas_proc = Proc.new do |previous_result, current_result|
    ti = current_result[:deltaIngress] 
    te = current_result[:deltaEgress]
    [ti,te]
    # don't do a return as this is Proc and a return will exit both this
    # and the caller
  end
  
  def frame_deltas
    return @@frame_deltas_proc
  end
  
  @@octets_to_mbps_proc = Proc.new do |previous_result, current_result|
    if previous_result != nil
      ti = current_result[:deltaIngress] * 8
      te = current_result[:deltaEgress] * 8
      time_diff = current_result[:timeCaptured] - previous_result[:timeCaptured]
      ti = (ti/time_diff)/1e6
      te = (te/time_diff)/1e6
    else
      # no previous data so can't calculate the time difference
      ti = nil
      te = nil
    end
    [ti,te]
    # don't do a return as this is Proc and a return will exit both this
    # and the caller
  end
  
  def octets_to_mbps
    return @@octets_to_mbps_proc
  end
  
  # returns ["name" => { 
  #           :sla => {} # optional depends on object ie Segment(N/A) vs ENNI but not port data
  #           :data => {cos_name => :ingress => [{:x => XX, :y => YY}]
  #                                 :egress => [{:x => XX :y => YY}]
  #                                 :sla => {} } # Segment(N/A), ENNI (N/A)
  #                                 :sla => {:ingress | :egress}{:utilization_high_threshold | utilization_low_threshold}  for Segment only  
  #           :sap_class => Klass
  #           :sap_id => id}]
  def stats_frame_results(data, counter_type, calc = frame_deltas)
    pp data
    return_data = {}
    data.each do |name, sap_data|
      return_data[name] = {}
      return_data[name][:sap_class] = sap_data[:klass]
      return_data[name][:sap_id] = sap_data[:id]
      # for ENNI data return the sla if needed
      if sap_data.has_key?(:sla)        
        return_data[name][:sla] = sap_data[:sla].clone
        if ! (counter_type == :octet_counts && calc == @@octets_to_mbps_proc)
          # utilization threshold only apply to the Mbits/s results
          return_data[name][:sla].delete(:utilization_high_threshold)
          return_data[name][:sla].delete(:utilization_low_threshold)
        end
      end
          
      return_data[name][:data] = {}      

      sap_data[:data].each do |cos_name, results|
        return_data[name][:data][cos_name] = {}
        if !results[:sla].blank?        
          return_data[name][:data][cos_name][:sla] = results[:sla].clone        
          if ! (counter_type == :octet_counts && calc == @@octets_to_mbps_proc)
            # utilization threshold only apply to the Mbits/s results
            return_data[name][:data][cos_name].delete(:sla)
          end       
        end
        
        #return_data[name][:data][cos_name][:sla] = results[:sla] if results.has_key?(:sla)
        
        return_data[name][:data][cos_name][:ingress] = []
        return_data[name][:data][cos_name][:egress] = []
        y_data = []
        previous = nil
        results[counter_type].each do |result|
          ti, te = calc.call(previous, result)
          return_data[name][:data][cos_name][:ingress] << {:x => result[:timeCaptured], :y => ti} if ti != nil
          return_data[name][:data][cos_name][:egress] << {:x => result[:timeCaptured], :y => te} if te != nil
          previous = result
        end
      end
    end

    return return_data
  end
  
  def stats_set_collection_interval_based_on_points(max_points_to_collect = 80)
    # Calaculate the collection period based on the the number of points 
    # Assuming the minimum period is 1 day.
    @collection_period_secs = (@end_time-@start_time).to_i/max_points_to_collect
    @collection_period_secs = collection_period.to_i if @collection_period_secs < collection_period.to_i
    @collection_period_secs = (@collection_period_secs/collection_period) * collection_period # round down to nearest collection period    
  end
  
  def stats_set_collection_interval(collection_interval = 30.minutes)
    collection_interval = collection_period if collection_interval < collection_period 
    # round down to nearest collection_period
    @collection_period_secs = (collection_interval/collection_period) * collection_period
  end

  def compute_delay(delay_type)
    return 0.0
  end
  
  # Takes a seris of time,val points and based on the passed list of event records divide them into a number of arrays of points depending
  # on the chop_times. The returned array will look like
  # [ [[x,y],[x,y]...], [[x,y],[x,y].....], ....... ]
  # results is an array of points (x,y) in the format [{:x=>time_seconds, :y=> val}, {}] or [[time,val]...]. Note the :x, :y etc can be set by 
  # passing the result_time_access, result_y_access to be another key or array index if you want
  # The threshold parameter is used to indcate when to split a new line.
  # The extend_line is used when the results are a state rather than a number of measured points. In this case the API will insert
  # extra x,y values to extend the state to the time of the chop
  def stats_chop_results(results, chop_times, threshold, extend_line = false, result_time_access = ':x', result_y_access = ':y')
    return [results] if chop_times.size == 0 || results.size == 0
    result_x = eval(result_time_access)
    result_y = eval(result_y_access)

    lines = []
    # find the starting point in the results based on the time of the first chop_time
    start_idx = results.bsearch_lower_boundary {|x| x[result_x].to_i*1000 <=> chop_times.first.time_of_event.to_i}-1
    
    start_idx += 1 if results[start_idx][result_x].to_i * 1000 == chop_times.first.time_of_event.to_i # if it's an extact time match then correct start_idx
    start_idx = 0 if start_idx < 0
    # add the results before the first chop time
    #lines << results[0..start_idx] if start_idx > 0
    
    # Find the ending point in the chop_times based on the last time of the results 
    last_chop_idx = chop_times.bsearch_upper_boundary {|x| x.time_of_event.to_i <=> results.last[result_x].to_i*1000 }-1
    
    # If all the chop times appear after the last result return the results
    return [results] if last_chop_idx < 0

    pre_point = nil  # holds a x,y value to prepend if needed (only set when extend_line is true)
    in_ok_period = chop_times.first.state >= threshold ? false : true #true    
    
    chop_times[0..last_chop_idx].each do |ct|
      lb = results.bsearch_upper_boundary {|x| x[result_x].to_i*1000 <=> ct.time_of_event.to_i}-1
      
      # if the chop time is before any results skip it
      if lb < 0
        in_ok_period = ct.state >= threshold ? false : true
        next
      end
          
      if ct.state >= threshold
        # The state is at the threshold so append the result data from start_idx to the current index
        if in_ok_period 
          tmp = []
          tmp += [pre_point] if pre_point != nil
          tmp += results[start_idx..lb]
          if ct.time_of_event.to_i/1000 > results[lb][result_x].to_i && extend_line
            tmp += [{result_x => ct.time_of_event.to_i/1000, result_y => results[lb][result_y]}]
          end
          lines << tmp.uniq
          in_ok_period = false
        end
      elsif !in_ok_period
        # The state is below the threshold so record the start index in the results 
        # and set the pre_point to the states change time and the y value to the 
        # to the start_index y value only if extend_line is true
        in_ok_period = true
        start_idx = lb
        start_idx += 1 if results[lb][result_x].to_i * 1000 != ct.time_of_event.to_i # if it's not an extact time match then correct start_idx
        start_idx = 0 if start_idx < 0 # The time of the event is before the 1st result so set start_idx to zero
        pre_point =  {result_x => ct.time_of_event.to_i/1000, result_y => results[lb][result_y]}  if extend_line #if lines.size > 0 && extend_line            
      end
    end
    
    tmp = []
    tmp += [pre_point] if pre_point != nil
    tmp += results[start_idx..-1]
    lines << tmp if chop_times[last_chop_idx].state < threshold
        
    return lines
  end
  
  # what_ua is an array of :alarm_times, :mtc_times
  # return value is a hash [:alarm_times | :mtc_times] => [ {"start" => time_milli|nil, "end" => time_milli|nil}... ]
  # start = An alarm  or maintenance period has started
  # end = An alarm or maintenance period has ended
  # A nil time at the start indicates that there was a unavailable start prior to the start_time
  # and the caller uses the start time
  # a nil time at the end of the array indicates it's still  unavailable after the end_time
  def unavailable_periods(what_ua, ua_threshold = UNAVAILABLE_THRESHOLD)
    start_time = @start_time.to_i
    end_time = @end_time.to_i
    start_ms = start_time * 1000
    end_ms = end_time * 1000
    
    return_value = {}
    if !what_ua.is_a?Array
      what_ua = [what_ua]
    end
    what_ua.each do |which_ua|
      ua_periods = case which_ua
      when :mtc_times
        threshold = AlarmSeverity::MTC
        @obj.mtc_history.subset(start_ms, end_ms)
      when :alarm_times
        threshold = ua_threshold
        @obj.alarm_history.subset(start_ms, end_ms) 
      else
        SW_ERR "Invalid argument #{which_ua.inspect}"
        return {}
      end
               
      previous = nil
      ua_times = []
      ua_periods.alarm_records.each {|ar| 
        if ar.state >= threshold
          ua_times << {"start" => ar.time_of_event.to_i} if (previous == false || previous == nil)
          previous = true
        else
          ua_times.last["end"] = ar.time_of_event.to_i if previous == true
          previous = false
        end
      }
    
      # if there are unavailable periods set the start/end times to be nil
      # if they straddle the start_time/end_time
      if !ua_times.empty? 
        ua_times.first["start"] = nil if ua_times.first["start"] < start_ms
        if ua_times.last["end"] == nil
          ua_times.last["end"] = nil
        end
      end
      return_value[which_ua] = ua_times
    end
      
    return return_value
  end
  
  def stats_delay_attribute_subset(delay_type, attributes)
    case delay_type
    when :delay
      attributes.reject {|a| a.to_s[/variation/]}
    when :delay_variation
      attributes.find_all {|a| a.to_s[/variation/]}
    else
      SW_ERR "Invalid delay type #{delay_type}"
      []
    end
  end
    
  def stats_delay_attriute_type(attribute)
    if attribute.to_s[/max/]
      "Maximum"
    elsif attribute.to_s[/min/]
      "Minimum"
    else
      ""
    end
  end
    
  def stats_calculate_overall_flr
    return compute_overall_flr 
  end
  
  # Note this updates the data passed in ie the contents of lines
  def self.pad_results(lines)
    timestamps = lines.collect {|line| line.collect {|data| data[:x]}}.flatten.uniq.compact
    lines.each do |line|
      line_ts = line.collect {|data| data[:x]}
      missing_ts = timestamps - line_ts
      missing_ts.each do |ts|
        line << {:x => ts, :y => nil}
      end
      line.sort! {|a,b| a[:x] <=> b[:x]}.last
    end
    return nil
  end      
  
  protected
  # This is used to adjust the start_time when collecting stats which will be displayed as a delta. If the end_time
  # does not fall on the end of a collection period then the Archive will return the last data point as the partial
  # total which will look like a large dip the values on a graph.
  # This method will use the end_time and adjust the start_time so it aligns on a collection interval
  def adjust_start_time
    points_to_collect = ((@end_time-@start_time).to_i/@collection_period_secs).to_i
    adjusted_start_time = @end_time.to_i - (@collection_period_secs * (points_to_collect))
    return adjusted_start_time
  end

  def get_available_time_periods(avail_data)
    time_periods = []
    for i in (0..avail_data.length-2)
      if avail_data[i][:y] == 1
        start_adjust = 0
        end_adjust = 0
        if i > 0 && avail_data[i-1][:y] == 0
          start_adjust = collection_period
        end
        if avail_data[i+1][:y] == 0
          end_adjust = collection_period
        end
        time_periods << [(avail_data[i][:x]-start_adjust), (avail_data[i+1][:x]+end_adjust)]
      end
    end
    return time_periods
  end
      
  # The return value is 
  # {:data => {
  #             "test_one"  => {"real-time" => {:x,:y}, {:x,:y}}
  #                            {"premium"   => {:x,:y}, {:x,:y}}
  #             "test_two"  => {"real-time" => {:x,:y}, {:x,:y}}
  #                            {"premium"   => {:x,:y}, {:x,:y}}
  #            }
  #  :sla => {"real_time" => "xxx", "premium" => "yyy"}
  # }
  # ie xxx[:data]["test_name"]["real-time"]
  #    xxx[:sla]["real-time"]
  def stats_process_delay_data(data, attribute, sla_attribute, ignore_sla = false)
    return_data = {}      
    slas, sla_ids = stats_get_slas(sla_attribute)
    refs, ref_ids = stats_get_reference_data(attribute)
    
    if ignore_sla
      # make up the slas based on what was collected and add a zero as the threshold for
      # those CoS which were N/A
      cos_names = data.keys
      cos_names.each do|cos_name| 
        if slas[cos_name] == nil
          slas = {}
          slas[cos_name] = {}      
        end
      end    
    elsif slas.size == 0
      # There is no CoS so return no data
      agg = case attribute
      when :delay, :delay_variation
        {:aggregate => 0.0}
      else
        {}
      end

      result = {:data => return_data, :sla => slas, :sla_id => sla_ids}
      result.merge!(agg)
      return result
    end
    
    slas.keys.each do |cos_name| 
      tests = data[cos_name].collect {|record| record['neTestIndex']}.uniq   
      tests.each do |test_name|
        # Collect delay data for each test    
        y_data = []
        time_data = []

        data[cos_name].each do |record|
          if record["neTestIndex"] == test_name 
            next if !record.has_key?(attribute) # if the attribute is not present don't process the record
            # if the value is nil return nil otherwise return the value
            y_value = nil     
            y_value = (record[attribute].to_f).round(2) if record[attribute]
            y_data << {:x => record['timeCaptured'].to_i, :y => y_value}              
          end      
        end   
        return_data[test_name] = {} if return_data[test_name] == nil
        return_data[test_name][cos_name] = y_data
      end
    end
    
    agg = case attribute
    when :delay
      {:aggregate => (compute_delay(:delay)).round(4)}
    when :delay_variation
      {:aggregate => compute_delay(:delay_variation).round(4)}
    else
      {}
    end
      
    result = {:data => return_data, :sla => slas, :sla_id => sla_ids}
    if !refs.empty?
      result[:ref] = refs
      result[:ref_id] = ref_ids
    end
    
    result.merge!(agg)
    
    return result
  end
  
  # returns [{"cos_name" => {:sla => xxxx, :sla_warning_threshold => xx}, {cos_id => {:sla => xx, :sla_warning_threshold => xx}] 
  def stats_get_slas(sla_attribute)
    SW_ERR "Not implemented"
  end 
  
  def stats_get_reference_data(attribute)
    # No reference data by default
    return {}, {}
  end
  

  public

  def get_sla_name(sla_metric)
    case sla_metric
    when :flr
      return "aggregate_flr"
    when :delay
      return :delay
    when :delay_variation
      return  :delay_variation
    when :max_delay
      return  :max_delay
    when :min_delay
      return  :min_delay
    when :max_delay_variation
      return  :max_delay_variation
    when :min_delay_variation
      return  :min_delay_variation
    else
      return nil
    end
  end


end
