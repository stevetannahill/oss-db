
# set up logging
MAX_LOG_SIZE = 10*1024*1024
class FormatLogger < Logger
  def format_message(severity, timestamp, progname, msg)
    "#{timestamp.to_s} #{Process.pid} #{severity} #{msg}\n"
  end
end

# Main begins
logger = FormatLogger.new("#{Rails.root}/log/#{File.basename(__FILE__, '.*')}.log", 1, MAX_LOG_SIZE)
Rails.logger = logger # Give logger access to all ruby objects
Rails.logger.level = (Rails.env == "development") ? Logger::Severity::DEBUG : Logger::Severity::INFO

keep_going = true
while (keep_going)
  Rails.logger.info "#{File.basename(__FILE__, '.*')} starting..."
  
  # Ensure the connection to the database is up
  ActiveRecord::Base.verify_active_connections!  
  cfg = SlaExceptionGenerator.config
  # Find latest time used to compute exceptions
  compute_start_time = Time.now
  last_time = ExceptionTime.find(:first)
  if last_time.nil?
    last_time = ExceptionTime.new
    start_time=SlaExceptionGenerator::FIRST_TIME
  else
    start_time=Time.at(last_time.time)
  end
  last_time.time = compute_start_time.to_i
  end_time = Time.now
  Rails.logger.info "Exception computation starting at #{compute_start_time}, start month(#{start_time}) end month(#{end_time})"
  SlaExceptionGenerator.compute_exceptions  start_time, end_time
  Rails.logger.info "Exception computation started at #{compute_start_time} and ended at #{Time.now}"

#  compute_start_time = Time.now
#  Rails.logger.info "Top talkers computation starting at #{compute_start_time}, start month(#{start_time}) end month(#{end_time})"
#  SlaExceptionGenerator.compute_top_talkers start_time, end_time
#  Rails.logger.info "Top talkers started at #{compute_start_time} and ended at #{Time.now}"

  last_time.save
  # sleep for configured interval - the time it took to do the processing
  remaining_time = cfg["processing_interval"]-((Time.now-compute_start_time).to_i)
  sleep(remaining_time) if remaining_time > 0
end

