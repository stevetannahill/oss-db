
class SamStatsArchive < Stats   

  def initialize(obj, start_time, end_time, current_time=Time.now, adjust_time=true)
    super(obj, start_time, end_time, current_time, adjust_time)
    # For SAM the time unit is milliseconds so convert from seconds to milliseconds
    @time_periods_millisecs = @time_periods_secs.map {|time| [to_native(time[0]), to_native(time[1])]  }
  end
  
  def get_supported_stats
    supported_stats = []
    if @obj.is_a?(OnNetOvc)
      supported_stats = [:availability, :flr, :delay, :traffic]
      if @obj.is_in_ls_core_transport_path
        if @obj.is_on_mini_site?
          supported_stats = [:availability]
        else
          supported_stats = [:availability, :flr, :traffic]
        end
      end
    else
      SW_ERR "Unsupported object #{@obj.class}"
    end
    return supported_stats
  end

  def get_delay_attributes(attribute_types)
    attribute_types = [attribute_types] if !attribute_types.is_a?(Array)
    attributes = attribute_types.uniq.collect do |attribute|
      case attribute
      when :sla
        [:delay, :delay_variation]
      when :troubleshooting
        [:min_delay, :max_delay]
      else
        SW_ERR "Invalid attribute type #{attribute}"
        []
      end
    end.flatten
    
    return attributes
  end    
  
  # returns {"cos_name" => {:cos_id => xx, :sla => {:sla_threshold => xx, :sla_warning_threshold => yy}, :aggregate_flr => FFF, :data => [{:x => XX, :y => YY}...]}}
  def stats_flr_results(normalized_data, cos_names = nil)  
    if cos_names == nil
      cos_names = normalized_data.keys
    else
      # if a single value is passed make it an array and then convert any Fixnums from the id to the cos_name
      cos_names = [cos_names] if !cos_names.is_a?(Array)
      cos_names = cos_names.collect {|cos_name| cos_name.is_a?(Fixnum) ? slas.find {|cos, sla_value| sla_value[:cos_id] == cos_name}.first : cos_name}.compact  
    end
    
    # only return data for the slas which have data
    cos_names = cos_names.collect {|cos_name| cos_name if normalized_data[cos_name] != nil}.compact
    
    # Get the overall FLR for each COS
    overall_flrs = stats_calculate_overall_flr
    
    return_data = {}
    cos_names.each do |cos_name|
      y_data = {}
      normalized_data[cos_name][:data].each do |name, data|
        y_data[name] = []
        last_idx = data.length-2
        for i in (0..last_idx)
          ti = data[i][:deltaIngress]
          te = data[i][:deltaEgress]
          delta = ti-te
           
          #Rails.logger.debug "#{cos_name} TOP flr points ti: #{ti} te: #{te} delta: #{delta} #{Time.at(data[i+1][:timeCaptured])} "
       
          # If there has been frame loss and we are at the end of the data set ie no next ti,te
          # then exit the loop. If the delta is zero then we can plot the frame loss as there is no 
          # need to access the next time period to see if any adjustments are needed.
          break if delta != 0 && i > last_idx-1
       
          if i <= last_idx-1
            ti_next = data[i+1][:deltaIngress] 
            te_next = data[i+1][:deltaEgress]             
            delta_next = ti_next - te_next
          end
           
          if (delta < 0) && (delta_next > 0)
            # The current period has more egress than ingress and the next period has less egress than ingress
            # Reduce the current period's egress by the next periods shortfall and also adjust the
            # the egress for the start of the next period down so the calculation will credit the next period
            # with more egress
            #te = te - delta.abs          
            if delta_next.abs > delta.abs
              delta_adj = delta.abs
            else
              delta_adj = delta_next.abs
            end
            te = te - delta_adj        
            data[i+1][:deltaEgress] = data[i+1][:deltaEgress]+delta_adj
          elsif (delta > 0) && (delta_next < 0)
            # The current period has less egress than ingress and the next period has more egress than ingress
            # Increase the current period's egress by the next periods excess and also adjust the
            # the egress for the start of the next period up so the calculation will debit the next period
            # with less egress
            #te = te + delta_next.abs
            if delta_next.abs > delta.abs
              delta_adj = delta.abs
            else
              delta_adj = delta_next.abs
            end
            te = te + delta_adj
            data[i+1][:deltaEgress] = data[i+1][:deltaEgress]-delta_adj #(delta_next.abs)         
          end
            
          y_value = 0.0

          if(ti > 0) # If there is no ingress there is no frame loss
            y_value = ((ti - te).to_f/ti.to_f) * 100
            if y_value.abs < 5e-4
              # if Y is very small set it to zero
              y_value = 0.0
            end
          end
          timeCaptured = data[i][:timeCaptured].to_i
          y_data[name][i] = {:x => timeCaptured, :y => y_value}
          Rails.logger.debug "#{cos_name} flr points y:#{y_value} ActualFLR:#{((ti - te).to_f/ti.to_f) * 100} ActualFlr before adj:#{(delta.to_f/ti.to_f) * 100} #{Time.at(timeCaptured)} Delta:#{delta} Delta_next:#{delta_next} ti:#{ti} te:#{te}"
        end

        # The first entry in y_data is NOT returned as the start time is adjusted to be 1 collection period before in case the start time is in the middle of a 
        # equal and opposite spikes - Always collecting before the start time means the algorithm in stats_flr_results will cancel out this out. 
        # stats_flr_results will then not return the 1st data point
        y_data[name] = y_data[name][1..-1]
        y_data[name] = [] if y_data[name].blank? # If there is no data always return an empty array
      
        overall_flr = overall_flrs[cos_name][:aggregate_flr]
        if overall_flr.nan? 
          overall_flr = "No data available"
        else
          overall_flr = "#{'%.5f' % overall_flr}"
        end
        return_data[cos_name] = {:cos_id => normalized_data[cos_name][:cos_id], :sla => normalized_data[cos_name][:sla], :aggregate_flr => overall_flr, :data => y_data}
      end
    end
    if @pad_results
      z = []
      return_data.each {|cos, cos_v| cos_v[:data].each {|test, test_v| z << test_v}}
      Stats.pad_results(z)
    end
    return return_data
  end
  
        
  def stats_get_delay_data(delay_type, test_name = nil)
    class_name = "ethernetoam.CfmTwoWayDelayTestResult"
    where = [["`maintenanceAssociationId`='#{@obj.service_id}'", "`execTrigger`='neTriggered'"]]
    where[0] << "`neTestIndex`='#{test_name}'" if test_name != nil
    group_by = ["neTestIndex", "execTrigger", "maintenanceAssociationId"]
    order_by = [timestamp_name]

    if !delay_type.is_a?(Array)
      delay_type = [delay_type]
    end
    
    attributes = {}
    sla_attributes = {}
    delay_type.each do |dt|    
      if dt == :delay_variation
        attributes[:delay_variation] = "AVG(roundTripJitter/2)"
        sla_attributes[:delay_variation] = 'delay_variation_us'
      elsif dt == :delay
        attributes[:delay] = "AVG(averageRoundTripTime/2)"
        sla_attributes[:delay] = 'delay_us'
      elsif dt == :min_delay
        attributes[:min_delay] = "MIN(minimumRoundTripTime/2)"
        sla_attributes[:min_delay] = 'delay_us'
      elsif dt == :max_delay
        attributes[:max_delay] = "MAX(maximumRoundTripTime/2)"
        sla_attributes[:max_delay] = 'delay_us'
      else
        SW_ERR "Invalid delay type #{dt}"
      end
    end
    
    time_periods_msecs = [[to_native(@start_time) + 999, to_native(@end_time) + 999]]  # period [begin + 999ms, end + 999ms] time  the bounds are inclusive.
    time_increment_msecs = to_native(@collection_period_secs)

    nms_id = @obj.get_network_manager.id
    result_data = DataArchiveIf.query_and_aggregate( class_name, timestamp_name, where, group_by, order_by, attributes, time_periods_msecs, time_increment_msecs)
    result_data = [] if result_data.nil?
    
    # strip out "execTrigger" and change time units to seconds and convert "delay", "delay_variation etc to symbols
    result_data.each {|result|
      result.delete("execTrigger")
      result[timestamp_name] = to_sec(result[timestamp_name])
      result.keys.each {|k|
        if delay_type.include?(k.intern)
          result[k.intern] = result[k]
          result.delete(k)
        end
      }
    }    
    
    data= {}
    limit_attributes = attributes.keys
    
    # Now convert the results to be per COS
    @obj.cos_instances.each do |cosi|
      data[cosi.class_of_service_type.name] = result_data.clone      
      limit_attributes.each do |limit_attribute|
        # Only create data when there is a COS specified
        if cosi.class_of_service_type.service_level_guarantee_types.first.send(sla_attributes[limit_attribute]) != "N/A"
          max_queue_delay = cosi.sla_max_queue_delay
          data[cosi.class_of_service_type.name].each {|result| result[limit_attribute] = result[limit_attribute.to_s].to_i + max_queue_delay}
        end
      end
    end

    return data
  end
  
  def stats_get_delay_tests(ignore_sla = false)    
    time_periods_msecs = [[to_native(@start_time) + 999, to_native(@end_time) + 999]]  # period [begin + 999ms, end + 999ms] time  the bounds are inclusive.
    nms_id = @obj.get_network_manager.id
    data = DataArchiveIf.query_delay_tests( time_periods_msecs)
    data = [] if data.nil?
    
    tests =  data.collect {|test| test["neTestIndex"]}.flatten.uniq
    # If there are no COS then don't return the tests
    return_tests = {}
    ["delay_variation_us", "delay_us"].each { |sla_attribute|
      return_tests[sla_attribute[0..-4]] = []
      return_tests[sla_attribute[0..-4]] = tests if stats_get_slas(sla_attribute).first.size != 0 || ignore_sla
    }
    return return_tests
  end
    
  def stats_get_normalized_flr_data(ignore_sla = false)
    # only single COS suppoted at the moment
    # test ["OutOfProfileOctetsForwarded", "inProfileOctetsForwarded"] ["outOfProfilePktsForwarded", "inProfilePktsForwarded"]
    if @obj.cos_instances.size > 1 
      SW_ERR "Multi COS is not supported"
      slas = stats_get_flr_slas(ignore_sla)
      refs, ref_ids = stats_get_reference_data(:frame_loss_ratio)
      return {slas.keys.first => {:cos_id => slas[slas.keys.first], :sla => slas[slas.keys.first], :ref => refs[refs.keys.first], :data => []}}
      #return per_cos_flr(ignore_sla)
    else
      return stats_sam_get_single_cos_flr(ignore_sla)
    end
  end
  

  # counter_types is an array of the following :dropped_frames, :frame_counts, :octet_counts, :dropped_octets 
  # returns raw data to be passed to stats_frame_results
  # return data is [[DisplayedName][:klass]
  #                                [:id]
  #                                [:data][cos_name][counter_type => [{:timeCaptured => xx, :deltaIngress => ccc, :deltaEgress => kk}.]..]
  #                                                 [:cos_id]
  #                                                 [:sla]["ingress" | "egress"][:utilization_high_threshold | utilization_low_threshold]
  #               ]
  def stats_get_frame_count_data(counter_types)     
    single_cos = @obj.cos_instances.size == 1
    counters = {}
    counter_types.each do |counter_type|
      case counter_type   
      when :dropped_frames
        if single_cos
          ingress_counters = ["ingressQChipDroppedHiPrioPackets_delta",
            "ingressQChipDroppedLoPrioPackets_delta"]
          egress_counters = ["egressQChipDroppedOutProfPackets_delta",
            "egressQChipDroppedInProfPackets_delta"]
        else
          ingress_counters = ["inProfilePktsDropped_delta", "outOfProfilePktsDropped_delta"]
          egress_counters = []
        end
      when :frame_counts 
        if single_cos
          ingress_counters = ["ingressQChipForwardedOutProfPackets_delta",
            "ingressQChipForwardedInProfPackets_delta"]
          egress_counters = ["egressQChipForwardedOutProfPackets_delta",
            "egressQChipForwardedInProfPackets_delta"]
        else
          ingress_counters = ["outOfProfilePktsForwarded_delta", "inProfilePktsForwarded_delta"]
          egress_counters = []
        end
      when :octet_counts
        if single_cos
          ingress_counters = ["ingressQChipForwardedInProfOctets_delta",
            "ingressQChipForwardedOutProfOctets_delta"]
          egress_counters = ["egressQChipForwardedInProfOctets_delta",
            "egressQChipForwardedOutProfOctets_delta"]
        else
          ingress_counters = ["inProfileOctetsForwarded_delta", "outOfProfileOctetsForwarded_delta"]
          egress_counters = []
        end
      when :dropped_octets 
        if single_cos
          ingress_counters = ["ingressQChipDroppedHiPrioOctets_delta",
            "ingressQChipDroppedLoPrioOctets_delta"]
          egress_counters = ["egressQChipDroppedInProfOctets_delta",
            "egressQChipDroppedOutProfOctets_delta"]
        else
          ingress_counters = ["inProfileOctetsDropped_delta", "outOfProfileOctetsDropped_delta"]
          egress_counters = []
        end
      else
        SW_ERR "Invalid counter type #{counter_type.inspect}"
      end
      counters[counter_type] = {:ingress_counters => ingress_counters, :egress_counters => egress_counters}
    end
    
    if single_cos
      data = stats_get_frame_data(counters)
    else
      SW_ERR "Multi-cos not supported"
      data = {}
      #data = per_cos_frame_data(counters)
    end
    
    # Add per COS SLA to octect_counts only. This is so the utilization thresholds can be displayed
    
    data.each do |display_name, frame_data|      
      frame_data[:data].keys.each do |cos_name|
        if frame_data[:data][cos_name].has_key?(:octet_counts)
          if frame_data[:data][cos_name][:cos_id]  != nil
            cep = CosEndPoint.find(frame_data[:data][cos_name][:cos_id])
            frame_data[:data][cos_name][:sla] = {}
            [:ingress, :egress].each do |direction|
              max_capacity = cos_max_capacity(cep, direction)
              if max_capacity != 0
                frame_data[:data][cos_name][:sla][direction] = {}
                frame_data[:data][cos_name][:sla][direction][:utilization_high_threshold] = cep.segment_end_point.get_utilization_threshold_mbps(direction, :major)
                frame_data[:data][cos_name][:sla][direction][:utilization_low_threshold] = cep.segment_end_point.get_utilization_threshold_mbps(direction, :minor)
              end
            end
          end
          # If there is no SLA data then remove empty sla hash
          frame_data[:data][cos_name].delete(:sla) if frame_data[:data][cos_name][:sla].blank?
        end
      end
    end      
        
    return data
  end

  def compute_overall_flr 
    # For SAM the time unit is milliseconds so convert from seconds to millisec
    time_periods_msecs = @time_periods_millisecs
    overall_flr = (0.0/0.0) # This is NaN

    # If there are no time periods then it's been available all the time
    if time_periods_msecs.empty?
      overall_flr = 0.0
    else
      class_name = "service.SapBaseStatsLogRecord"
      ingress_counters = {"ingressQChipForwardedOutProfPackets_delta" => "SUM(ingressQChipForwardedOutProfPackets_delta)",
        "ingressQChipForwardedInProfPackets_delta" => "SUM(ingressQChipForwardedInProfPackets_delta)"}
      egress_counters = {"egressQChipForwardedOutProfPackets_delta" => "SUM(egressQChipForwardedOutProfPackets_delta)",
        "egressQChipForwardedInProfPackets_delta" => "SUM(egressQChipForwardedInProfPackets_delta)",
        "egressQChipDroppedInProfPackets_delta" => "SUM(egressQChipDroppedInProfPackets_delta)",
        "egressQChipDroppedOutProfPackets_delta" => "SUM(egressQChipDroppedOutProfPackets_delta)"}
      attributes = ingress_counters.merge(egress_counters)
      group_by = ["monitoredObjectSiteId" , "displayedName"]

      # get list of saps to get the stats from
      saps = []
      @obj.segment_end_points.each {|ep| 
        ep.enni.ports.each {|port|
          saps << ["`displayedName`='#{ep.stats_displayed_name}'", "`monitoredObjectSiteId`='#{port.node.loopback_ip}'"]
        }
      }
      nms_id = @obj.get_network_manager.id
      data = DataArchiveIf.query_and_summarize(class_name, timestamp_name, saps.uniq, group_by, attributes, time_periods_msecs)
      data = [] if data.nil?
    
      # add up all the ingress/egress for each SAP - The API will add up all the data for each given time period
      ti = 0
      te = 0

      data.each_index do |i|
        idata = data[i]
        Rails.logger.debug "Overall flr raw #{idata["displayedName"]}:#{idata["monitoredObjectSiteId"]} #{idata["ingressQChipForwardedOutProfPackets_delta"]} #{idata["ingressQChipForwardedInProfPackets_delta"]} #{idata["egressQChipForwardedOutProfPackets_delta"]} #{idata["egressQChipForwardedInProfPackets_delta"].to_i} #{idata["egressQChipDroppedInProfPackets_delta"].to_i} #{idata["egressQChipDroppedOutProfPackets_delta"].to_i}"

        ingress = ingress_counters.keys.inject(0) {|total, counter| total = total + idata[counter].to_i}
        egress = egress_counters.keys.inject(0) {|total, counter| total = total + idata[counter].to_i}
 
        ti = ti + ingress
        te = te + egress
      end

      if !data.empty?
        # if there was data and there was no ingress return 0 rather than NaN
        # If CCM tests are running on the Segment then we must subtract those frames from the egress count
        # Find the number of collection_period periods that were Available 
        # Temporary until we can get this from the Archive
        collection_periods = []

        time_periods_secs1 = @time_periods_secs
        time_periods_secs1[0][0] = @start_time.to_i
        time_periods_secs1.each do |tp|
          start_tp_collection_period = (tp[0]-@start_time.to_i)/collection_period.to_f
          end_tp_collection_period = ((tp[1]-@start_time.to_i)/collection_period.to_f)+1
          collection_periods[start_tp_collection_period..end_tp_collection_period] = [true]*((end_tp_collection_period-start_tp_collection_period))
        end
        num_time_periods = collection_periods.find_all{|tp| tp}.size     
        total_ccm_frames = (num_time_periods*(collection_period/1.minute)) * @obj.ccm_frames_generated_internally_per_minute
        
        overall_flr = ti == 0 ? 0.0 : ((ti - (te-total_ccm_frames))/ti.to_f) * 100 
        Rails.logger.debug "new overall CCM frames #{total_ccm_frames}"
      end  
      Rails.logger.debug "new overall FLR #{ti} #{te} #{overall_flr}"
    end

    # TODO - We currently only support single COS The following would have to change so it returns each COS flr rather
    # than just the same one...
    slas = {}    
    @obj.cos_instances.each do |cos| 
      threshold = cos.class_of_service_type.frame_loss_ratio
      if threshold != "N/A"
        slas["#{cos.class_of_service_type.name}"] = {:cos_id => cos.id, :aggregate_flr => overall_flr}
      end
    end    
    
    return slas
  end    


  protected
  
  def collection_period
    return 5.minutes
  end
  
  # The remove_queue_delay flag is used to get the SLAs with the queue delay removed
  # this is used so that requests to SAM can be made with the delay set as the SAM
  # results do not include the queue delay
  def stats_get_slas(sla_attribute, remove_queue_delay = false)
    slas = {}    
    sla_ids = {}
    if !sla_attribute.nil?
      @obj.cos_instances.each do |cosi|
        threshold = cosi.class_of_service_type.service_level_guarantee_types.first.send(sla_attribute)
        if threshold != "N/A"
          threshold = threshold.to_f
          threshold = threshold.to_i - cosi.sla_max_queue_delay if remove_queue_delay
          slas[cosi.class_of_service_type.name] = {}
          slas[cosi.class_of_service_type.name][:sla_threshold] = threshold
          slas[cosi.class_of_service_type.name][:sla_warning_threshold] = threshold * UtilizationThreshold.first[:sla_warning_threshold]
          sla_ids[cosi.id] = {}
          sla_ids[cosi.id][:sla_threshold] = threshold
          sla_ids[cosi.id][:sla_warning_threshold] = slas[cosi.class_of_service_type.name][:sla_warning_threshold]          
        end
      end 
    end
    return slas, sla_ids
  end
  
  def stats_get_flr_slas(ignore_sla = false)
    slas = {}    
    @obj.cos_instances.each do |cos| 
      threshold = cos.class_of_service_type.frame_loss_ratio
      if threshold != "N/A" || ignore_sla
        slas["#{cos.class_of_service_type.name}"] = {:cos_id => cos.id, :sla_threshold => threshold}
        slas["#{cos.class_of_service_type.name}"][:sla_warning_threshold] = (threshold.to_f * UtilizationThreshold.first[:sla_warning_threshold]).to_s
      end
    end    
    return slas
  end

  def stats_get_delay_sla_method(delay_type)
    case delay_type
    when :delay then 'delay_us'
    when :delay_variation then 'delay_variation_us'
    else
      nil
    end
  end
 
  def stats_get_availability_slas
    return @obj.sla_availability_value 
  end
  
  # Make a SOAP request to get the data, processes the data to a normalised format  
  # for a single COS Segment in the following format
  # returns {"cos_name" => {:cos_id => id, :data => [{:timeCaptured => time(in seconds) , :totalIngress => XX, :totalEgress => YY}]}}
  def stats_sam_get_single_cos_flr(ignore_sla = false)   
    class_name = "service.SapBaseStatsLogRecord"
    ingress_counters = {"ingressQChipForwardedOutProfPackets_delta" => "SUM(ingressQChipForwardedOutProfPackets_delta)",
      "ingressQChipForwardedInProfPackets_delta" => "SUM(ingressQChipForwardedInProfPackets_delta)"}
    egress_counters = {"egressQChipForwardedOutProfPackets_delta" => "SUM(egressQChipForwardedOutProfPackets_delta)",
      "egressQChipForwardedInProfPackets_delta" => "SUM(egressQChipForwardedInProfPackets_delta)",
      "egressQChipDroppedInProfPackets_delta" => "SUM(egressQChipDroppedInProfPackets_delta)",
      "egressQChipDroppedOutProfPackets_delta" => "SUM(egressQChipDroppedOutProfPackets_delta)"}
    attributes = ingress_counters.merge(egress_counters)
    
    # get list of saps to get the stats from
    saps = []
    @obj.segment_end_points.each {|ep| 
      ep.enni.ports.each {|port|
        saps << ["`displayedName`='#{ep.stats_displayed_name}'", "`monitoredObjectSiteId`='#{port.node.loopback_ip}'"]
      }
    }

    group_by = ["monitoredObjectSiteId" , "displayedName"]
    order_by = [timestamp_name] 
    # The start time is adjusted to be 1 collection period before in case the start time is in the middle of a equal and opposite spikes - Always collecting
    # before the start time means the algorithm in stats_flr_results will cancel out this out. stats_flr_results will then not return the 1st data point
    time_periods_msecs = [[to_native(@start_time-@collection_period_secs)+999, to_native(@end_time) + 999]]  # period [begin + 999ms, end + 999ms] time  the bounds are inclusive.
    time_increment_msecs = to_native(@collection_period_secs)    
        
    nms_id = @obj.get_network_manager.id
    data = DataArchiveIf.query_and_aggregate( class_name, timestamp_name, saps.uniq, group_by, order_by, attributes, time_periods_msecs, time_increment_msecs)
    data = [] if data.nil?  

    # add up all the ingress/egress for a time period - It's assumed that all the saps will have been measured within
    # a couple of minutes of each other.....Is this a valid assumption ???
    time = to_sec(data.first[timestamp_name]) if data.size > 0
    normalized_data = []
    ti = 0
    te = 0

    # If CCM tests are running on the Segment then we must subtract those frames from the egress count
    ccm_egress_adjustment = @obj.ccm_frames_generated_internally_per_minute * (@collection_period_secs/1.minute)
    
    sap_found = Hash[*data.collect {|d| [d["displayedName"]+d["monitoredObjectSiteId"], false]}.flatten]

    data.each_index do |i|
      idata = data[i]
      #Rails.logger.debug "raw flr data #{idata["displayedName"]}:#{idata["monitoredObjectSiteId"]} #{Time.at(idata["timeCaptured"].to_i/1000)} #{idata["ingressQChipForwardedOutProfPackets_delta"]} #{idata["ingressQChipForwardedInProfPackets_delta"]} #{idata["egressQChipForwardedOutProfPackets_delta"]} #{idata["egressQChipForwardedInProfPackets_delta"].to_i} #{idata["egressQChipDroppedInProfPackets_delta"].to_i} #{idata["egressQChipDroppedOutProfPackets_delta"].to_i}"
       
      ingress = ingress_counters.keys.inject(0) {|total, counter| total = total + idata[counter].to_i}
      egress = egress_counters.keys.inject(0) {|total, counter| total = total + idata[counter].to_i}
       
      # The data we get from SAM is per SAP which can be measured at slightly different times so
      # here we have to assume that all data closely related in time are for the same time period
      if to_sec(idata[timestamp_name]) <= time + (2.minutes)
        ti = ti + ingress
        te = te + egress
        SW_ERR "Already added in this SAP! Error?? #{sap_found[idata["monitoredObjectPointer"]]} #{idata["monitoredObjectPointer"]}" if sap_found[idata["displayedName"]+idata["monitoredObjectSiteId"]]
        sap_found[idata["displayedName"]+idata["monitoredObjectSiteId"]] = true
      end
      if (to_sec(idata[timestamp_name]) >= time + collection_period) || i == data.size-1
        # only record data if all saps have been found
        found_all_sap = true
        sap_found.each_value {|s| found_all_sap = found_all_sap && s}
        te = te - ccm_egress_adjustment
        normalized_data << {:timeCaptured => time , :deltaIngress => ti, :deltaEgress => te} # if found_all_sap # Removed this check - TDB.. when cdb has more SAP than switch still show data
        #Rails.logger.debug "Added new data #{Time.at(time)} #{ti} #{te} #{found_all_sap}"
        sap_found.each_key {|k| sap_found[k] = false}
        time = to_sec(idata[timestamp_name])
        ti = ingress
        te = egress
        sap_found[idata["displayedName"]+idata["monitoredObjectSiteId"]] = true
      end
    end

    slas = stats_get_flr_slas(ignore_sla)
    return {"#{slas.keys.first}" => {:cos_id => slas[slas.keys.first], :sla => slas[slas.keys.first], :data => {"Total" => normalized_data}}}
    
  end

  # Get frame data for the given attributes for an Segment
  # Input
  #   counters[counter_type] = {:ingress_counters => ingress_counters, :egress_counters => egress_counters}
  #
  # return data is [[DisplayedName][:klass]
  #                                [:id]
  #                                [:data][cos_name][counter_type => [{:timeCaptured => xx, :totalIngress => ccc, :totalEgress => kk}.]..]
  #               ]

  def stats_get_frame_data(counters)     
    class_name = "service.SapBaseStatsLogRecord"

    cos_name = @obj.cos_instances[0].cos_name # This is for a single cos Segment
      
    return_data = {}
    @obj.segment_end_points.each {|ep|
      return_data[ep.stats_displayed_name] = {}
      return_data[ep.stats_displayed_name][:klass] = ep.class
      return_data[ep.stats_displayed_name][:id] = ep.id
      return_data[ep.stats_displayed_name][:data] = {} 
      return_data[ep.stats_displayed_name][:data][cos_name] = {}
      return_data[ep.stats_displayed_name][:data][cos_name][:cos_id] = ep.cos_end_points.first # only single COS
      counters.keys.each {|counter_type| return_data[ep.stats_displayed_name][:data][cos_name][counter_type] = []}
    }
    
    # get the list of all the SAP counter attributes
    attributes = {}
    counters.values.each do |counter_dir|
      counter_dir.values.flatten.each do |attr|
        attributes[attr] = "SUM(#{attr})"
      end
    end

    # get list of saps to get the stats from
    saps = []
    @obj.segment_end_points.each {|ep| 
      ep.enni.ports.each {|port|
        saps << ["`displayedName`='#{ep.stats_displayed_name}'", "`monitoredObjectSiteId`='#{port.node.loopback_ip}'"]
      }
    }

    group_by = ["monitoredObjectSiteId" , "displayedName"]
    order_by = [timestamp_name]
    time_periods_msecs = [[to_native(adjust_start_time), to_native(@end_time)]]  # period time  the bounds are inclusive.
    time_increment_msecs = to_native(@collection_period_secs)
        
    nms_id = @obj.get_network_manager.id
    data = DataArchiveIf.query_and_aggregate( class_name, timestamp_name, saps.uniq, group_by, order_by, attributes, time_periods_msecs, time_increment_msecs)
    data = [] if data.nil?
    
    counters.each do |counter_type, counters|
      ingress_counters = counters[:ingress_counters]
      egress_counters = counters[:egress_counters]

      # For MultiChassis need to add up the data from each chassis to make one value 
      time = to_sec(data.first[timestamp_name]) if data.size > 0
      totalipo = 0
      total_time = 0
      write_last_result = false
      sap_data =  Hash.new {|h,k| h[k] = {:deltaIngress => 0, :deltaEgress => 0}}

      data.each_index do |i|
        idata = data[i]
        #Rails.logger.debug "raw data #{idata["displayedName"]}:#{idata["monitoredObjectSiteId"]} #{Time.at(idata["timeCaptured"].to_i/1000)} #{idata}"

        # Add counters per sap
        ingress = ingress_counters.inject(0) {|total, counter| total = total +  idata[counter].to_i}
        egress = egress_counters.inject(0) {|total, counter| total = total +  idata[counter].to_i}

        if (to_sec(idata[timestamp_name]) >= time + collection_period) || i == data.size-1
          # Its the last result and it's associated with the previous result so add it
          if i == data.size-1 && (to_sec(idata[timestamp_name]) < time + collection_period)
            sap_data[idata["displayedName"]][:deltaIngress] += ingress
            sap_data[idata["displayedName"]][:deltaEgress] += egress
          elsif i == data.size-1 && (to_sec(idata[timestamp_name]) >= time + collection_period)
            # The last result should be a seperate result so add the current and set flag to
            # perform the last write at the end of the loop
            write_last_result = true
          end
          sap_data.each {|display_name,data1|
            #Rails.logger.debug "Added new data #{Time.at(time)} #{display_name} #{data1[:deltaIngress]} #{data1[:deltaEgress]}"
            return_data[display_name][:data][cos_name][counter_type] << {:timeCaptured => time , :deltaIngress => data1[:deltaIngress], :deltaEgress => data1[:deltaEgress]}
          }
          time = to_sec(idata[timestamp_name])
          sap_data =  Hash.new {|h,k| h[k] = {:deltaIngress => 0, :deltaEgress => 0}}
          sap_data[idata["displayedName"]][:deltaIngress] = ingress
          sap_data[idata["displayedName"]][:deltaEgress] = egress
        else
          sap_data[idata["displayedName"]][:deltaIngress] += ingress
          sap_data[idata["displayedName"]][:deltaEgress] += egress
        end
      end
      if write_last_result
        sap_data.each {|display_name,data1| 
          Rails.logger.debug "Added new data #{Time.at(time)} #{display_name} #{data1[:deltaIngress]} #{data1[:deltaEgress]}"
          return_data[display_name][:data][cos_name][counter_type] << {:timeCaptured => time , :deltaIngress => data1[:deltaIngress], :deltaEgress => data1[:deltaEgress]}
        }
      end
    end
    return return_data
  end
  
  

  
  #===================================================================
  # The following code is incomplete and needs to be finished
  # - get proper q's and p' from api and remove hardcodings
  # - add broadcast support
  #  multiply the broadcast count by the number of other saps in the Segment and add it to the ingress count
  # - really cleanup code - Make api names better
  # - Test

  
  # returns {"sap1" => {queue_id_x || policer_id_x => [{:timeCaptured => time(in seconds) , "ingress" => XX, "egress" => YY}.....]},
  #                    {queue_id_2 || policer_id_2 => [{:timeCaptured => time(in seconds) , "ingress" => XX, "egress" => YY}.....]}
  #          "sap2" => {.......}
  #         }
  def get_per_cos_data(start_time, end_time, segmentepps, attributes)
    times = @times == nil ? [{:start_time => to_native(@start_time).to_s, :end_time => to_native(@end_time).to_s}] : @times
    #times = [{:start_time => ((Time.now-1.hour).to_i*1000).to_s, :end_time => (Time.now.to_i*1000).to_s}] if times == nil

    # Make a mapping from displayName, queueId|policerId, id, direction to cos_name
    queue_policer_ids_to_cos_name_mapping = Hash.new {|h,k| h[k] = Hash.new {|h,k| h[k] = Hash.new {|h,k| h[k] = Hash.new {|h,k| h[k] = nil}}}}

    # For each SAP gets it's displayed name and QueueIds or PolicerIds per direction
    # Note cannot use Hash.new {|h,k| h[k] = Hash... etc as this is a proc and marshalling cannot send procs (or "code")
      
    # This is all hardcoded - TODO call API on qos_policy to get the real data
    sapIds = {}
    sapIds["service.CompleteServiceIngressPacketOctetsLogRecord"] = {}
    sapIds["service.CompleteServiceEgressPacketOctetsLogRecord"] = {}
    cos_mapping = {}
    segmentepps.each do |ep|
      sapIds["service.CompleteServiceIngressPacketOctetsLogRecord"][ep.stats_displayed_name] = {}
      sapIds["service.CompleteServiceIngressPacketOctetsLogRecord"][ep.stats_displayed_name][:nodes] = ep.enni.ports.collect{|p| p.node.loopback_ip}.uniq.compact
      sapIds["service.CompleteServiceEgressPacketOctetsLogRecord"][ep.stats_displayed_name] = {}
      sapIds["service.CompleteServiceEgressPacketOctetsLogRecord"][ep.stats_displayed_name][:nodes] = ep.enni.ports.collect{|p| p.node.loopback_ip}.uniq.compact

      #        if !ep.is_connected_to_monitoring_port
      # Std data
      sapIds["service.CompleteServiceIngressPacketOctetsLogRecord"][ep.stats_displayed_name]["queueId"] = []
      sapIds["service.CompleteServiceIngressPacketOctetsLogRecord"][ep.stats_displayed_name]["policerId"] = []

      sapIds["service.CompleteServiceIngressPacketOctetsLogRecord"][ep.stats_displayed_name]["queueId"] = [1]
      queue_policer_ids_to_cos_name_mapping[ep.stats_displayed_name]["queueId"][1][:totalIngress] = "Standard Data"
      sapIds["service.CompleteServiceEgressPacketOctetsLogRecord"][ep.stats_displayed_name]["policerId"] = [1]
      queue_policer_ids_to_cos_name_mapping[ep.stats_displayed_name]["policerId"][1][:totalEgress] = "Standard Data"

      # RT data
      sapIds["service.CompleteServiceIngressPacketOctetsLogRecord"][ep.stats_displayed_name]["policerId"] += [2]
      queue_policer_ids_to_cos_name_mapping[ep.stats_displayed_name]["policerId"][2][:totalIngress] = "Real Time"
      sapIds["service.CompleteServiceEgressPacketOctetsLogRecord"][ep.stats_displayed_name]["policerId"] += [3]
      queue_policer_ids_to_cos_name_mapping[ep.stats_displayed_name]["policerId"][3][:totalEgress] = "Real Time"

      # Prem data
      sapIds["service.CompleteServiceIngressPacketOctetsLogRecord"][ep.stats_displayed_name]["policerId"] += [1]
      queue_policer_ids_to_cos_name_mapping[ep.stats_displayed_name]["policerId"][1][:totalIngress] = "Premium Data"
      sapIds["service.CompleteServiceEgressPacketOctetsLogRecord"][ep.stats_displayed_name]["policerId"] += [2]
      queue_policer_ids_to_cos_name_mapping[ep.stats_displayed_name]["policerId"][2][:totalEgress] = "Premium Data"
      #        end
        
=begin
        ep.qos_policies.each do |qos_policy|
          qos_policy.classes_of_service.each do |cos|         
            id_type = case cos.rlm
            when "Shaping" then "queueId"
            when "Policing" then "policerId"
            when "None" then "queueId"
            else
              SW_ERR "Invalid cos rlm #{cos.rlm}"
              ""
            end

            case qos_policy.direction
            when "Ingress"
              sapIds["service.CompleteServiceIngressPacketOctetsLogRecord"][ep.stats_displayed_name][id_type] = [] if sapIds["service.CompleteServiceIngressPacketOctetsLogRecord"][ep.stats_displayed_name][id_type] == nil
              sapIds["service.CompleteServiceIngressPacketOctetsLogRecord"][ep.stats_displayed_name][id_type] += cos.queue_policer_id
              queue_policer_ids_to_cos_name_mapping[ep.stats_displayed_name][id_type][cos.queue_policer_id.first][:totalIngress] = cos.name
            when "Egress" 
              sapIds["service.CompleteServiceEgressPacketOctetsLogRecord"][ep.stats_displayed_name][id_type] = [] if sapIds["service.CompleteServiceEgressPacketOctetsLogRecord"][ep.stats_displayed_name][id_type] == nil
              sapIds["service.CompleteServiceEgressPacketOctetsLogRecord"][ep.stats_displayed_name][id_type] += cos.queue_policer_id
              queue_policer_ids_to_cos_name_mapping[ep.stats_displayed_name][id_type][cos.queue_policer_id.first][:totalEgress] = cos.name
            else
              SW_ERR "Unkndown QoS direction #{qos.direction}"
            end
          end
        end
=end        
    end

    # collect the data by giving the SAP and QueueIds
    # name is the class name with -xxxxx appended. This is needed so tell if the record is ingress or egress

    # inProfilePktsForwarded outOfProfilePktsForwarded
    # ["OutOfProfileOctetsForwarded", "inProfileOctetsForwarded", "queueId", "sapId"]
    # sapid["displayName"] => {"queueId" => [1,2,3] || "policerId" => [1,2,3]}

    query_filter = sapIds
    sam_api = SamIf::StatsApi.new(@obj.get_network_manager)
    result, data = sam_api.get_service_stats(
      query_filter,
      times,
      attributes + ["queueId", "policerId", "displayedName", "name", "portId", "monitoredObjectPointer"])
    sam_api.close

    return result, data
      
  end

  # return data is [displayedName][cos_name][{:timeCaptured => ss, :totalIngress => ll, :totalEgress => ff}...]
  def get_sap_data(cos_data, segmentepps, attributes)
    # Make a mapping from displayName, queueId|policerId, id, direction to cos_name
    queue_policer_ids_to_cos_name_mapping = Hash.new {|h,k| h[k] = Hash.new {|h,k| h[k] = Hash.new {|h,k| h[k] = Hash.new {|h,k| h[k] = nil}}}}  
      
    # For each SAP gets it's displayed name and QueueIds or PolicerIds per direction 
    # Note cannot use Hash.new {|h,k| h[k] = Hash... etc as this is a proc and marshalling cannot send procs (or "code")      
    
    # currently hardcoded - call API on qos_policy
    cos_mapping = {}
    segmentepps.each do |ep|    
      #        if !ep.is_connected_to_monitoring_port
      # Std data
      queue_policer_ids_to_cos_name_mapping[ep.stats_displayed_name]["queueId"][1][:totalIngress] = "Standard Data"
      queue_policer_ids_to_cos_name_mapping[ep.stats_displayed_name]["policerId"][1][:totalEgress] = "Standard Data"
      
      # RT data
      queue_policer_ids_to_cos_name_mapping[ep.stats_displayed_name]["policerId"][2][:totalIngress] = "Real Time"
      queue_policer_ids_to_cos_name_mapping[ep.stats_displayed_name]["policerId"][3][:totalEgress] = "Real Time"
      
      # Prem data
      queue_policer_ids_to_cos_name_mapping[ep.stats_displayed_name]["policerId"][1][:totalIngress] = "Premium Data"
      queue_policer_ids_to_cos_name_mapping[ep.stats_displayed_name]["policerId"][2][:totalEgress] = "Premium Data"
      #        end
    end
    
    data = cos_data

    # need to check for rollover of the counters so remember the last count per SAP(monitoredObjectPointer) per Port Per Queue per Direction
    # and the last record
    # make a Hash based on the monitoredObjectPointer, Port, "queue or policer", QueueId/policerId and direction
    last_record = Hash.new {|h,k| 
      h[k] = Hash.new{|h,k|
        h[k] = Hash.new{|h,k|
          h[k] = Hash.new{|h,k|
            h[k] = Hash.new{|h,k|
              h[k] = nil}
          }
        }
      }
    }

    roll_over = Hash.new {|h,k| 
      h[k] = Hash.new{|h,k|
        h[k] = Hash.new{|h,k|
          h[k] = Hash.new{|h,k|
            h[k] = Hash.new{|h,k|
              h[k] = Hash.new{|h,k|
                h[k] = 0}
            }
          }
        }
      }
    }

    adjusted_counter = Hash.new

    totalipo = 0
    total_time = 0
    # {sapIs => {"Queue | Policer" => {QueueId => []}}

    new_data = Hash.new {|h,k| 
      h[k] = Hash.new{|h,k| 
        h[k] = [] }}

    data.each_index do |i|
      idata = data[i]
      #Rails.logger.debug "raw flr data #{idata["monitoredObjectPointer"]} #{Time.at(idata["timeCaptured"].to_i/1000)} #{idata["timeCaptured"]} #{idata["ingressQChipForwardedOutProfPackets"]} #{idata["ingressQChipForwardedInProfPackets"]} #{idata["egressQChipForwardedOutProfPackets"]} #{idata["egressQChipForwardedInProfPackets"].to_i}"
      direction = idata["name"][/.*-/]
      dir_sym = direction == "service.CompleteServiceIngressPacketOctets-" ? :totalIngress : :totalEgress
      id_type = idata["queueId"] == nil ? "policerId" : "queueId"
      #attributes.each {|a|
      #  Rails.logger.debug "Top loop #{i} #{idata["displayedName"]} #{Time.at(idata["timeRecorded"].to_i/1000)} #{idata[a]} #{direction}"
      #}

      attributes.each do |counter|
        if (last_record[idata["monitoredObjectPointer"]][idata["portId"]][id_type][idata[id_type]][direction] != nil) &&
            (idata[counter].to_i < last_record[idata["monitoredObjectPointer"]][idata["portId"]][id_type][idata[id_type]][direction][counter].to_i)
          Rails.logger.debug "Hit rollover condition at #{Time.at(idata["timeRecorded"].to_i/1000)} for #{idata["monitoredObjectPointer"]} #{counter} #{idata[counter]} #{last_record[idata["monitoredObjectPointer"]][idata["portId"]][id_type][idata[id_type]][direction][counter]}"
          #roll_over[idata["monitoredObjectPointer"]][idata["portId"]][id_type][idata[id_type]][direction][counter] += 2**128
          roll_over[idata["monitoredObjectPointer"]][idata["portId"]][id_type][idata[id_type]][direction][counter] += (last_record[idata["monitoredObjectPointer"]][idata["portId"]][id_type][idata[id_type]][direction][counter].to_i) - idata[counter].to_i
        end
        adjusted_counter[counter] = idata[counter].to_i + roll_over[idata["monitoredObjectPointer"]][idata["portId"]][id_type][idata[id_type]][direction][counter]
      end
      last_record[idata["monitoredObjectPointer"]][idata["portId"]][id_type][idata[id_type]][direction] = idata

      counter = 0
      adjusted_counter.values.compact.each {|ac| counter += ac}

      # How do we validate we got all the data ?????
      #         found_item = new_data[idata["displayedName"]][id_type][idata[id_type].to_i].bsearch {|x| x[:timeCaptured] <=> idata["timeRecorded"].to_i/1000}
      #         puts "BJW #{idata["displayedName"]} #{idata[id_type].to_i} #{(new_data[idata["displayedName"]][id_type][idata[id_type].to_i].last[:timeCaptured].to_i) + (2.minutes)} #{idata["timeRecorded"].to_i/1000}" if !new_data[idata["displayedName"]].empty?
      cos_name = queue_policer_ids_to_cos_name_mapping[idata["displayedName"]][id_type][idata[id_type].to_i][dir_sym]
      #         Rails.logger.debug "raw flr data #{cos_name} #{idata["monitoredObjectPointer"]} #{id_type} #{idata[id_type].to_i} #{dir_sym.to_s} #{Time.at(idata["timeRecorded"].to_i/1000)} #{adjusted_counter.inspect} "
       
      if (!new_data[idata["displayedName"]][cos_name].empty?) && to_sec(idata["timeRecorded"]) <= (new_data[idata["displayedName"]][cos_name].last[:timeCaptured].to_i) + (2.minutes)
        found_item = new_data[idata["displayedName"]][cos_name].size-1
      else
        found_item = nil
      end
        
      if found_item == nil
        # not found so add
        new_data[idata["displayedName"]][cos_name] << {:timeCaptured => to_sec(idata["timeRecorded"]), :totalEgress => 0, :totalIngress => 0, :num_records => 0}
        found_item = new_data[idata["displayedName"]][cos_name].size-1
      end
      new_data[idata["displayedName"]][cos_name][found_item][dir_sym] += counter
      new_data[idata["displayedName"]][cos_name][found_item][:num_records] += 1
    end
    
    
    # for each SAP validate that the correct number of records for each timestamp has been received
    # If there are any records which don't have the correct number of ports then remove the record
    segmentepps.each do |ep|
      if new_data[ep.stats_displayed_name].blank?
        SW_ERR "Could not find sapId #{ep.stats_displayed_name} in retrived data #{new_data.keys.inspect}"
      else
        num_ports = ep.enni.ports.size * 2 # *2 because of each direction
        new_data[ep.stats_displayed_name].each_pair {|cos_name, records| 
          records.each {|record|
            if record[:num_records] != num_ports
              SW_ERR "Not enough records for SapId #{ep.stats_displayed_name} id_type #{cos_name} Expected Ports #{num_ports} Actual #{record.inspect}"
              new_data[ep.stats_displayed_name][cos_name].delete(record) # delete this record from the data
            else
              record.delete(:num_records) # drop the :num_records field as it's no longer needed
            end
          }
        }
      end
    end

    return new_data
  end

  # returns {"cos_name" => {:cos_id => id, :data => [{:timeCaptured => time(in seconds) , :totalIngress => XX, :totalEgress => YY}]}}
  def per_cos_flr(ignore_sla = false)    
    result, cos_data = get_per_cos_data(@start_time, @end_time, @obj.segment_end_points, ["outOfProfilePktsForwarded", "inProfilePktsForwarded"])
    cos_frame_data = get_sap_data(cos_data, @obj.segment_end_points, ["outOfProfilePktsForwarded", "inProfilePktsForwarded"])
    # return data is [displayedName][cos_name][{:timeCaptured => ss, :totalIngress => ll, :totalEgress => ff}...]
    slas = stats_get_flr_slas(ignore_sla)
    display_names = @obj.segment_end_points.collect {|ep| ep.stats_displayed_name}.compact
    normalized_data = {}
    slas.keys.each do |cos_name|
      normalized_data[cos_name] = {:cos_id => slas[cos_name], :sla => slas[cos_name][:sla_threshold], :data => []}
      all_cos_data = display_names.collect {|display_name| cos_frame_data[display_name][cos_name]}.flatten.compact.sort {|a,b| a[:timeCaptured] <=> b[:timeCaptured]}
      next if all_cos_data.empty?
      time = all_cos_data.first[:timeCaptured]
      ti = te = 0
      all_cos_data.each_index do |i|
        # The data we get from SAM is per SAP which can be measured at slightly different times so 
        # here we have to assume that all data closely related in time are for the same time period  
        if all_cos_data[i][:timeCaptured] <= time + (2.minutes)
          ti = ti + all_cos_data[i][:totalIngress]
          te = te + all_cos_data[i][:totalEgress]
        end
        if (all_cos_data[i][:timeCaptured] >= time + collection_period) || i == all_cos_data.size-1
          normalized_data[cos_name][:data] << {:timeCaptured => time , :totalIngress => ti, :totalEgress => te} 
          #Rails.logger.debug "Added new data #{Time.at(time)} #{ti} #{te}"
          time = all_cos_data[i][:timeCaptured]
          ti = all_cos_data[i][:totalIngress]
          te = all_cos_data[i][:totalEgress]
        end    
      end
    end
    
    return normalized_data
  end

  def per_cos_frame_data(counters)      
    sap_attributes = counters.values.collect {|attributes| attributes.values}.flatten
    result, cos_data = get_per_cos_data(@start_time, @end_time, @obj.segment_end_points, sap_attributes)
    
    return_data = {}
    @obj.segment_end_points.each {|ep|
      return_data[ep.stats_displayed_name] = {}
      return_data[ep.stats_displayed_name][:klass] = ep.class
      return_data[ep.stats_displayed_name][:id] = ep.id
      return_data[ep.stats_displayed_name][:data] = {}
      @obj.cos_instances.each {|cosi|
        return_data[ep.stats_displayed_name][:data][cosi.cos_name] = {}
        counters.keys.each {|counter_type| return_data[ep.stats_displayed_name][:data][cosi.cos_name][counter_type] = []}
      }
    }
    
    #return_data = Hash.new {|h,k| h[k] = Hash.new {|h,k| h[k] = Hash.new {|h,k| h[k] = {}}}}
    if result
      display_name_to_ep = {}
      @obj.segment_end_points.each {|ep| display_name_to_ep[ep.stats_displayed_name] = ep}
      counters.each do |counter, attributes|
        cos_frame_data = get_sap_data(cos_data, @obj.segment_end_points, attributes.values.flatten)
        cos_frame_data.each do |display_name, results|
          return_data[display_name][:klass] = display_name_to_ep[display_name].class
          return_data[display_name][:id] = display_name_to_ep[display_name].id
          results.each do |cos_name, data|
            return_data[display_name][:data][cos_name][counter] = data
          end
        end
      end
    end 
    
    return return_data
  end

  #===================================================================
  public


  def compute_cos_end_point_utilization_exceptions
    table_name = 'service.SapBaseStatsLogRecord'
    nms_id = @obj.get_network_manager.id
    group_by = nil
    order_by = nil
    top_columns = {METRIC_NAMES[:octets_count] => 'SUM'}
    @obj.cos_end_points.each do|cep|
      [:ingress, :egress].each do |direction|
        [  :major, :minor ].each do |severity|
          selected_columns = (direction == :ingress) ? { METRIC_NAMES[:octets_count] =>'SUM(`ingressQChipForwardedInProfOctets_delta` + `ingressQChipForwardedOutProfOctets_delta`)' } : {METRIC_NAMES[:octets_count] => 'SUM(`egressQChipForwardedInProfOctets_delta` + `egressQChipForwardedOutProfOctets_delta`)' }
          # We don't use 'cep' to determine the right SAPs as there is no support for COS yet.
          # Therefore no more than one cep.
          saps = []
          name = @obj.stats_displayed_name
          @obj.enni.ports.each do|port|
            site  = port.node.loopback_ip
            unless site.nil? or site.empty? or name.nil? or name.empty?
              saps << ["`displayedName`='#{name}'",
                "`monitoredObjectSiteId`='#{site}'"]
            end
          end
          unless saps.nil? or saps.empty?
            where = saps.uniq
            time_periods_native = get_time_periods_native
            time_increment_secs = @obj.get_utilization_errored_period_delta_time_secs
            puts "$$$$$$$$$$$$$$$$ #{time_increment_secs} #{@obj.class}:#{@obj.id}"
            time_increment_native = to_native(time_increment_secs)
            count_condition_name=nil
            selected_columns.each{ |k,v| count_condition_name=k; break } unless selected_columns.nil?
            condition_s = "`#{count_condition_name}` > #{@obj.get_utilization_errored_period_threshold direction, severity}"
            count_condition =  [ condition_s ]
            results = DataArchiveIf.query_aggregate_and_count table_name, timestamp_name, where, group_by, order_by, selected_columns, time_periods_native, time_increment_native, count_condition, top_columns
          end
          unless (results.nil?)
            puts results
            count = results[0]['COUNT']
            time_over = count * time_increment_secs
            octets_over = results[0][METRIC_NAMES[:octets_count]]
            rate_over = octets_over *8  / time_over / 1e6  unless 0==time_over
            max_errored_count = @obj.get_utilization_errored_period_max_errored_count  direction, severity
            if count > max_errored_count
              puts "#{cep.id}, #{direction}, #{@start_time.to_i}, #{@end_time.to_i}"
              ex = ExceptionUtilizationCosEndPoint.find( :first, :conditions => [ "cos_end_point_id=? AND direction=? AND time_declared BETWEEN ? AND ?", cep.id, direction, @start_time.to_i, @end_time.to_i ])
              ex = ExceptionUtilizationCosEndPoint.new if ex.nil?
              ex.time_declared  = @end_time.to_i
              ex.cos_end_point_id = cep.id
              ex.severity = severity
              ex.value = time_over
              ex.direction = direction
              ex.time_over = time_over
              ex.average_rate_while_over_threshold_mbps = rate_over
              ex.save
              output = <<EOF
ExceptionUtilizationEnni.create :time_declared  => #{@end_time.to_i},
:cos_end_point_id => #{cep.id},
:severity => #{ex.severity},
:value =>  #{time_over},
:direction => "#{direction }",
:time_over => #{time_over},
:average_rate_while_over_threshold_mbps => #{rate_over}
valid?(#{ex.valid?})
EOF
              puts output
            break if severity == :major # stop here no need to do the :minor
            end
          end
        end
      end
    end
  end


  # return the capacity of a COS endpoint for the given direction
  def cos_max_capacity(cep, direction)
    return cep.rate_mbps direction
  end

#===============================================================================
#===============================================================================
#===============================================================================

  def self.compute_top_talkers_cos_end_point time_periods_secs
    columns = {
      "ingress" =>"`ingressQChipForwardedInProfOctets_delta` + `ingressQChipForwardedOutProfOctets_delta`",
      "egress" => "`egressQChipForwardedInProfOctets_delta` + `egressQChipForwardedOutProfOctets_delta`"
    }
    class_name = "service.SapBaseStatsLogRecord"
    # construct the list of SAPs we are interested in.
    # TODO: find a more scalable way to achieve the same results.
    saps = []
    OnNetOvcEndPointEnni.find_each do |end_point|
      if end_point.segment.get_prov_state.is_a?(ProvLive)
        name = end_point.stats_displayed_name
        end_point.enni.ports.each do |port|
          site  = port.node.loopback_ip
          unless site.nil? or site.empty? or name.nil? or name.empty?
            saps << ["`displayedName`='#{name}'", "`monitoredObjectSiteId`='#{site}'" ]
            #puts "`displayedName`='#{name}' `monitoredObjectSiteId`='#{site}'"
          end
        end
      end
    end
    where = saps.uniq
    results = compute_top_talkers time_periods_secs, class_name, columns, where
    cep_data = []
    unless results.nil? or results.empty?
      # Convert "monitoredObjectSiteId"/"displayedName" to cep id.
      results.each do |row|
        site = row["monitoredObjectSiteId"]
        name = row["displayedName"]
        begin
          # TODO: devise a more efficient way to get the enni and cep_id as this is very expensive.
          # The displayedName is in the format: "Port 1/1/14:100.0" we take the first part prior to ':' to identify the enni.
          port_name = name[0..name.index(':')-1]
          enni = Node.find_by_loopback_ip(site).ports.find_all {|port| port_name==port.demarc.stats_displayed_name unless port.demarc.nil?}.first.demarc
          cep_id = enni.segment_end_points.find_all{ |segment| name== segment.stats_displayed_name }.first.cos_end_points.first.id
          cep_data << {:cos_end_point_id=>cep_id, :monitoredObjectSiteId => site, :displayedName => name, :octets_count => row["octets_count"], :direction => "ingress"==row[:direction] ? "ingress" : "egress"}
          #   pp cep_data
        rescue Exception => e
          SW_ERR "error: site='#{site}' name='#{name}' port_name='#{port_name}': #{e.message} "
        end
      end
    end
    return cep_data
  end



  protected

  def self.compute_top_talkers time_periods_secs, class_name, columns, where
    results = []
=begin  the following has to be completely revisited.
    group_by = ["monitoredObjectSiteId" , "displayedName"]
    order_by = ["octets_count"]
    time_periods_millisecs = time_periods_secs.map {|time| [to_native(time[0]), to_native(time[1])]  }

    ["ingress", "egress"].each do |direction|
      column     = columns[direction]
      attributes = {
        "octets_count" => "SUM(#{column})",
      }
      NetworkManager.find(:all, :group => "data_archive_id").each do |nms|
        data = DataArchiveIf.query_and_summarize(class_name, timestamp_name, where, group_by, attributes, time_periods_millisecs, order_by, false)
        unless data.nil?
          data.each { |row|
            row[:direction] = direction
            row["octets_count"] = row["octets_count"].to_i
            results << row
          }
        end
      end
    end
    # Sort merged results and pick only top 10
    unless results.empty?
      results = results.sort_by { |item| item["octets_count"] }
      results.reverse!
    end
=end
    return results

  end
  
  def to_native the_time
    return the_time.to_i * 1000
  end
  
  def to_sec the_time
    # returns time in msecs
    (the_time.to_i / 1000).to_i
  end
  
  def get_time_periods_native
    # returns time in msecs
    return @time_periods_millisecs
  end

  def get_table_and_selected_columns(metric_type)
    selected_columns        = nil
    case metric_type
    when :frame_loss
      table_name = "service.SapBaseStatsLogRecord"
      # fl: (ingress - egress)
      selected_columns = {
        METRIC_NAMES[metric_type] => "(SUM(ingressQChipForwardedOutProfPackets_delta)+SUM(ingressQChipForwardedInProfPackets_delta))-(SUM(egressQChipForwardedOutProfPackets_delta)+SUM(egressQChipForwardedInProfPackets_delta)+SUM(egressQChipDroppedInProfPackets_delta+SUM(egressQChipDroppedOutProfPackets_delta))"
      }
    when :frame_loss_ratio
      table_name = "service.SapBaseStatsLogRecord"
      # flr: (ingress - egress) / ingress
      selected_columns = {
        METRIC_NAMES[metric_type] => "100 * ((SUM(ingressQChipForwardedOutProfPackets_delta)+SUM(ingressQChipForwardedInProfPackets_delta))-(SUM(egressQChipForwardedOutProfPackets_delta)+SUM(egressQChipForwardedInProfPackets_delta)+SUM(egressQChipDroppedInProfPackets_delta+SUM(egressQChipDroppedOutProfPackets_delta)) )/(SUM(ingressQChipForwardedOutProfPackets_delta)+SUM(ingressQChipForwardedInProfPackets_delta))"
      }
    when :delay
      table_name = "ethernetoam.CfmTwoWayDelayTestResult"
      selected_columns = {METRIC_NAMES[metric_type] => "MAX(averageRoundTripTime)" }
    when :delay_variation
      table_name = "ethernetoam.CfmTwoWayDelayTestResult"
      selected_columns  = {METRIC_NAMES[metric_type] => "MAX(roundTripJitter)"}
    when :min_delay
      table_name = "ethernetoam.CfmTwoWayDelayTestResult"
      selected_columns = {METRIC_NAMES[metric_type] => "MIN(minimumRoundTripTime)"}
    when :min_delay_variation
      SW_ERR "ERROR: :min_delay_variation not supported\n"
    when :max_delay
      table_name = "ethernetoam.CfmTwoWayDelayTestResult"
      selected_columns = {METRIC_NAMES[metric_type] => "MAX(maximumRoundTripTime)"}
    when :max_delay_variation
      SW_ERR "ERROR: :max_delay_variation not supported\n"
    else
      SW_ERR "ERROR: unknown  #{metric_type}\n"
    end
    return timestamp_name, table_name, selected_columns
  end

  def get_object_keys metric_type
    keys = []
    case metric_type
    when :frame_loss, :frame_loss_ratio
      @obj.segment_end_points.each {|ep|
        ep.enni.ports.each {|port|
          keys << {"displayedName" => ep.stats_displayed_name, "monitoredObjectSiteId" => port.node.loopback_ip }
        }
      }
    when :delay, :delay_variation, :min_delay, :min_delay_variation, :max_delay, :max_delay_variation
      keys << {'maintenanceAssociationId' => @obj.service_id , 'execTrigger' => 'neTriggered' }
    else
      SW_ERR "ERROR: unknown  #{metric_type}\n"
    end
    return keys
  end
  
  def timestamp_name
    "timeCaptured"
  end
  
end



#==================

