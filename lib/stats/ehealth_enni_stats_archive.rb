
class EhealthEnniStatsArchive < Stats

  def initialize(obj, start_time, end_time, current_time=Time.now, adjust_time=true)
    super(obj, start_time, end_time, current_time, adjust_time)
  end

  def stats_get_availability_slas
    return @obj.sla_availability_value
  end

  def get_supported_stats
    supported_stats = [:traffic]
    return supported_stats
  end

  # counter_types is an array of the following :frame_counts, :octet_counts
  # returns raw data to be passed to stats_frame_results
  # return data is [[DisplayedName][:klass]
  #                                [:id]
  #                                [:data]["N/A"][counter_type => [{:timeCaptured => xx, :deltaIngress => ccc, :deltaEgress => kk}.]..]
  #               ]
  def stats_get_enni_count_data(counter_types)
    counters = {}
    counter_types.each do |counter_type|
      case counter_type
      when :frame_counts
        ingress_counters = ["receivedUnicastPackets_delta",
          "receivedMulticastPackets_delta",
          "receivedBroadcastPackets_delta"]
        egress_counters = ["transmittedUnicastPackets_delta",
          "transmittedMulticastPackets_delta",
          "transmittedBroadcastPackets_delta"]
      when :octet_counts
        ingress_counters = ["receivedTotalOctets_delta"]
        egress_counters = ["transmittedTotalOctets_delta"]
      else
        SW_ERR "Invalid counter type #{counter_type.inspect}"
      end
      counters[counter_type] = {:ingress_counters => ingress_counters, :egress_counters => egress_counters}
    end
    return stats_get_enni_data(counters )
  end


  protected

  def collection_period
    return 5.minutes
  end


  # Get frame data for the given attributes for an Segment
  # Input
  #   counters[counter_type] = {:ingress_counters => ingress_counters, :egress_counters => egress_counters}
  #
  # return data is [[DisplayedName][:klass]
  #                                [:id]
  #                                [:sla => {:cir_mbps => dd, :eir_mbps => dd, :line_rate => dd}]
  #                                [:data][cos_name][counter_type => [{:timeCaptured => xx, :totalIngress => ccc, :totalEgress => kk}.]
  #                                                 [:sla => {TBD}]]
  #               ]
  # Note that the returned data will contain the combined ENNI data eg for a Multi-chassis the displayName will be "Lag 12"
  # For a lag ENNI as well as the ENNI data there will be per port data - The displayedName is in the format of "site-ip Port x/y/z"
  # For port related data there is no SLA information

  def stats_get_enni_data(counters)
    Rails.logger.debug "Serge: stats_get_enni_data"
    time_name = "timeCaptured"
    class_name = "ehealth_util"
    cos_name = "N/A" # ENNIs don't have COS
pp @obj
pp @obj.ports.first.node
    # get the list of all the ENNI counter attributes
    attributes = {}
    counters.values.each do |counter_dir|
      counter_dir.values.flatten.each do |attr|
        attributes[attr] = "SUM(#{attr})"
      end
    end
    # get list of ENNIs to get the stats from
    if @obj.is_multi_chassis
      ennis = @obj.ports.collect{|port| ["`displayedName`='#{@obj.stats_displayed_name}'", "`monitoredObjectSiteId`='#{port.node.loopback_ip}'"]}
    Rails.logger.debug "Serge: stats_get_enni_data  1"
    else
      ennis = [["`displayedName`='#{@obj.stats_displayed_name}'", "`monitoredObjectSiteId`='#{@obj.ports.first.node.loopback_ip}'"]]
    Rails.logger.debug "Serge: stats_get_enni_data  2 '#{@obj.ports.first.node.loopback_ip}'"
    end
    # For Lags get the per port data as well
    if @obj.is_lag
      ennis += @obj.ports.collect{|port| ["`displayedName`='#{port.stats_displayed_name}'", "`monitoredObjectSiteId`='#{port.node.loopback_ip}'"]}
    Rails.logger.debug "Serge: stats_get_enni_data  3"
    end
    group_by = ["monitoredObjectSiteId" , "displayedName"]
    order_by = ["timeCaptured"]
    time_increment_msecs = @collection_period_secs
    data = DataArchiveIf.query_and_aggregate(class_name, time_name, ennis.uniq, group_by, order_by, attributes, @time_periods_secs, time_increment_msecs)
    data = [] if data.nil?
    # get mapping of IP to site name
    ip_to_name = {}
    @obj.ports.each {|port| ip_to_name[port.node.loopback_ip] = port.node.name}
    if data.empty?
      # There is no data so return the expected display names but with [data][cos_name][counter_type] = []
      # Get the list of ENNIs Note that here we use the node name NOT the loopback IP in case there is a fake ENNI and the IP address is not filled out
      if @obj.is_multi_chassis
        ennis = @obj.ports.collect{|port| {"displayedName" => @obj.stats_displayed_name, "monitoredObjectSiteId" => port.node.name}}
    Rails.logger.debug "Serge: stats_get_enni_data  4"
      else
        ennis = [{"displayedName" => @obj.stats_displayed_name, "monitoredObjectSiteId" => @obj.ports.first.node.name}]
    Rails.logger.debug "Serge: stats_get_enni_data  5 '#{@obj.ports.first.node.name}'"
    pp @obj.ports.first.node
      end
      # For Lags get the per port data as well
      if @obj.is_lag
        ennis += @obj.ports.collect{|port| {"displayedName" => port.stats_displayed_name, "monitoredObjectSiteId" => port.node.name}}
    Rails.logger.debug "Serge: stats_get_enni_data  6"
      end
      return_data = {}
      ennis.each do |enni|
        displayed_name = enni["displayedName"]
        node_name = enni["monitoredObjectSiteId"]
        # Get Lag/port to display
        dn = displayed_name
        if @obj.stats_displayed_name == displayed_name
          dn = displayed_name
        else
          dn = "#{node_name} #{displayed_name}"
        end
        return_data[dn] = {}
        return_data[dn][:klass] = @obj.class
        return_data[dn][:id] = @obj.id
        return_data[dn][:data] = {}
        return_data[dn][:data][cos_name] = {}
        return_data[dn][:data][cos_name][:cos_id] = nil
        counters.keys.each {|counter_type| return_data[dn][:data][cos_name][counter_type] = []}
        # get the CIR, EIR and Rate - Only for the ENNI itself not the ports
        if @obj.stats_displayed_name == displayed_name
          return_data[dn][:sla] = {}
          return_data[dn][:sla][:cir] = (@obj.rate * @obj.cir_limit)/100.0
          return_data[dn][:sla][:eir] = (@obj.rate * @obj.eir_limit)/100.0
          return_data[dn][:sla][:line_rate] = @obj.rate
          # Use in the Ingress or Egress KPI ? I think ENNI are the same in both directions....
          return_data[dn][:sla][:utilization_high_threshold] = @obj.get_utilization_threshold_mbps(:ingress, :major)
          return_data[dn][:sla][:utilization_low_threshold] = @obj.get_utilization_threshold_mbps(:ingress, :minor)
        end
      end
      return return_data
    end
    # massage the displayed name to add the site_name if it's port information for a Lag
    data.collect! do |record|
      if @obj.stats_displayed_name == record["displayedName"]
        record
      else
        record.merge!({"displayedName" => "#{ip_to_name[record["monitoredObjectSiteId"]]} #{record["displayedName"]}"})
      end
    end
    return_data = {}
    data.collect {|record| record["displayedName"]}.uniq.each do |displayed_name|
      return_data[displayed_name] = {}
      return_data[displayed_name][:klass] = @obj.class
      return_data[displayed_name][:id] = @obj.id
      return_data[displayed_name][:data] = {}
      return_data[displayed_name][:data][cos_name] = {}
      return_data[displayed_name][:data][cos_name][:cos_id] = nil
      counters.keys.each {|counter_type| return_data[displayed_name][:data][cos_name][counter_type] = []}
      # get the CIR, EIR and Rate - Only for the ENNI itself not the ports
      if @obj.stats_displayed_name == displayed_name
        return_data[displayed_name][:sla] = {}
        return_data[displayed_name][:sla][:cir] = (@obj.rate * @obj.cir_limit)/100.0
        return_data[displayed_name][:sla][:eir] = (@obj.rate * @obj.eir_limit)/100.0
        return_data[displayed_name][:sla][:line_rate] = @obj.rate
        # Use in the Ingress or Egress KPI ? I think ENNI are the same in both directions....
        return_data[displayed_name][:sla][:utilization_high_threshold] = @obj.get_utilization_threshold_mbps(:ingress, :major)
        return_data[displayed_name][:sla][:utilization_low_threshold] = @obj.get_utilization_threshold_mbps(:ingress, :minor)
      end
    end
    counters.each do |counter_type, counters|
      ingress_counters = counters[:ingress_counters]
      egress_counters = counters[:egress_counters]
      # For MultiChassis need to add up the data from each chassis to make one value
      time = (data.first['timeCaptured'].to_i) if data.size > 0
      write_last_result = false
      enni_data =  Hash.new {|h,k| h[k] = {:deltaIngress => 0, :deltaEgress => 0}}
      data.each_index do |i|
        idata = data[i]
        # Add counters per enni
        ingress = ingress_counters.inject(0) {|total, counter| total = total +  idata[counter].to_i}
        egress = egress_counters.inject(0) {|total, counter| total = total +  idata[counter].to_i}
        if (idata["timeCaptured"].to_i >= time + collection_period) || i == data.size-1
          # Its the last result and it's associated with the previous result so add it
          if i == data.size-1 && (idata["timeCaptured"].to_i < time + collection_period)
            enni_data[idata["displayedName"]][:deltaIngress] += ingress
            enni_data[idata["displayedName"]][:deltaEgress] += egress
          elsif i == data.size-1 && (idata["timeCaptured"].to_i >= time + collection_period)
            # The last result should be a separate result so add the current and set flag to
            # perform the last write at the end of the loop
            write_last_result = true
          end
          enni_data.each {|display_name,data1|
            #Rails.logger.debug "Added new data #{Time.at(time)} #{display_name} #{data1[:deltaIngress]} #{data1[:deltaEgress]}"
            return_data[display_name][:data][cos_name][counter_type] << {:timeCaptured => time , :deltaIngress => data1[:deltaIngress], :deltaEgress => data1[:deltaEgress]}
          }
          time = idata["timeCaptured"].to_i
          enni_data =  Hash.new {|h,k| h[k] = {:deltaIngress => 0, :deltaEgress => 0}}
          enni_data[idata["displayedName"]][:deltaIngress] = ingress
          enni_data[idata["displayedName"]][:deltaEgress] = egress
        else
          enni_data[idata["displayedName"]][:deltaIngress] += ingress
          enni_data[idata["displayedName"]][:deltaEgress] += egress
        end
      end
      if write_last_result
        enni_data.each {|display_name,data1|
          Rails.logger.debug "Added new data #{Time.at(time)} #{display_name} #{data1[:deltaIngress]} #{data1[:deltaEgress]}"
          return_data[display_name][:data][cos_name][counter_type] << {:timeCaptured => time , :deltaIngress => data1[:deltaIngress], :deltaEgress => data1[:deltaEgress]}
        }
      end
    end
    return return_data
  end


  public
  def compute_enni_utilization_exceptions
    time_name  = 'timeCaptured'
    table_name = 'ehealth_util'
    top_columns = {METRIC_NAMES[:octets_count] => 'SUM'}
    # get list of ENNIs to get the stats from
    name = @obj.stats_displayed_name
    ennis = []
    if @obj.is_multi_chassis
      @obj.ports.each do |port|
        ennis << [
          "`displayedName`='#{name}'",
          "`monitoredObjectSiteId`='#{port.node.loopback_ip}'" ] unless port.node.loopback_ip.nil? or port.node.loopback_ip.empty? or name.nil? or name.empty?
    Rails.logger.debug "Serge: compute_enni_utilization_exceptions  1"
      end
    else
      ennis << [
        "`displayedName`='#{name}'",
        "`monitoredObjectSiteId`='#{@obj.ports.first.node.loopback_ip}'"] unless @obj.ports.first.node.loopback_ip.nil? or @obj.ports.first.node.loopback_ip.empty? or name.nil? or name.empty?
    Rails.logger.debug "Serge: compute_enni_utilization_exceptions  2"
    end
    where = ennis.uniq
    time_periods_native = get_time_periods_native
    group_by = nil
    order_by = nil
    nms_id = @obj.get_network_manager.id
    [:ingress, :egress].each do |direction|
      selected_columns = (direction == :ingress) ? { METRIC_NAMES[:octets_count] => 'SUM(`receivedTotalOctets_delta`)'}  : { METRIC_NAMES[:octets_count] => 'SUM(`transmittedTotalOctets_delta`)'}
      [  :major, :minor ].each do |severity|
        results = nil
        unless ennis.nil? or ennis.empty?
          time_increment_secs = @obj.get_utilization_errored_period_delta_time_secs
          time_increment_native = convert_to_native_time_units time_increment_secs
          count_condition_name=nil
          selected_columns.each{ |k,v| count_condition_name=k; break } unless selected_columns.nil?
          condition_s = "`#{count_condition_name}` > #{@obj.get_utilization_errored_period_threshold direction, severity}"
          count_condition =  [ condition_s ]
          results = DataArchiveIf.query_aggregate_and_count table_name, time_name, where, group_by, order_by, selected_columns, time_periods_native, time_increment_native, count_condition, top_columns
        end
        unless results.nil?
          count = results[0]['COUNT']
          time_over = count * time_increment_secs
          octets_over = results[0][METRIC_NAMES[:octets_count]]
          rate_over = octets_over *8  / time_over / 1e6  unless 0==time_over
          max_errored_count = @obj.get_utilization_errored_period_max_errored_count  direction, severity
          if count > max_errored_count
            ex = ExceptionUtilizationEnni.find( :first, :conditions => [ "enni_new_id=? AND direction=? AND time_declared BETWEEN ? AND ?",@obj.id, direction.to_s, @start_time.to_i, @end_time.to_i ])
            ex = ExceptionUtilizationEnni.new if ex.nil?
            ex.time_declared  = @end_time.to_i
            ex.enni_new_id = @obj.id
            ex.severity = severity
            ex.value =  time_over
            ex.direction = direction
            ex.time_over = time_over
            ex.average_rate_while_over_threshold_mbps = rate_over
            ex.save
            output = <<EOF
 ExceptionUtilizationEnni.create :time_declared  =>#{ @end_time.to_i},
:enni_new_id => #{@obj.id},
:severity => #{ex.severity},
:value => #{count},
:direction => "#{direction}",
:time_over => #{time_over},
:average_rate_while_over_threshold_mbps => #{rate_over}
valid?(#{ex.valid?})
EOF
            puts output
            break if severity == :major # stop here no need to do the :minor
          end
        end
      end
    end
  end


  #TODO find a way to combine data for multi-chassis ports into a single row.
  def self.compute_top_talkers_enni time_periods_secs
    columns = {
      "ingress" => "`receivedTotalOctets_delta`",
    #  "egress"  => "`transmittedTotalOctets_delta`",
    }
    class_name = "ehealth_util"
    # construct the list of ennis we are interested in.
    # TODO: find a more scalable way to achieve the same results.
    ennis = []
    EnniNew.find_each do |enni|
      if enni.get_prov_state.is_a?(ProvLive)
        name = enni.stats_displayed_name
        enni.ports.each do |port|
          site = port.node.loopback_ip
          unless site.nil? or site.empty? or name.nil? or name.empty?
            ennis << ["`monitoredObjectSiteId`='#{site}'" , "`displayedName`='#{name}'"]
            #        puts "`displayedName`='#{name}' `monitoredObjectSiteId`='#{site}', cenx_name(#{enni.cenx_name}), id(#{enni.id}), is_monitored(#{enni.is_connected_to_monitoring_port})"
          end
        end
      end
    end
    where = ennis.uniq
    results = SamStatsArchive.compute_top_talkers time_periods_secs, class_name, columns, where
    enni_data = []
    unless results.nil? or results.empty?
      # Convert "monitoredObjectSiteId"/"displayedName" to enni id.
      results.each do |row|
        site = row["monitoredObjectSiteId"]
        name = row["displayedName"]
        begin
          # TODO: devise a more efficient way to get the enni_id as this is very expensive.
          enni_id = Node.find_by_loopback_ip(site).ports.find_all {|port|  name==port.demarc.stats_displayed_name unless port.demarc.nil?}.first.demarc_id
          enni_data << {:enni_id=>enni_id, :monitoredObjectSiteId => site, :displayedName => name, :octets_count => row["octets_count"], :direction => "ingress"==row[:direction] ? "ingress" : "egress"}
          #   pp enni_data
        rescue Exception => e
          SW_ERR "error: site='#{site}' name='#{name}' : #{e.message} "
        end
      end
    end
    return enni_data
  end

  protected

  def convert_to_native_time_units the_time
    return the_time
  end


  def get_time_periods_native
    # returns time in msecs
    return @time_periods_secs
  end


end

