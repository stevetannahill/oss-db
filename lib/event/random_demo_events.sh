#!/bin/bash
#set -x

TIME_BETWEEN_FAILURES=300
TIME_FOR_OUTAGE=30
#EVENT_CLIENT="event_client.rb"
EVENT_CLIENT="/cenx/oss-db/current/lib/event/event_client.rb"

MOVCS_SIZE=16
MOVCS=(
        Telus:161:90:37
        Telefonica:163:91:37
        Telus:167:93:38
        Telefonica:169:94:38
        Verizon:173:96:39
        Telefonica:175:97:39
        Verizon:179:99:40
        Telefonica:181:100:40
        Telefonica:185:102:41
        Telus:187:103:41
        Telefonica:191:105:42
        Telus:193:106:42
        Telefonica:197:108:43
        Verizon:199:109:43
        Telefonica:203:111:44
        Verizon:205:112:44
      )

while ((1)); do
  # pick an ovc
  ((MOVC_INDEX=RANDOM%MOVCS_SIZE))
  MOVC=${MOVCS[$MOVC_INDEX]}

  # parse out the ID fields
  MOVC_EPID=$(echo ${MOVC} | cut -d ":" -f2)
  MOVC_ID=$(echo ${MOVC} | cut -d ":" -f3)
  MOVC_EVCID=$(echo ${MOVC} | cut -d ":" -f4)

  # bring the ovc down
  ruby ${EVENT_CLIENT} --id ${MOVC_EPID} --type OvcEndPointUni --event Failed --details "CCM Failure."
  ruby ${EVENT_CLIENT} --id ${MOVC_ID} --type MemberOvc --event Failed --details "Endpoint down."
  ruby ${EVENT_CLIENT} --id ${MOVC_EVCID} --type Evc --event Failed --details "OVC down."

  # leave them down for a bit
  sleep ${TIME_FOR_OUTAGE}

  # bring it back up
  ruby ${EVENT_CLIENT} --id ${MOVC_EPID} --type OvcEndPointUni --event Ok
  ruby ${EVENT_CLIENT} --id ${MOVC_ID} --type MemberOvc --event Ok
  ruby ${EVENT_CLIENT} --id ${MOVC_EVCID} --type Evc --event Ok

  # wait before starting again
  sleep ${TIME_BETWEEN_FAILURES}
done
