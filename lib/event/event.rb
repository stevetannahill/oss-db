class Event
  include DRbUndumped

  def initialize(channels, tag, objid, type, date, notification, details)
    @event_tag = Event.blank?(tag) ? "Untagged" : tag
    @entity_id = objid
    @entity_type = type
    @event_date = date
    @event_notification = notification
    @details = details
    @channels = channels
  end
  
  attr_reader :channels, :event_tag, :entity_id, :entity_type, :event_date, :event_notification, :details
  attr_accessor :sequence_tag

  def to_json
    { "origin_tag" => @event_tag,
      "entity_id" => @entity_id,
      "entity_type" => @entity_type,
      "datestamp" => @event_date,
      "notification_type" => @event_notification,
      "details" => @details,
      "sequence_tag" => @sequence_tag,
      "channels" => JSON.generate(@channels) }
  end

  def to_s
    "#{@event_tag} : #{@sequence_tag} : #{Time.at(@event_date).to_s} : #{@entity_id} : #{@entity_type} : #{@event_notification} : #{@details} : #{@channels.inspect}"
  end

  def describe
    "#{@event_tag} (\"#{@event_notification}\" for #{@entity_type}-#{@entity_id} at #{@event_date}: #{@details}), sequence #{@sequence_tag}, on channels #{@channels.inspect}"
  end
  
  def self.blank?(input)
    input.nil? || input == ''
  end
  
end


class EventServerApi
  include DRbUndumped

  def initialize(q, logger)
    @evt_q = q
    @logger = logger
  end
end

class EventReporter < EventServerApi
  def report_demo(channels,
                  tag,
                  entity_id,
                  entity_type,
                  event_date,
                  event_notification,
                  details)
    evt = Event.new(channels, tag, entity_id, entity_type, event_date, event_notification, details)
    @logger.debug("Received report #{{"tag"=>tag,"entity_id"=>entity_id,"entity_type"=>entity_type,"event_date"=>event_date,"event_notification"=>event_notification,"details"=>details}.inspect}")

    @evt_q.push_event(evt)
  end
end


class EventPublisher < EventServerApi
  def publish(session,host_name)
    while @evt_q.queue_has_events?
      evt = @evt_q.pop_event
      @logger.info("Processing #{evt.describe}")
      post_args = evt.to_json
      post_args["host_name"] = host_name
      resp = session.post("/", post_args)
      if resp.status == 200
        @logger.info("Published event #{evt.describe}")
      else
        @logger.error("Could not publish event, response:#{resp.inspect}")
      end
    end
  end
end


class EventQueue
  def initialize(logger)
    @logger = logger
    @queue = []
    @sequence_tag = Time.now.to_i
  end

  def queue_has_events?
    0 < @queue.size
  end

  def queue_size
    @queue.size
  end

  def push_event(evt)
    @sequence_tag += 1
    evt.sequence_tag = @sequence_tag

    depth = @queue.push(evt).index(evt)
    @logger.debug("Added event #{evt.describe} to the queue in position #{depth}.")
    return @queue.index(evt)
  end

  def pop_event
    evt = @queue.slice!(0)
    @logger.debug("Removed event #{evt.describe} from the queue.")
    return evt
  end
end
