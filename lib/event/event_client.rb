#!/usr/bin/ruby -w
#
# Event Client, using DRb
require 'drb'
require "optparse"

entity_types = [ "Path",
                 "OnNetOvc",
                 "OnNetOvcEndPointEnni",
                 "EnniNew",
                 "OvcEndPointEnni",
                 "OffNetOvc",
                 "OvcEndPointUni",
                 "Uni"
               ]

event_notifs = [ "Internal Error",
                 "Out of Service",
                 "Maintenance",
                 "Not Monitored",
                 "Ok",
                 "Warning",
                 "Failed"
               ]

nouns = [ "warthog", "hedgehog", "badger", "drake", "eft",
          "fawn", "gibbon", "heron", "ibex", "jackalope",
          "koala", "lynx", "meerkat", "narwhal"
        ]

adjectives = [ "warty", "hoary", "breezy", "dapper", "edgy",
               "feisty", "gutsy", "hardy", "intrepid", "jaunty",
               "karmic", "lucid", "maverick", "natty"
             ]

# Process command line args
opt = {}
optparse = OptionParser.new do |opts|
  opts.banner = "Usage #{File.basename(__FILE__)} [opt]"

  opts.separator ""
  opts.separator "Specific options:"

  opt[:flood] = 1
  opts.on( '--flood SIZE', 'The number of events to send',
                           '(defaults to 1)' ) do |size|
    opt[:flood] = size.to_i
  end

  opt[:id] = nil
  opts.on( '--id ID', 'The ID of the object' ) do |id|
    opt[:id] = id
  end

  opt[:type] = nil
  opts.on( '--type TYPE', 'The type of object',
                          "Valid types are #{entity_types.inspect}") do |type|
    opt[:type] = type
  end

  opt[:event] = nil
  opts.on( '--event TYPE', 'The notification type of the event',
                           "Valid types are #{event_notifs.inspect}") do |type|
    opt[:event] = type
  end

  opt[:details] = ""
  opts.on( '--details DETAILS', 'The detail message in the event') do |details|
    opt[:details] = details
  end

  opt[:random] = false
  opts.on( '--random', 'Generate random values for unassigned fields') do
    opt[:random] = true
  end
end

begin
  optparse.parse!
rescue OptionParser::InvalidOption => e
  puts e
  puts optparse
  exit 1
end


DRb.start_service
evt_reporter = DRbObject.new nil, "druby://localhost:3939"


for i in (1..opt[:flood].to_i)

  evt_tag = "EVT#{i}"
  evt_objid = opt[:id]
  evt_objtype = opt[:type]
  evt_notif = opt[:event]
  evt_details = opt[:details]

  # reset to random values if called for
  if opt[:random]
    evt_objid = rand(10000) if evt_objid.nil?
    evt_objtype = entity_types[rand(entity_types.size)] if evt_objtype.nil?
    evt_notif = event_notifs[rand(event_notifs.size)] if evt_notif.nil?
    evt_details = "The #{nouns[rand(nouns.size)]} is #{adjectives[rand(adjectives.size)]}." if evt_details.nil?
  end

  # send event, and collect its position in the queue
  begin
    depth = evt_reporter.report_demo([1,2,3],
                                     evt_tag,
                                     evt_objid,
                                     evt_objtype,
                                     Time.now.to_i,
                                     evt_notif,
                                     evt_details)
  rescue DRb::DRbConnError => e
    puts e
    puts "Can not send to Event Server."
    exit 1
  end
  puts "#{Time.now}: Pushed event #{evt_tag}, position #{depth} in queue."
end
