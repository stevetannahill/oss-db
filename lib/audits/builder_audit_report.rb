
class BuilderAuditReport < Report
    
  def self.generate_report(schedule_id = nil)
    if schedule_id
      sr = Schedule.find(schedule_id)
      rep = BuilderAuditReport.create(:schedule => sr)
      summary = BuilderLog.summary(false, rep.report_period_start, rep.report_period_end)
      
      rep.critical_exceptions = summary["Failed"]
      rep.minor_exceptions = summary["Not Currently Supported"] 
      rep.save
      
      if sr.email_distribution
        report_txt = "#{summary.collect {|k,v| "#{k.to_s.capitalize}: #{v}"}.join("\n")}"
        report_txt << "\nCDB Automated Build Center URL: #{URI.join("#{CdbMailer.cdb_url}", "/automated_build_center/").to_s}\n"
        attachments = {}
        if sr.formats["csv"]
          csv = BuilderLog.csv("%", "%", rep.report_period_start, rep.report_period_end)
          oem_report_csv = BuilderLog.oem_report_csv(rep.report_period_start, rep.report_period_end)
          attachments['builder_results.csv'] = {:content => csv}
          attachments['oem_report.csv'] = {:content => oem_report_csv}
        end
        CdbMailer.scheduled_audit(rep, report_txt, attachments).deliver
      end
    end
  end
  
  def update_report
    # Don't support
  end

end