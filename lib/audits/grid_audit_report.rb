
class GridAuditReport < Report
  after_initialize :setup_vars
  attr_accessor :results, :output_limit
  E_TO_I_KEY = "Eve.grid.gci.e_to_i"
  I_TO_E_KEY = "Eve.grid.gci.i_to_e"
  LKP_KEY = "lkp:*:circuit"
  OUTPUT_LIMIT = 40

  SECTION_TITLES = {  grid_audit_valid: "Check validity of Grid's IDs", grid_audit_abc: "Check Grid circuits vs ABC circuits", grid_audit_da: "Check Grid's grid ids vs DA's grid ids",
                      grid_audit_lkp: "Check Grid's grid ids vs LKP's grid ids", grid_audit_samples: "Check samples missing from circuits", grid_audit_avail: "Check availability CDB vs DA" }

  
    
  def self.generate_report(schedule_id = nil, output_limit = OUTPUT_LIMIT, availability= false, start_date= Time.now - 1.month)
    if schedule_id
      sr = Schedule.find(schedule_id)
      rep = GridAuditReport.create(:schedule => sr)
      start_date = rep.report_period_start
    else
      rep = GridAuditReport.new
    end
    rep.results = []
    rep.output_limit = output_limit
    
    rep.check_id_validity
    rep.check_grid_vs_build_logs
    rep.check_grid_vs_da
    rep.check_grid_vs_lkp
    rep.check_samples_lost
    rep.check_for_mismatches(start_date) if availability
    
    if schedule_id
      rep.critical_exceptions = rep.results.size
      rep.save
      
      if sr.email_distribution
        report_txt = "Total Errors:#{rep.results.size}\n\n" + rep.results.collect {|result| result.collect {|k,v| "#{k.to_s.capitalize}: #{v}"}.join("\n")}.join("\n===\n")
        attachments = {}
        CdbMailer.scheduled_audit(rep, report_txt, attachments).deliver
      end
    end
    return rep.results
  end
  
  def update_report
    # Don't support
  end

  def check_id_validity
    no_id_circuits = []
    redis_missing_circuits = []
    incorrect_circuit_ids = []
    no_ref_cicuits = []
    cdb_missing_circuits = []
    grid_id_mismatches = []
    missing_last_hop_circuits = []
    SpirentCosTestVector.find_in_batches(:include => {:cos_end_point => :segment_end_point}) do |ctvs|
      ctvs.each do |ctv|
        # Only check Live CTV's
        next if ctv.cos_end_point.segment_end_point.prov_name != "Live"
    
        if !ctv.circuit_id.blank?
          
          grid_grid_id = @grid_redis.hget(E_TO_I_KEY, ctv.circuit_id)
          if !ctv.grid_circuit_id
            no_id_circuits << [ctv, grid_grid_id]
          elsif !grid_grid_id
            redis_missing_circuits << ctv             
          elsif ctv.grid_circuit_id != grid_grid_id.to_i
            incorrect_circuit_ids << [ctv, grid_grid_id]    
          elsif !ctv.ref_circuit_id.blank?
            if !ctv.ref_grid_circuit_id
              grid_ref_grid_id = @grid_redis.hget(E_TO_I_KEY, ctv.ref_circuit_id)
              no_ref_cicuits << [ctv, grid_ref_grid_id]                 
            else
              # Check that the ref_ctv exists by looking it up by ref_circuit_id & the grid id's match
              ref_ctv = CosTestVector.find_by_circuit_id(ctv.ref_circuit_id)
              if !ref_ctv
                cdb_missing_circuits << ctv
              elsif ref_ctv.grid_circuit_id != ctv.ref_grid_circuit_id
                grid_id_mismatches << [ctv, ref_ctv]                   
              end
            end
            if !ctv.last_hop_circuit_id.blank?
              last_hop_ctv = CosTestVector.find_by_circuit_id(ctv.last_hop_circuit_id)
              if !last_hop_ctv
                missing_last_hop_circuits << ctv
              end
            end
          end
        end
      end
    end
    @results << no_grid_circuit_id(no_id_circuits) unless no_id_circuits.empty?
    @results << missing_grid_circuit_id_in_grid(redis_missing_circuits)  unless redis_missing_circuits.empty?
    @results << incorrect_grid_circuit_id(incorrect_circuit_ids) unless incorrect_circuit_ids.empty?
    @results << no_ref_grid_circuit_id(no_ref_cicuits)  unless no_ref_cicuits.empty?
    @results << missing_xxx_circuit_id_in_cdb(cdb_missing_circuits, :ref_circuit_id)  unless cdb_missing_circuits.empty?
    @results << mismatch_of_grid_ids_between_ctv_and_ref_ctv(grid_id_mismatches)  unless grid_id_mismatches.empty?
    @results << missing_xxx_circuit_id_in_cdb(missing_last_hop_circuits, :last_hop_circuit_id)  unless missing_last_hop_circuits.empty?
  end
  
  def check_grid_vs_lkp
    grid_ids = @grid_redis.hkeys(I_TO_E_KEY).collect!{|gid| gid.to_i}
    lkp_gids = @lkp_redis.keys(LKP_KEY).collect!{|key| key.split(":")[1].to_i}
    #an alternate to .keys() (0 .. grid_redis.get("Eve.grid.gci.grid_circuit_id_count").to_i).each{|gid| a << gid if lkp_redis.exists("lkp:#{gid}:circuit")}
    
    gids_in_grid_but_not_in_lkp = (grid_ids - lkp_gids)
    gids_in_lkp_but_not_in_grid = (lkp_gids - grid_ids)

    if !gids_in_lkp_but_not_in_grid.empty?
      @results << circuits_in_xxx_not_in_grid(gids_in_lkp_but_not_in_grid, "LKP")
    end

    if !gids_in_grid_but_not_in_lkp.empty?
      @results << circuits_in_grid_not_in_xxx(gids_in_grid_but_not_in_lkp, "LKP")
    end
  end
  
  def check_grid_vs_da
    grid_ids = @grid_redis.hkeys(I_TO_E_KEY).collect!{|gid| gid.to_i}
    da_grid_ids = DataArchiveIf.get_all_grid_ids("ethframedelaytest")
    if da_grid_ids 
      gids_in_grid_but_not_in_da = grid_ids - da_grid_ids
      gids_in_da_but_not_in_grid = da_grid_ids - grid_ids

      if !gids_in_da_but_not_in_grid.empty?
        @results << circuits_in_xxx_not_in_grid(gids_in_da_but_not_in_grid, "DA")
      end

      if !gids_in_grid_but_not_in_da.empty?
        @results << circuits_in_grid_not_in_xxx(gids_in_grid_but_not_in_da, "DA")
      end
    else
      @results << failed_to_get_data_archive_data
    end
  end  
  
  def check_grid_vs_build_logs
    # Find the circuits which ABC has attempted to build vs the circuits that Grid has seen
    abc_build_circuits = BuilderLog.pluck(:name)
    grid_circuits = @grid_redis.hkeys(E_TO_I_KEY)
    circuits_that_abc_has_not_attempted_to_build = grid_circuits-abc_build_circuits
    circuits_that_abc_has_attempted_to_build_but_unknown_in_grid = abc_build_circuits-grid_circuits
    
    if !circuits_that_abc_has_not_attempted_to_build.empty?
      @results << circuits_that_abc_has_not_attempted_to_build(circuits_that_abc_has_not_attempted_to_build)
    end

    if !circuits_that_abc_has_attempted_to_build_but_unknown_in_grid.empty?
      @results << circuits_that_abc_has_attempted_to_build_but_unknown_in_grid(circuits_that_abc_has_attempted_to_build_but_unknown_in_grid)
    end

    # Check the circuits grid_ids in ABC against Grid
    abc_build_gids = BuilderLog.pluck(:grid_id)
    grid_gids = @grid_redis.hkeys(I_TO_E_KEY).collect{|gid| gid.to_i}
    gids_in_grid_but_not_abc = grid_gids-abc_build_gids
    gids_in_abc_but_not_in_grid = abc_build_gids-grid_gids
    
    if !gids_in_abc_but_not_in_grid.empty?
      @results << circuits_in_xxx_not_in_grid(gids_in_abc_but_not_in_grid, "ABC", true)
    end

    if !gids_in_grid_but_not_abc.empty?
      @results << circuits_in_grid_not_in_xxx(gids_in_grid_but_not_abc, "ABC")
    end       
  end
  
  def check_samples_lost
    t = Time.at(Time.now.to_i).utc
    circuits_missing_samples = {}
    circuits = @lkp_redis.keys("lkp:*:#{Time.utc(t.year,t.month).to_i}:tracking")
    circuits.each do |circuit|
      missed = @lkp_redis.hget(circuit, 'samples_lost')
      if missed.to_i > 0
        circuits_missing_samples[circuit.split(":")[1].to_i] = {  :cname => @grid_redis.hget(I_TO_E_KEY, circuit.split(":")[1].to_i), :missing => missed}
      end
    end
    if !circuits_missing_samples.empty?
      @results << circuits_samples_lost(circuits_missing_samples)
    end
  end

  def check_for_mismatches(start_date)
    start_date = start_date.to_i
    total_cdb_cleaned = cdb_cleaned_dup = cdb_cleaned_identical = 0
    total_mismatched = total_good = cdb_count = da_count = cdb_dup = cdb_one = da_dup = da_sync = 0
    list = {}
    CosTestVector.find_each do |ctv|
      entry = {}
      entry[:gid] = ctv.grid_circuit_id
      entry[:circuit] = ctv.circuit_id
      type = ""
      cdb_records = ctv.alarm_histories[0].alarm_records
      if cdb_records
        cdb_records.select! do |record| 
          record[:time_of_event] >= (start_date * 1000)
        end
      else
        cdb_records = []
      end

      cdb_records_clean = []
      last = {}
      dup = ident = false
      cdb_records.each do |record|
        if last[:state] == record[:state] && ((last[:time] + (300 * 1000)) == record[:time_of_event] || last[:time] == record[:time_of_event])
          if last[:time] == record[:time_of_event]
            ident = true
          else
            dup = true
          end
        else
          cdb_records_clean << record
          last[:state] = record[:state]
          last[:time] = record[:time_of_event]
        end
      end

      if dup || ident
        total_cdb_cleaned = total_cdb_cleaned + 1
        if dup
          cdb_cleaned_dup = cdb_cleaned_dup + 1
          list["cdb_cleaned_dup"] = [] if !list["cdb_cleaned_dup"] #init
          list["cdb_cleaned_dup"] << entry
        else
          cdb_cleaned_identical = cdb_cleaned_identical + 1
          list["cdb_cleaned_ident"] = [] if !list["cdb_cleaned_ident"] #init
          list["cdb_cleaned_ident"] << entry
        end
      end

      cdb_size = cdb_records_clean.size

      class_name = 'ethframedelaytest'
      columns = ['grid_circuit_id', 'time_start', 'available']
      where = [["`grid_circuit_id` = '#{ctv.grid_circuit_id}'"]]
      limit = nil
      results = DataArchiveIf.last_availability_for( class_name, 'time_start', columns, where, limit)
      if results
        results.select!{ |result| result["time_start"] >= start_date}
      else 
        results = []
      end

      if results.size != cdb_size
        entry[:da] = results.size
        entry[:cdb] = cdb_size
        type = "mismatch"
        total_mismatched = total_mismatched + 1
        if results.size > cdb_size
          type = type + " da"
          da_count = da_count + 1
          if da_has_duplicates?(results)
            da_dup = da_dup + 1
            type = type + " duplicates"
          else
            da_sync = da_sync + 1
            type = type + " sync"
          end
        else
          type = type + " cdb"
          cdb_count = cdb_count + 1

          if cdb_has_duplicates?(cdb_records_clean)
            cdb_dup = cdb_dup + 1
            type = type + " duplicates"
          elsif cdb_size - 1 == results.size
            cdb_one = cdb_one + 1
            type = type + " off-by-one"
          else
            type = type + " other"
          end
        end
        list[type] = [] if !list[type] #initialize on first occurence
        list[type] << entry
      else
        total_good = total_good + 1
      end
    end
    output_mismatches(list)
  end

  private

  def setup_vars
    grid_app = Eve::Application.new(:config => "#{Rails.root}/config/grid.yml", :observer => true)
    GridApp.eve = grid_app
    GridApp.eve.run([])
    
    @grid_redis = grid_app.redis
    @lkp_redis = LKP.redis
  end
  
  def rails_console_cmd
    "bundle exec rails c"
  end
  
  def no_grid_circuit_id(circuits)
    if @output_limit > 0
      limited_circuits = circuits[0..@output_limit]
    else
      limited_circuits = circuits
    end
    limited_circuits.map!{|ctv, grid_circuit_id| "#{ctv.circuit_id}:#{grid_circuit_id}"}
    {:error => "There are #{circuits.size} CTVs the do not have a grid_circuit_id: (list limited to #{@output_limit})\n#{limited_circuits.join("\n")}",
     :action => "The circuits are listed in the form [circuit_id]:[grid_id]\n" \
                "You can assign these grid_ids to their CTVs by following these commands:\n" \
                "#{rails_console_cmd}\n" \
                "ctv = CosTestVector.find_by_circuit_id([circuit_id])\n" \
                "ctv.grid_circuit_id = [grid_id]; ctv.save"
    }
  end

  def missing_grid_circuit_id_in_grid(circuits)
    if @output_limit > 0
      limited_circuits = circuits[0..@output_limit]
    else
      limited_circuits = circuits
    end
    limited_circuits.map!{|ctv| "#{ctv.circuit_id}:#{ctv.grid_circuit_id}"}
    {:error => "There are #{circuits.size} CTVs that have grid_circuit_ids in CDB but no circuit_grid_id in grid: (list limited to #{@output_limit})\n#{limited_circuits.join("\n")}",
     :action => "The circuits are listed in the form [circuit_id]:[grid_id]\n" \
                "Either the CDB database has been replaced or the Grid redis database has been replaced/erased"
    }
  end
  
  def incorrect_grid_circuit_id(circuits)
    if @output_limit > 0
      limited_circuits = circuits[0..@output_limit]
    else
      limited_circuits = circuits
    end
    limited_circuits.map!{|ctv, grid_circuit_id| "#{ctv.circuit_id}: (#{ctv.grid_circuit_id} || #{grid_circuit_id}"}
    {:error => "There are #{circuits.size} CTVs that have mismatched grid_circuit_ids: (list limited to #{@output_limit})\n#{limited_circuits.join("\n")}",
     :action => "The circuits are listed in the form [circuit_id]: ([CDB grid_id] || [Grid grid_id])\n" \
                "Determine which Grid id is correct (most likely the Grid grid_circuit_id is the correct one). Assuming it's the latter\n" \
                "#{rails_console_cmd}\n" \
                "ctv = CosTestVector.find_by_circuit_id([circuit_id])\n" \
                "ctv.grid_circuit_id = [Grid grid_id]; ctv.save"
    }
  end
  
  def no_ref_grid_circuit_id(circuits)
    if @output_limit > 0
      limited_circuits = circuits[0..@output_limit]
    else
      limited_circuits = circuits
    end
    limited_circuits.map!{|ctv, grid_ref_grid_id| "Circuit: #{ctv.circuit_id}, ref_grid_circuit_id: grid_circuit_id"}
    {:error => "There are #{circuits.size} CTVs that do not have ref_grid_circuit_ids however they did have ref_circuit_ids which allowed the retireval of the ref_grid_circuit_id",
     :action => "Set the CTV ref_grid_circuit id by following these commands:\n" \
                 "#{rails_console_cmd}\n" \
                 "ctv = CosTestVector.find_by_circuit_id([Circuit])\n" \
                 "ctv.ref_grid_circuit_id = [ref_grid_circuit_id]; ctv.save"
    }
  end
  
  def missing_xxx_circuit_id_in_cdb(circuits, missing_attribute)
    if @output_limit > 0
      limited_circuits = circuits[0..@output_limit]
    else
      limited_circuits = circuits
    end
    limited_circuits.map!{|ctv| "Circuit: #{ctv.circuit_id}, Missing: #{ctv.send(missing_attribute)}"}
    {:error => "There are #{circuits.size} CTVs that have #{missing_attribute} in CDB but no corresponding circuit_id in CDB",
     :action => "It has not been created in CDB"
    }
  end
  
  def mismatch_of_grid_ids_between_ctv_and_ref_ctv(circuits) 
    if @output_limit > 0
      limited_circuits = circuits[0..@output_limit]
    else
      limited_circuits = circuits
    end
    limited_circuits.map!{|ctv, ref_ctv| "Circuit:#{ctv.circuit_id}, Ref_grid:#{ctv.ref_grid_circuit_id}, ref_ctv:#{ref_ctv.circuit_id}:#{ref_ctv.grid_circuit_id}"}
    {:error => "There are #{circuits.size} CTVs who's ref_grid_circuit_id does not match their reference CTV's grid_circuit_id: (list limited to #{@output_limit})\n#{limited_circuits.join("\n")}",
     :action => "The circuits are listed in the form [circuit_id], [ref_grid_circuit_id], (ref)[circuit_id]:[grid_circuit_id])\n" \
                "Set the ref_grid_circuit_id in the originating CTV to the (ref)[grid_circuit_id]\n" \
                "#{rails_console_cmd}\n" \
                "ctv = CosTestVector.find_by_circuit_id([circuit_id])\n" \
                "ctv.ref_grid_circuit_id = (ref)[grid_circuit_id]; ctv.save"
    }
  end
  
  def circuits_that_abc_has_not_attempted_to_build(circuits)
    if @output_limit > 0
      limited_circuits = circuits[0..@output_limit]
    else
      limited_circuits = circuits
    end
    display_circuits = limited_circuits.collect do |cid|
      gid = @grid_redis.hget(E_TO_I_KEY, cid)
      gid = "Not Known" if !gid
      "#{cid}:#{gid}"
    end 
      
    {:error => "Circuits that ABC never tried to build but Grid has detected them (#{circuits.size}). List limited to #{@output_limit} \n#{display_circuits.join("\n")}",
     :action => "This indicates a problem between Grid and LKP or LKP thinks it has already sent ABC a build request\n" \
                 "Manually request a build for the circuits or investigate why LKP is not sending requests to ABC"
    }
  end
  
  def circuits_that_abc_has_attempted_to_build_but_unknown_in_grid(circuits)
    if @output_limit > 0
      limited_circuits = circuits[0..@output_limit]
    else
      limited_circuits = circuits
    end
    display_circuits = limited_circuits.collect do |cid|
      bl = BuilderLog.find_by_name(cid)
      gid = bl ? bl.grid_id : "Not Known"
      "#{cid}:#{gid}"
    end
    {:error => "Circuits that ABC has tried to build but are not known to Grid (#{circuits.size}). List limited to #{@output_limit}\n#{display_circuits.join("\n")}",
     :action => "A new CDB database has been loaded onto the system "
    }
  end
  
  def failed_to_get_data_archive_data
    {:error => "Failed to connect to the DataArchive",
     :action => "Check the DataArchive is up and accessible"
    }
  end
  
  def circuits_in_xxx_not_in_grid(circuits, xxx, lookup_cid_in_builder_log = false)
    if @output_limit > 0
      limited_circuits = circuits[0..@output_limit]
    else
      limited_circuits = circuits
    end
    if lookup_cid_in_builder_log
      display_circuits = limited_circuits.collect do |gid|
        bl = BuilderLog.find_by_grid_id(gid)
        cid = bl ? bl.name : "Not Known"
        "#{cid}:#{gid}"
      end
    else
      display_circuits = limited_circuits
    end
    {:error => "Circuits that are in #{xxx} but are not in Grid (#{circuits.size}). List limited to #{@output_limit}\n#{display_circuits.join("\n")}",
     :action => "The #{xxx} may have data from another source....this should not occur"
    }
  end
  
  def circuits_in_grid_not_in_xxx(circuits, xxx)
    display_circuits = get_display_list(circuits)
    {:error => "Circuits that are in Grid but are not in #{xxx} (#{circuits.size}). List limited to #{@output_limit}\n#{display_circuits.join("\n")}",
     :action => "The #{xxx} is not importing data"
    }
  end
  
  def get_display_list(grids)
    if @output_limit > 0
      limited_grids = grids[0..@output_limit]
    else
      limited_grids = grids
    end
    limited_grids.collect do |gid|
      cid = @grid_redis.hget(I_TO_E_KEY, gid)
      cid = "Not Known" if !cid
      "#{cid}:#{gid}"
    end
  end

  def circuits_samples_lost(circuits_missing_samples)
    # convert { gid => {cname: , missing:} } to [ [gid, {cname:, missing:}], ] then sort
    sorted = circuits_missing_samples.to_a.sort {|a, b| b[1][:missing] <=> a[1][:missing]}
    if @output_limit > 0
      limited_sorted = sorted[0..@output_limit]
    else
      limited_sorted = sorted
    end
    output = limited_sorted.collect do |key, value| # technically not a hash key => value its actually [[key, value], ]
      "#{value[:cname]}:#{key}, Missing Samples: #{value[:missing]}"
    end
    {
      :error => "Circuits missing some samples. List sorted greatest missing to least and limited to #{@output_limit} out of #{sorted.count}\n#{output.join("\n")}",
      :action => "Check the Spirent data feed"
    }
  end

  def output_mismatches(list)
    if list["cdb_cleaned_dup"]
      lines = list["cdb_cleaned_dup"].collect{ |entry| "#{entry[:circuit]}:#{entry[:gid]}" }
      if @output_limit > 0
        output = lines[0 .. @output_limit]
      else
        output = lines
      end
      @results << { :error => "There are #{lines.size} circuits that have duplicate cdb availability within 5 minutes: (List limited to #{@output_limit})\n#{output.join("\n")}",
                    :action => "Investigate availability of these cicuits"
      }
    end
    if list["cdb_cleaned_ident"]
      lines = list["cdb_cleaned_ident"].collect{ |entry| "#{entry[:circuit]}:#{entry[:gid]}" }
      if @output_limit > 0
        output = lines[0 .. @output_limit]
      else
        output = lines
      end
      @results << { :error => "There are #{lines.size} circuits that have duplicate cdb availability in the same time period: (List limited to #{@output_limit})\n#{output.join("\n")}",
                   :action => "Investigate availability of these cicuits"
      }
    end
    if list["mismatch cdb duplicates"]
      lines = list["mismatch cdb duplicates"].collect{ |entry| "#{entry[:circuit]}:#{entry[:gid]} cdb entries: #{entry[:cdb]}, da entries: #{entry[:da]}" }
      if @output_limit > 0
        output = lines[0 .. @output_limit]
      else
        output = lines
      end
      @results << { :error => "There are #{lines.size} circuits that have duplicate cdb availability more than 5 inutes apart: (List limited to #{@output_limit})\n#{output.join("\n")}",
                    :action => "Investigate availability of these cicuits"
      }
    end
    if list["mismatch cdb off-by-one"]
      lines = list["mismatch cdb off-by-one"].collect{ |entry| "#{entry[:circuit]}:#{entry[:gid]} cdb entries: #{entry[:cdb]}, da entries: #{entry[:da]}" }
      if @output_limit > 0
        output = lines[0 .. @output_limit]
      else
        output = lines
      end
      @results << { :error => "There are #{lines.size} circuits that have one more availability entries in cdb than in da: (List limited to #{@output_limit})\n#{output.join("\n")}",
                    :action => "Investigate availability of these cicuits"
      }
    end
    if list["mismatch cdb other"]
      lines = list["mismatch cdb other"].collect{ |entry| "#{entry[:circuit]}:#{entry[:gid]} cdb entries: #{entry[:cdb]}, da entries: #{entry[:da]}" }
      if @output_limit > 0
        output = lines[0 .. @output_limit]
      else
        output = lines
      end
      @results << { :error => "There are #{lines.size} circuits that have more than 1 more availability entry in cdb than in da: (List limited to #{@output_limit})\n#{output.join("\n")}",
                    :action => "Investigate availability of these cicuits"
      }
    end
    if list["mismatch da duplicates"]
      lines = list["mismatch da duplicates"].collect{ |entry| "#{entry[:circuit]}:#{entry[:gid]} cdb entries: #{entry[:cdb]}, da entries: #{entry[:da]}" }
      if @output_limit > 0
        output = lines[0 .. @output_limit]
      else
        output = lines
      end
      @results << { :error => "There are #{lines.size} circuits that have duplicate da availability due to an unknown reason: (List limited to #{@output_limit})\n#{output.join("\n")}",
                    :action => "Investigate availability of these cicuits"
      }
    end
    if list["mismatch da sync"]
      lines = list["mismatch da sync"].collect{ |entry| "#{entry[:circuit]}:#{entry[:gid]} cdb entries: #{entry[:cdb]}, da entries: #{entry[:da]}" }
      if @output_limit > 0
        output = lines[0 .. @output_limit]
      else
        output = lines
      end
      @results << { :error => "There are #{lines.size} circuits that have more availability entries in da: (List limited to #{@output_limit})\n#{output.join("\n")}",
                    :action => "Investigate availability of these cicuits"
      }
    end
  end

  def da_has_duplicates?(results)
    last = nil
    results.any? do |result|
      if result["available"] == last
        true
      else
        last = result["available"]
        false
      end
    end
  end

  def cdb_has_duplicates?(cdb_records)
    last = ""
    cdb_records.any? do |record|
      if record[:state] == last
        true
      else
        last = record[:state]
        false
      end
    end
  end
end