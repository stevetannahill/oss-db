# ==============================================
# = List Missing Service Providers from Market =
# ==============================================
# script/runner lib/tools/market_service_providers.rb 
# Find all Service Providers that are in Market
# but are not found in CDB.


cdb_sps = ServiceProvider.all
ServiceProvider.establish_connection "market_#{Rails.env}"
market_sps = ServiceProvider.all


puts "Market Service Providers not found in CDB"
puts "-"*40
puts "ID\t\tNAME"
market_sps.each do |market_sp|
  sp = cdb_sps.find { |cdb_sp| cdb_sp.name.downcase == market_sp.name.downcase }
  unless sp
    puts "#{market_sp.id}\t\t#{market_sp.name}"
  end
end
