module LightSquaredTypes

  ACCESS_SOLUTION_MODEL = [
    "UNI-UNI",
    "UNI-ENNI",
    "N/A"
  ]

end

module HwTypes

  SITE_LAYOUTS = [

   # Base Business
   "StandardExchange7750",
   "StandardExchange7450",

   # Sprint
   "SprintMSC_Samsung",
   "SprintMSC_Samsung_Mock",
   "SprintMSC_ALU",
   "SprintMSC_Ericsson",
   "SprintMSC_Ericsson_S6",
   "SprintWDC",
   "SprintCDC",
   
   # Cox
   "CoxRetailAggSite",
   "CoxWholesaleAggSite",

   #CoreSite
   "CoreSite_Cenx_Playground",
   "CoreSite_LALab",
   "CoreSite_427SLasalle",
   "CoreSite_12100SunriseValley",
   "CoreSite_1275Kstreet",
   "CoreSite_900NAlameda",
   "CoreSite_OneWilshire",
   "CoreSite_32AA",
   "CoreSite_55SouthMarket",
   "CoreSite_1656McCarthy",
   "CoreSite_2972Stender",

   #VzW
   "VzW_CS_ALU",

   #Yoigo
   "Yoigo_RNC",
   "Yoigo_LL",
   "Yoigo_ML",
   "Yoigo_POC_15",
   "Yoigo_XPOC_005",
   "Yoigo_HUB",

   #Depricated
   "LightSquaredExchange7450_TAP",
   "LightSquaredExchange7450_RSC",
   "LightSquaredExchange7210_SGW"

  ]
  
  #This is used when checking the site_layout to make sure it is
  # part of a valid operator_network_type
  # Syntax "site_layout" => "operator_network_type"
  VALID_SITE_LAYOUT = {

    # Base Business
    "StandardExchange7750" => "Cenx Exchange",
    "StandardExchange7450" => "Cenx Exchange",

    # Sprint
    "SprintMSC_Samsung" => "OEM-SAMSUNG",
    "SprintMSC_Samsung_Mock" => "OEM-SAMSUNG",
    "SprintMSC_ALU" => "OEM-ALU",
    "SprintMSC_Ericsson" => "OEM-ERICSSON",
    "SprintMSC_Ericsson_S6" => "OEM-ERICSSON",
    "SprintWDC" => "Sprint Global MPLS Network",
    "SprintCDC" => "Sprint Global MPLS Network",

    # Cox
    "CoxRetailAggSite" => "Cox Retail",
    "CoxWholesaleAggSite" => "Cox Wholesale",

    #CoreSite
    "CoreSite_Cenx_Playground" => "Open Cloud Exchange",
    "CoreSite_LALab" => "Open Cloud Exchange",
    "CoreSite_427SLasalle" => "Open Cloud Exchange",
    "CoreSite_12100SunriseValley" => "Open Cloud Exchange",
    "CoreSite_1275Kstreet" => "Open Cloud Exchange",
    "CoreSite_900NAlameda" => "Open Cloud Exchange",
    "CoreSite_OneWilshire" => "Open Cloud Exchange",
    "CoreSite_32AA" => "Open Cloud Exchange",
    "CoreSite_55SouthMarket" => "Open Cloud Exchange",
    "CoreSite_1656McCarthy" => "Open Cloud Exchange",
    "CoreSite_2972Stender" => "Open Cloud Exchange",

    #VzW
    "VzW_CS_ALU" => "OEM-ALU",

    #Yoigo
    "Yoigo_RNC" => "Access and Transport Network",
    "Yoigo_LL" => "Access and Transport Network",
    "Yoigo_ML" => "Access and Transport Network",
    "Yoigo_POC_15" => "Access and Transport Network",
    "Yoigo_XPOC_005" => "Access and Transport Network",
    "Yoigo_HUB" => "Access and Transport Network",

    # Depricated
    "LightSquaredExchange7450_TAP" => "Cenx Light Squared Exchange",
    "LightSquaredExchange7450_RSC" => "Cenx Light Squared Exchange",
    "LightSquaredExchange7210_SGW" => "Cenx Light Squared Exchange"

  }
  
  NM_TYPES = [
    "5620Sam",
    "BrixWorx",

    # Sprint
    "eScout",
    "Sprint NEM",

    #CoreSite
    "SolarWinds",

    #Yoigo
    "Tellabs EMS",
    "OSS-RC",
    "IPT-NMS"

  ]
  
  NODE_TYPES = [
    #Displayed           Stored in DB
    ["7750-sr12",        "7750-sr12"],
    ["7450-ESS7",       "7450-ESS7"],
    ["7450-ESS7-2",       "7450-ESS7-2"], #Secondary 7450
    ["7210 SAS-M",       "7210 SAS-M"],
    ["7705 SAR-F",       "7705 SAR-F"],
    ["7705 SAR-8",       "7705 SAR-8"],

    ["cNode2020",        "cnode2020"],
    ["BV-3000",          "BV-3000"],
    ["RTU-310",          "RTU-310"],
    ["SMB-600",          "smb-600"],
    ["SMB-6000",         "smb-6000"],
    ["Cisco-3750",       "cisco-3750"],
    ["Dell-PowerEdge-R410", "Dell-PowerEdge-R410"],
    ["Terminal-Server-2511", "Terminal-Server-2511"],
    ["Juniper-SSG-5", "Juniper-SSG-5"],
    ["Juniper-SSG-140", "Juniper-SSG-140"],
    ["Adtran NetVanta 3305", "Adtran NetVanta 3305"],
    ["Cisco 2821", "Cisco 2821"],
    ["Cisco 1841", "Cisco 1841"],
    ["Telco T-Marc 340", "Telco T-Marc 340"],
    ["Telco T-Marc 380", "Telco T-Marc 380"],

    # Sprint
    ["ASR9100",     "ASR9100"],
    ["ASR9010",     "ASR9010"],
    ["SE1200",   "SE1200"],
    ["QoS-Scope-1G", "QoS-Scope-1G"],
    ["QoS-Scope-10G", "QoS-Scope-10G"],
    
    # AT&T 
    ["7609-S", "7609-S"],
    ["WIPM-LLC", "WIPM-LLC"],

    #CoreSite
    ["Brocade MLXe8",  "Brocade MLXe8"],
    ["Brocade MLXe16",  "Brocade MLXe16"],
    ["Brocade MLXe32",  "Brocade MLXe32"],
    ["Brocade CES 2024",  "Brocade CES 2024"],
    ["Brocade CES 2048",  "Brocade CES 2048"],

    #Yoigo
    ["Tellabs 8660",  "Tellabs 8660"],
    ["Tellabs 8630",  "Tellabs 8630"],
    ["MINI-LINK TN",  "MINI-LINK TN"],
    ["RNC",  "RNC"],
    ["BSC",  "BSC"],
    ["SIU",  "SIU"],
    ["RBS",  "RBS"],
    ["RBS 2000",  "RBS 2000"],
    ["RBS 3000",  "RBS 3000"],
    ["RBS 6000",  "RBS 6000"],

    #VzW
    ["ALU MMBTS",  "ALU MMBTS"]
    
  ]
  
  PORT_TYPES = [
    #Displayed        Stored in DB
    ["1 Gbps",        "1G"],
    ["10 Gbps",       "10G"],
    ["Test",          "test"],
    ["Other",         "other"]
  ]
  
  UNI_PHY_TYPES = [
    #Displayed        Stored in DB
    ["1Base5", "1Base5" ],
    ["2Base-TL", "2Base-TL" ],
    ["10Base2", "10Base2" ],
    ["10Base5", "10Base5" ],
    ["10Base-T", "10Base-T" ],
    ["10Base-FL", "10Base-FL" ],
    ["10Base-FB", "10Base-FB" ],
    ["10Base-FP", "10Base-FP" ],
    ["10/100Base-T", "10/100Base-T" ],
    #Fast Ethernet (100 Mbit/s)
    ["100Base-FX", "100Base-FX" ],
    ["100Base-SX", "100Base-SX" ],
    ["100Base-T", "100Base-T" ],
    ["100Base-T2", "100Base-T2" ],
    ["100Base-T4", "100Base-T4" ],
    ["100Base-TX", "100Base-TX" ],
    #Gigabit Ethernet
    ["1000Base-BX10", "1000Base-BX10" ],
    ["1000Base-CX", "1000Base-CX" ],
    ["1000Base-KX", "1000Base-KX" ],
    ["1000Base-LH", "1000Base-LH" ],
    ["1000Base-LX", "1000Base-LX" ],
    ["1000Base-LX10", "1000Base-LX10" ],
    ["1000Base-SX", "1000Base-SX" ],
    ["1000Base-T", "1000Base-T" ],
    ["1000Base-TX", "1000Base-TX" ],
    ["1000Base-ZX", "1000Base-ZX" ],    
    #10 Gigabit Ethernet
    ["10GBase-CX4", "10GBase-CX4" ],
    ["10GBase-ER", "10GBase-ER" ],
    ["10GBase-EW", "10GBase-EW" ],
    ["10GBase-LR", "10GBase-LR" ],
    ["10GBase-LRM", "10GBase-LRM" ],
    ["10GBase-LW", "10GBase-LW" ],
    ["10GBase-LX4", "10GBase-LX4" ],
    ["10GBase-SR", "10GBase-SR" ],
    ["10GBase-T", "10GBase-T" ],
    # Variants
    ["10/100/1000BaseT", "10/100/1000BaseT" ],
    ["Other", "Other" ],
    ["N/A", "N/A" ]
  ]

 DEMARC_DEVICE_TYPES = [
    #Displayed        Stored in DB
    ["Accedian", "Accedian"],
    ["Accedian EtherNID", "Accedian EtherNID"],
    ["Accedian EtherNID GE", "Accedian EtherNID GE"],
    ["Accedian MetroNID GE", "Accedian MetroNID GE"],
    ["Accedian MetroNID TE-R", "Accedian MetroNID TE-R"],
    ["Accedian MetroNODE 10GE", "Accedian MetroNODE 10GE"],
    ["Accedian Overture ISG26R", "Accedian Overture ISG26R"],
    ["Accedian-premium",  "Accedian-premium"],
    ["Adtran NetVanta 838/834",  "Adtran NetVanta 838/834"],
    ["Adva",  "Adva"],
    ["ADVA FSP 150cc-201GE",  "ADVA FSP 150cc-201GE"],
    ["ADVA FSP150cc-825",  "ADVA FSP150cc-825"],
    ["ADVA FSP 150cc-825",  "ADVA FSP 150cc-825"],
    ["ADVA FSP 825",  "ADVA FSP 825"],
    ["ALU 6250 -8M",  "ALU 6250 -8M"],
    ["ALU 7210",  "ALU 7210"],
    ["ALU 7210 SAR",  "ALU 7210 SAR"],
    ["ALU 7210 SAS-D",  "ALU 7210 SAS-D"],
    ["ALU 7210 SAS-M",  "ALU 7210 SAS-M"],
    ["ALU7210", "ALU7210"],
    ["ALU 7705",  "ALU 7705"],
    ["ALU 7705 SAR-M",  "ALU 7705 SAR-M"],
    ["ALU 7705 SAR",  "ALU 7705 SAR"],
    ["ALU 7705 SAR-F",  "ALU 7705 SAR-F"],
    ["ALU 7705 SAR-8",  "ALU 7705 SAR-8"],
    ["ALU 7750", "ALU 7750"],
    ["ALU7705", "ALU7705"],
    ["ALU MMBTS", "ALU MMBTS"],
    ["ALU SR7450", "ALU SR7450"],
    ["ALU SR7750", "ALU SR7750"],
    ["ALU7705", "ALU7705"],
    ["ALU 9500 MPR", "ALU 9500 MPR"],
    ["ALU 9228 MBS", "ALU 9228 MBS"],
    ["ALU 7450ESS-6", "ALU 7450ESS-6"],
    ["ANDA  2108",  "ANDA  2108"],
    ["ANDA  2118",  "ANDA  2118"],
    ["ANDA  2212",  "ANDA  2212"],
    ["Atrica/NSN A-2140",  "Atrica/NSN A-2140"],
    ["Atrica/NSN A-4100",  "Atrica/NSN A-4100"],
    ["Atrica/NSN A-8100",  "Atrica/NSN A-8100"],
    ["Brocade",  "Brocade"],
    ["Brocade MLXe8",  "Brocade MLXe8"],
    ["Brocade MLXe16",  "Brocade MLXe16"],
    ["Brocade MLXe32",  "Brocade MLXe32"],
    ["Brocade CES 2024",  "Brocade CES 2024"],
    ["Brocade CES 2048",  "Brocade CES 2048"],
    ["Calix E7 GPON with ONT", "Calix E7 GPON with ONT"],
    ["Canoga Perkins 9145E",  "Canoga Perkins 9145E"],
    ["Catalyst 6509", "Catalyst 6509"],
    ["Ciena",  "Ciena"],
    ["Ciena 311v",  "Ciena 311v"],
    ["Ciena LE-311v",  "Ciena LE-311v"],
    ["Ciena CN3911",  "Ciena CN3911"],
    ["Ciena 3930",  "Ciena 3930"],
    ["Ciena 3931",  "Ciena 3931"],
    ["Ciena CN3960",  "Ciena CN3960"],
    ["Ciena 4200",  "Ciena 4200"],
    ["Ciena 5150",  "Ciena 5150"],
    ["Ciena 5410",  "Ciena 5410"],
    ["Ciena 6500",  "Ciena 6500"],
    ["CISCO ME",  "CISCO ME"],
    ["CISCO ME-3400E",  "CISCO ME-3400E"],
    ["Cisco ME3400 EG",  "Cisco ME3400 EG"],
    ["CISCO ME-3550 G-12",  "CISCO ME-3550 G-12"],
    ["CISCO ME-3750",  "CISCO ME-3750"],
    ["CISCO-454",  "CISCO-454"],
    ["CISCO 3400",  "CISCO 3400"],
    ["CISCO 3800",  "CISCO 3800"],
    ["CISCO 4900",  "CISCO 4900"],
    ["CISCO 4900M",  "CISCO 4900M"],
    ["CISCO 6506",  "CISCO 6506"],
    ["CISCO 7600",  "CISCO 7600"],
    ["CISCO 7606",  "CISCO 7606"],
    ["CISCO 7609",  "CISCO 7609"],
    ["CISCO-7609",  "CISCO-7609"],
    ["Cisco Core Router", "Cisco Core Router"],
    ["Cisco ONS 15454 -GE-XP",  "Cisco ONS 15454 -GE-XP"],
    ["Cisco 4900/4924",  "Cisco 4900/4924"],
    ["Cisco 7606 Supervisor Engine 720-3 BXL",  "Cisco 7606 Supervisor Engine 720-3 BXL"],
    ["Cisco ASR1000", "Cisco ASR1000"],
    ["Cisco ASR9000", "Cisco ASR9000"],
    ["Cisco ASR9010", "Cisco ASR9010"],
    ["Cisco ASR9100", "Cisco ASR9100"],
    ["Cisco ME3400", "Cisco ME3400"],
    ["Cisco ONS 15454", "Cisco ONS 15454"],
    ["Ericsson MM-BTS", "Ericsson MM-BTS"],
    ["Ericsson SE-800", "Ericsson SE-800"],
    ["Ericsson SE-1200", "Ericcson SE-1200"],
    ["Ericsson SP-210", "Ericsson SP-210"],
    ["Ericsson SP-310", "Ericsson SP-310"],
    ["Ericsson SPO-1410", "Ericsson SPO-1410"],
    ["Ericsson MLTN AMM 6PC", "Ericsson MLTN AMM 6PC"],
    ["Fujitsu 9500", "Fujitsu 9500"],
    ["FWS", "FWS"],
    ["Hatteras",  "Hatteras"],
    ["Huawei",  "Huawei"],
    ["Infinera", "Infinera"],
    ["Juniper", "Juniper"],
    ["Juniper MX Series", "Juniper MX Series"],
    ["Juniper M120", "Juniper M120"],
    ["Juniper M120 Edge Router", "Juniper M120 Edge Router"],
    ["Juniper MX960", "Juniper MX960"],
    ["remote Juniper MX80", "remote Juniper MX80"],
    ["Juniper MX80", "Juniper MX80"],
    ["Juniper EX4200", "Juniper EX4200"],
    ["NSN", "NSN"],
    ["NSN A4100", "NSN A4100"],
    ["NSN A2200", "NSN A2200"],
    ["NSN A8100", "NSN A8100"],
    ["NSN eNodeB", "NSN eNodeB"],
    ["Overture 24", "Overture 24"],
    ["RAD ETX-202A", "RAD ETX-202A"],
    ["RAD ETX-204A", "RAD ETX-204A"],
    ["Samsung SmartMBS", "Samsung SmartMBS"],  #Sprint Demo
    ["Samsung MMBS", "Samsung MMBS"], 
    ["Telco T-Marc 340", "Telco T-Marc 340"],
    ["TBD", "TBD"],
    # Yoigo
    ["Tellabs 8660",  "Tellabs 8660"],
    ["Tellabs 8630",  "Tellabs 8630"],
    ["MINI-LINK TN",  "MINI-LINK TN"],
    ["RNC",  "RNC"],
    ["BSC",  "BSC"],
    ["SIU",  "SIU"],
    ["RBS",  "RBS"],
    ["RBS 2000",  "RBS 2000"],
    ["RBS 3000",  "RBS 3000"],
    ["RBS 6000",  "RBS 6000"]
  ]
  
  #WARNING: If you add or modify a value below please ensure you modify or add
  # the value to the PHY_TYPES_RATE
  PHY_TYPES = [
    #Displayed        Stored in DB
    ["1000Base-LX; 1310nm; SMF",  "1000Base-LX; 1310nm; SMF"],
    ["1000Base-SX; 850nm; MMF",  "1000Base-SX; 850nm; MMF"],
    ["1000Base-ZX; 1550nm; SMF",  "1000Base-ZX; 1550nm; SMF"],
    ["10GigE LR; 1310nm; SMF", "10GigE LR; 1310nm; SMF"],
    ["10GigE SR; 850nm; MMF", "10GigE SR; 850nm; MMF"],
    ["10GigE ER; 1550nm; SMF", "10GigE ER; 1550nm; SMF"],
    ["100M Copper", "100M Copper" ],
    ["10/100/1000 Base T",  "10/100/1000 Base T"],
    ["Other",      "Other"],
    ["STM-1",      "STM-1"],
    ["L3 Handoff",  "L3 Handoff"],
  ]
  #WARNING: The values here must match the values in PHY_TYPES
  PHY_TYPES_RATE = {
    "1000Base-LX; 1310nm; SMF" => 1000,
    "1000Base-SX; 850nm; MMF" => 1000,
    "1000Base-ZX; 1550nm; SMF" => 1000,
    "10GigE LR; 1310nm; SMF" => 10000,
    "10GigE SR; 850nm; MMF" => 10000,
    "10GigE ER; 1550nm; SMF" => 10000,
    "10/100/1000 Base T" => 1000,
    "100M Copper" => 100,
    "Other" => 1000,
    "STM-1" => 155,
    "L3 Handoff" => 10000
  }

  PHY_HANDOFF_TYPES = [
    #Displayed        Stored in DB
    "Auto-Negotiate", # TODO Depricated
    "Fixed Speed", # TODO Depricated
    "Both Supported", # TODO Depricated
    "Customer Orderable",
    "Always Enabled",
    "Always Disabled",
    "N/A"
  ]

  FIBER_HANDOFF_TYPES = [
    ["SC", "SC"],
    ["LC", "LC"],
    ["ST", "ST"],
    ["FC", "FC"],
    ["N/A", "N/A"],
    ["TBD", "TBD"],
  ]
  
  XSFP_TYPES = [
    #Displayed        Stored in DB
    ["10/100/1000 Base T",  "10/100/1000 Base T"],
    ["1G LX SFP",  "1G LX SFP"],
    ["1G SX SFP",  "1G SX SFP"],
    ["10G LR XFP", "10G LR XFP"],
    ["10G SR XFP", "10G SR XFP"],
    ["Other",      "Other"]
  ]

  TAG_MAPPING_TYPES = [
    "Outer Tag only",
    "Outer and Inner Tag",
    "Either mechanism supported"
  ]

  LAG_SELECTION_CONTROL = [
    "CENX controls selection",
    "Member controls selection",
    "Both Supported",
    "N/A"
  ]

  LAG_TYPES = [
    #Displayed        Stored in DB
    ["None", "None"],
    ["Active/Active", "Active/Active"],
    ["Active/Standby", "Active/Standby"],
    ["Both are supported",  "Both are supported"]
  ]
  
  ENNI_COS_ID_TYPES = [
    "Port",
    "VLAN",
    "S-VID",
    "PCP",
    "IPP (ToS)",
    "DSCP (DiffServ)"
  ]

  UNI_COS_ID_TYPES = [
    "Port",
    "VLAN",
    "PCP",
    "IPP (ToS)",
    "DSCP (DiffServ)",
    "N/A"
  ]

  PROTECTION_TYPES = [
    #Displayed      
    ["MChassis-LAG", "MChassis-LAG"],
    ["MCard-LAG", "MCard-LAG"],
    ["SCard-LAG", "SCard-LAG"],
    ["unprotected","unprotected"],
    ["unprotected-LAG","unprotected-LAG"]
  ]

  LAG_MODES = [
    ["Active/Active", "Active/Active"],
    ["Active/Standby", "Active/Standby"],
    ["N/A", "N/A"]
  ]
    
  DEMARC_ICONS = [
    ["Internet", "internet"],
    ["Router", "router"],
    ["SBC", "sbc"],
    ["Building", "building"],
    ["Voip", "voip"],
    ["VPN", "vpn"],
    ["Cell Site", "cell site"],
    ["MW Site", "MW site"],
    ["ENNI", "enni"],
    ["Test Head", "test head"],
    ["Switch", "switch"]
  ]

  DEMARC_TYPES = [
    #Displayed        Stored in DB
    ["UNI","UNI"],
    ["ENNI","ENNI"],
    ["IP VPN", "IP VPN"],
    ["Internet", "Internet"],
    ["Voice Gateway", "Voice Gateway"],
    ["Session Border Controller","Session Border Controller"],
    ["Mobile Switching Center","Mobile Switching Center"],
    ["Router","Router"],
    ["Cell Tower","Cell Tower"]
  ]
    
  Path_TYPES = [
    "Point to Point",
    "Point to Multipoint",
    "Multipoint to Multipoint",
#    "Rooted Multipoint"
  ]

  CENX_Path_TYPES = [
    "Standard",
    "Member Link",
    "LightSquared Single Cell",
    "LightSquared Multi Cell",
    "Tunnel",
    "Light Squared Aggregated Access",
    "Light Squared Core Transport",

    #Sprint
    "Sprint Fiber BackHaul Site",
    "Sprint Donor Site",
    "Sprint Intermediate Site",
    "Sprint Remote Site",
    "Sprint Core Transport",

    #CoreSite
    "CoreSite Intra-Node",
    "CoreSite Intra-Site",
    "CoreSite Inter-Site",

    #Layer 3
    "Generic IP Flow",

    # Depricated Sprint
    "Sprint Samsung AT&T", # Sprint DEMO
    "Sprint Samsung AT&T Pt-Pt", # Sprint DEMO
    "Sprint ALU VzT Tagged", # Sprint DEMO
    "Sprint ALU LAG Protected", # Sprint DEMO
    "Sprint ALU LAG Protected with MW", # Sprint DEMO

    #VzW
    "VzW Cell Site",

    #Yoigo
    "Yoigo",

    # Cox
    "Cox_Internal",
    "Cox_Buy",
    "Cox_Sell"
  ]

  CENX_Path_TUNNEL_TYPES = [
    "Member Link",
    "Tunnel",
    "Light Squared Aggregated Access"
  ]

  OVC_CLASSES = [
    ["OnNetOvc","OnNetOvc"],
    ["OffNetOvc","OffNetOvc"]
  ]

  OVC_ENDPOINT_CLASSES = [
    ["SegmentEndPoint","SegmentEndPoint"],
    ["OvcEndPointEnni","OvcEndPointEnni"],
    ["OvcEndPointUni","OvcEndPointUni"],
    ["OnNetOvcEndPointEnni","OnNetOvcEndPointEnni"]
  ]

  OVC_MAX_NUM_COS = ["1","2","3","4","5","6","7","8"]
  COS_ENNI_EGRESS_MARKING = ["Preserve","0","1","2","3","4","5","6","7","Copy C-TAG PCP","All are supported", "N/A"]
  COS_UNI_EGRESS_MARKING = ["Preserve","0","1","2","3","4","5","6","7","All are supported", "N/A"]
  
end 

module FrameTypes
  
  FRAME_COLOR_MARKING_TYPES = [
    #Displayed        Stored in DB
    ["DEI",        "DEI"],
    ["PCP 8p0d",   "PCP 8p0d"],
    ["PCP 7p1d",   "PCP 7p1d"],
    ["PCP 6p2d",   "PCP 6p2d"],
    ["PCP 5p3d",   "PCP 5p3d"],
    ["not passed", "not passed"],
    ["other", "other"]
    ]

  FRAME_COS_MARKING_TYPES = [
    #Displayed        Stored in DB
    ["Pbits", "Pbits"],
    ["DSCP", "DSCP"],
    ["EXP", "EXP"]
    ]

  ETHER_TYPES = [
    #Displayed        Stored in DB
    ["0x8100",        "0x8100"],
    ["0x88a8",        "0x88a8"],
    ["0x9100",        "0x9100"],
    ["0x8010",        "0x8010"],
    ["0x8020",        "0x8020"],
    ["0x9010",        "0x9010"],
    ["0x9020",        "0x9020"],
    ["Any",           "Any"],
    ["Other",         "Other"]
    ]

  PORT_ENCAP_TYPES = [
    "DOT1Q",
    "QINQ",
    "NULL"
  ]
    
  L2CP_HANDLING = [
    #Displayed        Stored in DB
    "Peer",
    "Discard",
    "Tunnel",
    "Unknown"
    ] 

  ## Warning - values used in OSS parser, dont change
  LOCALE_DIRECTION = [
    #Displayed        Stored in DB
    "UNI-INGRESS",
    "UNI-EGRESS",
    "ENNI-INGRESS",
    "ENNI-EGRESS"
    ]

  DIRECTION = [
    #Displayed        Stored in DB
    ["INGRESS","INGRESS"],
    ["EGRESS","EGRESS"]
    ]

  MEG_FORMATS = [
    ["TBD", "TBD"],
    ["802.1ag (IEEE)", "802.1ag (IEEE)"],
    ["Y.1731 (ITU-T)", "Y.1731 (ITU-T)"]
  ]

  MA_FORMATS = [
    ["TBD", "TBD"],
    ["icc-based (ITU-T)", "icc-based"],
    ["integer (2 octet)", "integer"],
    ["string (raw ASCII)", "string"],
    ["vid (0-4094)", "vid"],
    ["vpn_id (per RFC 2685)", "vpn_id"]
  ]

  ## Warning - values used in OSS parser, dont change
  MONITORING_FRAME_REFLECTION = [
    ["TBD", "TBD"],
    ["DMM/DMR", "DMM/DMR"],
    ["LBM/LBR", "LBM/LBR"],
    ["TWAMP","TWAMP"],
    ["Raw-Loopback", "Raw-Loopback"],
    ["IP-Ping", "IP-Ping"],
    ["MAC and VLAN Loopback", "MAC and VLAN Loopback"],
    ["VLAN Loopback", "VLAN Loopback"],
    ["Link OAM", "Link OAM"],
    ["SAA", "SAA"],
    ["ESA", "ESA"],
  ]      
end

module ServiceCategories
  
  SERVICE_MEF9_CERTIFICATION = [
    #Displayed         Stored in DB
    ["MEF 9 EPL", "MEF 9 EPL"],
    ["MEF 9 EVPL", "MEF 9 EVPL"],
    ["MEF 9 ELAN", "MEF 9 ELAN"],
    ["Not MEF 9 Certified", "Not MEF 9 Certified"],
    ["Not Applicable", "Not Applicable"]
  ]

  SERVICE_MEF14_CERTIFICATION = [
    #Displayed         Stored in DB
    ["MEF 14 EPL", "MEF 14 EPL"],
    ["MEF 14 EVPL", "MEF 14 EVPL"],
    ["MEF 14 ELAN", "MEF 14 ELAN"],
    ["Not MEF 14 Certified", "Not MEF 14 Certified"],
    ["Not Applicable", "Not Applicable"]
  ]

  CE_VLAN_PRESERVATION = [
    #Displayed         Stored in DB
    ["Yes", "Yes"],
    ["No", "No"],
    ["Can support both", "Can support both"]
  ]

  SERVICE_FRAME_DELIVERY = [
    #Displayed         Stored in DB
    ["Conditional", "Conditional"],
    ["Unconditional", "Unconditional"]
  ]

  SERVICE_TYPES = [
    #Displayed         Stored in DB
    ["Virtual Private LAN Service (vpls)", "vpls"],
    ["Virtual Private Remote Networking Service (vprn)", "vprn"],
    ["Mirror (for packet capture)", "mirror"]
  ]

  COLOR_MODE = [ 
    #Displayed         Stored in DB
    "color-aware",
    "not color-aware"
  ]

  COUPLING_FLAG = [
    #Displayed         Stored in DB
    "0",
    "1"
  ]

  SLA_CALCULATION_METHOD = [
    #Displayed         Stored in DB
    "Round Trip",
    "One Way"
  ]

  ON_NET_OVC_COS_TYPES = {
    #Name => {
    #  :priority => <int>,
    #  :short_name => <str>,
    #  :fwding_class => <str>,
    #  :cir_cac_limit => <int>,
    #  :eir_cac_limit => <int>,
    #  "commited" => {<cir> => <cbs>, ... }, 
    #  "excess" => {<eir> => <ebs>, ... }}
    "Real Time" => {
      :priority => 1,
      :short_name => 'R',
      :fwding_class => 'ef',
      :cir_cac_limit => 100,
      :eir_cac_limit => 0,
      "commited" =>  {
        9000 => 15,
        500000 => 30,
        1000000 => 60
      },
      "excess" => { 0 => 0 }
    },
    "Premium Data" => {
      :priority => 2,
      :short_name => 'P',
      :fwding_class => 'l1',
      :cir_cac_limit => 100,
      :eir_cac_limit => 0,
      "commited" => {
        1000 => 15,
        2000 => 18,
        3000 => 21,
        4000 => 24,
        5000 => 27,
        6000 => 30,
        7000 => 33,
        8000 => 36,
        9000 => 39,
        10000 => 43,
        20000 => 47,
        30000 => 51,
        40000 => 55,
        50000 => 59,
        60000 => 63,
        70000 => 67,
        80000 => 71,
        90000 => 75,
        100000 => 80,
        200000 => 85,
        300000 => 90,
        400000 => 95,
        500000 => 100
# Following apply if buying from Verizon
#        2000 => 45, (15+30)
#        4000 => 54,
#        6000 => 63,
#        8000 => 72,
#        10000 => 81,
#        12000 => 90,
#        14000 => 99,
#        16000 => 108,
#        18000 => 117,
#        20000 => 129,
#        40000 => 141,
#        60000 => 153,
#        80000 => 165,
#        100000 => 177,
#        120000 => 189,
#        140000 => 201,
#        160000 => 213,
#        180000 => 225,
#        200000 => 240,
#        400000 => 255,
#        600000 => 270,
#        800000 => 285,
#        1000000 => 300 (100+200)
       },
       "excess" => { 0 => 0 }
      },
    "Standard Data" => {
      :priority => 3,
      :short_name => 'S',
      :fwding_class => 'l2',
      :cir_cac_limit => 0,
      :eir_cac_limit => 300,
      "commited" => { 0 => 0 },
      "excess" => {
        1000 => 30,
        2000 => 36,
        3000 => 42,
        4000 => 48,
        5000 => 54,
        6000 => 60,
        7000 => 66,
        8000 => 72,
        9000 => 78,
        10000 => 86,
        20000 => 94,
        30000 => 102,
        40000 => 110,
        50000 => 118,
        60000 => 126,
        70000 => 134,
        80000 => 142,
        90000 => 150,
        100000 => 160,
        200000 => 170,
        300000 => 180,
        400000 => 190,
        500000 => 200 
      }
    },

    #For Sprint Core Demo .... todo
    "Hi Priority" => {
      :priority => 1,
      :short_name => 'R',
      :fwding_class => 'ef',
      :cir_cac_limit => 100,
      :eir_cac_limit => 0,
      "commited" =>  {
        9000 => 15,
        500000 => 30,
        1000000 => 60
      },
      "excess" => { 0 => 0 }
    },
    #For Cox Demo .... todo
    "Premium" => {
      :priority => 3,
      :short_name => 'S',
      :fwding_class => 'l2',
      :cir_cac_limit => 0,
      :eir_cac_limit => 300,
      "commited" => { 0 => 0 },
      "excess" => {
        1000 => 30,
        2000 => 36,
        3000 => 42,
        4000 => 48,
        5000 => 54,
        6000 => 60,
        7000 => 66,
        8000 => 72,
        9000 => 78,
        10000 => 86,
        20000 => 94,
        30000 => 102,
        40000 => 110,
        50000 => 118,
        60000 => 126,
        70000 => 134,
        80000 => 142,
        90000 => 150,
        100000 => 160,
        200000 => 170,
        300000 => 180,
        400000 => 190,
        500000 => 200
      }
    },
    "Basic" => {
      :priority => 3,
      :short_name => 'S',
      :fwding_class => 'l2',
      :cir_cac_limit => 0,
      :eir_cac_limit => 300,
      "commited" => { 0 => 0 },
      "excess" => {
        1000 => 30,
        2000 => 36,
        3000 => 42,
        4000 => 48,
        5000 => 54,
        6000 => 60,
        7000 => 66,
        8000 => 72,
        9000 => 78,
        10000 => 86,
        20000 => 94,
        30000 => 102,
        40000 => 110,
        50000 => 118,
        60000 => 126,
        70000 => 134,
        80000 => 142,
        90000 => 150,
        100000 => 160,
        200000 => 170,
        300000 => 180,
        400000 => 190,
        500000 => 200
      }
    },
    "Guaranteed CIR" => {
      :priority => 1,
      :short_name => 'R',
      :fwding_class => 'ef',
      :cir_cac_limit => 100,
      :eir_cac_limit => 0,
      "commited" =>  {
        50_000 => 32,
        100_000 => 64,
        10_000_000 => 128,

      },
      "excess" => { 0 => 0 }
    },
  }
  
  CENX_TEST_COS_VALUES = {
    :rates => {
      "ingress_cir_Mbps" => 1,
      "ingress_cbs_kB" => '',
      "ingress_eir_Mbps" => 0,
      "ingress_ebs_kB" => '',
      "egress_cir_Mbps" => 1,
      "egress_cbs_kB" => '',
      "egress_eir_Mbps" => 0,
      "egress_ebs_kB" => ''
    },
    :mapping => {
      "be" => "0",
      "l2" => "1",
      "af" => "2",
      "l1" => "3",
      "h2" => "4",
      "ef" => "5",
      "h1" => "6",
      "nc" => "7"
    },
    :rlm => {
      "Egress" => "Policing",
      "Ingress" => "Policing"
    }
  }

  RATE_LIMITING_MECHANISM = [
    "Policing",
    "Shaping",
    "Dimensioning",
    "MWL-Capacity",
    "None",
    "Can support all"
  ]

  #Beware, this list is used in QosPolicies by checking only first letter
  # thus if you have to add a new rate limiting mechanism make sure it has a
  # unique first letter
  RATE_LIMITING_MECHANISM_ACTUAL = [
    "Policing",
    "Shaping",
    "Dimensioning",
    "MWL-Capacity",
    "None"
  ]

  LAYERS = [
    "L1",
    "L2",
    "L3",
    "None"
  ]

end

module ServiceProviderTypes
  OWNER_ROLE = [
     #Displayed         Stored in DB
     ["Buyer", "Buyer"],
     ["Seller", "Seller"]
  ]

  ORDER_TYPES = [
    ["New", "New"],
    ["Change", "Change"],
    ["Renewal", "Renewal"],
    ["Disconnect", "Disconnect"]
  ]
  
  BULK_ORDER_TYPES = [
    ["Statement of Work", "Statement of Work"],
    ["Circuit Order", "Circuit Order"],
    ["Access Bulk Order", "Access Bulk Order"]
  ]

  ORDERED_ENTITY_TYPES = [
    ["Demarc", "Demarc"],
    ["Segment", "Segment"]
  ]
  
  BULK_ORDERED_ENTITY_TYPES = [
    ["Path", "Path"]
  ]

  ORDERED_ENTITY_SUBTYPES = [
    ["ENNI", "ENNI"],
    ["UNI", "UNI"],
    ["", ""],
    ["OnNetOvc", "OnNetOvc"],
    ["OffNetOvc", "OffNetOvc"]
  ]
  
  BULK_ORDERED_ENTITY_SUBTYPES = [
    ["", ""]
  ]
end

module ServiceTestTypes
  TEST_TYPES = [
    "Ethernet Frame Delay Test",
    "Ethernet Loopback Test",
    "Ping Active Test",
    "TWAMP Test",
    "IP-SLA Test"
  ]
end

module ServiceReportTypes
  REPORT_TYPES = [
    ["SLA Report"],
    ["Turnup Test Report"],
    ["Troubleshooting Report"]
  ]
end

module MemberAttrTypes

  ATTR_TYPES = [
    ["string", "string"],
    ["boolean", "boolean"],
    ["integer", "integer"],
    ["menu", "menu"]
  ]

  ATTR_CONTROL_ACTIONS = [
    "Show if True",
    "Require if True",
    "Disable if True",
    "Assure is True",
    "Set Value To",
    "Copy Value To",
    "Execute Code"
  ]

  ATTR_OBJECT_TYPES = [
    #Displayed         Stored in DB
    ["Attribute", "MemberAttr"],
    ["Member Handle", "MemberHandle"]
  ]

  ATTR_AFFECTED_CLASSES = [
    #Displayed         Stored in DB
    ["Operator Network", "OperatorNetwork"],
    ["Path", "Path"],
    ["Segment", "Segment"],
    ["Segment End Point", "SegmentEndPoint"],
    ["Demarc", "Demarc"],
    ["Service Order", "ServiceOrder"]
  ]
end
