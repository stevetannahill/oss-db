require File.dirname(__FILE__) + "/common_import_new"

def process_lh_data section_data

  @raw_item_count += section_data.size

  #Control flags
  current_table_pointer = 0
  next_table_pointer = 0
  table_data = []

  result_ok, next_table_pointer = find_next_table section_data, 1
  while result_ok
    current_table_pointer = next_table_pointer
    result_ok, next_table_pointer = find_next_table section_data, current_table_pointer+1
    if result_ok
      print_info "\n<------------------------------------>"
      #puts "==== Next Table [#{current_table_pointer}..#{next_table_pointer-1}]"
      table_data = section_data[current_table_pointer..next_table_pointer-1]
      result_ok = process_lh_table_data table_data
    end
  end

  @db_row_count += @segment_types.size
  @db_row_count += @es_types.size
  @db_row_count += @cos_types.size*6
  @db_row_count += @uni_types.size


end

def process_lh_table_data table_data
  result_ok = true
  @table_count += 1
  @item_count += table_data.size

  table_title = table_data[0]
  print_info "Processing Table: #{table_title}; Size: #{table_data.size} ..."

  case table_title
    when "<LH Service Names>"
      process_lh_service_types table_data
    when "<LH Frame Aware>"
      process_lh_frame_aware table_data
    when "<LH Service KPIs>"
      process_lh_service_kpis table_data
    when "<LH Infrastructure>"
      process_lh_infrastructure table_data
    when "<LH Circuit Requirements>"
      process_service_type_notes table_data,6
    when "<LH Frame Aware Services>"
      process_service_type_notes table_data,5
    when "<LH Frame Unaware Services>"
      process_service_type_notes table_data,5
    when "<LH Dedicated Cct Availability>"
      process_service_type_notes table_data,10
    when "<Your UNI Types>"
      process_lh_uni_types table_data
    when "<Service Type UNI Mapping>"
      process_lh_uni_mapping table_data
    when "<UNI Attributes>"
      process_lh_uni_attributes table_data
    when '<LH Doc Status>', '<LH Sub-providers>', '<LH Latency Change>', '<LH Overview>', '<LH 1G 10G Support>', '<LH Service Proposals>', '<LH Service Test and Ops>', '<LH UNI Requirements>', '<Additional UNI Info>', '<CENX Cct End-Points>', '<LH General Notes>'
      skip_over_table table_data
    else
      table_processing_error table_title, table_data.size
      result_ok = false
  end
  return result_ok
end

def process_lh_service_kpis table_data
  print_array_data table_data
  last_row_processed = 0
  #print_error 'XXX'
end

def process_service_type_notes table_data,rows_to_skip
  print_array_data table_data
  last_row_processed = 0

  # Skip over first part of the table
  last_row_processed += @num_service_columns*rows_to_skip
    
  @segment_types.each do |st|
    print_debug "Fill [#{st.number}]; #{st.object.name} Additional notes with [#{last_row_processed+st.number}] #{table_data[last_row_processed+st.number]}"
    rows = []
    rows << TableRow.new("append_string", "notes", table_data[last_row_processed+st.number])
    rows.each do |r| process_row r, st.object end
  end

  last_row_processed += @num_service_columns

  # Sanity check
  if (last_row_processed != table_data.size-1)
    print_error "#{table_data[0]} Table not properly Processed [#{last_row_processed}/#{table_data.size-1}]"
  else
    print_info "#{table_data[0]} Table Processed OK - [#{last_row_processed}/#{table_data.size-1}]"
  end

  # Save Service Types into the DB
  @segment_types.each do |ot|
    save_to_database "Segment Type", ot.object
  end

end

def process_lh_service_types table_data

  print_array_data table_data

  @num_service_columns = table_data.size-2

  1.upto(@num_service_columns) do |i|
    if (is_non_empty_data table_data[i])
      ot = SegmentType.new
      ot.operator_network_type_id = @ont.id
      ot.name = "#{table_data[i]} Segment"
      @segment_types << SegmentTypeRow.new( i, ot )

      est = EthernetServiceType.new
      est.operator_network_type_id = @ont.id
      est.name = "#{table_data[i]}"
      est.segment_types.push(ot)
      @es_types << SegmentTypeRow.new( i, est )
    end

#    notes = @oss.notes
#    notes = notes + table_data[table_data.size-1]

    @oss.notes = table_data[table_data.size-1]
    save_to_database "Offered Services Study", @oss
  end

  print_info "Segment Types Found: #{@segment_types.size} out of possible #{@num_service_columns}"

end

def process_lh_frame_aware table_data
  last_row_processed = 0
  print_array_data table_data

  #Fill in the Default Values
  @segment_types.each do |st|
    rows = []
    rows << TableRow.new("string", "segment_type", "Point to Point")
    rows << TableRow.new("string", "mef9_cert", 'Not Applicable')
    rows << TableRow.new("string", "mef14_cert", 'Not Applicable')
    rows << TableRow.new("string", "c_vlan_id_preservation", 'Yes')
    rows << TableRow.new("boolean", "c_vlan_cos_preservation", 'Yes')
    rows << TableRow.new("integer", "max_num_cos", '1')

    rows << TableRow.new("string", "color_marking", 'other')
    rows << TableRow.new("string", "color_marking_notes", 'Transparent')
    rows << TableRow.new("string", "unicast_frame_delivery_conditions", 'Unconditional')
    rows << TableRow.new("string", "unicast_frame_delivery_details", 'Unconditional')
    rows << TableRow.new("string", "multicast_frame_delivery_conditions", 'Unconditional')
    rows << TableRow.new("string", "multicast_frame_delivery_details", 'Unconditional')
    rows << TableRow.new("string", "broadcast_frame_delivery_conditions", 'Unconditional')
    rows << TableRow.new("string", "broadcast_frame_delivery_details", 'Unconditional')
    rows << TableRow.new("integer", "max_mtu", '9600')

#    rows << TableRow.new("string", "equipment_vendor", table_data[last_row_processed+st.number])


    rows.each do |r| process_row r, st.object end
  end

  # Frame Aware?
  @segment_types.each do |st|
    print_debug "Fill [#{st.number}]; #{st.object.name} frame aware with [#{last_row_processed+st.number}] #{table_data[last_row_processed+st.number]}"
    rows = []
    rows << TableRow.new("boolean", "is_frame_aware", table_data[last_row_processed+st.number].to_i == 1 ? 'Yes' : 'No')
    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns

  # Skip SLA Parameters FLR, DV, Avail and BER, SES, Avail
  last_row_processed += @num_service_columns*2

  #Additional notes
  @segment_types.each do |st|
    print_debug "Fill [#{st.number}]; #{st.object.name} Additional notes with [#{last_row_processed+st.number}] #{table_data[last_row_processed+st.number]}"
    rows = []
    rows << TableRow.new("string", "notes", table_data[last_row_processed+st.number])
    rows.each do |r| process_row r, st.object end
  end
  last_row_processed += @num_service_columns


  # Sanity check
  if (last_row_processed != table_data.size-1)
    print_error "Frame Aware Table not properly Processed [#{last_row_processed}/#{table_data.size-1}]"
  else
    print_info "Frame Aware Table Processed OK - [#{last_row_processed}/#{table_data.size-1}]"
  end

  # Save Service Types into the DB
  @segment_types.each do |ot|
    save_to_database "Segment Type", ot.object
  end

#  et = DemarcType.find(@et_id)
#  if !et
#    print_error "Cant Find ENNI Type"
#  end

  @es_types.each do |est|
    save_to_database "Ethernet Service Type", est.object
    #et.ethernet_service_types.push(est.object)
  end

  process_lh_cos_types

end

def process_lh_cos_types

  @num_cos_columns = 1

  1.upto(@num_cos_columns) do |i|
    cost = ClassOfServiceType.new
    cost.operator_network_type_id = @ont.id
    cost.name = "LongHaul"
    @cos_types << CosTypeRow.new( i, 'X', cost )
  end

  letters =  ['A','B','C','D','E','F','G','H']

  @cos_types.each do |cost|
    cost.letter = letters[cost.number-1]
    print_info "COSType: #{cost.number} == #{cost.letter}"
  end

  #Copy Note into all CoS's
  @cos_types.each do |cost|
    rows = []
    rows << TableRow.new("string", "notes", "Default CoS for LongHaul")
    rows.each do |r| process_row r, cost.object end
  end

  @es_types.each do |est|
    @cos_types.each do |cost|
       cost.object.ethernet_service_types.push(est.object)
       print_debug "Cos: #{cost.object.name} added to EST: #{est.object.name}"
    end
  end

  @cos_types.each do |cost|
    rows = []
    rows << TableRow.new("string", "cos_id_type_enni", 'Port')
    rows << TableRow.new("string", "cos_mapping_enni_ingress", '*')
    rows << TableRow.new("string", "cos_marking_enni_egress", 'Preserve')
    rows << TableRow.new("string", "cos_id_type_uni", 'Port')
    rows << TableRow.new("string", "cos_mapping_uni_ingress", '*')
    rows << TableRow.new("string", "cos_marking_uni_egress", 'Preserve')
    
    rows.each do |r| process_row r, cost.object end
  end

  num_end_point_columns = 2
  endpoint_column_names = ["ENNI", "UNI"]
  @es_types.each do |est|
    1.upto(num_end_point_columns) do |i|
      location = endpoint_column_names[i-1]
      default_cos = 'A'
      cost = @cos_types.select{|ct| ct.letter == default_cos}.pop
      segmentept = SegmentEndPointType.new
      segmentept.operator_network_type_id = @ont.id
      segmentept.name = "#{location} Endpoint [#{est.object.name}]"
      segmentept.class_of_service_type = cost.object
      segmentept.ethernet_service_types.push(est.object)

      save_to_database "Segment Endpoint Type ", segmentept
      print_debug "Cos: #{segmentept.name} added to EST: #{est.object.name} - #{segmentept.class_of_service_type_id}"
    end
  end

  @cos_types.each do |cost|
    rows = []
    rows << TableRow.new("string", "availability", '99.7')
    rows << TableRow.new("string", "frame_loss_ratio", 'TBD')
    rows << TableRow.new("string", "mttr_hrs", 'TBD')
    rows.each do |r| process_row r, cost.object end

    save_to_database "CoS Type #{cost.object.name}", cost.object

    slg = ServiceLevelGuaranteeType.new
    slg.class_of_service_type_id = cost.object.id
    slg.min_dist_km = '0'
    slg.max_dist_km = '*'

    slg.delay_us = 'N/A'
    slg.delay_variation_us = 'N/A'

    save_to_database "CoS Type #{cost.object.name} SLG ", slg
  end

 @cos_types.each do |cost|
   table_processing_modes = ["ENNI-INGRESS", "ENNI-EGRESS", "UNI-INGRESS", "UNI-EGRESS"]

   table_processing_modes.each do |table_processing_mode|
     bwpt = BwProfileType.new
     bwpt.class_of_service_type_id = cost.object.id
     bwpt.bwp_type = table_processing_mode

     rows = []
     rows << TableRow.new("boolean", "rate_limiting_performed", 'No')
     rows << TableRow.new("string", "rate_limiting_mechanism", 'None')
#     rows << TableRow.new("string", "cir_range_notes", table_data[last_row_processed+1])
#     rows << TableRow.new("string", "cbs_range_notes", table_data[last_row_processed+2])
#     rows << TableRow.new("string", "eir_range_notes", table_data[last_row_processed+3])
#     rows << TableRow.new("string", "ebs_range_notes", table_data[last_row_processed+4])
#     rows << TableRow.new("string", "coupling_flag", table_data[last_row_processed+5])
#     rows << TableRow.new("string", "color_mode", table_data[last_row_processed+6])
#     rows << TableRow.new("string", "notes", table_data[table_data.size-1])

     rows.each do |r| process_row r, bwpt end
     save_to_database "BWP Type", bwpt
   end
  end

end


def process_lh_infrastructure table_data
  last_row_processed = 0
  print_array_data table_data
  max_number_of_mux_types = 5
  max_number_optical_line_rates = 7
  max_number_transport_mechanisms = 5
  max_number_switched_ethernet = 7
  rows = []
  first_write = true

  #Wavelength Mux Type

  @segment_types.each do |st|
    root_index = last_row_processed+(st.number-1)*max_number_of_mux_types
    string_to_enter = ''
    need_to_write = false

    1.upto(max_number_of_mux_types) do |i|
      puts "Processing data: [#{root_index+i}] #{table_data[root_index+i]}"
      need_to_write = table_data[root_index+i].to_i == 1 ? true : false
      case i
        when 1
          string_to_enter = 'DWDM'
        when 2
          string_to_enter = 'CWDM'
        when 3
          string_to_enter = 'Dedicated fiber'
        when 4
          need_to_write = false
        when 5
          if is_non_empty_data table_data[root_index+i]
            string_to_enter = table_data[root_index+i]
            need_to_write = true
          end
        end

        if( need_to_write)
          if first_write
            rows << TableRow.new("string", "underlying_technology", string_to_enter)
            first_write = false
          else
            rows << TableRow.new("append_string", "underlying_technology", string_to_enter)
          end
        end
      end
      rows.each do |r| process_row r, st.object end
    end
    last_row_processed += @num_service_columns*max_number_of_mux_types

    # Optical Line Rates - Skip over for now
    last_row_processed += @num_service_columns*max_number_optical_line_rates

    #Transport Mechanism
    @segment_types.each do |st|
      root_index = last_row_processed+(st.number-1)*max_number_transport_mechanisms
      rows = []
      string_to_enter = ''
      need_to_write = false

      1.upto(max_number_transport_mechanisms) do |i|
        puts "Processing data: [#{root_index+i}] #{table_data[root_index+i]}"
        need_to_write = table_data[root_index+i].to_i == 1 ? true : false
        case i
          when 1
            string_to_enter = 'IEEE 802.3'
          when 2
            string_to_enter = 'SONET'
          when 3
            string_to_enter = 'OTN'
          when 4
            need_to_write = false
          when 5
            if is_non_empty_data table_data[root_index+i]
              string_to_enter = table_data[root_index+i]
              need_to_write = true
            end
        end

        if( need_to_write)
          if first_write
            rows << TableRow.new("string", "underlying_technology", string_to_enter)
            first_write = false
          else
            rows << TableRow.new("append_string", "underlying_technology", string_to_enter)
          end
        end
      end
      rows.each do |r| process_row r, st.object end
    end
    last_row_processed += @num_service_columns*max_number_transport_mechanisms

    #Switched Ethernet
    @segment_types.each do |st|
      root_index = last_row_processed+(st.number-1)*max_number_switched_ethernet
      rows = []
      string_to_enter = ''
      need_to_write = false

      1.upto(max_number_switched_ethernet) do |i|
        puts "Processing data: [#{root_index+i}] #{table_data[root_index+i]}"
        need_to_write = table_data[root_index+i].to_i == 1 ? true : false
        case i
          when 1
            string_to_enter = 'MPLS'
          when 2
            string_to_enter = 'MPLS/VPLS'
          when 3
            string_to_enter = 'PBB'
          when 4
            string_to_enter = 'PBB-TE'
          when 5
            string_to_enter = 'Cisco REP'
          when 6
            need_to_write = false
          when 7
            if is_non_empty_data table_data[root_index+i]
              string_to_enter = table_data[root_index+i]
              need_to_write = true
            end
        end

        if( need_to_write)
          if first_write
            rows << TableRow.new("string", "redundancy_mechanism", string_to_enter)
            first_write = false
          else
            rows << TableRow.new("append_string", "redundancy_mechanism", string_to_enter)
          end
        end
      end
      rows.each do |r| process_row r, st.object end
    end
    last_row_processed += @num_service_columns*max_number_switched_ethernet

  #Additional Notes
    @segment_types.each do |st|
      rows = []
      rows << TableRow.new("append_string", "notes", table_data[last_row_processed+1])
      rows.each do |r| process_row r, st.object end
    end
    last_row_processed += 1

    #rows << TableRow.new("string", "service_attribute_notes", table_data[last_row_processed+st.number])
#    rows << TableRow.new("string", "equipment_vendor", table_data[last_row_processed+st.number])

  # Sanity check
  if (last_row_processed != table_data.size-1)
    print_error "LH Infrastructure not properly Processed [#{last_row_processed}/#{table_data.size-1}]"
  else
    print_info "LH Infrastructure Processed OK - [#{last_row_processed}/#{table_data.size-1}]"
  end

  # Save Service Types into the DB
  @segment_types.each do |ot|
    save_to_database "Segment Type", ot.object
  end

end

def process_lh_uni_types table_data
  print_array_data table_data
  @num_uni_columns = table_data.size-2

  1.upto(@num_uni_columns) do |i|
    if (is_non_empty_data table_data[i])
      ut = UniType.new
      ut.demarc_type_type ='UNI Type'
      ut.operator_network_type_id = @ont.id
      ut.name = "#{table_data[i]} (for Light Squared LongHaul)"
      @uni_types << UniTypeRow.new( i, 'X', ut )
    end
  end

  letters = ['A','B','C','D','E','F','G','H',]
  @uni_types.each do |ut|
    ut.letter = letters[ut.number-1]
    print_info "UNIT #{ut.number} == #{ut.letter}"
  end

  print_info "Uni Types Found: #{@uni_types.size} out of possible #{@num_uni_columns}"
end

def process_lh_uni_mapping table_data
  print_array_data table_data

  # Sanity check
  expected_size = @num_service_columns*@num_uni_columns+2

  if (table_data.size != expected_size)
    print_error "#{table_data[0]} is wrong size [got: #{table_data.size} expect:#{expected_size} Version: #{@oss_document_version}]"
  else
    print_info "Processing #{table_data[0]}: [got: #{table_data.size} expect:#{expected_size}]"
  end

  @es_types.each do |est|
    1.upto(@num_uni_columns) do |i|
      uni_mapping = table_data[((est.number-1)*@num_uni_columns)+i]
      if (is_non_empty_data uni_mapping)
        ut = @uni_types.select{|u| u.letter == uni_mapping}.pop
        if ut
          ut.object.ethernet_service_types.push(est.object)
          #est.object.demarc_types.push(ut.object)
          print_debug "UNI: #{ut.object.name} added to EST: #{est.object.name}"
        else
          print_error "UNI #{uni_mapping} NOT added to EST: #{est.object.name}"
        end
      end
    end
  end
end

def process_lh_uni_attributes table_data
  print_array_data table_data

  last_row_processed = 0
  max_number_of_phy_types = 5
  max_number_of_loopback_types = 6

  expected_table_size = @num_uni_columns*(max_number_of_phy_types+max_number_of_loopback_types*2+6)+2


  if table_data.size != expected_table_size
    print_error "Error: Unexpected Table #{table_data[0]} size got: #{table_data.size} expect: #{expected_table_size}"
    return
  end

  # Phy types
  @uni_types.each do |ut|
    root_index = last_row_processed+(ut.number-1)*max_number_of_phy_types+1
    rows = []
    rows << TableRow.new("string", "physical_medium", table_data[root_index])
    1.upto(max_number_of_phy_types-2) do |i|
      if is_non_empty_data table_data[root_index+i]
        #print_info "**** UNI Append physical_medium #{table_data[root_index+i]}"
        rows << TableRow.new("append_string", "physical_medium", table_data[root_index+i])
      end
    end
    rows.each do |r| process_row r, ut.object end
  end
  last_row_processed += @num_uni_columns*max_number_of_phy_types

  #Auto-negotiate
  @uni_types.each do |ut|
    rows = []
    rows << TableRow.new("string", "auto_negotiate", table_data[last_row_processed+ut.number])
    rows.each do |r| process_row r, ut.object end
  end
  last_row_processed += @num_uni_columns

  #MTU
  @uni_types.each do |ut|
    rows = []
    rows << TableRow.new("integer", "mtu", table_data[last_row_processed+ut.number])
    rows.each do |r| process_row r, ut.object end
  end
  last_row_processed += @num_uni_columns

  #CFM
  @uni_types.each do |ut|
    rows = []
    rows << TableRow.new("boolean", "cfm_supported", table_data[last_row_processed+ut.number])
    rows.each do |r| process_row r, ut.object end
  end
  last_row_processed += @num_uni_columns

  #Loopback Mechanism
  @uni_types.each do |ut|
    first_write = true
    root_index = last_row_processed+(ut.number-1)*max_number_of_loopback_types
    rows = []
    string_to_enter = ''
    need_to_write = false

    1.upto(max_number_of_loopback_types) do |i|
      puts "Processing data: [#{root_index+i}] #{table_data[root_index+i]}"
      need_to_write = table_data[root_index+i].to_i == 1 ? true : false
      case i
        when 1
          string_to_enter = 'DMM/DMR'
        when 2
          string_to_enter = 'LBM/LBR'
        when 3
          string_to_enter = 'Raw-Loopback'
        when 4
          string_to_enter = 'IP-Ping'
          #puts "Reflection Detect: #{string_to_enter} W: #{need_to_write}"
        when 5
          need_to_write = false
        when 6
          if is_non_empty_data table_data[root_index+i]
            string_to_enter = table_data[root_index+i]
            need_to_write = true
          end
      end

      #puts "Reflection Check Write: #{need_to_write}"
      if( need_to_write)
        #puts "Writing Reflection Mechanism: [#{string_to_enter}"

        if first_write
          rows << TableRow.new("string", "reflection_mechanisms", string_to_enter)
          first_write = false
        else
          rows << TableRow.new("append_string", "reflection_mechanisms", string_to_enter)
        end
      end
    end
    rows.each do |r| process_row r, ut.object end

  end
  last_row_processed += @num_uni_columns*max_number_of_loopback_types

  #Turnup Loopback Mechanism
  @uni_types.each do |ut|
    first_write = true
    root_index = last_row_processed+(ut.number-1)*max_number_of_loopback_types
    rows = []
    string_to_enter = ''
    need_to_write = false

    1.upto(max_number_of_loopback_types) do |i|
      puts "Processing data: [#{root_index+i}] #{table_data[root_index+i]}"
      need_to_write = table_data[root_index+i].to_i == 1 ? true : false
      case i
        when 1
          string_to_enter = 'DMM/DMR'
        when 2
          string_to_enter = 'LBM/LBR'
        when 3
          string_to_enter = 'Raw-Loopback'
        when 4
          string_to_enter = 'IP-Ping'
          puts "Turnup Reflection Detect: #{string_to_enter} W: #{need_to_write}"
        when 5
          need_to_write = false
        when 6
          if is_non_empty_data table_data[root_index+i]
            string_to_enter = table_data[root_index+i]
            need_to_write = true
          end
      end

      puts "Turnup Reflection Check Write: #{need_to_write}"
      if( need_to_write)
        puts "Writing Turnup Reflection Mechanism: [#{string_to_enter}"

        if first_write
          rows << TableRow.new("string", "turnup_reflection_mechanisms", string_to_enter)
          first_write = false
        else
          rows << TableRow.new("append_string", "turnup_reflection_mechanisms", string_to_enter)
        end
      end
    end
    rows.each do |r| process_row r, ut.object end

  end
  last_row_processed += @num_uni_columns*max_number_of_loopback_types

  # Line rate reflection
  @uni_types.each do |ut|
    rows = []
    rows << TableRow.new("boolean", "service_rate_reflection_supported", table_data[last_row_processed+ut.number*2-1])
    rows << TableRow.new("string", "max_reflection_rate", table_data[last_row_processed+ut.number*2])
    rows.each do |r| process_row r, ut.object end
  end
  last_row_processed += @num_uni_columns*2


  #Device Type
  @uni_types.each do |ut|
    rows = []
    rows << TableRow.new("string", "connected_device_type", table_data[last_row_processed+ut.number])
    rows.each do |r| process_row r, ut.object end
  end
  last_row_processed += @num_uni_columns

  @uni_types.each do |ut|
    rows = []
    rows << TableRow.new("string", "remarks", table_data[last_row_processed+1])
    rows.each do |r| process_row r, ut.object end
  end

  last_row_processed += 1

  # Sanity check
  if (last_row_processed != table_data.size-1)
    print_error "UNI Attributes not properly Processed [#{last_row_processed}/#{table_data.size-1}]"
  else
    print_info "UNI Attributes Processed OK - [#{last_row_processed}/#{table_data.size-1}]"
  end


  # Save UNI Types into the DB
  @uni_types.each do |ut|
    ut.object.ls_access_solution_model = @ls_solution_model
    save_to_database "Uni Type", ut.object
  end

end