require File.dirname(__FILE__) + "/common_import_new"

def process_header_data section_data
  print_array_data section_data

  @item_count += section_data.size
  @raw_item_count += section_data.size
  
  section_title = ''
  section_title = "#{section_data[0]}"
  if section_title == "[Completed by/for]"
    @oss_document_type = 'Core'
  elsif section_title == "[LH Provider Name]"
    @oss_document_type = 'LS-LH'
  elsif section_title == "[Access Provider Name]"
    @oss_document_type = 'Sprint'
  else
    @oss_document_type = 'LS'
  end

  #Sanity check
  if @oss_document_type == 'Core'
    expected_size = 7
  elsif @oss_document_type == 'LS-LH'
    expected_size = 8
  else
    expected_size = 10
  end
  
  if( section_data.size != expected_size)
    print_error "Table: #{section_data[0]} is wrong size: #{section_data.size} expected #{expected_size}"
  end

  @oss = OssInfo.new
  @oss.operator_network_type_id = @ont.id
  
  @oss.completion_date = "#{section_data[5]}"

  @oss_document_version = section_data[6]

  if @oss_document_type == 'Sprint'
    @ls_solution_model = "#{section_data[8]}"
    print_info "*** Solution Model: #{@ls_solution_model}"
  end

  if @oss_document_type == 'Core'
    @oss.name = "#{section_data[1]} (#{@ont.name} network) - #{@oss.completion_date}"
  elsif @oss_document_type == 'LS-LH'
    print_info "LongHaul Case"
    @oss.name = "#{section_data[1]} (#{@ont.name} network) for Light Squared LH - #{@oss.completion_date}"
  else
    @oss.name = "#{section_data[1]} (#{@ont.name} network) - #{@oss.completion_date}"
    #@oss.name = "#{section_data[1]} (#{@ont.name} network) for Light Squared Access - #{@oss.completion_date}"
  end

  save_to_database "Offered Services Study", @oss
  @db_row_count += 1

end