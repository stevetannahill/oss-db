
def is_section_header s
  return s.first == '[' && s.last == ']' ? true : false
end

def is_table_header s
  return s.first == '<' && s.last == '>' ? true : false
end

def find_next_section data_array, start_pointer
  result_ok = true
  lookup_pointer = 0
  if start_pointer < data_array.size
    lookup_pointer = start_pointer
    while( lookup_pointer < data_array.size && !is_section_header("#{data_array[lookup_pointer]}") )
      lookup_pointer += 1
    end
  else
    result_ok = false
  end

  return result_ok, lookup_pointer

end

def find_next_table data_array, start_pointer
  result_ok = true
  lookup_pointer = 0
  if start_pointer < data_array.size
    lookup_pointer = start_pointer
    while( lookup_pointer < data_array.size && !is_table_header("#{data_array[lookup_pointer]}") )
      lookup_pointer += 1
    end
  else
    result_ok = false
  end

  return result_ok, lookup_pointer
end

def print_array_data array
  i = 0
  array.each do |d|
    print_debug "[#{i}]: #{d}"
    i += 1
  end

end

def str_to_bool(value)
  if "Y" == value.strip[0..0].upcase
    return true
  elsif value == "Auto-Negotiate"
    return true
  elsif value == "Enabled"
    return true
  else
    return false
  end
end

def is_non_empty_data value
  return value == "" || value == "Select..." ? false : true
end

def is_valid_data value
  if value == "Select..." ||  value == "Select.."
    return false
  end

  return true
end

def process_row row, object

  if !is_valid_data row.value
    print_warning "Empty data entry #{row.name} type: #{row.type} value: \"#{row.value}\" ...ignoring this data"
    return
  end

  case row.type
  when "string"
    object["#{row.name}"] = row.value
  when "append_string"
    str = ''
    if(object.read_attribute("#{row.name}"))
      str = object.read_attribute("#{row.name}")
    end
    str += "; #{row.value}"
    object["#{row.name}"] = str
  when "append_new_line"
    str = ''
    if(object.read_attribute("#{row.name}"))
      str = object.read_attribute("#{row.name}")
    end
    str += "\n#{row.value}"
    object["#{row.name}"] = str
  when "boolean"
    bool_value = str_to_bool row.value
    object["#{row.name}"] = bool_value
  when "integer"
    object["#{row.name}"] = row.value.to_i
  end

end

def skip_over_table table_data
  print_warning "Skipping over table #{table_data[0]} of size: #{table_data.size}"
  print_array_data table_data
end

def table_processing_error table_title, table_size
      print_error "Table #{table_title} not recognized..."
      @item_count -= table_size
      @table_count -= 1
end

def save_to_database id_string, object
    object_id = nil
    default_output_text = "CDB SAVE: #{id_string}: #{object.name}"

    @db_save_attempt += 1
    if object.save
      print_info "#{default_output_text} OK"
      @db_save_count += 1
      object_id = object.id
    else
      errors = ''
       object.errors.full_messages.each do |error|
         errors += error
       end
       print_error "#{default_output_text} FAILED\n--------\n#{errors}\n--------"
       @db_save_failures += "#{id_string}: #{object.name}; "
    end
    return object_id
end

def print_info s
  puts "#{s}"
end

def print_warning s
  @warning_count += 1
  print_string = "Warning: #{s}"
  puts print_string
  @all_warnings += "  [#{@warning_count}] - #{print_string}\n"
end

def print_error s
  @error_count += 1
  print_string = "ERROR: #{s}"
  puts print_string
  SW_ERR print_string
  @all_errors += "  [#{@error_count}] - #{print_string}\n"
  raise print_string
end

def print_debug s
  if @options.verbose
    puts "DEBUG: #{s}"
  end
end

class TableRow < Struct.new(:type, :name, :value); end


