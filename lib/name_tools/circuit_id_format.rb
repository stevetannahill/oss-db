module CircuitIdFormat

  class None
    DEFAULT = {:site_name => "TBD",
               :priority => "",
               :site_type => "TBD",
               :vlan_id => "",
               :site_name => "TBD",
               :primary => true,
               :backup => false,
               :p_b => "P"}
  
    def self.decode(circuit_id)
      DEFAULT
    end

    def self.name(circuit_id)
      "TBD"
    end

    def self.short_name(circuit_id)
      "TBD"
    end
    
    def self.valid(circuit_id, fail_text = [])
      true
    end
  end

  class SpirentSprint < None
    # CH03XC803-ETH-P-V110-CIR00050-P0
    def self.decode(circuit_id)
      split_id = circuit_id.split("-")  
      if split_id.size == 6
        {:site_name => split_id[0],
         :site_type => split_id[1],
         :primary => split_id[2] == "P",
         :backup => split_id[2] == "B",
         :p_b => split_id[2],
         :vlan_id => split_id[3][1..-1],
         :cir => split_id[4].sub(/CIR/,""),
         :priority => split_id.last[1..-1]
        }          
      else
        SW_ERR "Invalid Circuit-id format #{circuit_id}"
        super
      end
    end
  
    def self.name(circuit_id)
      return circuit_id
      #circuit_info = decode(circuit_id)
      #circuit_info[:site_type] + (circuit_info[:primary] ? "-P" : "-B") + "-V" + circuit_info[:vlan_id] + "-P" + circuit_info[:priority]
    end
  
    def self.short_name(circuit_id)
      return circuit_id
      #circuit_info = decode(circuit_id)
      #circuit_info[:site_name] + (circuit_info[:primary] ? "-P-P" : "-B-P") + circuit_info[:priority]
    end
    
    def self.valid(circuit_id, fail_text = [])
      valid = false
      fail_text.clear
      split_id = circuit_id.split("-")  
      if split_id.size == 6 
        decoded_cid = decode(circuit_id)
        if decoded_cid[:cir].size == 5
          valid = true
        else
          fail_text << "(#{circuit_id}) CIR must be 5 characters"
        end
      else
        fail_text << "(#{circuit_id}) Circuit Id conists of 6 parts seperated by -. "
      end
      if valid
        if (ctv = CosTestVector.find(:first, :conditions => ["circuit_id LIKE ?", "#{decoded_cid[:site_name]}-%"]))
          p_ctv = CosTestVector.find(:first, :conditions => ["circuit_id LIKE ?", "#{decoded_cid[:site_name]}-#{decoded_cid[:site_type]}-P-%-CIR#{decoded_cid[:cir]}-%"])
          b_ctv = CosTestVector.find(:first, :conditions => ["circuit_id LIKE ?", "#{decoded_cid[:site_name]}-#{decoded_cid[:site_type]}-B-%-CIR#{decoded_cid[:cir]}-%"])
          p_info = CircuitIdFormat::SpirentSprint.decode(p_ctv.circuit_id) if p_ctv
          b_info = CircuitIdFormat::SpirentSprint.decode(b_ctv.circuit_id) if b_ctv
          circuits = [ctv.circuit_id]
          circuits << p_ctv.circuit_id if p_ctv
          circuits << b_ctv.circuit_id if b_ctv
          circuits.uniq
          
          if p_ctv || b_ctv
            # At this point we know Cascade, site type and CIR match
            # check the VLAN matches 
            if (decoded_cid[:primary] && p_ctv) && (decoded_cid[:vlan_id] != p_info[:vlan_id]) ||
               (decoded_cid[:backup] && b_ctv) && (decoded_cid[:vlan_id] != b_info[:vlan_id])
              fail_text << "(#{circuit_id}) Circuit VLAN is #{decoded_cid[:vlan_id]} does not match existing VLAN (#{circuits.join(",")})"
              valid = false
            else
              # We now know Cascade, site type, CIR and either the VLAN matches or there is no existing Primary/Backup circuit
            end
          else
            # There was a mismatch
            fail_text << "(#{circuit_id}) Circuit does not match existing formats (#{circuits.join(",")})"
            valid = false
          end
        end
      end
      return valid
    end
  end

  class Yoigo < None
    
    #frbtx321m02_dllatxol0dw_UPS_BE - Src_Dest_testtype_Prio(BestEffort/RealTime)
    def self.decode(circuit_id)
      split_id = circuit_id.split("_")  
      if split_id.size == 6
        {:site_name => split_id[0] + "_" + split_id[1],
         :site_type => split_id[2],
         :primary => split_id[3] == "P",
         :backup => split_id[3] == "B",
         :vlan_id => "",
         :priority => split_id.last,
         :p_b => "B"
        }
      else
        SW_ERR "Invalid Circuit-id format #{circuit_id}"
        super
      end
    end
  
    def self.name(circuit_id)
      return circuit_id
      #circuit_info = decode(circuit_id)
      #circuit_info[:site_name] + (circuit_info[:primary] ? "-P" : "-B") + "-P" + circuit_info[:priority]
    end
  
    def self.short_name(circuit_id)
      name(circuit_id)
    end
    
    def self.valid(circuit_id, fail_text = [])
      # There should be 4 items - Need to do more validation
      split_id = circuit_id.split("_")  
      split_id.size == 6
    end    
  end

  class SpirentAtt < None
    # SPIRENT: FRBRTXBL01W/DLLATXOL0DW/USID25466/EVCPREF/00001-P0
    
    def self.decode(circuit_id)
      split_id = circuit_id.split("/")  
      if split_id.size == 5
        decoded_data = {:site_name => split_id[0] + "/" + split_id[1],
         :site_type => "",
         :primary => split_id[3] == "EVCPREF",
         :backup => split_id[3] == "EVCALT",
         :vlan_id => "",
         :priority => "",
         :p_b => (split_id[3] == "EVCPREF") ? "P" : "B"
        }
        decoded_data[:priority] = circuit_id.split("-").last[1..-1]
      else
        SW_ERR "Invalid Circuit-id format #{circuit_id}"
        decoded_data = super
      end
      return decoded_data
    end
  
    def self.name(circuit_id)
      return circuit_id
      #circuit_info = decode(circuit_id)
      #circuit_info[:site_name] + (circuit_info[:primary] ? "-P" : "-B") + "-P" + circuit_info[:priority]
    end
  
    def self.short_name(circuit_id)
      name(circuit_id)
    end
    
    def self.valid(circuit_id, fail_text = [])
      # There should be 5 items - Need to do more validation
      split_id = circuit_id.split("/")  
      split_id.size == 5
    end 
  end

  class WipmAtt < None
    
    #frbtx321m02_dllatxol0dw_UPS_BE - Src_Dest_testtype_Prio(BestEffort/RealTime)
    def self.decode(circuit_id)
      split_id = circuit_id.split("_")  
      if split_id.size == 4
        {:site_name => split_id[0] + "_" + split_id[1],
         :site_type => "",
         :primary => false,
         :backup => true,
         :vlan_id => "",
         :priority => split_id.last,
         :p_b => "B"
        }
      else
        SW_ERR "Invalid Circuit-id format #{circuit_id}"
        super
      end
    end
  
    def self.name(circuit_id)
      return circuit_id
      #circuit_info = decode(circuit_id)
      #circuit_info[:site_name] + (circuit_info[:primary] ? "-P" : "-B") + "-P" + circuit_info[:priority]
    end
  
    def self.short_name(circuit_id)
      name(circuit_id)
    end
    
    def self.valid(circuit_id, fail_text = [])
      # There should be 4 items - Need to do more validation
      split_id = circuit_id.split("_")  
      split_id.size == 4
    end    
  end
  
end