# == Schema Information
#
# Table name: paths
#
#  id                  :integer(4)      not null, primary key
#  operator_network_id :integer(4)
#  created_at          :datetime
#  updated_at          :datetime
#  cenx_id             :string(255)
#  description         :string(255)
#  path_type_id        :integer(4)
#  cenx_path_type      :string(255)     default("Standard")
#  cenx_name_prefix    :string(255)
#  event_record_id     :integer(4)
#  sm_state            :string(255)
#  sm_details          :string(255)
#  sm_timestamp        :integer(8)
#  prov_name           :string(255)
#  prov_notes          :text
#  prov_timestamp      :integer(8)
#  order_name          :string(255)
#  order_notes         :text
#  order_timestamp     :integer(8)
#  type                :string(255)
#

require 'test_helper'

class PathTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "the truth" do
    assert true
  end
end
