# == Schema Information
#
# Table name: ports
#
#  id                    :integer(4)      not null, primary key
#  name                  :string(255)
#  created_at            :datetime
#  updated_at            :datetime
#  node_id               :integer(4)
#  patch_panel_slot_port :string(255)
#  strands               :string(255)
#  color_code            :string(255)
#  notes                 :text
#  patch_panel_id        :integer(4)
#  connected_port_id     :integer(4)
#  mac                   :string(255)
#  phy_type              :string(255)
#  other_phy_type        :string(255)
#  demarc_id             :integer(4)
#

require 'test_helper'

class PortTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "the truth" do
    assert true
  end
end
