require 'test_helper'

class OnNetOvcOrderTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  
  should_belong_to :path
  should_belong_to :on_net_ovc
  should_validate_uniqueness_of :description, :scoped_to => :path_id
  should_validate_numericality_of :primary_contact_id , :message => 'is not selected'
  should_validate_presence_of :description, :order_type, :primary_contact_id, :path_id, :requested_service_date, :order_received_date
  
  #should_allow_values_for :cenx_id, "100712345678", "100728374065", "100"
  #should_not_allow_values_for :cenx_id, "100112345678", "090728374065", "100", "102920394939382929383", "100712345678"
  
  context "A On Net OVC order" do
    setup do
      @onovco = Factory(:on_net_ovc_order)
      @onovco2 = Factory(:on_net_ovc_order)
    end
    
    should "allow '100712345678' for cenx_id on Update" do
        @onovco.cenx_id = '100712345678'
        @onovco.save
        assert_equal true, @onovco.valid?
    end
    
    should "not allow '100' for cenx_id on Update" do
        @onovco.cenx_id = '100'
        @onovco.save
        assert_equal false, @onovco.valid?
    end
    
    should "allow it's own cenx_id on Update" do
        @onovco.cenx_id = @onovco.cenx_id
        @onovco.save
        assert_equal true, @onovco.valid?
    end
    
    should "not allow another On Net OVC orders cenx_id on Update" do
        @onovco.cenx_id = @onovco2.cenx_id
        @onovco.save
        assert_equal false, @onovco.valid?
    end
  end
  
  context "Up to 1000 On Net OVC order" do
    setup do
      @numonovcos = OnNetOvcOrder.find(:all).length
      @onovcos = Array.new(1000 - @numonovcos){ Factory.create(:on_net_ovc_order) }
    end
    
    should "successfully save" do
      assert OnNetOvcOrder.find(:all).length == 1000
    end
  end
  
  context "Every On Net OVC order" do
    setup do
      @onovcos = OnNetOvcOrder.find(:all)
    end
    
    should "have a valid cenx id" do
        @onovcos.each { |onovco| assert IdTools::CenxIdGenerator.decode_cenx_id(onovco.cenx_id)[0]}
    end
    
    should "have a unique cenx id" do
        @onovcos.each { |onovco| assert OnNetOvcOrder.find(:all, :conditions => ["cenx_id = ?", onovco.cenx_id]).length == 1 }
    end
  end
  
end
# == Schema Information
#
# Table name: service_orders
#
#  id                          :integer(4)      not null, primary key
#  type                        :string(255)
#  title                       :string(255)
#  cenx_id                     :string(255)
#  action                      :string(255)
#  primary_contact_id          :integer(4)
#  testing_contact_id          :integer(4)
#  requested_service_date      :date
#  order_received_date         :date
#  order_acceptance_date       :date
#  order_completion_date       :date
#  customer_acceptance_date    :date
#  billing_start_date          :date
#  operator_network_id         :integer(4)
#  expedite                    :boolean(1)
#  status                      :string(255)
#  notes                       :text
#  ordered_entity_id           :integer(4)
#  ordered_entity_type         :string(255)
#  created_at                  :datetime
#  updated_at                  :datetime
#  ordered_entity_subtype      :string(255)
#  path_id                     :integer(4)
#  ordered_entity_group_id     :integer(4)
#  ordered_operator_network_id :integer(4)
#  order_state                 :string(255)
#  ordered_object_type_id      :integer(4)
#  ordered_object_type_type    :string(255)
#  ordered_entity_snapshot     :text
#  technical_contact_id        :integer(4)
#  local_contact_id            :integer(4)
#  foc_date                    :date
#  bulk_order_type             :string(255)
#  design_complete_date        :date
#  order_created_date          :date
#  order_name                  :string(255)
#  order_notes                 :text
#  order_timestamp             :integer(8)
#

