# == Schema Information
#
# Table name: class_of_service_types
#
#  id                             :integer(4)      not null, primary key
#  name                           :string(255)
#  operator_network_type_id       :integer(4)
#  cos_id_type_enni               :string(255)
#  cos_mapping_enni_ingress       :string(255)
#  cos_mapping_uni_ingress        :string(255)
#  availability                   :string(255)
#  frame_loss_ratio               :string(255)
#  mttr_hrs                       :string(255)
#  notes                          :text
#  created_at                     :datetime
#  updated_at                     :datetime
#  cos_id_type_uni                :string(255)     default("pcp")
#  cos_marking_enni_egress        :string(255)
#  cos_marking_uni_egress         :string(255)
#  cos_marking_enni_egress_yellow :string(255)     default("N/A")
#  cos_marking_uni_egress_yellow  :string(255)     default("N/A")
#  type                           :string(255)
#

require 'test_helper'

class ClassOfServiceTypeTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "the truth" do
    assert true
  end
end
