# == Schema Information
#
# Table name: cos_test_vectors
#
#  id                              :integer(4)      not null, primary key
#  type                            :string(255)
#  test_type                       :string(255)
#  test_instance_class_id          :string(255)
#  notes                           :string(255)
#  cos_end_point_id                :integer(4)
#  service_level_guarantee_type_id :integer(4)
#  created_at                      :datetime
#  updated_at                      :datetime
#  delay_error_threshold           :string(255)
#  dv_error_threshold              :string(255)
#  delay_warning_threshold         :string(255)
#  dv_warning_threshold            :string(255)
#  service_instance_id             :string(255)
#  sla_id                          :string(255)
#  test_instance_id                :string(255)
#  flr_error_threshold             :string(255)
#  flr_warning_threshold           :string(255)
#  availability_guarantee          :string(255)
#  cenx_id                         :string(255)
#  flr_sla_guarantee               :string(255)
#  dv_sla_guarantee                :string(255)
#  delay_sla_guarantee             :string(255)
#  circuit_id                      :string(255)
#  ref_circuit_id                  :string(255)
#  event_record_id                 :integer(4)
#  sm_state                        :string(255)
#  sm_details                      :string(255)
#  sm_timestamp                    :integer(8)
#  last_hop_circuit_id             :string(255)
#  flr_reference                   :string(255)
#  delay_reference                 :string(255)
#  delay_min_reference             :string(255)
#  delay_max_reference             :string(255)
#  dv_reference                    :string(255)
#  dv_min_reference                :string(255)
#  dv_max_reference                :string(255)
#  loopback_address                :string(255)
#  circuit_id_format               :string(255)
#  grid_circuit_id                 :integer(4)
#  ref_grid_circuit_id             :integer(4)
#  protection_path_id              :integer(4)
#

require 'test_helper'

class CosTestVectorTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "the truth" do
    assert true
  end
end
