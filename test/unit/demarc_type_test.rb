# == Schema Information
#
# Table name: demarc_types
#
#  id                                :integer(4)      not null, primary key
#  type                              :string(255)
#  name                              :string(255)
#  demarc_type_type                  :string(255)
#  operator_network_type_id          :integer(4)
#  physical_medium                   :string(255)
#  physical_medium_notes             :string(255)
#  auto_negotiate                    :string(255)
#  mtu                               :integer(4)
#  max_num_segments                      :string(255)
#  max_num_segments_notes                :string(255)
#  connected_device_type             :string(255)
#  remarks                           :string(255)
#  lldp_untagged                     :string(255)
#  stp_untagged                      :string(255)
#  rstp_untagged                     :string(255)
#  mstp_untagged                     :string(255)
#  evst_untagged                     :string(255)
#  rpvst_untagged                    :string(255)
#  eaps_untagged                     :string(255)
#  pause_untagged                    :string(255)
#  lacp_untagged                     :string(255)
#  garp_untagged                     :string(255)
#  port_auth_untagged                :string(255)
#  lacp_notes_untagged               :string(255)
#  lamp_untagged                     :string(255)
#  link_oam_untagged                 :string(255)
#  mrp_b_untagged                    :string(255)
#  cisco_bpdu_untagged               :string(255)
#  default_l2cp_untagged             :string(255)
#  lldp_tagged                       :string(255)
#  stp_tagged                        :string(255)
#  rstp_tagged                       :string(255)
#  mstp_tagged                       :string(255)
#  pause_tagged                      :string(255)
#  lacp_tagged                       :string(255)
#  garp_tagged                       :string(255)
#  port_auth_tagged                  :string(255)
#  lacp_notes_tagged                 :string(255)
#  lamp_tagged                       :string(255)
#  link_oam_tagged                   :string(255)
#  mrp_b_tagged                      :string(255)
#  cisco_bpdu_tagged                 :string(255)
#  default_l2cp_tagged               :string(255)
#  frame_format                      :string(255)
#  frame_format_notes                :string(255)
#  consistent_ethertype              :boolean(1)
#  outer_tag_segment_mapping             :string(255)
#  lag_supported                     :boolean(1)
#  lag_type                          :string(255)
#  lacp_supported                    :boolean(1)
#  lacp_priority_support             :boolean(1)
#  protection_notes                  :string(255)
#  ah_supported                      :boolean(1)
#  ag_supported                      :boolean(1)
#  oam_notes                         :string(255)
#  multi_link_support                :boolean(1)
#  max_links_supported               :string(255)
#  bundling                          :boolean(1)
#  ato_bundling                      :boolean(1)
#  cfm_supported                     :boolean(1)
#  service_multiplexing              :boolean(1)
#  reflection_mechanisms             :string(255)
#  created_at                        :datetime
#  updated_at                        :datetime
#  lag_control                       :string(255)
#  ls_access_solution_model          :string(255)     default("N/A")
#  turnup_reflection_mechanisms      :string(255)
#  service_rate_reflection_supported :boolean(1)
#  max_reflection_rate               :string(255)
#

require 'test_helper'

class DemarcTypeTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "the truth" do
    assert true
  end
end
