# == Schema Information
#
# Table name: demarcs
#
#  id                               :integer(4)      not null, primary key
#  type                             :string(255)
#  cenx_id                          :string(255)
#  cenx_demarc_type                 :string(255)
#  notes                            :text
#  operator_network_id              :integer(4)
#  address                          :string(255)
#  port_name                        :string(255)
#  physical_medium                  :string(255)
#  connected_device_type            :string(255)
#  connected_device_sw_version      :string(255)
#  reflection_mechanism             :string(255)
#  mon_loopback_address             :string(255)
#  end_user_name                    :string(255)
#  is_new_construction              :boolean(1)
#  contact_info                     :string(255)
#  created_at                       :datetime
#  updated_at                       :datetime
#  demarc_type_id                   :integer(4)
#  max_num_segments                 :integer(4)
#  ether_type                       :string(255)
#  protection_type                  :string(255)
#  lag_id                           :integer(4)
#  fiber_handoff_type               :string(255)
#  auto_negotiate                   :boolean(1)
#  ah_supported                     :boolean(1)
#  lag_mac_primary                  :string(255)
#  emergency_contact_id             :integer(4)
#  lag_mac_secondary                :string(255)
#  port_enap_type                   :string(255)     default("QINQ")
#  lag_mode                         :string(255)
#  cir_limit                        :integer(4)      default(100)
#  eir_limit                        :integer(4)      default(300)
#  cenx_name_prefix                 :string(255)
#  demarc_icon                      :string(255)     default("building")
#  on_behalf_of_service_provider_id :integer(4)
#  event_record_id                  :integer(4)
#  sm_state                         :string(255)
#  sm_details                       :string(255)
#  sm_timestamp                     :integer(8)
#  prov_name                        :string(255)
#  prov_notes                       :text
#  prov_timestamp                   :integer(8)
#  order_name                       :string(255)
#  order_notes                      :text
#  order_timestamp                  :integer(8)
#  near_side_clli                   :string(255)     default("TBD")
#  far_side_clli                    :string(255)     default("TBD")
#  latitude                         :decimal(9, 6)   default(0.0)
#  longitude                        :decimal(9, 6)   default(0.0)
#  site_id                          :integer(4)
#

require 'test_helper'

class DemarcTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "the truth" do
    assert true
  end
end
