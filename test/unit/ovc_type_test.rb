# == Schema Information
#
# Table name: segment_types
#
#  id                                  :integer(4)      not null, primary key
#  name                                :string(255)
#  segment_type                            :string(255)
#  mef9_cert                           :string(255)
#  mef14_cert                          :string(255)
#  max_mtu                             :integer(4)
#  c_vlan_cos_preservation             :boolean(1)
#  c_vlan_id_preservation              :string(255)
#  s_vlan_cos_preservation             :boolean(1)
#  s_vlan_id_preservation              :string(255)
#  color_marking                       :string(255)
#  color_marking_notes                 :string(255)
#  unicast_frame_delivery_conditions   :string(255)
#  multicast_frame_delivery_conditions :string(255)
#  broadcast_frame_delivery_conditions :string(255)
#  unicast_frame_delivery_details      :string(255)
#  multicast_frame_delivery_details    :string(255)
#  broadcast_frame_delivery_details    :string(255)
#  notes                               :text
#  created_at                          :datetime
#  updated_at                          :datetime
#  operator_network_type_id            :integer(4)
#  underlying_technology               :string(255)
#  equipment_vendor                    :string(255)
#  service_attribute_notes             :string(255)
#  max_num_cos                         :string(255)
#  redundancy_mechanism                :string(255)
#  is_frame_aware                      :boolean(1)      default(TRUE)
#

require 'test_helper'

class SegmentTypeTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "the truth" do
    assert true
  end
end
