require 'test_helper'

class SegmentEndPointTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "the truth" do
    assert true
  end
end
# == Schema Information
#
# Table name: segment_end_points
#
#  id                        :integer(4)      not null, primary key
#  type                      :string(255)
#  cenx_id                   :string(255)
#  segment_id                :integer(4)
#  demarc_id                 :integer(4)
#  segment_end_point_type_id :integer(4)
#  md_format                 :string(255)
#  md_level                  :string(255)
#  md_name_ieee              :string(255)
#  ma_format                 :string(255)
#  ma_name                   :string(255)
#  is_monitored              :boolean(1)
#  notes                     :string(255)
#  stags                     :string(255)
#  ctags                     :string(255)
#  oper_state                :string(255)
#  created_at                :datetime
#  updated_at                :datetime
#  eth_cfm_configured        :boolean(1)      default(TRUE)
#  reflection_enabled        :boolean(1)
#  mep_id                    :string(255)     default("TBD")
#  event_record_id           :integer(4)
#  sm_state                  :string(255)
#  sm_details                :string(255)
#  sm_timestamp              :integer(8)
#  prov_name                 :string(255)
#  prov_notes                :text
#  prov_timestamp            :integer(8)
#  order_name                :string(255)
#  order_notes               :text
#  order_timestamp           :integer(8)
#

