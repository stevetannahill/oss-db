# == Schema Information
#
# Table name: nodes
#
#  id                   :integer(4)      not null, primary key
#  name                 :string(255)
#  node_type            :string(255)
#  primary_ip           :string(255)
#  created_at           :datetime
#  updated_at           :datetime
#  site_id :integer(4)
#  secondary_ip         :string(255)
#  net_mask             :string(255)
#  gateway_ip           :string(255)
#  notes                :string(255)
#  cabinet_id           :integer(4)
#  loopback_ip          :string(255)
#  network_manager_id   :integer(4)
#  event_record_id      :integer(4)
#  sm_state             :string(255)
#  sm_details           :string(255)
#  sm_timestamp         :integer(8)
#  prov_name            :string(255)
#  prov_notes           :text
#  prov_timestamp       :integer(8)
#  order_name           :string(255)
#  order_notes          :text
#  order_timestamp      :integer(8)
#

require 'test_helper'

class NodeTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "the truth" do
    assert true
  end
end
