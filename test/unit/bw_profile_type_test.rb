# == Schema Information
#
# Table name: bw_profile_types
#
#  id                       :integer(4)      not null, primary key
#  name                     :string(255)
#  bwp_type                 :string(255)
#  class_of_service_type_id :integer(4)
#  cir_range_notes          :string(255)
#  cbs_range_notes          :string(255)
#  eir_range_notes          :string(255)
#  ebs_range_notes          :string(255)
#  color_mode               :string(255)
#  coupling_flag            :string(255)
#  notes                    :text
#  created_at               :datetime
#  updated_at               :datetime
#  rate_limiting_performed  :boolean(1)
#  rate_limiting_mechanism  :string(255)
#  rate_limiting_layer      :string(255)
#  rate_limiting_notes      :string(255)
#

require 'test_helper'

class BwProfileTypeTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "the truth" do
    assert true
  end
end
