# == Schema Information
#
# Table name: cos_end_points
#
#  id                         :integer(4)      not null, primary key
#  segment_end_point_id       :integer(4)
#  cos_instance_id            :integer(4)
#  ingress_mapping            :string(255)
#  ingress_cir_kbps           :string(255)
#  ingress_eir_kbps           :string(255)
#  ingress_cbs_kB             :string(255)
#  ingress_ebs_kB             :string(255)
#  created_at                 :datetime
#  updated_at                 :datetime
#  connected_cos_end_point_id :integer(4)
#  type                       :string(255)
#  egress_marking             :string(255)
#  egress_cir_kbps            :string(255)
#  egress_eir_kbps            :string(255)
#  egress_cbs_kB              :string(255)
#  egress_ebs_kB              :string(255)
#  ingress_rate_limiting      :string(255)     default("Policing")
#  egress_rate_limiting       :string(255)     default("None")
#  event_record_id            :integer(4)
#  sm_state                   :string(255)
#  sm_details                 :string(255)
#  sm_timestamp               :integer(8)
#

require 'test_helper'

class CosEndPointTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "the truth" do
    assert true
  end
end
