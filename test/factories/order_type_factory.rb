Factory.sequence :order_type_name do |n|
  "Order Type #{n}"
end

Factory.define :order_type do |ot|
  ot.name { Factory.next(:order_type_name) }
  ot.operator_network_type_id { random_id(OperatorNetworkType.find(:all), "Order Type", "Operator Network Type") }
end
