Factory.sequence :cabinet_name do |n|
  "Cabinet #{n}"
end

Factory.define :cabinet do |cab|
  cab.name { Factory.next(:cabinet_name) }
  cab.site_id {
    sites = Site.find(:all)
    sites[rand(sites.length)].id
  }
end


