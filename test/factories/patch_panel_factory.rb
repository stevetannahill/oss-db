Factory.sequence :patch_panel_name do |n|
  "Patch Panel #{n}"
end

Factory.define :patch_panel do |ossi|
  ossi.name { Factory.next(:patch_panel_name) }
  ossi.site_id { random_id(Site.all, "Patch Panel", "Site") }
end