Factory.define :cos_end_point do |cosi|
  cosi.segment_end_point_id {
    random_id(SegmentEndPoint.all.reject{|segmentep| segmentep.get_candidate_cos_instances.empty? }, "COS End Point", "Segment End Point", "with unused COS Instances")
  }
  cosi.cos_instance_id {|c| random_id(c.get_candidate_cos_instances, "COS End Point", "COS Instance", "for Segment End Point #{c.segment_end_point.cenx_name}") }
  cosi.ingress_mapping {'1'}
  cosi.egress_marking {'7'}
  cosi.ingress_rate_limiting {'Policing'}
  cosi.egress_rate_limiting {'Shaping'}
  cosi.ingress_cir_kbps { |c| ServiceCategories::ON_NET_OVC_COS_TYPES[c.class_of_service_type.name][:cir_cac_limit].zero? ? "0" : (rand(500)+1).to_s }
  cosi.ingress_eir_kbps { |c| ServiceCategories::ON_NET_OVC_COS_TYPES[c.class_of_service_type.name][:eir_cac_limit].zero? ? "0" : (rand(500)+1).to_s }
  cosi.ingress_cbs_kB { rand(500).to_s }
  cosi.ingress_ebs_kB { rand(500).to_s }
  cosi.egress_cir_kbps { |c| ServiceCategories::ON_NET_OVC_COS_TYPES[c.class_of_service_type.name][:cir_cac_limit].zero? ? "0" : (rand(500)+1).to_s }
  cosi.egress_eir_kbps { |c| ServiceCategories::ON_NET_OVC_COS_TYPES[c.class_of_service_type.name][:eir_cac_limit].zero? ? "0" : (rand(500)+1).to_s }
  cosi.egress_cbs_kB { rand(500).to_s }
  cosi.egress_ebs_kB { rand(500).to_s }
end

#Build a test cos_end_point as defined by ServiceCategories::CENX_TEST_COS_VALUES
Factory.define :test_cos_end_point, :class => CosEndPoint, :parent => :cos_end_point do |cosi|
  rates = ServiceCategories::CENX_TEST_COS_VALUES[:rates]
  cosi.ingress_cir_kbps { rates["ingress_cir_Mbps"] * 1000 }
  cosi.ingress_eir_kbps { rates["ingress_eir_Mbps"] * 1000 }
  cosi.ingress_cbs_kB { rates["ingress_cbs_kB"] }
  cosi.ingress_ebs_kB { rates["ingress_ebs_kB"] }
  cosi.egress_cir_kbps { rates["egress_cir_Mbps"] * 1000 }
  cosi.egress_eir_kbps { rates["egress_eir_Mbps"] * 1000 }
  cosi.egress_cbs_kB { rates["egress_cbs_kB"] }
  cosi.egress_ebs_kB { rates["egress_ebs_kB"] }

  cosi.ingress_rate_limiting {ServiceCategories::CENX_TEST_COS_VALUES[:rlm]["Ingress"]}
  cosi.egress_rate_limiting {ServiceCategories::CENX_TEST_COS_VALUES[:rlm]["Egress"]}

  cosi.ingress_mapping { |c|
    cos = ServiceCategories::ON_NET_OVC_COS_TYPES[c.class_of_service_type.name]
    ServiceCategories::CENX_TEST_COS_VALUES[:mapping][cos[:fwding_class]]}
  cosi.egress_marking { |c|
    cos = ServiceCategories::ON_NET_OVC_COS_TYPES[c.class_of_service_type.name]
    ServiceCategories::CENX_TEST_COS_VALUES[:mapping][cos[:fwding_class]]}
  
end
