Factory.define :cos_instance do |cosi|
  cosi.segment_id {
    random_id(OffNetOvc.all.reject{|segment| segment.get_candidate_costs.empty? }, "COS Instance", "Segment", "with an Ethernet Service Type that has unused COS Types")
  }
  cosi.class_of_service_type_id {|c| random_id(c.get_candidate_costs, "COS Instance", "COS Type", "for Segment #{c.segment.name}") }
end

Factory.define :cenx_cos_instance, :class => CenxCosInstance, :parent => :cos_instance do |ccosi|
  ccosi.segment_id {
    random_id(OnNetOvc.all.reject{|segment| segment.get_candidate_costs.empty? }, "COS Instance", "Segment", "with an Ethernet Service Type that has unused COS Types")
  }
end