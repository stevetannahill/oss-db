Factory.sequence :demarc_member_handle do |n|
  "Demarc #{n}"
end

Factory.define :demarc_common, :class => Demarc do |demarc|
  demarc.operator_network_id { random_id(OperatorNetwork.all.reject{|on| on.site.nil?}, "Demarc", "Operator Network") }
#  demarc.member_handle {|d| (d.service_provider && !d.service_provider.is_system_owner) ? Factory.next(:demarc_member_handle) : nil }
  demarc.address { random_street_address }
  demarc.demarc_type_id {|d|
    dts = d.get_candidate_demarc_types
    (dts.nil? || dts.empty?) ? nil : ((rand(2) > 0) ? dts[rand(dts.length)].id : nil)
  }
  demarc.physical_medium{|d|
    if d.demarc_type && !d.demarc_type.get_candidate_physical_mediums.empty?
      random_item(d.demarc_type.get_candidate_physical_mediums, "Demarc", "Physical Medium", "for Demarc Type #{d.demarc_type.name}")
    elsif d.cenx_demarc_type == "UNI"
      random_item(HwTypes::UNI_PHY_TYPES.map{|disp, value| value }, "UNI", "HwTypes::UNI_PHY_TYPES")
    elsif d.cenx_demarc_type == "ENNI"
      random_item(HwTypes::PHY_TYPES.map{|disp, value| value }, "ENNI", "HwTypes::PHY_TYPES")
    else
      nil
    end
  }
  demarc.connected_device_type{|d|
    if d.demarc_type && !d.demarc_type.get_candidate_connected_device_types.empty?
      random_item(d.demarc_type.get_candidate_connected_device_types, "Demarc", "Connected Device Type", "for Demarc Type #{d.demarc_type.name}")
    else
      random_item(HwTypes::DEMARC_DEVICE_TYPES.map{|disp, value| value }, "Demarc", "HwTypes::DEMARC_DEVICE_TYPES")
    end
  }
  demarc.reflection_mechanism {|d|
    if d.cenx_demarc_type == "UNI"
      if d.demarc_type && !d.demarc_type.get_candidate_reflection_mechanisms.empty?
        random_item(d.demarc_type.get_candidate_reflection_mechanisms, "UNI", "Reflection Mechanisms", "for UNI Type #{d.demarc_type.name}")
      else
        random_item(FrameTypes::MONITORING_FRAME_REFLECTION.map{|disp, value| value }, "UNI", "FrameTypes::MONITORING_FRAME_REFLECTION")
      end
    else
      nil
    end
  }
  demarc.ether_type {|d|
    if d.cenx_demarc_type == "ENNI"
      if d.demarc_type && !d.demarc_type.get_candidate_ether_types.empty?
        random_item(d.demarc_type.get_candidate_ether_types.map{|disp, value| value }, "ENNI", "Ether Type", "for ENNI Type #{d.demarc_type.name}")
      else
        random_item(FrameTypes::ETHER_TYPES.map{|disp, value| value }, "ENNI", "FrameTypes::ETHER_TYPES")
      end
    else
      nil
    end
  }
end

Factory.define :demarc, :parent => :demarc_common do |demarc|
  demarc.cenx_demarc_type { random_item(HwTypes::DEMARC_TYPES.map{|disp, value| value } - ["ENNI", "UNI"], "Demarc", "HwTypes::DEMARC_TYPES") }
end

Factory.define :uni, :parent => :demarc_common, :class => Uni do |uni|
  uni.cenx_demarc_type "UNI"
end

Factory.define :uni_full, :parent => :uni do |uni|
  uni.port_name "UNI Port"
  uni.mon_loopback_address { rand(2) ? Factory.next(:ip) : Factory.next(:mac)}
  uni.connected_device_sw_version "UNI SW Version"
  uni.end_user_name "End User"
  uni.is_new_construction { rand(2) }
  uni.contact_info "Just yell really loudly!"
end

Factory.define :enni_new, :parent => :demarc_common, :class => EnniNew do |enni|
  enni.cenx_demarc_type "ENNI"
  enni.protection_type {random_item(HwTypes::PROTECTION_TYPES.map{|disp, value| value}, "ENNI", "HwTypes:PROTECTION_TYPES") }
  enni.port_enap_type {random_item(FrameTypes::PORT_ENCAP_TYPES.map{|value| value }, "ENNI", "FrameTypes::PORT_ENCAP_TYPES") }
  enni.operator_network_id { random_id(OperatorNetwork.all, "Demarc", "Operator Network") }
  enni.site_id { |e|
    if e.operator_network.sites.any?
      e.operator_network.sites.first.id
    else
      random_id(Site.all, "ENNI", "Exchange") 
    end

  }
  enni.lag_id {|e| (e.protection_type != "unprotected") ? IdTools::LagIdGenerator.generate_enni_lag_id(Site.find(e.site_id)) : nil }
  enni.demarc_icon {"enni"}
end
