Factory.sequence :class_of_service_type_name do |n|
  "Class Of Service Type #{n}"
end

Factory.define :class_of_service_type do |cost|
  cost.name{ Factory.next(:class_of_service_type_name) }
  cost.operator_network_type_id { random_id(OperatorNetworkType.all(), "COS Type", "Operator Network Type") }
  cost.cos_mapping_enni_ingress{ random_list(Array.new(8){|n| "#{n}"}, "COS Type", "COS Value List") }
  cost.cos_marking_enni_egress{ '1' }
  cost.cos_mapping_uni_ingress{ random_list(Array.new(8){|n| "#{n}"}, "COS Type", "COS Value List") }
  cost.cos_marking_uni_egress{ '1' }
  cost.cos_id_type_enni { random_item(HwTypes::ENNI_COS_ID_TYPES, "COS Type", "HwTypes::ENNI_COS_ID_TYPES") }
  cost.cos_id_type_uni { random_item(HwTypes::UNI_COS_ID_TYPES, "COS Type", "HwTypes::UNI_COS_ID_TYPES") }
end
