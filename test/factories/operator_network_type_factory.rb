Factory.sequence :operator_network_type_name do |n|
  "Operator Network Type #{n}"
end

Factory.define :operator_network_type do |ont|
  ont.name { Factory.next(:operator_network_type_name) }
  ont.service_provider_id {
    sps = ServiceProvider.find(:all)
    raise "Failed to create Operator Network Type - No Service Providers exist!" if sps.empty?
    sps[rand(sps.length)].id
  }
end
