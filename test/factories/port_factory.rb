Factory.sequence :port_name do |n|
  "Port #{n}"
end

Factory.define :port do |port|
  port.name { Factory.next(:port_name) }
  port.node_id {|p|
    nodes = Node.find(:all)
    raise "Failed to create Port - No Nodes exist!" if nodes.empty?
    enni = p.demarc
    if enni
      nodes = nodes.select{|n| n.site == enni }
      raise "Failed to create Port - No Nodes exist for Site:#{enni.site.name}!" if nodes.empty?
    end
    nodes[rand(nodes.length)].id
  }
  port.phy_type {
    pts = HwTypes::PHY_TYPES.map{|disp, value| value }
    raise "Failed to create Port - HwTypes:PHY_TYPES is empty!" if pts.empty?
    pts[rand(pts.length)]
  }
end
