Factory.sequence :network_manager_name do |n|
  "Network Manager #{n}"
end

Factory.define :network_manager do |nm|
  nm.name { Factory.next(:network_manager_name) }
  nm.nm_type {
    random_item(HwTypes::NM_TYPES, "Network Manager", "HwTypes:NM_TYPES")
  }
end
