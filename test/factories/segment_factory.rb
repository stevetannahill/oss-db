Factory.define :segment do |segment|
  segment.path_network_id { random_id(OperatorNetwork.all.reject{|on| on.paths.empty?}, "Segment", "Operator Network", "that have Paths") }
  segment.ethernet_service_type_id {|o| random_id(o.get_candidate_ethernet_service_types, "Segment", "Ethernet Service Type", "for Network #{o.get_operator_network.name}") }
  segment.segment_type_id {|o| random_id(o.ethernet_service_type.segment_types, "Segment", "Segment Type", "for Ethernet Service Type #{o.ethernet_service_type.name}") }
  segment.use_member_attrs false
end

Factory.define :on_net_ovc, :class => OnNetOvc, :parent => :segment do |onovc|
  onovc.operator_network_id nil
  onovc.segment_owner_role "Seller"
  onovc.site_id {
    sites = Site.all.reject{|site| site.demarcs.size == 0 || !site.operator_network.operator_network_type || site.operator_network.operator_network_type.ethernet_service_types.reject{|est| est.segment_types.empty?}.empty? }
    random_id(sites, "On Net OVC", "Site", "that have Operator Network Types with Ethernet Service Types that have Segment Types! and have Demarcs")
  }
  
  onovc.service_id {|c|
    id = nil
    (c.site.service_id_low..c.site.service_id_high).each do |i|
      unless OnNetOvc.find(:first, :conditions => ["service_id = ?", i])
        id = i
        break
      end
    end
    raise "Failed to create On Net OVC - No more service id's for On Net OVC's on Site:#{c.site.name}" if !id
    id
  }
end

Factory.define :off_net_ovc, :class => OffNetOvc, :parent => :segment do |offovc|
  offovc.path_network_id { random_id(OperatorNetwork.all.reject{|on| on.paths.empty?}, "Segment", "Operator Network", "that have Paths") }
  offovc.segment_owner_role {|o|
    ons = OperatorNetwork.all.reject{|on| !on.operator_network_type || on.operator_network_type.ethernet_service_types.reject{|est| est.segment_types.empty?}.empty? }
    raise "Failed to create Off Net OVC - No Operator Networks exist that have Operator Network Types with Ethernet Service Types that have Segment Types!" if ons.empty?
    ons = ons & o.path_network.service_provider.operator_networks
    ons.empty? ? "Seller" : random_item(ServiceProviderTypes::OWNER_ROLE.map{|disp, value| value }, "Off Net OVC", "ServiceProviderTypes::OWNER_ROLE")
  }
  offovc.operator_network_id {|o|
    ons = case o.segment_owner_role
      when "Seller" then OperatorNetwork.all
      when "Buyer" then o.path_network.service_provider.operator_networks
    end
    ons.reject!{|on| !on.operator_network_type || on.operator_network_type.ethernet_service_types.reject{|est| est.segment_types.empty?}.empty? }
    random_id(ons, "Off Net OVC", "Operator Network", "for #{o.segment_owner_role} on #{o.path_network.name} that have Operator Network Types with Ethernet Service Types that have Segment Types")
  }
end
