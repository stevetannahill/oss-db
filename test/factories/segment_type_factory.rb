Factory.sequence :segment_type_name do |n|
  "Segment Type #{n}"
end

Factory.define :segment_type do |segmentt|
  segmentt.name { Factory.next(:segment_type_name) }
  segmentt.operator_network_type_id { random_id(OperatorNetworkType.find(:all), "Segment Type", "Operator Network Type") }
  segmentt.max_mtu { random_mtu }
  segmentt.color_marking { random_item(FrameTypes::FRAME_COLOR_MARKING_TYPES.map{|disp, value| value }, "Segment Type", "FrameTypes::FRAME_COLOR_MARKING_TYPES") }
  segmentt.unicast_frame_delivery_conditions { random_item(ServiceCategories::SERVICE_FRAME_DELIVERY.map{|disp, value| value }, "Segment Type", "ServiceCategories::SERVICE_FRAME_DELIVERY") }
  segmentt.multicast_frame_delivery_conditions { random_item(ServiceCategories::SERVICE_FRAME_DELIVERY.map{|disp, value| value }, "Segment Type", "ServiceCategories::SERVICE_FRAME_DELIVERY") }
  segmentt.broadcast_frame_delivery_conditions { random_item(ServiceCategories::SERVICE_FRAME_DELIVERY.map{|disp, value| value }, "Segment Type", "ServiceCategories::SERVICE_FRAME_DELIVERY") }
  segmentt.mef9_cert { random_item(ServiceCategories::SERVICE_MEF9_CERTIFICATION.map{|disp, value| value }, "Segment Type", "ServiceCategories::OWNER_ROLE") }
  segmentt.mef14_cert { random_item(ServiceCategories::SERVICE_MEF14_CERTIFICATION.map{|disp, value| value }, "Segment Type", "ServiceCategories::SERVICE_MEF9_CERTIFICATION") }
  segmentt.c_vlan_id_preservation { random_item(ServiceCategories::CE_VLAN_PRESERVATION.map{|disp, value| value }, "Segment Type", "ServiceCategories::CE_VLAN_PRESERVATION") }
  segmentt.c_vlan_cos_preservation { ["Yes","No"][rand(2)] }
  segmentt.segment_type { random_item(HwTypes::Path_TYPES, "Path Type", "HwTypes::Path_TYPES") }
  segmentt.max_num_cos { 1 }
end
