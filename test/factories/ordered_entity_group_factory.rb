Factory.sequence :ordered_entity_group_name do |n|
  "Ordered Entity Group #{n}"
end

Factory.define :ordered_entity_group do |ot|
  ot.name { Factory.next(:ordered_entity_group_name) }
  ot.order_type_id { random_id(OrderType.find(:all).reject{|ot| ot.operator_network_type.nil? || ot.operator_network_type.ethernet_service_types.empty?}, "Ordered Entity Group", "Order Type", "with Ethernet Service Types") }
  ot.ethernet_service_type_id {|o| random_id(OperatorNetworkType.find(o.order_type.operator_network_type_id).ethernet_service_types, "Ordered Entity Group", "Ethernet Service Type", "for Operator Network Type #{OperatorNetworkType.find(o.order_type.operator_network_type_id).name}") }
end
