Factory.sequence :demarc_type_name do |n|
  "Demarc Type #{n}"
end

Factory.define :demarc_type do |dt|
  dt.name { Factory.next(:demarc_type_name) }
  dt.operator_network_type_id { random_id(OperatorNetworkType.find(:all), "Demarc Type", "Operator Network Type") }
  dt.physical_medium "Generic Physical Medium"
  dt.mtu { random_mtu }
  dt.connected_device_type { random_list(HwTypes::DEMARC_DEVICE_TYPES.map{|disp, value| value}, "Demarc Type", "HwTypes::DEMARC_DEVICE_TYPES", 0, 12) }
end

Factory.define :enni_type, :class => EnniType, :parent => :demarc_type do |et|
  et.demarc_type_type "ENNI Type"
  et.physical_medium { random_list(HwTypes::PHY_TYPES.map{|disp, value| value }, "ENNI Type", "HwTypes::PHY_TYPES", 1, 6, "\n") }
  et.frame_format { random_item(FrameTypes::ETHER_TYPES.map{|disp, value| value }, "ENNI Type", "FrameTypes::ETHER_TYPES") }
  et.outer_tag_segment_mapping { random_item(HwTypes::TAG_MAPPING_TYPES, "ENNI Type", "HwTypes::TAG_MAPPING_TYPES") }
  et.lag_type { random_item(HwTypes::LAG_TYPES.map{|disp, value| value }, "ENNI Type", "HwTypes::LAG_TYPES") }
  et.auto_negotiate { random_item(HwTypes::PHY_HANDOFF_TYPES, "Handoff Type", "HwTypes::PHY_HANDOFF_TYPES") }
  et.lag_control { random_item(HwTypes::LAG_SELECTION_CONTROL, "Handoff Type", "HwTypes::LAG_SELECTION_CONTROL") }
end

Factory.define :uni_type, :class => UniType, :parent => :demarc_type do |et|
  et.demarc_type_type "UNI Type"
  et.physical_medium { random_list(HwTypes::UNI_PHY_TYPES.map{|disp, value| value }, "UNI Type", "HwTypes::UNI_PHY_TYPES", 1, 20) }
  et.reflection_mechanisms { random_list(FrameTypes::MONITORING_FRAME_REFLECTION.map{|value| value[0]}, "UNI Type", "FrameTypes::MONITORING_FRAME_REFLECTION") }
  et.auto_negotiate { random_item(HwTypes::PHY_HANDOFF_TYPES, "Handoff Type", "HwTypes::PHY_HANDOFF_TYPES") }
end
