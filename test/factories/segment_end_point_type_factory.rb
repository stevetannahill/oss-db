Factory.sequence :segment_end_point_type_name do |n|
  "Segment End Point Type #{n}"
end

Factory.define :segment_end_point_type do |segmentept|
  segmentept.name { Factory.next(:segment_end_point_type_name) }
  segmentept.operator_network_type_id { random_id(OperatorNetworkType.find(:all), "Segment End Point Type", "Operator Network Type") }
end
