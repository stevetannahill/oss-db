Factory.sequence :ethernet_service_type_name do |n|
  "Ethernet Service Type #{n}"
end

Factory.define :ethernet_service_type do |est|
  est.name { Factory.next(:ethernet_service_type_name) }
  est.operator_network_type_id {
    onts = OperatorNetworkType.find(:all)
    raise "Failed to create Ethernet Service Type - No Operator Network Types exist!" if onts.empty?
    onts[rand(onts.length)].id
  }
end
