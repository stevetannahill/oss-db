Factory.sequence :oss_info_name do |n|
  "OSS Info #{n}"
end

Factory.define :oss_info do |ossi|
  ossi.name { Factory.next(:oss_info_name) }
  ossi.operator_network_type_id { random_id(OperatorNetworkType.find(:all), "OSS Info", "Operator Network Type") }
end