Factory.sequence :path_member_handle do |n|
  "Path #{n}"
end

Factory.define :path do |path|
  path.operator_network_id { random_id(OperatorNetwork.all, "Path", "OperatorNetwork") }
#  path.member_handle { Factory.next(:path_member_handle) }
end

Factory.define :evc, :class => Evc, :parent => :path do |path|
end
