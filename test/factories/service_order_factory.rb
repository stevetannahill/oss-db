Factory.sequence :service_order_title do |n|
  "Service Order #{n}"
end

Factory.define :service_order do |so|
  so.title { Factory.next(:service_order_title) }
  so.operator_network_id {|s|
    if s.ordered_entity != nil
      s.ordered_entity.operator_network_id ? s.ordered_entity.operator_network_id : s.ordered_entity.get_operator_network.id
    else
      (s.is_a?(SegmentOrder) && s.path) ? random_id(s.path.service_provider.operator_networks, "Service Order", "Operator Networks", "for Service Provider #{s.path.service_provider.name}") : random_id(OperatorNetwork.all, "Service Order", "Operator Network")
    end
  }
  so.ordered_operator_network_id {|s|
    if s.type == EnniOrderNew.name
      random_id(OperatorNetwork.all.reject{|on| on.service_provider.is_system_owner}, "Service Order", "Operator Network")
    else
      random_id(OperatorNetwork.all, "Service Order", "Operator Network")
    end
  }
  so.primary_contact_id {|s| random_id(OperatorNetwork.find(s.operator_network_id).get_candidate_contacts, "Service Order", "Contacts", "for Operator Network #{OperatorNetwork.find(s.operator_network_id).name}") }
  so.technical_contact_id {|s| random_id(OperatorNetwork.find(s.operator_network_id).get_candidate_contacts, "Service Order", "Contacts", "for Operator Network #{OperatorNetwork.find(s.operator_network_id).name}") }
  so.testing_contact_id {|s| random_id(OperatorNetwork.find(s.operator_network_id).get_candidate_contacts, "Service Order", "Contacts", "for Operator Network #{OperatorNetwork.find(s.operator_network_id).name}") }
  so.local_contact_id {|s| random_id(OperatorNetwork.find(s.operator_network_id).get_candidate_contacts, "Service Order", "Contacts", "for Operator Network #{OperatorNetwork.find(s.operator_network_id).name}") }

  so.action {|s| (s.ordered_entity && s.ordered_entity.service_orders.length > 1) ? random_item(ServiceProviderTypes::ORDER_TYPES.map{|disp, value| value} - ["New"], "Service Order", "ServiceProviderTypes::ORDER_TYPES") : "New" }
  so.order_created_date { Date.today + rand(200) - 100 }
  so.order_received_date {|s| s.order_created_date + rand(60) }
  so.requested_service_date {|s| s.order_received_date + rand(60) }
end

Factory.define :segment_order, :class => SegmentOrder, :parent => :service_order do |so|
  so.ordered_entity_type "Segment"
  so.path_id {|s| s.ordered_entity ? s.ordered_entity.path_id : random_id(Path.all, "Segment Order", "Path") }
end

Factory.define :on_net_ovc_order, :class => OnNetOvcOrder, :parent => :segment_order do |so|
  so.ordered_entity_subtype "OnNetOvc"
  so.ordered_entity_id { (OnNetOvc.all.length > 0 && rand(3) > 0) ? random_id(OnNetOvc.all, "On Net OVC Order", "On Net OVC") : nil } 
end

Factory.define :off_net_ovc_order, :class => OffNetOvcOrder, :parent => :segment_order do |so|
  so.ordered_entity_subtype "OffNetOvc"
  so.ordered_entity_id { 
    offovcs = OffNetOvc.all.reject{|offovc| offovc.get_operator_network.nil? || offovc.get_operator_network.operator_network_type.nil? || offovc.get_operator_network.operator_network_type.ordered_entity_groups.select{|oeg| oeg.ordered_entity_types.include? offovc.segment_type}.empty?}
    (offovcs.length > 0 && rand(3) > 0) ? random_id(offovcs, "Off Net OVC Order", "Off Net OVCs", "with an Operator Network who's Operator Network Type has a Ordered Entity Group that contains the Off Net OVC's Segment Type") : nil
  }
  so.ordered_entity_group_id {|s|
    if s.ordered_entity
      raise "Failed to create Off Net OVC Order - Ordered Entity must be a Off Net OVC" if !s.ordered_entity.is_a? OffNetOvc
      raise "Failed to create Off Net OVC Order - Ordered Entity must have a Operator Network" if s.ordered_entity.get_operator_network.nil?
      raise "Failed to create Off Net OVC Order - Ordered Entity must have a Operator Network with a Operator Network Type" if s.ordered_entity.get_operator_network.operator_network_type.nil?
      random_id(s.ordered_entity.get_operator_network.operator_network_type.ordered_entity_groups.select{|oeg| oeg.ordered_entity_types.include? offovc.segment_type}, "Off Net OVC Order", "Ordered Entity Groups", "that contain the Segment Type #{s.ordered_entity.segment_type.name}")
    else
      random_id(OrderedEntityGroup.all.reject{|oeg| oeg.ordered_entity_types.select{|oet| oet.is_a? SegmentType}.empty?}, "Off Net OVC Order", "Ordered Entity Group", "that contain Segment Types")
    end
  }
  so.ordered_object_type {|s|
    if s.ordered_entity
      raise "Failed to create Off Net OVC Order - Ordered Entity must be a Off Net OVC" if !s.ordered_entity.is_a? OffNetOvc
      raise "Failed to create Off Net OVC Order - Ordered Entity must have a Segment Type" if s.ordered_entity.segment_type.nil?
      s.ordered_entity.segment_type
    else
      s.ordered_entity_group_id ? random_item(OrderedEntityGroup.find(s.ordered_entity_group_id).ordered_entity_types.select{|oet| oet.is_a? SegmentType}, "Off Net OVC Order", "Ordered Entity Types", "for the Ordered Entity Group #{OrderedEntityGroup.find(s.ordered_entity_group_id).name}") : nil
    end
  }
end

Factory.define :enni_order_new, :class => EnniOrderNew, :parent => :service_order do |so|
  so.ordered_entity_type "Demarc"
  so.ordered_entity_subtype "ENNI"
  so.ordered_entity_id { (EnniNew.all.length > 0 && rand(3) > 0) ? random_id(EnniNew.all, "ENNI Order", "ENNI") : nil }
end
