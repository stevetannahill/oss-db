Factory.sequence :network_management_server_name do |n|
  "Network Managent Server #{n}"
end

Factory.define :network_management_server do |nm_server|
  nm_server.name { Factory.next(:network_management_server_name) }
  nm_server.network_manager_id {
    random_id(NetworkManager.all, "Network Management Server", "Network Manager")
  }
  nm_server.primary_ip {
    Factory.next(:ip)
  }
end
