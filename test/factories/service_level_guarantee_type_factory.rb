Factory.define :service_level_guarantee_type do |slgt|
  slgt.class_of_service_type_id { random_id(ClassOfServiceType.all(), "SLG Type", "COS Type") }
  slgt.min_dist_km {|st|
    dists = st.class_of_service_type.service_level_guarantee_types.collect{|s| s.max_dist_km.to_i}
    dists.empty? ? 0 : dists.sort.last
  }
  slgt.max_dist_km {|st|
    st.min_dist_km.to_i + rand(50) + 50
  }
  slgt.delay_us 5000
  slgt.delay_variation_us 5
end