Factory.define :bw_profile_type do |bwpt|
  bwpt.class_of_service_type_id {
    costs = ClassOfServiceType.all.select{|cost| cost.bw_profile_types.size < FrameTypes::LOCALE_DIRECTION.size}
    random_id(costs, "BWP Type", "COS Type", "with free BWP Types") 
  }
  bwpt.bwp_type {|bt|
    bwpts = FrameTypes::LOCALE_DIRECTION - bt.class_of_service_type.bw_profile_types.collect{|b| b.bwp_type}
    random_item(bwpts, "BWP Type", "FrameTypes::LOCALE_DIRECTION", "for COS Type #{bt.class_of_service_type.name}") 
  }
  bwpt.color_mode { random_item(ServiceCategories::COLOR_MODE, "BWP Type", "ServiceCategories::COLOR_MODE") }
  bwpt.coupling_flag { random_item(ServiceCategories::COUPLING_FLAG, "BWP Type", "ServiceCategories::COUPLING_FLAG") }
  bwpt.rate_limiting_mechanism { random_item(ServiceCategories::RATE_LIMITING_MECHANISM, "COS Type", "ServiceCategories::RATE_LIMITING_MECHANISM") }
end