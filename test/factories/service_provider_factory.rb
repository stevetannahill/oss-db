Factory.sequence :service_provider_name do |n|
  "Service Provider #{n}"
end

Factory.define :service_provider do |sp|
  sp.name { Factory.next(:service_provider_name) }
end
