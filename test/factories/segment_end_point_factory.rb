Factory.define :segment_end_point do |segmentep|
  segmentep.segment_id { random_id(OffNetOvc.all, "Segment End Point", "Off Net OVC") }
  segmentep.segment_end_point_type_id {|o|
    segmentts = o.get_candidate_segment_end_point_types
    segmentts.empty? ? 1 : segmentts[rand(segmentts.length)].id
  }
end

Factory.define :ovc_end_point_enni, :class => OvcEndPointEnni, :parent => :segment_end_point do |segmentep|
end

Factory.define :ovc_end_point_uni, :class => OvcEndPointUni, :parent => :segment_end_point do |segmentep|
end

Factory.define :on_net_ovc_end_point_enni, :class => OnNetOvcEndPointEnni, :parent => :ovc_end_point_enni do |segmentep|
  segmentep.segment_id { random_id(OnNetOvc.all, "On Net Segment End Point", "On Net OVC") }
end