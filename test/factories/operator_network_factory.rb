Factory.sequence :operator_network_name do |n|
  "Operator Network #{n}"
end

Factory.define :operator_network do |on|
  on.name { Factory.next(:operator_network_name) }
  on.service_provider_id {
    sps = ServiceProvider.find(:all)
    raise "Failed to create Operator Network - No Service Providers exist!" if sps.empty?
    sps.reject!{|sp| sp.operator_network_types.empty?}
    random_id(sps, "Operator Network", "Service Provider", "that contain Operator Network Types")
  }
  on.operator_network_type_id { |o| random_id(o.service_provider.operator_network_types, "Operator Network", "Operator Network Types", "for Service Provider:#{o.service_provider.name}") }
end
