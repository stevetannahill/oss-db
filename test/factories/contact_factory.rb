Factory.sequence :contact_name do |n|
  "Contact #{n}"
end

Factory.define :contact do |c|
  c.name { Factory.next(:contact_name) }
  c.work_phone { random_phone_number }
  c.mobile_phone { random_phone_number }
  c.fax { random_phone_number }
  c.address { random_street_address }
  c.service_provider_id {
    sps = ServiceProvider.find(:all)
    raise "Failed to create Contact - No Service Providers exist!" if sps.empty?
    sps[rand(sps.length)].id
  }
  c.e_mail {|contact| "#{contact.name.gsub(/\s+/, '.')}@#{(contact.service_provider && contact.service_provider.website) ? contact.service_provider.website.gsub(/(.*:\/\/)|(www\.)/,'') : 'example.com'}" }
end
