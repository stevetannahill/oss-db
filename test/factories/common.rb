MAX_STAG = 4094
MAX_MTU = 9194
STREET_NUMBER_LIMIT = 10000

STREET_TYPES = [ "Ave", "Blvd", "Rd", "St", "Dr", "Gate", "Ln", "Pl", "Ter", "Trl", "Way", "Ct", "Pl", "Cres", "Sq", "Pkwy" ]

STREET_NAMES = [ # A large, unique, and meaningful set of names to be used for streets
                 # First 32 uniquely-named Presidents (two Adams', Roosevelts')
                 "George Washington", "Adams", "Jefferson", "Madison", "Monroe", "Jackson", "Van Buren", "Harrison", "Tyler", "Polk",
                   "Taylor", "Fillmore", "Pierce", "Buchanan", "Lincoln", "Johnson", "Grant", "Hayes", "Garfield", "Arthur",
                   "Cleveland", "Harrison", "McKinley", "Roosevelt", "Taft", "Wilson", "Harding", "Coolidge", "Hoover", "Truman",
                   "Eisenhower", "Kennedy",

                 # 49 Signatories to the Declaration of Independence, who are not already named above
                 "Bartlett", "Braxton", "Carroll", "Chase", "Clark", "Clymer", "Ellery", "Floyd", "Franklin", "Gerry",
                   "Gwinnett", "Hancock", "Hall", "Hart", "Hewes", "Heyward", "Hooper", "Hopkins", "Hopkinson", "Huntington",
                   "Lee", "Lewis", "Livingston", "Lynch", "McKean", "Middleton", "Morris", "Morton", "Nelson", "Paca",
                   "Penn", "Paine", "Read", "Rodney", "Ross", "Rush", "Rutledge", "Sherman", "Smith", "Stockton",
                   "Stone", "Thomson", "Thornton", "Walton", "Whipple", "Williams", "Witherspoon", "Walcott", "Wythe",

                 # 50 US States
                 "Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colerado", "Connecticut", "Delaware", "Florida", "Georgia",
                   "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland",
                   "Massachusetts", "Michigan", "Missouri", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey",
                   "New Mexico", "New York", "N. Carolina", "N. Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "S. Carolina",
                   "S. Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "W. Virginia", "Wisconsin", "Wyoming",

                 # 44 State Capitals that don't create duplicate names
                 "Montgomery", "Juneau", "Phoenix", "Little Rock", "Sacramento", "Denver", "Hartford", "Dover", "Tallahassee", "Atlanta",
                   "Honolulu", "Boise", "Springfield", "Indianapolis", "Des Moines", "Topeka", "Frankfort", "Baton Rouge", "Augusta", "Annapolis",
                   "Boston", "Lansing", "Saint Paul", "Helena", "Carson", "Concord", "Trenton", "Santa Fe", "Albany", "Raleigh",
                   "Bismarck", "Columbus", "Salem", "Harrisburg", "Providence", "Pierre", "Nashville", "Austin", "Montpelier", "Richmond",
                   "Olympia", "Charleston", "Cheyenne", "Columbia",

                 # 83 Non-Active Supreme Court Justices that don't create duplicate names
                 "Wilson", "Jay", "Cushing", "Blair", "Iredell", "Paterson", "Chase", "Ellsworth", "Moore", "Marshall",
                   "Todd", "Duvall", "Story", "Thompson", "Trimble", "McLean", "Baldwin", "Wayne", "Taney", "Barbour",
                   "Catron", "Daniel", "Woodbury", "Grier", "Curtis", "Campbell", "Clifford", "Swayne", "Miller", "Davis",
                   "Field", "Strong", "Bradley", "Hunt", "Waite", "Harlan", "Woods", "Matthews", "Gray", "Blatchford",
                   "Lamar", "Fuller", "Brewer", "Brown", "Shiras", "Peckham", "McKenna", "Holmes", "Day", "Moody",
                   "Lurton", "Hughes", "Devanter", "Pitney", "McReynolds", "Brandies", "Clarke", "Sutherland", "Butler", "Sanford",
                   "Roberts", "Cardozo", "Black", "Reed", "Frankfurter", "Douglas", "Murphy", "Byrnes", "Burton", "Vinson",
                   "Clark", "Minton", "Warren", "Whittaker", "Stewart", "Goldberg", "Fortas", "Burger", "Blackmun", "Powell",
                   "Rehnquist", "O'Connor", "Souter",
              
                 # First 55 NASA Astronauts - "Mercury 7", "The New Nine", "The Fourteen", "The Scientists", "The Original 19"
                 "Shepard", "Grissom", "Glenn", "Carpenter", "Schirra", "Cooper", "Slayton",
                 "Armstrong", "Borman", "Conrad", "Lovell", "McDivitt", "See", "Stafford", "White", "Young",
                 "Aldrin", "Anders", "Bassett", "Bean", "Cernan", "Chaffee", "Collins", "Cunningham", "Eisele", "Freeman",
                 "Gordon", "Schweickart", "Scott", "Williams",
                 "Garriott", "Gibson", "Graveline", "Kerwin", "Michel", "Shmitt",
                 "Brand", "Bull", "Carr", "Duke", "Engle", "Evans", "Givens", "Haise", "Irwin", "Lind",
                   "Lousma", "Mattingly", "McCandless", "Mitchell", "Pogue", "Roosa", "Swigert", "Weitz", "Worden",
               ]



Factory.sequence :ip do |n|
  ip = ""
  for i in 1..4
    section = n % 100**i
    n = n / 100**i
    ip = "." + section.to_s + ip
  end
  ip[1..-1]
end

Factory.sequence :mac do |n|
  ip = ""
  for i in 1..6
    section = n % 256**i
    n = n / 256**i
    ip = ":" + ("%02x" % section.to_s) + ip
  end
  ip[1..-1]
end


def random_id(objs, type_name, list_type_name, append = false)
  raise "Failed to create #{type_name} - No #{list_type_name}s exist#{append ? " " + append : ""}!" if objs.empty?
  objs[rand(objs.length)].id
end

def random_item(items, type_name, list_name, append = false)
  raise "Failed to create #{type_name} - #{list_name} is empty#{append ? " " + append : ""}!" if items.empty?
  items[rand(items.length)]
end

def random_list(items, type_name, list_name, min = 0, max = false, delimiter = "; ")
  raise "Failed to create #{type_name} - #{list_name} is empty!" if items.empty?
  if max && max < 0
    raise "Failed to create #{type_name} - maximum #{list_name} is less than 0!"
  end
  if max && min > max
    raise "Failed to create #{type_name} - maximum #{list_name} is less than 0!"
  end
  selected = []
  (0..(rand(((max && max <= items.length) ? max : items.length) - min) + min)).each do |i|
    item = items[rand(items.length)]
    while(selected.include? item)
      item = items[rand(items.length)]
    end
    selected.push(item)
  end
  selected.join(delimiter)
end

def random_from_list(list)
  list[rand(list.size)]
end

def random_number(len)
  num = ""
  for i in 1..len
    num << rand(9).to_s
  end

  return num
end

def random_stag
  rand(MAX_STAG)+1
end

def random_mtu
  rand(MAX_MTU)+1
end

def random_phone_number
  "#{random_number(3)}-#{random_number(3)}-#{random_number(4)}"
#  "#{rand(8)+1}#{rand(9)}#{rand(9)}-#{rand(8)+1}#{rand(9)}#{rand(9)}-#{rand(9)}#{rand(9)}#{rand(9)}#{rand(9)}"
end

def random_mac
  mac = Array.new

  # yes MAC octets go to ff
  for i in 1..6
    mac.push(random_number(2))
  end

  return mac.join(":")
end

def random_ipv4
  "#{rand(255)}.#{rand(255)}.#{rand(255)}.#{rand(255)}"
end

def random_ipv6
  "#{rand(255)}.#{rand(255)}.#{rand(255)}.#{rand(255)}/#{rand(32)+1}"
end

def random_street_address
  "#{rand(STREET_NUMBER_LIMIT)} #{random_from_list(STREET_NAMES)} #{random_from_list(STREET_TYPES)}"
end

