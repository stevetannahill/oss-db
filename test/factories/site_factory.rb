Factory.sequence :site_name do |n|
  "Site #{n}"
end

Factory.sequence :site_node_prefix do |n|
  "SITE#{n}"
end

Factory.sequence :site_net_addr do |n|
  ip = n * 1024
  ip = ip % 1000000000
  ip = ("%09d" % ip)
  (ip[0..2] + "." + ip[3..5] + "." + ip[6..8] + ".0/" + (n % 100).to_s).gsub(/(^|\.)0+(\d)/){|s| "#{$1}#{$2}"}
end

Factory.sequence :service_id_low do |n|
    (n-1) * 100
end

Factory.sequence :service_id_high do |n|
    n * 100 - 1
end

Factory.define :site do |site|
  site.name { Factory.next(:site_name) }
  site.city { |e| "City of #{e.name}" }
  site.country { |s| "Country of #{s.name}" }
  site.operator_network_id { |e|
    if (e.pop and e.pop.operator_network)
      e.pop.operator_network.id
    else
      random_id(OperatorNetwork.find_cenx_networks_w_no_sites_attached, "Exchange", "Cenx Operator Network")
    end
  }
  site.node_prefix { Factory.next(:site_node_prefix) }
  site.site_host "Host Name"
  site.address "123 A St.\nCity, State\nPostal Code"
  site.contact_info "Phone around!"
  site.floor "13th"
  site.clli {|e| e.node_prefix + "CENX0" }
  site.broadcast_address { Factory.next(:ip) }
  site.network_address { Factory.next(:site_net_addr) }
  site.host_range {|e|
    main = e.network_address.match(/^(\d{1,3}\.){3}/)[0]
    "#{main}.0 - #{main}.255"
  }
  site.hosts_per_subnet "510"
  site.service_id_low { Factory.next(:service_id_low) }
  site.service_id_high { Factory.next(:service_id_high) }
end
