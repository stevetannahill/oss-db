Factory.sequence :node_name do |n|
  "Node #{n}"
end

Factory.sequence :node_mask do |n|
  "/" + (n % 100).to_s
end

Factory.define :node do |node|
  node.site_id {
    random_id(Site.find(:all), "Node", "Site")
  }
  node.cabinet_id {|n|
    cabs = Cabinet.find(:all, :conditions => ["site_id = ?", n.site_id])
    cabs.empty? ? nil :cabs[rand(cabs.length)].id
  }
  node.node_type {
    random_item(HwTypes::NODE_TYPES.map{|disp, value| value}, "Node", "HwTypes:NODE_TYPES")
  }
  node.name {|n| n.default_name }
  node.primary_ip {|n| ip = Node.new(n.attributes).default_ip("primary"); ip ? ip : Factory.next(:ip)}
  node.secondary_ip {|n| Node.new(n.attributes).default_ip("secondary") }
  node.gateway_ip {|n| Node.new(n.attributes).default_ip("gateway") }
  node.loopback_ip { Factory.next(:ip) }
  node.net_mask { Factory.next(:node_mask) }
  node.network_manager_id{|n|
    random_id(n.get_candidate_network_managers, "Node", "Network Manager", "that can attach to a Node of type #{n.node_type}")
  }
end
