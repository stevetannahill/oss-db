Factory.sequence :member_attr_name do |n|
  "Member Attr #{n}"
end

Factory.define :member_attr do |ma|
  ma.name { Factory.next(:member_attr_name) }
  ma.syntax { random_item(MemberAttrTypes::ATTR_TYPES.map{|disp, value| value }, "MemberAttr", "MemberAttrTypes::ATTR_TYPES") }
  ma.allow_blank true
  ma.disabled false
  ma.affected_entity_type{ random_item(TypeElement.get_subclasses, "MemberAttr", "Subclasses of Type Element").class_name }
  ma.affected_entity_id{|m| random_id(m.affected_entity_type.constantize.all, "MemberAttr", "Subclasses of Type Element").class_name }
  ma.value_range {|m| (m.syntax == "menu") ? "1;2;3;4;5" : "" }
end

Factory.define :member_handle, :parent => :member_attr, :class => MemberHandle do |mh|
end

Factory.define :class_member_attr, :parent => :member_attr do |cma|
  cma.affected_entity_id nil
end

Factory.define :class_member_handle, :parent => :class_member_attr, :class => MemberHandle do |cmh|
end
