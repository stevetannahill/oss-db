Factory.sequence :path_type_name do |n|
  "Path Type #{n}"
end

Factory.define :path_type do |patht|
  patht.name { Factory.next(:path_type_name) }
  patht.operator_network_type_id { random_id(OperatorNetworkType.find(:all), "Segment Type", "Operator Network Type") }
  patht.mtu { random_mtu }
  patht.max_num_unis { rand(498)+2 }
  patht.path_type { random_item(HwTypes::Path_TYPES, "Segment Type", "HwTypes::Path_TYPES") }
end

Factory.define :point_to_point_path_type, :parent => :path_type do |patht|
  patht.max_num_unis { 2 }
  patht.path_type { "Point to Point" }
end
