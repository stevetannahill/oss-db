# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_stats_session',
  :secret      => '3be0b0cd54e7b9274bc7d857e5bec22b7f79ae1ef18504c3799b3c8886dfe060ad4e1dc4732f7df497dd5881aa4b5de628e24c2dea5f5c8acbd93c71d345c1e3'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
